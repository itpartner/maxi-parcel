(function($) {

    var reEscape = new RegExp('(\\' + ['/', '.', '-', '*', '+', '?', '|', '(', ')', '[', ']', '{', '}', '\\'].join('|\\') + ')', 'g');

    function fnFormatResult(value, data, currentValue) {
        value = value || '';
        var pattern = '(' + currentValue.replace(reEscape, '\\$1') + ')';
        return value.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>');
    }

    function Autocomplete(el, options) {
        this.el = $(el);
        this.el.attr('autocomplete', 'off');
        this.data = [];
        this.badQueries = [];
        this.selectedIndex = -1;
        this.currentValue = this.el.val();
        this.intervalId = 0;
        this.cachedResponse = [];
        this.onChangeInterval = null;
        this.cacheKeyPrefix = 'cache_prefix_';
        this.serviceUrl = options.serviceUrl;
        this.isLocal = false;
        this.isBusy = false;
        this.options = {
            autoSubmit: false,
            minChars: 2,
            maxHeight: 250,
            deferRequestBy: 100,
            width: 0,
            highlight: true,
            noCache : true,
            CacheLiveTime: 60*60*2,  // 2 hours
            params: {},
            fnFormatResult: fnFormatResult,
            delimiter: null,
            zIndex: 9999
        };
        this.initialize();

        if( options )
            this.setOptions(options);
    }

    $.fn.autocomplete = function(options) {
        return new Autocomplete(this.get(0)||$('<input />'), options);
    };


    Autocomplete.prototype = {

        killerFn: null,

        initialize: function() {

            var me, uid, autocompleteElId;
            me = this;
            uid = Math.floor(Math.random()*0x100000).toString(16);
            autocompleteElId = 'Autocomplete_' + uid;

            this.killerFn = function(e) {
                if ($(e.target).parents('.autocomplete').size() === 0) {
                    me.killSuggestions();
                    me.disableKillerFn();
                }
            };

            if (!this.options.width) { this.options.width = this.el.width(); }
            this.mainContainerId = 'AutocompleteContainter_' + uid;

            this.el.parent().append('<div id="' + this.mainContainerId + '" style="position:absolute;z-index:9999;"><div class="autocomplete-w1"><div class="autocomplete" id="' + autocompleteElId + '" style="display:none;"></div></div></div>');
            //$('<div id="' + this.mainContainerId + '" style="position:absolute;z-index:9999;"><div class="autocomplete-w1"><div class="autocomplete" id="' + autocompleteElId + '" style="display:none; width:300px;"></div></div></div>').appendTo('body');

            this.container = $('#' + autocompleteElId);
            this.fixPosition();
            if (window.opera) {
                this.el.keypress(function(e) { me.onKeyPress(e); });
            } else {
                this.el.keydown(function(e) { me.onKeyPress(e); });
            }
            this.el.keyup(function(e) { me.onKeyUp(e); });
            this.el.blur(function() { me.enableKillerFn(); });
            this.el.focus(function() { me.fixPosition(); });
        },

        setOptions: function(options){
            var o = this.options;
            $.extend(o, options);
            if(o.lookup){
                this.isLocal = true;
                if($.isArray(o.lookup)){ o.lookup = { data:o.lookup }; }
            }
            $('#'+this.mainContainerId).css({ zIndex:o.zIndex });

            var sWidth = o.width+4;

            if(this.el.parent().parent().is('div.suggest-combo')) sWidth = this.el.parent().parent().width() - 2;

            this.container.css({ maxHeight: o.maxHeight + 'px', minWidth:sWidth });
        },

        clearCache: function(){
            this.cachedResponse = [];
            this.badQueries = [];
        },

        disable: function(){
            this.disabled = true;
        },

        enable: function(){
            this.disabled = false;
        },

        fixPosition: function() {
            var offset = this.el.position();
            $('#' + this.mainContainerId).css({ top: (offset.top + this.el.outerHeight()) + 'px', left: offset.left + 'px' });
        },

        enableKillerFn: function() {
            var me = this;

            $(document).bind('click', me.killerFn);

            if( me.options.params.forceSelection != undefined )
            {
                if( (me.el[0]._value != undefined) && (me.el[0]._value.value != undefined) )
                {
                    if(me.el[0]._value.value != me.el.val())
                    {
                        if( me.el.val() == '')
                        {
                            //					me.el[0]._value = {};
                            me.el[0].setValue('');
                        }
                        else {
                            //					me.el.val(me.el[0]._value.value);
                            me.el[0].setValue(me.el[0]._value);
                        }
                    }
                }
                else
                    me.el.val('');
            }
        },

        disableKillerFn: function() {
            var me = this;
            $(document).unbind('click', me.killerFn);
        },

        killSuggestions: function() {
            var me = this;
            this.stopKillSuggestions();
            this.intervalId = window.setInterval(function() { me.hide(); me.stopKillSuggestions(); }, 300);
        },

        stopKillSuggestions: function() {
            window.clearInterval(this.intervalId);
        },

        onKeyPress: function(e) {
            if (this.disabled) return;
            if(!this.enabled && e.keyCode == 40) {
                this.onValueChange();
                return;
            }

            // return will exit the function
            // and event will not be prevented
            //console.log(e.keyCode);
            switch (e.keyCode) {
                case 27: //KEY_ESC:
                    this.el.val(this.currentValue);
                    this.hide();
                    break;
                case 9: //KEY_TAB:
                case 13: //KEY_RETURN:
                    if (this.selectedIndex === -1) {
                        this.hide();
                        return;
                    }
                    this.select(this.selectedIndex);
                    if(e.keyCode === 9){ return; }
                    break;
                case 38: //KEY_UP:
                    this.moveUp();
                    break;
                case 40: //KEY_DOWN:
                    this.moveDown();
                    break;
                default:
                    return;
            }
            e.stopImmediatePropagation();
            e.preventDefault();
        },

        onKeyUp: function(e) {
            if(this.disabled){ return; }
            switch (e.keyCode) {
                case 38: //KEY_UP:
                case 40: //KEY_DOWN:
                case 9://tab
                case 16://shift+tab
                    return;
            }
            clearInterval(this.onChangeInterval);
            if (this.currentValue !== this.el.val()) {
                if (this.options.deferRequestBy > 0 ) {
                    // Defer lookup in case when value changes very quickly:
                    var me = this;
                    this.onChangeInterval = setInterval(function() { me.onValueChange(); }, this.options.deferRequestBy);
                } else {
                    this.onValueChange();
                }
            }
        },

        onValueChange: function(useEmptyQuery) {
            clearInterval(this.onChangeInterval);

            var me = this;
            if( this.isBusy ){
                this.onChangeInterval = setInterval(function() { me.onValueChange(); }, me.options.deferRequestBy);
                return;
            }

            this.currentValue = this.el.val();
            var q = this.getQuery(this.currentValue);
            this.selectedIndex = -1;
            if ((q === '' || q.length < this.options.minChars ) && !this.el.is('.suggest-combo-input')) {
                this.hide();
            } else {
                this.getSuggestions(useEmptyQuery ? null : q);
            }
        },

        getQuery: function(val) {
            var d, arr;
            d = this.options.delimiter;
            if (!d) { return $.trim(val); }
            arr = val.split(d);
            return $.trim(arr[arr.length - 1]);
        },

        getSuggestionsLocal: function(q) {
            var ret, arr, len, val, i;
            arr = this.options.lookup;
            len = arr.data.length;
            ret = { data:[] };
            q = q.toLowerCase();
            for(i=0; i< len; i++){
                val = arr.data[i];
                if(val.toLowerCase().indexOf(q) === 0){
                    ret.data.push(arr.data[i]);
                }
            }
            return ret;
        },

        getSuggestions: function(q) {
            var cr, me;

            me = this;
            me.options.params.query = q === null ? '' : q;

            for( var i in me.options.refFields )
            {
                var val = (me.el[0].parentForm && me.el[0].parentForm.fields[me.options.refFields[i].fieldName] && me.el[0].parentForm.fields[me.options.refFields[i].fieldName].getValue ) ?  me.el[0].parentForm.fields[me.options.refFields[i].fieldName].getValue() : undefined;

                me.options.params[i] = '';

                if( val != undefined ){
                    if(me.options.refFields[ i ].key){
                        val = val[ me.options.refFields[ i ].key ];
                    }
                    me.options.params[i] = val;
                }
            }

            this.gcLocalStorage();

            //cr = this.isLocal ? this.getSuggestionsLocal(q) : this.cachedResponse[ JSON.stringify(this.options.params) ];
            cr = this.isLocal ? this.getSuggestionsLocal(q) : JSON.parse( window.localStorage.getItem( this.cacheKeyPrefix + JSON.stringify(this.options.params) ) );

            if( cr && cr.timestamp )
            {
                var now = new Date().getTime();
                if( now - cr.timestamp > me.options.CacheLiveTime*1000)
                {
                    window.localStorage.removeItem( this.cacheKeyPrefix + JSON.stringify(this.options.params) );
                    cr = '';
                }
            }

            if (cr && $.isArray(cr.data)) {
                this.data = cr.data;
                this.suggest();
            } else {
                var canceled = me.el[0].callListeners('beforeQuery',[me.options.params]);
                for(var i=0; i < canceled.length; i++) {
                    if(canceled[i] === false) return false;
                }

                this.isBusy=true;
                this.el.addClass("ui-autocomplete-loading");
                $.post(this.serviceUrl, me.options.params, function(txt) { me.processResponse(txt,q); }, 'text');
            }
        },

        gcLocalStorage: function() {
            if(!window.localStorage || !window.localStorage.getItem) return;
            var expire = new Date().getTime() - this.options.CacheLiveTime*1000;
            if(localStorage.length > 1000) {
                localStorage.clear();
            } else {
                var lastClearTime = parseInt(localStorage.getItem(this.cacheKeyPrefix+'last_clear_time'),10) || 0;
                if(lastClearTime < (new Date().getTime()) - 600000) {
                    for(var k in localStorage) {
                        if(!Object.prototype.hasOwnProperty.call(localStorage,k)) continue;
                        if(k.indexOf(this.cacheKeyPrefix) === 0) {
                            try {
                                var ts = parseInt(JSON.parse(localStorage.getItem(k)).timestamp,10) || 0;
                                if(ts < expire) localStorage.removeItem(k);
                            } catch(e) {
                                localStorage.removeItem(k);
                            }
                        }
                    }
                    localStorage.setItem(this.cacheKeyPrefix+'last_clear_time',new Date().getTime());
                }
            }
        },

        isBadQuery: function(q) {
            var i = this.badQueries.length;
            while (i--) {
                if (q.indexOf(this.badQueries[i].query) === 0) { return true; }
            }
            return false;
        },

        hide: function() {
            this.enabled = false;
            this.selectedIndex = -1;
            this.container.hide();
            if(this.el.parent().parent().is('div.suggest-combo')) this.el.parent().parent().removeClass("suggest-show");
            else this.el.removeClass("suggest-show");
        },

        suggest: function() {
            if (this.data.length === 0 || !this.el.focused()) {
                this.hide();
                return;
            }

            var me, len, div, f, v, i, s, mOver, mClick;
            me = this;
            len = this.data.length;
            f = this.options.fnFormatResult;
            v = this.getQuery(this.currentValue);
            mOver = function(xi) { return function() { me.activate(xi); }; };
            mClick = function(xi) { return function() { me.select(xi); }; };
            this.container.hide().empty();
            for (i = 0; i < len; i++) {
                s = this.data[i]['html_value'];
                div = $((me.selectedIndex === i ? '<div class="selected"' : '<div') + ' >' + f(s, this.data[i], v) + '</div>');
                div.title = s;
                div.mouseover(mOver(i));
                div.click(mClick(i));
                this.container.append(div);
            }

            this.autoFill(this.currentValue, this.data[0]);

            this.enabled = true;
            this.container.show();
            if(this.el.parent().parent().is('div.suggest-combo')) this.el.parent().parent().addClass("suggest-show");
            else this.el.addClass("suggest-show");
            if(!this.el.focused()) this.el.focus();
        },

        autoFill: function (q, data){
            if( this.options.params.forceSelection != undefined )
            {
                var currentValue = this.el.val();
                if( (currentValue.toLowerCase() == q.toLowerCase()) && (data.value.toLowerCase().substr(0, q.length) == q.toLowerCase()) ) {
                    this.onSelect(0);
                    this.el.selection(q.length, data.value.length);
                }
            }
        },

        processResponse: function(text,q) {
            var response;
            this.isBusy = false;
            this.el.removeClass("ui-autocomplete-loading");
            try {
                response = eval('(' + text + ')');
            } catch (err) { return; }

            if(response.error){
                Server.defaultErrorFn(response.error.data);
                return;
            }
            if (!$.isArray(response.data)) { response.data = []; }
            if(!this.options.noCache){
                //this.cachedResponse[ JSON.stringify(this.options.params) ] = response;
                if (response.data.length === 0) { this.badQueries.push( JSON.stringify(this.options.params) ); }

                response.timestamp = new Date().getTime();
                try {
                    window.localStorage.setItem( this.cacheKeyPrefix + JSON.stringify(this.options.params), JSON.stringify(response));
                } catch (e) {
                    window.localStorage.clear();
                }
            }
            if (response.query === this.getQuery(this.currentValue) || this.el.hasClass("ui-autocomplete-loading") || q === null) {
                this.el.removeClass("ui-autocomplete-loading");
                this.data = response.data;
                this.suggest();
            }
        },

        activate: function(index) {
            var divs, activeItem;
            divs = this.container.children();
            // Clear previous selection:
            if (this.selectedIndex !== -1 && divs.length > this.selectedIndex) {
                $(divs.get(this.selectedIndex)).removeClass();
            }
            this.selectedIndex = index;
            if (this.selectedIndex !== -1 && divs.length > this.selectedIndex) {
                activeItem = divs.get(this.selectedIndex);
                $(activeItem).addClass('selected');
            }
            return activeItem;
        },

        deactivate: function(div, index) {
            div.className = '';
            if (this.selectedIndex === index) { this.selectedIndex = -1; }
        },

        select: function(i) {
            var selectedValue, f;
            selectedValue = this.data[i]['value'];
            if (selectedValue) {
                this.el.val(selectedValue);
                if (this.options.autoSubmit) {
                    f = this.el.parents('form');
                    if (f.length > 0) { f.get(0).submit(); }
                }
                this.currentValue = selectedValue;
                this.el.selection(selectedValue.length, selectedValue.length);
                this.hide();
                this.onSelect(i);
            }
        },

        moveUp: function() {
            if (this.selectedIndex === -1) { return; }
            if (this.selectedIndex === 0) {
                this.container.children().get(0).className = '';
                this.selectedIndex = -1;
                this.el.val(this.currentValue);
                return;
            }
            this.adjustScroll(this.selectedIndex - 1);
        },

        moveDown: function() {
            if (this.selectedIndex === (this.data.length - 1)) { return; }
            this.adjustScroll(this.selectedIndex + 1);
        },

        adjustScroll: function(i) {
            var activeItem, offsetTop, upperBound, lowerBound;
            activeItem = this.activate(i);
            offsetTop = activeItem.offsetTop;
            upperBound = this.container.scrollTop();
            lowerBound = upperBound + this.options.maxHeight - 25;
            if (offsetTop < upperBound) {
                this.container.scrollTop(offsetTop);
            } else if (offsetTop > lowerBound) {
                this.container.scrollTop(offsetTop - this.options.maxHeight + 25);
            }
            this.el.val(this.getValue(this.data[i]['value']));
        },

        onSelect: function(i) {
            var me, fn, s, d;
            me = this;
            fn = me.options.onSelect;
            s = me.data[i]['value'];
            d = me.data[i];

            var canceled = me.el[0].callListeners('suggestSelect',[d]);
            for(var i=0; i < canceled.length; i++) {
                if(canceled[i] === false) return false;
            }

            me.el.val(me.getValue(s));
            me.el[0].setValue(d);
            for( var i in me.options.onSuggest )
            {
                var field = me.el[0].parentForm.fields[ i ];
                var value = d[ me.options.onSuggest[ i ] ] != undefined ? d[ me.options.onSuggest[ i ] ] : '';

                field.setValue( value );
            }
            if ($.isFunction(fn)) { fn(s, d, me.el); }
        },

        getValue: function(value){
            var del, currVal, arr, me;
            me = this;
            del = me.options.delimiter;
            if (!del) { return value; }
            currVal = me.currentValue;
            arr = currVal.split(del);
            if (arr.length === 1) { return value; }
            return currVal.substr(0, currVal.length - arr[arr.length - 1].length) + value;
        }

    };

    $.fn.selection = function(start, end) {
        if (start !== undefined) {
            return this.each(function() {
                if( this.createTextRange ){
                    var selRange = this.createTextRange();
                    if (end === undefined || start == end) {
                        selRange.move("character", start);
                        selRange.select();
                    } else {
                        selRange.collapse(true);
                        selRange.moveStart("character", start);
                        selRange.moveEnd("character", end);
                        selRange.select();
                    }
                } else if( this.setSelectionRange ){
                    this.setSelectionRange(start, end);
                } else if( this.selectionStart ){
                    this.selectionStart = start;
                    this.selectionEnd = end;
                }
            });
        }
        var field = this[0];
        if ( field.createTextRange ) {
            var range = document.selection.createRange(),
                orig = field.value,
                teststring = "<->",
                textLength = range.text.length;
            range.text = teststring;
            var caretAt = field.value.indexOf(teststring);
            field.value = orig;
            this.selection(caretAt, caretAt + textLength);
            return {
                start: caretAt,
                end: caretAt + textLength
            }
        } else if( field.selectionStart !== undefined ){
            return {
                start: field.selectionStart,
                end: field.selectionEnd
            }
        }
    };

}(jQuery));
