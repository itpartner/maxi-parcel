//validation
framework.validationFn = {};
framework.validationFn.regex = function(value) {
	if (!this.__validationRegex) {
		try {
			this.__validationRegex = eval(this.getAttribute('validation'));
		} 
		catch (err) {
			this.__validationRegex = /zrqwerhquiwewrnweiuznrqwoienrzoiqwe/;
		}
	}
	if(this.__validationRegex.test(value)) return '';
	else return L('Невалидна стойност','default');
};

framework.validationFn.email = function(value){
	return '';
};
framework.validationFn.count = function(value){
	if(value && (isNaN(parseInt(value)) || !/^\d*$/.test(value))) {
		return L('Невалидна стойност','default')
	} else {
		return ''
	}
};
framework.validationFn.integer = function(value){
	if(value && (isNaN(parseInt(value)) || !/^-?\d*$/.test(value))) {
		return L('Невалидна стойност','default')
	} else {
		return ''
	}
};
framework.validationFn['float'] = function(value){
	if(value && isNaN(parseFloat(value))) {
		return L('Невалидна стойност','default')
	} else {
		return ''
	}
};
framework.validationFn.price = function(value){
	if(value && (isNaN(parseFloat(value)) || !/^-?\d*\.?\d*$/.test(value))) {
		return L('Невалидна стойност','default')
	} else {
		return ''
	}
};

//key restriction
framework.keyRestrictionFn = {};
framework.keyRestrictionFn.regex = function(event,ch) {
	if(event.ctrlKey || event.altKey || !ch) return true;
	if (!this.__keyRestrictionRegex) {
		try {
			this.__keyRestrictionRegex = eval(this.getAttribute('keyRestriction'));
		} 
		catch (err) {
			this.__keyRestrictionRegex = /zrqwerhquiwewrnweiuznrqwoienrzoiqwe/;
		}
	}
	return this.__keyRestrictionRegex.test(ch);
};

framework.keyRestrictionFn.email = function(event,ch){
	if(event.ctrlKey || event.altKey || !ch) return true;
	return true;
};
framework.keyRestrictionFn.count = function(event,ch){
	if(event.ctrlKey || event.altKey || !ch) return true;
	return /[\d]+/.test(ch);
};
framework.keyRestrictionFn.integer = function(event,ch){
	if(event.ctrlKey || event.altKey || !ch) return true;
	return /[-\d]+/.test(ch);
};
framework.keyRestrictionFn['float'] = function(event,ch){
	if(event.ctrlKey || event.altKey || !ch) return true;
	return /[-+e.\d]+/.test(ch);
};
framework.keyRestrictionFn.price = function(event,ch){
	if(event.ctrlKey || event.altKey || !ch) return true;
	return /[-.\d]+/.test(ch);
};
framework.keyRestrictionFn.time = function(event,ch){
	if(event.ctrlKey || event.altKey || !ch) return true;
	var value = event.currentTarget.value;
	if(value.length == 2) 
	{
		value = value.concat(":");
		event.currentTarget.value = value;
	}
	ch = value.concat(ch);		
	return /^($|([0-2]$|([01][0-9]|[2][0-3])))($|:)($|[0-5])($|[0-9])$/.test(ch);
};