framework = {};

//get i set value metodi za poletata
framework.attachMethods = function (el) {
    //panel frame
    jQuery(el).find('div.panel-frame').each(function (i, frame) {
        var jFrame = jQuery(frame);
        frame.addListener = framework.addListener;
        frame.removeListener = framework.removeListener;
        frame.callListeners = framework.callListeners;
        frame.addButtonShortcut = function (button, key, ctrl, alt, shift) {
            var jButton, hintClassArr = ['k'], eventType = [];
            if(typeof(button) == 'string') jButton = jFrame.find(button);
            else jButton = jQuery(button);

            if(ctrl) eventType.push('Ctrl');
            if(alt) eventType.push('Alt');
            if(shift) eventType.push('Shift');
            eventType.push(key);
            frame.addListener(eventType.join('+'), function () {
                jButton.trigger('click');
            });

            //hint
            if(ctrl) hintClassArr.push('ctrl');
            if(alt) eventType.push('alt');
            if(shift) eventType.push('shift');
            if(hintClassArr.length > 1) {
                jButton.append(jQuery('<div></div>').text(key).addClass(hintClassArr.join('-')).addClass('shortcut-hint'));
                if(jButton.css('position') == 'static') jButton.css('position', 'relative');
            }
        }
    });
    //panel frame collapse
    jQuery(el).find('div.panel-frame.collapsible').each(function (i, frame) {
        var jFrame = jQuery(frame);
        var collapseFn = function (event) {
            if(jFrame.hasClass('collapsed')) {
                var canceled = frame.callListeners('expand', []);
                for (var i = 0; i < canceled.length; i++) if(canceled[i] === false) return;
                jFrame.removeClass('collapsed');
                jFrame.find('ul.menu-button').each(function (i, menuBtn) {
                    menuBtn.fixWidth();
                });
            } else {
                var canceled = frame.callListeners('collapse', []);
                for (var i = 0; i < canceled.length; i++) if(canceled[i] === false) return;
                jFrame.addClass('collapsed');
            }

        };
        jFrame.find('div.panel-frame-top').dblclick(function (event) {
            if(event.target.nodeName.toLowerCase() == 'button') return;
            collapseFn();
        });
        jFrame.find('div.panel-frame-top button.collapse').click(collapseFn);
    });


    //panel resize
    jQuery(el).find('div.panel-frame.v-resizable').each(function (ii, panel) {
        var jPanel = jQuery(panel);
        var jGrids = jPanel.find('div.grid-container');
        var jBody = jPanel.find('> div.panel-frame-body');

        jPanel.find('> div.frame-v-resizer').draggable({
            cursor: 'n-resize',
            axis: 'y',
            start: function (event, ui) {
                ui.helper.addClass('resizing');
            },
            stop: function (event, ui) {
                var diff = ui.position.top - ui.originalPosition.top;
                if(jGrids.length) {
                    jGrids.each(function (i, grid) {
                        var newHeight = grid.getDataHeight(true) + diff / jGrids.length;
                        if(newHeight < 0) newHeight = 0;
                        grid.setDataHeight(newHeight);
                    });
                } else {
                    var newHeight = jBody.height() + diff;
                    if(newHeight < 0) newHeight = 0;
                    jBody.height(newHeight);
                }
                ui.helper.removeClass('resizing');
                ui.helper.css({left: 0, top: ''});
            },
            drag: function (event, ui) {
                ui.helper.css({left: 0});
            }
        });
        jPanel.find('> div.frame-v-resizer').dblclick(function () {
            if(jGrids.length) {
                jGrids.each(function (i, grid) {
                    grid.setDataHeight('');
                });
            } else {
                jBody.css({height: ''});
            }
        });

    });

    //tooltip
    jQuery(el).find('[tooltip]').basicToolTip({contentAttribute: 'tooltip'});
    //FIELDS
    jQuery(el).find('span[name]').each(function (i, spanEl) {
        spanEl.getValue = function () {
            return this.innerHTML;
        };
        spanEl.setValue = function (value) {
            this.innerHTML = value;
        }
    });
    jQuery(el).find('input').each(function (i, el) {
        if(!el.getAttribute('type') || ['radio', 'checkbox', 'file'].indexOf(el.getAttribute('type').toLowerCase()) == -1) {
            el.getValue = function () {
                return this.value;
            };
            el.setValue = function (value) {
                this.value = value;
            };
        } else if(el.type == 'checkbox') {
            el.getValue = function () {
                return !!this.checked;
            };
            el.setValue = function (value) {
                this.checked = !empty(value);
            };
        } else if(el.type == 'file') {
            $(el).bind('change', function(){
                for(var i=0; i < this.files.length; i++) {
                    if(this.files[i]['dataURL']) continue;
                    (function(input,file,loadedCount){
                        var fileReader = new FileReader;
                        fileReader.addEventListener('load', function(){
                            file['dataURL'] = fileReader.result;
                            checkLoaded();
                        });
                        fileReader.readAsDataURL(file);
                    }(el, this.files[i]));
                }

                var loadedCount = this.files.length;
                var checkLoaded = function() {
                    if(--loadedCount) return;
                    setTimeout(function () {
                        el.callListeners('change', [el.getValue()]);
                    }, 0);
                }
            });

            el.setValue = function (value) {
                return;
            };

            el.getValue = function () {
                var files = [];
                for(var i=0; i < this.files.length; i++) {
                    files.push({
                        name: this.files[i].name,
                        type: this.files[i].type,
                        dataURL: this.files[i].dataURL
                    });
                }
                return files;
            };
        }

        if(jQuery(el).hasClass('datePicker')) {
            framework.initDatePicker();
            jQuery(el).datepicker({
                onSelect: function () {
                    if(this._onChange) {
                        this._onChange()
                    }
                    this.setError();
                },
                beforeShowDay: function (date) {
                    if(this._disableDays) {
                        return this._disableDays(date)
                    }
                    return [true];
                },
                onClose: function (dateText, inst) {
                    if(this._onClose) {
                        this._onClose(dateText, inst);
                    }
                }
            });
            el.getValue = function () {
                var date = jQuery(this).datepicker('getDate');
                if(!date) {
                    return '';
                }
                return jQuery.datepicker.formatDate('yy-mm-dd', date);
            };
            el.setValue = function (value) {
                try {
                    jQuery(el).datepicker("setDate", jQuery.datepicker.parseDate('yy-mm-dd', value));
                } catch (err) {
                }
            }
        }

        if(jQuery(el).hasClass('dateTimePicker')) {
            framework.initDateTimePicker();
            jQuery(el).datetimepicker({
                onSelect: function () {
                    if(this._onChange) {
                        this._onChange()
                    }
                    this.setError();
                },
                beforeShowDay: function (date) {
                    if(this._disableDays) {
                        return this._disableDays(date)
                    }
                    return [true];
                },
                onClose: function (dateText, inst) {
                    if(this._onClose) {
                        this._onClose(dateText, inst);
                    }
                }
            });
            el.getValue = function () {
                var date = jQuery(this).datepicker('getDate');
                if(!date) {
                    return '';
                }
                return jQuery.datepicker.formatDate('yy-mm-dd hh:ii:ss', date);
            };
            el.setValue = function (value) {
                try {
                    jQuery(el).datepicker("setDate", jQuery.datepicker.parseDate('yy-mm-dd hh:ii:ss', value));
                } catch (err) {
                }
            }
        }

        if(!el.getAttribute('type') || ['radio', 'hidden', 'checkbox'].indexOf(el.getAttribute('type').toLowerCase()) == -1) {
            el._errMsg = '';
            el.setError = framework.elementMethods.setErrorInp;
            el.isValid = framework.elementMethods.isValid;
            jQuery(el).bind('blur.validation', framework.elementMethods.onInputBlurValidation);
            jQuery(el).bind('keypress.keyRestriction', framework.elementMethods.onInputKeyPressRestriction);
            jQuery(el).bind('keypress.clearErr', framework.elementMethods.onInputKeyPressClearErr);
        }

        jQuery(el).bind('keypress.checkEnter', framework.elementMethods.onInputKeyPressCheckEnter);
    });

    jQuery(el).find('textarea').each(function (i, el) {
        el.getValue = function () {
            return this.value;
        };
        el.setValue = function (value) {
            this.value = value;
        };
        el.setError = framework.elementMethods.setError;
        el.isValid = framework.elementMethods.isValid;
        jQuery(el).bind('blur.validation', framework.elementMethods.onInputBlurValidation);
        jQuery(el).bind('keypress.keyRestriction', framework.elementMethods.onInputKeyPressRestriction);
        jQuery(el).bind('keypress.clearErr', framework.elementMethods.onInputKeyPressClearErr);
//		jQuery(el).bind('keypress.checkEnter',framework.elementMethods.onInputKeyPressCheckEnter);
    });

    jQuery(el).find('select').each(function (i, el) {
        el._errMsg = '';
        el.setError = framework.elementMethods.setError;
        jQuery(el).bind('change.clearErr', framework.elementMethods.onInputKeyPressClearErr);
//		jQuery(el).bind('keypress.checkEnter',framework.elementMethods.onInputKeyPressCheckEnter);
//		jQuery(el).bind('click.clearErr',framework.elementMethods.onInputKeyPressClearErr);

        if(el.multiple) {
            el.getValue = function () {
                var value = [];
                jQuery(this).find("option:selected").each(function (i, el) {
                    value.push(el.value);
                });
                return value;
            };
            el.setValue = function (value) {
                value = value || [];
                if(!value.length) value = [];
                var valuesObj = {};
                for (var i = 0; i < value.length; i++) {
                    valuesObj[value[i]] = true;
                }
                jQuery(this).find("option").each(function (i, el) {
                    el.selected = valuesObj[el.value];
                });
            };
        } else {
            el.getValue = function () {
                return this.value;
            };
            el.setValue = function (value) {
                if(!value && !jQuery(this).find('option[value=]').length) return;//za IE
                var oldValue = this.value;//za IE
                this.value = value;
                this._lastSetValue = value;
                if(this.value != value) this.value = oldValue;//za IE
            };
        }
        el.getContent = function () {
            return this._content;
        };
        el.setContent = function (content) {
            content = content || [];
            if(!content.length) content = [];
            this._content = content;
            var oldVal = el._lastSetValue;
            this.innerHTML = '';
            var groups = {
                '__select_': jQuery(this)
            };
            for (var i = 0; i < content.length; i++) {
                var opt = document.createElement('option');
                opt.innerHTML = content[i][this.labelField] || '';
                if(typeof(content[i][this.valueField]) != 'undefined') opt.value = content[i][this.valueField];
                if(typeof(content[i][this.titleField]) != 'undefined') opt.setAttribute('title', content[i][this.titleField]);
                if(typeof(content[i][this.customField]) != 'undefined') jQuery(opt).attr('custom_field', content[i][this.customField]);
                if(this.groupField && content[i][this.groupField]) {
                    if(!groups[content[i][this.groupField]]) {
                        var optGroup = document.createElement('optgroup');
                        optGroup.setAttribute('label', content[i][this.groupField]);
                        groups[content[i][this.groupField]] = jQuery(optGroup);
                        groups['__select_'].append(optGroup);
                    }
                    groups[content[i][this.groupField]].append(opt);
                } else {
                    groups['__select_'].append(opt);
                }
            }

            if(content.length) this.value = content[0][this.valueField];//za IE

            this.setValue(oldVal);
        };
        el._lastSetValue = el.value;
        el.valueField = el.valueField || el.getAttribute('valueField') || 'id';
        el.labelField = el.labelField || el.getAttribute('labelField') || 'value';
        el.titleField = el.titleField || el.getAttribute('titleField') || 'title';
        el.customField = el.customField || el.getAttribute('customField') || 'custom_field';
        el.groupField = el.groupField || el.getAttribute('groupField') || '';
    });

    jQuery(el).find('ul.keyvaluepairs').each(function (i, ul) {
        var $ul = $(ul);
        var $optionsUl = $ul.find('ul');
        var options = [];
        var suffixes = {};
        $ul.find("span[name]").each(function(){
            options.push($(this).attr('name'));
            suffixes[$(this).attr('name')] = $(this).attr('suffix');
        });

        $ul.find("input").each(function(i,el){
            el.setError = framework.elementMethods.setError;
        });

        $ul.find('.add').click(function(){
            var value = {};
            try {
                $ul.find("input").each(function(i, el){
                    var k = $(this).closest('span').attr('name');
                    var v = $(this).val();
                    if(!v.length) {
                        el.setError("Моля, въведете стойност");
                        throw 'ball';
                    }
                    value[k] = v;
                });
                $ul.find("input").each(function(){$(this).val('');});
            } catch(err) {return;}

            if(this.editLi) {
                setLiValue(this.editLi, value);
                toggleEditAdd();
            } else {
                addValue(value);
            }
        });

        var toggleEditAdd = function(editLi) {
            var opt = options;
            if(editLi && editLi != $ul.find('.add')[0].editLi) {
                if(editLi) for(var i in opt) $ul.find('span[name="'+opt[i]+'"]').find('input')[0].setValue(editLi[0].values[opt[i]]);
                $ul.find('.add').find('div').attr('class', 'icon icon fa fa-pencil');
                $ul.find('.add')[0].editLi = editLi;
            } else {
                for(var i in opt) $ul.find('span[name="'+opt[i]+'"]').find('input')[0].setValue('');
                $ul.find('.add')[0].editLi = null;
                $ul.find('.add').find('div').attr('class', 'icon icon fa fa-plus');
            }
        }

        ul.getValue = function () {
            var value = [];
            $optionsUl.find('li').each(function(i,el){
                value.push(el.values);
            });
            return value;
        };

        var setLiValue = function(li, value) {
            var span = li.find('span')[0];
            span = span ? $(span) : $('<span></span>');

            li[0].values= value;

            var text = [];
            for(var i in options) text.push(value[options[i]] + suffixes[options[i]] || '');

            span.text(text.join(' - '));

            if(!li.find('span').length) li.append(span);

            return li;
        }

        var addValue = function(value) {
            var li = $('<li></li>');

            setLiValue(li, value);

            var btn = $('<button></button>').append('<div class="icon fa fa-times remove"></div>');

            btn.click(function(){
                $(this).closest('li').remove();
            });

            li.click(function(){
                toggleEditAdd(li);
            });

            li.append(btn);
            $optionsUl.append(li);
        };

        ul.setValue = function (value) {
            $optionsUl.find('li').remove();
            for(var k in value) addValue(value[k]);
        };
    });


    jQuery(el).find('ul.menu-button').each(function (i, ul) {
        var jUl = jQuery(ul);
        var jButtonIcon = jUl.find('li.btn button div.icon');
        ul._onclick = function () {
            return this.callListeners('click', [jQuery(this).find('li.btn button').attr('itemname')]);
        };
        ul.getValue = function () {
            return jQuery(this).find('li.btn button').attr('itemname');
        };
        ul.setValue = function (value, fromClickEvent) {
            if(typeof(value) == 'undefined') return;
            var jTR = jUl.find('tr[itemname=' + value + ']');
            if(jTR.length != 1 || jTR.is('.disabled')) return;

            //change event
            var results = this.callListeners('change', arguments);
            if(fromClickEvent) results = results.concat(ul.callListeners('clickChange', arguments));
            if(!fromClickEvent) results = results.concat(ul.callListeners('setValue', arguments));
            for (var i = 0; i < results.length; i++) if(results[i] === false) return;

            jUl.find('li.btn button').attr('itemname', jTR.attr('itemname'));
            jUl.find('li.btn button span').text(jTR.find('td.label-cell').text());
            if(jButtonIcon.length) {
                jButtonIcon[0].className = jTR.find('div.icon')[0].className;
            }
            if(fromClickEvent) {
                ul._onclick();
                ul.saveState();
            }
        };
        ul.setContent = function (content) {
            if(empty(content) || !content.length) return;

            var oldValue = ul.getValue();

            var bHasIcons = false;
            var newValue;
            for (var i = 0; i < content.length; i++) {

                if(content[i].name == oldValue) newValue = oldValue;
                if(content[i]['iconCls'])
                    bHasIcons = true;
            }
            newValue = newValue || content[0].name;
            var tbody = jQuery('<tbody>');

            sc: for (var i = 0; i < content.length; i++) {
                var aNextItem = content[i + 1];

                if(content[i]['removed'])    continue;

                var oRow = jQuery('<tr>');
                if(content[i]['name'] == "_") {
                    //ako e posledniq ili ako sledvashtiq pak e separator ne se printira
                    if(!aNextItem) return;
                    if(aNextItem['name'] == '_') continue;

                    oRow.append(jQuery('<td>').attr('colspan', bHasIcons ? 2 : 1).append(jQuery('<div>'))).addClass('separator');
                } else {
                    if(bHasIcons) {
                        oRow.append(jQuery('<td class="icon-cell">').append(jQuery('<div class="icon">').addClass(content[i]['iconCls'])));
                    }
                    oRow.append(jQuery('<td class="label-cell">').append(jQuery('<span>').text(content[i]['text'])));
                    oRow.attr('itemname', content[i]['name']);
                }
                if(!empty(content[i]['disabled'])) oRow.addClass('disabled');

                tbody.append(oRow);
            }
            jUl.find('table').html('').append(tbody);
            ul.setValue(newValue);
            ul._fixedWidth = false;
            ul.fixWidth();
        };
        ul.saveState = function () {
            if(!ul.parentForm) return;
            if(!jUl.attr('name')) return;
            ul.parentForm.saveState('menuButton-' + jUl.attr('name') + '.value', ul.getValue());
        };
        ul.loadState = function () {
            if(!ul.parentForm) return;
            if(!jUl.attr('name')) return;
            ul.setValue(ul.parentForm.getState('menuButton-' + jUl.attr('name') + '.value'));
        };
        ul.addListener = framework.addListener;
        ul.removeListener = framework.removeListener;
        ul.callListeners = framework.callListeners;
        ul.fixWidth = function () {
            jUl = jQuery(this);
            if(ul._fixedWidth) return;
            if(!jUl.is(':visible')) return;

            jUl.find('li.list').css({display: 'block', visibility: 'hidden'});
            jUl.find('li.list table').css({display: 'table', visibility: 'hidden'});

            jUl.find('li.btn button').width('');
            jUl.find('li.btn button').width(jUl.find('li.list table').width()+5);

            jUl.find('li.list').css({display:'',visibility:''});
            jUl.find('li.list table').css({display: '', visibility: ''});
            this._fixedWidth = true;
        };
        ul._fixedWidth = false;
        ul.fixWidth();
        jUl.click(function (event) {
            var jTarget = jQuery(event.target);
            if(jTarget.hasClass('arrow')) {
                if(!jUl.hasClass('expanded')) {
                    jUl.addClass('expanded');
                    jQuery(document.body).bind('click.menu-button', function () {
                        jUl.removeClass('expanded');
                        jQuery(document.body).unbind('click.menu-button');
                    });
                    event.stopPropagation();
                }
                return;
            }

            if(jTarget.is('button') || jTarget.parent().is('button')) {
                event.preventDefault();
                ul._onclick();
                return;
            }

            var tr = event.target;
            while (tr.nodeName.toLowerCase() != 'tr') {
                if(tr.nodeName.toLowerCase() == 'ul') {
                    return;
                }
                tr = tr.parentNode;
            }

            event.preventDefault();
            if(jQuery(tr).is('.separator') || jQuery(tr).is('.disabled')) {
                event.stopPropagation();
                return;
            }

            jUl.removeClass('expanded');
            ul.setValue(tr.getAttribute('itemname'), true);
        });
    });

    jQuery(el).find('.edited-info').each(function (i, span) {
        var jSpan = jQuery(span);
        span.getValue = function () {
            return this._value;
        };
        span.setValue = function (value) {
            var old = this._value;
            this._value = jQuery.extend({}, old, value);
            if(empty(this._value.created_user) && !empty(this._value.updated_user)) this._value.created_user = this._value.updated_user;
            if(empty(this._value.created_time) && !empty(this._value.updated_time)) this._value.created_time = this._value.updated_time;
            this._updateToolTips();
        };
        span._jCreatedIcon = jSpan.find('.icon-created-info');
        span._jUpdatedIcon = jSpan.find('.icon-updated-info');
        span._jClosedIcon = jSpan.find('.icon-closed-info');

        span._updateToolTips = function () {
            var createdToolTip = [];
            if(this._value.created_user) createdToolTip[createdToolTip.length] = this._value.created_user;
            try {
                if(this._value.created_time) createdToolTip[createdToolTip.length] = L('Време') + ': ' + jQuery.datepicker.formatDate(framework.getDateTimeFormat(), jQuery.datepicker.parseDate('yy-mm-dd hh:ii:ss', this._value.created_time));
            } catch (err) {
            }
            if(createdToolTip.length) span._jCreatedIcon.css('display', 'inline-block').attr({'tooltip': '<b>' + L('Създал') + '</b><br/>' + createdToolTip.join('<br/>')}).basicToolTip();
            else span._jCreatedIcon.css('display', 'none').attr({'tooltip': ''}).basicToolTip();

            var updatedToolTip = [];
            if(this._value.updated_user) updatedToolTip[updatedToolTip.length] = this._value.updated_user;
            try {
                if(this._value.updated_time) updatedToolTip[updatedToolTip.length] = L('Време') + ': ' + jQuery.datepicker.formatDate(framework.getDateTimeFormat(), jQuery.datepicker.parseDate('yy-mm-dd hh:ii:ss', this._value.updated_time));
            } catch (err) {
            }
            if(updatedToolTip.length) span._jUpdatedIcon.css('display', 'inline-block').attr({'tooltip': '<b>' + L('Редактирал') + '</b><br/>' + updatedToolTip.join('<br/>')}).basicToolTip();
            else span._jUpdatedIcon.css('display', 'none').attr({'tooltip': false}).basicToolTip();

            var closedToolTip = [];
            if(this._value.closed_user) closedToolTip[closedToolTip.length] = this._value.closed_user;
            try {
                if(this._value.closed_time) closedToolTip[closedToolTip.length] = L('Време') + ': ' + jQuery.datepicker.formatDate(framework.getDateTimeFormat(), jQuery.datepicker.parseDate('yy-mm-dd hh:ii:ss', this._value.closed_time));
            } catch (err) {
            }
            if(closedToolTip.length) {
                span._jClosedIcon.css({'display': 'inline-block'}).attr({'tooltip': '<b>' + L('Приключил') + '</b><br />' + closedToolTip.join('<br />')}).basicToolTip();
            }
            else {
                span._jClosedIcon.css({'display': 'none'}).attr({'tooltip': false}).basicToolTip();
            }
        };
        span.setValue({
            created_time: jSpan.attr('created_time'),
            created_user: jSpan.attr('created_user'),
            updated_time: jSpan.attr('updated_time'),
            updated_user: jSpan.attr('updated_user'),
            closed_time: jSpan.attr('closed_time'),
            closed_user: jSpan.attr('closed_user'),
        });
    });

    jQuery(el).find('div.split-button').each(function (i, div) {
        var jDiv = jQuery(div);
        jDiv.delegate('button', 'click', function () {
            var currentValue = jDiv[0].getValue();
            var val = this.getAttribute('name');
            if(val == currentValue) return;
            jButtons.removeClass('selected');
            jQuery(this).addClass('selected');
            setTimeout(function () {
                div.callListeners('change', [val]);
            }, 0);
        });
        var jButtons = jDiv.find('button');
        div.getValue = function () {
            var jSelectedBtn = jButtons.filter('.selected');
            if(jSelectedBtn.length) return jSelectedBtn.attr('name');
            else return null;
        };
        div.setValue = function (value) {
            jButtons.removeClass('selected');
            var res = div.callListeners('setValue', [value]);
            for (var i = 0; i < res.length; i++) if(res[i] === false) return;
            jButtons.each(function (i, btn) {
                if(btn.getAttribute('name') === value) {
                    jQuery(btn).addClass('selected');
                    return false;
                }
            });
            return value;
        };

        div.addListener = framework.addListener;
        div.removeListener = framework.removeListener;
        div.callListeners = framework.callListeners;
    });
    //FORM
    jQuery(el).find('div.form').each(function (i, el) {
        var findFields = function (el) {
            el = el || this;
            //field
            if(typeof(el.getValue) == 'function' && (el.name || el.getAttribute('name'))) {

                var name = el.name || el.getAttribute('name');
                this.fields[name] = el;
                this.fields[name].parentForm = this;
            }
            //radio
            else if(el.nodeName && el.nodeName == 'INPUT' && el.type && el.type == 'radio' && el.name) {
                //obekt koito simulira grupa ot radio butoni
                if(!this.fields[el.name]) {
                    this.fields[el.name] = {
                        radioButtons: {},
                        getValue: framework.elementMethods.radioGetValue,
                        setValue: framework.elementMethods.radioSetValue
                    }
                }
                if(el.value) this.fields[el.name].radioButtons[el.value] = el;
            }
            else if(el.childNodes && el.childNodes.length) {
                for (var i = 0; i < el.childNodes.length; i++) {
                    if(el.childNodes[i].nodeName && el.childNodes[i].nodeName == 'DIV' && jQuery(el.childNodes[i]).hasClass('form')) continue;
                    findFields.apply(this, [el.childNodes[i]]);
                }
            }
        };
        el.findFields = findFields;
        el.getFieldValues = function (fieldNamesToGet) {
            var values = {};
            for (var fieldName in this.fields) {
                values[fieldName] = this.fields[fieldName].getValue();
            }

            if(fieldNamesToGet) {
                var valuesToGet = {};
                for (var i = 0; i < fieldNamesToGet.length; i++) {
                    valuesToGet[fieldNamesToGet[i]] = values[fieldNamesToGet[i]];
                }
                return valuesToGet;
            } else {
                return values;
            }
        };
        el.fields = {};
        el.request = function (method, params, fields, successFn, context, errorFn, loadingMask, nowait, synchronous) {
            context = context || this;
            if(!this.serverClassName) {
                alert('serverClassName is empty');
                return false;
            }
            var requestParams = Util.apply({}, params, this.baseParams);
            this.findFields(this);
            requestParams.fields = this.getFieldValues(fields);

            //before event
            var canceled = this.callListeners('beforeRequest', [requestParams, method]);
            for (var i = 0; i < canceled.length; i++) {
                if(canceled[i] === false) return false;
            }

            var onError = function (error) {
                var canceled = false;
                if(error.type == 'MultilineTextPromptException') {
                    framework.prompt({'inputType':'textarea'}, error.message, function(r){
                        if(!r) return;
                        requestParams.fields[error.additional_data] = r;
                        submit.apply(this)
                    }, this);
                    canceled = true;
                    return;
                } else {
                    if(errorFn) {
                        if(errorFn.apply(context, [error]) === false) {
                            canceled = true;
                        }
                    }
                }
                if(!canceled) {
                    this.setError(error);
                }
            };
            var submit = function(){
                Server.call(
                    this.serverClassName + '.' + method,
                    [requestParams],
                    function (result) {
                        this.processResponse(result, method, requestParams, successFn, context);
                    },
                    this,
                    onError,
                    loadingMask == false ? false : true,
                    nowait,
                    synchronous
                );
            };
            submit.apply(this)
        };

        el.processResponse = function (result, method, requestParams, successFn, context) {
            //after event
            var canceled = this.callListeners('afterRequest', [result, method, requestParams]);
            for (var i = 0; i < canceled.length; i++) {
                if(canceled[i] === false) return false;
            }

            //alert
            if(result && result.alert) {
                result.alert.callBack = function () {
                    if(this.fields[result.alert.focusField]) {
                        jQuery(this.fields[result.alert.focusField]).focus();
                    }
                };
                result.alert.context = this;
                framework.messageBox(result.alert);
            }

            //set fields
            if(result && result.fields) {
                this.setFields(result.fields);
            }
            if(successFn) {
                successFn.apply(context, [result]);
            }
        };

        el.setFields = function (values) {
            if(values) {
                for (var fieldName in values) {
                    //value
                    if(!this.fields[fieldName]) continue;
                    if(values[fieldName].content) {
                        if(typeof(this.fields[fieldName].setContent) == 'function') {
                            this.fields[fieldName].setContent(values[fieldName].content);
                        }
                        this.fields[fieldName].content = values[fieldName].content;
                    }
                    for (var k in values[fieldName]) {
                        switch (k) {
                            case 'value' :
                                if(typeof(this.fields[fieldName].setValue) == 'function') {
                                    this.fields[fieldName].setValue(values[fieldName].value);
                                } else {
                                    this.fields[fieldName].value = value;
                                }
                                break;
                            case 'content' :
                                break;
                            case 'style' :
                                if(typeof(values[fieldName].style) == 'object') {
                                    for (var styleName in values[fieldName].style) {
                                        if(this.fields[fieldName].style[styleName] !== undefined) {
                                            this.fields[fieldName].style[styleName] = values[fieldName].style[styleName];
                                        }
                                    }
                                }
                                break;
                            case 'toolTip':
                                jQuery(this.fields[fieldName]).attr('tooltip', values[fieldName].toolTip).basicToolTip();
                                break;
                            case 'type':
                            case 'id':
                            case 'name':
                                break;
                            default:
                                if(typeof(this.fields[fieldName][k]) == 'function') {
                                } else if(typeof(this.fields[fieldName][k]) == 'undefined') {
                                    if(this.fields[fieldName]['radioButtons']) {
                                        $.each(this.fields[fieldName]['radioButtons'], function(){
                                            $(this).attr(k, values[fieldName][k]);
                                        });
                                    } else {
                                        jQuery(this.fields[fieldName]).attr(k, values[fieldName][k]);
                                    }
                                } else {
                                    this.fields[fieldName][k] = values[fieldName][k];
                                }
                        }
                    }
                }
            }
        };

        el.setError = function (error) {
            for (var fieldName in this.fields) {
                if(!this.fields.hasOwnProperty(fieldName) || !this.fields[fieldName] || typeof(this.fields[fieldName].setError) != 'function') continue;
                this.fields[fieldName].setError();
            }
            if(!error) return;
            if(typeof(error) == 'string') error = {message: error};

            if(error.type == 'EmptySession') {
                Server.onSessionExpire();
                return;
            }
            
            var that = this;
            var errorsByFields = {};
            var focusField = '';
            var getErrorsByFields = function (err) {
                var i;
                if(err.targetFields && err.targetFields.length) for (i = 0; i < err.targetFields.length; i++) {
                    if(!errorsByFields[err.targetFields[i]]) errorsByFields[err.targetFields[i]] = [];
                    var errCopy = jQuery.extend({}, err);
                    errCopy.inner = [];
                    errorsByFields[err.targetFields[i]].push(errCopy);
                }
                if(!focusField && err.focusField && that.fields[err.focusField]) focusField = err.focusField;
                if(err.inner && err.inner.length) for (i = 0; i < err.inner.length; i++) getErrorsByFields(err.inner[i]);
            };

            getErrorsByFields(error);

            for (var fieldName in errorsByFields) {
                if(errorsByFields[fieldName].length && this.fields[fieldName] && typeof(this.fields[fieldName].setError) == 'function') {
                    this.fields[fieldName].setError(Server.createErrorHTML(errorsByFields[fieldName]).wrap('<div></div>').parent().html());
                }
            }

            Server.defaultErrorFn.apply(this, [error, function (error) {
                if(focusField && this.fields[focusField]) {
                    jQuery(this.fields[focusField]).focus();
                }
                if(typeof(error.callBack) == 'function') error.callBack.apply(error.context || {}, [error]);
            }, this]);
        };

        el.attachLabels = function () {
            var jForm = jQuery(this);
            var form = this;
            jForm.find('label[fieldName]').each(function (l, label) {
                var fieldName = label.getAttribute('fieldName').split('.');
                var optionName = fieldName[1] || false;
                fieldName = fieldName[0];
                var field = form.fields[fieldName] || false;
                if(field && field.radioButtons && optionName && field.radioButtons[optionName]) {
                    field = field.radioButtons[optionName];
                }
                if(field) {
                    if(!field.id) {
                        field.id = 'auto-id-' + framework.generatedID++;
                    }
                    label.setAttribute('for', field.id);
                }
            });
        };

        el.addListener = framework.addListener;
        el.removeListener = framework.removeListener;
        el.callListeners = framework.callListeners;
        el.baseParams = {};
        el.findFields(el);
        el.attachLabels();

        el.serverClassName = el.serverClassName || el.getAttribute('serverClassName');
        //state
        el.stateName = el.stateName || el.getAttribute('statename');
        el._state = el._state || {};
        el.saveState = function (name, value) {
            el._state[name] = value;
            if(el.stateName) framework.statesToSave[el.stateName] = el._state;
        };
        el.getState = function (name) {
            return el._state[name];
        };
        el.applyFieldStates = function () {
            for (var i in el.fields) if(typeof(el.fields[i].loadState) == 'function') el.fields[i].loadState();
        };
        el.applyFieldStates();


        //GRID

        var jGrid = jQuery(el).find('div.grid-container').not(jQuery(el).find('div.form div.grid-container'));
        if(jGrid.length == 1 && !jGrid[0].__initialized) {
            var grid = jGrid[0];
            grid.__initialized = true;
            el.grid = grid;
            /** methods **/
            grid.setData = function (data, totals) {
                grid.callListeners("beforeSetData", [data]);
                data = data || [];

                grid._data = data;
                grid._totals = totals;
                var normalTbody = document.createElement('tbody');
                var lockedTbody = document.createElement('tbody');
                jQuery([lockedTbody, normalTbody]).addClass('gird-data-body');
                jHeaderTables.find('tbody').html('');
                if(data.length) {
                    for (var i = 0; i < data.length; i++) {
                        var normalRow = grid._createRow(null, data[i], null, false);
                        var lockedRow = grid._createRow(null, data[i], null, true);
                        if(!data[i]._row_class_name && i % 2) jQuery([normalRow, lockedRow]).addClass('odd');
                        if(data[i]._row_class_name) jQuery([normalRow, lockedRow]).addClass(data[i]._row_class_name);
                        normalTbody.appendChild(normalRow);
                        lockedTbody.appendChild(lockedRow);
                    }

                    var totalsNormalRow = grid._createTotalsRow(totals, false);
                    var totalsLockedRow = grid._createTotalsRow(totals, true);
                    if(totalsNormalRow && totalsLockedRow) {
                        jQuery(jHeaderTables[1]).find('tbody').append(totalsNormalRow);
                        jQuery(jHeaderTables[0]).find('tbody').append(totalsLockedRow);
                    }

                } else {
                    var emptyNormalRow = grid._createRow(null, null, null, false);
                    var emptyLockedRow = grid._createRow(null, null, null, true);
                    emptyNormalRow.className = 'empty-row';
                    emptyLockedRow.className = 'empty-row';
                    normalTbody.appendChild(emptyNormalRow);
                    lockedTbody.appendChild(emptyLockedRow);
                }


                grid._selectedRows = [];
                //uncheck check all box
                jGrid.find('table.grid-headers thead th.column-check_box_selection input').attr('checked', false);

                jGrid.find('.normal-columns table.grid-data').html('').append(jQuery(normalTbody));
                jGrid.find('.locked-columns table.grid-data').html('').append(jQuery(lockedTbody));

                jGrid.find('table.grid-data [basictip]').basicToolTip({
                    contentAttribute: 'basictip'
                });

                if(grid.initConfig.tplCfg && grid.initConfig.tplCfg.multiPageSelection) {
                    var selectedRowIndexes = [];
                    grid._data.forEach(function (dataRow, i) {
                        if(empty(dataRow['id'])) throw new Error('Empty ID in multi page selection.');
                        if(grid._multiPageSelectedRows[dataRow['id']]) selectedRowIndexes.push(i);
                    });
                    grid.setSelectedRowIndexes(selectedRowIndexes);
                    grid.updateMultiPageSelectionNotice();
                } else {
                    grid._selectedRowIndexes = [];
                    //uncheck check all box
                    jGrid.find('table.grid-headers thead th.column-check_box_selection input').attr('checked', false);
                }

//				if(grid.initConfig.tplCfg && !grid.initConfig.tplCfg.disableRowHeightSync) {
//					var jNormalRows = jQuery(normalTbody).find('> tr');
//					var jLockedRows = jQuery(lockedTbody).find('> tr');
//					for(var i=0; i < jNormalRows.length; i++) {
//						var nh = jQuery(jNormalRows[i]).height();
//						var lh = jQuery(jLockedRows[i]).height();
//						if(nh > lh) jQuery(jLockedRows[i]).height(nh);
//						else if(nh < lh) jQuery(jNormalRows[i]).height(lh);
//					}
//				}
                layout.autoGridHeight();
                jHeaderDiv.scrollLeft(jDataDiv.scrollLeft());
                grid.syncVScroll();
                grid.callListeners("afterSetData");
            };

            grid._createRow = function (row, rowData, columns, locked) {
                row = row || document.createElement('tr');
                columns = columns || grid.columns;
                for (var i = 0; i < columns.length; i++) {

                    if(columns[i].children && columns[i].children.length) {
                        grid._createRow(row, rowData, columns[i].children, locked);
                    } else {
                        if((!locked && columns[i].locked) || (locked && !columns[i].locked)) continue;
                        var td = document.createElement('td');
                        var clickFn = false;
                        if(typeof(grid.clickListeners) == 'object' && typeof(grid.clickListeners[columns[i].dataField]) == 'function') {
                            clickFn = grid.clickListeners[columns[i].dataField];
                        }
                        td.setAttribute('class', 'column-' + columns[i].dataField + ' ctype-' + columns[i].type + ' ' + columns[i].class);
                        td.dataField = columns[i].dataField;
                        var divche = document.createElement('div');
                        var content = '';
                        if(rowData && grid.itemRenderers && grid.itemRenderers[columns[i].dataField]) {
                            content = grid.itemRenderers[columns[i].dataField].apply(grid, [columns[i], rowData, clickFn]);
                        } else {
                            content = '';
                        }


                        if(typeof(content) == 'string') {
                            divche.innerHTML = content;
                        } else {
                            divche.appendChild(content);
                        }

                        td.appendChild(divche);
                        row.appendChild(td);
                    }
                }
                return row;
            };
            grid._createTotalsRow = function (totals, locked) {
                if(empty(totals)) return null;
                var row = grid._createRow(null, totals, null, locked);
                jQuery(row).find('td').each(function (i, td) {
                    if(['string', 'number'].indexOf(typeof(totals[td.dataField])) == -1) {
                        jQuery(td).html('').addClass('empty');
                    }
                });
                row.className = 'totals-row';
                return row;
            };
            grid.getCurrentData = function () {
                var data = [];
                for (var i = 0; i < grid._data.length; i++) {
                    data.push(Util.apply({}, grid._data[i]));
                }
                return data;
            };

            //params,fields,successFn,context,errorFn
            grid.loadData = function (params, fields, successFn, context, errorFn) {
                Array.prototype.unshift.call(arguments, 'loadData');
                el.request.apply(el, arguments);
            };
            el.addListener('beforeRequest', function (request) {
                request.sortFields = grid._sort;
                if(grid.pagingBar && !request.multiPageSelectAll) {
                    request.paging = grid.pagingBar.getPaging();
                }
                if(grid.getSelectedFilter) {
                    request.filter = {
                        id: grid.getSelectedFilter()
                    }
                }
                request.filterItems = grid.getQuickFilterItems();

                //ako se e smenil requesta izchistvame multipage selectiona
                //durt hack za da hvanem i parametrite ot ostanalite 'beforeRequest' listeneri
                setTimeout(function () {
                    if(request.multiPageSelectIgnoreParams) return;
                    var cmpRequest = {};
                    for (var i in request) {
                        if(!request.hasOwnProperty(i) || i == 'sortFields' || i == 'paging' || i == 'multiPageSelectAll') continue;
                        cmpRequest[i] = request[i];
                    }
                    var requestCmpJSON = JSON.stringify(cmpRequest);
                    if(requestCmpJSON != el._lastRequestCmp) grid.setMultiPageSelectedRows([]);
                    grid.updateMultiPageSelectionNotice();
                    el._lastRequestCmp = requestCmpJSON;
                });
            });

            el.addListener('afterRequest', function (result, method, request) {
                //multipage selection shemi
                if(request.multiPageSelectAll) {
                    grid.setMultiPageSelectedRows(result.gridData);
                    grid.updateMultiPageSelectionNotice();
                    return;
                }
                if(!result) return;
                if(result.gridData) {
                    var start = 0;
                    if(request.paging) start = intval(request.paging.start);
                    if(result.gridData.length) {
                        for (var i = 0; i < result.gridData.length; i++) result.gridData[i]['__row_index'] = start + i;
                    }
                    grid.setData(result.gridData, result.gridTotals);
                }
                if(result.paging && grid.pagingBar) {
                    grid.pagingBar.setPaging(result);
                }
                if(result.sortFields) {
                    grid._sort = result.sortFields || [];
                    grid.displaySort();
                }

                var totalRowsText;
                if(result.paging && result.paging.totalRows) {
                    totalRowsText = ' (' + result.paging.totalRows + ')';
                } else if(result.gridData) {
                    totalRowsText = ' (' + result.gridData.length + ')';
                }
                var jFrameTitleRowCount = jQuery('#' + grid.initConfig.containerPrefix + '-frame-container > div.panel-frame-top > div.panel-frame-title > span.title-row-count');
                if(jFrameTitleRowCount.length == 1) {
                    jFrameTitleRowCount.text(totalRowsText);
                } else {
                    if(!grid.jTopBarCount) {
                        grid.jTopBarCount = jQuery(' <span class="top-bar-count"></span>');
                        jGrid.find('> .grid-top-bar .top-bar-title').append(grid.jTopBarCount);
                    }
                    grid.jTopBarCount.text(totalRowsText);
                }

            });

            grid._selectedRowIndexes = [];
            grid._multiPageSelectedRows = {};
            grid.getSelectedRows = function () {
                var rows = [];
                if(grid.initConfig.tplCfg && grid.initConfig.tplCfg.multiPageSelection) {
                    for (var i in grid._multiPageSelectedRows) {
                        rows.push(Util.apply({}, grid._multiPageSelectedRows[i]));
                    }
                } else {
                    for (var i = 0; i < grid._selectedRowIndexes.length; i++) {
                        rows.push(Util.apply({}, grid._data[grid._selectedRowIndexes[i]]));
                    }
                }
                return rows;
            };
            grid.setMultiPageSelectedRows = function (rows) {
                if(!grid.initConfig.tplCfg || !grid.initConfig.tplCfg.multiPageSelection) return;
                grid._multiPageSelectedRows = {};
                grid.setSelectedRowIndexes([]);
                grid.addMultiPageSelectedRows(rows);
            };
            grid.addMultiPageSelectedRows = function (rows) {
                if(!grid.initConfig.tplCfg || !grid.initConfig.tplCfg.multiPageSelection) return;
                for (var i in rows) {
                    if(!rows.hasOwnProperty(i)) continue;
                    if(empty(rows[i]['id'])) throw new Error('Empty ID in multi page selection.');
                    grid._multiPageSelectedRows[rows[i]['id']] = rows[i];
                }
                var rowIndexes = [];
                grid._data.forEach(function (row, i) {
                    if(grid._multiPageSelectedRows[row.id]) rowIndexes.push(i);
                });
                grid.addSelectedRowIndexes(rowIndexes);
            };
            grid.getSelectedRowIndexes = function () {
                var selectedRows = [];
                for (var i = 0; i < grid._selectedRowIndexes.length; i++) {
                    selectedRows.push(grid._selectedRowIndexes[i]);
                }
                return selectedRows;
            };
            grid.addSelectedRowIndexes = function (rowIndexes) {
                var lockedRows = jGrid.find('.locked-columns table.grid-data > tbody > tr');
                var normalRows = jGrid.find('.normal-columns table.grid-data > tbody > tr');
                var tmpSelectedRowIndexes = {};
                grid._selectedRowIndexes.forEach(function (i) {
                    tmpSelectedRowIndexes[i] = true;
                });
                if(rowIndexes && rowIndexes.length) {
                    for (var i = 0; i < rowIndexes.length; i++) {
                        if(rowIndexes[i] >= 0 && rowIndexes[i] < normalRows.length) {
                            if(!tmpSelectedRowIndexes[rowIndexes[i]]) {
                                grid._selectedRowIndexes.push(rowIndexes[i]);
                                tmpSelectedRowIndexes[rowIndexes[i]] = true;
                                jQuery(normalRows[rowIndexes[i]]).addClass('selected');
                                jQuery(lockedRows[rowIndexes[i]]).addClass('selected');
                                if(grid.initConfig.tplCfg && grid.initConfig.tplCfg.selectionMode && grid.initConfig.tplCfg.selectionMode == 'checkbox') {
                                    jQuery([lockedRows[rowIndexes[i]], normalRows[rowIndexes[i]]]).find('td.ctype-checkBoxSelection input').attr('checked', true);
                                }
                            }
                        }
                    }
                }
                grid._setCheckAllBox();

                if(grid.initConfig.tplCfg && grid.initConfig.tplCfg.multiPageSelection) {
                    grid._selectedRowIndexes.forEach(function (i) {
                        if(!grid._data[i]) return;
                        var dataRow = grid._data[i];
                        if(empty(dataRow['id'])) throw new Error('Empty ID in multi page selection.');
                        grid._multiPageSelectedRows[dataRow['id']] = dataRow;
                    });
                }
            };
            grid.setSelectedRowIndexes = function (rowIndexes) {
                if(grid.initConfig.tplCfg && grid.initConfig.tplCfg.selectionMode && grid.initConfig.tplCfg.selectionMode == 'checkbox') {
                    jGrid.find('table.grid-data > tbody > tr > td.ctype-checkBoxSelection input').attr('checked', false);
                }

                jGrid.find('table.grid-data > tbody > tr.selected').removeClass('selected');

                if(grid.initConfig.tplCfg && grid.initConfig.tplCfg.multiPageSelection) {
                    grid._selectedRowIndexes.forEach(function (i) {
                        if(!grid._data[i]) return;
                        var dataRow = grid._data[i];
                        if(empty(dataRow['id'])) throw new Error('Empty ID in multi page selection.');
                        delete grid._multiPageSelectedRows[dataRow['id']];
                    });
                }
                grid._selectedRowIndexes = [];
                grid.addSelectedRowIndexes(rowIndexes);
            };

            grid._setCheckAllBox = function () {
                if(grid._data.length == grid._selectedRowIndexes.length) {
                    jGrid.find('thead th.ctype-checkBoxSelection input').attr('checked', true);
                } else {
                    jGrid.find('thead th.ctype-checkBoxSelection input').attr('checked', false);
                }
            };

            grid.displaySort = function () {
                jGrid = jGrid || jQuery(this);
                this._sort = this._sort || [];
                var n = 0;
                jGrid.find('thead th div.sort-img').removeClass('desc').removeClass('asc');
                jGrid.find('thead th span.sort-num').text('');
                for (var i = 0; i < this._sort.length; i++) {
                    var jTH = jGrid.find('thead th.column-' + this._sort[i].field);
                    if(!jTH.length) continue;
                    if(this._sort[i].dir == 'ASC') {
                        jTH.find('div.sort-img').addClass('asc');
                    } else {
                        jTH.find('div.sort-img').addClass('desc');
                    }
                    jTH.find('span.sort-num').text(++n);
                }
            };
            grid.setSort = function (newSort) {
                this._sort = [];
                if(newSort && newSort.length) {
                    for (var i = 0; i < newSort.length; i++) {
                        if(newSort[i].field) {
                            this._sort.push({
                                field: newSort[i].field,
                                dir: typeof(newSort[i].dir) == 'string' && newSort[i].dir.toUpperCase() == 'ASC' ? 'ASC' : 'DESC'
                            });
                        }
                    }
                }
                var sort = this.getSort();
                this.getForm().saveState('grid.sort', sort);
                this.loadData(null, null, function () {
                    grid.callListeners("afterSort");
                });
                return sort;
            };
            grid.getSort = function () {
                var sort = [];
                for (var i = 0; i < this._sort.length; i++) {
                    sort.push(jQuery.extend({}, this._sort[i]));
                }
                return sort;
            };

            grid.getColumnHeader = function (column) {
                if(typeof(column) == 'string') {
                    var jTH = jGrid.find('.normal-columns table.grid-headers thead th.column-' + column);
                    if(!jTH.length)
                        return false;
                    var th = jTH[0];
                } else {
                    if(column && column.nodeName && column.nodeName.toLowerCase() == 'th') {
                        th = column;
                    } else {
                        return false;
                    }
                }
                return th;
            };
            grid.setColumnWidth = function (column, width, dontSaveState) {
                width = parseInt(width);
                if(isNaN(width)) return false;
                if(width < 0) width = 0;
                var th = grid.getColumnHeader(column);
                if(!th) return false;

                if(th.columnCfg.sortable === false) {
                    if((width + framework.grid.headerUnsortableOffset) <= 0) {
                        width = 1 - framework.grid.headerUnsortableOffset;
                    }
                    th.columnCfg.headerStyle.width = (width + framework.grid.headerUnsortableOffset) + 'px';
                } else {
                    if(width + framework.grid.headerSortableOffset <= 0) {
                        width = 1 - framework.grid.headerSortableOffset;
                    }
                    th.columnCfg.headerStyle.width = (width + framework.grid.headerSortableOffset) + 'px';
                }
                th.columnCfg.dataStyle.width = width + 'px';
                th.columnCfg.currentWidth = width;
                if(!dontSaveState) grid.saveColumnState();
                return width;
            };
            grid.getColumnWidth = function (column) {
                var th = grid.getColumnHeader(column);
                if(!th) return false;
                return th.columnCfg.currentWidth || th.columnCfg.width;
            };
            grid.showColumn = function (column) {
                var th = grid.getColumnHeader(column);
                if(!th) return false;

                th.columnCfg.hidden = false;
                grid.saveColumnState();
                grid.createHeaders();
                grid.setData(grid._data, grid._totals);
            };
            grid.hideColumn = function (column) {
                var th = grid.getColumnHeader(column);
                if(!th) return false;

                th.columnCfg.hidden = true;
                grid.saveColumnState();
                grid.createHeaders();
                grid.setData(grid._data, grid._totals);
            };
            grid.getColumnsForJSON = function (columns) {
                columns = columns || grid.columns;
                var columnsRes = [];
                for (var i = 0; i < columns.length; i++) {
                    columnsRes[i] = {};
                    for (var k in columns[i]) {
                        if(!columns[i].hasOwnProperty(k)) continue;
                        if(k == 'children') {
                            if(columns[i]['children'].length) {
                                columnsRes[i]['children'] = grid.getColumnsForJSON(columns[i].children);
                            }
                        } else if(k == 'parentColumn') {
                        } else if(columns[i][k] && (
                                typeof(columns[i][k].cssText) != 'undefined' ||
                                typeof(columns[i][k].nodeName) != 'undefined'
                            )
                        ) {
                        } else {
                            columnsRes[i][k] = columns[i][k];
                        }
                    }
                }
                return columnsRes;
            };
            grid.getTotalRows = function () {
                if(this.pagingBar) {
                    return this.pagingBar._totalRows;
                } else {
                    return this._data.length;
                }
            };

            grid.getQuickFilterItems = function () {
                var jTHs = jGrid.find('thead th.filtered');
                var items = [];
                jTHs.each(function (i, th) {
                    items.push(th.quickFilter);
                });
                return items;
            };
            grid.setQuickFilters = function (filters, keepOld) {
                if(!keepOld) jGrid.find('thead th.filtered').each(function (i, th) {
                    grid.removeQuickFilter(th);
                });
                for (var column in filters) {
                    grid.setQuickFilter(column, filters[column]);
                }
            };
            grid.setQuickFilter = function (column, filter) {
                column = grid.getColumnHeader(column);
                if(!column || !column.columnCfg.filterable) return;
                column.quickFilter = filter;
                jQuery(column).addClass('filtered');
            };
            grid.removeQuickFilter = function (column) {
                column = grid.getColumnHeader(column);
                if(!column || !column.columnCfg.filterable) return;
                column.quickFilter = false;
                jQuery(column).removeClass('filtered');
            };
            grid.saveColumnState = function () {
                var state = {};
                grid.getColumnState(state);
                grid.getForm().saveState('grid.columns', state);
            };
            grid.getColumnState = function (state, columns) {
                state = state || {};
                columns = columns || grid.columns;
                for (var i = 0; i < columns.length; i++) {
                    state[columns[i].dataField] = {};
                    if(columns[i].children && columns[i].children.length) grid.getColumnState(state, columns[i].children);
                    else {
                        state[columns[i].dataField].hidden = columns[i].hidden;
                        state[columns[i].dataField].width = grid.getColumnWidth(columns[i].dataField);
                    }
                    state[columns[i].dataField].order = i;
                    state[columns[i].dataField].locked = columns[i].locked;
                }
            };

            grid.setDataHeight = function (height, dontSaveState) {
                height = parseInt(height, 10);
                if(!dontSaveState) grid.getForm().saveState('grid.dataHeight', height);
                if(isNaN(height)) height = '';
                else height = height + 'px';
                jGrid.find('div.grid-data').css({height: height});
            };
            grid.getDataHeight = function (bRealHeight) {
                if(bRealHeight) return jGrid.find('div.grid-data').height();
                var height = jGrid.find('div.grid-data')[0].style.height;
                return (height ? intval(height) : '');
            };

            grid.getColumnIndexInGroup = function (col) {
                var th = grid.getColumnHeader(col);
                if(!th) return false;
                var group = th.columnCfg.parentColumn ? th.columnCfg.parentColumn.children : grid.columns;
                for (var i = 0; i < group.length; i++) if(group[i] == th.columnCfg) return i;
                return false;
            };

            grid.moveColumn = function (col, index) {
                col = grid.getColumnHeader(col);
                var group = col.columnCfg.parentColumn ? col.columnCfg.parentColumn.children : grid.columns;
                if(index < 0) index = 0;
                if(index > group.length) index = group.length;

                var oldIndex = null;
                for (var i = 0; i < group.length; i++) if(group[i].dataField == col.columnCfg.dataField) oldIndex = i;
                if(oldIndex === null) {
                    console.log('wtf');
                    return;
                }
                if(oldIndex == index) return;
                var tmp = group.splice(oldIndex, 1);
                group.splice(index, 0, tmp[0]);
                grid.saveColumnState();
                grid.createHeaders();
                grid.setData(grid._data, grid._totals);
            };

            grid.lockColumn = function (col) {
                var th = grid.getColumnHeader(col);
                if(!th) return false;
                th.columnCfg.locked = true;
                grid.saveColumnState();
                grid.createHeaders();
                grid.setData(grid._data, grid._totals);
            };

            grid.unlockColumn = function (col) {
                var th = grid.getColumnHeader(col);
                if(!th) return false;
                th.columnCfg.locked = false;
                grid.saveColumnState();
                grid.createHeaders();
                grid.setData(grid._data, grid._totals);
            };

            /** init **/

            grid.clickListeners = {};
            grid.initConfig = jGrid.attr('initConfig');
            if(!grid.initConfig) debugger;
            grid.gridPrefix = grid.initConfig.gridPrefix;
            grid.itemRenderers = {};
            //main stucture

            grid.getForm = function () {
                return el;
            };

            //create headers
            grid.columns = grid.initConfig.gridColumns;

            var jHeaderTables = jGrid.find('table.grid-headers');
            jHeaderTables.each(function (i, table) {
                var thead = document.createElement('thead');
                table.appendChild(thead);
                table.locked = !i;
                for (var i = 0; i < grid.initConfig.headerRowCount; i++) {
                    thead.appendChild(document.createElement('tr'));
                }
                table.appendChild(document.createElement('tbody'));
            });

            if(grid.initConfig.tplCfg && grid.initConfig.tplCfg.selectionMode && grid.initConfig.tplCfg.selectionMode == 'checkbox') {
                grid.columns.unshift({
                    type: 'checkBoxSelection',
                    hideable: false,
                    dataField: 'check_box_selection',
                    headerText: '<input type="checkbox" class="check-all-box"/>',
                    headerToolTip: L('Маркирай/отмаркирай всички', 'default'),
                    columnListText: L('Маркиране'),
                    width: 20,
                    sortable: false,
                    resizable: false,
                    movable: false
                });
            }

            if(empty(grid.initConfig.hideRowIndexes) && !(grid.initConfig.tplCfg && grid.initConfig.tplCfg.hideRowIndexes)) {
                grid.columns.unshift({
                    type: 'rowIndex',
                    hideable: false,
                    dataField: '__row_index',
                    headerText: '#',
                    headerToolTip: L('Пореден номер', 'default'),
                    columnListText: L('Пореден #', 'default'),
                    width: 24,
                    sortable: false,
                    resizable: true,
                    movable: false
                });
            }

            //apply column state
            grid._applyColumnStateToConfig = function (state, columns) {
                state = state || {};
                var hasUnordered = false;
                for (var i = 0; i < columns.length; i++) {
                    if(columns[i].children && columns[i].children.length) grid._applyColumnStateToConfig(state, columns[i].children);
                    else {
                        if(state[columns[i].dataField]) {
                            columns[i].hidden = !!state[columns[i].dataField].hidden;
                            if(state[columns[i].dataField].width) columns[i].width = state[columns[i].dataField].width;
                        }
                    }
                    if(state[columns[i].dataField]) {
                        columns[i].order = state[columns[i].dataField].order;
                        columns[i].locked = state[columns[i].dataField].locked
                    }
                    if(typeof(columns[i].order) != 'number') hasUnordered = true;
                }


                if(!hasUnordered) columns.sort(function (c1, c2) {
                    return c1.order - c2.order;
                });
            };
            grid._applyColumnStateToConfig(grid.getForm().getState('grid.columns'), grid.columns);

            grid._createHeaders = function (gridColumns, headerRows, startCol, row, locked) {
                var col = 0;
                for (var i = 0; i < gridColumns.length; i++) {
                    var th = document.createElement('th');
                    var divche = document.createElement('div');
                    divche.setAttribute('class', 'title-div');
                    headerRows[row].appendChild(th);
                    gridColumns[i].thElement = th;
                    gridColumns[i].divElement = divche;
                    var span = document.createElement('span');
                    span.innerHTML = gridColumns[i].headerText;
                    if(!gridColumns[i].filterable || !framework.grid.filterRowRenderers[gridColumns[i].type]) {
                        if(gridColumns[i].hasOwnProperty('headerToolTip')) {
                            if(gridColumns[i].headerToolTip) {
                                th.setAttribute('tooltip', gridColumns[i].headerToolTip);
                            }
                        }
                        else {
                            th.setAttribute('tooltip', gridColumns[i].headerText);
                        }
                    }
                    jQuery(divche).append(span);

                    th.appendChild(divche);
                    th.columnCfg = gridColumns[i];
                    if(gridColumns[i].children && gridColumns[i].children.length) {
                        for (var j = 0; j < gridColumns[i].children.length; j++) {
                            gridColumns[i].children[j].parentColumn = gridColumns[i];
                        }
                        var childColCount = grid._createHeaders(gridColumns[i].children, headerRows, startCol + col, row + 1, locked);
                        th.setAttribute('colspan', childColCount);
                        if(childColCount === 0) {
                            th.style.display = 'none';
                        }
                        th.setAttribute('class', 'ctype-group');
                        col += childColCount;
                    } else {
                        if(framework.grid.itemRenderers[gridColumns[i].type]) {
                            grid.itemRenderers[gridColumns[i].dataField] = framework.grid.itemRenderers[gridColumns[i].type];
                        } else {
                            grid.itemRenderers[gridColumns[i].dataField] = framework.grid.itemRenderers['string'];
                        }

                        th.setAttribute('class', 'column-' + gridColumns[i].dataField + ' ctype-' + gridColumns[i].type);
                        gridColumns[i].headerStyle = Util.createStyleRule(grid.gridPrefix + '-styles', '#' + grid.gridPrefix + '-container th.column-' + gridColumns[i].dataField + ' > div');
                        gridColumns[i].dataStyle = Util.createStyleRule(grid.gridPrefix + '-styles', '#' + grid.gridPrefix + '-container td.column-' + gridColumns[i].dataField + ' > div');
                        gridColumns[i].columnStyle = Util.createStyleRule(grid.gridPrefix + '-styles', '#' + grid.gridPrefix + '-container .column-' + gridColumns[i].dataField);


                        th.setAttribute('rowspan', headerRows.length - row);

                        //sort
                        if(gridColumns[i].sortable !== false) {
                            var sortSeparator = document.createElement('div');
                            sortSeparator.setAttribute('class', 'sort-separator');
                            th.appendChild(sortSeparator);
                            var sortDiv = document.createElement('div');
                            sortDiv.setAttribute('class', 'sort-btn');
                            var sortNum = document.createElement('span');
                            sortNum.setAttribute('class', 'sort-num');
                            sortDiv.appendChild(sortNum);
                            var sortImg = document.createElement('div');
                            sortImg.setAttribute('class', 'sort-img');
                            sortDiv.appendChild(sortImg);
                            th.appendChild(sortDiv);

                            jQuery(th).addClass('sortable');
                        }
                        if(gridColumns[i].resizable !== false) {
                            jQuery(th).addClass('resizable');
                        }

                        if(gridColumns[i].filterable && framework.grid.filterRowRenderers[gridColumns[i].type]) {
                            jQuery(th).addClass('filterable');
                        }
                        var resizer = document.createElement('div');
                        resizer.setAttribute('class', 'column-resizer');
                        th.appendChild(resizer);

                        //width
                        grid.setColumnWidth(th, gridColumns[i].width, true);

                        //style
                        if(gridColumns[i].style && typeof(gridColumns[i] == 'object')) {
                            for (var property in gridColumns[i].style) {
                                gridColumns[i].dataStyle[property] = gridColumns[i].style[property];
                            }
                        }
                        //hidden ?
                        //if (gridColumns[i].hidden || (gridColumns[i].locked && !locked) || (!gridColumns[i].locked && locked) ) {
                        if(gridColumns[i].hidden || locked) {
                            th.style.display = 'none';
                        } else {
                            th.style.display = '';
                            th.parentNode.style.display = '';
                            col++;
                        }

                        gridColumns[i].columnStyle.display = gridColumns[i].hidden ? 'none' : '';
                    }

                }
                return col;
            };

            grid.createHeaders = function () {
                jHeaderTables.each(function (i, table) {
                    var thead = table.getElementsByTagName('thead')[0];
                    for (var i = 0; i < thead.childNodes.length; i++) thead.childNodes[i].innerHTML = '';
                    grid._createHeaders(grid.columns, thead.childNodes, 0, 0, table.locked);
                });

                setTimeout(function () {
                    grid._initDragResize()
                }, 0);
                grid.displaySort();

                //init filter
                jGrid.find('thead th.filterable .title-div').hover(
                    //in
                    function () {
                        if(grid.quickFilterVisible) return;
                        var th = this.parentNode;
                        jQuery(document).bind('mousemove.quickFilter', function (event) {
                            if(grid.quickFilterShowTimer != -1) clearTimeout(grid.quickFilterShowTimer);
                            grid.quickFilterShowTimer = setTimeout(function () {
                                framework.grid.showQuickFilter(th, grid);
                            }, 500);
                        });
                    },
                    //out
                    function () {
                        jQuery(document).unbind('mousemove.quickFilter');
                        if(grid.quickFilterShowTimer != -1) {
                            clearTimeout(grid.quickFilterShowTimer);
                        }
                    }
                );

                jHeaderTables.find('[tooltip]').basicToolTip();
            };


            grid.createHeaders();

            var jNormalColumnsDataDiv = jGrid.find('.normal-columns .grid-data');
            var jLockedColumnsDataDiv = jGrid.find('.locked-columns .grid-data');
            grid.syncVScroll = function () {
                jLockedColumnsDataDiv.scrollTop(jNormalColumnsDataDiv.scrollTop());
            };
            jNormalColumnsDataDiv.scroll(grid.syncVScroll);

            if(grid.initConfig.tplCfg && grid.initConfig.tplCfg.selectionMode && grid.initConfig.tplCfg.selectionMode == 'checkbox') {
                jGrid.delegate('thead th.ctype-checkBoxSelection input', 'change', function (event) {
                    if(event.target.checked) {
                        var selected = [];
                        for (var i = 0; i < grid._data.length; i++) {
                            selected.push(i);
                        }
                        grid.setSelectedRowIndexes(selected);
                    } else {
                        grid.setSelectedRowIndexes([]);
                    }
                    grid.updateMultiPageSelectionNotice();
                });
            }


            //quick filter
            grid.quickFilterShowTimer = -1;
            grid.quickFilterVisible = false;


            //create body
            jGrid.find('.normal-columns table.grid-data')[0].appendChild(document.createElement('tbody'));

            //scroll
            var jDataDiv = jGrid.find('.normal-columns div.grid-data');
            var stateHeight = grid.getForm().getState('grid.dataHeight');
            if(stateHeight === false || typeof(stateHeight) == 'string' || typeof(stateHeight) == 'number') {
                grid.setDataHeight(stateHeight, true);
            } else if(grid.initConfig.tplCfg && grid.initConfig.tplCfg.dataHeight) {
                grid.setDataHeight(grid.initConfig.tplCfg.dataHeight, true);
            }
            var jHeaderDiv = jGrid.find('div.grid-headers-outer');
            jDataDiv.scroll(function (event) {
                    jHeaderDiv.scrollLeft(jDataDiv.scrollLeft())
                }
            );
            grid._width_ratio = 1;

            grid.addListener = framework.addListener;
            grid.removeListener = framework.removeListener;
            grid.callListeners = framework.callListeners;

            //time period btn
            var jPeriodBtn = jGrid.find('ul.time-period');
            if(jPeriodBtn.length) {
                var showTimePeriodWindow = function () {
                    var jFilterDiv = jQuery('<div class="time-button-filter flat-buttons"><table><tbody></tbody></table></div>');
                    if(!jPeriodBtn[0]._range_filter_item) jPeriodBtn[0]._range_filter_item = {compareType: 'dbt'};
                    var jFilterTR = jQuery(framework.grid.filterRowRenderers['dateTime']({}, {headerText: '', dataField: '_some_fake_data_field'}, jPeriodBtn[0]._range_filter_item));

                    jFilterTR.prepend('<td class=""><div style="margin-right:7px;" class="icon fa fa-calendar"></td><td></div></td>');

                    jFilterTR.find('td.value-cell span.date2').css({marginLeft: '3px', marginRight: '3px'});
                    jFilterTR.append('<td class="confirm"><button><div class="icon fa fa-check"></div></button></td>');
                    jFilterTR.append('<td class="remove"><button><div class="icon fa fa-close"></div></button></td>');
                    jFilterDiv.find('tbody').append(jFilterTR);

                    jFilterDiv.find('td.remove button').click(function () {
                        wnd.close();
                    });
                    var onConfirm = function () {
                        jPeriodBtn[0]._range_filter_item = jFilterTR[0].getItemValue();
                        jPeriodBtn[0].setValue('range');
                        wnd.close();
                        grid.loadData();
                    };
                    jFilterDiv.find('td.confirm button').click(onConfirm);
                    jFilterDiv.find('td.value-cell').delegate('input', 'keypress', function (e) {
                        this.onPressEnterFn = onConfirm;
                        framework.elementMethods.onInputKeyPressCheckEnter.apply(this, arguments);
                    });
                    var wnd = framework.createWindow({
                        content: jFilterDiv,
//							parent:grid,
//							modal:false,
                        frame: false,
                        closable: false
                    }).window;
                    jFilterDiv.find('td.value-cell input:first').focus();
                };
                jPeriodBtn[0].addListener('clickChange', function (value) {
                    if(value == 'range') {
                        showTimePeriodWindow();
                        return false;
                    }
                });
                jPeriodBtn[0].addListener('click', function (value) {
                    if(value == 'range') {
                        showTimePeriodWindow();
                        return false;
                    } else {
                        grid.loadData();
                    }
                });
                jPeriodBtn[0]._org_getValue = jPeriodBtn[0].getValue;
                jPeriodBtn[0].getValue = function () {
                    var val = jPeriodBtn[0]._org_getValue();
                    if(val == 'range') return jPeriodBtn[0]._range_filter_item;
                    else return val;
                }
            }


            //export
            var jExportBtn = jGrid.find('ul.export-button');
            if(jExportBtn.length == 1) {
                jExportBtn[0].addListener('click', function (type) {
                    var request = jQuery.extend({}, el.baseParams);
                    request.fields = el.getFieldValues();
                    request._isExport = true;
                    var canceled = el.callListeners('beforeRequest', [request, 'loadData']);
                    for (var i = 0; i < canceled.length; i++) {
                        if(canceled[i] === false) return false;
                    }

                    request.serverClassName = el.serverClassName;
                    request.columns = grid.getColumnsForJSON();
                    request.gridTitle = jQuery(grid).attr('gridTitle');
                    framework.doExport('exportGridData', type, request);
                });
            }
            //filters
            var jFiltersBtn = jGrid.find('ul.filters-button');
            if(jFiltersBtn.length == 1) {
                var openFilterWindow = function (id) {
                    var gridTitle = jQuery('#' + grid.initConfig.containerPrefix + '-frame-container > div.panel-frame-top > div.panel-frame-title > span.title').text();
                    if(gridTitle) {
                        gridTitle = ' (' + gridTitle + ')';
                    }
                    if(grid._openFilterWindow) {
                        grid._openFilterWindow.close();
                    }
                    framework.createWindow({
                        title: L('Редакция на филтър', 'default') + gridTitle,
                        tplName: 'main/grid/GridFilterWindow',
                        parent: grid,
                        collapsible: true,
                        modal: false,
                        baseParams: {
                            id: id,
                            getGrid: function () {
                                return grid;
                            },
                            gridName: jGrid.attr('gridname')
                        }
                    });
                };
                grid.setFilters = function (filters) {
                    if(filters) grid._filters = filters;
                    else grid._filters = [];
                    var tbody = document.createElement('tbody');
                    for (var i = 0; i < grid._filters.length; i++) {
                        var row = document.createElement('tr');
                        var nameCell = document.createElement('td');
                        nameCell.className = 'filter-name';
                        row.setAttribute('id_filter', grid._filters[i].id);
                        jQuery(nameCell).text(grid._filters[i].filter_name);
                        row.appendChild(nameCell);
                        var editCell = document.createElement('td');
                        editCell.className = 'filter-edit';
                        var icon = document.createElement('i');
                        icon.className = 'icon fa fa-pencil';
                        editCell.appendChild(icon);
                        row.appendChild(editCell);

                        var deleteCell = document.createElement('td');
                        deleteCell.className = 'filter-delete';
                        var icon = document.createElement('i');
                        icon.className = 'icon fa fa-trash-o';
                        deleteCell.appendChild(icon);
                        row.appendChild(deleteCell);
                        tbody.appendChild(row);
                    }
                    jFiltersBtn.find('li.list table').html('').append(tbody);
                };
                grid.setFilters(grid.initConfig.filters);

                grid.getSelectedFilter = function () {
                    if(jFiltersBtn.hasClass('pressed')) {
                        return intval(jFiltersBtn.find('button.filter-btn').attr('id_filter'));
                    } else {
                        return 0;
                    }
                };
                grid.setSelectedFilter = function (filterID, forceReload, dontSaveState) {
                    filterID = intval(filterID);
                    if(grid.getSelectedFilter() == filterID) {
                        if(forceReload) grid.loadData();
                        return;
                    }
                    if(!filterID) {
                        jFiltersBtn.removeClass('pressed');
                    } else {
                        var jTR = jFiltersBtn.find('tr[id_filter=' + filterID + ']');
                        if(jTR.length === 1) {
                            jFiltersBtn.find('button.filter-btn').attr('id_filter', filterID);
                            jFiltersBtn.find('button.filter-btn span').text(jTR.find('td.filter-name').text());
                            jFiltersBtn.addClass('pressed');
                        } else {

                            jFiltersBtn.removeClass('pressed');
                        }
                    }
                    if(!dontSaveState) grid.getForm().saveState('grid.filter.selectedFilter', filterID);
                    if(!dontSaveState || forceReload) grid.loadData();
                };

                grid.setSelectedFilter(grid.getForm().getState('grid.filter.selectedFilter'), false, true);

                jFiltersBtn.click(function (event) {
                    var jTarget = jQuery(event.target);
                    if(jTarget.is('li.btn') || jTarget.is('span.arrow')) {
                        if(!jFiltersBtn.hasClass('expanded')) {
                            var maxHeight = jGrid.height() - 100;
                            maxHeight = maxHeight > 120 ? maxHeight : 120;
                            jFiltersBtn.find('li.list div.list-inner').css('max-height', maxHeight + 'px');
                            jFiltersBtn.addClass('expanded');
                            jQuery(document.body).bind('click.menu-button', function () {
                                jFiltersBtn.removeClass('expanded');
                                jQuery(document.body).unbind('click.menu-button');
                            });
                            event.stopPropagation();
                        }
                    } else if(jTarget.is('button.filter-btn') || jTarget.parent().is('button.filter-btn')) {
                        if(jFiltersBtn.hasClass('pressed')) {
                            grid.setSelectedFilter(0);
                        } else {
                            grid.setSelectedFilter(jFiltersBtn.find('button.filter-btn').attr('id_filter'));
                        }
                    } else {
                        if(jTarget.is('button.new-filter') || jTarget.parent().is('button.new-filter')) {
                            openFilterWindow(0);
                            jFiltersBtn.removeClass('expanded');
                            return;
                        }
                        var jTmpNode = jTarget, jTR = false, jTD = false;
                        while (!jTmpNode.is('ul')) {
                            if(jTmpNode.is('td')) {
                                jTD = jTmpNode;
                            } else if(jTmpNode.is('tr')) {
                                jTR = jTmpNode;
                            }
                            jTmpNode = jTmpNode.parent();
                        }
                        if(jTD && jTD.is('td.filter-edit')) {
                            openFilterWindow(jTR.attr('id_filter'));
                            jFiltersBtn.removeClass('expanded');
                        } else if(jTD && jTD.is('td.filter-name')) {
                            grid.setSelectedFilter(jTR.attr('id_filter'));
                            jFiltersBtn.removeClass('expanded');
                        } else if(jTD && jTD.is('td.filter-delete')) {
                            var x = function () {
                                var delID = jTR.attr('id_filter');
                                Server.call('main/grid/GridFilterWindow.del', [delID], function (res) {
                                        grid.setFilters(res);
                                        if(delID == intval(jFiltersBtn.find('button.filter-btn').attr('id_filter'))) {
                                            grid.setSelectedFilter();
                                            jFiltersBtn.removeClass('pressed');
                                            jFiltersBtn.find('button.filter-btn').attr('id_filter', 0);
                                            jFiltersBtn.find('button.filter-btn span').text(L('Избери филтър', 'default'));
                                        }
                                    }, null, null, true
                                );
                            };
                            x();
                        }
                    }
                });
            } else {
                grid.setSelectedFilter = function () {
                };
                grid.getSelectedFilter = function () {
                    return 0
                };
                grid.setFilters = function () {
                };
            }

            //column show / hide
            var jColumnsBtn = jGrid.find('ul.columns-button');
            if(jColumnsBtn.length == 1 && jGrid[0].columns.length) {
                var jColumnBtnRows;
                jColumnsBtn.addClass('with-hover');
                var createColumnRows = function (columns, tbody, depth) {
                    for (var i = 0; i < columns.length; i++) {
                        var row = document.createElement('tr');
                        row.className = 'columns-btn-row';
                        row.dataField = columns[i].dataField;
                        row.gridColumn = columns[i];
                        var check, cell, checkCell, text;
                        tbody.appendChild(row);
                        if(columns[i].children && columns[i].children.length) {
                            cell = document.createElement('td');
                            cell.style.paddingLeft = (depth * 15 + 10) + 'px';
                            cell.style.fontWeight = 'bold';
                            text = document.createElement('span');
//							text.style.fontStyle = 'italic';
                            var theText = Util.trim(columns[i].headerText);
                            if(!theText) theText = Util.trim(columns[i].headerToolTip);
                            if(columns[i].columnListText) theText = columns[i].columnListText;
                            jQuery(text).html(theText);
                            cell.appendChild(text);
                            row.appendChild(cell);
                            //row.appendChild(document.createElement('td'));
                            createColumnRows(columns[i].children, tbody, depth + 1);
                        } else {
                            cell = document.createElement('td');
                            if(columns[i].hideable !== false) {
                                check = document.createElement('input');
                                check.setAttribute('type', 'checkbox');
                                check.dataField = columns[i].dataField;
                                check.checked = !columns[i].hidden;
                                cell.appendChild(check);
                            }
                            cell.style.paddingLeft = 5 + (depth * 15) + 'px';
                            text = document.createElement('span');
                            var theText = Util.trim(columns[i].headerText);
                            if(!theText) theText = Util.trim(columns[i].headerToolTip);
                            if(columns[i].columnListText) theText = columns[i].columnListText;
                            jQuery(text).html(theText);
                            cell.appendChild(text);
                            row.appendChild(cell);
//							var jLockCell = jQuery('<td></td>').addClass('column-lock').append(jQuery('<div class=icon></div>'));
//							if(columns[i].locked) jLockCell.addClass('locked');
//							jQuery(row).append(jLockCell)
                        }
                    }
                };
                jColumnsBtn.delegate('.list-inner  td', 'mousedown', function (downEvent) {
                    if(downEvent.button !== 0) return;//ako ne e lqv buton...
                    jColumnsBtn.removeClass('with-hover');
                    var jOrigin = jQuery(this).parent();
                    var moveIndex = null;
                    jOrigin.addClass('drag-origin');
                    jQuery(document.body).bind('mouseup.columnBtnDrag', function () {
                        jQuery(document.body).unbind('mouseup.columnBtnDrag');
                        jQuery(document.body).unbind('mousemove.columnBtnDrag');
                        jColumnBtnRows.removeClass('drop-after drop-before');
                        jOrigin.removeClass('drag-origin');
                        jColumnsBtn.addClass('with-hover');
                        if(moveIndex !== null) {
                            grid.moveColumn(jOrigin[0].gridColumn.thElement, moveIndex);
                            loadGridColumns();
                        }
                    });
                    jQuery(document.body).bind('mousemove.columnBtnDrag', function (e) {

                        var jTR = jQuery(e.target).parents('tr.columns-btn-row');
                        if(!jTR.length || jTR[0].gridColumn.parentColumn != jOrigin[0].gridColumn.parentColumn) {
                            moveIndex = null;
                            jColumnBtnRows.removeClass('drop-after drop-before');
                            return;
                        }

                        if(!jTR.hasClass('drop-after') && !jTR.hasClass('drop-before')) {
                            jColumnBtnRows.removeClass('drop-after drop-before');
                        }
                        if(e.layerY > (jTR.height() / 2)) {
                            if(jTR[0].gridColumn.type == 'group') return;
                            if(!jTR.hasClass('drop-after')) jTR.removeClass('drop-before').addClass('drop-after');
                            moveIndex = grid.getColumnIndexInGroup(jTR[0].gridColumn.thElement) + 1;
                            if(jTR[0] == jOrigin[0]) moveIndex -= 1;
                        } else {
                            if(!jTR.hasClass('drop-before')) jTR.removeClass('drop-after').addClass('drop-before');
                            moveIndex = grid.getColumnIndexInGroup(jTR[0].gridColumn.thElement);
                        }
                        if(grid.getColumnIndexInGroup(jTR[0].gridColumn.thElement) > grid.getColumnIndexInGroup(jOrigin[0].gridColumn.thElement)) moveIndex -= 1;

                    });
                });
                var loadGridColumns = function () {
                    var tbody = document.createElement('tbody');
                    createColumnRows(jGrid[0].columns, tbody, 0);
                    jColumnsBtn.find('li.list table').html('').append(tbody);
                    jColumnBtnRows = jColumnsBtn.find('div.list-container tr');
                };
                jColumnsBtn.click(function (event) {
                    var jTarget = jQuery(event.target);
                    if(jTarget.is('li.btn') || jTarget.is('button') || jTarget.is('span.arrow')) {
                        if(!jColumnsBtn.hasClass('expanded')) {
                            loadGridColumns();
                            var maxHeight = jGrid.height() - 100;
                            maxHeight = maxHeight > 120 ? maxHeight : 120;
                            jColumnsBtn.find('li.list div.list-inner').css({'max-height': maxHeight + 'px', 'overflow-y' : 'auto'});
                            jColumnsBtn.addClass('expanded');
                            jQuery(document.body).bind('click.menu-button', function () {
                                jColumnsBtn.removeClass('expanded');
                                jQuery(document.body).unbind('click.menu-button');
                            });
                            event.stopPropagation();
                            return;
                        }
                    }

                    if(jTarget.is('td.column-lock') || jTarget.parent().is('td.column-lock')) {
                        if(jTarget.parent().is('td.column-lock')) jTarget = jTarget.parent();
                        jTarget.toggleClass('locked');
                        if(jTarget.hasClass('locked')) grid.lockColumn(jTarget.parent()[0].dataField);
                        else grid.unlockColumn(jTarget.parent()[0].dataField);
                        event.stopPropagation();
                    } else if(jTarget.parent().is('td') || jTarget.parent().is('tr')) {
                        if(jTarget.is('input:checkbox')) {
//							var jCheck = jTarget.parent().find('input:checkbox');
                            var jCheck = jTarget;
                            if(jCheck.length && jCheck[0].dataField) {
                                if(!jTarget.is('input:checkbox')) jCheck.attr('checked', !jCheck.attr('checked'));
                                if(jCheck.attr('checked')) {
                                    jGrid[0].showColumn(jCheck[0].dataField);
                                } else {
                                    jGrid[0].hideColumn(jCheck[0].dataField);
                                }
                            }
                        }
                        event.stopPropagation();
                    }
                });
            }
            //column resize
            jGrid.delegate('table.grid-headers thead th.resizable div.column-resizer', 'dblclick', function (event) {
                var jTH = jQuery(event.target.parentNode);
                jTH[0].columnCfg.width = grid.setColumnWidth(
                    jTH[0],
                    0
                );
                var maxWidth = 0;
                if(jTH.hasClass('sortable')) {
                    if(maxWidth < jTH.find('div.title-div')[0].scrollWidth - framework.grid.headerSortableOffset) {
                        maxWidth = jTH.find('div.title-div')[0].scrollWidth - framework.grid.headerSortableOffset;
                    }
                } else {
                    if(maxWidth < jTH.find('div.title-div')[0].scrollWidth - framework.grid.headerUnsortableOffset) {
                        maxWidth = jTH.find('div.title-div')[0].scrollWidth - framework.grid.headerUnsortableOffset;
                    }
                }
                jGrid.find('td.column-' + jTH[0].columnCfg.dataField + ' > div').each(function (i, dataDiv) {
                    if(maxWidth < dataDiv.scrollWidth) {
                        maxWidth = dataDiv.scrollWidth;
                    }
                });
                jTH[0].columnCfg.width = grid.setColumnWidth(
                    jTH[0],
                    maxWidth
                );
                jHeaderDiv.scrollLeft(jDataDiv.scrollLeft());
            });
            grid._jColumnResizeLine = jGrid.find('div.grid-data > div.resizer-line');
            grid._initDragResize = function () {
                jGrid.find('table.grid-headers thead th.resizable div.column-resizer').draggable({
                    cursor: 'e-resize',
                    start: function (event, ui) {
                        grid._jColumnResizeLine.offset({
                            left: ui.offset.left + 2,
                            top: grid._jColumnResizeLine.offset().top
                        });

                        grid._jColumnResizeLine.height(grid._jColumnResizeLine.parent().find('table.grid-data').height());
                        grid._jColumnResizeLine.css('display', 'block');
                    },
                    stop: function (event, ui) {
                        ui.helper[0].parentNode.columnCfg.width = grid.setColumnWidth(
                            ui.helper[0].parentNode,
                            ui.helper[0].parentNode.columnCfg.width + ui.position.left
                        );
                        ui.helper[0].style.top = 0;
                        ui.helper[0].style.left = 0;
                        grid._jColumnResizeLine.css('display', 'none');
                        jHeaderDiv.scrollLeft(jDataDiv.scrollLeft());
                    },
                    drag: function (event, ui) {
                        grid._jColumnResizeLine.offset({
                            left: ui.offset.left + 2,
                            top: grid._jColumnResizeLine.offset().top
                        });
                    }
                });
            };

            //sort
            grid._sort = grid.getForm().getState('grid.sort');
            if(!grid._sort || !grid._sort.length) grid._sort = [];

            jGrid.delegate('table.grid-headers thead th.sortable > div.title-div', 'click', function (event) {
//			jGrid.find('table.grid-headers thead th.sortable > div.title-div').click(function(event){
                var th = event.target;
                while (1) {
                    if(th.nodeName.toLowerCase() == 'th') {
                        break;
                    }
                    th = th.parentNode;
                }
                var jTH = jQuery(th);
                var thClassNames = th.className.split(' ');
                var dataField = '';
                for (var i = 0; i < thClassNames.length; i++) {
                    if(thClassNames[i].indexOf('column-') == 0) {
                        dataField = thClassNames[i].substr(7);
                        break;
                    }
                }
                if(dataField) {
                    if(th.__originalSort) {
                        if(th.__originalSort.cls == 'asc') {
                            grid.setSort([{field: dataField, dir: 'DESC'}]);
                        }
                        else {
                            grid.setSort([{field: dataField, dir: 'ASC'}]);
                        }
                    } else {
                        grid.setSort([{field: dataField, dir: jTH.find('div.sort-img').hasClass('asc') ? 'DESC' : 'ASC'}]);
                    }


                }
                event.stopPropagation();
            });
            jGrid.delegate('table.grid-headers thead th.sortable div.sort-btn', 'click', function (event) {
//			jGrid.find('table.grid-headers thead th.sortable div.sort-btn').click(function(event){
                var th = event.target;
                while (1) {
                    if(th.nodeName.toLowerCase() == 'th') {
                        break;
                    }
                    th = th.parentNode;
                }
                var jTH = jQuery(th);
                var thClassNames = th.className.split(' ');
                var dataField = '';
                for (var i = 0; i < thClassNames.length; i++) {
                    if(thClassNames[i].indexOf('column-') == 0) {
                        dataField = thClassNames[i].substr(7);
                        break;
                    }
                }
                var sDir = 'ASC';
                if(th.__originalSort) {
                    if(th.__originalSort.cls == 'asc') {
                        sDir = 'DESC';
                    }
                } else {
                    if(jTH.find('div.sort-img').hasClass('asc')) sDir = 'DESC';
                }

                var sort = grid.getSort();
                var found = false;
                for (var i = 0; i < sort.length; i++) {
                    if(sort[i].field == dataField) {
                        found = true;
                        sort[i].dir = sDir;
                    }
                }
                if(!found) {
                    sort.push({field: dataField, dir: sDir});
                }
                grid.setSort(sort);
                event.stopPropagation();
            });


            //create paging bar
            var jPagingBar = jGrid.find('div.grid-bottom-bar div.paging-bar');
            grid.updateMultiPageSelectionNotice = function () {
            };
            if(jPagingBar.length == 1) {
                var pagingBarHTML = '' +
                    '<button class="paging-first" disabled><div class="icon fa fa-fast-backward"></div></button>' +
                    '<button class="paging-prev" disabled><div class="icon fa fa-play fa-flip-horizontal"></div></button>' +
                    '<span>' + L('страница', 'default') + ' </span>' +
                    '<input type="text" class="current-page"/>' +
                    '<button class="paging-confirm"><div class="icon fa fa-check"></div></button>' +
                    '<span>' + L('&nbsp;от', 'default') + ' </span>' +
                    '<span class="total-pages"></span>' +
                    '<button class="paging-next" disabled><div class="icon fa fa-play"></div></button>' +
                    '<button class="paging-last" disabled><div class="icon fa fa-fast-forward"></div></button>' +
                    '<span class="paging-per-page"><select class="paging-per-page"></select> ' + L('на страница', 'default') + '.</span>';
                '';

                jPagingBar.append(pagingBarHTML);
                jPagingBar.find('button.paging-first').click(function (event) {
                    grid.pagingBar.setPage(1);
                });
                jPagingBar.find('button.paging-prev').click(function (event) {
                    grid.pagingBar.setPage(grid.pagingBar._currentPage - 1);
                });
                jPagingBar.find('button.paging-next').click(function (event) {
                    grid.pagingBar.setPage(grid.pagingBar._currentPage + 1);
                });
                jPagingBar.find('button.paging-last').click(function (event) {
                    grid.pagingBar.setPage(grid.pagingBar._totalPages);
                });
                jPagingBar.find('input.current-page').bind('keypress.checkEnter', function (event) {
                    if(event.keyCode == 13 && !event.altKey && !event.ctrlKey && !event.shiftKey) {
                        jPagingBar.find('button.paging-confirm').trigger('click');
                    }
                });

                jPagingBar.find('button.paging-confirm').click(function (event) {
                    var inpValue = jPagingBar.find('input.current-page').val();
                    if(isNaN(parseInt(inpValue))) return;
                    grid.pagingBar.setPage(parseInt(inpValue));

                });
                grid.pagingBar = jPagingBar[0];
                grid.pagingBar._totalPages = 1;
                grid.pagingBar._currentPage = 1;
                grid.pagingBar.getTotalPages = function () {
                    return this._totalPages;
                };
                grid.pagingBar.setTotalPages = function (pages) {
                    this._totalPages = pages;
                    jQuery(this).find('span.total-pages').html(pages + '');
                };
                grid.pagingBar.getPage = function () {
                    return this._currentPage;
                };

                grid.pagingBar.setPage = function (pageNum) {
                    if(pageNum <= 0) {
                        pageNum = 1;
                    }
                    if(pageNum > this._totalPages) {
                        pageNum = this._totalPages;
                    }
                    jQuery(this).find('input.current-page').val(pageNum);

                    if(pageNum == 1) {
                        jQuery(this).find('.paging-first, .paging-prev').attr('disabled', true);
                    } else {
                        jQuery(this).find('.paging-first, .paging-prev').attr('disabled', false);
                    }
                    if(pageNum == this._totalPages) {
                        jQuery(this).find('.paging-last, .paging-next').attr('disabled', true);
                    } else {
                        jQuery(this).find('.paging-last, .paging-next').attr('disabled', false);
                    }

//					if(pageNum != this._currentPage) {
                    this._currentPage = pageNum;
                    grid.loadData();
//					};
                };
                grid.pagingBar.setPaging = function (response) {
                    this._totalRows = parseInt(response.paging.totalRows) || 0;
                    this._totalPages = Math.ceil(this._totalRows / this.perPage.value);
                    this._totalPages = this._totalPages || 1;

                    if(this._currentPage < this._totalPages) {
                        jQuery(this).find('.paging-last, .paging-next').attr('disabled', false);
                    } else {
                        jQuery(this).find('.paging-last, .paging-next').attr('disabled', true);
                        this._currentPage = this._totalPages;
                        jQuery(this).find('input.current-page').val(this._currentPage);
                    }

                    if(this._currentPage > 1) {
                        jQuery(this).find('.paging-first, .paging-prev').attr('disabled', false);
                    } else {
                        jQuery(this).find('.paging-first, .paging-prev').attr('disabled', true);
                    }

                    jPagingBar.find('span.total-pages').text(this._totalPages);
                };
                grid.pagingBar.getPaging = function () {
                    return {
                        start: (this._currentPage - 1 ) * this.perPage.value,
                        limit: this.perPage.value
                    };
                };
                var possibleRowsPerPage = grid.initConfig.tplCfg.possibleRowsPerPage;
                if(possibleRowsPerPage.length > 1) {
                    jPagingBar.find('span.paging-per-page').css({display: 'inline-block'});
                }
                var perPageOptions = [];
                for (var i = 0; i < possibleRowsPerPage.length; i++) {
                    perPageOptions.push('<option value="' + possibleRowsPerPage[i] + '">' + possibleRowsPerPage[i] + '</option>');
                }
                jPagingBar.find('select.paging-per-page').html(perPageOptions.join(''));
                grid.pagingBar.perPage = jPagingBar.find('select.paging-per-page')[0];
                var TPLCfgrowsPerPage = grid.initConfig.tplCfg.rowsPerPage;
                grid.pagingBar.perPage.value = TPLCfgrowsPerPage ? TPLCfgrowsPerPage : possibleRowsPerPage[0];
                var perPageState = grid.getForm().getState('grid.paging.perPage');
                if(perPageState) jQuery(grid.pagingBar.perPage).val(perPageState);
                jQuery(grid.pagingBar.perPage).change(function (event) {
                    grid.getForm().saveState('grid.paging.perPage', grid.pagingBar.perPage.value);
                    grid.pagingBar._totalPages = Math.ceil(grid.pagingBar._totalRows / grid.pagingBar.perPage.value);
                    grid.pagingBar._totalPages = grid.pagingBar._totalPages || 1;
                    grid.pagingBar.setPage(grid.pagingBar.getPage());
                });
                jQuery(this).find('input.current-page').val(1);

                //multipage selection
                if(grid.initConfig.tplCfg && grid.initConfig.tplCfg.multiPageSelection) {
                    var multiPageSelectionNoticeText = L('Видими са {visibleSelected} от общо {totalSelected} маркирани реда.', 'default') + ' - <span class="unselect link">' + L('отмаркирай', 'default') + '</span>';
                    var multiPageSelectAllText = '<span class="select-all link">' + L('Маркирай всички ({totalRows}) реда.') + '</span>';
                    var jMultiPageSelectionNotice = jQuery('<span style="font-style:italic;margin-left:10px;" class="multi-page-selection-notice"></span>');
                    jPagingBar.append(jMultiPageSelectionNotice);

                    jMultiPageSelectionNotice.delegate('span.unselect', 'click', function (e) {
                        grid.setMultiPageSelectedRows([]);
                        jMultiPageSelectionNotice.html('');
                    });
                    jMultiPageSelectionNotice.delegate('span.select-all', 'click', function (e) {
                        grid.loadData({multiPageSelectAll: true});
                    });

                    grid.updateMultiPageSelectionNotice = function () {
                        setTimeout(function () {
                            var p = {
                                totalSelected: Object.keys(grid._multiPageSelectedRows).length,
                                visibleSelected: grid._selectedRowIndexes.length,
                                totalRows: grid.getTotalRows(),
                                currentRows: grid._data.length
                            };

                            if(p.totalSelected > p.visibleSelected) {
                                jMultiPageSelectionNotice.html(multiPageSelectionNoticeText.supplant(p))
                            } else {
                                if(p.totalSelected == p.visibleSelected && p.visibleSelected == p.currentRows && p.currentRows < p.totalRows) {
                                    jMultiPageSelectionNotice.html(multiPageSelectAllText.supplant(p));
                                } else {
                                    jMultiPageSelectionNotice.html('');
                                }
                            }
                        }, 0);
                    };
                    grid.addListener('rowSelect', grid.updateMultiPageSelectionNotice);
                }
            }
            grid.dblClickListeners = {};
            jGrid.find('table.grid-data').dblclick(function (event) {
                var jTD = jQuery(event.target);
                while (2) {
                    if(jTD.is('td')) break;
                    if(jTD.is('table')) return;
                    jTD = jTD.parent();
                }
                var cellClassNames = jTD[0].className.split(' ');
                for (var i = 0; i < cellClassNames.length; i++) {
                    if(cellClassNames[i].indexOf('column-') == 0) {
                        var cellDataField = cellClassNames[i].substr(7);
                        break;
                    }
                }
                if(cellDataField == 'check_box_selection') return;
                if(grid.dblClickListeners[cellDataField]) {
                    grid.dblClickListeners[cellDataField].apply({}, [jTD.parent()[0].sectionRowIndex, grid._data[jTD.parent()[0].sectionRowIndex], jTD[0]]);
                }
                grid.callListeners('cellDoubleClick', [jTD.parent()[0].sectionRowIndex, grid._data[jTD.parent()[0].sectionRowIndex], jTD[0]]);
            });

            //durtiq click listener
            var lastClickedRowIndex = -1;
            jGrid.find('table.grid-data').delegate('tbody.gird-data-body > tr > td', 'click', function (event) {
                var clickedCell = this;
                var clickedRow = this.parentNode;
                var tmp = event.target;
                var jTarget = jQuery(event.target);
                var isClickable = jTarget.is('button:not([disabled]), .clickable') || jTarget.parents('button:not([disabled]), .clickable').length > 0;


                var jClickedRow = jQuery(clickedRow);
                if(jClickedRow.is('.totals-row')) return;

                //cell click handlers
                if(isClickable) {
                    var cellDataField = clickedCell.dataField;
                    if(cellDataField && typeof(grid.clickListeners[cellDataField]) == 'function') {
                        grid.clickListeners[cellDataField].apply({}, [clickedRow.sectionRowIndex, grid._data[clickedRow.sectionRowIndex], clickedCell, event]);
                    }
                    return;
                }

                //row selection
                if(clickedRow) {
                    if(!grid.initConfig.tplCfg || !grid.initConfig.tplCfg.selectionMode || grid.initConfig.tplCfg.selectionMode == 'single') {
                        grid.setSelectedRowIndexes([clickedRow.sectionRowIndex]);
                        lastClickedRowIndex = clickedRow.sectionRowIndex;
                    } else {
                        //multiple selection

                        var rows = jGrid.find('table.grid-data > tbody > tr');
                        var last = clickedRow.sectionRowIndex;
                        var first = clickedRow.sectionRowIndex;
                        if(lastClickedRowIndex >= 0) {
                            if(lastClickedRowIndex > clickedRow.sectionRowIndex) {
                                first = clickedRow.sectionRowIndex;
                                last = lastClickedRowIndex;
                            } else {
                                last = clickedRow.sectionRowIndex;
                                first = lastClickedRowIndex;
                            }
                        }


                        if(event.ctrlKey && event.shiftKey) {
                            var rowIndexes = [];
                            for (var i = first; i <= last; i++) {
                                rowIndexes.push(i);
                            }
                            grid.addSelectedRowIndexes(rowIndexes);
                            lastClickedRowIndex = clickedRow.sectionRowIndex;
                        } else if(event.ctrlKey || (jQuery(clickedCell).hasClass('ctype-checkBoxSelection'))) {
                            var index = grid._selectedRowIndexes.indexOf(clickedRow.sectionRowIndex);
                            if(index >= 0) {
                                if(grid.initConfig.tplCfg && grid.initConfig.tplCfg.multiPageSelection) {
                                    var dataRow = grid._data[clickedRow.sectionRowIndex];
                                    if(dataRow) {
                                        if(empty(dataRow['id'])) throw new Error('Empty ID in multi page selection.');
                                        delete grid._multiPageSelectedRows[dataRow['id']];
                                    }
                                }
                                grid._selectedRowIndexes.splice(index, 1);
                                jClickedRow.removeClass('selected');
                                jQuery([
                                    jGrid.find('.normal-columns table.grid-data > tbody > tr')[clickedRow.sectionRowIndex],
                                    jGrid.find('.locked-columns table.grid-data > tbody > tr')[clickedRow.sectionRowIndex]
                                ]).removeClass('selected').find('td.ctype-checkBoxSelection input').attr('checked', false);
                                grid._setCheckAllBox();
                            } else {
                                grid.addSelectedRowIndexes([clickedRow.sectionRowIndex]);
                            }

                            lastClickedRowIndex = clickedRow.sectionRowIndex;
                        } else if(event.shiftKey) {
                            var rowIndexes = [];
                            for (var i = first; i <= last; i++) {
                                rowIndexes.push(i);
                            }
                            grid.setSelectedRowIndexes(rowIndexes);
                        } else {
                            lastClickedRowIndex = clickedRow.sectionRowIndex;
                            grid.setSelectedRowIndexes([clickedRow.sectionRowIndex]);
                        }
                    }

                    grid.callListeners('rowSelect', [clickedRow, clickedCell]);

                }

            });

            if(grid.initConfig && grid.initConfig.defaultQuickFilters) {
                grid.setQuickFilters(grid.initConfig.defaultQuickFilters);
            }

            grid.setData([]);
            jGrid.find('[tooltip]').basicToolTip();
            el.loadData = grid.loadData;
        } else {
            el.loadData = function () {
            };
        }
    });

    jQuery(el).find('input[suggest]').each(function (i, el) {
        framework.initSuggestField(el);
    });

    jQuery(el).find('input[input_select]').each(function (i, inputEl) {
        var jInput = jQuery(inputEl);

        var jSelect = jInput.parent().find('select[name=' + jInput.attr('input_select') + ']');

        if(!jSelect.length)
            return;
        var updateOptions = function () {
            jSelect[0]._optionsByCustomField = {};
            jSelect.find('option').each(function (i, opt) {
                jSelect[0]._optionsByCustomField[opt.getAttribute('custom_field')] = opt;
            });
        };
        updateOptions();

        jInput.keyup(function () {
            var option = jSelect[0]._optionsByCustomField[Util.trim(jInput.val())];
            if(option) {
                jSelect.val(option.getAttribute('value'));
                jSelect.trigger('change');
            }
        });
        var onSelectChange = function () {
            jInput.val(jSelect.find('option:selected').attr('custom_field'));
        };
        jSelect.change(onSelectChange);
        var orgSetValue = jSelect[0].setValue;
        jSelect[0].setValue = function (value) {
            orgSetValue.apply(this, arguments);
            onSelectChange();
        };
        onSelectChange();

        var orgSetContent = jSelect[0].setContent;
        jSelect[0].setContent = function () {
            orgSetContent.apply(this, arguments);
            updateOptions();
        }
    });

    jQuery(el).find('select[dragSelect]').each(function (i, selectToEl) {

        var tmp = selectToEl.getAttribute('dragSelect').split(';');

        var atr = [];

        for (var i = 0; i < tmp.length; i++) {
            var atr1 = [];
            atr1 = tmp[i].split('=');
            atr[jQuery.trim(atr1[0])] = jQuery.trim(atr1[1]);
        }

        var selectFrom = jQuery(selectToEl.parentForm).find('select[name=' + atr['srcSelect'] + ']');

        var selectTo = jQuery(selectToEl);

        selectFrom.dblclick(function (event) {
            if(event.target.nodeName.toLowerCase() == 'select') {
                jQuery(event.target).find('option:selected').appendTo(selectTo);
            } else {
                jQuery(event.target).appendTo(selectTo);
            }
        });

        selectTo.dblclick(function (event) {
            if(event.target.nodeName.toLowerCase() == 'select') {
                jQuery(event.target).find('option:selected').appendTo(selectFrom);
            } else {
                jQuery(event.target).appendTo(selectFrom);
            }
        });

        //Move selected to source select
        if(atr['btnToSrc']) {
            jQuery(el).find('button[name=' + atr['btnToSrc'] + ']').click(function (event) {
                selectTo.find('option:selected').appendTo(selectFrom);
            });
        }

        //Move selected from source select
        if(atr['btnFromSrc']) {
            jQuery(el).find('button[name=' + atr['btnFromSrc'] + ']').click(function (event) {
                selectFrom.find('option:selected').appendTo(selectTo);
            });
        }

        //Move all to source select
        if(atr['btnAllToSrc']) {
            jQuery(el).find('button[name=' + atr['btnAllToSrc'] + ']').click(function (event) {
                selectTo.find('option').appendTo(selectFrom);
            });
        }

        //Move all from source select
        if(atr['btnAllFromSrc']) {
            jQuery(el).find('button[name=' + atr['btnAllFromSrc'] + ']').click(function (event) {
                selectFrom.find('option').appendTo(selectTo);
            });
        }

        var srcOptions = {};

        selectFrom.find('option').each(function (i, el) {
            srcOptions[el.value] = el;
        });

        selectTo[0].getValue = function () {
            var value = [];
            selectTo.find("option").each(function (i, el) {
                value.push(el.value);
            });
            return value;
        };

        selectTo[0].setValue = function (value) {
            selectTo.find('option').appendTo(selectFrom);

            value = value || [];
            if(!value.length) value = [];
            for (var i = 0; i < value.length; i++) {
                if(srcOptions[value[i]]) {
                    selectTo.append(srcOptions[value[i]]);
                }
            }
        };

        selectFrom[0].getValue = function () {
        };
    });

    jQuery(el).find('div.suggest-combo').each(function(i, div) {
        div._input = jQuery(div).find('input')[0];
        jQuery(div).find('> span').click(function(e) {
            if(!div._input._autoComplete.enabled) {
                if(!jQuery(div._input).focused()) jQuery(div._input).focus();
                e.preventDefault();
                e.stopPropagation();
                setTimeout(function(){
                    div._input._autoComplete.onValueChange(true);
                },0);
            }
        });
    });

    jQuery(el).find('div.form').each(function (i, form) {
        for (var i in form.fields) {
            //set value event
            form.fields[i].originalSetValue = form.fields[i].setValue;
            if(form.fields[i].nodeName) {
                form.fields[i].addListener = function (name, fn, context) {
                    if(name == 'change' && !this._onChange) {
                        this._onChange = function () {
                            this.callListeners('change', [this.getValue()]);
                        };
                        if(this.nodeName.toLowerCase() == 'select' || this.type == 'checkbox') {
                            jQuery(this).change(this._onChange);
                        } else if(jQuery(this).is('input.datePicker') || jQuery(this).is('input.dateTimePicker')) {

                        } else {
                            jQuery(this).keypress(function () {
                                var that = this;
                                setTimeout(function () {
                                    that._onChange();
                                }, 0);
                            });
                        }
                    }
                    framework.addListener.apply(this, arguments);
                };
            } else if(form.fields[i].radioButtons) {
                form.fields[i].addListener = function (name, fn, context) {
                    if(name == 'change' && !this._onChange) {
                        this._onChange = true;
                        var that = this;
                        for (var j in this.radioButtons) {
                            jQuery(this.radioButtons[j]).change(function () {
                                that.callListeners('change', [that.getValue()]);
                            });
                        }
                    }
                    framework.addListener.apply(this, arguments);
                }
            } else {
                alert('xa');
                form.fields[i].addListener = framework.addListener;
            }

            form.fields[i].removeListener = framework.removeListener;
            form.fields[i].callListeners = framework.callListeners;
            form.fields[i].setValue = function () {
                var res = this.callListeners('setValue', arguments);
                var canceled = false;
                for (var i = 0; i < res.length; i++) {
                    if(res[i] === false) {
                        canceled = true;
                        break;
                    }
                }
                if(!canceled) {
                    this.originalSetValue.apply(this, arguments);
                    this.callListeners('change', [this.getValue()]);
                }
            };

            //clear error
            if(typeof(form.fields[i].setError) == 'function') {
                form.fields[i].addListener('setValue', function (val) {
                        this.setError();
                    },
                    form.fields[i]
                );
            }
        }
    });

};

framework.addListener = function (eventName, fn, context) {
    if(!this._listeners) this._listeners = {};
    this._listeners[eventName] = this._listeners[eventName] || [];
    this.removeListener(eventName, fn);
    this._listeners[eventName].push({fn: fn, context: context});

};
framework.removeListener = function (eventName, fn) {
    if(!this._listeners) this._listeners = {};
    if(this._listeners[eventName] && this._listeners[eventName].length) {
        for (var i = 0; i < this._listeners[eventName].length; i++) {
            if(this._listeners[eventName][i].fn == fn) {
                this._listeners[eventName].splice(i);
                break;
            }
        }
    }
};
framework.callListeners = function (eventName, params) {
    if(!this._listeners) this._listeners = {};
    var results = [];
    if(this._listeners[eventName] && this._listeners[eventName].length) {
        for (var i = 0; i < this._listeners[eventName].length; i++) {
            if(typeof(this._listeners[eventName][i].fn) == 'function') {
                if(!params || !params.length) params = [];
                results.push(this._listeners[eventName][i].fn.apply(this._listeners[eventName][i].context, params));
            }
        }
    }
    return results;
};
framework.addLoadingMask = function (progressCfg) {
    //remove focus
    if(!framework.loadingMask) {
        var parent = document.getElementById('cm');
        if(!parent) return;
        framework.loadingMask = document.createElement('div');
        framework.loadingMask.innerHTML = '<table><tr valign="middle"><td align="center">' +
            //'<img src="images/preloader.gif" class="loading-bar"/>'+
            '<i class="fa fa-refresh fa-spin" style="font-size: 100px; color: #607D8B;"></i>' +
            '<div class="panel-frame progress-bar-frame" style="padding:10px;">' +
            '<div class="panel-frame-body">' +
            '<div class="progress-bar-description" style="max-width:400px;text-align:left;padding-bottom:6px;">' +
            '</div>' +
            '<div class="progress-bar-outer" style="min-width:200px;">' +
            '<div class="progress-bar-progress">' +
            '</div>' +
            '<span class="progress-bar-text">' +
            '</span>' +
            '</div>' +
            '<button class="close" style="display:none;float: right;margin-top:10px;"><div class="icon icon-close"></div><span>Откажи</span></button>' +
            '</div>' +
            '</div>' +
            '</td></tr></table>';
        framework.loadingMask.className = 'loading-mask';
        framework.loadingMask.style.display = 'none';
        parent.appendChild(framework.loadingMask);
    }
    if(!framework.loadingMaskCount) {
        framework.focusElementBeforeLoadingMask = document.activeElement;
        jQuery('<input style="position:fixed;top:-1231px;"/>').appendTo(document.body).focus().remove();
        framework.loadingMask.style.display = '';
    }
    framework.loadingMaskCount++;
};

framework.setLoadingMaskProgress = function (progressCfg) {
    if(empty(progressCfg) || !framework.loadingMask) return;
    if(!jQuery(framework.loadingMask).hasClass('progress-bar')) jQuery(framework.loadingMask).addClass('progress-bar');
    var val = parseFloat(progressCfg.percent);
    if(isNaN(val) || val < 0 || val > 100) val = 0;
    jQuery(framework.loadingMask).find('.progress-bar-progress').css({right: (100 - val).toFixed(2) + '%'});
    jQuery(framework.loadingMask).find('.progress-bar-text').text(progressCfg.completionText || Math.round(val, 0) + '%');
    if(typeof progressCfg.descriptionText != 'undefined') {
        jQuery(framework.loadingMask).find('.progress-bar-description').html(progressCfg.descriptionText);
        jQuery(framework.loadingMask).find('.progress-bar-description').css({display: empty(progressCfg.descriptionText) ? 'none' : ''});
    }
    if(['red', 'green', 'blue'].indexOf(progressCfg.color) >= 0) {
        jQuery(framework.loadingMask).find('.progress-bar-outer').removeClass('red green blue').addClass(progressCfg.color);
    }
    if(typeof progressCfg.onCancel != 'undefined') {
        jQuery(framework.loadingMask).find('button.close').css({display: progressCfg.onCancel ? '' : 'none'});
        if(typeof progressCfg.onCancel == 'function') {
            jQuery(framework.loadingMask).find('button.close').unbind('click').click(progressCfg.onCancel);
        }
    }
};

framework.removeLoadingMask = function () {
    if(!framework.loadingMaskCount) return;
    if(framework.loadingMaskCount == 1) {
        framework.loadingMask.style.display = 'none';
        jQuery(framework.loadingMask).find('button.close').unbind('click');
        framework.setLoadingMaskProgress({descriptionText: '', onCancel: null, pecent: 0});
        jQuery(framework.loadingMask).removeClass('progress-bar');
        var jLastFocusEl = jQuery(framework.focusElementBeforeLoadingMask);
        if(!jLastFocusEl.hasClass('main-menu-link')) jLastFocusEl.focus();
    }
    framework.loadingMaskCount--;
};
framework.loadingMaskCount = 0;
framework.scrollerWidth = 17;

framework.generatedID = 1;

framework.elementMethods = {};

framework.elementMethods.radioGetValue = function () {
    for (var i in this.radioButtons) {
        if(this.radioButtons[i].checked) return this.radioButtons[i].value;
    }
    return '';
};
framework.elementMethods.radioSetValue = function (value) {
    if(this.radioButtons[value]) this.radioButtons[value].checked = true;
};


framework.elementMethods.setErrorInp = function (errMsg) {
    var jThis = jQuery(this);
    if(jThis.hasClass('suggest-combo-input'))
        jThis = jThis.parent().parent();
    if(errMsg) {
        if(typeof(this._errOriginalWidth) != 'number') {
            this._errOriginalWidth = jThis.width();
            this._errOriginalPaddingRight = intval(jThis.css('paddingRight'));
        }
        jThis.css('paddingRight', (this._errOriginalPaddingRight + 17) + 'px');
        jThis.width(this._errOriginalWidth - 17);
        framework.elementMethods.setError.apply(this, arguments);
    } else {
        if(typeof(this._errOriginalWidth) == 'number') {
            jThis.css('paddingRight', (this._errOriginalPaddingRight) + 'px');
            jThis.width(this._errOriginalWidth);
        }
        this._errOriginalWidth = false;
        framework.elementMethods.setError.apply(this, arguments);
    }
};
framework.elementMethods.setError = function (errMsg) {
    var jThis = jQuery(this);
    if(errMsg) {
        this._errMsg = errMsg;
        jThis.addClass('err');
        if(this.title) {
            this.__err_original_title = this.title;
        }
        if(this.getAttribute('tooltip')) {
            this.__err_original_tooltip = this.getAttribute('tooltip');
        }
        this.setAttribute('tooltip', '<span style="color:#E91E63;">' + errMsg + '</span>');
        jThis.basicToolTip();
    } else if(this._errMsg) {
        this._errMsg = '';
        jThis.removeClass('err');
        if(this.__err_original_title) {
            this.title = this.__err_original_title;
            this.__err_original_title = null;
            this.__err_original_tooltip = null;
            jThis.basicToolTip('remove');
        } else if(this.__err_original_tooltip) {
            this.setAttribute('tooltip', this.__err_original_tooltip);
            this.__err_original_title = null;
            this.__err_original_tooltip = null;
        } else {
            jThis.basicToolTip('remove');
            this.__err_original_title = null;
            this.__err_original_tooltip = null;
        }
    }
};

framework.elementMethods.onInputKeyPressCheckEnter = function (event) {
    if(event.keyCode == 13 && !event.altKey && !event.ctrlKey && !event.shiftKey) {
        if(typeof(this.onPressEnterFn) == 'function') {
            this.onPressEnterFn(event);
        }
        if(this.getAttribute('onPressEnterBtn')) jQuery(this.parentForm).find(this.getAttribute('onPressEnterBtn')).trigger('click');
    }
};

framework.elementMethods.onInputKeyPressClearErr = function (event) {
    this.setError();
};
framework.elementMethods.onInputKeyPressRestriction = function (event) {
    var keyRestriction = this.getAttribute('keyRestriction');
    var jThis = jQuery(this);
    var canceled = false;
    if(keyRestriction) {
        if(typeof(framework.keyRestrictionFn[keyRestriction]) == 'function') {
            var fn = framework.keyRestrictionFn[keyRestriction];
        } else {
            var fn = framework.keyRestrictionFn['regex'];
        }
        var charCode = event.charCode || event.which;
        var ch = charCode ? String.fromCharCode(charCode) : false;
        if(charCode < 20) ch = false;
        if(fn.apply(this, [event, ch]) === false) {
            canceled = true;
            event.preventDefault();
        }
    }
};
framework.elementMethods.onInputBlurValidation = function (event) {
    this.isValid(true);
};
framework.elementMethods.isValid = function (setTheError) {
    var validation = this.getAttribute('validation');
    var valid = false;
    if(validation) {
        if(typeof(framework.validationFn[validation]) == 'function') {
            var fn = framework.validationFn[validation];
        } else {
            var fn = framework.validationFn['regex'];
        }
        var msg = fn.apply(this, [this.value]);
        if(msg) {
            if(setTheError) {
                if(this.getAttribute('validationMsg')) {
                    this.setError(this.getAttribute('validationMsg'));
                } else {
                    this.setError(msg);
                }
            }
            valid = false;
        } else {
            if(setTheError) this.setError();
            valid = true;
        }
    } else {
        valid = true;
    }
    return valid;
};

framework.doExport = function (exportMethod, exportType, params) {
    // проверка заради визуализирането на pdf под Firefox на MAC OS
    if(exportType == 'PDF') {
        Util.BrowserDetect.init();

        var os = Util.BrowserDetect.OS;
        var browser = Util.BrowserDetect.browser;

        if(os == 'Mac' && browser == 'Firefox') {
            params['pdf_attach'] = 1;
        }
    }

    var JSONParams = JSON.stringify(params);
    var useGet = true;
    if(JSONParams.length > 250) useGet = false;

    if(!exportMethod || !exportType) return false;
    Util.openWithPostParams(BASE_URL + '/export.php', {
        exportMethod: exportMethod,
        exportType: exportType,
        request: JSONParams
    }, null, null, useGet);
};
framework.notExport = framework.doExport;


framework.dialogNumbers = 1;
framework.createWindow = function (config) {
    config = config || {};
    config.closable = config.closable !== false;
    config.modal = config.modal !== false;
    if(!config.tplName && !config.content) return false;
    if(config.baseParams) {
        var baseParams = config.baseParams;
        delete config.baseParams;
    } else {
        var baseParams = {};
    }
    config.isWindow = true;
    var prefix = 'window-content-' + framework.window.prefixCounter++;
    var windowOuter = document.createElement('div');
    var jWindowOuter = jQuery(windowOuter);
    var jParent = jQuery(config.parent);
    if(!jParent.length) {
        if(!config.modal) alert('Параметъра parent е задължителен за немодални прозорци.');
        jParent = jQuery('#cm');
    }

    windowOuter.className = 'window-outer';

    var show = function (fn) {
        var jWindow = jWindowOuter.find('#' + prefix + '-frame-container');
        if(jWindow.length !== 1) {
            framework.removeLoadingMask();
            console.log('Unable to create window - frame not found.');
            jWindowOuter.remove();
            return;
        }
        var jPanelBody = jWindow.children().filter('div.panel-frame-body');
        var jPanelHeader = jWindow.children().filter('div.panel-frame-top');
        jPanelHeader.attr('unselectable', 'on');
        jWindowOuter.css('visibility', 'hidden');
        jWindowOuter.css('display', 'block');

        jWindow[0].config = config;
        jWindow[0].close = framework.window.close;
        jWindow[0].setSize = framework.window.setSize;
        jWindow[0].center = framework.window.center;
        jWindow[0].addListener = framework.addListener;
        jWindow[0].removeListener = framework.removeListener;
        jWindow[0].callListeners = framework.callListeners;

        var rootEl = document.getElementById(prefix) || {};
        rootEl.window = jWindow[0];
        framework.attachMethods(jWindowOuter);

        if(config.modal) {
            framework.window.addModalLayer();
        }

        jWindowOuter[0].style.zIndex = framework.window.baseZIndex + framework.window.openWindows.push(jWindowOuter[0]);
        framework.lastActiveElement = jPanelBody[0];

        if(!(rootEl.getAttribute && rootEl.getAttribute('isError'))) {
            try {
                fn.apply({}, [rootEl, prefix, baseParams]);
            } catch (err) {
                err.fileName = config.tplName + '.js';
                err.lineNumber = err.lineNumber - 12;//reda na eval-a vuv server.js
                if(typeof(console) != 'undefined' && typeof(console.log) == 'function') {
                    console.log(err);
                }
            }
            if(config.width) jPanelBody.width(intval(config.width));
            if(config.height) jPanelBody.height(intval(config.height));
            if(config.maxWidth || config.maxHeight) jPanelBody.css({overflow: 'auto'});
            if(config.maxWidth) jPanelBody.css({maxWidth: intval(config.maxWidth) + 'px'});
            if(config.maxHeight) jPanelBody.css({maxHeight: intval(config.maxHeight) + 'px'});
        }

        jWindow.find('button.close').bind('click.closeWindow', function (event) {
            jWindow[0].close();
        });
        if(jPanelHeader.find('button.close').length) jWindow[0].addListener('Esc', function () {
            jWindow[0].close();
        });


        jWindowOuter.bind('mousedown.setOnTop', framework.window.onWindowClickSetOnTop);
        jPanelHeader.bind('mousedown.windowDrag', {jContainer: jWindowOuter}, framework.window.onMouseDownDrag);

        jWindow[0].center();
        var jFocusEl = jQuery(rootEl).find('[requestFocus]:first');
        if(!jFocusEl.length) jFocusEl = jQuery(rootEl).find('input:text:first');

        jWindowOuter.css('visibility', 'visible');
        if(jFocusEl.length) {
            if(framework.loadingMaskCount) framework.focusElementBeforeLoadingMask = jFocusEl[0];
            else jFocusEl.focus();
        }
        if(typeof(config.callBack) == 'function') config.callBack.call(config.context, jWindow[0]);

        if(rootEl.initResponse) rootEl.processResponse(rootEl.initResponse, null, {}, null, config.context);
        if(rootEl.initError) rootEl.setError(rootEl.initError);

        return rootEl;
    };

    var create = function (response) {
        jParent.append(jWindowOuter);
        jWindowOuter.append(response);

        var rootEl = document.getElementById(prefix);

        if(config.content || !rootEl) {
            return show(function () {
            });
        } else {
            var fn = rootEl.fn;
            if(typeof (fn) == 'function') show(fn);
            else if(typeof(fn) == 'string') {
                framework.addLoadingMask();
                Server.loadFnScript(fn, function (realFn) {
                        show(realFn);
                        framework.removeLoadingMask();
                    }, {}, function () {
                        framework.removeLoadingMask();
                    }
                );
            } else {
                show(function () {
                });
            }
        }
    };

    if(config.content) {
        var frameClasses = ['panel-frame'];
        if(config.collapsible) frameClasses.push('collapsible');
        var frame = document.createElement('div');

        frame.id = prefix + '-frame-container';
        frame.className = frameClasses.join(' ');
        var frameTop = document.createElement('div');
        frameTop.className = 'panel-frame-top';
        if(config.frame === false) frameTop.style.display = 'none';
        var frameTitle = document.createElement('div');
        frameTitle.className = 'panel-frame-title';
        if(config.title) {
            jQuery(frameTitle).text(config.title);
        }

        var frameButtons = document.createElement('div');
        frameButtons.className = 'panel-frame-buttons';
        frameTop.appendChild(frameButtons);
        frameTop.appendChild(frameTitle);
        if(config.collapsible) {
            var closeBtn = document.createElement('button');
            closeBtn.className = 'collapse';
            closeBtn.innerHTML = '<div class="icon fa fa-caret-up"></div>';
            frameButtons.appendChild(closeBtn);
        }
        if(config.closable) {
            var closeBtn = document.createElement('button');
            closeBtn.className = 'close';
            closeBtn.innerHTML = '<div class="icon fa fa-close"></div>';
            frameButtons.appendChild(closeBtn);
        }

        frame.appendChild(frameTop);
        var frameBody = document.createElement('div');
        frameBody.className = 'panel-frame-body';
        frame.appendChild(frameBody);
        var jContent = jQuery(config.content);
        if(jContent.length) {
            if(jContent.parent().length) {
                frame.originalParent = jContent.parent()[0];
            }
            jQuery(frameBody).append(jContent);
        } else {
            return;
        }
        return create(frame);
    } else if(config.target) {
        var url = BASE_URL + '/index.php?direct=1&target=' + config.tplName;
        var paramsID = framework.window.prefixCounter++;
        var params = {
            baseParams: JSON.stringify(baseParams),
            frameConfig: JSON.stringify(config),
            paramsID: paramsID
        };
        framework.window.popupParams[paramsID] = baseParams;
        setTimeout(function () {
            delete framework.window.popupParams[paramsID]
        }, 30000);
        var windowParams = ['resizeable', 'scrollbars', 'resizable'];
        if(config.width) {
            windowParams.push('width=' + config.width);
        } else {
            windowParams.push('left=0');
            windowParams.push('width=' + window.screen.availWidth);
        }
        if(config.height) {
            windowParams.push('height=' + config.height);
        } else {
            windowParams.push('top=0');
            windowParams.push('height=' + window.screen.availHeight);
        }

        var wnd = Util.openWithPostParams(url, params, config.target, windowParams.join(', '));
    } else {
        framework.addLoadingMask();
        delete config.parent;
        jQuery.ajax({
            type: 'POST',
            url: BASE_URL + '/load_tpl.php',
            data: {
                file: config.tplName,
                prefix: prefix,
                baseParams: JSON.stringify(baseParams),
                getParams: JSON.stringify(Server.getParams),
                frameConfig: JSON.stringify(config),
                getTranslation: translations[config.tplName] ? '' : 'true',
                formStates: framework.getStatesToSave()
            },
            dataType: 'text',
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                framework.alert('Connection error.', 'error')
                framework.removeLoadingMask();
            },
            success: function () {
                framework.removeLoadingMask();
                create.apply(this, arguments);
            },
            timeout: Server.requestTimeout
        });
    }
};
framework.window = {};
framework.window.popupParams = {};
framework.window.getPopupParams = function (id) {
    return framework.window.popupParams[id];
};
framework.window.center = function () {
    var jWindowOuter = jQuery(this).parent();
    var top = ((jQuery(window).height() - jWindowOuter.height()) / 2 );
    var left = ((jQuery(window).width() - jWindowOuter.width()) / 2 );
    jWindowOuter.css({
        top: (top >= 0 ? top : 0 ) + 'px',
        left: (left >= 0 ? left : 0 ) + 'px'
    })
};
framework.window.setSize = function (width, height, center) {
    var jWindowBody = jQuery(this).children().filter('.panel-frame-body');
    jWindowBody.width(width);
    jWindowBody.height(height);
    if(center !== false)  this.center();
};
framework.window.onMouseDownDrag = function (event) {
    var jThis = jQuery(this);
    var jTarget = jQuery(event.target);
    if(jTarget.is('button') || jTarget.parent().is('button')) return;
    var data = {
        startMouseX: event.clientX,
        startMouseY: event.clientY,
        startWindowX: intval(event.data.jContainer.css('left')),
        startWindowY: intval(event.data.jContainer.css('top')),
        clientMaxX: jQuery(window).width(),
        clientMaxY: jQuery(window).height(),
        windowHeight: jThis.parent().outerHeight(),
        windowWidth: jThis.parent().outerWidth(),
        jContainer: event.data.jContainer,
        hasMoved: false
    };
    jQuery(document).bind('mouseup.windowDrag', data, framework.window.onMouseUpDrag);
    jQuery(document).bind('mousemove.windowDrag', data, framework.window.onMouseMoveDrag);
};
framework.window.onMouseMoveDrag = function (event) {
    if(!event.data.hasMoved) event.data.jContainer.addClass('moving');
    event.data.hasMoved = true;
    var mouseX = event.clientX;
    var mouseY = event.clientY;

    //dopuska prozoreca da izliza chastichno izvun stranicata
    if(mouseX < 5) mouseX = 5;
    if(mouseX > event.data.clientMaxX - 5) mouseX = event.data.clientMaxX - 5;
    if(mouseY < 5) mouseY = 5;
    if(mouseY > event.data.clientMaxY - 5) mouseY = event.data.clientMaxY - 5;

    var x = event.data.startWindowX + mouseX - event.data.startMouseX;
    var y = event.data.startWindowY + mouseY - event.data.startMouseY;

    //ne dopuska prozoreca da izliza izvun stranicata
//	if(x > event.data.clientMaxX - event.data.windowWidth -5) x = event.data.clientMaxX - event.data.windowWidth -5;
//	if(y > event.data.clientMaxY - event.data.windowHeight -5) y = event.data.clientMaxY - event.data.windowHeight -5;
//	if(x <5) x = 5;
//	if(y <5) y = 5;
    event.data.jContainer.css({
        left: x,
        top: y
    });
};
framework.window.onMouseUpDrag = function (event) {
    event.data.jContainer.removeClass('moving');
    jQuery(document).unbind('mouseup.windowDrag');
    jQuery(document).unbind('mousemove.windowDrag');
};
framework.window.onWindowClickSetOnTop = function (event) {
    for (var i = framework.window.openWindows.length - 1; i >= 0; i--) {
        if(framework.window.openWindows[i] == this) {
            framework.window.openWindows.splice(i, 1);
            break;
        }
    }
    framework.window.openWindows.push(this);
    for (var i = 0; i < framework.window.openWindows.length; i++) framework.window.openWindows[i].style.zIndex = framework.window.baseZIndex + i;
};
framework.window.addModalLayer = function () {
    if(!framework.window.modalLayer) {
        framework.window.modalLayer = document.createElement('div');
        framework.window.modalLayer.className = 'window-modal-layer';
        document.body.appendChild(framework.window.modalLayer);
    }
    framework.window.modalLayer.style.zIndex = framework.window.baseZIndex + framework.window.openWindows.push(framework.window.modalLayer);
    framework.window.modalLayer.style.display = 'block';
    return framework.window.modalLayer;
};
framework.window.removeModalLayer = function () {
    if(!framework.window.modalLayer || !framework.window.openWindows.length || framework.window.modalLayer.style.display == 'none') return;
    for (var i = framework.window.openWindows.length - 1; i >= 0; i--) {
        if(framework.window.openWindows[i] == framework.window.modalLayer) {
            framework.window.openWindows.splice(i, 1);
            break;
        }
    }
    var found = false;
    for (var i = framework.window.openWindows.length - 1; i >= 0; i--) {
        framework.window.openWindows[i].style.zIndex = framework.window.baseZIndex + i;
        if(framework.window.openWindows[i] == framework.window.modalLayer) {
            found = true;
        }
    }
    if(!found) {
        framework.window.modalLayer.style.display = 'none';
    }
};
framework.window.close = function (forceClose) {
    var canceled = this.callListeners('close');
    if(!forceClose) for (var i = 0; i < canceled.length; i++) if(canceled[i] === false) return false;
    var jThis = jQuery(this);
    if(!jThis.parents('body')[0] || jThis.parents('body')[0] != document.body) return;
    var jWindowParent = jThis.parent();
    if(this.originalParent) {
//		this.parentNode.removeChild(this);
//		this.originalParent.appendChild(this);
        jQuery(this).appendTo(jQuery(this.originalParent));
    }
    jWindowParent.remove();
    if(this.config.modal) {
        framework.window.removeModalLayer();
    }

    for (var i = framework.window.openWindows.length - 1; i >= 0; i--) {
        if(framework.window.openWindows[i] == this.parentNode) {

            framework.window.openWindows.splice(i, 1);
            break;
        }
    }
    for (var i = 0; i < framework.window.openWindows.length; i++) framework.window.openWindows[i].style.zIndex = framework.window.baseZIndex + i;

    if(framework.window.openWindows.length) {
        var lastWnd = framework.window.openWindows[framework.window.openWindows.length - 1];
        framework.lastActiveElement = jQuery(lastWnd).children('.panel-frame').children(':first')[0];
    }
};
framework.window.openWindows = [];
framework.window.modalLayer = false;
framework.window.baseZIndex = 5000;
framework.window.prefixCounter = 1;


framework.messageBox = function (config) {
    if(!config) {
        return;
    }

    if(typeof(config) == 'string' || typeof(config) == 'number') {
        config = {'msg': config};
    }

    jQuery.extend(
        {
            'msg': 'Default message.',
            'title': 'Default title.',
            'type': 'info',
            'buttons': [],
            'context': {}
        },
        config
    );
    var iconSrc;
    switch (config.type) {
        case 'warning':
            iconSrc = 'fa fa-exclamation-triangle';
            break;
        case 'confirm':
            iconSrc = 'fa fa-check-circle';
            break;
        case 'error':
            iconSrc = 'fa fa-times-circle';
            break;
        case 'comment':
            iconSrc = 'fa fa-comment';
            break;
        default:
            iconSrc = 'fa fa-info-circle';
    }

    var msgBoxStyle = '';

    if(config.width && parseInt(config.width))
        msgBoxStyle = 'width:' + config.width + 'px;';

    var windowContentHTML = '<div class="msg-box-content" style="' + msgBoxStyle + '">'
        + '<table>'
        + '<tr>'
        + '<td class="icon-cell">'
        + '<div class="icon  ' + iconSrc + '"></div>'
        + '</td>'
        + '<td class="content-cell">'
        + '<div class="content"></div>'
        + '</td>'
        + '</tr>'
        + '</table>'
        + '<div class="toolbar">'
        + '</div>'
        + '</div>';

    var jWindowContent = jQuery(windowContentHTML);
    jWindowContent.find('td.content-cell .content').append(config.msg);
    if(config.buttons && config.buttons.length) {
        for (var i = 0; i < config.buttons.length; i++) {
            if(typeof(config.buttons[i]) == 'object') {
                var btnCfg = config.buttons[i];
            } else {
                var btnCfg = {
                    text: config.buttons[i],
                    value: config.buttons[i]
                }
            }
            var btn = document.createElement('button');
            var icon = false;
            if(btnCfg.iconCls) {
                icon = document.createElement('div');
                icon.className = 'icon ' + btnCfg.iconCls;
            }
            if(icon) btn.appendChild(icon);
            if(btnCfg.text) {
                var text = document.createElement('span');
                jQuery(text).text(btnCfg.text);
                btn.appendChild(text);
            }
            if(btnCfg.isDefault) {
                btn.setAttribute('isdefault', true);
            }
            if(typeof(btnCfg.value) != 'undefined') {
                btn._value = btnCfg.value;
            }
            jWindowContent.find('div.toolbar').append(btn);
        }
    } else {
        jWindowContent.find('div.toolbar').html('<button><div class="icon fa fa-check"></div><span>OK</span></button>');
    }
    var wnd = framework.createWindow({
        closable: config.closable ? true : false,
        title: config.title,
        content: jWindowContent
    }).window;
    if(!jWindowContent.find('div.toolbar button[isdefault]').focus().length) {
        jWindowContent.find('div.toolbar button:first').focus();
    }

    wnd.addListener('Esc', function () {
        if(typeof(config.callBack) == 'function') {
            config.callBack.apply(config.context, [false]);
        }
        wnd.close();
    });

    jWindowContent.find('div.toolbar button').bind('click', function (event) {
        if(typeof(config.callBack) == 'function') {
            config.callBack.apply(config.context, [this._value]);
        }
        wnd.close();
    });
    return wnd;
};

framework.alert = function (message, type, title, callBack) {
    return framework.messageBox({
        msg: message,
        title: title,
        type: type,
        callBack: callBack
    });
};

framework.msgBox = function (title, type, msg, btn, callBack, parent) {
    framework.messageBox({
        title: title,
        msg: msg,
        callBack: callBack,
        type: type,
        buttons: btn ? btn.split(' ') : null
    });

};

framework.confirm = function (title, msg, callback, context) {
    var config;
    if(typeof(title) == 'string' || typeof(title) == 'number') {
        config = {
            msg: msg,
            title: title,
            callBack: callback,
            context: context
        };
    } else {
        config = title;
    }

    config.buttons = [
        {
            text: config.okButtonText || L('Да'),
            value: true,
            iconCls: 'fa fa-check'
        }, {
            text: config.cancelButtonText || L('Не'),
            value: false,
            iconCls: 'fa fa-close'
        }
    ];

    config.type = 'confirm';

    framework.messageBox(config);
};

framework.prompt = function (title, msg, callback, context, value) {
    var config;
    if(typeof(title) == 'string' || typeof(title) == 'number') {
        config = {
            msg: msg,
            title: title,
            callBack: callback,
            context: context,
            value: value
        };
    } else {
        config = title;
    }
    config.type = 'confirm';
    config.msg = config.msg || msg || '';
    config.context = config.context || context;

    if(config.inputType && config.inputType == 'textarea') {
        var cols = config.textareaCols || 80;
        var rows = config.textareaRows || 5;
        config.msg += '<br /><br /><textarea rows="'+rows+'" cols="'+cols+'" value="" class="framework_prompt_input" style="width: 100%;-moz-box-sizing:border-box; -webkit-box-sizing:border-box; box-sizing:border-box;" ></textarea>';
    } else {
        config.msg += '<br /><br /><input value="" class="framework_prompt_input" style="width: 100%;-moz-box-sizing:border-box; -webkit-box-sizing:border-box; box-sizing:border-box;" />';
    }

    var cb = config.callBack || callback;
    config.callBack = function (sure) {
        if(cb) cb.call(config.context, sure ? jInput.val() : false);
    };

    config.buttons = [
        {
            text: config.okButtonText || 'OK',
            value: true,
            iconCls: 'fa fa-check'
        }, {
            text: config.cancelButtonText || 'Cancel',
            value: false,
            iconCls: 'fa fa-close'
        }
    ];

    var wnd = framework.messageBox(config);
    var jInput = jQuery(wnd).find('.framework_prompt_input');
    jInput.val(config.value || '');
    jInput.selection(0, 141414);
    jInput[0].onPressEnterFn = function () {
        wnd.close();
        if(typeof(cb) == 'function') cb.call(config.context, jInput.val());
    };
    jInput.focus();
    return wnd;
};


translations = {};
translations['default'] = {};
translations.setTranslationsForFile = function (file, translations) {
    this[file] = translations;
};

translations.getTranslation = function (text, file) {
    return text;
};
function L() {
    return translations.getTranslation.apply(translations, arguments);
}

framework.initDatePicker = function () {
    if(framework.initDatePicker.init) return;
    framework.initDatePicker.init = true;
    jQuery.datepicker.setDefaults({
        showWeek: true,
        weekHeader: '№',
        monthNames: [
            L('Януари', 'default'),
            L('Февруари', 'default'),
            L('Март', 'default'),
            L('Април', 'default'),
            L('Май', 'default'),
            L('Юни', 'default'),
            L('Юли', 'default'),
            L('Август', 'default'),
            L('Септември', 'default'),
            L('Октомври', 'default'),
            L('Ноември', 'default'),
            L('Демекври', 'default')
        ],
        monthNamesShort: L('Яну Фев Мар Апр Май Юни Юли Авг Сеп Окт Ное Дек', 'default').split(' '),
        dayNames: [
            L('Неделя', 'default'),
            L('Понеделник', 'default'),
            L('Вторник', 'default'),
            L('Сряда', 'default'),
            L('Четвъртък', 'default'),
            L('Петък', 'default'),
            L('Събота', 'default')
        ],
        inline: true,
        dateFormat: ( L('js_date_format') != 'js_date_format' ? L('js_date_format') : 'dd-mm-yy' ),
        formatDate: L('php_date_format'),
        dayNamesMin: L('Нд Пн Вт Ср Чт Пт Сб', 'default').split(' '),
        closeText: L('Запaзи', 'default'), // Display text for close link
        prevText: L('Назад', 'default'), // Display text for previous month link
        nextText: L('Напред', 'default'), // Display text for next month link
        currentText: L('Днес', 'default'), // Display text for current month link
        firstDay: 1, // The first day of the week, Sun = 0, Mon = 1, ...
        gotoCurrent: true, // When true the current day link moves to the currently selected date instead of today.
        stepMonth: 1 // Set how many months to move when clicking the Previous/Next links.
    });
};

framework.initDateTimePicker = function () {
    if(framework.initDateTimePicker.init) return;

    jQuery.timepicker.setDefaults({
        monthNames: [
            L('Януари', 'default'),
            L('Февруари', 'default'),
            L('Март', 'default'),
            L('Април', 'default'),
            L('Май', 'default'),
            L('Юни', 'default'),
            L('Юли', 'default'),
            L('Август', 'default'),
            L('Септември', 'default'),
            L('Октомври', 'default'),
            L('Ноември', 'default'),
            L('Демекври', 'default')
        ],
        monthNamesShort: L('Яну Фев Мар Апр Май Юни Юли Авг Сеп Окт Ное Дек', 'default').split(' '),
        dayNames: [
            L('Неделя', 'default'),
            L('Понеделник', 'default'),
            L('Вторник', 'default'),
            L('Сряда', 'default'),
            L('Четвъртък', 'default'),
            L('Петък', 'default'),
            L('Събота', 'default')
        ],
        inline: true,
        dateFormat: ( L('js_date_format') != 'js_date_format' ? L('js_date_format') : 'dd.mm.yy' ),
        formatDate: L('php_date_format'),
        dayNamesMin: L('Нд Пн Вт Ср Чт Пт Сб', 'default').split(' '),
        closeText: L('Запaзи', 'default'), // Display text for close link
        prevText: L('Назад', 'default'), // Display text for previous month link
        nextText: L('Напред', 'default'), // Display text for next month link
        currentText: L('Днес', 'default'), // Display text for current month link
        firstDay: 1, // The first day of the week, Sun = 0, Mon = 1, ...
        gotoCurrent: true, // When true the current day link moves to the currently selected date instead of today.
        stepMonth: 1, // Set how many months to move when clicking the Previous/Next links.
        currentText: L('Текущо време', 'default'),
        timeText: L('Време', 'default'),
        hourText: L('Час', 'default'),
        minuteText: L('Минути', 'default'),
        secondText: L('Секунди', 'default'),
        showSecond: true,
        timeFormat: 'hh:mm:ss'
    });
};
framework.statesToSave = {};
framework.getStatesToSave = function (asObject) {
    var states = asObject ? framework.statesToSave : JSON.stringify(framework.statesToSave);
    framework.statesToSave = {};
    return states;
};

jQuery(document).ready(function () {
    framework.lastActiveElement = null;
    jQuery(document).bind('mousedown.activeelement', function (e) {
        framework.lastActiveElement = e.target;
    });
    jQuery(document).bind('keydown.keyshortcuts keyup.shortcuts', function (e) {
        //find target form
        if(!framework.lastActiveElement) return;
        var jFrame = jQuery(framework.lastActiveElement).parents('.panel-frame:first');
        if(!jFrame.length) return;
        var frame = jFrame[0];

        //console.log([e.type,e.ctrlKey,e.shiftKey,e.altKey]);
        if(e.type == 'keydown') {
            if(e.ctrlKey) jFrame.addClass('kd-ctrl');
            if(e.altKey) jFrame.addClass('kd-alt');
            if(e.shiftKey) jFrame.addClass('kd-shift');
        } else {
            if(!e.ctrlKey) jFrame.removeClass('kd-ctrl');
            if(!e.altKey) jFrame.removeClass('kd-alt');
            if(!e.shiftKey) jFrame.removeClass('kd-shift');
            return;
        }

        var character = String.fromCharCode(e.keyCode).toUpperCase();
        var code = e.keyCode;

        if(code == 188) character = ","; //If the user presses , when the type is onkeydown
        if(code == 190) character = "."; //If the user presses , when the type is onkeydown
        if(code == 192) character = "`"; //If the user presses , when the type is onkeydown

        //Work around for stupid Shift key bug created by using lowercase - as a result the shift+num combination was broken
        var shift_nums = {
            "`": "~",
            "1": "!",
            "2": "@",
            "3": "#",
            "4": "$",
            "5": "%",
            "6": "^",
            "7": "&",
            "8": "*",
            "9": "(",
            "0": ")",
            "-": "_",
            "=": "+",
            ";": ":",
            "'": "\"",
            ",": "<",
            ".": ">",
            "/": "?",
            "\\": "|"
        };
        //Special Keys - and their codes
        var special_keys = {
            '27': 'Esc',
            '9': 'Tab',
            '32': 'Space',
            '13': 'Enter',
            '8': 'Backspace',

            '145': 'ScrollLock',
            '20': 'CapsLock',
            '144': 'NumLock',

            '19': 'Break',

            '45': 'Insert',
            '36': 'Home',
            '46': 'Delete',
            '35': 'End',

            '33': 'PageUp',
            '34': 'PageDown',

            '37': 'Left',
            '38': 'Up',
            '39': 'Right',
            '40': 'Down',

            '112': 'F1',
            '113': 'F2',
            '114': 'F3',
            '115': 'F4',
            '116': 'F5',
            '117': 'F6',
            '118': 'F7',
            '119': 'F8',
            '120': 'F9',
            '121': 'F10',
            '122': 'F11',
            '123': 'F12',

            '219': '[',
            '220': '\\',
            '221': ']',
            '191': '/',
            '111': '/',
            '106': '*',
            '109': '-',
            '107': '+',

            '97': '1',
            '98': '2',
            '99': '3',
            '100': '4',
            '101': '5',
            '102': '6',
            '103': '7',
            '104': '8',
            '105': '9'
        };

        var result = [];

        if(e.ctrlKey) result.push('Ctrl');
        if(e.altKey) result.push('Alt');
        if(e.shiftKey) result.push('Shift');

        if(special_keys[code]) result.push(special_keys[code]);
        else if(shift_nums[character]) result.push(character);
        else if(code != 16 && code != 17 && code != 18) result.push(character);

        var res = result.join('+');
        if(frame._listeners && frame._listeners[res] && frame._listeners[res].length) {
            frame.callListeners(res, [e]);
            e.preventDefault();
        }
    });
});

framework.initSuggestField = function(el){

    if(el.getAttribute('suggest') && el.getAttribute('suggest') != 'null') {

        var options = {
            serviceUrl: 'suggest.php',
            params: {},
            refFields: {},
            onSuggest: {}
        };

        var arr = el.getAttribute('suggest').split(';');

        for (var i = 0; i < arr.length; i++) {
            var optionElement = jQuery.trim(arr[i]).split(':');

            if(optionElement[1]) {
                optionElement[1] = jQuery.trim(optionElement[1]);

                if(optionElement[1][0] == '$') {
                    var refFiled = optionElement[1].substr(1).split('.');

                    var fieldName = jQuery.trim(refFiled[0]);

                    if(el.parentForm.fields[fieldName]) {

                        var key = jQuery.trim(refFiled[1]) ? jQuery.trim(refFiled[1]) : '';

                        options.refFields[optionElement[0]] =
                        {
                            field: el.parentForm.fields[fieldName],
                            fieldName: fieldName,
                            key: key
                        }
                    }
                } else {
                    options.params[optionElement[0]] = optionElement[1];
                }
            }
        }

        if(el.getAttribute('onsuggest')) {
            var arr = el.getAttribute('onsuggest').split(';');

            for (var i = 0; i < arr.length; i++) {
                var optionElement = jQuery.trim(arr[i]).split(':');

                optionElement[0] = jQuery.trim(optionElement[0]);

                if(optionElement[1]) {
                    optionElement[1] = jQuery.trim(optionElement[1]);

                    if(el.parentForm.fields[optionElement[0]])
                        options.onSuggest[optionElement[0]] = optionElement[1];
                }
            }
        }
        el._autoComplete = jQuery(el).autocomplete(options);
    }
    el.getValue = function () {
        var result;
        if(!this._value || this.value != this._value.value) {
            result = {input_value: this.value}
        } else {
            result = this._value;
            result.input_value = this.value;
        }
        if(result.input_value === 'null') result.input_value = '';
        return result;
    };
    el.setValue = function (value) {
        this._value = value;

        if(typeof(value) != 'object')
            this.value = value;

        else if(value.value != undefined)
            this.value = empty(value.value) && value.value !== 0 ? '' : value.value;
        else if(value.input_value != undefined)
            this.value = empty(value.input_value) && value.input_value !== 0 ? '' : value.input_value;
    };
    try {
        el.setValue(JSON.parse(el.getAttribute('init_value')));
    } catch (e) {
    }
};

framework.getDateFormat = function () {
    var format = L('js_date_format', 'default');
    if(format == 'js_date_format') format = "dd.mm.yy";
    return format;
};
framework.getDateTimeFormat = function () {
    var format = L('js_date_time_format', 'default');
    if(format == 'js_date_time_format') format = "dd.mm.yy hh:ii:ss";
    return format;
};