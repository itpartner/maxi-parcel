layout = {};
layout.initMenu = function() {
    if(!layout.menu || !layout.menu.list) return;
    var i;
    layout.menu.tree = [];
    layout.menu.byFileName = {};
    for(i in layout.menu.list) {
        layout.menu.list[i].children = [];
        if(layout.menu.list[i].target_type == 'file' && !empty(layout.menu.list[i].target)) layout.menu.byFileName[layout.menu.list[i].target] = layout.menu.list[i];
    }
    for(i in layout.menu.list) {
        if(empty(layout.menu.list[i].id_parent)) {
            layout.menu.tree.push(layout.menu.list[i]);
        } else if(!empty(layout.menu.list[layout.menu.list[i].id_parent])) {
            layout.menu.list[layout.menu.list[i].id_parent].children.push(layout.menu.list[i]);
        }
    }

    var sortMenuTree = function(menu) {
        menu.sort(function(a,b){return a.menu_order - b.menu_order});
        for(var i=0; i < menu.length; i++) sortMenuTree(menu[i].children);
    };
    sortMenuTree(layout.menu.tree);

    //create html
    layout.menu.jMainUl = jQuery('<ul class="sf-menu align-left"></ul>');
    var createMenuHtml = function(menu,jUl) {
        menu.forEach(function(item){
            if(empty(item.name)) return;
            var anchorAttr = {};
            anchorAttr.type = item.target_type;
            anchorAttr.tabindex = '-1';
            if(item.tooltip) anchorAttr.tooltip = item.tooltip;
            if(item.target_type == 'file') {
                anchorAttr.href = BASE_URL+'/?target='+item.target;
                anchorAttr.filename = item.target;
            } else if(item.target_type == 'link') {
                anchorAttr.href = item.target;
                anchorAttr.target = '_blank';
            } else {
                if(empty(item.children)) return;
                anchorAttr.href = 'javascript:void(0);'
            }
            var jLi = jQuery('<li></li>').appendTo(jUl);
            var jAnchor = jQuery('<a></a>').attr(anchorAttr).text(item.name).addClass('main-menu-link').appendTo(jLi);
            if(item.icon) {
                jUl.addClass('with-icons');
                jAnchor.css({backgroundImage:'url("'+(item.icon.indexOf('http://') === 0 ? item.icon : BASE_URL+'/images/'+item.icon)+'")'});
            }
            if(item.children.length) {
                var jChildUl = jQuery('<ul></ul>').appendTo(jLi);
                createMenuHtml(item.children,jChildUl);
            }
        });
    };
    createMenuHtml(layout.menu.tree,layout.menu.jMainUl);
    layout.menu.jMainUl.prependTo('#top');
    layout.menu.jMainUl.find('a[tooltip]').basicToolTip();
    jQuery('#top ul.sf-menu').supersubs({
        minWidth:    10,   // minimum width of sub-menus in em units
        maxWidth:    30,   // maximum width of sub-menus in em units
        extraWidth:  2     // extra width can ensure lines don't sometimes turn over due to slight rounding differences and font-family
    }).superfish();
    layout.menu.jMainUl.delegate('a','click',function(e) {
        var jAnchor = jQuery(this);
        var type = jAnchor.attr('type');
        if(type == 'file') {
            var fileName = jAnchor.attr('filename');
            if(empty(fileName)) {
                e.preventDefault();
                return;
            }
            layout.menu.jMainUl.hideSuperfishUl();
            jQuery('#content').focus();
            if(e.altKey || e.ctrlKey || e.shiftKey) return;
            e.preventDefault();
            layout.loadContent({
                tplName:fileName
            });
        }
    });
};

layout.initLayout = function() {
    layout.initMenu();

    //logout btn
    jQuery('#top .toolbar').append(jQuery(
        '<div id="user_pass_change" style="padding-right: 5px;">' + currentUser.displayName + '</div>' +
        '<button id="btn-logout" class="align-right" tabindex="-1">'+
        '<span class="icon fa fa-sign-out"></span>'+
        '<span>'+L('Изход','default')+'</span>'+
        '</button>'
    ));
    jQuery('#btn-logout').click(function(){
        var url = BASE_URL+"/login.php";
        (SSLLogin) && (url= url.replace("http://","https://"));
        jQuery(window).trigger('unload');
        jQuery(window).unbind('unload');
        window.location.href = url + "?logout=true";
    });

    if(Server.getParams['direct']) return;

    layout.loadContent({
        tplName: Server.getParams.target
    });
};

layout.contentCache = {};
layout.currentTplName = '';

layout.loadContent = function(config,target) {
    config = config || {};
    config.closable = false;
    if(!config.tplName && !config.content) return false;
    var baseParams ;
    if (config.baseParams) {
        baseParams = config.baseParams;
        delete config.baseParams;
    } else {
        baseParams = {};
    }

    var prefix = 'main-content-'+framework.window.prefixCounter++;
    var contentWrapper = document.createElement('div');
    var jContentWrapper = jQuery(contentWrapper);
    contentWrapper.className = 'content-wrapper';

    var show = function (fn) {
        var rootEl = document.getElementById(prefix) || {};

        if(!(rootEl.getAttribute && rootEl.getAttribute('isError'))) {
            try {
                fn.apply({},[rootEl,prefix,baseParams]);
            } catch (err) {
                err.fileName = config.tplName+'.js';
                err.lineNumber = err.lineNumber - 12;//reda na eval-a vuv server.js
                if(typeof(console) != 'undefined' && typeof(console.log) == 'function') {
                    console.log(err);
                }
            }
        }

        if(rootEl.initResponse) rootEl.processResponse(rootEl.initResponse, null, {}, null, config.context);
        if(rootEl.initError) rootEl.setError(rootEl.initError);

        if(typeof(config.callBack) == 'function') config.callBack.call(config.context,rootEl);
        layout.autoGridHeight();

        return rootEl;
    };

    var create = function(content){
        var jWindow = jContentWrapper.find('#'+prefix+'-frame-container');
        var jPanelBody =  jWindow.children().filter('div.panel-frame-body');
        if(jPanelBody[0])framework.lastActiveElement = jPanelBody[0];
        jQuery(target || '#content').html('').append(jContentWrapper);
        jContentWrapper.append(content);
        framework.attachMethods(jContentWrapper);

        var rootEl = document.getElementById(prefix);

        if (config.content || !rootEl) {
            return show(function(){});
        } else {
            var fn = rootEl.fn;
            if(typeof (fn) == 'function') show(fn);
            else if(typeof(fn) == 'string') {
                framework.addLoadingMask();
                Server.loadFnScript(fn, function(realFn){
                        show(realFn);
                        framework.removeLoadingMask();
                    }, {}, function(){
                        framework.removeLoadingMask();
                    }
                );
            } else {
                show(function(){});
            }
        }
    };

    if (config.content) {
        var frameClasses = ['panel-frame'];
        if(config.collapsible) frameClasses.push('collapsible');
        var frame = document.createElement('div');

        frame.id = prefix+'-frame-container';
        frame.className = frameClasses.join(' ');
        var frameTop = document.createElement('div');
        frameTop.className = 'panel-frame-top';
        if(config.frame === false) frameTop.style.display = 'none';
        var frameTitle = document.createElement('div');
        frameTitle.className = 'panel-frame-title';
        if(config.title) {
            jQuery(frameTitle).text(config.title);
        }

        var frameButtons = document.createElement('div');
        frameButtons.className = 'panel-frame-buttons';
        frameTop.appendChild(frameButtons);
        frameTop.appendChild(frameTitle);
        if(config.collapsible) {
            var closeBtn = document.createElement('button');
            closeBtn.className = 'collapse';
            closeBtn.innerHTML = '<div class="icon fa fa-caret-up"></div>';
            frameButtons.appendChild(closeBtn);
        }
        if(config.closable) {
            var closeBtn = document.createElement('button');
            closeBtn.className = 'close';
            closeBtn.innerHTML = '<div class="icon fa fa-close"></div>';
            frameButtons.appendChild(closeBtn);
        }


        frame.appendChild(frameTop);
        var frameBody = document.createElement('div');
        frameBody.className = 'panel-frame-body';
        frame.appendChild(frameBody);
        var jContent = jQuery(config.content);
        if(jContent.length) {
            if(jContent.parent().length) {
                frame.originalParent = jContent.parent()[0];
            }
            jQuery(frameBody).append(jContent);
        } else {
            return ;
        }
        return create(frame);
    } else {
        framework.addLoadingMask();
        delete config.parent;
        jQuery.ajax({
            type: 'POST',
            url: BASE_URL + '/load_tpl.php',
            data: {
                file: config.tplName,
                prefix: prefix,
                baseParams: JSON.stringify(baseParams),
                getParams: JSON.stringify(Server.getParams),
                getTranslation: translations[config.tplName] ? '' : 'true',
                formStates:framework.getStatesToSave()
            },
            dataType: 'text',
            error: function(XMLHttpRequest, textStatus, errorThrown){
                framework.alert('Connection error.', 'error')
                framework.removeLoadingMask();
            },
            success: function() {
                framework.removeLoadingMask();
                create.apply(this,arguments);
            },
            timeout: Server.requestTimeout
        });
    }
};

layout.autoGridHeight = function() {
    jQuery('#content .grid-container.auto-vresize').each(function(ii,grid){
        var jGrid = jQuery(grid);
        var headersHeight = 0;
        jGrid.find('.grid-headers-inner').each(function(i,headers){
            var h = jQuery(headers).height();
            if(h > headersHeight) headersHeight = h;
        });
        var topBarHeight =  jGrid.find('> .grid-top-bar').height();
        if(topBarHeight > 0) topBarHeight += 3;
        var bottomBarHeight = jGrid.find('> .grid-bottom-bar').height();
        if(bottomBarHeight > 0) bottomBarHeight +=5;
        jGrid.find('div.grid-data').height(jGrid.height() - (headersHeight + topBarHeight + bottomBarHeight));
    });
};