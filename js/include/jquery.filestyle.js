(function($) {
	$.fn.filestyle = function(options) {
    	var settings = {width : 250};
	            
	    if (options)
			$.extend(settings, options);
	    
		return this.each(function() {
			var self = this;
	        var wrapper = $("<div class='brows_file_image'>").css({
				"width"               : settings.imagewidth  + "px"                     ,
				"height"              : settings.imageheight + "px"                     ,
				"background"          : "url(" + settings.image + "1.png) 0 0 no-repeat",
				"background-position" : "right"                                         ,
				"display"             : "inline"                                        ,
				"position"            : "absolute"                                      ,
				"overflow"            : "hidden"                                        ,
				"margin-left"         : '5px'
	        });
	                        
	        var filename = $('<input class="browsed_files" readonly>').addClass($(self).attr("class")).css({
				"display" : "inline",
				"width"   : (settings.width - (settings.imagewidth + 6)) + "px",
				"height"  : (settings.imageheight - 6) + "px"
		     });
			
			var clearImage = $('<img tooltip="' + L('Премахни избраните файлове') + '" classs="clear_browsed_file" src="images/icons/broom.png" style="' + 
				'cursor: pointer;' +
				'position: relative;' + 
				'left    : -22px;' +
				'top     : ' + ((settings.imageheight / 2) - (16 / 2)) + 'px;' +
			'" />');
			
			$(self).before(filename);
	        $(self).after(clearImage);
			$(self).wrap(wrapper);
			
			$(self).css({
				"position" : "relative",
				"height"   : settings.imageheight + "px",
				"width"    : settings.width + "px",
				"display"  : "inline",
				"cursor"   : "pointer",
				"opacity"  : "0.0"
			});
			
//			if ($.browser.mozilla) {
//				if (/Win/.test(navigator.platform)) {
//					$(self).css("margin-left","-120px");
//				} else {
//					$(self).css("margin-left", "-168px");                    
//				};
//	        } else {
//				$(self).css("margin-left", settings.imagewidth - settings.width + "px");                
//	        };
			
			
			$(self).mouseenter(function() {
				$(self.parentNode).css("background-image", "url(" + settings.image + "2.png)");
			});
			
			$(self).mouseleave(function() {
				$(self.parentNode).css("background-image", "url(" + settings.image + "1.png)");
			});
			
			$(self).mousedown(function() {
				$(self.parentNode).css("background-image", "url(" + settings.image + "3.png)");
			});
			
			$(self).bind("change", function() {
				var sFileForToolTip = '';
				if ($(self)[0].files) {
					var sFiles = '';
					for (var i = 0; i < $(self)[0].files.length; i++) {
						sFiles += ($(self)[0].files[i].fileName || $(self)[0].files[i].name) + '; ';
						sFileForToolTip += ($(self)[0].files[i].fileName || $(self)[0].files[i].name) + ';<br />';
					}
					
					filename.val(sFiles);
				} else {
					var sRemoveFakePath = $(self).val();
					var aRemoveFakePath = sRemoveFakePath.split('\\');
					
					filename.val((aRemoveFakePath[aRemoveFakePath.length - 1]) + ';');
					sFileForToolTip = aRemoveFakePath[aRemoveFakePath.length - 1] + ';';
				}
				
				filename.basicToolTip();
				filename.attr('tooltip', sFileForToolTip);
	        });
			
			$(self).basicToolTip();
			$(self).attr('tooltip', L('Прикачи файл'));
			
			clearImage.basicToolTip();
			
			clearImage.click(
				function() {
					filename.val('');
					$(self)[0].setValue();
				}
			);
	    });
	};  
})(jQuery);
