Server = {};
/**
 * @param {String} name
 * @param {Function} successFn
 */
Server.loadFnScript = function(name,successFn,context,errorFn) {
    if(!name) return;
	context = context || {};
	jQuery.ajax({
		url: BASE_URL +'/src/'+name,
		success: function(data) {
			try {
				eval("var fn = " + data);
				successFn.apply(context, [fn,name]);
			} catch (err) {
				err.fileName = name+'.js';
				err.lineNumber = err.lineNumber - 12;//reda na eval-a vuv server.js
				if(typeof(console) != 'undefined' && typeof(console.log) == 'function') {
					console.log(err);
				}
				if(typeof(errorFn) == 'function') errorFn.apply(context,[name]);
			}
		},
		error:function() {
			if(typeof(errorFn) == 'function') errorFn.apply(context,[name]);
		},
		dataType: 'text'
	});
};
Server.clearRequestBuffer = function() {
	Server.requestBuffer = [];
};
if(!window.BASE_URL) alert('BASE_URL is not defined!.');

Server.requestTimeout = (typeof(REQUEST_TIME_LIMIT) !='undefined' && REQUEST_TIME_LIMIT) ? REQUEST_TIME_LIMIT * 1000 : 60000;

Server.sendRequestWaitTime = 10;
Server.sendRequestWaitTimer = -1;

Server.requestBuffer = [];

Server.call = function (target, params, successFn, context, errorFn, loadingMask, delay, synchronous) {
	var request;
	if(typeof(target)!= 'object' || target === null) {
		request = {
			target:target,
			params:params,
			successFn:successFn,
			context:context,
			errorFn:errorFn,
			loadingMask:loadingMask,
			delay:delay,
			synchronous:synchronous
		}
	} else {
		request = jQuery.extend({}, target);
	}
	request.params = request.params || [];
	request.successFn = request.successFn || function(){};
	request.errorFn = request.errorFn || Server.defaultErrorFn;
	request.context = request.context || {};
	if(request.loadingMask) {
		framework.addLoadingMask();
	}
	Server.requestBuffer.push(request);
	if(Server.sendRequestWaitTimer != -1) {
		clearTimeout(Server.sendRequestWaitTimer);
	}
	if (request.delay === false || request.delay === 0) {
		Server.sendRequests();
	} else if(request.delay === -1) {
		
	} else {
		request.delay = parseInt(request.delay);
		request.delay = request.delay || Server.sendRequestWaitTime;
		Server.sendRequestWaitTimer = setTimeout(function(){
			Server.sendRequests();
		}, request.delay);
	}
};

Server.onSessionExpire = function() {
	document.body.onunload = null;
	jQuery(window).unbind('unload');
	document.location.href = BASE_URL+'/?session_expired';
};

Server.sendRequests = function() {
	var sentRequestsBuffer = Server.requestBuffer;
	Server.requestBuffer = [];
	clearTimeout(Server.sendRequestWaitTimer);
	var requests = [];
	var async = true;
	for(var i=0; i<sentRequestsBuffer.length;i++) {
		requests.push({
			id	   : i,
			method : sentRequestsBuffer[i].target,
			params : sentRequestsBuffer[i].params
		});
		if(sentRequestsBuffer[i].synchronous) {
			debugger;
			async = false;
		}
	}
	jQuery.ajax({
		type:'POST',
		async:async,
		url:BASE_URL + '/rpc.php',
		data:JSON.stringify(requests),
		dataType:'json',
		abort:function(){
			debugger;
		},
		onabort:function(){
			debugger;
		},
		error:function(XMLHttpRequest, textStatus, errorThrown) {
			framework.alert('Connection error.', 'error');
			for (var i = 0; i < sentRequestsBuffer.length; i++) {
				if (sentRequestsBuffer[i].loadingMask) {
					framework.removeLoadingMask();
				}
			}
		},
		success:function(responses) {
			if(!responses || !responses.length) {
			    framework.alert('Connection error.', 'error');
                framework.removeLoadingMask();
            }

            for(var i=0; i<responses.length; i++) {
                if(sentRequestsBuffer[responses[i]['id']].loadingMask) framework.removeLoadingMask();
                try {
					if (responses[i].error) {
						responses[i].error = responses[i].error.data || responses[i].error;
						if(responses[i].error.type == 'EmptySession') {
							Server.onSessionExpire();
							return;
						}
						sentRequestsBuffer[responses[i]['id']].errorFn.apply(sentRequestsBuffer[responses[i]['id']].context, [responses[i].error]);
					}
					else {
						sentRequestsBuffer[responses[i]['id']].successFn.apply(sentRequestsBuffer[responses[i]['id']].context, [responses[i].result]);
					}
				} catch (err) {
					if(typeof(console) != 'undefined' && typeof(console.log) == 'function') {
						console.log(err);
					}
				}
			}
		},
		timeout: Server.requestTimeout
	});
};

Server._createErrorHTML = function(error) {
	var jLi = jQuery('<li></li>').html(error.message);
    if(error['log_id'] && error['has_logs_access']){
        jLi.toggleClass('clickable');
        jLi.click(function(){
            framework.createWindow({tplName:'main/logs/SystemLog', baseParams:{error:error}});
        });
    }
	if(error.inner && error.inner.length) {
		var jUl = jQuery('<ul></ul>').appendTo(jLi);
		for(var i=0; i < error.inner.length; i++) jUl.append(Server._createErrorHTML(error.inner[i]));
	}
	return jLi;
};

Server.createErrorHTML = function(errors) {
	var jUl = jQuery('<ul></ul>').addClass('error-msg');
	for(var i=0; i < errors.length; i++) jUl.append(Server._createErrorHTML(errors[i]));
	return jUl;
};

Server.defaultErrorFn = function(error, callBack, context) {
	if(error.type == 'EmptySession') {
		Server.onSessionExpire();
		return;
	};
	framework.messageBox({
        title:empty(error.log_id) ? '' : error.log_id,
		msg:Server.createErrorHTML([error]),
		type:'error',
		callBack:function(){
			if(typeof(callBack)=='function')callBack.apply(context,[error])
		}
	});
};


Server.getParams = {};
