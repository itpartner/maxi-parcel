(function(jQuery) {
	jQuery.fn.basicToolTip = function(options) {
		if(options === 'remove') {
			return this.each(function() {
				jQuery.fn.basicToolTip.hideFn();
				jQuery(this).unbind('mouseenter.tooltip');
				jQuery(this).unbind('mouseleave.tooltip');
				this.__basic_tool_tip_init = false;
			});
		}
	    options = jQuery.extend({}, jQuery.fn.basicToolTip.defaults, options);
	    return this.each(function() {            
			var jEl = jQuery(this);
			var el = this;
			el.__tooltip_original_title = el.title;
			el.title = ''; 
			if(el.__basic_tool_tip_init) return;
			el.__basic_tool_tip_init = true;
			jEl.bind('mouseenter.tooltip', function(event) {
				if(!jEl.attr(options.contentAttribute)) return;
				jQuery.fn.basicToolTip.showFn = function() {
					jQuery.fn.basicToolTip.tipElement[0].style.top = 0;
					jQuery.fn.basicToolTip.tipElement[0].style.left = 0;
					jQuery.fn.basicToolTip.tipElement.html(jEl.attr(options.contentAttribute));
					jQuery.fn.basicToolTip.tipElement.css('visibility','hidden');
					jQuery.fn.basicToolTip.tipElement.css('display','inline-block');
					if((jQuery.fn.basicToolTip.mouseX + 15 + jQuery.fn.basicToolTip.tipElement.width()) > jQuery(window).width()) {
						var x = jQuery(window).width() - jQuery.fn.basicToolTip.tipElement.width() - 20;
					} else {
						var x = jQuery.fn.basicToolTip.mouseX + 10;
					}
					
					if((jQuery.fn.basicToolTip.mouseY + 15 + jQuery.fn.basicToolTip.tipElement.height()) > jQuery(window).height()) {
						var y = jQuery.fn.basicToolTip.mouseY - jQuery.fn.basicToolTip.tipElement.height() - 15;
					} else {
						var y = jQuery.fn.basicToolTip.mouseY + 20;
					}
					
					jQuery.fn.basicToolTip.tipElement[0].style.top = y + 'px';
					jQuery.fn.basicToolTip.tipElement[0].style.left = x + 'px';
					jQuery.fn.basicToolTip.tipElement.css('visibility','');
					jQuery.fn.basicToolTip.showTimer = -1;
					jQuery.fn.basicToolTip.showFn = false;
				};
				jQuery.fn.basicToolTip.showTimer = setTimeout(jQuery.fn.basicToolTip.showFn, jQuery.fn.basicToolTip.showDelay);
			});
			jEl.bind('mouseleave.tooltip', jQuery.fn.basicToolTip.hideFn);
		
	    });
	    
	};
	jQuery.fn.basicToolTip.hideFn = function(){
		if (jQuery.fn.basicToolTip.showTimer != -1) {
			clearTimeout(jQuery.fn.basicToolTip.showTimer);
		}
		jQuery.fn.basicToolTip.showTimer = -1;
		jQuery.fn.basicToolTip.showFn = false;
		jQuery.fn.basicToolTip.tipElement.css('display', 'none');
	};
	jQuery.fn.basicToolTip.showDelay = 500;
	jQuery.fn.basicToolTip.showTimer = -1;
	jQuery.fn.basicToolTip.showFn = false;
	
	jQuery(document).ready(function(){
		var	parent = jQuery(document.body);
		var tipDiv = document.createElement('div');
		tipDiv.className = 'basic-tooltip';
		parent.append(tipDiv);
		jQuery.fn.basicToolTip.tipElement = jQuery(tipDiv);
		jQuery(document).mousemove(function(event) {
			var x = 5;
			if (jQuery.fn.basicToolTip.showTimer != -1 && jQuery.fn.basicToolTip.showFn) {
				jQuery.fn.basicToolTip.mouseX = event.clientX;
				jQuery.fn.basicToolTip.mouseY = event.clientY;
				clearTimeout(jQuery.fn.basicToolTip.showTimer);
				jQuery.fn.basicToolTip.showTimer = setTimeout(jQuery.fn.basicToolTip.showFn, jQuery.fn.basicToolTip.showDelay);
				//alert('client:'+event.clientX+','+event.clientY + "\n" + 'page:'+event.pageX+','+event.pageY + "\n" + 'screen:'+event.screenX+','+event.screenY)
			}
		});
	});
	jQuery.fn.basicToolTip.defaults = {
		contentAttribute : 'tooltip'
	};
})(jQuery);