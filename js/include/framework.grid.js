framework.grid = {};

framework.grid.headerSortableOffset = -29;

framework.grid.headerUnsortableOffset = 2;

framework.grid.itemRenderers = {};
framework.grid.itemRenderers.string = function(columnCfg,rowData, clickFn) {
    var span = document.createElement('span');
    var jSpan = jQuery(span);
    var classes = clickFn ? ['clickable'] : [''];
    if (columnCfg.dataField && rowData[columnCfg.dataField] !== undefined && rowData[columnCfg.dataField] !== null) {
        jSpan.text(rowData[columnCfg.dataField]);
    }
    if(columnCfg.titleField && rowData[columnCfg.titleField]) {
        span.title = rowData[columnCfg.titleField];
    }
    if(columnCfg.toolTipField && rowData[columnCfg.toolTipField]) {
        span.setAttribute('basictip',rowData[columnCfg.toolTipField]);
    } else if (columnCfg.dataToolTip) {
        span.setAttribute('basictip',columnCfg.dataToolTip);
    }
    if(columnCfg.classNameField && rowData[columnCfg.classNameField]) {
        classes.push(rowData[columnCfg.classNameField]);
    }
    span.className = classes.join(' ');
    return span;
};

framework.grid.itemRenderers.html = function(columnCfg, rowData, clickFn){
    if(rowData[columnCfg.dataField]) {
        return rowData[columnCfg.dataField];
    } else {
        return '';
    }
};

framework.grid.itemRenderers.checkBoxSelection = function(columnCfg, rowData, clickFn){
    return '<input type="checkbox" />';
};
framework.grid.itemRenderers.rowIndex = function(columnCfg, rowData, clickFn){
    var span = document.createElement('span');
    span.innerHTML = intval(rowData['__row_index']) + 1;
    return span;
};

framework.grid.itemRenderers.image = function(columnCfg,rowData, clickFn) {
    var classes = clickFn ? ['clickable'] : [];
    var img = document.createElement('img');
    if(columnCfg.imgField) {
        if (rowData[columnCfg.imgField]) {
            if (rowData[columnCfg.imgField].indexOf('http://') === 0) {
                img.setAttribute('src', rowData[columnCfg.imgField]);
            } else {
                img.setAttribute('src', BASE_URL + '/images/' + rowData[columnCfg.imgField]);
            }
        } else {
            return '';
        }
    } else if(columnCfg.img && rowData[columnCfg.dataField]) {
        if (columnCfg.img.indexOf('http://') === 0) {
            img.setAttribute('src', columnCfg.img);
        } else {
            img.setAttribute('src',  BASE_URL + '/images/' + columnCfg.img);
        }
    }
    else return '';
    if(columnCfg.classNameField && rowData[columnCfg.classNameField]) {
        classes.push(rowData[columnCfg.classNameField]);
    }

    img.className = classes.join(' ');
    if(columnCfg.titleField && rowData[columnCfg.titleField]) {
        img.title = rowData[columnCfg.titleField];
    }
    if(columnCfg.toolTipField && rowData[columnCfg.toolTipField]) {
        img.setAttribute('basictip',rowData[columnCfg.toolTipField]);
    } else if (columnCfg.dataToolTip) {
        img.setAttribute('basictip',columnCfg.dataToolTip);
    }
    return img;
};
framework.grid.itemRenderers.editedBy = function(columnCfg,rowData) {
    var tooltip = '';
    var created = '';
    var updated = '';

    var img = document.createElement('div');

    img.setAttribute('class', 'icon fa fa-user');

    if(columnCfg.createdUserField && rowData[columnCfg.createdUserField]) {
        created = rowData[columnCfg.createdUserField];
    }

    if(columnCfg.createdTimeField && rowData[columnCfg.createdTimeField]) {
        if(created)
        {
            try {
                var date = jQuery.datepicker.parseDate( 'yy-mm-dd hh:ii:ss', rowData[columnCfg.createdTimeField] );
                var format = framework.getDateTimeFormat();

                created += '<br/>'+L('Време', 'default')+': ' + jQuery.datepicker.formatDate( format, date );
            }
            catch(e){
                created += '<br/>'+L('Време', 'default')+': ' + rowData[columnCfg.createdTimeField];
            }
        }
        else
        {
            try {
                var date = jQuery.datepicker.parseDate( 'yy-mm-dd hh:ii:ss', rowData[columnCfg.createdTimeField] );
                var format = framework.getDateTimeFormat();

                created += ' ' + jQuery.datepicker.formatDate( format, date );
            }
            catch(e){
                created = rowData[columnCfg.createdTimeField];
            }
        }
    }

    if(columnCfg.updatedUserField && rowData[columnCfg.updatedUserField]) {
        updated = rowData[columnCfg.updatedUserField];
    }

    if(columnCfg.updatedTimeField && rowData[columnCfg.updatedTimeField]) {
        if(updated)
        {
            try {
                var date = jQuery.datepicker.parseDate( 'yy-mm-dd hh:ii:ss', rowData[columnCfg.updatedTimeField] );
                var format = framework.getDateTimeFormat();

                updated += '<br/>'+L('Време', 'default')+': ' + jQuery.datepicker.formatDate( format, date );
            }
            catch(e){
                updated += '<br/>'+L('Време', 'default')+': ' + rowData[columnCfg.updatedTimeField];
            }
        }
        else
        {
            try {
                var date = jQuery.datepicker.parseDate( 'yy-mm-dd hh:ii:ss', rowData[columnCfg.updatedTimeField] );
                var format = framework.getDateTimeFormat();

                updated += ' ' + jQuery.datepicker.formatDate( format, date );
            }
            catch(e){
                updated = rowData[columnCfg.updatedTimeField];
            }
        }
    }

    if(created)
        tooltip += '<b>'+L('Създал', 'default')+'</b><br/>' + created;

    if(updated)
        tooltip += tooltip != '' ? '<br/><br/>' + '<b>'+L('Редактирал', 'default')+'</b><br/>' + updated : '<b>'+L('Редактирал', 'default')+'</b><br/>' + updated;

    if(tooltip)
        img.setAttribute('basictip', tooltip);

    return img;
};
framework.grid.itemRenderers.button = function(columnCfg,rowData) {
    var btn = document.createElement('button');
    var btnText = '';
    if(columnCfg.text) {
        btnText = columnCfg.text;
    } else if (rowData[columnCfg.dataField]) {
        btnText = rowData[columnCfg.dataField];
    }

    var imgSrc = '';
    if(columnCfg.imgField && rowData[columnCfg.imgField]) {
        imgSrc = rowData[columnCfg.imgField];
    } else if(columnCfg.img) {
        imgSrc = columnCfg.img;
    }

    if(imgSrc) {
        var img = document.createElement('img');
        if (imgSrc.indexOf('http://') === 0) {
            img.src = imgSrc;
        } else {
            img.src = BASE_URL+'/images/'+imgSrc;
        }
        btn.appendChild(img);
    }

    var iconCls = '';
    if(columnCfg.iconCls) iconCls += columnCfg.iconCls;
    if(columnCfg.iconClsField && rowData[columnCfg.iconClsField]) iconCls += ' '+rowData[columnCfg.iconClsField];
    if(iconCls) {
        var iconDiv = document.createElement('div');
        iconDiv.setAttribute('class',iconCls);
        btn.appendChild(iconDiv);
    }

    if(columnCfg.disabledField && !empty(rowData[columnCfg.disabledField])) {
        btn.setAttribute('disabled', rowData[columnCfg.disabledField] ? true : false);
    }
    if (btnText) {
        btn.appendChild(document.createElement('span'));
        jQuery(btn).find('span').text(btnText);
    }
    if(columnCfg.titleField && rowData[columnCfg.titleField]) {
        btn.title = rowData[columnCfg.titleField];
    }
    if(columnCfg.toolTipField && rowData[columnCfg.toolTipField]) {
        btn.setAttribute('basictip',rowData[columnCfg.toolTipField]);
    } else if (columnCfg.dataToolTip) {
        btn.setAttribute('basictip',columnCfg.dataToolTip);
    }
    return btn;
};
framework.grid.itemRenderers.currency = function(columnCfg,rowData,clickFn) {

    var value = "";
    var currency = "";

    if(columnCfg.dataField && rowData)
        value = rowData[columnCfg.dataField];

    if(columnCfg.currencyField && rowData[columnCfg.currencyField])
        currency = rowData[columnCfg.currencyField];
    value = parseFloat(value);
    if(isNaN(value)) return '';
    else value = Math.round(parseFloat(value)*100)/100;

    if(columnCfg.alwaysShowPlus && value > 0) value = '+'+value;

    switch( currency )
    {
        case "€" :
        case "EUR" :
            value =   "€ "+value;
            break;
        case "₤" :
        case "GBP" :
            value =   "₤ "+value;
            break;
        case "$" :
        case "USD" :
            value =   "$ "+value;
            break;
        case "лв" :
        case "BGN" :
            value =   value + ' лв';
            break;
        case "leu" :
        case "RON" :
            value =   value + ' leu';
            break;
        case "Kč" :
        case "CZK" :
            value =   value + ' Kč';
            break;
        case "Ft" :
        case "HUF" :
            value =   value + ' Ft';
            break;
        default:
            value =   value+' '+currency;
    }

    var span = document.createElement('span');
    var jSpan = jQuery(span);
    var classes = clickFn ? ['clickable'] : [''];
    jSpan.text(value);

    if(columnCfg.titleField && rowData[columnCfg.titleField]) {
        span.title = rowData[columnCfg.titleField];
    }
    if(columnCfg.toolTipField && rowData[columnCfg.toolTipField]) {
        span.setAttribute('basictip',rowData[columnCfg.toolTipField]);
    } else if (columnCfg.dataToolTip) {
        span.setAttribute('basictip',columnCfg.dataToolTip);
    }
    if(columnCfg.classNameField && rowData[columnCfg.classNameField]) {
        classes.push(rowData[columnCfg.classNameField]);
    }
    span.className = classes.join(' ');
    return span;
};
framework.grid.itemRenderers.date = function(columnCfg,rowData) {

    framework.initDatePicker();

    var value = '';

    if(columnCfg.dataField && rowData[columnCfg.dataField])
        value = rowData[columnCfg.dataField];

    try {
        var date = jQuery.datepicker.parseDate( 'yy-mm-dd', value );
        var format = framework.getDateFormat();

        return jQuery.datepicker.formatDate( format, date );
    } catch(e) {
        return '';
    }

};
framework.grid.itemRenderers.dateTime = function(columnCfg,rowData) {
    framework.initDatePicker();

    var value = '';

    if(columnCfg.dataField && rowData[columnCfg.dataField])
        value = rowData[columnCfg.dataField];

    try {
        var date = jQuery.datepicker.parseDate( 'yy-mm-dd hh:ii:ss', value );
        var format = framework.getDateTimeFormat();

        return jQuery.datepicker.formatDate( format, date );
    } catch(e) {
        return '';
    }
};
framework.grid.itemRenderers.time = function(columnCfg,rowData) {

    framework.initDatePicker();

    var value = 'dsa';

    if(columnCfg.dataField && rowData[columnCfg.dataField])
        value = rowData[columnCfg.dataField];

    try {
        var date = jQuery.datepicker.parseDate( 'yy-mm-dd hh:ii:ss', value );
        var format = L('js_time_format', 'default');
        if( format == 'js_time_format' )
            format="hh:ii:ss";

        return jQuery.datepicker.formatDate( format, date );
    } catch(e) {
        return '';
    }
};

framework.grid.itemRenderers['enum'] = function(columnCfg,rowData,clickFn) {
    var value = '',el,tooltip,classes = [];
    if(clickFn) classes[classes.length] = 'clickable';
    if(columnCfg.dataField && rowData[columnCfg.dataField])
        value = rowData[columnCfg.dataField];

    if(columnCfg.items[value]) {
        if(columnCfg.items[value].img) {
            el = document.createElement('img');
            if (columnCfg.items[value].img.indexOf('http://') === 0) {
                el.setAttribute('src', columnCfg.items[value].img);
            } else {
                el.setAttribute('src',  BASE_URL + '/images/' + columnCfg.items[value].img);
            }
            tooltip = columnCfg.items[value].text;
        } else if(columnCfg.items[value].iconCls) {
            el = document.createElement('div');
            $(el).attr('style','width:100%;text-align:center;');
            var icon = document.createElement('div');
            $(icon).attr('class', columnCfg.items[value].iconCls);
            if(columnCfg.items[value].text) $(icon).attr('title', columnCfg.items[value].text);
            $(el).append(icon);
        } else if(columnCfg.items[value].text) {
            el = document.createElement('span');
            el.appendChild(document.createTextNode(columnCfg.items[value].text));
        }
    } else {
        return '';
    }
    if(!el) return '';

    if(columnCfg.titleField && rowData[columnCfg.titleField]) {
        el.title = rowData[columnCfg.titleField];
    }
    if(columnCfg.toolTipField && rowData[columnCfg.toolTipField]) tooltip = rowData[columnCfg.toolTipField];
    if(tooltip) el.setAttribute('basictip',tooltip);
    el.setAttribute('class', classes.join(' '));
    return el;
};
framework.grid.itemRenderers['icons'] = function(columnCfg,rowData,clickFn) {
    var value = '', el = null; //,classes = [];
    var classes = clickFn ? ['clickable'] : [''];

    if(columnCfg.dataField && rowData[columnCfg.dataField]) value = rowData[columnCfg.dataField];

    var icons = value.split(',');
    for(var i in icons) {
        var iconName = icons[i.trim()];
        if(iconName == '') continue;

        if(!el){
            el =document.createElement('div');
            $(el).attr('style','width:100%;text-align:center;');
        }

        var icon = document.createElement('div');
        $(icon).attr('class', columnCfg.items[iconName].iconCls);
        if(columnCfg.items[iconName].text) $(icon).attr('title', columnCfg.items[iconName].text);
        $(el).append(icon);
    }

    if(!el) return '';

    if(columnCfg.titleField && rowData[columnCfg.titleField]) {
        el.title = rowData[columnCfg.titleField];
    }

    el.setAttribute('class', classes.join(' '));
    return el;
};


//filters
framework.grid.filterRowRenderers = {};
framework.grid.filterRowRenderers.string = function(grid,columnCfg,value) {
    var row = document.createElement('tr');
    row.className = 'filter-item filter-item-string';
    var nameCell = document.createElement('td');
    nameCell.className = 'name-cell';
    nameCell.innerHTML = columnCfg.columnListText || columnCfg.headerText;
    var cmpTypeCell = document.createElement('td');
    cmpTypeCell.className = 'cmp-type-cell';
    cmpTypeCell.innerHTML = '<select>'
        +'<option selected value="ct">'+L('съдържа','default')+'</option>'
        +'<option value="nct">'+L('не съдържа','default')+'</option>'
        +'<option value="eq">'+L('равно на','default')+'</option>'
        +'<option value="ne">'+L('различно от','default')+'</option>'
        +'<option value="bg">'+L('започва с','default')+'</option>'
        +'<option value="nbg">'+L('не започва с','default')+'</option>'
        +'</select>';
    var valueCell = document.createElement('td');
    valueCell.className = 'value-cell';
    valueCell.innerHTML = '<input type="text"/>';

    row.appendChild(nameCell);
    row.appendChild(cmpTypeCell);
    row.appendChild(valueCell);

    row.getItemValue = function() {
        var jRow = jQuery(row);
        return {
            type : 'string',
            dataField : columnCfg.dataField,
            compareType : jRow.find('td.cmp-type-cell select').val(),
            compareValue : jRow.find('td.value-cell input').val()
        };

    };
    row.setItemValue = function(value) {
        var jRow = jQuery(row);
        if(!value || !value.compareType ) return;
        jRow.find('td.cmp-type-cell select').val(value.compareType);
        jRow.find('td.value-cell input').val(value.compareValue || '')
    };
    row.setItemValue(value);
    return row;
};

framework.grid.filterRowRenderers.__number = function(grid,columnCfg,value,type) {
    var row = document.createElement('tr');
    row.className = 'filter-item filter-item-'+type;
    var nameCell = document.createElement('td');
    nameCell.className = 'name-cell';
    nameCell.innerHTML = columnCfg.columnListText || columnCfg.headerText;
    var cmpTypeCell = document.createElement('td');
    cmpTypeCell.className = 'cmp-type-cell';
    cmpTypeCell.innerHTML = '<select>'
        +'<option selected value="eq">=</option>'
        +'<option value="ne">&ne;</option>'
        +'<option value="gt">&gt;</option>'
        +'<option value="gte">&ge;</option>'
        +'<option value="lt">&lt;</option>'
        +'<option value="lte">&le;</option>'
        +'</select>';
    var valueCell = document.createElement('td');
    valueCell.className = 'value-cell';
    valueCell.innerHTML = '<input type="text"/>';

    row.appendChild(nameCell);
    row.appendChild(cmpTypeCell);
    row.appendChild(valueCell);

    row.getItemValue = function() {
        var jRow = jQuery(row);
        return {
            type : type,
            dataField : columnCfg.dataField,
            compareType : jRow.find('td.cmp-type-cell select').val(),
            compareValue : jRow.find('td.value-cell input').val()
        };

    };
    row.setItemValue = function(value) {
        jRow = jQuery(row);
        if(!value) return;
        if(value.compareType) jRow.find('td.cmp-type-cell select').val(value.compareType);
        if(value.compareValue) jRow.find('td.value-cell input').val(value.compareValue)
    };
    row.setItemValue(value);
    return row;
};
framework.grid.filterRowRenderers['float'] = function(grid,columnCfg,value) {
    return framework.grid.filterRowRenderers.__number(grid,columnCfg,value,'float');
};
framework.grid.filterRowRenderers['integer'] =  function(grid,columnCfg,value) {
    return framework.grid.filterRowRenderers.__number(grid,columnCfg,value,'integer');
};

framework.grid.filterRowRenderers['currency'] =  function(grid,columnCfg,value) {
    return framework.grid.filterRowRenderers.__number(grid,columnCfg,value,'float');
};

framework.grid.filterRowRenderers['date'] = function(grid,columnCfg,value) {
    var row = document.createElement('tr');
    row.className = 'filter-item filter-item-date';
    var nameCell = document.createElement('td');
    nameCell.className = 'name-cell';
    nameCell.innerHTML = columnCfg.columnListText || columnCfg.headerText;
    var cmpTypeCell = document.createElement('td');
    cmpTypeCell.className = 'cmp-type-cell';
    cmpTypeCell.innerHTML = '<select>'
        +'<option selected value="deq">'+L('на','default')+'</option>'
        +'<option value="dbt">'+L('между','default')+'</option>'
        +'<option value="dlt">'+L('преди','default')+'</option>'
        +'<option value="dgt">'+L('след','default')+'</option>'
        +'<option value="dl">'+L('през последните','default')+'</option>'
        +'</select>';
    var valueCell = document.createElement('td');
    valueCell.className = 'value-cell';

    valueCell.innerHTML = '<input type="text" class="interval" style="width:50px;margin-right:5px;" /><select class="interval-type" style="width:auto;">'
        +'<option value="h">'+L('часа')+'</option>'
        +'<option selected value="d">'+L('дни')+'</option>'
        +'<option value="w">'+L('седмици')+'</option>'
        +'<option value="m">'+L('месеца')+'</option>'
        +'</select><input type="text" class="date1" style="width:85px" /><span class="date2" style="margin:0 5px;">'+L('и','default')+'</span><input type="text" class="date2" style="width:85px" />';
    valueCell.style.minWidth = '215px';
    row.appendChild(nameCell);
    row.appendChild(cmpTypeCell);
    row.appendChild(valueCell);

    framework.initDatePicker();
    jQuery(row).find('td.value-cell input.date1, td.value-cell input.date2').datepicker();
    var onChangeCompareType = function() {
        jCmpTypeSelect = jQuery(cmpTypeCell).find('select');
        if(jCmpTypeSelect.val() == 'dbt') {
            jQuery(valueCell).find('.date2,.date1').css({display: ''});
            jQuery(valueCell).find('.interval,.interval-type').css({display: 'none'});
        } else if(jCmpTypeSelect.val() == 'dl'){
            jQuery(valueCell).find('.date2,.date1').css({display: 'none'});
            jQuery(valueCell).find('.interval,.interval-type').css({display: ''});
        } else {
            jQuery(valueCell).find('.date1').css({display: ''});
            jQuery(valueCell).find('.interval,.interval-type,.date2').css({display: 'none'});
        }
    };
    jQuery(row).find('td.cmp-type-cell select').change(onChangeCompareType);
    onChangeCompareType();
    row.getItemValue = function() {
        var jRow = jQuery(row);
        return {
            type : 'date',
            dataField : columnCfg.dataField,
            compareType : jRow.find('td.cmp-type-cell select').val(),
            compareValue : {
                interval:jRow.find('td.value-cell input.interval').val(),
                interval_type:jRow.find('td.value-cell select.interval-type').val(),
                date1:jQuery.datepicker.formatDate('yy-mm-dd', jRow.find('td.value-cell input.date1').datepicker('getDate')),
                date2:jQuery.datepicker.formatDate('yy-mm-dd', jRow.find('td.value-cell input.date2').datepicker('getDate'))
            }
        };
    };
    row.setItemValue = function(value) {
        var jRow = jQuery(row);
        if(!value) return;
        if(value.compareType)jRow.find('td.cmp-type-cell select').val(value.compareType);
        onChangeCompareType();
        if(value.compareValue) {
            jRow.find('td.value-cell input.interval').val(value.compareValue.interval);
            jRow.find('td.value-cell select.interval-type').val(value.compareValue.interval_type);
            jRow.find('td.value-cell input.date1').datepicker( "setDate" , jQuery.datepicker.parseDate('yy-mm-dd', value.compareValue.date1));
            jRow.find('td.value-cell input.date2').datepicker( "setDate" , jQuery.datepicker.parseDate('yy-mm-dd', value.compareValue.date2));
        }
    };
    row.setItemValue(value);
    return row;
};

framework.grid.filterRowRenderers.dateTime = framework.grid.filterRowRenderers['date'];

framework.grid.filterRowRenderers['enum'] = function(grid,columnCfg,value) {
    var row = document.createElement('tr');
    row.className = 'filter-item filter-item-date';
    var nameCell = document.createElement('td');
    nameCell.className = 'name-cell';
    nameCell.innerHTML = columnCfg.columnListText || columnCfg.headerText;
    var cmpTypeCell = document.createElement('td');
    cmpTypeCell.className = 'cmp-type-cell';
    cmpTypeCell.innerHTML = '<select>'
        +'<option selected value="eq">'+L('равно на','default')+'</option>'
        +'<option value="ne">'+L('различно от','default')+'</option>'
        +'<option value="ct">'+L('съдържа','default')+'</option>'
        +'</select>';
    var cmpSelect = cmpTypeCell.firstChild;
    var valueCell = document.createElement('td');
    valueCell.className = 'value-cell';
    var select = document.createElement('select');
    var option;
    for(var val in columnCfg.items) {
        option = document.createElement('option');
        option.value = val;
        jQuery(option).text(columnCfg.items[val].text.replace(/(<.*?>)/ig,"") || val);
        jQuery(select).append(option);
    }
    valueCell.appendChild(select);

    var inp = document.createElement('input');
    valueCell.appendChild(inp);

    jQuery([inp,select]).css({minWidth:'150px',boxSizing:'border-box'});

    row.appendChild(nameCell);
    row.appendChild(cmpTypeCell);
    row.appendChild(valueCell);

    var onChangeCompareType = function() {
        if(cmpSelect.value == 'ct') {
            jQuery(valueCell).find('select').css({display: 'none'});
            jQuery(valueCell).find('input').css({display: ''});
        } else {
            jQuery(valueCell).find('select').css({display: ''});
            jQuery(valueCell).find('input').css({display: 'none'});
        }
    };

    jQuery(row).find('td.cmp-type-cell select').change(onChangeCompareType);
    onChangeCompareType();

    row.getItemValue = function() {
        var jRow = jQuery(row);
        var val, cmpType = jRow.find('td.cmp-type-cell select').val();
        if(cmpType == 'ct') val = jRow.find('td.value-cell input').val();
        else val = jRow.find('td.value-cell select').val();
        return {
            type : 'enum',
            dataField : columnCfg.dataField,
            compareType : cmpType,
            compareValue : val
        };
    };
    row.setItemValue = function(value) {
        var jRow = jQuery(row);
        if(!value) return;
        if(value.compareType)jRow.find('td.cmp-type-cell select').val(value.compareType);
        if(value.compareValue) {
            if(value.compareType == 'ct') jRow.find('td.value-cell input').val(value.compareValue);
            else jRow.find('td.value-cell select').val(value.compareValue);
        }

        onChangeCompareType();
    };
    row.setItemValue(value);
    return row;
};



//quickFilter
framework.grid.showQuickFilter = function(th,grid) {
    var jTH = jQuery(th);
    var jFilterDiv = jQuery('<div class="quick-filter"><table><tbody></tbody></table></div>');
    jFilterDiv.css({visibility:'hidden'});
    jFilterDiv.appendTo(jQuery(grid).find('.grid-headers-outer'));
    var hideTimeout = -1;
    jTH.bind('mouseleave.quickFilter',function(){
        hideTimeout = setTimeout(function(){
            jFilterDiv.remove();

        },500);
    });


    //create filter
    var jFilterTR = jQuery(framework.grid.filterRowRenderers[th.columnCfg.type](grid,th.columnCfg,th.quickFilter));

    jFilterTR.prepend('<td class=""><div class="icon fa fa-filter"></div></td>');
    jFilterTR.append('<td class="confirm"><button><div class="icon fa fa-check"></div></button></td>');
    jFilterTR.append('<td class="remove"><button><div class="icon fa fa-close"></div></button></td>');
    jFilterDiv.find('tbody').append(jFilterTR);

    jFilterDiv.click(function(event){
        event.stopPropagation();
    });
    jFilterDiv.find('td.remove button').click(function(){
        var reload = jTH[0].quickFilter;
        jTH[0].quickFilter = false;
        jTH.removeClass('filtered');
        jFilterDiv.remove();
        grid.quickFilterVisible = false;
        if(reload) grid.loadData();
    });
    var onConfirm = function(){
        jTH[0].quickFilter = jFilterTR[0].getItemValue();
        jTH.addClass('filtered');
        jFilterDiv.remove();
        grid.quickFilterVisible = false;
        grid.loadData();
    };
    jFilterDiv.find('td.value-cell').delegate('input','keypress',function(e){
        this.onPressEnterFn = onConfirm;
        framework.elementMethods.onInputKeyPressCheckEnter.apply(this,arguments);
    });
    jFilterDiv.find('td.confirm button').click(onConfirm);

    //ot koq strana ?!
    if(jTH.offset().left + jFilterDiv.outerWidth() > jQuery(window).width()) {
        var x = jQuery(window).width() - jTH.offset().left + jTH.position().left - jFilterDiv.outerWidth() - 25;
    } else {
        var x = jTH.position().left;
    }

    var y = jTH.position().top + jTH.outerHeight();

    jFilterDiv.css({top: y+'px', left: x+ 'px', visibility:''});
    jFilterDiv.find('td.value-cell input:visible:first').focus();

    jFilterDiv.bind('mouseenter',function(){
        grid.quickFilterVisible = true;
        clearTimeout(hideTimeout);
        jTH.unbind('mouseleave.quickFilter');
        jQuery(document).bind('click.quickFilter',function(){
            jQuery(document).unbind('click.quickFilter');
            jFilterDiv.remove();
            grid.quickFilterVisible = false;
        });
    });
};




