function empty(p) {return p==0 || !p || (typeof(p) == 'object' && jQuery.isEmptyObject(p));}

if (!String.prototype.quote) {
    String.prototype.quote = function () {
        var c, i, l = this.length, o = '"';
        for (i = 0; i < l; i += 1) {
            c = this.charAt(i);
            if (c >= ' ') {
                if (c === '\\' || c === '"') {
                    o += '\\';
                }
                o += c;
            } else {
                switch (c) {
                case '\b':
                    o += '\\b';
                    break;
                case '\f':
                    o += '\\f';
                    break;
                case '\n':
                    o += '\\n';
                    break;
                case '\r':
                    o += '\\r';
                    break;
                case '\t':
                    o += '\\t';
                    break;
                default:
                    c = c.charCodeAt();
                    o += '\\u00' + Math.floor(c / 16).toString(16) +
                        (c % 16).toString(16);
                }
            }
        }
        return o + '"';
    };
}

if (!String.prototype.supplant) {
	String.prototype.supplant = function (o) {
		return this.replace(/{([^{}]*)}/g,
			function (a, b) {
				var r = o[b];
				return typeof r === 'string' || typeof r === 'number' ? r : a;
			}
		);
	};
}

if (!String.prototype.trim) {
	String.prototype.trim = function () {
		return this.replace(/^\s*(\S*(?:\s+\S+)*)\s*$/, "$1");
	};
}

if(!Array.indexOf){
    Array.prototype.indexOf = function(obj){
        for(var i=0; i<this.length; i++){
            if(this[i]==obj){
                return i;
            }
        }
        return -1;
    }
}
if (!Array.prototype.forEach) {
	Array.prototype.forEach = function(fun /*, thisp */) {
		"use strict";

		if (this === void 0 || this === null) throw new TypeError();

		var t = Object(this);
		var len = t.length >>> 0;
		if (typeof fun !== "function") throw new TypeError();

		var thisp = arguments[1];
		for (var i = 0; i < len; i++) {
			if (i in t) fun.call(thisp, t[i], i, t);
		}
	};
}
if (!Array.prototype.map) {
	Array.prototype.map = function(fun /*, thisp */) {
		"use strict";

		if (this === void 0 || this === null) throw new TypeError();

		var t = Object(this);
		var len = t.length >>> 0;
		if (typeof fun !== "function") throw new TypeError();

		var res = new Array(len);
		var thisp = arguments[1];
		for (var i = 0; i < len; i++) {
			if (i in t) res[i] = fun.call(thisp, t[i], i, t);
		}

		return res;
	};
}
Util = {};
getById = function (id) {
	return document.getElementById(id);
};
Util.trim = function (stringToTrim) {
	if(!stringToTrim) return '';
	return stringToTrim.toString().replace(/^\s+|\s+$/g,"");
};
Util.ltrim = function(stringToTrim) {
	if(!stringToTrim) return '';
	return stringToTrim.toString().replace(/^\s+/,"");
};
Util.rtrim = function(stringToTrim) {
	if(!stringToTrim) return '';
	return stringToTrim.toString().replace(/\s+$/,"");
};

Util.getStyleRules = function (id) {
	var el = document.getElementById(id);
	if(!el || el.nodeName != 'STYLE') return false;
	var rules = false;
	if(el.styleSheet) {
		if(el.styleSheet.rules) {
			rules = el.styleSheet.rules;
		} else {
			rules = el.styleSheet.cssRules;
		}

	} else {
		if(el.sheet.rules) {
			rules = el.sheet.rules;
		} else {
			rules = el.sheet.cssRules;
		}
	}
	return rules;
};
Util.getStyleRule = function(id, selector) {
	if(typeof(selector)!='string') return false;
	selector = Util.trim(selector);
	if(!selector) return false;

	var rules = Util.getStyleRules(id);
	if(!rules) return false;

	for(var j=0; j< rules.length; j++) {
		if (rules[j].selectorText.toLowerCase() == selector.toLowerCase()) {
			return rules[j].style;
		}
	}
	return false;
};
Util.createStyleRule = function(id,selector) {
	if(typeof(selector)!='string') return false;
	selector = Util.trim(selector);
	if(!selector) return null;

	var rule = Util.getStyleRule(id,selector);
	if(rule) return rule;

	var styleTag = document.getElementById(id);
	if(!styleTag) return null;
	var styleSheet = styleTag.styleSheet || styleTag.sheet;

	if (styleSheet.addRule) {
		styleSheet.addRule(selector, null,0); //IE
	} else {
		styleSheet.insertRule(selector+' { }', 0);
	}
	return Util.getStyleRule(id,selector);
};

Util.deleteStyleRule = function (id,selector) {
	selector = Util.trim(selector);
	if(!selector) return false;
	var styleTag = document.getElementById(id);
	if(!styleTag) return;
	var styleSheet = styleTag.styleSheet || styleTag.sheet;
	var rules = styleSheet.cssRules || styleSheet.rules;

	for (var j = 0; j < rules.length; j++) {
		if (rules[j].selectorText == selector) {
			if (styleSheet.deleteRule) {
				styleSheet.deleteRule(j);
			}
			else {
				styleSheet.removeRule(j);
			}
		}
	}
};



/**
 * Copies all the properties of source to target.
 * @param {Object} target The receiver of the properties
 * @param {Object} source The source of the properties
 * @param {Object} defaults A different object that will also be applied for default values
 * @return {Object} returns obj
 */
Util.apply = function(target, source, defaults) {
    // no "this" reference for friendly out of scope calls
    if(defaults){
       Util.apply(target, defaults);
    }
    if(target && source && typeof source == 'object'){
        for(var p in source){
            target[p] = source[p];
        }
    }
    return target;
};

/**
 * Copies all the properties of source to target if they don't already exist.
 * @param {Object} target The receiver of the properties
 * @param {Object} source The source of the properties
 * @return {Object} returns obj
 */
Util.applyIf = function(target, source){
    if(target){
        for(var p in source){
            if(typeof(target[p]) !== 'undefined') {
                target[p] = source[p];
            }
        }
    }
    return target;
};

Util.openWithPostParams = function(url, params, target, windowParams, useGet) {
	var form = document.createElement('form');
	if(!url) return false;
	params = params || {};
	for(var i in params) {
		var inp = document.createElement('input');
		inp.type = 'hidden';
		inp.name = i;
		inp.value = params[i];
		form.appendChild(inp);
	}

	form.method = empty(useGet) ? 'POST' : 'GET';

	if(url.indexOf('?') == -1) {
		url = url + '?__random_param='+Math.random();
	} else {
		url = url + '&__random_param='+Math.random();
	}
	form.action = url;

	form.style.display = 'none';
	document.body.appendChild(form);
	if(target && target == 'window') {
		windowParams = windowParams || 'width=500,height=400,resizeable,scrollbars';
		var windowName = 'wnd-'+Math.floor(Math.random()*3241234441414141);
		var wnd = window.open('', windowName, windowParams);
		form.target = windowName;
	} else if ( typeof(target) == 'string' && target ) {
		var wnd = false;
		form.target = target;
	} else {
		var wnd = false;
		form.target = '_blank';
	}

	form.submit();
	document.body.removeChild(form);
	return wnd;
};

Util.mysqlDateTimeToJS = function( timestamp ){
	if(!timestamp) return false;
	var regex=/^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9]) (?:([0-2][0-9]):([0-5][0-9]):([0-5][0-9]))?$/;
	var parts=timestamp.replace(regex,"$1 $2 $3 $4 $5 $6").split(' ');

	if( parseInt(parts[0]) > 0 )
		return new Date(parts[0],parts[1]-1,parts[2],parts[3],parts[4],parts[5]);

	return false;

};

Util.formatDate = function( timestamp, mask ){
	if(!timestamp) return '';
	var hours 		= timestamp.getHours().toString();
	var minutes		= timestamp.getMinutes().toString();
	var secounds 	= timestamp.getSeconds().toString();

	var day 		= timestamp.getDate().toString();
	var month 		= (timestamp.getMonth()+1).toString();
	var year 		= timestamp.getFullYear().toString();

	return mask.replace('H', strLeft(hours, 2, '0')).replace('h', strLeft(hours, 2, '0')).replace('I', strLeft(minutes, 2, '0')).replace('i', strLeft(minutes, 2, '0')).replace('s',strLeft(secounds, 2, '0')).replace('S',strLeft(secounds, 2, '0')).replace('d',strLeft(day, 2, '0')).replace('D',strLeft(day, 2, '0')).replace('m',month).replace('M',strLeft(month, 2, '0')).replace('Y',year).replace('y',year.substr(-2));
};

Util.jsTimeStampDiffToString = function(stamp, short) {
	if(stamp == 0) return short ? '00:00' : '00:00:00';
	var minusSign = stamp < 0 ? '- ' : '';
	stamp = Math.abs(stamp);

	if(short) {
		stamp = ((stamp / 1000)/60).toFixed(0) * 60 * 1000;
	}

	return minusSign+strLeft((Math.floor((stamp)/3600000))+'',2,'0') + ':' + strLeft((Math.floor((stamp)/60000)%60)+'',2,'0') + ( !short ?  (':' + strLeft((Math.floor((stamp)/1000%60))+'',2,'0')) : '');
};

function strLeft( str, length, chr ){
	while( str.length<length )
		str = chr + str;

	return str;
}

function intval(val) {
	val = parseInt(val,10);
	return val ? val : 0;
}
function floatVal(val) {
	val = parseFloat(val);
	return val ? val : 0;
}

Util.createCookie = function(bRemember,userName,userPass) {
	var date = new Date();
	var val = $.rc4EncryptStr("userlogin:"+userName+"/userpass:"+userPass+"/chkRemember:true","stronghold");
    date.setTime(date.getTime()+(30*24*60*60*1000));
	$.cookie('eerem', val, {expires: 30, path: '/'});
};

Util.readCookie = function() {
	if (!$.cookie('eerem')) return;
	var _a = $.rc4DecryptStr($.cookie('eerem'),"stronghold").split('/');
	var oData={};
	for (var i=0;i<_a.length;i++) {
		var _k = _a[i].split(':');
		oData[_k[0]]=_k[1];
	}
	return oData;
};

Util.deleteCookie = function(){
	$.cookie('eerem', null, {path: '/'});
};

Util.setCookie = function (name,val,days) {
    if(!name || !val ) return;
    jQuery.cookie(name,val,{expires:days});
};
Util.getCookie = function(name) {
    return jQuery.cookie(name);
};

//login notifications
Util.loginNotifications = {};
Util.loginNotifications.show = function(notifications) {
	var notificationsByType = {};
	if(notifications && notifications.length) notifications.forEach(function(item) {
		if(!item.type) return;
		if(!notificationsByType[item.type]) notificationsByType[item.type] = [];
		notificationsByType[item.type][notificationsByType[item.type].length] = item;
	});
	for(var type in notificationsByType) {
		if(typeof(Util.loginNotifications.fns[type]) == 'function') Util.loginNotifications.fns[type](notificationsByType[type]);
	}
};
Util.loginNotifications.fns = {};
Util.loginNotifications.fns.message = function(notifications) {
	if(!notifications || !notifications.length) return;
	var messagesByType = {};
	notifications.forEach(function(item){
		if(!item.data) return;
		if(!item.data.type) item.data.type = 'info';
		if(!messagesByType[item.data.type]) messagesByType[item.data.type] = [];
		messagesByType[item.data.type][messagesByType[item.data.type].length] = item;
	});
};

Util.distanceByGeo = function(lat1,lon1,lat2,lon2) {
    var R = 6378137; // m
    var dLat = (lat2-lat1) / 180 * Math.PI;
    var dLon = (lon2-lon1) / 180 * Math.PI;
    lat1 = lat1 / 180 * Math.PI;
    lat2 = lat2 / 180 * Math.PI;

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    return R * c;
};

Util.BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		//this.OS = this.searchString(this.dataOS) || "an unknown OS";
		this.OS = this.searchOS() || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchOS: function() {
		for (var str in this.osList) {			
			if (navigator.userAgent.search(this.osList[str])>-1) return str;						
		}
		return false;
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	recomandedBrowsers: {"Chrome":7,"Safari":5,"Opera":9,"Firefox":3,"Explorer":8},
	dataBrowser: [
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		},
		{string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari",
			versionSearch: "Version"
		},
		{
			prop: window.opera,
			identity: "Opera"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			   string: navigator.userAgent,
			   subString: "iPhone",
			   identity: "iPhone/iPod"
		},
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	],
	osList : {'Windows 3.11':'Win16',
		'Windows 95' : '(Windows 95)|(Win95)|(Windows_95)',
		'Windows 98' : '(Windows 98)|(Win98)',
		'Windows 2000' : '(Windows NT 5.0)|(Windows 2000)',
		'Windows XP' : '(Windows NT 5.1)|(Windows XP)',
		'Windows Server 2003' : '(Windows NT 5.2)',
		'Windows Vista' : '(Windows NT 6.0)',
		'Windows 7 Home' : '(Windows NT 6.1)',
		'Windows 7' : '(Windows NT 7.0)',
		'Windows NT 4.0' : '(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)',
		'Windows ME' : 'Windows ME',
		'Open BSD' : 'OpenBSD',
		'Sun OS' : 'SunOS',
		'Linux' : '(Linux)|(X11)',
		'Mac OS' : '(Mac_PowerPC)|(Macintosh)'
	}

};

Util.openPrintInvoice = function(num) {
    if (!num) return;
    framework.doExport('printInvoice','pdf', {num:num});
};

/**
 * Връща името на използвания браузър
 * @returns {string} firefox, chrome, explorer, opera, other
 */
Util.detectBrowserType = function() {
    var isOpera = !! window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
    //noinspection JSUnresolvedVariable
	var isFirefox = typeof InstallTrigger !== 'undefined'; // Firefox 1.0+
    var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
    // At least Safari 3+: "[object HTMLElementConstructor]"
    var isChrome = !! window.chrome && !isOpera; // Chrome 1+
    var isIE = /*@cc_on!@*/
        false || !! document.documentMode; // At least IE6
    if(isFirefox)
        return 'firefox';
    else if(isChrome)
        return 'chrome';
    else if(isIE)
        return 'explorer';
    else if(isOpera)
        return 'opera';
    else
        return 'other';
};


(function(jQuery) {
	jQuery.fn.focused = function(options) {
		var focused = false;
		this.each(function(i,el) {
			if(el == document.activeElement) focused = true;
		});
		return focused;
	}
})(jQuery);



