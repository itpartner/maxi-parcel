<?php
require_once 'init.php';

use main\Access;use main\db\DBC;use main\language\Language;use main\Login;use main\logs\Logs;use main\System;

try {

    try{
        define('REVISION', `git rev-parse --short HEAD`);
    } catch (Throwable $e){};

if (!isset($_GET) || $_GET['target'] != 'pallex/client/Calculator')
    Login::check();

System::$aGetParams = $_GET;

if(!empty($_GET) && empty($_GET['direct'])) {
    $_SESSION['_tmp_get_params'] = $_GET;
    header('location: ' . $_SERVER['DOCUMENT_URI']);
    die();
}
if(!empty($_SESSION['_tmp_get_params'])) {
    System::$aGetParams = $_SESSION['_tmp_get_params'];
    unset($_SESSION['_tmp_get_params']);
} else {
    System::$aGetParams = $_GET;
}
?><!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo(L('Макси пратка')); ?></title>
    <link type="text/css" href="css/site.css" rel="stylesheet"/>
    <script type="text/javascript">
        BASE_URL = "<?=BASE_URL?>";
        REQUEST_TIME_LIMIT = <?=REQUEST_TIME_LIMIT?>;
        SSLLogin = "<?=SSL?>";
        LANG = '<?=Language::getCurrentLanguage()?>';
        currentUser = {
            displayName: <?=json_encode(sprintf("%s %s %s [%s]", $_SESSION['userdata']['fname'], $_SESSION['userdata']['mname'], $_SESSION['userdata']['lname'], $_SESSION['userdata']['username']), JSON_UNESCAPED_UNICODE);?>
        };
    </script>
    <script type="text/javascript" src="js/include/jquery.min.js?<?=REVISION;?>"></script>
    <script type="text/javascript" src="js/include/jquery.ui/jquery-ui-1.8.2.custom.min.js?<?=REVISION;?>"></script>
    <script type="text/javascript" src="js/include/jquery.ui/jquery.ui.tabs.override.js?<?=REVISION;?>"></script>
    <script type="text/javascript" src="js/include/Server.js?<?=REVISION;?>"></script>
    <script type="text/javascript" src="js/include/util.js?<?=REVISION;?>"></script>
    <script type="text/javascript" src="js/include/framework.js?<?=REVISION;?>"></script>
    <script type="text/javascript" src="js/include/framework.grid.js?<?=REVISION;?>"></script>
    <script type="text/javascript" src="js/include/framework.validators.js?<?=REVISION;?>"></script>
    <script type="text/javascript" src="js/include/layout.js?<?=REVISION;?>"></script>
    <script type="text/javascript" src="js/include/basicToolTip.js?<?=REVISION;?>"></script>
    <script type="text/javascript" src="js/plugins/autocomplete/jquery.autocomplete.js?<?=REVISION;?>"></script>
    <script type="text/javascript" src="js/include/jquery.ui/jquery.ui.datepicker.js?<?=REVISION;?>"></script>
    <script type="text/javascript" src="js/include/jquery.ui/jquery-ui-timepicker-addon.js?<?=REVISION;?>"></script>
    <script type="text/javascript" src="js/plugins/jsTree/jquery.jstree.js?<?=REVISION;?>"></script>
    <script type="text/javascript" src="js/include/jquery.filestyle.js?<?=REVISION;?>"></script>
    <script type="text/javascript" src="js/plugins/jquery.cookie.js?<?=REVISION;?>"></script>
    <script type="text/javascript" src="js/include/hoverIntent.js?<?=REVISION;?>"></script>
    <script type="text/javascript" src="js/plugins/superfish/js/supersubs.js?<?=REVISION;?>"></script>
    <script type="text/javascript" src="js/plugins/superfish/js/superfish.js?<?=REVISION;?>"></script>
    <script type="text/javascript">
        var target = "<?=System::$aGetParams['target']?>";
        jQuery(document).ready(function () {
            jQuery(window).resize(function () {
                layout.autoGridHeight();
            });
            if(jQuery.browser.opera) jQuery('body').delegate('button', 'mousedown', function (e) {
                e.preventDefault();
            });
            layout.initLayout();
        });

        Server.getParams = <?=json_encode(System::$aGetParams, JSON_UNESCAPED_UNICODE)?>;

        <?php if ($_GET['target'] != 'pallex/client/Calculator') { ?>
        layout.menu = {};
        layout.menu.list = <?=json_encode(array_filter(DBC::$slave->selectAssoc("SELECT m.id as __key, m.* FROM menu m", \main\menu\Menu::$cacheTime), function ($aRow) {
            return Access::hasMenuAccess($aRow);
        }), JSON_UNESCAPED_UNICODE);?>;
        <?php } ?>

        <?php } catch (Exception $e) {
            Logs::logException($e);
            die($e->getMessage());
        } ?>
    </script>
    <style type="text/css">
        body, html {
            margin: 0;
            padding: 0;
            height: 100%;
            width: 100%;
            overflow: hidden;
        }
        .logo {
            height: 600px;
            opacity: 0.8;
            background-size: 800px;
            background-repeat: no-repeat;
            background-position: center;
        }
    </style>
    <?php if ($_GET['target'] == 'pallex/client/Calculator') { ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link type="text/css" rel="stylesheet" href="css/reset.css">
        <link type="text/css" rel="stylesheet" href="css/index.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <?php } ?>
</head>
<body id="body">
<div <?php echo(($_GET['target'] != 'pallex/client/Calculator') ? 'id="cm"' : 'id="maxi"'); ?> >
    <?php if(empty(System::$aGetParams['direct'])): ?>
        <div id="top">
            <div class="toolbar" style="float: right;padding-top: 2px;"></div>
        </div>
        <div id="content" class="main-content">
            <table width="100%" height="100%">
                <tr>
                    <td>
                        <div class="logo"></div>
                    </td>
                </tr>
            </table>
        </div>
    <?php else: require_once 'load_tpl.php'; endif; ?>
</div>
</body>
</html>