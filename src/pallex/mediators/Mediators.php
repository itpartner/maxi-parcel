<?php
namespace pallex\mediators;

use main\db\DBC;
use main\grid\GridPanelHandler;
use main\suggest\Suggest;

class Mediators extends GridPanelHandler {
    public function __construct() {
        $this->oBase = DBC::$slave;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'name',
                'width' => 200,
                'headerText' => L('Наименование'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_FLOAT,
                'dataField' => 'charge',
                'width' => 100,
                'headerText' => L('Надценка лв.'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'email',
                'width' => 200,
                'headerText' => L('Ел. Поща'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'code',
                'width' => 100,
                'headerText' => L('Код'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumnEditedBy()
        );

        $this->sReportSelectStatement = "
            select m.id, m.code, m.name, m.charge, m.email,
            CONCAT_WS(' ',pc.fname,pc.mname,pc.lname) AS created_user,
            CONCAT_WS(' ',pu.fname,pu.mname,pu.lname) AS updated_user, m.created_time, m.updated_time
            FROM mediators m
            LEFT JOIN personnel pc ON pc.id = m.created_user
            LEFT JOIN personnel pu ON pu.id = m.updated_user
        ";

        $this->aReportWhereStatement[] = 'm.to_arc = 0';
        //$this->sReportGroupStatement = '';
        $this->bUseSQLPaging = false;
        $this->aDefaultSort = array(
            array('field' => 'name', 'dir' => 'ASC')
        );

        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
    }

    public function load($aRequest = array(), $aInitParams = array()) {
        return parent::load($aRequest, $aInitParams);
    }
}