<?php
namespace pallex\mediators;

use main\TPLBase;

class TPLEdit extends TPLBase {

    public function __construct($sPrefix, array $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);

        $this->bPrintWithTitle = true;
        $this->aFrameConfig['title'] = empty($aInitParams['row']['id']) ? L('Нов Посредник') : L('Редакция на Посредник:') . ' ' . $aInitParams['row']['name'];
    }

    public function printHtml() { ?>

        <table style="width: 100%">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName'=>'name', 'text' => L('Наименование')));?></td>
                            <td><? $this->printElement('input', array('name'=>'name')); ?></td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName'=>'charge' , 'text' => L('Надценка')));?></td>
                            <td><? $this->printElement('input', array('name'=>'charge', 'keyRestriction' => 'float')); ?></td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName'=>'email' , 'text' => L('Ел. Поща')));?></td>
                            <td><? $this->printElement('input', array('name'=>'email')); ?></td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName'=>'code', 'text' => L('Код')));?></td>
                            <td><? $this->printElement('input', array('name'=>'code')); ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="toolbar align-right" style="margin-top: 5px">
                        <? $this->printElement('lasteditedinfo'); ?>
                        <? $this->printElement('closebutton'); ?>
                        <? $this->printElement('confirmbutton'); ?>
                        <? $this->printElement('button', array('class' => 'delete_metal align-right', 'text' => L('Изтрий'), 'name' => 'delete_metal', 'iconCls' => 'icon fa fa-trash')) ?>
                    </div>
                </td>
            </tr>
        </table>

        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, baseParams) {

            if (baseParams['row']['id'] == 0) {
                $(container).find('.delete_metal').css('display', 'none');
            }

            $(container).find('.confirm').click(function(){
                container.request('save',{id: baseParams['row']['id']},null, function(){
                    $(container).find('.close').click();
                    baseParams.getParentGrid().loadData();
                });
            });

            $(container).find('.delete_metal').click(function() {
                container.request('delete', {id: baseParams['row']['id']}, null, function() {
                    $(container).find('.close').click();
                    baseParams.getParentGrid().loadData();
                });
            });
        });

JS;
    }


}