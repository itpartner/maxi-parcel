<?php
namespace pallex\mediators;

use emc\production\schedule\Scheduler;
use main\db\DBC;
use main\Exception;
use main\form\FormPanelHandler;
use main\InvalidParameter;

/**
 * Class Edit
 * @accessLevelDescription Редактиране на Посредник
 */
class Edit extends FormPanelHandler {

    public function init($aRequest = array(), $aInitParams = array()) {
        foreach ($aInitParams['row'] as $k => $v) $this->setField($k, $v);
        $this->setLastEditInfo($aInitParams['row']);

        return $this->oResponse;
    }

    public function save($aRequest) {
        return DBC::$main->withTrans(function ($aRequest) {
            $metal = $aRequest['fields'];
            $metal['id'] = intval($aRequest['id']);
            $metal = array_map('trim', $metal);

            foreach (array('name', 'charge', 'email', 'code') as $k)
                if(empty($metal[$k])) throw new InvalidParameter('Необходимо е да въведете стойност', array($k));


            if(DBC::$slave->selectRow("SELECT id FROM mediators WHERE id != {$metal['id']} AND code = " . DBC::$slave->quote($metal['code']))) throw new InvalidParameter(L('Вече съществува Посредник с този код.'));

            DBC::$main->update('mediators', $metal);

            return $this->oResponse;
        }, [$aRequest]);
    }

    public function delete($aRequest) {
        $metal = $aRequest['fields'];
        $metal['id'] = intval($aRequest['id']);

        if(empty($metal['id'])) throw new InvalidParameter;

        DBC::$main->delete('mediators', $metal['id']);
        return $this->oResponse;
    }
}