<?php
namespace pallex\pallets;

use main\db\DBC;
use main\grid\GridPanelHandler;
use main\suggest\Suggest;

class Pallets extends GridPanelHandler {
    public function __construct() {
        $this->oBase = DBC::$slave;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'name',
                'width' => 100,
                'headerText' => L('Име'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_INT,
                'dataField' => 'l',
                'width' => 100,
                'headerText' => L('Дължина'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_INT,
                'dataField' => 'w',
                'width' => 100,
                'headerText' => L('Ширина'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_INT,
                'dataField' => 'h',
                'width' => 100,
                'headerText' => L('Височина'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_INT,
                'dataField' => 'max_weight',
                'width' => 100,
                'headerText' => L('Тегло до'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumnEditedBy()
        );

        $this->sReportSelectStatement = "
            select * from pallets
        ";

        $this->aReportWhereStatement[] = 'to_arc = 0';
        //$this->sReportGroupStatement = '';
        $this->aDefaultSort = array(
            array('field' => 'name', 'dir' => 'ASC')
        );

        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
    }

    public function load($aRequest = array(), $aInitParams = array()) {
        return parent::load($aRequest, $aInitParams);
    }
}