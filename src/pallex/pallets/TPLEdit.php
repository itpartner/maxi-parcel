<?php
namespace pallex\pallets;

use main\TPLBase;

class TPLEdit extends TPLBase {

    public function __construct($sPrefix, array $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);

        $this->bPrintWithTitle = true;
        $this->aFrameConfig['title'] = empty($aInitParams['row']['id']) ? L('Нов вид палет') : L('Редакция на палет:') . ' ' . $aInitParams['row']['code'];
    }

    public function printHtml() { ?>
        <style type="text/css">
            #<?= $this->sPrefix ?> td {
                padding: 2px;
            }

            #<?= $this->sPrefix ?> .width30 {
                                       box-sizing: border-box;
                                       width: 30px;
                                   }
            #<?= $this->sPrefix ?> .width30::after {
                                       content: "sdf";
                                   }
            #<?= $this->sPrefix ?> .fr {
                                       float: right;
                                   }
        </style>

        <table style="width: 100%">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName'=>'name', 'text' => L('Име')));?></td>
                            <td><? $this->printElement('input', array('name'=>'name')); ?></td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('text' => L('Размери')));?></td>
                            <td class="fr">
                                <? $this->printElement('label', array('fieldName'=>'w', 'text' => L('W')));?>
                                <? $this->printElement('input', array('class' => 'width30', 'name'=>'w', 'keyRestriction' => 'integer')); ?>
                                <? $this->printElement('label', array('fieldName'=>'l', 'text' => L('L')));?>
                                <? $this->printElement('input', array('class' => 'width30', 'name'=>'l', 'keyRestriction' => 'integer')); ?>
                                <? $this->printElement('label', array('fieldName'=>'h', 'text' => L('H')));?>
                                <? $this->printElement('input', array('class' => 'width30', 'name'=>'h', 'keyRestriction' => 'integer')); ?>
                            </td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName'=>'max_weight', 'text' => L('Тегло до')));?></td>
                            <td><? $this->printElement('input', array('name'=>'max_weight', 'keyRestriction' => 'integer')); ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="toolbar align-right" style="margin-top: 5px">
                        <? $this->printElement('lasteditedinfo'); ?>
                        <? $this->printElement('closebutton'); ?>
                        <? $this->printElement('confirmbutton'); ?>
                        <? $this->printElement('button', array('class' => 'delete_metal align-right', 'text' => L('Изтрий'), 'name' => 'delete_metal', 'iconCls' => 'icon fa fa-trash')) ?>
                    </div>
                </td>
            </tr>
        </table>

        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, baseParams) {

            if (baseParams['row']['id'] == 0) {
                $(container).find('.delete_metal').css('display', 'none');
            }

            $(container).find('.confirm').click(function(){
                container.request('save',{id: baseParams['row']['id']},null, function(){
                    $(container).find('.close').click();
                    baseParams.getParentGrid().loadData();
                });
            });

            $(container).find('.delete_metal').click(function() {
                container.request('delete', {id: baseParams['row']['id']}, null, function() {
                    $(container).find('.close').click();
                    baseParams.getParentGrid().loadData();
                });
            });
        });

JS;
    }


}