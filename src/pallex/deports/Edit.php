<?php
namespace pallex\deports;

use emc\production\schedule\Scheduler;
use main\db\DBC;
use main\Exception;
use main\form\FormPanelHandler;
use main\grid\GridPanelHandler;
use main\InvalidParameter;
use pallex\GlobalHelper;

/**
 * Class Edit
 * @accessLevelDescription Редактиране на Депо
 */
class Edit extends GridPanelHandler {
    public function __construct() {
        $this->oBase = DBC::$main;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'code',
                'width' => 100,
                'headerText' => L('ПК'),
                'sortable' => false,
                'resizable' => true,
                'filterable' => false
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'name',
                'width' => 200,
                'headerText' => L('Име'),
                'sortable' => false,
                'resizable' => true,
                'filterable' => false
            )),
            $this->newColumnEditedBy(),
            $this->newColumn(
                array(
                    'type' => GridPanelHandler::CTYPE_BTN,
                    'iconCls' => 'icon fa fa-trash',
                    'dataField' => 'delete',
                    'headerText' => '',
                    'width' => 30,
                    'sortable'  => false,
                    'resizable' => false,
                    'filterable'=> false
                )),
        );

        $this->sReportSelectStatement = "
            select dp.id, pk.code, pk.name,
            CONCAT_WS(' ',pc.fname,pc.mname,pc.lname) AS created_user,
            dp.created_time
            FROM deports_postcodes dp
            LEFT JOIN postcodes pk on pk.id = dp.id_postcode
            LEFT JOIN deports d on d.id = dp.id_deport
            LEFT JOIN personnel pc ON pc.id = dp.created_user
        ";

        parent::__construct();
    }

    public function loadData($aRequest, $nTimeFrom = 0, $nTimeTo = 0)
    {
        $this->aReportWhereStatement[] = 'dp.id_deport = '.intval($aRequest['id']);
        $this->bUseSQLPaging = false;
        unset($aRequest['paging']);

        return parent::loadData($aRequest, $nTimeFrom, $nTimeTo);
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        foreach ($aInitParams['row'] as $k => $v) $this->setField($k, $v);
        $this->setLastEditInfo($aInitParams['row']);

        $this->setField('city', array('id' => $aInitParams['row']['id_city'], 'value' => $aInitParams['row']['city']));
        $this->setField('postcode', array('id' => $aInitParams['row']['id_postcode'], 'value' => $aInitParams['row']['postcode']));

        if (!$aRequest['id']) $aRequest['id'] = $aInitParams['row']['id'];

        return $this->loadData($aRequest);
    }

    public function save($aRequest) {
        return DBC::$main->withTrans(function ($aRequest) {
            $metal = $aRequest['fields'];
            $metal['id'] = intval($aRequest['id']);
            $metal = array_map('trim', $metal);

            foreach (array('code', 'name', 'address') as $k)
                if(empty($metal[$k])) throw new InvalidParameter('Необходимо е да въведете стойност', array($k));

            if (empty($aRequest['fields']['city']['id'])) throw new InvalidParameter('Необходимо е да въведете стойност', array('city'));
            if (empty($aRequest['fields']['postcode']['id'])) throw new InvalidParameter('Необходимо е да въведете стойност', array('postcode'));

            $metal['id_city'] = intval($aRequest['fields']['city']['id']);
            $metal['id_postcode'] = intval($aRequest['fields']['postcode']['id']);

            if(DBC::$slave->selectRow("SELECT id FROM deports WHERE id != {$metal['id']} AND to_arc = 0 AND code = " . DBC::$slave->quote($metal['code']))) throw new InvalidParameter(L('Вече съществува ПК с този код.'));
            if(DBC::$slave->selectRow("SELECT id FROM deports WHERE id != {$metal['id']} AND to_arc = 0 AND id_postcode = " . DBC::$slave->quote($metal['id_postcode']))) throw new InvalidParameter(L('Вече съществува Депо с този ПК.'));

            DBC::$main->update('deports', $metal);

            $this->oResponse->id = $metal['id'];

            return $this->oResponse;
        }, [$aRequest]);
    }

    public function delete($aRequest) {
        $metal = $aRequest['fields'];
        $metal['id'] = intval($aRequest['id']);

        if(empty($metal['id'])) throw new InvalidParameter;

        DBC::$main->delete('deports', $metal['id']);

        return $this->oResponse;
    }

    public function addPk($aRequest)
    {
        $_SESSION['deportID'] = $aRequest['id'];

        $dbf = array(
            'id_deport' => $aRequest['id'],
            'id_postcode' => $aRequest['fields']['pc']['id']
        );

        if(empty($dbf['id_postcode'])) throw new InvalidParameter('Необходимо е да въведете стойност', array('pc'));

        if(DBC::$main->selectRow("SELECT id FROM deports_postcodes WHERE id_postcode = ".intval($dbf['id_postcode']))) throw new InvalidParameter(L('Този ПК е вече добвен към депо.'));

        DBC::$main->insert('deports_postcodes',$dbf);

        return $this->loadData($aRequest);
    }

    public function delPk($aRequest)
    {
        $_SESSION['deportID'] = $aRequest['id'];

        DBC::$main->delete('deports_postcodes', $aRequest['id_pk']);

        return $this->loadData($aRequest);
    }
}