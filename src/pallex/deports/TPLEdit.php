<?php
namespace pallex\deports;

use main\grid\TPLGridPanel;

class TPLEdit extends TPLGridPanel {

    public function __construct($sPrefix, array $aInitParams) {
         $this->{'aConfig'} = array(
            'disablePaging' => true,
            'dataHeight' => 250,
            'topBarItems' => array(
                ['label', array('fieldName' => 'pc', 'text' => L('ПК'))],
                ['input', array('name' => 'pc', 'suggest' => 'method: pallex\deports\Deports.suggestPostCode;')],
                ['button', array('class' => 'add', 'iconCls' => 'icon fa fa-plus-circle', 'text' => L('Добави'))]
            ),
            'bottomBarItems' => array(
                array('lasteditedinfo'),
                array('closebutton'),
                array('confirmbutton'),
                array('deletebutton'),
            )
        );

        parent::__construct($sPrefix, $aInitParams);

        $this->bPrintWithTitle = true;
        $this->aFrameConfig['title'] = empty($aInitParams['row']['id']) ? L('Ново депо') : L('Редактиране на депо:') . ' ' . $aInitParams['row']['code'];
    }

    public function printBeforeGrid() { ?>

        <table>
            <tr>
                <td><? $this->printElement('label', array('fieldName'=>'code', 'text' => L('Код')));?></td>
                <td><? $this->printElement('input', array('name'=>'code')); ?></td>
            </tr>
            <tr>
                <td><? $this->printElement('label', array('fieldName'=>'name', 'text' => L('Наименование')));?></td>
                <td><? $this->printElement('input', array('name'=>'name')); ?></td>
            </tr>
            <tr>
                <td><? $this->printElement('label', array('fieldName'=>'city' , 'text' => L('Населено място')));?></td>
                <td><? $this->printElement('input', array('name'=>'city', 'suggest' => 'method: pallex\postcodes\PostCodes.suggestCity;')); ?></td>
            </tr>
            <tr>
                <td><? $this->printElement('label', array('fieldName'=>'address', 'text' => L('Адрес')));?></td>
                <td><? $this->printElement('input', array('name'=>'address')); ?></td>
            </tr>
            <tr>
                <td><? $this->printElement('label', array('fieldName'=>'postcode', 'text' => L('ПК на депото')));?></td>
                <td><? $this->printElement('input', array('name'=>'postcode', 'suggest' => 'method: pallex\hubs\Hubs.suggestPostCode;')); ?></td>
            </tr>
        </table>
        <hr/>

        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, baseParams) {

            if (baseParams['row']['id'] == 0) {
                $(container).find('.delete').attr({disabled:true});
                $(container).find('.add').attr({disabled:true});
                container.fields['pc'].disabled = true;
            }
            container.baseParams.id = baseParams['row']['id'];

            $(container).find('.confirm').click(function(){
                container.request('save',{id: baseParams['row']['id']},null, function(res){
                    $(container).find('.delete').attr({disabled:false});
                    $(container).find('.add').attr({disabled:false});
                    container.fields['pc'].disabled = false;
                    
                    container.baseParams.id = res.id;
                    
                    if (baseParams['row']['id'] > 0)
                        $(container).find('.close').click();
                    
                    baseParams['row']['id'] = res.id;
                    
                    baseParams.getParentGrid().loadData();
                });
            });

            $(container).find('.delete').click(function() {
                container.request('delete', {}, null, function() {
                    $(container).find('.close').click();
                    baseParams.getParentGrid().loadData();
                });
            });
            
            $(container).find('.add').click(function() {
                container.request('addPk',{});
            });
            
            container.grid.clickListeners['delete'] = function(rowNum, fieldElement, htmlElement) {
                container.request('delPk',{id_pk: container.grid.getCurrentData()[rowNum]['id']});
            };
        });

JS;
    }


}