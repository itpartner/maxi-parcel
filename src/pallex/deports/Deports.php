<?php
namespace pallex\deports;

use main\db\DBC;
use main\grid\GridPanelHandler;
use main\suggest\Suggest;

class Deports extends GridPanelHandler {
    public function __construct() {
        $this->oBase = DBC::$slave;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'country',
                'width' => 100,
                'headerText' => L('Държава'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'code',
                'width' => 100,
                'headerText' => L('Код'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'name',
                'width' => 200,
                'headerText' => L('Име'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'city',
                'width' => 200,
                'headerText' => L('Населено място'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'address',
                'width' => 200,
                'headerText' => L('Адрес'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'postcode',
                'width' => 100,
                'headerText' => L('ПК'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumnEditedBy()
        );

        $this->sReportSelectStatement = "
            select d.id, y.name as country, d.code, d.name, c.name as city, d.address, pk.code as postcode, d.id_city, d.id_postcode,
            CONCAT_WS(' ',pc.fname,pc.mname,pc.lname) AS created_user,
            CONCAT_WS(' ',pu.fname,pu.mname,pu.lname) AS updated_user, d.created_time, d.updated_time
            from deports d 
            left join postcodes pk on pk.id = d.id_postcode
            left join cities c on c.id = d.id_city
            left join cities y on y.id = c.id_country and y.is_country > 0
            LEFT JOIN personnel pc ON pc.id = d.created_user
            LEFT JOIN personnel pu ON pu.id = d.updated_user
        ";

        $this->aReportWhereStatement[] = 'd.to_arc = 0';
        $this->bUseSQLPaging = false;

        $this->aDefaultSort = array(
            array('field' => 'code', 'dir' => 'ASC')
        );

        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
    }

    public function load($aRequest = array(), $aInitParams = array()) {
        return parent::load($aRequest, $aInitParams);
    }

    public function suggestCity($aRequest) {
        $aResult = array();
        $aResult['select'] 	= "SELECT id, name as value, name as html_value FROM cities pa ";
        $aResult['where'][] = "MATCH(pa.search_text) AGAINST('*{$aRequest['query']}*' IN BOOLEAN MODE) AND pa.to_arc = 0";
        $aResult['group'] 	= 'html_value';

        return Suggest::select($aResult);
    }

    public function suggestPostCode($aRequest) {
        $aResult = array();
        $aResult['select'] 	= "SELECT id, code as value, code as html_value FROM postcodes pa ";
        $aResult['where'][] = "pa.code LIKE '%{$aRequest['query']}%' AND pa.to_arc = 0";
        $aResult['group'] 	= 'html_value';

        return Suggest::select($aResult);
    }
}