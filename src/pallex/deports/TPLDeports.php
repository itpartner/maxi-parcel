<?php
namespace pallex\deports;

use main\db\DBC;
use main\grid\GridPanelHandler;
use main\grid\TPLGridPanel;

class TPLDeports extends TPLGridPanel {
    public function __construct($sPrefix, $aInitParams = array()) {
        $this->{'aConfig'} = array(
            'title' => L('Депа'),
            'autoVResize' => true,
            'topBarItems' => array(
                array('button', array('name' => 'add', 'class' => 'add', 'iconCls' => 'icon fa fa-plus-circle', 'text' => L('Добави')))
            ),
            'bottomBarItems' => array(
                array('exportbutton', array('align' => 'right'))
            )
        );
        parent::__construct($sPrefix, $aInitParams);
    }


    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, aBaseParams) {
            $(container).find('.add').click(function() {
                framework.createWindow({
                    tplName: 'pallex/deports/Edit',
                    baseParams: {row: {id: 0}, getParentGrid: function() {return container.grid;}}
                });
            });

            container.grid.clickListeners['code'] = function(rowNum, fieldElement, htmlElement) {
                framework.createWindow({
                    tplName: 'pallex/deports/Edit',
                    baseParams: { row: container.grid.getCurrentData()[rowNum], getParentGrid:function(){return container.grid;}}
                });
            };
        });
JS;
    }
}