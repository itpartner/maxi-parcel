<?php
namespace pallex\tariffs;

use main\db\DBC;
use main\grid\GridPanelHandler;
use main\suggest\Suggest;
use pallex\PallexUtils;

class Tariffs extends GridPanelHandler {
    public function __construct() {
        $this->oBase = DBC::$slave;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'pallet',
                'width' => 100,
                'headerText' => L('Палет'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'zone',
                'width' => 100,
                'headerText' => L('Зона'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
                //'items' => PallexUtils::dbLoadZones(true)
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'direction',
                'width' => 200,
                'headerText' => L('Направление'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true,
                'items' => PallexUtils::dbLoadDirections(true)
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'service',
                'width' => 200,
                'headerText' => L('Услуга'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true,
                'items' => PallexUtils::dbLoadServices()
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_INT,
                'dataField' => 'range_from',
                'width' => 100,
                'headerText' => L('От'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_INT,
                'dataField' => 'range_to',
                'width' => 100,
                'headerText' => L('До'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_FLOAT,
                'dataField' => 'pricename',
                'width' => 100,
                'headerText' => L('Цена'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumnEditedBy()
        );

        $sLv = PallexUtils::translatePriceType('price');
        $sPercent = PallexUtils::translatePriceType('percent');

        $this->sReportSelectStatement = "
            select t.id, t.range_from, t.range_to, t.price, t.price_type, t.service, t.direction, t.id_zone,
            if (t.price_type = 'price', concat(t.price, ' ', '$sLv'), 
            concat(t.price, ' ', '$sPercent')) as pricename,
            if(p.id>0, p.name, '".L('Всички')."') as pallet, 
            if(z.id>0, z.name, '".L('Всички')."') as zone, 
            p.id as id_pallet,
            CONCAT_WS(' ',pc.fname,pc.mname,pc.lname) AS created_user,
            CONCAT_WS(' ',pu.fname,pu.mname,pu.lname) AS updated_user, 
            t.created_time, t.updated_time
            FROM tariffs t 
            LEFT JOIN pallets p on p.id = t.id_pallet
            left join zones z on z.id = t.id_zone
            LEFT JOIN personnel pc ON pc.id = t.created_user
            LEFT JOIN personnel pu ON pu.id = t.updated_user
        ";

        $this->aReportWhereStatement[] = 't.to_arc = 0';
        //$this->sReportGroupStatement = '';
        $this->bUseSQLPaging = false;
        $this->aDefaultSort = array(
            array('field' => 'pallet', 'dir' => 'ASC')
        );

        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
    }

    public function load($aRequest = array(), $aInitParams = array()) {
        return parent::load($aRequest, $aInitParams);
    }
}