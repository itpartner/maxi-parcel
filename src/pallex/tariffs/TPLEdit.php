<?php
namespace pallex\tariffs;

use main\TPLBase;
use pallex\PallexUtils;

class TPLEdit extends TPLBase {

    public function __construct($sPrefix, array $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);

        $this->bPrintWithTitle = true;
        $this->aFrameConfig['title'] = empty($aInitParams['row']['id']) ? L('Нова Тарифа') : L('Редакция на Тарифа');
    }

    public function printHtml() { ?>
        <style type="text/css">

            #<?= $this->sPrefix ?> select {
                box-sizing: border-box;
                width: 100%;
            }
            #<?= $this->sPrefix ?> td {
                                       padding: 3px;
                                   }
            #<?= $this->sPrefix ?> .width80 {
                                       box-sizing: border-box;
                                       width: 80px;
                                   }
            #<?= $this->sPrefix ?> .width50 {
                                       box-sizing: border-box;
                                       width: 50px;
                                   }
        </style>
        <table style="width: 100%">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName'=>'id_pallet', 'text' => L('Палет')));?></td>
                            <td><? $this->printElement('select', array('name'=>'id_pallet'), PallexUtils::dbLoadPallets()); ?></td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName'=>'zone', 'text' => L('Зона')));?></td>
                            <td><? $this->printElement('select', array('name'=>'zone'), PallexUtils::dbLoadZones(true)); ?></td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName'=>'direction', 'text' => L('Направление')));?></td>
                            <td><? $this->printElement('select', array('name'=>'direction'), PallexUtils::dbLoadDirections(true)); ?></td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName'=>'service', 'text' => L('Услуга')));?></td>
                            <td><? $this->printElement('select', array('name'=>'service'), PallexUtils::dbLoadServices()); ?></td>
                        </tr>
                        <tr>
                            <td>
                                <? $this->printElement('label', array('fieldName'=>'service', 'text' => L('Брой')));?>
                            </td>
                            <td>
                                <span>
                                    <? $this->printElement('label', array('fieldName'=>'range_from', 'text' => L('От')));?>
                                    <? $this->printElement('input', array('name'=>'range_from', 'class' => 'width80', 'keyrestriction' => 'integer')); ?>
                                </span>
                                <span style="float: right;">
                                    <? $this->printElement('label', array('fieldName'=>'range_to', 'text' => L('До')));?>
                                    <? $this->printElement('input', array('name'=>'range_to', 'class' => 'width80', 'keyrestriction' => 'integer')); ?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <? $this->printElement('label', array('fieldName'=>'price', 'text' => L('Цена')));?>
                            </td>
                            <td>
                                <? $this->printElement('input', array('name'=>'price', 'class' => 'width80', 'keyrestriction' => 'float')); ?>
                                <? $this->printElement('select', array('name' => 'price_type', 'class' => 'width50'), PallexUtils::dbLoadPriceTypes()); ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="toolbar align-right" style="margin-top: 5px">
                        <? $this->printElement('lasteditedinfo'); ?>
                        <? $this->printElement('closebutton'); ?>
                        <? $this->printElement('confirmbutton'); ?>
                        <? $this->printElement('button', array('class' => 'delete_metal align-right', 'text' => L('Изтрий'), 'name' => 'delete_metal', 'iconCls' => 'icon fa fa-trash')) ?>
                    </div>
                </td>
            </tr>
        </table>

        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, baseParams) {

            if (baseParams['row']['id'] == 0) {
                $(container).find('.delete_metal').css('display', 'none');
            }

            $(container).find('.confirm').click(function(){
                container.request('save',{id: baseParams['row']['id']},null, function(){
                    $(container).find('.close').click();
                    baseParams.getParentGrid().loadData();
                });
            });

            $(container).find('.delete_metal').click(function() {
                container.request('delete', {id: baseParams['row']['id']}, null, function() {
                    $(container).find('.close').click();
                    baseParams.getParentGrid().loadData();
                });
            });
        });

JS;
    }


}