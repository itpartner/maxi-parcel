<?php
/**
 * Created by PhpStorm.
 * User: spetrov
 * Date: 11.10.18
 * Time: 18:09
 */

namespace pallex\client;

use main\TPLBase;
use pallex\PallexUtils;

class TPLCalculator extends TPLBase
{
    public function printHtml()
    {
        ?>

        <nav class="maxiBkColor maxiNavMenu">
            <div class="logoBox">
                <p>Макси пратка</p>
            </div>
        </nav>
        <div class="maxiNavMenuFix"></div>

        <?php if (!PallexUtils::isValidMediatorCode($_GET['code'])) { ?>
            <div class="maxiPanel">
                <h3>калкулаторът не може да бъде директно използван.</h3>
            </div>
        <?php return; } ?>

        <div class="maxiPanel" id="<?=$this->sPrefix?>_view1">
            <div class="maxiTitle">
                <h1>Калкулатор</h1>
            </div>
            <fieldset>
                <legend>Информация за пратка</legend>
                <div class="col-container pad-bot">
                    <div class="col-2 input colVertSep">
                        <? $this->printElement('input', ['value' => '1', 'name' => 'par_count', 'required' => 'true', 'keyrestriction' => 'integer', 'class' => 'clear-calc']); ?>
                        <? $this->printElement('label', ['fieldName' => 'par_count', 'text' => 'Брой / бр.']); ?>
                        <div class="error_msg maxiTaLeft" id="<?=$this->sPrefix?>_err_par_count"></div>
                    </div>

                    <div class="col-2 input colVertSep">
                        <? $this->printElement('input', ['value' => '', 'name' => 'par_weight', 'required' => 'true', 'keyrestriction' => 'integer', 'class' => 'clear-calc']); ?>
                        <? $this->printElement('label', ['fieldName' => 'par_weight', 'text' => 'Тегло / кг.']); ?>
                        <div class="error_msg maxiTaLeft" id="<?=$this->sPrefix?>_err_par_weight"></div>
                    </div>

                    <div class="col-2 input colVertSep">
                        <? $this->printElement('input', ['value' => '', 'name' => 'par_l', 'required' => 'true', 'keyrestriction' => 'integer', 'class' => 'clear-calc']); ?>
                        <? $this->printElement('label', ['fieldName' => 'par_l', 'text' => 'Дължина / см.']); ?>
                        <div class="error_msg maxiTaLeft" id="<?=$this->sPrefix?>_err_par_l"></div>
                    </div>
                    <div class="col-2 input colVertSep">
                        <? $this->printElement('input', ['value' => '', 'name' => 'par_w', 'required' => 'true', 'keyrestriction' => 'integer', 'class' => 'clear-calc']); ?>
                        <? $this->printElement('label', ['fieldName' => 'par_w', 'text' => 'Ширина / см.']); ?>
                        <div class="error_msg maxiTaLeft" id="<?=$this->sPrefix?>_err_par_w"></div>
                    </div>

                    <div class="col-2 input colVertSep">
                        <? $this->printElement('input', ['value' => '', 'name' => 'par_h', 'required' => 'true', 'keyrestriction' => 'integer', 'class' => 'clear-calc']); ?>
                        <? $this->printElement('label', ['fieldName' => 'par_h', 'text' => 'Височина / см.']); ?>
                        <div class="error_msg maxiTaLeft" id="<?=$this->sPrefix?>_err_par_h"></div>
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <legend>Допълнителни услуги</legend>
                <div class="col-container">
                    <div class="col-3 maxiTaLeft input" >
                        <? $this->printElement('input', ['name' => 'cd_value', 'required' => 'true', 'keyrestriction' => 'float', 'class' => 'clear-calc']); ?>
                        <? $this->printElement('label', ['fieldName' => 'cd_value', 'text' => 'Наложен платеж / лв.']); ?>
                    </div>
                    <div class="col-1"></div>
                    <div class="col-3 maxiTaCenter checkbox">
                        <? $this->printElement('input', ['type' => 'checkbox', 'name' => 'stretch', 'value' => '1', 'class' => 'clear-calc']); ?>
                        <? $this->printElement('label', ['fieldName' => 'stretch', 'text' => 'Стречоване']); ?>
                    </div>
                    <div class="col-3 maxiTaRight checkbox">
                        <? $this->printElement('input', ['type' => 'checkbox', 'name' => 'rental', 'value' => '1', 'class' => 'clear-calc']); ?>
                        <? $this->printElement('label', ['fieldName' => 'rental', 'text' => 'Палети под наем']); ?>
                    </div>
                </div>
            </fieldset>

            <fieldset id="form_from">
                <legend>От</legend>
                <div class="col-container pad-bot">
                    <div class="col-5 select colVertSep">
                        <? $this->printElement('select', array('name' => 'from_country', 'value' => 1033),
                            PallexUtils::dbLoadCountries()); ?>
                        <? $this->printElement('label', array('fieldName'=>'from_country', 'text' => 'Държава'));?>
                    </div>
                    <div class="col-5 input colVertSep">
                        <? $this->printElement('input', array('name' => 'from_city', 'type' => 'text', 'required' => 'true',
                            'suggest' => 'method: pallex\client\Calculator.suggestCity;country:$from_country;', 'class' => 'clear-calc')); ?>
                        <? $this->printElement('label', array('fieldName'=>'from_city', 'text' => 'Населено място'));?>
                        <div class="error_msg maxiTaLeft" id="<?=$this->sPrefix?>_err_from_city"></div>
                    </div>
                </div>

                <table class="from-no-city-hide hide">
                    <tr>
                        <td>
                            <div class="col-container pad-bot">
                                <div class="col-5 radioSelect left">
                                    <input type="radio" name="from_dir" id="<?=$this->sPrefix?>_from_dir_address" value="address" checked/>
                                    <label for="<?=$this->sPrefix?>_from_dir_address">Адрес</label>
                                </div>
                                <div class="col-5 radioSelect right">
                                    <input type="radio" name="from_dir" id="<?=$this->sPrefix?>_from_dir_office" value="office" />
                                    <label for="<?=$this->sPrefix?>_from_dir_office">Офис</label>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="maxiTaLeft pad-bot">
                            <div class="col-container from-dir-address">
                                <div class="col-2 select colVertSep">
                                    <? $this->printElement('select', array('name' => 'from_pk'), []); ?>
                                    <? $this->printElement('label', array('fieldName' => 'from_pk', 'text' => 'ПК')); ?>
                                </div>

                                <div class="col-8 input colVertSep">
                                    <? $this->printElement('input', ['name' => 'from_address', 'required' => 'true']); ?>
                                    <? $this->printElement('label', ['fieldName' => 'from_address', 'text' => 'Адрес']); ?>
                                    <div class="error_msg maxiTaLeft" id="<?=$this->sPrefix?>_err_from_address"></div>
                                </div>
                            </div>
                            <div class="col-container hide from-dir-office">
                                <div class="select col-10">
                                    <? $this->printElement('select', array('name' => 'from_office', 'class' => 'clear-calc'), []); ?>
                                    <? $this->printElement('label', array('fieldName' => 'from_office', 'text' => 'Офис')); ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>

            <fieldset id="form_to">
                <legend>До</legend>

                <div class="col-container pad-bot">
                    <div class="col-5 select colVertSep">
                        <? $this->printElement('select', array('name' => 'to_country', 'value' => 1033),
                            PallexUtils::dbLoadCountries()); ?>
                        <? $this->printElement('label', array('fieldName'=>'to_country', 'text' => 'Държава'));?>
                    </div>

                    <div class="col-5 input colVertSep">
                        <? $this->printElement('input', array('name' => 'to_city', 'required' => 'true',
                            'suggest' => 'method: pallex\client\Calculator.suggestCity;country:$to_country;', 'class' => 'clear-calc')); ?>
                        <? $this->printElement('label', array('fieldName'=>'to_city', 'text' => 'Населено място'));?>
                        <div class="error_msg maxiTaLeft" id="<?=$this->sPrefix?>_err_to_city"></div>
                    </div>
                </div>

                <table class="to-no-city-hide hide">
                    <tr>
                        <td>
                            <div class="col-container pad-bot">
                                <div class="col-5 radioSelect left">
                                    <input type="radio" name="to_dir" id="<?=$this->sPrefix?>_to_dir_address" value="address" checked/>
                                    <label for="<?=$this->sPrefix?>_to_dir_address">Адрес</label>
                                </div>
                                <div class="col-5 radioSelect right">
                                    <input type="radio" name="to_dir" id="<?=$this->sPrefix?>_to_dir_office" value="office"/>
                                    <label for="<?=$this->sPrefix?>_to_dir_office">Офис</label>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="maxiTaLeft pad-bot">
                            <div class="col-container to-dir-address">
                                <div class="col-2 select colVertSep">
                                    <? $this->printElement('select', array('name' => 'to_pk'), []); ?>
                                    <? $this->printElement('label', array('fieldName' => 'to_pk', 'text' => 'ПК')); ?>
                                </div>

                                <div class="col-8 input colVertSep">
                                    <? $this->printElement('input', ['name' => 'to_address', 'required' => 'true']); ?>
                                    <? $this->printElement('label', ['fieldName' => 'to_address', 'text' => 'Адрес']); ?>
                                    <div class="error_msg maxiTaLeft" id="<?=$this->sPrefix?>_err_to_address"></div>
                                </div>
                            </div>
                            <div class="col-container hide to-dir-office">
                                <div class="col-10 select">
                                    <? $this->printElement('select', array('name' => 'to_office', 'class' => 'clear-calc'), []); ?>
                                    <? $this->printElement('label', array('fieldName' => 'to_office', 'text' => 'Офис')); ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>

            <fieldset>
                <legend>Резултат</legend>
                <div class="hide col-container services-result">
                    <div class="col-5 maxiTaCenter">
                        <button class="default service-standard"></button>
                    </div>
                    <div class="col-5 maxiTaCenter">
                        <button class="default service-premium"></button>
                    </div>
                </div>
                <button class="default calculate" type="button">Изчисли</button>
            </fieldset>
        </div>

        <div class="maxiPanel hide" id="<?=$this->sPrefix?>_view2">
            <div class="maxiTitle">
                <h1>Заявка</h1>
            </div>

            <fieldset>
                <legend>Тарифа</legend>
                <table class="">
                    <tr>
                        <td>
                            <div class="radioSelect left">
                                <input type="radio" name="tariff" id="<?=$this->sPrefix?>_tariff_standard" value="standard" checked />
                                <label for="<?=$this->sPrefix?>_tariff_standard" class="service-standard">Стандарт - 20.12 лв.</label>
                            </div>
                        </td>
                        <td class="pad-bot">
                            <div class="radioSelect right">
                                <input type="radio" name="tariff" id="<?=$this->sPrefix?>_tariff_premium" value="premium" />
                                <label for="<?=$this->sPrefix?>_tariff_premium" class="service-premium">Платинум - 23.12 лв.</label>
                            </div>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <fieldset>
                <legend>Подател</legend>
                <div class="col-container pad-bot">
                    <div class="col-7 input colVertSep">
                        <? $this->printElement('input', ['name' => 'sender_name', 'required' => 'true']); ?>
                        <? $this->printElement('label', ['fieldName' => 'sender_name', 'text' => 'Име']); ?>
                        <div class="error_msg maxiTaLeft" id="<?=$this->sPrefix?>_err_sender_name"></div>
                    </div>
                    <div class="col-3 input colVertSep">
                        <? $this->printElement('input', ['name' => 'sender_phone', 'required' => 'true']); ?>
                        <? $this->printElement('label', ['fieldName' => 'sender_phone', 'text' => 'Телефон']); ?>
                        <div class="error_msg maxiTaLeft" id="<?=$this->sPrefix?>_err_sender_phone"></div>
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <legend>Получател</legend>
                <div class="col-container pad-bot">
                    <div class="col-7 input colVertSep">
                        <? $this->printElement('input', ['name' => 'receiver_name', 'required' => 'true']); ?>
                        <? $this->printElement('label', ['fieldName' => 'receiver_name', 'text' => 'Име']); ?>
                        <div class="error_msg maxiTaLeft" id="<?=$this->sPrefix?>_err_receiver_name"></div>
                    </div>
                    <div class="col-3 input colVertSep">
                        <? $this->printElement('input', ['name' => 'receiver_phone', 'required' => 'true']); ?>
                        <? $this->printElement('label', ['fieldName' => 'receiver_phone', 'text' => 'Телефон']); ?>
                        <div class="error_msg maxiTaLeft" id="<?=$this->sPrefix?>_err_receiver_phone"></div>
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <legend>Допълнителна информация</legend>
                <div class="maxiTaLeft col-container pad-bot">
                    <div class="col-5 input colVertSep">
                        <? $this->printElement('input', ['name' => 'load_at', 'class' => 'datepicker', 'required' => 'true']); ?>
                        <? $this->printElement('label', ['fieldName' => 'load_at', 'text' => 'Товарене на']); ?>
                        <div class="error_msg maxiTaLeft" id="<?=$this->sPrefix?>_err_load_at"></div>
                    </div>
                    <div class="col-5 select colVertSep">
                        <select name="pay_side">
                            <option value="sender">Подател</option>
                            <option value="receiver">Получател</option>
                        </select>
                        <? $this->printElement('label', ['fieldName' => 'pay_side', 'text' => 'Платец']); ?>
                    </div>
                </div>
            </fieldset>

            <div class="pad-top">
                <div class="col-container">
                    <div class="col-5 maxiTaLeft">
                        <button class="gray service-back">Обратно</button>
                    </div>
                    <div class="col-5 maxiTaRight">
                        <button class="default service-send">Изпрати</button>
                    </div>
                </div>
            </div>
        </div>

    <div class="maxiPanel hide" id="<?=$this->sPrefix?>_view3">
        <div class="maxiTitle">
            <h1>Потвърждение</h1>
        </div>

        <fieldset>
        <legend>Информация</legend>
            <div class="maxiTaLeft pad-bot">
                <p>Вашата заявка с номер <b id="<?=$this->sPrefix?>_request_number"></b> е приета.</p>
            </div>
            <div class="maxiTaLeft hide" id="<?=$this->sPrefix?>_request_info">
                <i>Възможна е промяна в цената на услугата.</i>
            </div>
        </fieldset>
    </div>

        <?
            if ($_GET['code'])
                $this->printElement('input', ['name' => 'mediator_code', 'type' => 'hidden', 'value' => $_GET['code']]);
        ?>
        <div class="maxiModalContent maxiDialog">
            <div class="maxiPanel maxiPanelBkColor maxiDialogContent" style="width: 400px;">
                <div class="maxiTitle">
                    <h1>Грешка</h1>
                </div>
                <div class="maxiPanelBody">
                    <p id="error_message_display">Bla bla error</p>
                </div>
                <div class="maxiPanelFooter">
                    <table>
                        <tr>
                            <td class="maxiTaCenter">
                                <button type="button" id="btnDiscApplication" onclick="closeModal();" >OK</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            function drawModalFrame()
            {
                var f = document.createElement('div');
                f.id = 'modalFrame';
                f.classList.add('maxiModalFrame');

                return f;
            }

            function showModal(modalFrame, contentId)
            {
                $('.maxiDialog').css('display', 'block');
                $(modalFrame).append($('.maxiDialog'));
                $('#body').append($(modalFrame));
            }

            function closeModal()
            {
                var x = document.getElementsByClassName('maxiModalContent');
                var b = document.getElementById('body');
                var i;
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = 'none';
                    b.appendChild(x[i]);
                }

                document.getElementById('body').removeChild(document.getElementById('modalFrame'));
            }

            function displayError(error) {
                $('#error_message_display').html(error);
                showModal(drawModalFrame());
            }
            $(function () {
                $('.datepicker').datepicker({
                    dateFormat: 'dd.mm.yy',
                    firstDay: 1
                }).attr({
                    autocomplete: 'off'
                });
            });

            function ssoDisplayError(elementId, error) {
                ssoClearErrors();

                document.getElementById(elementId).innerText = error;
            }

            function ssoDisplaySuccess(elementId, success) {
                ssoClearErrors();

                document.getElementById(elementId).classList.add('success_msg');
                document.getElementById(elementId).innerText = success;

            }

            function ssoClearErrors() {
                var x = document.getElementsByClassName("error_msg");
                var i;
                for (i = 0; i < x.length; i++) {
                    x[i].classList.remove('success_msg');
                    x[i].innerText = '';
                }
            }

        </script>

        <?php
    }

    public function getJSFunction()
    {
        return <<<'JS'
        (function(container, prefix, baseParams) {
            
            function clearCalc() {
                $('.services-result').addClass('hide');
                $('.calculate').removeClass('hide');
            }
            
            function loadPk(pk, off, city) {
                $(container.fields[pk]).find('option').remove();
                $(container.fields[off]).find('option').remove();
                
                container.request('loadPk', {dir: pk, city: city}, null, function (res) {
                    if (res.deports.length > 0) {
                        for (i = 0; i < res.deports.length; i++) {
                            $(container.fields[off]).append($('<option>', { 
                                value: res.deports[i].id,
                                text : res.deports[i].name + ' / ПК:' + res.deports[i].postcode + ' ' + res.deports[i].address
                            }));
                        }
                    } else {
                        $(container.fields[off]).append($('<option>', { 
                                value: 0,
                                text : '-'
                            }));
                    }
                    
                    if (res.pks.length > 0) {
                        for (i = 0; i < res.pks.length; i++) {
                            $(container.fields[pk]).append($('<option>', { 
                                value: res.pks[i].id,
                                text : res.pks[i].code
                            }));
                        }
                    } else {
                        $(container.fields[pk]).append($('<option>', { 
                                value: 0,
                                text : '-'
                            }));
                    }
                });
            }
            
            container.fields['from_country'].addListener('change', function() {
                container.fields['from_city'].setValue('');
            });
            
            container.fields['to_country'].addListener('change', function() {
                container.fields['to_city'].setValue('');
            });

            container.fields['from_city'].addListener('suggestSelect', function (e) {
                ssoClearErrors();
                $('.from-no-city-hide').removeClass('hide');
                $('.from-no-city-show').addClass('hide');
                loadPk('from_pk', 'from_office', e.id);
            });
            
            container.fields['from_city'].addListener('change', function () {
                if (typeof container.fields.from_city.getValue().id === "undefined") {
                    $('.from-no-city-hide').addClass('hide');
                    $('.from-no-city-show').removeClass('hide');
                    clearCalc();
                }
            });
            
            container.fields.to_city.addListener('suggestSelect', function (e) {
                ssoClearErrors();
                $('.to-no-city-hide').removeClass('hide');
                $('.to-no-city-show').addClass('hide');
                loadPk('to_pk','to_office', e.id);
            });
            
            container.fields.to_city.addListener('change', function (e) {
                if (typeof container.fields.to_city.getValue().id === "undefined") {
                    $('.to-no-city-hide').addClass('hide');
                    $('.to-no-city-show').removeClass('hide');
                    clearCalc();
                }
            });
            
            container.fields['from_dir'].addListener('change', function() {
                if (container.fields['from_dir'].getValue() == "office") {
                    $('.from-dir-address').addClass('hide');
                    $('.from-dir-office').removeClass('hide');
                } else {
                    $('.from-dir-address').removeClass('hide');
                    $('.from-dir-office').addClass('hide');
                }
                clearCalc();
            });
            container.fields['to_dir'].addListener('change', function() {
                if (container.fields['to_dir'].getValue() == "office") {
                    $('.to-dir-address').addClass('hide');
                    $('.to-dir-office').removeClass('hide');
                } else {
                    $('.to-dir-address').removeClass('hide');
                    $('.to-dir-office').addClass('hide');
                }
                clearCalc();
            });
            
            container.fields['tariff'].addListener('change', function() {
                
            });
            
            $(container).find('.calculate').click(function(){
                ssoClearErrors();
                
                let v = ['par_count', 'par_weight', 'par_l', 'par_w', 'par_h'];
                for (let i = 0; i < v.length; i++) {
                    if (container.fields[v[i]].getValue() == '') {
                        ssoDisplayError(prefix + '_err_' + v[i], 'моля попълнете.');
                        container.fields[v[i]].focus();
                        return;
                    }
                }

                if (typeof container.fields['from_city'].getValue().id === "undefined") {
                    ssoDisplayError(prefix + '_err_from_city', 'Не сте избрали населено място.');
                    container.fields['from_city'].focus();
                    return;
                }
                if (typeof container.fields['to_city'].getValue().id === "undefined") {
                    ssoDisplayError(prefix + '_err_to_city', 'Не сте избрали населено място.');
                    container.fields['to_city'].focus();
                    return;
                }
                if (container.fields['from_dir'].getValue() == 'address' && container.fields['from_address'].getValue() == '') {
                    ssoDisplayError(prefix + '_err_from_address', 'Не сте посочили адрес.');
                    container.fields['from_address'].focus();
                    return;
                }
                if (container.fields['to_dir'].getValue() == 'address' && container.fields['to_address'].getValue() == '') {
                    ssoDisplayError(prefix + '_err_to_address', 'Не сте посочили адрес.');
                    container.fields['to_address'].focus();
                    return;
                }
                
                container.request('calculate', {}, null, function (res) {
                    $('.service-standard').html(res.prices.standardText);
                    $('.service-premium').html(res.prices.premiumText);
                    $('.services-result').removeClass('hide');
                    $('.calculate').addClass('hide');
                }, this, function(res) {
                    displayError(res.message);
                    return false;
                });
            });
            
            $(container).find('button.service-standard').click(function(){
                $('#' + prefix + '_view1').addClass('hide');
                $('#' + prefix + '_view2').removeClass('hide');
                container.fields['tariff'].setValue('standard');
            });
            
            $(container).find('button.service-premium').click(function(){
                $('#' + prefix + '_view1').addClass('hide');
                $('#' + prefix + '_view2').removeClass('hide');
                container.fields['tariff'].setValue('premium');
            });
            
            $(container).find('button.service-back').click(function(){
                $('#' + prefix + '_view1').removeClass('hide');
                $('#' + prefix + '_view2').addClass('hide');
                $('.services-result').addClass('hide');
                $('.calculate').removeClass('hide');
            });
            
            $(container).find('.clear-calc').each(
                function(i,v) {
                    v.addListener('change', function(){
                        clearCalc();
                    });
                }
                );
            
            $(container).find('button.service-send').click(function(){
                if (container.fields['sender_name'].getValue() == '') {
                    ssoDisplayError(prefix + '_err_sender_name', 'Не сте посочили име.');
                    container.fields['sender_name'].focus();
                    return;
                }
                if (container.fields['sender_phone'].getValue() == '') {
                    ssoDisplayError(prefix + '_err_sender_phone', 'Не сте посочили телефон.');
                    container.fields['sender_phone'].focus();
                    return;
                }
                if (container.fields['receiver_name'].getValue() == '') {
                    ssoDisplayError(prefix + '_err_receiver_name', 'Не сте посочили име.');
                    container.fields['receiver_name'].focus();
                    return;
                }
                if (container.fields['receiver_phone'].getValue() == '') {
                    ssoDisplayError(prefix + '_err_receiver_phone', 'Не сте посочили телефон.');
                    container.fields['receiver_phone'].focus();
                    return;
                }
                if (container.fields['load_at'].getValue() == '') {
                    ssoDisplayError(prefix + '_err_load_at', 'Не сте посочили дата.');
                    return;
                }
                
                container.request('doRequest', {}, null, function (res) {
                    $('#' + prefix + '_request_number').html(res.request.log_id);
                    if (res.request.pallet_oversize) {
                        $('#' + prefix + '_request_info').removeClass('hide');
                    }
                    $('#' + prefix + '_view1').addClass('hide');
                    $('#' + prefix + '_view2').addClass('hide');
                    $('#' + prefix + '_view3').removeClass('hide');
                }, this, function(res) {
                    displayError(res.message);
                    return false;
                });
            });
        });
JS;
    }
}