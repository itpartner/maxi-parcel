<?php
/**
 * Created by PhpStorm.
 * User: spetrov
 * Date: 11.10.18
 * Time: 17:49
 */

namespace pallex\client;


use main\db\DBC;
use main\form\FormPanelHandler;
use main\suggest\Suggest;
use main\InvalidParameter;
use main\Util;
use pallex\PallexUtils;

class Calculator extends FormPanelHandler
{
    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->oResponse;
    }

    public function suggestCity($aRequest) {
        $aResult = array();
        $idc = intval($aRequest['country']);
        $aResult['select'] 	= "SELECT id, name as value, name as html_value FROM cities pa ";
        $aResult['where'][] = "MATCH(pa.search_text) AGAINST('*{$aRequest['query']}*' IN BOOLEAN MODE) AND pa.id_country = '$idc' AND pa.to_arc = 0";
        $aResult['group'] 	= 'html_value';

        return Suggest::select($aResult);
    }

    public function loadPk($aRequest) {
        $db = DBC::$slave;

        if (empty($aRequest['city'])) throw new InvalidParameter('Необходимо е да въведете стойност', array(''));

        $result = [];

        $result['deports'] = $db->select("select d.id, d.name, pk.code as postcode, d.address from deports d left join postcodes pk on pk.id = d.id_postcode where d.to_arc = 0 and d.id_city = ".intval($aRequest['city']).";");
        $result['pks'] = $db->select("select p.id, p.code from postcodes p left join deports_postcodes dp on dp.id_postcode = p.id left join deports d on d.id = dp.id_deport where d.to_arc = 0 and p.to_arc = 0 and p.id_city = ".intval($aRequest['city'])." and not isnull(dp.id);");

        return $result;
    }

    private function _palletIsOverHeight($pallets, $h) {
        $ret = true;

        foreach ($pallets as $k => $v) {
            if ($h <= $v['h']) { $ret = false; break; }
        }

        return $ret;
    }

    private function _palletMaxHeight($pallets) {
        $maxH = 0;

        foreach ($pallets as $k => $v) {
            if ($maxH < $v['h']) $maxH = $v['h'];
        }

        return $maxH;
    }

    private function _palletIsOverWeight($pallets, $weight) {
        $ret = true;

        foreach ($pallets as $k => $v) {
            if ($weight <= $v['maxW']) { $ret = false; break; }
        }

        return $ret;
    }

    private function guessPallet($w, $h, $l, $maxW) {
        $db = DBC::$slave;

        $pallets = $db->select("select id, w, l, h, max_weight as maxW, name from pallets where to_arc = 0;");

        $our = null;//палето което търсим

        if (!$this->_palletIsOverHeight($pallets, $h) && !$this->_palletIsOverWeight($pallets, $maxW))
        {//търсеното пале попада в някой от палетата на разположение
            foreach ($pallets as $k => $v) {
                if (($v['h'] >= $h) && ($v['maxW'] >= $maxW)) {
                    /* текущото пале "върши работа" */
                    if ($our == null) $our = $v;
                    else {
                        $ourDiff = ($our['w'] - $w) + ($our['h'] - $h) + ($our['l'] - $l);//разликата м/у текущото пале и "нашето"
                        $curDiff = ($v['w'] - $w) + ($v['h'] - $h) + ($v['l'] - $l);//разликата м/у текущото пале и това което търсим
                        if ($ourDiff > $curDiff) $our = $v;//текущото пале "стърчи" по-малко => става "нашето"
                    }
                }
            }
        }

        if ($our == null)
        {//нямаме намерено пале което попада в палетата на разположение
            if ($this->_palletIsOverHeight($pallets, $h))
            {//ако търсеното пале е по-високо от палетата на разположение
                $maxH = $this->_palletMaxHeight($pallets);
                $heaviest = null;
                //търсим палето с най-подходяща максимално допустима маса (МДМ) само в палетата с най-голяма височина
                foreach ($pallets as $k => $v) {
                    if ($v['h'] < $maxH) continue;

                    if ($heaviest == null) $heaviest = $v;
                    else {
                        if ($v['maxW'] > $heaviest['maxW']) $heaviest = $v;
                    }

                    if ($maxW <= $v['maxW'])
                    {
                        if ($our == null) $our = $v;
                        else {
                            $ourDiff = ($our['maxW'] - $maxW);
                            $curDiff = ($v['maxW'] - $maxW);
                            if ($ourDiff > $curDiff) $our = $v;//текущото пале е с по-подходяща МДМ => става "нашето"
                        }
                    }
                }

                if ($our == null) $our = $heaviest;//ако все още нямаме пале вземаме палето с най-голяма МДМ
            }

            if ($our == null)
            {//все още нямаме пале, т.е. (текущото пале не стърчи по височина => е с по-голяма МДМ)
                //вземаме палето с най-голяма височина и най-голяма МДМ
                $maxH = $this->_palletMaxHeight($pallets);
                foreach ($pallets as $k => $v) {
                    if ($v['h'] < $maxH) continue;

                    if ($our == null) $our = $v;
                    else {
                        if ($our['maxW'] < $v['maxW']) $our = $v;
                    }
                }
            }
        }

        if ($our != null) {
            if (($our['w'] < $w) || ($our['l'] < $l))
                $our['overload'] = ((($w/100) * ($l/100) / 0.96 - 1) * 100);
            else
                $our['overload'] = 0;

            if ($this->_palletIsOverHeight($pallets, $h) || $this->_palletIsOverWeight($pallets, $maxW))
                $our['oversize'] = true;
        }

        return $our;
    }

    private function guessDeport($pk) {
        $db = DBC::$slave;

        $d = $db->selectRow("select d.* from deports_postcodes p left join deports d on d.id = p.id_deport where p.id_postcode = ".intval($pk).";");

        return $d;
    }

    private function guessHub($dFrom, $dTo) {
        $db = DBC::$slave;

        $d = $db->selectRow("select h.* from transport_matrix tm left join hubs h on h.id = tm.id_hub where tm.deport_from = ".intval($dFrom)." and tm.deport_to = ".intval($dTo).";");

        return $d;
    }

    private function guessMediator($code) {
        $db = DBC::$slave;

        $code = $db->escape($code);

        $d = $db->selectRow("select * from mediators where to_arc = 0 and code = '$code';");

        return $d;
    }

    private function guessZone($pk, $hub) {
        $db = DBC::$slave;

        $d = $db->selectRow("select id_zone from hubs_zones where id_hub = ".intval($hub)." and id_postcode = ".intval($pk).";");

        return $d['id_zone'];
    }

    private function guessPrices($pallet, $zone, $direction, $count, $service) {
        $db = DBC::$slave;

        $baseQry = "select id, price, price_type from tariffs 
            where to_arc = 0 and service = '$service' and range_from <= '$count' and '$count' <= range_to
            ";
        switch ($service) {
            case 'cd' : $baseQry .= ""; break;
            default : $baseQry .= " and price_type = 'price'"; break;
        }

        $wherePalletMatch = " and id_pallet = '$pallet'";
        $wherePalletAll = " and id_pallet = 0";
        $whereZoneMatch = " and id_zone = '$zone'";
        $whereZoneAll = " and id_zone = 0";
        $whereDirectionMatch = " and direction = '$direction'";
        $whereDirectionAll = " and direction = 'all'";

        $p = $this->guessBestZoneDirection($db, $baseQry, $wherePalletMatch, $whereZoneMatch, $whereZoneAll, $whereDirectionMatch, $whereDirectionAll);
        if (!$p) {
            $p = $this->guessBestZoneDirection($db, $baseQry, $wherePalletAll, $whereZoneMatch, $whereZoneAll, $whereDirectionMatch, $whereDirectionAll);
        }

        return $p;
    }

    private function guessBestZoneDirection($db, $baseQry, $concatWhere, $whereZoneMatch, $whereZoneAll, $whereDirectionMatch, $whereDirectionAll) {
        /* търсене на съвпадение за точна Зона и точно Направление */
        $where = $whereZoneMatch.$whereDirectionMatch;
        $p = $db->selectRow($baseQry.$where.$concatWhere);
        if (!$p) {
            /* търсене на съвпадение за всяка Зона и точно Направление */
            $where = $whereZoneAll.$whereDirectionMatch;
            $p = $db->selectRow($baseQry.$where.$concatWhere);
            if (!$p) {
                /* търсене на съвпадение за точна Зона и всяко Направление */
                $where = $whereZoneMatch.$whereDirectionAll;
                $p = $db->selectRow($baseQry.$where.$concatWhere);
                if (!$p) {
                    /* търсене на съвпадение за всяка Зона и всяко Направление */
                    $where = $whereZoneAll.$whereDirectionAll;
                    $p = $db->selectRow($baseQry.$where.$concatWhere);
                }
            }
        }

        return $p;
    }

    private function logRequest($type, $palletId, $palletCount, $services, $from, $to, $mediator, $reqInfo, $service, $price) {
        $db = DBC::$main;

        $data = array(
            'request_type' => $type,
            'id_pallet' => $palletId,
            'pallet_count' => $palletCount,
            'stretch' => $services['stretch'],
            'rental' => $services['rental'],
            'from_id_city' => $from['id_city'],
            'from_id_postcode' => $from['pk'],
            'from_dir' => $from['dir'],
            'from_text' => ($from['dir'] == 'address' ? $from['address'] : $from['depo']['name'].' / '.$from['depo']['address']),
            'to_id_city' => $to['id_city'],
            'to_id_postcode' => $to['pk'],
            'to_dir' => $to['dir'],
            'to_text' => ($to['dir'] == 'address' ? $to['address'] : $to['depo']['name'].' / '.$to['depo']['address']),
            'id_mediator' => ($mediator != null ? $mediator['id'] : 0),
            'service_type' => $service,
            'price' => $price,
        );
        if ($services['cd'] != '') {
            $data['cd_value'] = $services['cd'];
        }
        if (isset($reqInfo)) {
            $data['from_client_name'] = $reqInfo['sender_name'];
            $data['from_client_phone'] = $reqInfo['sender_phone'];
            $data['to_client_name'] = $reqInfo['receiver_name'];
            $data['to_client_phone'] = $reqInfo['receiver_phone'];
            $data['pay_side'] = $reqInfo['pay_side'];
            $data['tm_load'] = $reqInfo['load_at'];
        }
        $db->insert('requests', $data);

        return $db->lastInsertID();
    }

    private function doCalc($params, $log_request = true) {
        /* инициализация на входните параметри */
        $pallet = [
            'w' => $params['par_w'],
            'h' => $params['par_h'],
            'l' => $params['par_l'],
            'weight' => $params['par_weight'],
            'count' => $params['par_count']
        ];
        $services = [
            'cd' => $params['cd_value'],
            'stretch' => $params['stretch'],
            'rental' => $params['rental'],
        ];
        $from = [
            'id_city' => $params['from_city']['id'],
            'dir' => $params['from_dir'],
            'pk' => $params['from_pk'],
            'address' => $params['from_address'],
            'office' => $params['from_office'],
        ];
        $to = [
            'id_city' => $params['to_city']['id'],
            'dir' => $params['to_dir'],
            'pk' => $params['to_pk'],
            'address' => $params['to_address'],
            'office' => $params['to_office'],
        ];

        /* инициализация и валидиране на параметрите за тарифиране */
        $pallet['out'] = $this->guessPallet($pallet['w'], $pallet['h'], $pallet['l'], $pallet['weight']);

        if (!isset($pallet['out']['id'])) throw new InvalidParameter('не може да се определи подходящо пале.');
        error_log($pallet['out']['name']);

        if ($from['dir'] == 'office') {
            $deportFrom = $from['office'];
            $from['pk'] = PallexUtils::dbLoadDeports($deportFrom)[0]['id_postcode'];
            $from['depo'] = $this->guessDeport($from['pk']);
        } else {
            $from['depo'] = $this->guessDeport($from['pk']);
            $deportFrom = $from['depo']['id'];
        }
        if (!isset($deportFrom)) throw new InvalidParameter('не може да се определи депо за изпращане.');

        if ($to['dir'] == 'office') {
            $deportTo = $to['office'];
            $to['pk'] = PallexUtils::dbLoadDeports($deportTo)[0]['id_postcode'];
            $to['depo'] = $this->guessDeport($to['pk']);
        } else {
            $to['depo'] = $this->guessDeport($to['pk']);
            $deportTo = $to['depo']['id'];
        }
        if (!isset($deportTo)) throw new InvalidParameter('не може да се определи депо за получаване.');

        if ($from['dir'] == 'office' && $to['dir'] == 'office' && $deportFrom == $deportTo)
            throw new InvalidParameter('не може да се изпраща пратка от офис до същия офис');

        $hub = $this->guessHub($deportFrom, $deportTo);
        if (!isset($hub['id'])) throw new InvalidParameter('не може да се определи транспортния хъб.');

        $zoneFrom = $this->guessZone($from['pk'], $hub['id']);
        if (!isset($zoneFrom)) throw new InvalidParameter('не може да се определи зоната за изпращане.');

        $zoneTo = $this->guessZone($to['pk'], $hub['id']);
        if (!isset($zoneTo)) throw new InvalidParameter('не може да се определи зоната за получаване.');

        /* тарифиране */
        $overload = $pallet['out']['overload'];
        if ($overload > 0) $overload = 1 + $overload/100; else $overload = 1;

        $cdPrice = 0;
        if (isset($services['cd']) && intval($services['cd']) > 0) {
            $cdDBPrice = $this->guessPrices($pallet['out']['id'], $zoneFrom, $from['dir'], $services['cd'], 'cd');
            if (!isset($cdDBPrice) || !$cdDBPrice) throw new InvalidParameter('не може да се определи цена за наложен платеж.'.$pallet['out']['id']);
            if ($cdDBPrice['price_type'] == 'price')
                $cdPrice = $cdDBPrice['price'];
            else
                $cdPrice = (intval($services['cd']) * $cdDBPrice['price']) / 100;
        }

        $stretchPrice = 0;
        if (isset($services['stretch']) && intval($services['stretch']) > 0) {
            $stretchDBPrice = $this->guessPrices($pallet['out']['id'], $zoneFrom, $from['dir'], $pallet['count'], 'stretch');
            if (!isset($stretchDBPrice) || !$stretchDBPrice) throw new InvalidParameter('не може да се определи цена за стречоване.');
            $stretchPrice = $stretchDBPrice['price'];
        }

        $rentPrice = 0;
        if (isset($services['rental']) && intval($services['rental']) > 0) {
            $rentDBPrice = $this->guessPrices($pallet['out']['id'], $zoneFrom, $from['dir'], $pallet['count'], 'rental');
            if (!isset($rentDBPrice) || !$rentDBPrice) throw new InvalidParameter('не може да се определи цена за палети под наем.');
            $rentPrice = $rentDBPrice['price'];
        }

        $priceDBSendStandard = $this->guessPrices($pallet['out']['id'], $zoneFrom, $from['dir'], $pallet['count'], 'standard');
        if (!$priceDBSendStandard) throw new InvalidParameter('не може да се определи цена за стандарт изпращане.');

        $priceDBSendPremium = $this->guessPrices($pallet['out']['id'], $zoneFrom, $from['dir'], $pallet['count'], 'premium');
        if (!$priceDBSendPremium) throw new InvalidParameter('не може да се определи цена за премиум изпращане.');

        $priceDBReceiveStandard = $this->guessPrices($pallet['out']['id'], $zoneTo, $to['dir'], $pallet['count'], 'standard');
        if (!$priceDBReceiveStandard) throw new InvalidParameter('не може да се определи цена за стандарт получаване.');

        $priceDBReceivePremium = $this->guessPrices($pallet['out']['id'], $zoneTo, $to['dir'], $pallet['count'], 'premium');
        if (!$priceDBReceivePremium) throw new InvalidParameter('не може да се определи цена за премиум получаване.');

        $mediatorPrice = 0;

        $mediator = $this->guessMediator($params['mediator_code']);
        if ($mediator != null) $mediatorPrice = $mediator['charge'];

        $priceSendStandard = $priceDBSendStandard['price'];
        $priceSendPremium = $priceDBSendPremium['price'];
        $priceReceiveStandard = $priceDBReceiveStandard['price'];
        $priceReceivePremium = $priceDBReceivePremium['price'];

        $priceStandard = $pricePremium = 0;

        $hubCharge = $hub['charge'];
        $hubOvercharge = $hub['overcharge'];

        /* в зависимост от вида надценка на Хъба пресмятаме цената */
        switch ($hub['overcharge_type']) {
            case 'price' :
                $priceStandard = ($priceSendStandard + $priceReceiveStandard) * $overload + $hubCharge + $hubOvercharge + $mediatorPrice;
                $pricePremium = ($priceSendPremium + $priceReceivePremium) * $overload + $hubCharge + $hubOvercharge + $mediatorPrice;
                break;
            case 'percent':
                $priceStandard = ((($priceSendStandard + $priceReceiveStandard) * $overload + $hubCharge)) * (1 + $hubOvercharge/100) + $mediatorPrice;
                $pricePremium = ((($priceSendPremium + $priceReceivePremium) * $overload + $hubCharge)) * (1 + $hubOvercharge/100) + $mediatorPrice;
                break;
        }

        /* добавяне на допълнителните услуги */
        $priceStandard += ($cdPrice + $stretchPrice + $rentPrice);
        $pricePremium += ($cdPrice + $stretchPrice + $rentPrice);

        $result = array();

        $result['prices'] = [
            'standard' => sprintf("%.02f", $priceStandard),
            'premium' => sprintf("%.02f", $pricePremium)
        ];

        $result['request'] = [
            'id_pallet' => $pallet['out']['id'],
            'pallet_count' => $pallet['count'],
            'services' => $services,
            'from' => $from,
            'to' => $to,
            'mediator' => $mediator
        ];
        if (isset($pallet['out']['oversize']))
            $result['request']['pallet_oversize'] = true;

        if ($log_request) {
            $this->logRequest('calc', $pallet['out']['id'], $pallet['count'], $services, $from, $to, $mediator, null,'standard', $priceStandard);
            $this->logRequest('calc', $pallet['out']['id'], $pallet['count'], $services, $from, $to, $mediator,null, 'premium', $pricePremium);
        }

        return $result;
    }

    public function calculate($aRequest, $log_request = true) {

        /* предварителна валидация на входните данни */
        foreach (['par_w', 'par_l', 'par_h', 'par_weight', 'par_count'] as $k) {
            if (empty($aRequest['fields'][$k]))
                throw new InvalidParameter('Необходимо е да въведете стойност '.$k, array($k));
        }

        if ($aRequest['fields']['from_dir'] == 'address') {
            if (empty($aRequest['fields']['from_pk']))
                throw new InvalidParameter('Необходимо е да въведете стойност за ПК', array('from_pk'));
            if (empty($aRequest['fields']['from_address']))
                throw new InvalidParameter('Необходимо е да въведете стойност за адрес', array('from_address'));
        } else {
            if (empty($aRequest['fields']['from_office']) || $aRequest['fields']['from_office'] == '0')
                throw new InvalidParameter('Необходимо е да въведете стойност за офис', array('from_office'));
        }

        if ($aRequest['fields']['to_dir'] == 'address') {
            if (empty($aRequest['fields']['to_pk']))
                throw new InvalidParameter('Необходимо е да въведете стойност за ПК', array('to_pk'));
            if (empty($aRequest['fields']['to_address']))
                throw new InvalidParameter('Необходимо е да въведете стойност за адрес', array('to_address'));
        } else {
            if (empty($aRequest['fields']['to_office']) || $aRequest['fields']['to_office'] == '0')
                throw new InvalidParameter('Необходимо е да въведете стойност за офис', array('to_office'));
        }

        $result = $this->doCalc($aRequest['fields'], $log_request);
        if (isset($result['prices'])) {
            $result['prices']['standardText'] = 'Стандарт - '.$result['prices']['standard'].' лв.';
            $result['prices']['premiumText'] = 'Премиум - '.$result['prices']['premium'].' лв.';
        }

        /* тарифиране */
        return $result;
    }

    public function doRequest($aRequest) {
        foreach (['sender_name', 'sender_phone', 'receiver_name', 'receiver_phone', 'load_at', 'tariff'] as $k) {
            if (empty($aRequest['fields'][$k]))
                throw new InvalidParameter('Необходимо е да въведете стойност '.$k, array($k));
        }

        $tariff = $aRequest['fields']['tariff'];

        $result = $this->calculate($aRequest, false);

        $price = $tariff == 'standard' ? $result['prices']['standard'] : $result['prices']['premium'];

        $req = $result['request'];
        $reqInfo = [
            'sender_name' => $aRequest['fields']['sender_name'],
            'sender_phone' => $aRequest['fields']['sender_phone'],
            'receiver_name' => $aRequest['fields']['receiver_name'],
            'receiver_phone' => $aRequest['fields']['receiver_phone'],
            'load_at' => PallexUtils::date2dbdate($aRequest['fields']['load_at']),
            'pay_side' => $aRequest['fields']['pay_side'],
        ];

        $result['request']['log_id'] = $this->logRequest('request', $req['id_pallet'], $req['pallet_count'], $req['services'], $req['from'], $req['to'], $req['mediator'], $reqInfo, $tariff, $price);

        return $result;
    }
}