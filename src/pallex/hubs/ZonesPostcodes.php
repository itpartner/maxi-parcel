<?php
namespace pallex\hubs;

use emc\production\schedule\Scheduler;
use main\db\DBC;
use main\Exception;
use main\form\FormPanelHandler;
use main\grid\GridPanelHandler;
use main\InvalidParameter;
use pallex\GlobalHelper;

/**
 * Class Edit
 * @accessLevelDescription Редактиране на метал
 */
class ZonesPostcodes extends GridPanelHandler {
    public function __construct() {
        $this->oBase = DBC::$main;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'code',
                'width' => 100,
                'headerText' => L('ПК'),
                'sortable' => false,
                'resizable' => true,
                'filterable' => false
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'name',
                'width' => 200,
                'headerText' => L('Име'),
                'sortable' => false,
                'resizable' => true,
                'filterable' => false
            )),
            $this->newColumnEditedBy(),
            $this->newColumn(
                array(
                    'type' => GridPanelHandler::CTYPE_BTN,
                    'iconCls' => 'icon fa fa-trash',
                    'dataField' => 'delete',
                    'headerText' => '',
                    'width' => 30,
                    'sortable'  => false,
                    'resizable' => false,
                    'filterable'=> false
                )),
        );

        $this->sReportSelectStatement = "
            select hz.id, pk.code, pk.name,
            CONCAT_WS(' ',pc.fname,pc.mname,pc.lname) AS created_user,
            hz.created_time
            FROM hubs_zones hz
            LEFT JOIN postcodes pk on pk.id = hz.id_postcode
            LEFT JOIN personnel pc ON pc.id = hz.created_user
        ";
        $this->bUseSQLPaging = false;

        parent::__construct();
    }

    public function loadData($aRequest, $nTimeFrom = 0, $nTimeTo = 0)
    {
        $this->aReportWhereStatement[] = 'hz.id_hub = '.intval($aRequest['id']).' and hz.id_zone = \''.intval($aRequest['zone']).'\'';
        unset($aRequest['paging']);

        return parent::loadData($aRequest, $nTimeFrom, $nTimeTo);
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        foreach ($aInitParams['row'] as $k => $v) $this->setField($k, $v);
        $this->setLastEditInfo($aInitParams['row']);

        $this->setField('city', array('id' => $aInitParams['row']['id_city'], 'value' => $aInitParams['row']['city']));

        if (!$aRequest['id']) $aRequest['id'] = $aInitParams['row']['id'];
        if (!$aRequest['zone']) $aRequest['zone'] = $aInitParams['row']['zone'];

        return $this->loadData($aRequest);
    }

    public function addPk($aRequest)
    {
        $dbf = array(
            'id_hub' => $aRequest['id'],
            'zone' => $aRequest['zone'],
            'id_postcode' => $aRequest['fields']['pc']['id']
        );

        if(empty($dbf['id_postcode'])) throw new InvalidParameter('Необходимо е да въведете стойност', array('pc'));

        $zoneId = DBC::$slave->selectRow("SELECT z.name FROM hubs_zones h left join zones z on z.id = h.id_zone WHERE h.id_hub = {$dbf['id_hub']} AND h.id_postcode = {$dbf['id_postcode']}");
        if($zoneId) throw new InvalidParameter(L('Този пощенски код вече е добавен към зона').' '.$zoneId['name']);

        DBC::$main->insert('hubs_zones',$dbf);

        return $this->loadData($aRequest);
    }

    public function delPk($aRequest)
    {
        DBC::$main->delete('hubs_zones', $aRequest['id_pk']);

        return $this->loadData($aRequest);
    }
}