<?php
namespace pallex\hubs;

use main\grid\TPLGridPanel;

class TPLZonesPostcodes extends TPLGridPanel {

    public function __construct($sPrefix, array $aInitParams) {
         $this->{'aConfig'} = array(
            'disablePaging' => true,
            'dataHeight' => 250,
            'topBarItems' => array(
                ['label', array('fieldName' => 'pc', 'text' => L('ПК'))],
                ['input', array('name' => 'pc', 'suggest' => 'method: pallex\hubs\Hubs.suggestPostCode;')],
                ['button', array('class' => 'add', 'iconCls' => 'icon fa fa-plus-circle', 'text' => L('Добави'))]
            ),
            'bottomBarItems' => array(
                array('closebutton'),
            )
        );

        parent::__construct($sPrefix, $aInitParams);

        $this->bPrintWithTitle = true;
        $this->aFrameConfig['title'] = L('Редактиране на зона: '). ' ' . $aInitParams['row']['zoneName'];
    }

    public function printBeforeGrid() { ?>

        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, baseParams) {
            
            container.baseParams.id = baseParams['row']['id'];
            container.baseParams.zone = baseParams['row']['zone'];
            
            $(container).find('.add').click(function() {
                container.request('addPk',{});
            });
            
            container.grid.clickListeners['delete'] = function(rowNum, fieldElement, htmlElement) {
                container.request('delPk',{id_pk: container.grid.getCurrentData()[rowNum]['id']});
            };
            container.window.addListener('close',function() {
                baseParams.setCount(container.grid.getCurrentData().length);
            });
        });

JS;
    }


}