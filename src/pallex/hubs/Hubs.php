<?php
namespace pallex\hubs;

use main\db\DBC;
use main\grid\GridPanelHandler;
use main\suggest\Suggest;
use pallex\PallexUtils;

class Hubs extends GridPanelHandler {
    public function __construct() {
        $this->oBase = DBC::$slave;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'country',
                'width' => 100,
                'headerText' => L('Държава'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true,
                'items' => PallexUtils::dbLoadCountries()
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'code',
                'width' => 100,
                'headerText' => L('Код'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'name',
                'width' => 200,
                'headerText' => L('Име'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'city',
                'width' => 200,
                'headerText' => L('Населено място'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'address',
                'width' => 200,
                'headerText' => L('Адрес'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'nopk',
                'width' => 200,
                'headerText' => L('Без ПК'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumnEditedBy()
        );

        $this->sReportSelectStatement = "
            select h.id, h.code, h.name, h.address, h.charge, h.overcharge, h.overcharge_type, 
            c.name as city, y.name as dcountry, c.id as id_city, y.id as country,
            CONCAT_WS(' ',pc.fname,pc.mname,pc.lname) AS created_user,
            CONCAT_WS(' ',pu.fname,pu.mname,pu.lname) AS updated_user, h.created_time, h.updated_time,
            ( (select count(*) from postcodes) - (select count(*) from hubs_zones where id_hub = h.id) ) as nopk
            FROM hubs h
            LEFT JOIN cities c on c.id = h.id_city
            LEFT JOIN cities y on y.id = c.id_country and y.is_country > 0
            LEFT JOIN personnel pc ON pc.id = h.created_user
            LEFT JOIN personnel pu ON pu.id = h.updated_user
        ";

        $this->aReportWhereStatement[] = 'h.to_arc = 0';
        $this->bUseSQLPaging = false;
        $this->aDefaultSort = array(
            array('field' => 'code', 'dir' => 'ASC')
        );

        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
    }

    public function load($aRequest = array(), $aInitParams = array()) {
        return parent::load($aRequest, $aInitParams);
    }

    public function suggestCity($aRequest) {
        $aResult = array();
        $aResult['select'] 	= "SELECT id, name as value, name as html_value FROM cities pa ";
        $aResult['where'][] = "MATCH(pa.search_text) AGAINST('*{$aRequest['query']}*' IN BOOLEAN MODE) AND pa.to_arc = 0";
        $aResult['group'] 	= 'html_value';

        return Suggest::select($aResult);
    }

    public function suggestPostCode($aRequest) {
        $aResult = array();
        $aResult['select'] 	= "SELECT id, code as value, code as html_value FROM postcodes pa ";
        $aResult['where'][] = "pa.code LIKE '%{$aRequest['query']}%' AND pa.to_arc = 0";
        $aResult['group'] 	= 'html_value';

        return Suggest::select($aResult);
    }
}