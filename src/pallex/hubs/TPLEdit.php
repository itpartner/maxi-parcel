<?php
namespace pallex\hubs;

use main\db\DBC;
use main\TPLBase;
use pallex\PallexUtils;

class TPLEdit extends TPLBase {

    public function __construct($sPrefix, array $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);

        $hub = intval($aInitParams['row']['id']);

        $this->aInitParams['zones'] = array();
        $this->aInitParams['zones_text_template'] = L('Общо {num} ПК');
        $this->aInitParams['zones_total_nopk'] = $aInitParams['row']['nopk'];

        $aZones = PallexUtils::dbLoadZones();
        foreach ($aZones as $k => $v) {
            $qry = "
                select count(*) as cnt from hubs_zones
                WHERE id_hub = $hub and id_zone = '".$v['id']."';
            ";
            $aZones[$k]['cnt'] = DBC::$slave->selectRow($qry)['cnt'];
            $this->aInitParams['zones'][$v['id']] = $aZones[$k];
        }

        $this->bPrintWithTitle = true;
        $this->aFrameConfig['title'] = empty($aInitParams['row']['id']) ? L('Нов Хъб') : L('Редакция на Хъб:') . ' ' . $aInitParams['row']['code'];
    }

    public function printHtml() { ?>
        <style type="text/css">

            #<?= $this->sPrefix ?> .width50 {
                                       box-sizing: border-box;
                                       width: 50px;
                                   }
            #<?= $this->sPrefix ?> td.pad {
                                       padding: 6px 3px;
                                   }
        </style>
        <table style="width: 100%">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName'=>'code', 'text' => L('Код')));?></td>
                            <td><? $this->printElement('input', array('name'=>'code')); ?></td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName'=>'name', 'text' => L('Наименование')));?></td>
                            <td><? $this->printElement('input', array('name'=>'name')); ?></td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName'=>'city' , 'text' => L('Населено място')));?></td>
                            <td><? $this->printElement('input', array('name'=>'city', 'suggest' => 'method: pallex\hubs\Hubs.suggestCity;')); ?></td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName'=>'address', 'text' => L('Адрес')));?></td>
                            <td><? $this->printElement('input', array('name'=>'address')); ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset id="zoneInfo">
                        <? $this->printElement('legend', array('text' => L('Зони'))); ?>
                        <select multiple="multiple" style="height: 100px; width: 100%;" class="zone-links">
                            <?php
                            foreach ($this->aInitParams['zones'] as $k => $v) {
                                echo('<option zone_name="'.$v['text'].'" zone_cnt="'.$v['cnt'].'" value="'.$v['id'].'">'.$v['text'].' / '.$v['cnt'].'</option>');
                            }
                            ?>
                        </select>
                        <div class="zone-display">
                            <span><? echo(L('Налице са')); ?></span>&nbsp;
                            <span id="zone_total"><? echo($this->aInitParams['zones_total_nopk']); ?></span>&nbsp;
                            <span><? echo(L('ПК без уточнена зона')); ?></span>
                        </div>
                    </fieldset>
                    <div style="display: none;" id="zones-template"><? echo($this->aInitParams['zones_text_template']); ?></div>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName'=>'charge', 'text' => L('Такса Хъб')));?></td>
                            <td>
                                <? $this->printElement('input', array('name'=>'charge', 'class' => 'width50', 'keyRestriction' => 'float')); ?>
                                <? $this->printElement('label', array('text' => L('лв.')));?>
                            </td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName'=>'overcharge', 'text' => L('Надценка')));?></td>
                            <td>
                                <? $this->printElement('input', array('name'=>'overcharge', 'class' => 'width50', 'keyRestriction' => 'float')); ?>
                                <? $this->printElement('select', array('name' => 'overcharge_type'), PallexUtils::dbLoadPriceTypes()); ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="toolbar align-right" style="margin-top: 5px">
                        <? $this->printElement('lasteditedinfo'); ?>
                        <? $this->printElement('closebutton'); ?>
                        <? $this->printElement('confirmbutton'); ?>
                        <? $this->printElement('deletebutton'); ?>
                    </div>
                </td>
            </tr>
        </table>

        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, baseParams) {
            //console.log(baseParams);
            var totalZonesNoPk = intval($('#zone_total').html());
            
            function renderText(id, num) {
                let t = $('#zones-template').html();
                t = t.replace(/{num}/g, num);
                $('.zone-links option[value="' + id + '"]').html($('.zone-links option[value="' + id + '"]').attr('zone_name') + ' / ' + num);
            }
            
            function renderZones() {
                if (totalZonesNoPk == 0)
                    $('.zone-display').css('display', 'none');
                else
                    $('.zone-display').css('display', 'block');
            }

            if (baseParams['row']['id'] == 0) {
                $(container).find('.delete').attr({disabled:true});
                $('#zoneInfo').css('display', 'none');
            }
            container.baseParams.id = baseParams['row']['id'];

            $(container).find('.confirm').click(function(){
                container.request('save',{id: baseParams['row']['id']},null, function(res){
                    container.baseParams.id = res.id;
                    
                    //if (baseParams['row']['id'] > 0)
                        $(container).find('.close').click();
                    
                    baseParams['row']['id'] = res.id;
                    //$('#zoneInfo').css('display', 'block');
                    
                    baseParams.getParentGrid().loadData();
                });
            });

            $(container).find('.delete').click(function() {
                container.request('delete', {}, null, function() {
                    $(container).find('.close').click();
                    baseParams.getParentGrid().loadData();
                });
            });
            
            $(container).find('.zone-links option').bind('dblclick', function() {
                let zoneID = this.getAttribute('value');
                let zoneName = this.getAttribute('zone_name');
                framework.createWindow({
                    tplName: 'pallex/hubs/ZonesPostcodes',
                    baseParams: {
                        row: {id: baseParams['row']['id'], zone: zoneID, zoneName: zoneName }, 
                        setCount: function(c) {
                            //console.log(c);
                            c = intval(c);
                            let zv = intval($('.zone-links option[value="' + zoneID + '"]').attr('zone_cnt'));
                            
                            if (c > zv)
                                totalZonesNoPk -= (c - zv);
                            else
                                totalZonesNoPk += (zv - c);
                            
                            renderText(zoneID, c);
                            
                            $('.zone-links option[value="' + zoneID + '"]').attr('zone_cnt', c);
                            $('#zone_total').html(totalZonesNoPk);
                            
                            if (totalZonesNoPk == 0)
                                $('.zone-display').css('display', 'none');
                            else
                                $('.zone-display').css('display', 'block');
                        }
                    }
                });
            });
            
            renderZones();
            
            container.window.addListener('close',function() {
                baseParams.getParentGrid().loadData();
            });
        });
JS;
    }


}