<?php
namespace pallex\hubs;

use main\db\DBC;
use main\db\DBConnection;
use main\grid\GridPanelHandler;
use main\grid\TPLGridPanel;
use pallex\PallexUtils;

class TPLHubs extends TPLGridPanel {
    public function __construct($sPrefix, $aInitParams = array()) {
        $this->{'aConfig'} = array(
            'title' => L('Хъбове'),
            'autoVResize' => true,
            'topBarItems' => array(
                array('label', array('fieldName' => 'country', 'text' => L('Държава'))),
                array('select', array('name' => 'country'), PallexUtils::dbLoadCountries()),
                array('button', array('name' => 'add', 'class' => 'add', 'iconCls' => 'icon fa fa-plus-circle', 'text' => L('Добави')))
            ),
            'bottomBarItems' => array(
                array('exportbutton', array('align' => 'right'))
            )
        );
        parent::__construct($sPrefix, $aInitParams);
    }


    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, aBaseParams) {
            $(container).find('.add').click(function() {
                framework.createWindow({
                    tplName: 'pallex/hubs/Edit',
                    baseParams: {row: {id: 0}, getParentGrid: function() {return container.grid;}}
                });
            });

            container.grid.clickListeners['code'] = function(rowNum, fieldElement, htmlElement) {
                framework.createWindow({
                    tplName: 'pallex/hubs/Edit',
                    baseParams: { row: container.grid.getCurrentData()[rowNum], getParentGrid:function(){return container.grid;}}
                });
            };
            
            $('select[name="country"]').val(1033);
        });
JS;
    }
}