<?php
namespace pallex;

spl_autoload_register(function($name) {
    if (preg_match("/^" . __NAMESPACE__ . "\\\\([a-zA-Z\\d\\\\]+)$/", $name, $matches)) include dirname(__FILE__) . '/' . str_replace('\\', '/', $matches[1]) . '.php';
});