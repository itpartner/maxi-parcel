<?php
namespace pallex\requests;

use main\db\DBC;
use main\grid\GridPanelHandler;
use main\suggest\Suggest;
use pallex\PallexUtils;

class Requests extends GridPanelHandler {
    public function __construct() {
        $this->oBase = DBC::$slave;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'tm_attempt',
                'width' => 100,
                'headerText' => L('Дата'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'request_type',
                'width' => 100,
                'headerText' => L('Тип'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true,
                'items' => array(
                    'calc' => ['id' => 'calc', 'text' => 'Калкулация'],
                    'request' => ['id' => 'request', 'text' => 'Заявка'],
                    'completed' => ['id' => 'completed', 'text' => 'Реализирани']
                )
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'pallet',
                'width' => 100,
                'headerText' => L('Палет'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_INT,
                'dataField' => 'count',
                'width' => 50,
                'headerText' => L('Брой'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'services',
                'width' => 100,
                'headerText' => L('Допъл. услуги'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'sender_city',
                'width' => 100,
                'headerText' => L('Подател нас. място'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'sender_pk',
                'width' => 100,
                'headerText' => L('Подател ПК'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'from_text',
                'width' => 100,
                'headerText' => L('Подател Направление'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'from_client_name',
                'width' => 100,
                'headerText' => L('Подател Име'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'from_client_phone',
                'width' => 100,
                'headerText' => L('Подател Тел.'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'receiver_city',
                'width' => 100,
                'headerText' => L('Получател нас. място'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'receiver_pk',
                'width' => 100,
                'headerText' => L('Получател ПК'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'to_text',
                'width' => 100,
                'headerText' => L('Получател Направление'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'to_client_name',
                'width' => 100,
                'headerText' => L('Получател Име'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'to_client_phone',
                'width' => 100,
                'headerText' => L('Получател Тел.'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'pay_side',
                'width' => 100,
                'headerText' => L('Платец'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true,
                'items' => array(
                    'sender' => ['id' => 'sender', 'text' => 'Подател'],
                    'receiver' => ['id' => 'receiver', 'text' => 'Получател']
                )
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'mediator',
                'width' => 100,
                'headerText' => L('Посредник'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_DATE,
                'dataField' => 'tm_load',
                'width' => 100,
                'headerText' => L('Товарене на'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'service_type',
                'width' => 100,
                'headerText' => L('Услуга'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true,
                'items' => array(
                    'standard' => ['id' => 'standard', 'text' => 'Стандарт'],
                    'premium' => ['id' => 'premium', 'text' => 'Премиум'],
                )
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_FLOAT,
                'dataField' => 'price',
                'width' => 100,
                'headerText' => L('Цена лв.'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'loading_num',
                'width' => 100,
                'headerText' => L('Товарителница №'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumnEditedBy()
        );

        $this->sReportSelectStatement = "
            select r.id, r.tm_attempt, r.request_type, r.pallet_count as count, r.loading_num, r.price,
            r.from_client_name, r.from_client_phone, r.tm_load, r.pay_side, r.service_type, m.name as mediator,
            r.to_client_name, r.to_client_phone, pal.name as pallet, r.to_text, r.from_text,
            sc.name as sender_city, rc.name as receiver_city, spk.code as sender_pk, rpk.code as receiver_pk,
            CONCAT_WS(' ',pc.fname,pc.mname,pc.lname) AS created_user,
            CONCAT_WS(' ',pu.fname,pu.mname,pu.lname) AS updated_user, r.created_time, r.updated_time,
            concat(
                if (not isnull(r.cd_value),concat('CD ', r.cd_value),''), ' ',
                if (r.stretch > 0, 'Stretch', ''), ' ',
                if (r.rental > 0, 'Rental', '')
            ) as services
            FROM requests r 
            LEFT JOIN mediators m on m.id = r.id_mediator
            LEFT JOIN pallets pal on pal.id = r.id_pallet
            LEFT JOIN cities sc on sc.id = r.from_id_city
            LEFT JOIN cities rc on rc.id = r.to_id_city
            LEFT JOIN postcodes spk on spk.id = r.from_id_postcode
            LEFT JOIN postcodes rpk on rpk.id = r.to_id_postcode
            LEFT JOIN personnel pc ON pc.id = r.created_user
            LEFT JOIN personnel pu ON pu.id = r.updated_user
        ";

        $this->aReportWhereStatement[] = 'r.tm_attempt between from_unixtime(unix_timestamp(now())-7*24*60*60) and now()';
        $mAccess = PallexUtils::dbLoadMediatorAccess($_SESSION['userdata']['id']);
        if (!PallexUtils::isMediatorAllAccess($mAccess))
            $this->aReportWhereStatement[] = "id_mediator in (select id_mediator from access_mediators where id_person = ".intval($_SESSION['userdata']['id']).")";
        $this->aDefaultSort = array(
            array('field' => 'tm_attempt', 'dir' => 'DESC')
        );

        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
    }

    public function load($aRequest = array(), $aInitParams = array()) {
        return parent::load($aRequest, $aInitParams);
    }

    public function filter($aRequest) {
        $df = PallexUtils::date2dbdate($aRequest['fields']['from_date']);
        $dt = PallexUtils::date2dbdate($aRequest['fields']['to_date']);
        $db = DBC::$slave;
        $reqType = $db->escape($aRequest['fields']['req_type']);
        if ($reqType == "all")
            $this->aReportWhereStatement[] = "r.tm_attempt between '$df' and '$dt'";
        else
            $this->aReportWhereStatement[] = "r.tm_attempt between '$df' and '$dt' and request_type = '$reqType'";
        $mAccess = PallexUtils::dbLoadMediatorAccess($_SESSION['userdata']['id']);
        if (!PallexUtils::isMediatorAllAccess($mAccess))
            $this->aReportWhereStatement[] = "id_mediator in (select id_mediator from access_mediators where id_person = ".intval($_SESSION['userdata']['id']).")";
        //$this->aReportWhereStatement[] = "1=1";
        return $this->loadData();
    }
}