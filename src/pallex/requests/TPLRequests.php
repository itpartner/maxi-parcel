<?php
namespace pallex\requests;

use main\db\DBC;
use main\grid\GridPanelHandler;
use main\grid\TPLGridPanel;

class TPLRequests extends TPLGridPanel {
    public function __construct($sPrefix, $aInitParams = array()) {
        $tmFrom = date('d.m.Y', time() - 7*24*60*60);
        $tmTo = date('d.m.Y', time());
        $this->{'aConfig'} = array(
            'title' => L('Заявки'),
            'autoVResize' => true,
            'topBarItems' => array(
                array('select', array('name' => 'req_type'), array(
                    ['id' => 'all', 'value' => L('Всички')],
                    ['id' => 'calc', 'value' => L('Калкулация')],
                    ['id' => 'request', 'value' => L('Заявка')],
                    ['id' => 'completed', 'value' => L('Реализирани')],
                )),
                array('label', array('fieldName' => 'from_date', 'text' => L('От'))),
                array('input', array('name' => 'from_date', 'value' => $tmFrom, 'class' => 'datepicker')),
                array('label', array('fieldName' => 'to_date', 'text' => L('До'))),
                array('input', array('name' => 'to_date', 'value' => $tmTo, 'class' => 'datepicker')),
                array('button', array('name' => 'search', 'class' => 'search', 'iconCls' => 'icon fa fa-search', 'text' => L('Търси')))
            ),
            'bottomBarItems' => array(
                array('exportbutton', array('align' => 'right'))
            )
        );
        parent::__construct($sPrefix, $aInitParams);
    }


    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, aBaseParams) {
            $(container).find('.search').click(function() {
                container.request('filter', {}, null, function(res) {
                    container.grid.setData(res.gridData);
                });
            });
            
            container.fields['req_type'].addListener('change', function() {
                container.request('filter', {}, null, function(res) {
                    container.grid.setData(res.gridData);
                });
            });

            container.grid.clickListeners['tm_attempt'] = function(rowNum, fieldElement, htmlElement) {
                framework.createWindow({
                    tplName: 'pallex/requests/Edit',
                    baseParams: { 
                        row: container.grid.getCurrentData()[rowNum], 
                        getParentGrid:function(){return container.grid;},
                        getBtnSearch:function() { return $(container).find('.search'); }
                    }
                });
            };
            
            $(container.fields['from_date']).datepicker({
                    dateFormat: 'dd.mm.yy',
                    firstDay: 1
                }).attr({
                    autocomplete: 'off'
                });
            
            $(container.fields['to_date']).datepicker({
                    dateFormat: 'dd.mm.yy',
                    firstDay: 1
                }).attr({
                    autocomplete: 'off'
                });
        });
JS;
    }
}