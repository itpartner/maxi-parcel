<?php
namespace pallex\requests;

use emc\production\schedule\Scheduler;
use main\db\DBC;
use main\Exception;
use main\form\FormPanelHandler;
use main\InvalidParameter;

/**
 * Class Edit
 * @accessLevelDescription Заявки
 */
class Edit extends FormPanelHandler {

    public function init($aRequest = array(), $aInitParams = array()) {
        foreach ($aInitParams['row'] as $k => $v) $this->setField($k, $v);
        $this->setLastEditInfo($aInitParams['row']);

        return $this->oResponse;
    }

    public function save($aRequest) {
        return DBC::$main->withTrans(function ($aRequest) {
            $metal = $aRequest['fields'];
            $metal['id'] = intval($aRequest['id']);
            $metal = array_map('trim', $metal);

            foreach (array('loading_num') as $k)
                if(empty($metal[$k])) throw new InvalidParameter('Необходимо е да въведете стойност', array($k));

            $metal['request_type'] = 'completed';

            if(DBC::$slave->selectRow("SELECT id FROM requests WHERE id != {$metal['id']} AND loading_num = " . DBC::$slave->quote($metal['loading_num']))) throw new InvalidParameter(L('Вече съществува Заявка с този товарителница.'));

            DBC::$main->update('requests', $metal);

            return $this->oResponse;
        }, [$aRequest]);
    }
}