<?php
/**
 * Created by PhpStorm.
 * User: spetrov
 * Date: 03.10.18
 * Time: 20:52
 */

namespace pallex;


use main\db\DBC;
use main\InvalidParameter;

class PallexUtils
{
    public static function dbLoadCountries()
    {
        $a = array();
        $res = DBC::$slave->select("select id, name from cities where is_country > 0 and to_arc = 0;");
        foreach ($res as $k => $v) {
            $a[$v['id']] = [
                'id' => $v['id'],
                'value' => $v['name'],
                    'text' => $v['name']
            ];
        }
        return $a;
    }

    public static function dbLoadPks($id_city = null)
    {
        $a = array();

        $qry = "select id, name from postcodes where to_arc = 0";
        if (isset($id_city)) $qry .= " and id_city = ".intval($id_city);
        $qry .= ";";

        $res = DBC::$slave->select($qry);
        foreach ($res as $k => $v) {
            $a[$v['id']] = [
                'id' => $v['id'],
                'value' => $v['name'],
                'text' => $v['name']
            ];
        }
        return $a;
    }

    public static function dbLoadDeports($id = null)
    {
        $qry = "select * from deports where to_arc = 0";
        if (isset($id)) $qry .= " and id = ".intval($id);
        $qry .= ";";

        $res = DBC::$slave->select($qry);

        return $res;
    }

    public static function dbLoadDirections($includeAll = false)
    {
        $a = array(
            'office' => ['id' => 'office'],
            'address' => ['id' => 'address'],
        );

        if ($includeAll) {
            $a['all'] = array('id' => 'all');
        }

        $a = array_reverse($a, true);

        foreach ($a as $k => $v) {
            $a[$k]['value'] = PallexUtils::translateDirection($v['id']);
            $a[$k]['text'] = PallexUtils::translateDirection($v['id']);
        }

        return $a;
    }

    public static function dbLoadServices()
    {
        $a = array(
            'standard' => ['id' => 'standard'],
            'premium' => ['id' => 'premium'],
            'cd' => ['id' => 'cd'],
            'rental' => ['id' => 'rental'],
            'stretch' => ['id' => 'stretch'],
        );
        foreach ($a as $k => $v) {
            $a[$k]['value'] = PallexUtils::translateServices($v['id']);
            $a[$k]['text'] = PallexUtils::translateServices($v['id']);
        }

        return $a;
    }

    public static function dbLoadPriceTypes()
    {
        $a = array(
            ['id' => 'price'],
            ['id' => 'percent'],
        );
        foreach ($a as $k => $v) {
            $a[$k]['value'] = PallexUtils::translatePriceType($v['id']);
            $a[$k]['text'] = PallexUtils::translatePriceType($v['id']);
        }

        return $a;
    }

    public static function dbLoadZones($includeAll = false)
    {
        $a = array();
        $res = DBC::$slave->select("select id, name from zones;");
        if ($includeAll) {
            $a[0] = [
                'id' => 0,
                'value' => PallexUtils::translateZones(0, null),
                'text' => PallexUtils::translateZones(0, null)
            ];
        }
        foreach ($res as $k => $v) {
            $a[$v['id']] = [
                'id' => $v['id'],
                'value' => $v['name'],
                'text' => $v['name']
            ];
        }

        return $a;
    }

    public static function dbLoadPallets()
    {
        $res = DBC::$slave->select("select id, name as value from pallets where to_arc = 0;");

        array_push($res, ['id' => '0', 'value' => L('Всички')]);

        return array_reverse($res, true);
    }

    public static function dbLoadMediators()
    {
        $res = DBC::$slave->select("select id, name as value from mediators where to_arc = 0;");

        array_push($res, ['id' => '0', 'value' => L('Всички')]);

        return array_reverse($res, true);
    }

    public static function dbLoadMediatorAccess($idPerson)
    {
        $res = DBC::$slave->select("
          select a.id_mediator as id, if (a.id_mediator>0,m.name,'".L('Всички')."') as value 
          from access_mediators a left join mediators m on m.id = a.id_mediator 
          where a.id_person = '$idPerson';");

        return $res;
    }

    public static function isMediatorAllAccess($mediators)
    {
        foreach ($mediators as $k => $v) {
            if ($v['id'] == '0') return true;
        }

        return false;
    }

    public static function translateZones($zone, $name)
    {
        switch ($zone)
        {
            case '0' : return L('Всички');
            default: return $name;
        }
    }

    public static function translateServices($service)
    {
        switch ($service)
        {
            case 'all' : return L('Всички');
            case 'cd' : return L('CD');
            case 'standard': return L('Стандарт');
            case 'premium': return L('Премиум');
            case 'rental' : return L('Палети под наем');
            case 'stretch' : return L('Стречоване');
            default: throw new InvalidParameter('invalid service. ('.$service.')');
        }
    }

    public static function translateDirection($dir)
    {
        switch ($dir)
        {
            case 'all' : return L('Всички');
            case 'address' : return L('Адрес');
            case 'office': return L('Офис');
            default: throw new InvalidParameter('invalid direction. ('.$dir.')');
        }
    }

    public static function translatePriceType($priceType)
    {
        switch ($priceType)
        {
            case 'percent' : return '%';
            case 'price' : return L('лв.');
            default: throw new InvalidParameter('invalid price type. ('.$priceType.')');
        }
    }

    public static function date2dbdate($date) {
        /* $date format is dd.mm.yyyy */
        $ex = explode('.', $date);

        return $ex[2].'-'.$ex[1].'-'.$ex[0];//return yyyy-mm-dd
    }

    public static function isValidMediatorCode($mediatorCode) {
        $mediatorCode = DBC::$slave->escape($mediatorCode);

        $code = DBC::$slave->selectRow("select id from mediators where code = '$mediatorCode'");
        if (!$code) return false;

        return $code['id'];
    }

    public static function captchaProvider($text) {
        $bg = imagecreatefrompng(dirname(__FILE__).'/../../images/captcha_background.png');
        $img = imagecreate(90, 31);

        imagecopyresized($img, $bg, 0, 0, 0, 0, 100, 33, 200, 80);
        imagesavealpha($img, true);
        $background = imagecolorallocatealpha($img, 255, 255, 255, 0);
        $color = imagecolorallocate($img, 0, 0, 0);

        imagettftext($img, 22, 9, 11, 31, $color, dirname(__FILE__).'/../../images/DejaVuSansMono.ttf', $text);
        header( "Content-type: image/png" );
        imagepng($img);
        imagecolordeallocate($color);
        imagecolordeallocate($background);
        imagedestroy($img);
        imagedestroy($bg);
    }
}