<?php
namespace pallex\transportMatrix;

use main\TPLBase;

class TPLEdit extends TPLBase {

    public function __construct($sPrefix, array $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);

        $this->bPrintWithTitle = true;
        $this->aFrameConfig['title'] = empty($aInitParams['id']) ? L('Нов Хъб') : L('Редакция на Хъб');
    }

    public function printHtml() { ?>

        <table style="width: 100%">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName'=>'hub' , 'text' => L('Хъб')));?></td>
                            <td><? $this->printElement('input', array('name'=>'hub', 'suggest' => 'method: pallex\transportMatrix\TransportMatrix.suggestHub;')); ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="toolbar align-right" style="margin-top: 5px">
                        <?
                        if (!empty($this->aInitParams['id'])) {
                            $this->printElement('lasteditedinfo');
                        } ?>
                        <? $this->printElement('closebutton'); ?>
                        <? $this->printElement('confirmbutton'); ?>
                        <? $this->printElement('button', array('class' => 'delete_metal align-right', 'text' => L('Изтрий'), 'name' => 'delete_metal', 'iconCls' => 'icon fa fa-trash')) ?>
                    </div>
                </td>
            </tr>
        </table>

        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, baseParams) {

            if (baseParams['id'] == 0) {
                $(container).find('.delete_metal').css('display', 'none');
            }

            $(container).find('.confirm').click(function(){
                container.request('save',{id: baseParams['id'], d_src: baseParams['d_src'], d_dst: baseParams['d_dst']},null, function(){
                    $(container).find('.close').click();
                    baseParams.getParentGrid().loadData();
                });
            });

            $(container).find('.delete_metal').click(function() {
                container.request('delete', {id: baseParams['id']}, null, function() {
                    $(container).find('.close').click();
                    baseParams.getParentGrid().loadData();
                });
            });
            
            container.window.addListener('close',function() {
                baseParams.getParentGrid().loadData();
            });
        });

JS;
    }


}