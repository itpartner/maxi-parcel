<?php
namespace pallex\transportMatrix;

use main\db\DBC;
use main\grid\GridPanelHandler;
use main\grid\TPLGridPanel;

class TPLTransportMatrix extends TPLGridPanel {
    public function __construct($sPrefix, $aInitParams = array()) {
        $this->{'aConfig'} = array(
            'title' => L('Транспортна Матрица'),
            'autoVResize' => true,
            'topBarItems' => array(
            ),
            'bottomBarItems' => array(
                array('exportbutton', array('align' => 'right'))
            )
        );
        parent::__construct($sPrefix, $aInitParams);
    }


    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, aBaseParams) {
            $(container).delegate('.hlink', 'click', function(e) {
                let d_src = $(this).attr('d_src');
                let d_dst = $(this).attr('d_dst');
                let m_id = $(this).attr('m_id');
                let hub = $(this).html();
                
                framework.createWindow({
                    tplName: 'pallex/transportMatrix/Edit',
                    baseParams: { id: m_id, d_src: d_src, d_dst: d_dst, hub: hub, getParentGrid:function(){return container.grid;} }
                });
            });
        });
JS;
    }
}