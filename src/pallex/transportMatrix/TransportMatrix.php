<?php
namespace pallex\transportMatrix;

use main\db\DBC;
use main\grid\GridPanelHandler;
use main\suggest\Suggest;

class TransportMatrix extends GridPanelHandler {
    private $matrix = array();

    public function __construct() {
        $this->oBase = DBC::$slave;

        $deports = DBC::$slave->select("select id, name from deports where to_arc = 0 order by id asc;");

        array_push($this->aColumns, $this->newColumn(array('headerText' => '','dataField' => 'name','sortable' => false,
            'resizable' => true,
            'filterable' => false)));
        foreach ($deports as $k => $v) {
            array_push($this->aColumns,
                $this->newColumn(array(
                    'type' => GridPanelHandler::CTYPE_HTML,
                    'dataField' => 'name'.$v['id'],
                    'width' => 200,
                    'headerText' => $v['name'],
                    'sortable' => false,
                    'resizable' => true,
                    'filterable' => false
                )));
            $this->matrix[$v['id']]['name'] = $v['name'];
        }

        foreach ($this->matrix as $k => $v) {
            foreach ($this->matrix as $ik => $iv) {
                if ($k == $ik) $this->matrix[$k]['name'.$ik] = '';
                else $this->matrix[$k]['name'.$ik] = '<a class="hlink" href="javascript:void(0);" m_id="0" d_src="'.$k.'" d_dst="'.$ik.'">'.(L('няма')).'</a>';
            }
        }

        $maches = DBC::$slave->select("
          select t.id, deport_from, deport_to, id_hub, h.name
          from transport_matrix t
          left join hubs h on h.id = t.id_hub;
        ");

        foreach ($maches as $k => $v) {
            $this->matrix[$v['deport_from']]['name'.$v['deport_to']] = '<a class="hlink" href="javascript:void(0);" m_id="'.$v['id'].'" d_src="'.$v['deport_from'].'" d_dst="'.$v['deport_to'].'">'.$v['name'].'</a>';
        }

        $this->sReportSelectStatement = "
          select 1 + 1
          ";

        $this->aReportWhereStatement[] = '0';

        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
    }

    public function getGridData($aRequest, $nTimeFrom = 0, $nTimeTo = 0)
    {
        return $this->matrix;
    }

    public function load($aRequest = array(), $aInitParams = array()) {
        return parent::load($aRequest, $aInitParams);
    }

    public function suggestHub($aRequest) {
        $aResult = array();
        $aResult['select'] 	= "SELECT id, name as value, name as html_value FROM hubs pa ";
        $aResult['where'][] = "name like ('%{$aRequest['query']}%') AND pa.to_arc = 0";
        $aResult['group'] 	= 'html_value';

        return Suggest::select($aResult);
    }
}