<?php
namespace pallex\transportMatrix;

use emc\production\schedule\Scheduler;
use main\db\DBC;
use main\Exception;
use main\form\FormPanelHandler;
use main\InvalidParameter;

/**
 * Class Edit
 * @accessLevelDescription Редактиране на Транспортна Матрица
 */
class Edit extends FormPanelHandler {

    public function init($aRequest = array(), $aInitParams = array()) {
        foreach ($aInitParams['row'] as $k => $v) $this->setField($k, $v);
        $this->setLastEditInfo($aInitParams['row']);

        if ($aInitParams['id'] > '0')
        {
            $hubId = DBC::$main->selectRow('select id_hub from transport_matrix where id = '.intval($aInitParams['id'].';'));
            $hubId = $hubId['id_hub'];

            $this->setField('hub', array('id' => $hubId, 'value' => $aInitParams['hub']));
        }

        return $this->oResponse;
    }

    public function save($aRequest) {
        return DBC::$main->withTrans(function ($aRequest) {
            $metal = $aRequest['fields'];
            $metal['id'] = intval($aRequest['id']);
            $metal = array_map('trim', $metal);

            if (empty($aRequest['fields']['hub']['id'])) throw new InvalidParameter('Необходимо е да въведете стойност', array('city'));

            $metal['deport_from'] = $aRequest['d_src'];
            $metal['deport_to'] = $aRequest['d_dst'];
            $metal['id_hub'] = $aRequest['fields']['hub']['id'];

            DBC::$main->update('transport_matrix', $metal);

            return $this->oResponse;
        }, [$aRequest]);
    }

    public function delete($aRequest) {
        $metal = $aRequest['fields'];
        $metal['id'] = intval($aRequest['id']);

        if(empty($metal['id'])) throw new InvalidParameter;

        DBC::$main->delete('transport_matrix', $metal['id']);
        return $this->oResponse;
    }
}