<?php
namespace pallex\postcodes;

use emc\production\schedule\Scheduler;
use main\db\DBC;
use main\Exception;
use main\form\FormPanelHandler;
use main\InvalidParameter;

/**
 * Class Edit
 * @accessLevelDescription Редактиране на Пощенски код
 */
class Edit extends FormPanelHandler {

    public function init($aRequest = array(), $aInitParams = array()) {
        foreach ($aInitParams['row'] as $k => $v) $this->setField($k, $v);
        $this->setLastEditInfo($aInitParams['row']);

        $this->setField('city', array('id' => $aInitParams['row']['id_city'], 'value' => $aInitParams['row']['city']));

        return $this->oResponse;
    }

    public function save($aRequest) {
        return DBC::$main->withTrans(function ($aRequest) {
            $metal = $aRequest['fields'];
            $metal['id'] = intval($aRequest['id']);
            $metal = array_map('trim', $metal);

            foreach (array('code', 'name') as $k)
                if(empty($metal[$k])) throw new InvalidParameter('Необходимо е да въведете стойност', array($k));

            if (empty($aRequest['fields']['city']['id'])) throw new InvalidParameter('Необходимо е да въведете стойност', array('city'));

            if(DBC::$slave->selectRow("SELECT id FROM postcodes WHERE id != {$metal['id']} AND code = " . DBC::$slave->quote($metal['code']))) throw new InvalidParameter(L('Вече съществува ПК с този код.'));

            $metal['id_city'] = $aRequest['fields']['city']['id'];

            DBC::$main->update('postcodes', $metal);

            return $this->oResponse;
        }, [$aRequest]);
    }

    public function delete($aRequest) {
        $metal = $aRequest['fields'];
        $metal['id'] = intval($aRequest['id']);

        if(empty($metal['id'])) throw new InvalidParameter;

        DBC::$main->delete('postcodes', $metal['id']);
        return $this->oResponse;
    }
}