<?php
namespace pallex\postcodes;

use main\db\DBC;
use main\grid\GridPanelHandler;
use main\suggest\Suggest;

class PostCodes extends GridPanelHandler {
    public function __construct() {
        $this->oBase = DBC::$slave;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'country',
                'width' => 100,
                'headerText' => L('Държава'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'code',
                'width' => 100,
                'headerText' => L('ПК'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'city',
                'width' => 200,
                'headerText' => L('Град'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'name',
                'width' => 200,
                'headerText' => L('Населено място'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumnEditedBy()
        );

        $this->sReportSelectStatement = "
            select p.id, p.code, p.name, c.name as city, y.name as country, c.id as id_city,
            CONCAT_WS(' ',pc.fname,pc.mname,pc.lname) AS created_user,
            CONCAT_WS(' ',pu.fname,pu.mname,pu.lname) AS updated_user, p.created_time, p.updated_time
            FROM postcodes p 
            LEFT JOIN cities c on c.id = p.id_city
            LEFT JOIN cities y on y.id = c.id_country and y.is_country > 0
            LEFT JOIN personnel pc ON pc.id = p.created_user
            LEFT JOIN personnel pu ON pu.id = p.updated_user
        ";

        $this->aReportWhereStatement[] = 'p.to_arc = 0';
        //$this->sReportGroupStatement = '';
        $this->bUseSQLPaging = false;
        $this->aDefaultSort = array(
            array('field' => 'code', 'dir' => 'ASC')
        );

        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
    }

    public function load($aRequest = array(), $aInitParams = array()) {
        return parent::load($aRequest, $aInitParams);
    }

    public function suggestCity($aRequest) {
        $aResult = array();
        $aResult['select'] 	= "SELECT id, name as value, name as html_value FROM cities pa ";
        $aResult['where'][] = "MATCH(pa.search_text) AGAINST('*{$aRequest['query']}*' IN BOOLEAN MODE) AND pa.to_arc = 0";
        $aResult['group'] 	= 'html_value';

        return Suggest::select($aResult);
    }
}