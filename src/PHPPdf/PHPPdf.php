<?php

use main\language\Language;

if(class_exists('FPDF') != true)
{
	require_once 'fpdf.php';
}

class PDF extends FPDF {
	public $aMyGridData;
	public $aMargin = array(
		'left'   => 20,
		'top'    => 25, 
		'right'  => 10,
		'bottom' => 18
	); 
	public $aHeader = array(
		'left'  => 20,
		'top'   => 10,
		'right' => 10
	); 
	public $aTables = array(
		'Caption' => array(
			'TextColor'   => 0,
			'Background'  => 210,
			'BorderColor' => 255,
			'BorderWidth' => 0.2,
			'FontSize'	  => 5.5,
			'Height' 	  => 5 
		),
		'Rows'    => array(
			'Background' => array(
				0 => 240,
				1 => 255
			),
			'TextColor'   => 0,
			'BorderColor' => 255,
			'BorderWidth' => 0.2,
			'FontSize'    => 5.5,
			'Height'      => 3.7
		),
		'Total'   => array(
			'Background'  => 210,
			'TextColor'   => 0,
			'BorderColor' => 255,
			'BorderWidth' => 0.2,
			'FontSize'    => 6,
			'Height'      => 5
		)
	);

	public $_PageWidth  = null;
	public $_PageHeight = null;
	
	public function PDF($orientation='P') {
		FPDF::FPDF($orientation);

		$this->AddFont('FreeSans', '', 'FreeSans.php');
		$this->AddFont('FreeSans', 'B', 'FreeSansB.php');
		$this->SetMargins( $this->aMargin['left'], $this->aMargin['top'], $this->aMargin['right'] );
		$this->_PageWidth  = $orientation == 'P' ? 210 : 297;
		$this->_PageHeight = $orientation == 'P' ? 297 : 210;
		$this->SetAuthor('IT Partner 2010');
		$this->SetCreator('Plamen Dimitrov');
		$this->AliasNbPages();
	}

	public function Cell($w, $h=0, $txt='', $border=0, $ln=0, $align='', $fill=0, $link='') {
		FPDF::Cell($w, $h, iconv('UTF-8', 'CP1251', $txt), $border, $ln, $align, $fill, $link);
	}

	public function Header() {
		$this->SetFont('FreeSans', 'B', 18);
		$this->Image('images/logo.gif', $this->aHeader['left'], $this->aHeader['top' ], 0, $this->aMargin['top'] - $this->aHeader['top']);
		$this->SetY($this->aHeader['top']);
		
		$nOldTitleSize = $this->FontSize;
		
		if(isset($this->nTitleSize) && $this->nTitleSize !== null) {
			$this->SetFontSize($this->nTitleSize);
		}
		$this->Cell(0, $this->aMargin['top'] - $this->aHeader['top'], $this->title, 0, 0, 'R');
		
		$this->SetFontSize($nOldTitleSize);
		
		$this->SetDrawColor(0);
		$this->SetLineWidth(0.5);
		$this->Line($this->aMargin['left'], $this->aMargin['top'] - 1, $this->_PageWidth -  $this->aMargin['right'],  $this->aMargin['top'] - 1);
		$this->Ln($this->aMargin['top'] - $this->aHeader['top'] + 2);
	}

	public function Footer() {
		$this->SetFont('FreeSans', '', 6);
		$this->SetDrawColor(90);
		$this->SetLineWidth(0.2);
		$this->Line($this->aMargin['left'], $this->_PageHeight - $this->aMargin['bottom'], $this->_PageWidth -  $this->aMargin['right'], $this->_PageHeight  - $this->aMargin['bottom']);
		$this->SetY( - $this->aMargin['bottom']);
		$this->Cell(0, 6, L('Генериран в') . date(" H:i:s d.m.Y г."), 0, 0, 'R');
		$this->SetY( - $this->aMargin['bottom']);
		$this->Cell(0, 6, L('Стр. ') . $this->PageNo() . ' / {nb} ', 0, 0, 'C');
		$this->SetFont('FreeSans', '', 4);
		$this->SetY( - $this->aMargin['bottom']);
		$this->Cell(0, 6, '', 0, 0, 'L');
	}
	
	public function MakeColWidth($aData, $nTableWidth) {
		$aWidths 	   = array();
		$unfixed_width = 0;
		$fixed_width   = 0;
		
		foreach($aData['fields'] as $sFieldKey => $aField) {
			if($aData['fields'][$sFieldKey]['headerText'] != '...') {
				// фиксирана ширина
				if(!empty($aField['pdfWidth'])) {
					$aWidths[$sFieldKey] = $aField['pdfWidth'];
					$fixed_width += $aWidths[$sFieldKey];
				} else {
					$this->SetFont('FreeSans', 'B',  $this->aTables['Caption']['FontSize'] );
					
					$width = $this->GetStringWidth($aData['fields'][$sFieldKey]['headerText']);
					$aWidths[$sFieldKey] = isset($aWidths[$sFieldKey]) ? ($width > $aWidths[$sFieldKey] ? $width : $aWidths[$sFieldKey]) : $width;
					foreach($aData['data'] as $aRow) {
						if(isset($aRow[$sFieldKey])) {
							$width = $this->GetStringWidth($aRow[$sFieldKey]);
							$aWidths[$sFieldKey] = isset($aWidths[$sFieldKey]) ? ($width > $aWidths[$sFieldKey] ? $width : $aWidths[$sFieldKey]) : $width;
						}
					}
					
					$unfixed_width += $aWidths[$sFieldKey];
				}
			}
		}

		// за прекалено широки колони се отнемат по 50% от ширината им
		foreach($aWidths as $key => $value) {
			if(count($aWidths) > 3 && ($value > ($unfixed_width * 0.7))) {
				$new_value= $value * 0.5;
				$aWidths[$key] = $new_value;
				$unfixed_width -= $value - $new_value;
			}
		}
		
		$delta = ($nTableWidth - $fixed_width) / $unfixed_width ;
		foreach($aData['fields'] as $sFieldKey => $aField) {
			if(empty($aField['pdfWidth'])) {
				$aWidths[$sFieldKey] = $delta * $aWidths[$sFieldKey];
			}
		}

		return $aWidths;
	}
	
	public function PrintTableHeader($aFields, $aWidths) {
		$this->SetLineWidth($this->aTables['Caption']['BorderWidth']);
		
		foreach ($aFields as $sFieldsKey => $aFieldsValue) {
			if (empty($aFieldsValue['children'])) {
				if (isset($aWidths[$aFieldsValue['dataField']])) {
					$this->aMyGridData[$aFieldsValue['dataField']] = array(
						'x'     => $this->GetX(),
						'y'     => $this->GetY(),
						'width' => $aWidths[$aFieldsValue['dataField']]
					);
					
					$this->Cell(
						$aWidths[$aFieldsValue['dataField']],
						$this->aTables['Caption']['Height'],
						'',
						1, 0, 'L', 1
					); 					
				}
			} else {
				$nCurrentX = $this->GetX();
				$nCurrentY = $this->GetY();
				
				$this->SetY($nCurrentY + $this->aTables['Caption']['Height']);
				$this->SetX($nCurrentX);
				
				$this->PrintTableHeader($aFieldsValue['children'], $aWidths);
				$nAfterChildCurrentX = $this->GetX();
				
				$this->SetXY($nCurrentX, $nCurrentY);
				
				$nParentWidth = $nAfterChildCurrentX - $nCurrentX;
				
				if($nParentWidth != 0) {
					$this->aMyGridData[$aFieldsValue['headerText']] = array(
						'x'     => $this->GetX(),
						'y'     => $this->GetY(),
						'width' => $nParentWidth
					);
					
					$this->Cell(
						$nParentWidth,
						$this->aTables['Caption']['Height'],
						'',
						1, 0, 'L', 1
					);
				}
			}
		}
	}
	
	public function RealPrintTableHeader($aFields, $nDepth, $nLavel , $totals = NULL) {
		$this->SetFillColor($this->aTables['Caption']['Background' ]);
		$this->SetTextColor($this->aTables['Caption']['TextColor'  ]);
		$this->SetDrawColor($this->aTables['Caption']['BorderColor']);
		$this->SetLineWidth($this->aTables['Caption']['BorderWidth']);
		$this->SetFont('FreeSans', 'B',  $this->aTables['Caption']['FontSize'] );
		
		foreach ($aFields as $sFieldKey => $aFieldValue) {
			$this->SetXY($aFieldValue['x'], $aFieldValue['y']);
			
			$this->Cell(
				$aFieldValue['width'],
				($this->aTables['Caption']['Height'] * $nLavel),
				'',
				1, 0, 'L', 1
			);

			$this->SetXY($aFieldValue['x'], $aFieldValue['y']);
			
			$aTxt = str_split(empty($aFieldValue['text']) ? $aFieldValue['dataField'] : $aFieldValue['text']);
			$nTxtW = 0;
			$sCellTxt = '';
			foreach ($aTxt as $nTxtKey => $sTxtVal) {
				$nTxtW += $this->GetStringWidth($sTxtVal);
				if ($nTxtW < ($aFieldValue['width'] + 5)) {
					$sCellTxt .= $sTxtVal;
				} else {
					break;
				}
			}
			
			$this->MultiCell(
				$aFieldValue['width'],
				$this->aTables['Caption']['Height'],
				$sCellTxt,
				'0', 'C'
			);
			
			end($aFields);
			$this->SetXY($aFieldValue['x'], $aFieldValue['y']);
		}
        
		if (!empty($totals)) {
            $this->Ln();
			foreach ( $aFields AS $aKey => $aValue ) { 
				if ( !empty($totals[$aValue['dataField']]) ) { 
					$this->Cell(
							$aValue['width'],
							$this->aTables['Rows']['Height'],
							$totals[$aValue['dataField']],
							1, 0, 'R', 1 
						);
				}
				else { 
					$this->Cell(
						$aValue['width'],
						$this->aTables['Rows']['Height'],
						$totals[$aValue['dataField']],
						1, 0, 'R', 1 
					);
				}
			}
		}
		
		$this->Ln();
	}
	
	public function FormatCell( $nDateFormat, &$sContent, &$sAllign ){
		$sAllign = 'L';

		switch($nDateFormat) {
			case DF_DIGIT : 
				$sContent = sprintf("%01.2f", $sContent);
				$sAllign = 'R';
				break;
			case DF_FLOAT : 
				$sContent = sprintf("%01.3f", $sContent);
				$sAllign = 'R';
				break;
			case DF_NUMBER : 
				$sContent = sprintf("%01.0f", $sContent);
				$sAllign = 'R';
				break;
			case DF_CURRENCY : 
				$sContent = sprintf("%01.2f %s.", $sContent, $_SESSION['system']['currency']);
				$sAllign = 'R';
				break;
		}		
	}
	
	private function strCmp($a, $b) {
        if ($a['x'] == $b['x']) {
	        return 0;
	    }
	    
	    return (($a['x'] < $b['x']) ? -1 : 1);
	}

	public function PrintTableData($aData, $aWidths) {
        $this->SetTextColor( $this->aTables['Rows']['TextColor'] );
		$this->SetDrawColor( $this->aTables['Rows']['BorderColor'] );
		$this->SetLineWidth( $this->aTables['Rows']['BorderWidth']  );
		$this->SetFont('FreeSans', '', $this->aTables['Rows']['FontSize']);
		$nColorIndex = 0;
		
		$aNewWidths = array();
		foreach ($aWidths as $sWidthsKey => $aWidthsVal) {
			$aNewWidths[$aWidthsVal['dataField']] = $aWidthsVal['width'];
		}
		
		usort($aWidths, array('PDF', 'strCmp'));
        
		foreach ($aData as $sDataKey => $aDataValue) {
			$this->SetFillColor($this->aTables['Rows']['Background'][$nColorIndex]);
			
			foreach ($aWidths as $sWidthsKey => $aWidthsVal) {
				if (isset($aDataValue[$aWidthsVal['dataField']]) || $aDataValue[$aWidthsVal['dataField']] === NULL ) {
					$this->SetX($aWidthsVal['x']);

                    switch ($aWidthsVal['type']) {
						case 'float':
							$sPrintText  = $aDataValue[$aWidthsVal['dataField']];
							//$sPrintAlign = 'R';
						break;
							
						case 'image':
							$aPath = explode('.', $aDataValue[$aWidthsVal['dataField']]);
							if ((end($aPath) == 'png') || (end($aPath) == 'gif') || (end($aPath) == 'ico'))
								$sPrintText = ' ';
							else 
								$sPrintText = $aDataValue[$aWidthsVal['dataField']];
							
							//$sPrintAlign = 'C';
						break;
							
						case 'currency':
							//$sPrintText  = sprintf(DBCurrency::getPriceFormat('BGN'), $aDataValue[$aWidthsVal['dataField']]);//премахнато с цел по-голяма флексабилност
                            $sPrintText  = sprintf($aDataValue[$aWidthsVal['dataField']].' %s', $aDataValue[$aWidthsVal['sign']]);
							$sPrintAlign = 'R';
						break;
							
						case 'date':
							$sPrintText = '';
							if (empty($aDataValue[$aWidthsVal['dataField']])) break;
							
                            $sTimestamp = strtotime($aDataValue[$aWidthsVal['dataField']]);
                            if (checkdate(date('m', $sTimestamp), date('d', $sTimestamp), date('Y', $sTimestamp))) {
                                $sPrintText = date(Language::getPHPDateFormat(), $sTimestamp);
                            }
                            
							//$sPrintAlign = 'C';
						break;
							
						case 'dateTime':
							$sPrintText = '';
							if (empty($aDataValue[$aWidthsVal['dataField']])) break;
								
                            $sTimestamp = strtotime($aDataValue[$aWidthsVal['dataField']]);
                            if (checkdate(date('m', $sTimestamp), date('d', $sTimestamp), date('Y', $sTimestamp))) {
                                $sPrintText = date(Language::getPHPDateTimeFormat(), $sTimestamp);
                            }
							
                            //$sPrintAlign = 'C';
						break;
							
						case 'time':
                            $stampVal = $aDataValue[$aWidthsVal['dataField']];
                            $sTimestamp = strtotime($aDataValue[$aWidthsVal['dataField']]);
                            if (!empty($sTimestamp)) {
                                $sPrintText = date(Language::getPHPTimeFormat(), $sTimestamp);
                            } else {
                                $sPrintText = '';
                            }

                            //$sPrintAlign = 'C';
                            break;

                        case '':
                            // колони етикет
                            break;

						case 'string':
						case 'integer':
                        default:
                            $sPrintText  = strip_tags($aDataValue[$aWidthsVal['dataField']]);
							$sPrintAlign = 'L';
                            break;
					}
					
					$this->Cell(
						$aWidthsVal['width'],
						$this->aTables['Rows']['Height'],
						$sPrintText,
						1, 0, $sPrintAlign, 1 
					);
					
					// рисуване на бял ред до края на страницата (за преахване на излишните надписи)
					$this->SetFillColor(255, 255, 255);
					$this->SetRightMargin(0);
					$this->Cell(
						0,
						$this->aTables['Rows']['Height'],
						'',
						1, 0, 'C', 1 
					);
					$this->SetRightMargin($this->aMargin['right']);
					$this->SetFillColor($this->aTables['Rows']['Background'][$nColorIndex]);
				}
			}
			
			$nColorIndex = ($nColorIndex + 1) % count($this->aTables['Rows']['Background']);
			$this->Ln();
		}
	}
	
	public function PrintTableDataOld($aData, $aWidths) {
		$this->SetTextColor( $this->aTables['Rows']['TextColor'] );
		$this->SetDrawColor( $this->aTables['Rows']['BorderColor'] );
		$this->SetLineWidth( $this->aTables['Rows']['BorderWidth']  );
		$this->SetFont('FreeSans', '', $this->aTables['Rows']['FontSize']);
		
		$aTotalFields = array();
		$nColorIndex  = 0;
		$nRowNum      = 0;
		
		foreach ($aData['data'] as $nKeyRow => $aRow) {
			$this->SetFillColor($this->aTables['Rows']['Background'][$nColorIndex]);
			$nRowNum++;
			
			foreach ($aData['fields'] as $sFieldKey => $aField) {
				if ($aData['fields'][$sFieldKey]->headerText != '...') {
					$nWidth = isset($aWidths[$sFieldKey]) ?  $aWidths[$sFieldKey] : 30;
					if ($nWidth>0) {
						$sCell = isset($aRow[$sFieldKey]) ? $aRow[$sFieldKey] : '';
						if (!empty($aField->sImg)) {
							if ($aRow[$sFieldKey] == '0' || $aRow[$sFieldKey] == '') {
								$aRow[$sFieldKey] = '';
							} elseif ( $aRow[$sFieldKey] == '1' ) {
								$aRow[$sFieldKey] = 'да';
							} 
						}
						
						if (!empty($aField->aAttributes['PDF_PLAY_CONTENT'])) {
							if ($aRow[$sFieldKey] == '0' || $aRow[$sFieldKey] == '') {
								$aRow[$sFieldKey] = '';
							} else {
								$aRow[$sFieldKey] = $aField->aAttributes['PDF_PLAY_CONTENT'];
							} 
						}
						
						$sAllign = 'L';
						$sContent 		= $aRow[$sFieldKey];

						// Полето трябва ли да е тотал
						if (isset( $aField->aAttributes['DATA_TOTAL'] ) && $aField->aAttributes['DATA_TOTAL']) {
							if(empty($aTotalFields[$sFieldKey])) {
								$aTotalFields[$sFieldKey] = 0;
							}
							
							$aTotalFields[$sFieldKey] += $sContent;
						}

						if (!empty($aField->aAttributes['DATA_FORMAT'])) {									
							$this->FormatCell( $aField->aAttributes['DATA_FORMAT'], $sContent, $sAllign );
						}
						
						if (isset($aField->aAttributes['align'])) {
							switch($aField->aAttributes['align']) {
								case 'right':
									$sAllign = 'R'; 
									break;
								case 'center':
									$sAllign = 'C';
									break; 
							}
						}

						$sBold = '';
						if (isset($aField->aAttributes['style'])) {
							if ($aField->aAttributes['style'] == 'font-weight:bold') {
								$sBold = 'B';
							}
						}
							
			
						if ($sFieldKey == '#') {
							$sContent = $nRowNum;
						}	
						
						$this->SetFont('FreeSans', $sBold, $this->aTables['Rows']['FontSize']);
						$this->Cell($nWidth, $this->aTables['Rows']['Height'], $sContent, 1, 0, $sAllign, 1);
					}
				}
			}
			
			// Изтрива остатъка от съдържанието на послдната колона за да се получи margin-right
			$this->SetFillColor( 255 );
			$this->Cell($this->_PageWidth, $this->aTables['Rows']['Height'], '',0,0,'L',1);
			$this->Ln();
			$this->SetX( $this->aMargin['left'] );
			$nColorIndex = ($nColorIndex + 1) % count($this->aTables['Rows']['Background']);
		}
		
		// Добавяне на тотали
		$this->SetTextColor($this->aTables['Total']['TextColor'  ]);
		$this->SetDrawColor($this->aTables['Total']['BorderColor']);
		$this->SetLineWidth($this->aTables['Total']['BorderWidth']);
		$this->SetFillColor($this->aTables['Total']['Background' ]);
		$this->SetFont('FreeSans', '', $this->aTables['Total']['FontSize']);
		$this->SetX( $this->aMargin['left'] );

		foreach ($aData['fields'] as $sFieldKey => $aField) {
			$sAllign  = 'L';
			$sContent = '';
			$nWidth   = isset($aWidths[$sFieldKey]) ?  $aWidths[$sFieldKey] : 30;
			
			$this->SetFillColor(255);

			if ($aTotalFields[$sFieldKey]) {
				$sContent = $aTotalFields[$sFieldKey];
				$this->SetFillColor($this->aTables['Total']['Background']);

				if (!empty( $aField->aAttributes['DATA_FORMAT'])) {
					$this->FormatCell( $aField->aAttributes['DATA_FORMAT'], $sContent, $sAllign );
				}
			} 
					
			$this->SetFont('FreeSans', $sBold, $this->aTables['Rows']['FontSize']);
			$this->Cell($nWidth, $this->aTables['Rows']['Height'], $sContent, 1, 0, $sAllign, 1);
		}
		
		// Изтрива остатъка от съдържанието на послдната колона за да се получи margin-right
		$this->SetFillColor(255);
		$this->Cell($this->_PageWidth, $this->aTables['Rows']['Height'], '', 0, 0, 'L', 1);
		$this->Ln();
		$nColorIndex = ($nColorIndex + 1) % count($this->aTables['Rows']['Background']);

		$this->Ln();
	}

	public function PrintTotal($aData) {
		$this->SetTextColor($this->aTables['Total']['TextColor'  ]);
		$this->SetDrawColor($this->aTables['Total']['BorderColor']);
		$this->SetLineWidth($this->aTables['Total']['BorderWidth']);
		$this->SetFillColor($this->aTables['Total']['Background' ]);
		$this->SetFont('FreeSans', '', $this->aTables['Total']['FontSize']);
		
		$this->Cell(0, $this->aTables['Total']['Height'], L('Брой редове в справката:') . ' ' . count($aData) . ' ' . L('реда'), 1, 0, 'L', 1);
		
		$this->Ln();
	}
}
?>