<?php
$type='TrueType';
$name='Bulgarian-Ariel';
$desc=array('Ascent'=>1253,'Descent'=>-236,'CapHeight'=>729,'Flags'=>32,'FontBBox'=>'[-22 -219 1097 927]','ItalicAngle'=>0,'StemV'=>70,'MissingWidth'=>800);
$up=-133;
$ut=20;
$cw=array(
	chr(0)=>800,chr(1)=>800,chr(2)=>800,chr(3)=>800,chr(4)=>800,chr(5)=>800,chr(6)=>800,chr(7)=>800,chr(8)=>800,chr(9)=>800,chr(10)=>800,chr(11)=>800,chr(12)=>800,chr(13)=>800,chr(14)=>800,chr(15)=>800,chr(16)=>800,chr(17)=>800,chr(18)=>800,chr(19)=>800,chr(20)=>800,chr(21)=>800,
	chr(22)=>800,chr(23)=>800,chr(24)=>800,chr(25)=>800,chr(26)=>800,chr(27)=>800,chr(28)=>800,chr(29)=>800,chr(30)=>800,chr(31)=>800,' '=>282,'!'=>333,'"'=>474,'#'=>556,'$'=>556,'%'=>889,'&'=>722,'\''=>238,'('=>333,')'=>333,'*'=>389,'+'=>584,
	','=>278,'-'=>333,'.'=>278,'/'=>278,'0'=>556,'1'=>556,'2'=>556,'3'=>556,'4'=>556,'5'=>556,'6'=>556,'7'=>556,'8'=>556,'9'=>556,':'=>333,';'=>333,'<'=>584,'='=>584,'>'=>584,'?'=>611,'@'=>975,'A'=>722,
	'B'=>722,'C'=>722,'D'=>722,'E'=>667,'F'=>611,'G'=>778,'H'=>722,'I'=>278,'J'=>556,'K'=>722,'L'=>611,'M'=>833,'N'=>722,'O'=>778,'P'=>667,'Q'=>778,'R'=>722,'S'=>667,'T'=>611,'U'=>722,'V'=>667,'W'=>944,
	'X'=>667,'Y'=>667,'Z'=>611,'['=>333,'\\'=>278,']'=>333,'^'=>584,'_'=>556,'`'=>333,'a'=>556,'b'=>611,'c'=>556,'d'=>611,'e'=>556,'f'=>333,'g'=>611,'h'=>611,'i'=>278,'j'=>278,'k'=>556,'l'=>278,'m'=>889,
	'n'=>611,'o'=>611,'p'=>611,'q'=>611,'r'=>389,'s'=>556,'t'=>333,'u'=>611,'v'=>556,'w'=>778,'x'=>556,'y'=>556,'z'=>500,'{'=>389,'|'=>280,'}'=>389,'~'=>584,chr(127)=>800,chr(128)=>800,chr(129)=>800,chr(130)=>333,chr(131)=>556,
	chr(132)=>338,chr(133)=>1000,chr(134)=>500,chr(135)=>500,chr(136)=>500,chr(137)=>1315,chr(138)=>465,chr(139)=>308,chr(140)=>1090,chr(141)=>800,chr(142)=>800,chr(143)=>800,chr(144)=>800,chr(145)=>194,chr(146)=>194,chr(147)=>338,chr(148)=>338,chr(149)=>590,chr(150)=>500,chr(151)=>1000,chr(152)=>500,chr(153)=>833,
	chr(154)=>389,chr(155)=>308,chr(156)=>1088,chr(157)=>800,chr(158)=>800,chr(159)=>479,chr(160)=>282,chr(161)=>287,chr(162)=>565,chr(163)=>565,chr(164)=>606,chr(165)=>565,chr(166)=>500,chr(167)=>500,chr(168)=>576,chr(169)=>833,chr(170)=>473,chr(171)=>456,chr(172)=>833,chr(173)=>333,chr(174)=>833,chr(175)=>500,
	chr(176)=>329,chr(177)=>833,chr(178)=>373,chr(179)=>373,chr(180)=>500,chr(181)=>542,chr(182)=>500,chr(183)=>282,chr(184)=>529,chr(185)=>969,chr(186)=>455,chr(187)=>456,chr(188)=>879,chr(189)=>879,chr(190)=>879,chr(191)=>463,chr(192)=>722,chr(193)=>722,chr(194)=>722,chr(195)=>611,chr(196)=>900,chr(197)=>709,
	chr(198)=>1093,chr(199)=>672,chr(200)=>757,chr(201)=>757,chr(202)=>750,chr(203)=>729,chr(204)=>874,chr(205)=>753,chr(206)=>778,chr(207)=>753,chr(208)=>671,chr(209)=>722,chr(210)=>611,chr(211)=>718,chr(212)=>892,chr(213)=>667,chr(214)=>816,chr(215)=>685,chr(216)=>1057,chr(217)=>1183,chr(218)=>928,chr(219)=>949,
	chr(220)=>687,chr(221)=>722,chr(222)=>1109,chr(223)=>698,chr(224)=>556,chr(225)=>606,chr(226)=>572,chr(227)=>454,chr(228)=>685,chr(229)=>556,chr(230)=>809,chr(231)=>546,chr(232)=>615,chr(233)=>615,chr(234)=>573,chr(235)=>577,chr(236)=>666,chr(237)=>603,chr(238)=>611,chr(239)=>603,chr(240)=>611,chr(241)=>556,
	chr(242)=>454,chr(243)=>556,chr(244)=>957,chr(245)=>556,chr(246)=>652,chr(247)=>578,chr(248)=>886,chr(249)=>968,chr(250)=>693,chr(251)=>811,chr(252)=>562,chr(253)=>564,chr(254)=>908,chr(255)=>596);
$enc='cp1252';
$diff='';
$file='FreeSansB.z';
$originalsize=17208;
?>
