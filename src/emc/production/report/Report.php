<?php
namespace emc\production\report;

use main\db\DBC;
use main\grid\GridPanelHandler;

/**
 * Class Report
 * @package emc\production\report
 * @accessLevelDescription Достъп до справка Отчет производство
 */
class Report extends GridPanelHandler {

    public function __construct() {
        $this->oBase = DBC::$main;

        $this->bUseSQLFilters = false;
        $this->bUseSQLPaging = false;
        $this->bUseSQLSort = false;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'order_num',
                'width' => 100,
                'headerText' => L('№ на поръчка'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'client_order_num',
                'width' => 150,
                'headerText' => L('Поръчка на клиента №'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'client_name',
                'width' => 150,
                'headerText' => L('Клиент име'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'size',
                'width' => 100,
                'headerText' => L('Размер'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'id_direction',
                'width' => 100,
                'headerText' => L('Направление'),
                'items' => DBC::$slave->selectAssoc("SELECT id as _k, id, name as text FROM production_directions WHERE to_arc = 0"),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'id_application',
                'width' => 150,
                'headerText' => L('Приложение'),
                'items' => DBC::$slave->selectAssoc("SELECT id as _k, id, name as text FROM production_applications WHERE to_arc = 0"),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => 'group',
                'headerText' => L('Дебелина'),
                'children' => array(
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_FLOAT,
                        'dataField' => 's',
                        'width' => 100,
                        'headerText' => L('по размер'),
                        'sortable' => true,
                        'resizable' => true,
                        'filterable' => true
                    )),
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_FLOAT,
                        'dataField' => 's_production',
                        'width' => 140,
                        'headerText' => L('за производство'),
                        'sortable' => true,
                        'resizable' => true,
                        'filterable' => true
                    )),
                    $this->newColumn(array(
                        'type' => 'group',
                        'headerText' => L('Допуски'),
                        'children' => array(
                            $this->newColumn(array(
                                'type' => GridPanelHandler::CTYPE_FLOAT,
                                'dataField' => 's_minus',
                                'width' => 50,
                                'headerText' => '-',
                                'sortable' => false,
                                'filterable' => false,
                                'resizable' => true,
                            )),
                            $this->newColumn(array(
                                'type' => GridPanelHandler::CTYPE_FLOAT,
                                'dataField' => 's_plus',
                                'width' => 50,
                                'headerText' => '+',
                                'sortable' => false,
                                'filterable' => false,
                                'resizable' => true,
                            ))
                        )
                    ))
                )
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_INT,
                'dataField' => 'l',
                'width' => 75,
                'headerText' => L('Дължина'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => 'group',
                'headerText' => L('Допуски'),
                'children' => array(
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_INT,
                        'dataField' => 'l_minus',
                        'width' => 50,
                        'headerText' => '+',
                        'sortable' => false,
                        'filterable' => false,
                        'resizable' => true,
                    )),
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_INT,
                        'dataField' => 'l_plus',
                        'width' => 50,
                        'headerText' => '-',
                        'sortable' => false,
                        'filterable' => false,
                        'resizable' => true,
                    )),
                )
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'id_metal',
                'width' => 125,
                'headerText' => L('Основен метал'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true,
                'items' => DBC::$slave->selectAssoc("SELECT id as _k, id, code as text FROM metals WHERE to_arc = 0")
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'id_metal_brand',
                'width' => 125,
                'headerText' => L('Метал марка'),
                'items' => DBC::$slave->selectAssoc("SELECT id as _k, id, name as text FROM production_metal_brands WHERE to_arc = 0"),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'line_code',
                'width' => 100,
                'headerText' => L('Машина'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'id_standard',
                'width' => 100,
                'headerText' => L('Стандарт'),
                'items' => DBC::$slave->selectAssoc("SELECT id as _k, id, code as text FROM production_standards WHERE to_arc = 0"),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => 'group',
                'headerText' => L('Маса'),
                'children' => array(
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_CURRENCY,
                        'dataField' => 'planned_mass',
                        'width' => 75,
                        'headerText' => L('планирана'),
                        'sortable' => false,
                        'resizable' => true,
                        'filterable' => false
                    )),
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_CURRENCY,
                        'dataField' => 'produced_mass',
                        'width' => 100,
                        'headerText' => L('произведена'),
                        'sortable' => false,
                        'resizable' => true,
                        'filterable' => false
                    ))
                )
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'planned_setup_duration',
                'width' => 225,
                'headerText' => L('Планувано време за пренастройка'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'real_setup_duration',
                'width' => 225,
                'headerText' => L('Реално време за пренастройка'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'planned_production_duration',
                'width' => 225,
                'headerText' => L('Планувано време за производство'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'real_production_duration',
                'width' => 225,
                'headerText' => L('Реално време за производство'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_INT,
                'dataField' => 'real_production_speed',
                'width' => 135,
                'headerText' => L('Реална скорост'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'lot_code',
                'width' => 90,
                'headerText' => L('Лот код'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_FLOAT,
                'dataField' => 'lot_mass',
                'width' => 135,
                'headerText' => L('Лот количество'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_FLOAT,
                'dataField' => 'discard',
                'width' => 100,
                'headerText' => L('Брак кг'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_FLOAT,
                'dataField' => 'discard_percent',
                'width' => 125,
                'headerText' => L('Брак процент'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_DATETIME,
                'dataField' => 'exit_production_date',
                'width' => 225,
                'headerText' => L('Излизане от производство'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_DATETIME,
                'dataField' => 'finish_order_date',
                'width' => 250,
                'headerText' => L('Прикл. на поръчка за производство'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'order_created_person',
                'width' => 200,
                'headerText' => L('Служител приел заявката'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_DATETIME,
                'dataField' => 'order_created_time',
                'width' => 175,
                'headerText' => L('Създаване на заявка'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'order_confirmed_person',
                'width' => 225,
                'headerText' => L('Служител потвърдил заявката'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_DATETIME,
                'dataField' => 'order_confirmed_time',
                'width' => 200,
                'headerText' => L('Потвърждаване на заявка'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'status',
                'width' => 100,
                'headerText' => L('Статус'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true,
                'style' => 'text-align: center',
                'items' => array(
                    'waiting' => array('text' => L('Изчаква'), 'iconCls' => 'fa fa-hourglass-2 fa-fw'),
                    'confirmed' => array('text' => L('Потвърдена'), 'iconCls' => 'fa fa-check-circle fa-fw'),
                    'production' => array('text' => L('В производство'), 'iconCls' => 'fa fa-refresh fa-fw'),
                    'finished' => array('text' => L('Приключена'), 'iconCls' => 'fa fa-cubes fa-fw'),
                    'cancelled' => array('text' => L('Отказана'), 'iconCls' => 'fa fa-ban fa-fw')
                )
            ))
        );

        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {

        return $this->loadData($aRequest);
    }

    public function loadData($aRequest) {
        $sTimePeriod = $this->getWhereFromTimeButton('exit_production_date', $aRequest['fields']['time_period']);

        $this->sReportSelectStatement = "
            SELECT
              ROUND(SUM(ps.planned_mass)/count(ps.id), 2)      AS planned_mass,
              ROUND(SUM(ps.produced_mass)/count(ps.id), 2)     AS produced_mass,
              ROUND((SUM(ps.produced_mass) / pss.m) / (sum(psr.real_production_duration_with_mass)/60)) AS real_production_speed,
              MAX(ps.time_to)                     AS exit_production_date,

              psr.*,

              SEC_TO_TIME(SUM(psr.real_setup_duration)) AS real_setup_duration ,
              SEC_TO_TIME(SUM(psr.real_production_duration)) AS real_production_duration,
              SEC_TO_TIME(SUM(psr.planned_setup_duration)) AS planned_setup_duration,
              SEC_TO_TIME(SUM(psr.planned_production_duration)) AS planned_production_duration,
              
              pos.id                              AS id_size,
              pos.l,
              pos.l_minus,
              pos.l_plus,
              pos.s                               AS s_production,
              pos.s_minus,
              pos.s_plus,
              pos.id_standard,
              pos.id_direction,
              pos.id_application,
              pos.id_metal_brand,

              pss.s,
              pss.code                            AS size,
              pss.id_metal,

              po.id,
              po.num                              AS order_num,
              po.client_order_num                 AS client_order_num,
              po.created_time                     AS order_created_time,
              po.confirmed_time                   AS order_confirmed_time,
              po.name                             AS client_name,
              po.status,

              CONCAT(p1.fname, ' ', p1.lname)     AS order_created_person,
              CONCAT(p2.fname, ' ', p2.lname)     AS order_confirmed_person,

              pos.produced_total_mass,
              '' as lot_code,
              '' as lot_mass,
              '' as discard,
              '' as discard_percent,
              
              GROUP_CONCAT(DISTINCT plcos2.id_order_size ORDER BY plcos2.id_order_size) as group_size_ids,
              GROUP_CONCAT(DISTINCT pl.code SEPARATOR ', ') AS line_code,

              ps.work_shift_type,
              f.order_finish_time as finish_order_date,
              ''
            FROM production_schedule ps
            JOIN (
              SELECT
                psr.id_schedule,
                SUM(IF(psr.type = 'real_setup', psr.duration, 0))             AS real_setup_duration,
                SUM(IF(psr.type = 'real_production', psr.duration, 0))        AS real_production_duration,
                SUM(IF(psr.type = 'real_production' AND ps.produced_mass > 0, psr.duration, 0))        AS real_production_duration_with_mass,

                SUM(IF(psr.type = 'planned_setup', psr.duration, 0))          AS planned_setup_duration,
                SUM(IF(psr.type = 'planned_production', psr.duration, 0))     AS planned_production_duration
              FROM production_schedule_rows psr
              JOIN production_schedule ps ON ps.id = psr.id_schedule AND ps.to_arc = 0
              WHERE 1
                AND psr.to_arc = 0
              GROUP BY ps.id
            ) psr ON psr.id_schedule = ps.id
            JOIN production_orders_sizes pos ON pos.id = ps.id_order_size
            JOIN production_orders po ON po.id = pos.id_order
            JOIN production_sizes pss ON pss.id = pos.id_size
            LEFT JOIN production_lines pl ON pl.id = ps.id_line AND pl.to_arc = 0
            LEFT JOIN personnel p1 ON p1.id = po.created_user
            LEFT JOIN personnel p2 ON p2.id = po.confirmed_user
            LEFT JOIN (
	            select
		            po.id as id_order,
		            sum(pos.complete) = count(pos.id) as order_finished,
		            max(ps.time_to) as order_finish_time,
		            ''
	            from production_orders po
	            left join production_orders_sizes pos ON pos.id_order = po.id and pos.to_arc = 0
	            left join production_schedule ps ON ps.id_order_size = pos.id and ps.to_arc = 0
	            where 1
	            and po.`status` = 'confirmed'
	            and po.to_arc = 0
	            group by po.id
            ) f ON f.id_order = po.id and f.order_finished = 1
                        
            LEFT JOIN production_lots_consume_order_sizes plcos ON plcos.id_order_size = ps.id_order_size and plcos.to_arc = 0
            LEFT JOIN production_lots_consume plc on plc.id = plcos.id_lot_consume and plc.to_arc = 0
            LEFT JOIN production_lots_consume_order_sizes plcos2 ON plcos2.id_lot_consume = plc.id 
        ";

        $this->aReportWhereStatement[] = "ps.to_arc = 0";
        $this->aReportWhereStatement[] = "pos.complete = 1";
        $this->aReportHavingStatement[] = "{$sTimePeriod}";
        $this->aReportHavingStatement[] = "produced_mass";
        $this->sReportGroupStatement = "pos.id";
        $this->aDefaultSort = array(
            array('field' => 'time_from', 'dir' => 'ASC')
        );

        return parent::loadData($aRequest);
    }

    public function processData($aData) {


        foreach ($aData as $k => $row) {
            if(empty($row['group_size_ids'])) continue;

            $groupData = DBC::$main->selectRow("
                select 
                group_concat(t.lot_code order by t.id) as lot_code,
                group_concat(t.mass order by t.id) as lot_mass,
                sum(t.mass) as group_used_mass,
                group_produced_mass
                from (
                    select
                    plc.*,
                    sum(pos.produced_total_mass) as group_produced_mass
                    from production_lots_consume plc
                    join production_lots_consume_order_sizes plcos ON plcos.id_lot_consume = plc.id and plcos.to_arc = 0
                    join production_orders_sizes pos ON pos.id = plcos.id_order_size
                    where 1
                    and plc.to_arc = 0
                    and plcos.id_order_size in({$row['group_size_ids']})
                    group by plc.id
                ) t
            ", 5);

            $aData[$k]['lot_code'] = $groupData['lot_code'];
            $aData[$k]['lot_mass'] = $groupData['lot_mass'];

            $groupDiscard = $groupData['group_used_mass'] - $groupData['group_produced_mass'];

            $ratio = $row['produced_total_mass'] / $groupData['group_produced_mass'];

            $aData[$k]['discard'] = round($groupDiscard * $ratio, 2);
            $aData[$k]['discard_percent'] = round($aData[$k]['discard'] / $groupData['group_used_mass'] * 100, 2);
        }

        return parent::processData($aData);
    }


    public function setGridTotals($aData, $aRequest = array(), $nTimeFrom = 0, $nTimeTo = 0) {
        $gridTotals = array(
            'produced_mass' => 0,
            'planned_mass' => 0
        );

        foreach ($aData as $val) {
            $gridTotals['produced_mass'] += $val['produced_mass'];
            $gridTotals['planned_mass'] += $val['planned_mass'];
        }

        $this->oResponse->gridTotals = $gridTotals;
        return $gridTotals;
    }
}