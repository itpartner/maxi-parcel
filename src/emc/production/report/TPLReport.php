<?php
namespace emc\production\report;

use main\grid\TPLGridPanel;
use main\TPLBase;

class TPLReport extends TPLGridPanel {
    public function __construct($sPrefix, $aInitParams = array()) {
        $this->{'aConfig'} = array(
            'title' => L('Отчет производство'),
            'autoVResize' => true,
            'topBarItems' => array(
                array('timeMenuButton', array('name' => 'time_period', 'align' => 'left'))
            ),
            'bottomBarItems' => array(
                array('exportButton', array('align' => 'right'))
            )
        );
        parent::__construct($sPrefix, $aInitParams);
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, aBaseParams) {
            var form = container;
            var grid = container.grid;

            grid.clickListeners['order_num'] = function(rowNum) {
                framework.createWindow({
                    tplName: 'emc/production/orders/Order',
                    baseParams: {
                        row: container.grid.getCurrentData()[rowNum],
                        getParentGrid: function() { return grid; },
                        refreshParentGrid: function () { grid.loadData(); }
                    }
                });
            };

            grid.clickListeners['size'] = function(rowNum, rowData) {
                framework.createWindow({
                    tplName: 'emc/production/orders/sizes/Size',
                    baseParams: {
                        id: rowData.id_size,
                        id_order: rowData.id,
                        refreshParentGrid: function () { grid.loadData(); }
                    }
                });
            };
        });
JS;
    }
}