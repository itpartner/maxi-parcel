<?php
namespace emc\production\linesload;

use emc\production\schedule\Scheduler;
use main\db\DBC;
use main\Exception;
use main\grid\GridPanelHandler;

/**
 * Class Load
 * @package emc\production\linesload
 * @accessLevelDescription Достъп до справка Натоварване на линии
 */
class Load extends GridPanelHandler {

    public function __construct() {
        $this->oBase = DBC::$slave;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'id',
                'width' => 100,
                'items' => DBC::$slave->selectAssoc("SELECT id, '', code AS text FROM production_lines WHERE to_arc = 0"),
                'headerText' => L('Линия'),
                'resizable' => true,
                'sortable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'status',
                'width' => 100,
                'headerText' => L('Статус'),
                'items' => [
                    'active' => ['text' => L('работи'), 'iconCls' => 'fa fa fa-check-circle'],
                    'idle' => ['text' => L('не работи'), 'iconCls' => 'fa fa fa-ban']
                ],
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_CURRENCY,
                'dataField' => 'load',
                'currencyField' => 'percent',
                'width' => 100,
                'headerText' => L('Натоварване'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
//            $this->newColumn(array(
//                'type' => GridPanelHandler::CTYPE_STRING,
//                'dataField' => 'planned_duration',
//                'width' => 100,
//                'headerText' => L('Планирано'),
//                'sortable' => true,
//                'resizable' => true,
//                'filterable' => true
//            )),
//            $this->newColumn(array(
//                'type' => GridPanelHandler::CTYPE_STRING,
//                'dataField' => 'max',
//                'width' => 100,
//                'headerText' => L('Max'),
//                'sortable' => true,
//                'resizable' => true,
//                'filterable' => true
//            ))
        );

        $this->bUseSQLFilters = false;
        $this->bUseSQLPaging = false;
        $this->bUseSQLSort = false;

        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
    }

    function getGridData($aRequest, $nTimeFrom = 0, $nTimeTo = 0) {
        $sqlTimeFrom = DBC::$main->quote(date('Y-m-d H:i:s',$nTimeFrom));
        $sqlTimeTo = DBC::$main->quote(date('Y-m-d H:i:s',$nTimeTo));

        $lines = DBC::$slave->select("
            SELECT
                pl.id,
                pl.status,
                pl.efficiency,
                IF(COALESCE(t.real_duration,0) > 0, t.real_duration, COALESCE(t.planned_duration,0)) as duration,
                ''
            FROM production_lines pl 
            LEFT JOIN (
                SELECT 
                ps.id_line,
                SUM(IF(psr.type in('real_production','real_setup'), psr.duration,0)) as real_duration,
                SUM(IF(psr.type in('planned_production','planned_setup'), psr.duration,0)) as planned_duration,
                ''
                FROM production_schedule ps
                JOIN production_schedule_rows psr ON psr.id_schedule = ps.id AND psr.to_arc = 0
                WHERE 1
                AND ps.to_arc = 0
            	AND ps.time_from >= $sqlTimeFrom 
            	AND ps.time_to <= $sqlTimeTo
                GROUP BY ps.id_line
            ) t ON t.id_line = pl.id
            WHERE 1
            AND pl.to_arc = 0
            ORDER BY pl.status, pl.code
        ");

        return array_map(function ($line) use ($aRequest, $nTimeFrom, $nTimeTo) {

            $workDuration = 0;
            try {
                $workDuration = array_reduce(Scheduler::generateShifts($line['id'], $nTimeFrom, $nTimeTo - $nTimeFrom), function (&$duration, $shift) use ($nTimeTo) {
                    if(strtotime($shift['time_to']) > $nTimeTo) return $duration;
                    return $duration += $shift['duration'];
                }, 0);
            } catch(Exception $ex){}

            $line['load'] = $workDuration ? round(($line['duration']/$workDuration) * 100,2) : 0;
            $line['load'] = min(100.00, $line['load']);
            $line['max'] = $workDuration;
            $line['percent'] = '%';

            return $line;
        }, $lines);
    }

}