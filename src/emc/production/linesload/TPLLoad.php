<?php
namespace emc\production\linesload;

use main\grid\TPLGridPanel;

class TPLLoad extends TPLGridPanel {
    public function __construct($sPrefix, $aInitParams = array()) {
        $this->{'aConfig'} = array(
            'title' => L('Натоварване на линии'),
            'autoVResize' => true,
            'topBarItems' => array(
                array('timeMenuButton', array('name' => 'time_period', 'align' => 'left'))
            ),
            'bottomBarItems' => array(
                array('exportButton', array('align' => 'right'))
            )
        );
        parent::__construct($sPrefix, $aInitParams);
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, aBaseParams) {
            var form = container;
            var grid = container.grid;


        });
JS;
    }
}