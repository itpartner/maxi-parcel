<?php
namespace emc\production\schedule;

use emc\nomenclatures\productionlines\ProductionLines;
use main\Access;
use main\db\DBC;
use main\Exception;
use main\grid\GridPanelHandler;
use main\InvalidParameter;
use main\Util;

/**
 * @accessLevelDescription Производствени програми
 */
class Schedule extends GridPanelHandler {

    public function __construct() {
        parent::__construct();

        $this->oBase = DBC::$main;

        $this->bUseSQLFilters = false;
        $this->bUseSQLPaging = false;
        $this->bUseSQLSort = false;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ICONS,
                'dataField' => 'row_icons',
                'width' => 40,
                'headerText' => '',
                'sortable' => false,
                'resizable' => true,
                'filterable' => false,
                'style' => 'text-align: center',
                'items' => array(
                    'needs_review' => array('text' => L('Необходим е преглед и потвърждаване на поръчката.'), 'iconCls' => 'fa fa-exclamation fa-fw'),
                    'delayed' => array('text' => L('Забавена'), 'iconCls' => 'fa fa-clock-o fa-fw fa-red')
                )
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ICONS,
                'dataField' => 'is_read',
                'width' => 40,
                'headerText' => '',
                'sortable' => false,
                'resizable' => true,
                'filterable' => true,
                'style' => 'text-align: center',
                'items' => array(
                    'unread' => array('text' => L('Нов размер'), 'iconCls' => 'fa fa-angle-double-right'),
                    'read' => array('text' => L(''), 'iconCls' => '')
                )
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'order_info',
                'width' => 100,
                'headerText' => 'Поръчка',
                'sortable' => false,
                'resizable' => true,
                'filterable' => false
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'id_direction',
                'width' => 100,
                'headerText' => 'Направление',
                'items' => DBC::$slave->selectAssoc("SELECT id as _k, id, name as text FROM production_directions WHERE to_arc = 0"),
                'sortable' => false,
                'resizable' => true,
                'filterable' => false
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'size',
                'width' => 100,
                'headerText' => 'Размер',
                'sortable' => false,
                'resizable' => true,
                'filterable' => false,
                'toolTipField' => Access::hasAccess('/') ? 'id_order_size' : '',
            )),

            $this->newColumn(array(
                'type' => 'group',
                'headerText' => L('Дебелина'),
                'children' => array(
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_FLOAT,
                        'dataField' => 's',
                        'width' => 100,
                        'headerText' => 'по размер',
                        'sortable' => false,
                        'resizable' => true,
                        'filterable' => false
                    )),
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_FLOAT,
                        'dataField' => 's_production',
                        'width' => 100,
                        'headerText' => 'за производство',
                        'sortable' => false,
                        'resizable' => true,
                        'filterable' => false
                    )),
                )
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_INT,
                'dataField' => 'l',
                'width' => 100,
                'headerText' => 'Дължина',
                'sortable' => false,
                'resizable' => true,
                'filterable' => false
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'id_metal_brand',
                'width' => 100,
                'headerText' => 'Метал марка',
                'items' => DBC::$slave->selectAssoc("SELECT id as _k, id, name as text FROM production_metal_brands WHERE to_arc = 0"),
                'sortable' => false,
                'resizable' => true,
                'filterable' => false
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'id_standard',
                'width' => 100,
                'headerText' => 'Стандарт',
                'items' => DBC::$slave->selectAssoc("SELECT id as _k, id, code as text FROM production_standards WHERE to_arc = 0"),
                'sortable' => false,
                'resizable' => true,
                'filterable' => false
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_INT,
                'dataField' => 'total_mass',
                'width' => 60,
                'headerText' => 'Маса',
                'sortable' => false,
                'resizable' => true,
                'filterable' => false
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_CURRENCY,
                'dataField' => 'work_shifts',
                'width' => 70,
                'headerText' => 'Смяна',
                'sortable' => false,
                'resizable' => true,
                'filterable' => false,
                'class' => 'multiRows'
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_CURRENCY,
                'dataField' => 'setup_times',
                'width' => 80,
                'headerText' => 'Пренастройка',
                'sortable' => false,
                'resizable' => true,
                'filterable' => false,
                'class' => 'multiRows'
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_CURRENCY,
                'dataField' => 'production_times',
                'width' => 80,
                'headerText' => 'Производство',
                'sortable' => false,
                'resizable' => true,
                'filterable' => false,
                'class' => 'multiRows'
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_CURRENCY,
                'dataField' => 'production_speed',
                'width' => 50,
                'headerText' => 'Скорост',
                'sortable' => false,
                'resizable' => true,
                'filterable' => false,
                'class' => 'multiRows'
            )),
            $this->newColumn(array(
                'type' => 'group',
                'headerText' => L('Маса'),
                'children' => array(
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_CURRENCY,
                        'dataField' => 'planned_mass',
                        'width' => 75,
                        'headerText' => 'планирана',
                        'sortable' => false,
                        'resizable' => true,
                        'filterable' => false,
                        'class' => 'multiRows'
                    )),
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_CURRENCY,
                        'dataField' => 'produced_mass',
                        'width' => 75,
                        'headerText' => 'произведена',
                        'sortable' => false,
                        'resizable' => true,
                        'filterable' => false,
                        'class' => 'multiRows'
                    )),
                )
            )),
            $this->newColumn(array(
                'type' => 'group',
                'headerText' => L('Закъснение'),
                'children' => array(
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_CURRENCY,
                        'dataField' => 'setup_delay',
                        'width' => 80,
                        'headerText' => 'пренастройка',
                        'sortable' => false,
                        'resizable' => true,
                        'filterable' => false,
                        'class' => 'multiRows'
                    )),
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_CURRENCY,
                        'dataField' => 'production_delay',
                        'width' => 80,
                        'headerText' => 'производство',
                        'sortable' => false,
                        'resizable' => true,
                        'filterable' => false,
                        'class' => 'multiRows'
                    )),
                )
            )),
        );
    }

    public function getGridData($aRequest, $nTimeFrom = 0, $nTimeTo = 0) {
        $timeWhere = $this->getWhereFromTimeButton('ps.work_shift_date', $aRequest['fields']['time']);

        $aRequest['fields']['id_line'] = intval($aRequest['fields']['id_line']);
        $confirmedOrdersOnly = $aRequest['confirmed_orders_only'] ? " AND po.status = 'confirmed' " : '';

        $data = DBC::$main->select("
            SELECT
                pos.id                       as _k,  #id_order_size
                ps.id						 as __k, #schedules 
										
                ps.id                        as id_schedule,

                ps.work_shift_date,
                
                REPLACE(CONCAT_WS('/', ps.work_shift_type, date_format(ps.work_shift_date,'%d.%m.%Y')),'R','Р') as work_shift_label,
                ps.id_order_size,                              
                ps.production_speed,
                ROUND(ps.planned_mass,2) AS planned_mass,
                ROUND(ps.produced_mass,2) AS produced_mass,
                
                ps.time_from < NOW() as is_editable,
                psr.real_setup_duration + psr.real_production_duration AS is_shift_complete,
                
                psr.*,
                
                pos.l,
                pos.s                              AS s_production,
                round(pos.total_mass, 0)           AS total_mass,
                round(pos.mass_in_stock, 0)        AS mass_in_stock,
                pos.desired_production_start, 
                pos.desired_production_end,
                pos.id_standard,
                pos.id_direction,
                pos.id_metal_brand,
                pos.d,
                pos.a,
                pos.h,
                pos.complete, 
                IF(pos.is_read, 'read', 'unread')  AS is_read,
                
                pss.s,
                pss.profile_type,
                pss.code                           as size,

                m.code                             as code_metal,
                
                po.id                              as id_order,
                po.num                             as order_num,
                po.client_order_num                as client_order_num,
                po.created_time                    as order_time,
                po.name                            as client_name,
                po.status                          as order_status,
                
                REPLACE(ps.work_shift_type,'R','Р') as work_shift_type,
                IF(po.needs_review OR pos.needs_review, 'needs_review', '') as row_icons,
                
                IF(psr.real_setup_duration + psr.real_production_duration, (((((((psr.real_production_duration* pl.efficiency)/60) * ps.production_speed * pss.m)  ) - ps.produced_mass) / m) / ps.production_speed) * 60, null) as production_delay,
                IF(psr.real_setup_duration + psr.real_production_duration, (psr.planned_setup_duration - psr.real_setup_duration) * -1, null) as setup_delay,
                 
                ''
            FROM production_schedule ps
            JOIN (
                SELECT
                    psr.id_schedule,
                    SUM(IF(psr.type = 'planned_setup',psr.duration,0)) as planned_setup_duration,
                    min(IF(psr.type = 'planned_setup',psr.time_from,null)) as planned_setup_from, 
                    max(IF(psr.type = 'planned_setup',psr.time_to,null)) as planned_setup_to,
                    SUM(IF(psr.type = 'real_setup',psr.duration,0)) as real_setup_duration,
                    MIN(IF(psr.type = 'real_setup',psr.time_from,null)) as real_setup_from, 
                    max(IF(psr.type = 'real_setup',psr.time_to,null)) as real_setup_to,
                                        
                    SUM(IF(psr.type = 'planned_production',psr.duration,0)) as planned_production_duration,
                    min(IF(psr.type = 'planned_production',psr.time_from,null)) as planned_production_from, 
                    max(IF(psr.type = 'planned_production',psr.time_to,null)) as planned_production_to,
                    SUM(IF(psr.type = 'real_production',psr.duration,0)) as real_production_duration,
                    min(IF(psr.type = 'real_production',psr.time_from,null)) as real_production_from, 
                    max(IF(psr.type = 'real_production',psr.time_to,null)) as real_production_to
                                        
                FROM production_schedule_rows psr
                JOIN production_schedule ps ON ps.id = psr.id_schedule AND ps.to_arc = 0 AND ps.id_line = {$aRequest['fields']['id_line']}
                WHERE 1
                AND psr.to_arc = 0
                GROUP BY ps.id
            ) psr ON psr.id_schedule = ps.id 
            JOIN production_orders_sizes pos ON pos.id = ps.id_order_size
            JOIN production_orders po ON po.id = pos.id_order AND po.to_arc = 0 $confirmedOrdersOnly
            JOIN production_sizes pss ON pss.id = pos.id_size
            JOIN production_lines pl ON pl.id = ps.id_line
            JOIN metals m ON m.id = pss.id_metal
            WHERE 1
            AND ps.to_arc = 0
            AND ps.id_line = {$aRequest['fields']['id_line']}
            AND {$timeWhere}
            GROUP BY ps.id
            ORDER BY ps.time_from
        ", null, 2);

        $orderInfoFields = ['id_direction', 'id_standard', 'size', 's', 's_production', 'l', 'id_metal_brand', 'total_mass', 'id_order', 'id_order_size', 'order_num', 'client_order_num', 'order_time', 'client_name', 'desired_production_start', 'desired_production_end', 'size', 'total_mass', 'mass_in_stock', 'complete', 'order_status', 'row_icons', 'is_read'];

        $gridRows = [];
        $prevWorkTo = null;
        $prevShifts = null;
        foreach ($data as $shifts) {
            $someShift = reset($shifts);
            $gridRow = [];

            foreach ($orderInfoFields as $k) $gridRow[$k] = $someShift[$k];
            foreach ($shifts as $k => $shift) foreach ($orderInfoFields as $field) unset($shifts[$k][$field]);

            $gridRow['is_size_delayed'] = empty($gridRow['complete']) && !empty(Util::SQLDateToTimeStamp($gridRow['desired_production_end'])) && substr(max(array_map(function ($s) {
                    return $s['planned_production_to'];
                }, $shifts)), 0, 10) > $gridRow['desired_production_end'];

            if($gridRow['is_size_delayed']) {
                $gridRow['row_icons'] .= ',delayed';
            }

            if($gridRow['order_status'] == 'cancelled') $gridRow['_row_class_name'] = 'orderCancelled';
            if($gridRow['is_read'] == 'unread') {
                $gridRow['_row_class_name'] .= ' orderUnread';
            }

            $currentWorkFrom = !empty(reset($shifts)['planned_setup_from']) ? reset($shifts)['planned_setup_from'] : reset($shifts)['planned_production_from'];
            if($currentWorkFrom != $prevWorkTo && $prevWorkTo != null && $prevShifts != null && empty(end($prevShifts)['is_shift_complete'])) {
                //проверка за валидност на дупката, да не е край на деня или почивен ден
                if(Util::SQLDateToTimeStamp($currentWorkFrom)) {
                    $testShifts = Scheduler::generateShifts($aRequest['fields']['id_line'], strtotime($prevWorkTo), 1);
                    if(!(end($testShifts)['time_to'] > $currentWorkFrom)) { // валидна дупка, имаме налично работно време в нея
                        $gridRow['_row_class_name'] .= ' productionHole';
                    }
                }
            }

            $gridRow['shifts'] = $shifts;
            $gridRows[] = $gridRow;

            $prevShifts = $shifts;
            $prevWorkTo = !empty(end($shifts)['planned_production_to']) ? end($shifts)['planned_production_to'] : end($shifts)['planned_setup_to'];
        }

        return $gridRows;
    }

    public function setGridTotals($aData, $aRequest = array(), $nTimeFrom = 0, $nTimeTo = 0) {
        $gridTotals = array(
            'is_totals' => 1,

            'total_mass' => 0,
            'mass_in_stock' => 0,
            'produced_mass' => 0,
            'planned_mass' => 0,

            'planned_setup_duration' => 0,
            'planned_setup_count' => 0,

            'planned_production_count' => 0,
            'planned_production_duration' => 0,

            'setup_times' => 'blah',
            'production_times' => 'blah',

            'setup_delay' => 0,
            'production_delay' => 0,
        );

        array_walk($aData, function ($row) use (&$gridTotals) {
            $gridTotals['total_mass'] += $row['total_mass'];
            $gridTotals['mass_in_stock'] += $row['mass_in_stock'];
            array_walk($row['shifts'], function ($shift) use (&$gridTotals) {
                $gridTotals['produced_mass'] += $shift['produced_mass'];
                $gridTotals['planned_mass'] += $shift['planned_mass'];

                $gridTotals['setup_delay'] += $shift['setup_delay'];
                $gridTotals['production_delay'] += $shift['production_delay'];

                foreach (array('planned_setup', 'planned_production') as $type) {
                    $gridTotals[$type . '_duration'] += $shift[$type . '_duration'];
                    $gridTotals[$type . '_count'] += !empty($shift[$type . '_duration']);
                }
            });
        });

        $this->oResponse->gridTotals = $gridTotals;
        return $gridTotals;
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
    }

    public function loadData($aRequest, $nTimeFrom = 0, $nTimeTo = 0) {
        $this->oResponse->{'isLineManager'} = ProductionLines::isUserLineProductionManager($aRequest['fields']['id_line'],$_SESSION['userdata']['id']);
        return parent::loadData($aRequest, $nTimeFrom, $nTimeTo);
    }

    public function changeProductionSpeed($aRequest) {
        return DBC::$main->withTrans(function ($aRequest) {
            if(!Access::hasAccess(CanEditProductionSpeed::class)) throw new Exception(CanEditProductionSpeed::$exMessage);

            $idOrderSize = intval($aRequest['id_order_size']);
            $idLine = intval($aRequest['fields']['id_line']);

            ProductionLines::checkIsUserLineProductionManager($idLine);

            $speed = floatval($aRequest['speed']);
            if(empty($speed) || $speed < 0) throw new InvalidParameter("Невалидна стойност за скорост.");

            DBC::$main->multiInsert('production_schedule', DBC::$main->select("
                SELECT 
                id,
                $speed as production_speed
                FROM production_schedule ps 
                WHERE 1
                AND ps.to_arc = 0
                AND ps.id_order_size = $idOrderSize
                AND ps.id_line = {$idLine}
                ORDER BY id DESC
                LIMIT 1
            "));
            Scheduler::scheduleLine($idLine);

            return $this->loadData($aRequest);
        }, [$aRequest]);
    }

    public function markRowAsRead($aRequest) {
        $idLine = intval($aRequest['fields']['id_line']);
        ProductionLines::checkIsUserLineProductionManager($idLine);

        $nRowId = intval($aRequest['id_order_size']);

        if(!empty($nRowId)) DBC::$main->multiInsert('production_orders_sizes', array(array(
            'id' => $nRowId,
            'is_read' => 1,
            'read_user' => intval($_SESSION['userdata']['id']),
            'read_time' => date("Y-m-d H:i:s", time())
        )));

        return $this->loadData($aRequest);
    }
}