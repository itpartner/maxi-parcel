<?php
namespace emc\production\schedule;

use emc\access\AccessLevel;

/**
 * @accessLevelDescription Право да се редактират работни смени
 */
class CanEditWorkShifts implements AccessLevel{
    public static $exMessage = "Нямате права за редакция на работна смяна.";
}