<?php
namespace emc\production\schedule;


use main\grid\TPLGridPanel;

class TPLRequiredMetalQuantity extends TPLGridPanel {

    function __construct($sPrefix, array $aInitParams) {

        $this->{'aConfig'} = array(
            'title' => L('Нужен метал'),
            'autoVResize' => true,
            'disablePaging' => true,
            'dataHeight' => '300px',
            'hideRowIndexes' => true,
            'topBarItems' => array(
            ),
            'bottomBarItems' => array(
                array('exportButton', array('align' => 'left')),
                array('closebutton')
            )
        );

        $this->{'bNoDefaultTopBarItems'} = true;

        parent::__construct($sPrefix, $aInitParams);
    }


    public function getJSFunction() {
        return <<<'JS'
        (function (form, prefix, initParams) {
        
            form.addListener('beforeRequest',function(request){
                request['fields'] = initParams['fields'];
            });
           
        });
JS;
    }
}