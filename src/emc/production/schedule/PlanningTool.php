<?php
namespace emc\production\schedule;

use emc\nomenclatures\productionlines\ProductionLines;
use main\db\DBC;
use main\form\FormPanelHandler;
use main\InvalidParameter;

/**
 * @accessLevelDescription Достъп до инструмента за препланиране
 */
class PlanningTool extends FormPanelHandler {

    function init($aRequest = array(), $aInitParams = array()) {
        $aRequest['fields']['id_line'] = $aInitParams['fields']['id_line'];
        return $this->load($aRequest);
    }

    function load($aRequest) {
        $idLine = intval($aRequest['fields']['id_line']);
        ProductionLines::checkIsUserLineProductionManager($idLine);

        $orderSizes = Scheduler::transformLineStateToRows(Scheduler::getLineState($idLine));
        foreach($orderSizes as $k => $v) {
            if($orderSizes[$k+1]['read_only'] && $v['read_only']) unset($orderSizes[$k]);
            if(!$orderSizes[$k+1]['read_only']) break;
        }

        $this->oResponse->{'orderSizes'} = $orderSizes;

        return $this->oResponse;
    }

    public function markRowAsRead($aRequest) {
        $idLine = intval($aRequest['fields']['id_line']);
        ProductionLines::checkIsUserLineProductionManager($idLine);

        $nRowId = intval($aRequest['id_order_size']);
        if(!empty($nRowId)) DBC::$main->multiInsert('production_orders_sizes', array(array(
            'id' => $nRowId,
            'is_read' => 1,
            'read_user' => intval($_SESSION['userdata']['id']),
            'read_time' => date("Y-m-d H:i:s", time())
        )));

        return $this->orderChanged($aRequest);
    }

    public function orderChanged($aRequest) {
        $idLine = intval($aRequest['fields']['id_line']);
        $newLineState = [];
        ProductionLines::checkIsUserLineProductionManager($idLine);

        $idOrderSize = intval($aRequest['orderSize']['id_order_size']);

        $possibleLines = $idOrderSize ? DBC::$main->selectAssoc("
               SELECT
                pl.id,
                pl.code
                FROM production_lines pl 
                JOIN production_orders_sizes pos ON pos.id = $idOrderSize 
                JOIN production_lines_sizes pls ON pls.id_size = pos.id_size AND pls.id_line = pl.id AND pls.to_arc = 0
                WHERE pl.to_arc = 0
            ") : [];

        if($idOrderSize && empty($possibleLines[$idLine])) {
            throw new InvalidParameter(L("Избраната линия не може да произвежда този размер. Възможни линии са: %s", implode(', ', $possibleLines)));
        }

        $oResponse = null;

        DBC::$main->withTrans(function () use (&$newLineState, $idLine, $aRequest, &$oResponse) {
            $lineState = Scheduler::getLineState($idLine);

            $scheduleIDsForDel = array_map(function ($row) {
                return intval($row['id']);
            }, array_filter($lineState, function ($e) {
                return empty($e['read_only']);
            }));

            Scheduler::deleteSchedules($scheduleIDsForDel);

            DBC::$main->multiInsert('production_schedule', array_map(function ($orderSize) use ($idLine) {
                return [
                    'id_line' => $idLine,
                    'id_order_size' => $orderSize['id_order_size'],
                    'production_speed' => $orderSize['speed'],
                    'time_from' => time(),
                    'time_to' => time(),
                ];
            }, array_filter($aRequest['orderSizes'], function ($orderSize) {
                return empty($orderSize['read_only']);
            })));

            Scheduler::scheduleLine($idLine);

            $newLineState = Scheduler::getLineState($idLine);

            if($aRequest['orderSize'] && $aRequest['orderSize']['id_line'] != $idLine) {
                Scheduler::checkIfOrderSizeHasProduction($aRequest['orderSize']['id_order_size']);

                $aRequest['orderSize']['id_line'] = intval($aRequest['orderSize']['id_line']);
                $aRequest['orderSize']['id_order_size'] = intval($aRequest['orderSize']['id_order_size']);

                Scheduler::deleteSchedules(DBC::$main->selectColumn("
                    SELECT
                        ps.id
                    FROM production_schedule ps
                    JOIN production_orders_sizes pos ON pos.id = ps.id_order_size AND pos.complete = 0
                    JOIN production_schedule_rows psr ON psr.id_schedule = ps.id AND psr.to_arc = 0
                    WHERE 1
                    AND ps.to_arc = 0 
                    AND ps.id_line = {$aRequest['orderSize']['id_line']} 
                    AND ps.id_order_size = {$aRequest['orderSize']['id_order_size']}
                "));
                Scheduler::scheduleLine($aRequest['orderSize']['id_line']);
            }

            $oResponse = $this->load($aRequest);

            if(empty($aRequest['save'])) DBC::$main->resetTrans();
        });

        return $oResponse;
    }

    public function changeLine($aRequest) {
        $idLine = intval($aRequest['fields']['id_line']);
        $orderSize = $aRequest['orderSize'];

        if(empty($aRequest['orderSize']) || ($aRequest['orderSize'] && $aRequest['orderSize']['id_line'] == $idLine)) return $this->load($aRequest);

        $orderSizes = Scheduler::transformLineStateToRows(Scheduler::getLineState($idLine));
        $orderSizes[] = $orderSize;

        $aRequest['save'] = false;
        $aRequest['orderSizes'] = $orderSizes;

        return $this->orderChanged($aRequest);
    }


}