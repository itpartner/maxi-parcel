<?php
namespace emc\production\schedule;

use main\TPLBase;

class TPLWorkShiftEdit extends TPLBase {
    public function __construct($sPrefix, array $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);
        $this->aFrameConfig['title'] = "Редакция на смяна";
    }

    public function printHtml() {
        ?>

        <table cellspacing="3px">
            <tr>
                <td colspan="2">
                    <fieldset>
                        <legend>Информация</legend>
                        <table>
                            <tr>
                                <td style="width: 80px;"><label>Поръчка:</label></td>
                                <td><span name="order_info" class="clickable"></span></td>
                            </tr>
                            <tr>
                                <td><label>Клиент:</label></td>
                                <td><span name="client_name"></span></td>
                            </tr>
                            <tr>
                                <td><label>Размер:</label></td>
                                <td><span name="code" class="clickable"></span></td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset style="height: 75px;">
                        <legend>Време</legend>
                        <table>
                            <tr>
                                <td><? $this->printLabel(L('Пренастройка:'), 'real_setup_duration\'') ?></td>
                                <td><? $this->printElement('input', array('name' => 'real_setup_duration', 'keyRestriction' => 'time', 'style' => 'width:40px;')) ?> часа</td>
                            </tr>
                            <tr>
                                <td><? $this->printLabel(L('Производство:'), 'real_production_duration') ?></td>
                                <td><? $this->printElement('input', array('name' => 'real_production_duration', 'keyRestriction' => 'time', 'style' => 'width:40px;')) ?> часа</td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
                <td>
                    <fieldset style="height: 75px;">
                        <legend>Количество</legend>
                        <table>
                            <tr>
                                <td><? $this->printLabel(L('Тегло:'), 'produced_mass') ?></td>
                                <td><? $this->printElement('input', array('name' => 'produced_mass', 'keyRestriction' => 'float', 'style' => 'width:110px;text-align:right;')) ?> kg</td>
                            </tr>
                            <tr>
                                <td colspan="2"><? $this->printElement('input', array('type' => 'checkbox', 'name' => 'complete',)) ?><? $this->printLabel(L('Размерът е приключен'), 'complete') ?></td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr class="lots-consume-row" style="display: none;">
                <td colspan="2">
                    <fieldset>
                        <legend>Количества от ЛОТ за серия:</legend>
                        <ul class="group-sizes" style="padding: 0 0 5px 15px;"></ul>
                        <? $this->printElement('keyvaluepairs', array(
                                'name' => 'production_lots_consume',
                                'style' => 'width:350px;',
                                'options' => array(
                                    array('key' => 'lot_code', 'label' => 'код:', 'style' => 'width:60px'),
                                    array('key' => 'mass', 'label' => 'количество:', 'suffix' => 'кг', 'style' => 'width:40px;text-align:right;', 'keyRestriction' => 'float'),
                                )
                            )
                        ); ?>
                    </fieldset>
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <fieldset>
                        <legend>Забележка:</legend>
                        <textarea rows="4" style="width: 360px" name="note"></textarea>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div class="toolbar align-right" style="margin-top: 5px">
                        <? $this->printElement('lasteditedinfo'); ?>
                        <? $this->printElement('closebutton'); ?>
                        <? $this->printElement('button', array('text' => L('Запази'), 'class' => 'save align-right', 'iconCls' => 'fa fa-check fa-fw')); ?>
                    </div>
                </td>
            </tr>
        </table>


        <?php
    }

    public function getJSFunction() {
        return <<<'JS'
        (function (form, prefix, initParams) {
           var $form = $(form);
           
           $form.find('.save').click(function(){
                form.request('save', null, null, function(r){
                    if(initParams.parentGrid) initParams.parentGrid().loadData();
                });
           });
           
           var schedule = {};
           var group = {};
           
           form.addListener('beforeRequest', function(request){
                request['id'] = schedule['id'];
                request['schedule'] = schedule;
           });
           
           form.addListener('afterRequest', function(response){
                schedule = response['schedule'];
                group = response['group'];
                updateStates();
           });
           
           
           var updateStates = function(){
                $form.find('[name="order_info"]').text(schedule['num']+'/'+schedule['order_date']);
                $form.parents().find('span.title').text((schedule['has_logged_work'] > 0 ? 'Редакция' : 'Приключване') + ' на смяна '+schedule['work_shift_type']+'/'+schedule['work_shift_date'] + ' ['+schedule['code']+']');
                if(empty(schedule['is_editable'])) {
                    $('.save').remove();
                }
                
                if(!empty(group)) {
                    $form.find('.lots-consume-row').css({'display':''});
                    var ul = $form.find('ul.group-sizes');
                    ul.empty();
                    $.each(group, function(k, v){
                        var element = $("<li></li>");
                        element.text(v['size_code'] + ' - ' + v['produced_mass'] + ' кг');
                        element.attr({class:'clickable', style:'padding-bottom:3px;'});
                        element.click(function(){
                            openSize(v['id_order_size']);
                        });
                        ul.append(element);
                    });
                }
           }
           
           $form.find('[name="code"]').click(function() {
                openSize(schedule['id_order_size']);
           });
           
           
           var openSize = function(idSize){
                framework.createWindow({
                    tplName: 'emc/production/orders/sizes/Size',
                    baseParams: {
                        id: idSize,
                        onSizeChange: function(){
                            if(initParams.parentGrid) initParams.parentGrid().loadData();
                        }
                    }
                });   
           };
           
           $form.find('[name="order_info"]').click(function() {
             framework.createWindow({
                    tplName: 'emc/production/orders/Order',
                    baseParams: {
                        row: {
                            id: schedule['id_order']
                        },
                        onOrderChange: function(){
                            if(initParams.parentGrid) initParams.parentGrid().loadData();
                        }
                    }
                });
           });
           
           
        });
JS;
    }
}