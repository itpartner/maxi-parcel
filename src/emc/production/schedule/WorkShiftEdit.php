<?php
namespace emc\production\schedule;

use main\Access;
use main\db\DBC;
use main\form\FormPanelHandler;
use main\InvalidParameter;
use main\Util;

/**
 * @accessLevelDescription Достъп до форма за редакция/приключване на раб. смяна
 */
class WorkShiftEdit extends FormPanelHandler {

    public function init($aRequest = array(), $aInitParams = array()) {
        $aRequest['id'] = $aInitParams['id_schedule'];
        return $this->load($aRequest);
    }

    public function load($aRequest) {
        if(empty($aRequest['id'])) throw new InvalidParameter;

        $idSchedule = intval($aRequest['id']);

        $schedule = DBC::$main->selectRow("
            SELECT
                ps.*,
                po.num,
                po.id as id_order,
                po.name as client_name,
                DATE_FORMAT(ps.work_shift_date,'%d.%m.%Y') as work_shift_date,
                DATE_FORMAT(po.created_time,'%d.%m.%Y') as order_date,
                TIME_FORMAT(SEC_TO_TIME(real_setup_duration),'%H:%i') as real_setup_duration,
                TIME_FORMAT(SEC_TO_TIME(real_production_duration),'%H:%i') as real_production_duration,
                
                psr.real_setup_duration + psr.real_production_duration as has_logged_work, 
                
                TIME_FORMAT(SEC_TO_TIME(planned_setup_duration),'%H:%i') as planned_setup_duration,
                TIME_FORMAT(SEC_TO_TIME(planned_production_duration),'%H:%i') as planned_production_duration,
                
                IF(psr.real_setup_duration + psr.real_production_duration, ps.produced_mass, ps.planned_mass) as produced_mass,
                
                psz.code,
                pos.complete,
                psr.created_time as updated_time,
                CONCAT_WS(' ',pu.fname,pu.mname,pu.lname) AS updated_user,
                CONCAT_WS(' ',pc.fname,pc.mname,pc.lname) AS created_user,
                ''
            FROM production_schedule ps
            JOIN production_orders_sizes pos ON pos.id = ps.id_order_size
            JOIN production_sizes psz ON psz.id = pos.id_size
            JOIN production_orders po ON po.id = pos.id_order
            LEFT JOIN (
                SELECT
                psr.id_schedule,
                SUM(IF(psr.type = 'real_setup', psr.duration,0)) as real_setup_duration,
                SUM(IF(psr.type = 'real_production', psr.duration,0)) as real_production_duration,
                SUM(IF(psr.type = 'planned_setup', psr.duration,0)) as planned_setup_duration,
                SUM(IF(psr.type = 'planned_production', psr.duration,0)) as planned_production_duration,
                psr.created_time,
                psr.created_user
                FROM production_schedule_rows psr
                JOIN production_schedule p ON p.id = psr.id_schedule AND p.to_arc = 0 AND p.id = $idSchedule
                WHERE 1
                AND psr.to_arc = 0
                GROUP BY psr.id_schedule
            ) psr ON psr.id_schedule = ps.id
            LEFT JOIN personnel pu ON pu.id = psr.created_user
            LEFT JOIN personnel pc ON pc.id = pc.created_user
            WHERE 1
            AND ps.id = $idSchedule
            AND ps.to_arc = 0
            GROUP BY ps.id
        ");

        foreach ($schedule as $field => $value) $this->setFieldValue($field, $value);


        $prefix = str_replace(':','',$schedule['real_setup_duration']) + str_replace(':','',$schedule['real_production_duration']) ? 'real' : 'planned';
        foreach (array('_setup_duration', '_production_duration') as $key) $this->setFieldValue('real'.$key, $schedule[$prefix.$key]);

        $this->setLastEditInfo($schedule);
        $schedule['is_editable'] = Access::hasAccess(CanEditFinishedWorkShifts::class) || $schedule['id'] >= Scheduler::lastCompletedShiftIDForLine($schedule['id_line']);


        if(Scheduler::isLastSizeInProductionGroup($schedule['id_order_size'])) {
            $this->oResponse->{'group'} = Scheduler::getProductionGroupByIDSize($schedule['id_order_size']);

            $orderSizeIDs = array_map(function ($g) {
                return intval($g['id_order_size']);
            }, $this->oResponse->{'group'});

            $this->setFieldValue('production_lots_consume', DBC::$main->select("
                select plc.lot_code, plc.mass
                FROM production_lots_consume plc
                JOIN production_lots_consume_order_sizes plcos ON plcos.id_lot_consume = plc.id AND plcos.to_arc = 0 and plcos.id_order_size in(" . implode(',', $orderSizeIDs) . ")
                GROUP BY plc.id
            "));
        }

        $this->oResponse->{'schedule'} = $schedule;

        return $this->oResponse;
    }


    public function save($aRequest) {
        return DBC::$main->withTrans(function ($aRequest) {
            $idSchedule = intval($aRequest['id']);

            foreach (array('real_setup_duration', 'real_production_duration') as $k) {
                $aRequest['fields'][$k] = array_map(function ($e) {
                    return intval(trim($e));
                }, explode(':', $aRequest['fields'][$k]));
                $aRequest['fields'][$k] = $aRequest['fields'][$k][0] * 3600 + $aRequest['fields'][$k][1] * 60;
                if($aRequest['fields'][$k] < 0) throw new InvalidParameter("Невалидна стойност за време.", [$k]);
            }

            if($aRequest['fields']['produced_mass'] < 0) throw new InvalidParameter("Невалидна стойност за маса.", ['produced_mass']);

            if(Scheduler::isLastSizeInProductionGroup($aRequest['schedule']['id_order_size'])) {
                $group = Scheduler::getProductionGroupByIDSize($aRequest['schedule']['id_order_size']);
                $orderSizeIDs = array_map(function ($g) {
                    return intval($g['id_order_size']);
                }, $group);
                if(empty($orderSizeIDs)) throw new InvalidParameter();

                DBC::$main->execute("
                    UPDATE production_lots_consume_order_sizes plcos 
                    JOIN production_lots_consume plc ON plc.id = plcos.id_lot_consume
                    SET plcos.to_arc = 1, plc.to_arc = 1
                    WHERE 1
                    AND plcos.id_order_size in(" . implode(',', $orderSizeIDs) . ")
                    AND plcos.to_arc = 0
                ");

                $forUpdate = [];
                foreach ($aRequest['fields']['production_lots_consume'] as &$lotConsume) {
                    DBC::$main->update('production_lots_consume', $lotConsume);
                    foreach ($orderSizeIDs as $osid) $forUpdate[] = ['id_order_size' => $osid, 'id_lot_consume' => $lotConsume['id']];
                }

                DBC::$main->multiInsert('production_lots_consume_order_sizes', $forUpdate);
            };

            $schedule = Scheduler::editWorkShift($idSchedule, $aRequest['fields']['produced_mass'], $aRequest['fields']['real_production_duration'], $aRequest['fields']['real_setup_duration'], $aRequest['fields']['complete']);

            $schedule['note'] = $aRequest['fields']['note'];
            DBC::$main->update('production_schedule', $schedule);

            return $this->load($aRequest);
        }, [$aRequest]);
    }


}