<?php
namespace emc\production\schedule;

use main\db\DBC;
use main\grid\GridPanelHandler;

/**
 * Class RequiredMetalQuantity
 * @package emc\production\schedule
 * @accessLevelDescription Достъп до панел "Нужен метал"
 */
class RequiredMetalQuantity extends GridPanelHandler {
    public function __construct() {
        $this->oBase = DBC::$main;

        $this->sReportTitle = 'Нужен метал';

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'id_metal',
                'width' => 100,
                'headerText' => 'Метал',
                'items' => DBC::$slave->selectAssoc("SELECT id as _k, id, CONCAT('(',code,') ', name) as text FROM metals WHERE to_arc = 0"),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_FLOAT,
                'dataField' => 's',
                'width' => 100,
                'headerText' => 'Дебелина',
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_INT,
                'dataField' => 'mass',
                'width' => 100,
                'headerText' => 'Количество',
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'metal_brand',
                'width' => 100,
                'headerText' => 'Марка/код',
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
        );

        $this->sReportSelectStatement = "
            SELECT
                psz.id_metal,
                pos.s,
                round(sum(ps.planned_mass * m.rate)) AS mass,
                pos.metal_brand
            FROM production_schedule ps
            JOIN production_orders_sizes pos ON pos.id = ps.id_order_size
            JOIN production_sizes psz ON psz.id = pos.id_size
            JOIN metals m ON m.id = psz.id_metal
        ";

        $this->aReportWhereStatement[] = "ps.to_arc = 0";
        $this->sReportGroupStatement = "concat_ws('@@',m.id,pos.s,pos.id_metal_brand)";

        parent::__construct();
    }

    function loadData($aRequest, $nTimeFrom = 0, $nTimeTo = 0) {
        $timeWhere = $this->getWhereFromTimeButton('ps.work_shift_date', $aRequest['fields']['time']);
        $this->aReportWhereStatement[] = "ps.id_line = {$aRequest['fields']['id_line']}";
        $this->aReportWhereStatement[] = $timeWhere;
        return parent::loadData($aRequest, $nTimeFrom, $nTimeTo);
    }


    function init($aRequest = array(), $aInitParams = array()) {
        $aRequest['fields'] = $aInitParams['fields'];
        return $this->loadData($aRequest);
    }

}