<?php

namespace emc\production\schedule\daemons;

use emc\production\schedule\Scheduler;
use main\daemons\BaseRobot;
use main\db\DBC;
use main\notifications\Mailer;
use main\Util;

class RobotNotifyProductionScheduleChanges extends BaseRobot {

    public function run($time, $params = array()) {

        DBC::$main->withTrans(function () {

            $startTime = strtotime(date('Y') . 'W' . date('W'));
            $endTime = strtotime(date('Y') . 'W' . (date('W') + 2));


            $lines = DBC::$slave->selectAssoc("SELECT id as _k, pl.* FROM production_lines pl WHERE to_arc = 0");
            $newSnapshots = [];

            $aNotificationsByManager = array();
            foreach ($lines as $idLine => $line) {
                $previousSnapshot = json_decode(DBC::$slave->selectField("SELECT snapshot FROM production_schedule_snapshots WHERE id_line = $idLine ORDER BY created_time DESC LIMIT 1"), true);

                $currentSnapshot = array_filter(Scheduler::transformLineStateToRows(Scheduler::getLineState($idLine)), function ($row) use($startTime, $endTime) {
                    return Util::SQLDateToTimeStamp($row['time_from']) >= $startTime && Util::SQLDateToTimeStamp($row['time_to']) <= $endTime;
                });

                $newSnapshots[] = [
                    'snapshot' => json_encode($currentSnapshot, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
                    'id_line' => $idLine
                ];

                if(empty($previousSnapshot)) continue;

                // проверка за добавени/премахнати размери

                $aPreviousSnapshot = array();
                $aCurrentSnapshot = array();
                $aNewSizes = array();
                $bHasDifferentOrder = false;
                $aTotalMassChangeKeys = array();
                foreach($previousSnapshot as $key => $snapshot) { $aPreviousSnapshot[$snapshot['id_order_size']] = $snapshot; }
                foreach($currentSnapshot as $key => $snapshot) { $aCurrentSnapshot[$snapshot['id_order_size']] = $snapshot; }

                $aRemovedSizes = $aPreviousSnapshot;
                $aCurrentSnapshotOrg = $aCurrentSnapshot;
                $aPreviousSnapshotOrg = $aPreviousSnapshot;
                foreach($aCurrentSnapshot as $key => $current) {
                    if(empty($aPreviousSnapshot[$key])) {
                        $aNewSizes[$key] = $current;
                        unset($aCurrentSnapshot[$key]); // премахване на добавените размери
                    }
                    else {
                        // проверка за промяна в total_mass
                        if($current['total_mass'] != $aPreviousSnapshot[$key]['total_mass']) {
                            $aTotalMassChangeKeys[] = $key;
                        }
                    }
                    unset($aRemovedSizes[$key]);
                }
                foreach($aRemovedSizes as $key => $val) { // премахване на премахнатите размери
                    if(!empty($aPreviousSnapshot[$key])) unset($aPreviousSnapshot[$key]);
                }

                // проверка в поредността
                $aCurrent = array_keys($aCurrentSnapshot);
                $aPrevious = array_keys($aPreviousSnapshot);
                foreach($aCurrent as $key => $current) {
                    if($current != $aPrevious[$key]) { $bHasDifferentOrder = true; break; }
                }

                // създаване на нотификация при налични промени(или масова извън цикъла)
                if($bHasDifferentOrder || !empty($aNewSizes) || !empty($aRemovedSizes) || !empty($aTotalMassChangeKeys)) {
                    $aNotificationsByManager[$line['id_line_manager']][] = array(
                        'lineInfo' => $line,
                        'hasDifferentOrder' => $bHasDifferentOrder,
                        'newSizes' => $aNewSizes,
                        'removedSizes' => $aRemovedSizes,
                        'totalMassChangeKeys' => $aTotalMassChangeKeys,
                        'prevSnapshot' => $aPreviousSnapshotOrg,
                        'currentSnapshot' => $aCurrentSnapshotOrg
                    );
                }
            }
            if(!empty($aNotificationsByManager)) {
                foreach($aNotificationsByManager as $nIdManager => $aNotifications) {
                    // извличане на мейла на мениджъра на линия
                    $aManager = DBC::$slave->selectRow("SELECT * FROM personnel p WHERE p.id = " . intval($nIdManager));

                    ob_start();
                    // създаване на html-а
                    foreach($aNotifications as $aLineChanges) {?>
                        <h3>Настъпили промени в производствената програма за линия <strong><?= $aLineChanges['lineInfo']['code']; ?></strong>:</h3>
                        <p style="font-size: 12px; font-style: italic;">
                            <?
                            if(!empty($aLineChanges['hasDifferentOrder'])) echo "- промяна в подредбата<br/>";
                            if(!empty($aLineChanges['removedSizes'])) echo "- премахнати редове<br/>";
                            if(!empty($aLineChanges['newSizes'])) echo "- добавени редове<br/>";
                            if(!empty($aLineChanges['totalMassChangeKeys'])) echo "- промяна в количество<br/>";
                            ?>
                        </p>
                        <table>
                            <tr>
                                <td valign="top" width="50%">
                                    <?php
                                    foreach($aLineChanges['prevSnapshot'] as $nKey => $aRow) {
                                        $class = 'unchanged';
                                        if(!empty($aLineChanges['removedSizes'][$nKey])) $class = "removed";
                                        else if(!empty($aLineChanges['newSizes'][$nKey])) $class = "added";
                                        ?>
                                        <div>
                                            <ul id="sortable">
                                                <li class="item <?=$class?>">
                                                    <table class="prev-table" cellpadding="3" border="1px" style="border-collapse: collapse;">
                                                        <tr>
                                                            <td>
                                                                <?=$aRow['order_num']?>
                                                            </td>
                                                            <td<?if(in_array($nKey, $aLineChanges['totalMassChangeKeys'])) echo " style=\"background-color:pink\""?>><?=$aRow['total_mass']?></td>
                                                            <td>Начало</td>
                                                            <td>Край</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <?=$aRow['order_name']?>
                                                            </td>
                                                            <td>Желано</td>
                                                            <td>
                                                                <?=$aRow['desired_production_start']?>
                                                            </td>
                                                            <td>
                                                                <?=$aRow['desired_production_end']?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <?=$aRow['size_code']?>
                                                            </td>
                                                            <td>Планирано</td>
                                                            <td>
                                                                <?=$aRow['time_from']?>
                                                            </td>
                                                            <td>
                                                                <?=$aRow['time_to']?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </li>
                                            </ul>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td valign="top">
                                    <?php
                                    foreach($aLineChanges['currentSnapshot'] as $nKey => $aRow) {
                                        $class = 'unchanged';
                                        if(!empty($aLineChanges['removedSizes'][$nKey])) $class = "removed";
                                        else if(!empty($aLineChanges['newSizes'][$nKey])) $class = "added";

                                        ?>
                                        <div>
                                            <ul id="sortable">
                                                <li class="item <?=$class?>">
                                                    <table class="current-table" cellpadding="3" border="1px" style="border-collapse: collapse;">
                                                        <tr>
                                                            <td>
                                                                <?=$aRow['order_num']?>
                                                            </td>
                                                            <td<?if(in_array($nKey, $aLineChanges['totalMassChangeKeys'])) echo " style=\"background-color:pink\""?>><?=$aRow['total_mass']?></td>
                                                            <td>Начало</td>
                                                            <td>Край</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <?=$aRow['order_name']?>
                                                            </td>
                                                            <td>Желано</td>
                                                            <td>
                                                                <?=$aRow['desired_production_start']?>
                                                            </td>
                                                            <td>
                                                                <?=$aRow['desired_production_end']?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <?=$aRow['size_code']?>
                                                            </td>
                                                            <td>Планирано</td>
                                                            <td>
                                                                <?=$aRow['time_from']?>
                                                            </td>
                                                            <td>
                                                                <?=$aRow['time_to']?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </li>
                                            </ul>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                        </table>

                        <!--suppress CssUnusedSymbol -->
                        <style type="text/css">

                            ul#sortable {
                                list-style: none;
                                overflow-y: auto;
                                padding: 0 !important;
                            }

                            #sortable li.item {
                                border: 1px solid #37474F;
                                padding: 3px !important;
                                margin: 0 0 5px 0 !important;
                                border-radius: 3px;
                                margin-right: 10px !important;
                            }

                            #sortable li.unchanged {
                                border-style: dashed;
                                background-color: #FFF8E1;
                            }

                            #sortable li.removed {
                                border-style: dashed;
                                background-color: #FFF8E1;
                                text-decoration: line-through;
                            }

                            #sortable li.added {
                                border-style: dashed;
                                background-color: #CDDC39;
                            }

                            #sortable div.container {
                                float: left;
                                width: 50%;
                            }

                            .sortable-ghost {
                                opacity: 0.3;
                            }

                            .current-table, .prev-table {
                                border-collapse: collapse;
                            }

                            .current-table td, .prev-table td {
                                padding: 2px 5px 2px 5px;
                                font-size: 10px;
                            }

                            .current-table td + td, .prev-table td + td, th + th { border-left: 1px solid #E0E0E0; }
                            .current-table tr + tr, .prev-table td + td, { border-top: 1px solid #E0E0E0; }

                        </style>

                        <?php
                    }
                    $messageContent = ob_get_clean();

                    ob_start();
                    ?>
                        <html>
                        <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                        </head>
                        <body>
                            <table align="center" border="0" cellspacing="0" cellpadding="0" style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #2e2e2e;">
                                <tr>
                                    <td align="center" bgcolor="#f8f8f8" style="padding: 0 0 40px 0;">
                                        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="padding: 10px 20px; font-family: Verdana, Arial, Helvetica, sans-serif;">
                                                    <?= trim($messageContent) ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </body>
                        </html>
                    <?

                    $fileName = sys_get_temp_dir() . DIRECTORY_SEPARATOR . $aNotifications[0]['lineInfo']['code'] . ' ' . date("Y-m-d") . ".html";
                    file_put_contents($fileName, ob_get_clean());

                    Mailer::send(array($aManager['email']), L("Налични промени в производствена програма"), "Промените са в прикачения файл.", null, null, [$fileName]);
                    unlink($fileName);
                }
            }

            DBC::$main->multiInsert('production_schedule_snapshots', $newSnapshots);
        });
    }
}