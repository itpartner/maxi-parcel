<?php
namespace emc\production\schedule;

use main\TPLBase;

class TPLEditMassInStock extends TPLBase {

	public function __construct($sPrefix, array $aInitParams) {
		parent::__construct($sPrefix, $aInitParams);

		$this->bPrintWithTitle = true;
		$this->aFrameConfig['title'] = L('Редакция');
	}

	protected function printStyles($sPrefix) {
		?>
		<style>
			#<?=$this->sPrefix?> .width100 {
				box-sizing: border-box;
				-moz-box-sizing: border-box;
				-webkit-box-sizing: border-box;
				width: 75px;
			}
		</style>
		<?php
	}

	public function printHtml() { ?>
		<table style="width: 100%">
			<tr>
				<td><? $this->printElement('label', array('fieldName' => 'mass_in_stock', 'text' => L('Налично количество метал:')));?></td>
				<td><? $this->printElement('input', array('name' => 'mass_in_stock', 'class' => 'width100', 'keyRestriction' => 'integer')); ?></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: right">
					<? $this->printElement('confirmbutton'); ?>
					<? $this->printElement('closebutton'); ?>
				</td>
			</tr>
		</table>
		<?php
	}

	public function getJSFunction() {
		return <<<'JS'

        (function(container, prefix, baseParams) {
            $(container).find('.confirm').click(function(){
                container.request('save', { row: baseParams['row'] }, null, function(){
                    $(container).find('.close').click();
                    baseParams.getParentGrid().loadData();
                });
            });
        });
JS;
	}


}