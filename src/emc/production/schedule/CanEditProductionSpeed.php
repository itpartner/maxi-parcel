<?php
namespace emc\production\schedule;

use emc\access\AccessLevel;

/**
 * @accessLevelDescription Право за редакция на производствена скорост
 */
class CanEditProductionSpeed implements AccessLevel{
    public static $exMessage = "Нямате права за редакция на производствена скорост.";
}