<?php
namespace emc\production\schedule;

use main\Access;
use main\db\DBC;
use main\form\FormPanelHandler;
use main\InvalidParameter;

/**
 * Class EditMassInStock
 * @accessLevelDescription Редактиране на количество метал
 */
class EditMassInStock extends FormPanelHandler {

	public function __construct() {
		Access::checkAccess(__CLASS__);
		return parent::__construct();
	}

	public function init($aRequest = array(), $aInitParams = array()) {
		$this->setField('mass_in_stock', $aInitParams['row']['mass_in_stock']);

		return $this->oResponse;
	}

	public function save($aRequest) {
		$nMassInStock = intval($aRequest['fields']['mass_in_stock']);

		if($nMassInStock < 0) throw new InvalidParameter(L('Не се позволява въвеждане на отрицателно число.'), array('mass_in_stock'));
		DBC::$main->multiInsert('production_orders_sizes', array(array('id' => $aRequest['row']['id_order_size'], 'mass_in_stock' => $nMassInStock)));

		return $this->oResponse;
	}
}