<?php
namespace emc\production\schedule;

use main\db\DBC;
use main\TPLBase;

class TPLPlanningTool extends TPLBase {

    private $params;

    public function __construct($sPrefix, array $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);
        $this->params = $aInitParams;

        $this->aFrameConfig['title'] = "Препланиране на линия";
    }

    function printStyles($sPrefix) {
        ?>
        <!--suppress CssUnusedSymbol -->
        <style type="text/css">

            #<?=$sPrefix;?> ul#sortable {
                list-style: none;
                overflow-y: auto;
            }

            #<?=$sPrefix;?> #sortable li.item {
                border: 1px solid #37474F;
                padding: 3px !important;
                margin: 0 0 5px 0px !important;
                border-radius: 3px;
                margin-right: 10px !important;
            }

            #<?=$sPrefix;?> #sortable li.movable {
                border-style: dashed;
                cursor: pointer;
                background-color: #FFF8E1;
            }

            #<?=$sPrefix;?> #sortable li.delayed {
                border: 2px dotted #E91E63;
                background-color: rgba(233,30,99,0.15);
            }

            #<?=$sPrefix;?> #sortable li.cancelled {
                font-style: italic;
                opacity: 0.5;
            }

            #<?=$sPrefix;?> #sortable li.static {
                cursor:no-drop !important;
                background-color: #ECEFF1;
            }

            #<?=$sPrefix;?> #sortable li.selected {
                background-color: #CDDC39 !important;
                font-weight: bold;
            }

            #<?=$sPrefix;?> #sortable div.container {
                float:left;
                width:50%;
            }

            #<?=$sPrefix;?> .sortable-ghost {
                opacity: 0.3;
            }

            #<?=$sPrefix;?> .times-table {
                border-collapse: collapse;
            }

            #<?=$sPrefix;?> .times-table td, #<?=$sPrefix;?> .info-table td {
                    padding: 2px 5px 2px 5px;
            }

            #<?=$sPrefix;?> .times-table td + td, th + th { border-left: 1px solid #E0E0E0; }
            #<?=$sPrefix;?> .times-table tr + tr { border-top: 1px solid #E0E0E0; }

            #<?=$sPrefix;?> .orderUnread {
                font-style: italic;
                font-weight: bold;
            }

        </style>


        <?php
    }


    public function printHtml() {
        ?>
        <script src="js/include/Sortable.js"></script>
        <div style="width: 600px;">
            <div class="toolbar" style="float: left;">
                <? $this->printElement('menubutton', array('name' => 'id_line', 'class' => 'align-left'), array_map(function ($line) {
                    return [
                        'name' => $line['id'],
                        'text' => $line['code'],
                        'iconCls' => 'fa fa ' . ($line['status'] == 'active' ? 'fa-check-circle' : 'fa-ban')
                    ];
                }, DBC::$slave->select("SELECT * FROM production_lines pl WHERE pl.to_arc = 0 ORDER BY pl.status, pl.code"))); ?>
            </div>

            <div style="float: left;">
                <ul id="sortable"></ul>
            </div>

            <div style="float:left; margin-top: 5px; width: 100%">
                <div class="toolbar align-right">
                    <? $this->printElement('closebutton'); ?>
                    <? $this->printElement('button', array('text' => L('Потвърди'), 'class' => 'confirm-schedule align-right', 'iconCls' => 'fa fa-check fa-fw')); ?>
                </div>
            </div>
            <div style="clear: both;"></div>

        </div>

        <?php
    }


    public function getJSFunction() {
        return /** @lang JavaScript */
            <<<'JS'
        (function (form, prefix, initParams) {
            var orderSizes = [];
            var selectedOrderSize = initParams['rows'][0] ? {
                'id_order_size': initParams['rows'][0]['id_order_size'],
                'id_line' : initParams['fields']['id_line']
            } : null;
            
            var sortableContainer = $(form).find("#sortable");
            
            form.fields['id_line'].setValue(initParams['fields']['id_line']);
            form.fields['id_line'].addListener('click', function(){
                form.request('changeLine');
            });
            
            var onOrderUpdate = function(save) {
                form.request('orderChanged', {save:save}, null, function(){
                    if(save) {
                        selectedOrderSize = null;
                        updateStates();
                        if(initParams.getParentGrid) initParams.getParentGrid().loadData();
                    }
                });
            };
            
            $(form).find('.confirm-schedule').click(function(){
                onOrderUpdate(true);
            });
            
            $(form).find('.close').click(function() {
                if(initParams.getParentGrid) initParams.getParentGrid().loadData();
            });
            
            getOrderSizes = function() {
               var orderSizes = [];
               sortableContainer.find('li.item').each(function(){orderSizes.push(this['orderSize']);});
               return orderSizes; 
            };
            
            var updateStates = function(){
                $(form).parents().find('span.title').text(selectedOrderSize ? "Препланиране на размер към линия" : "Препланиране на линия");
            
                sortableContainer.empty();
                $.each(orderSizes, function(k,orderSize){
                    sortableContainer.append(drawSize(orderSize));
                });
                
                sortableContainer.find('[basictip]').basicToolTip({
                    contentAttribute: 'basictip'
                });
                
                Sortable.create(sortableContainer.get(0), {
                    scroll: true,
                    draggable: ".movable",
                    onUpdate: function (/**Event*/ evt) {
                       onOrderUpdate(false);
                    }
                });
            };
            
            var drawSize = function(orderSize) {
                var li = $("<li></li>");
                li.get(0).orderSize = orderSize;
                
                li.addClass('item');
                
                if(selectedOrderSize && selectedOrderSize['id_order_size'] == orderSize['id_order_size']) li.addClass('selected');
                
                li.addClass(orderSize['read_only'] ? 'static' : 'movable');
                if(orderSize['order_status'] == 'cancelled') {
                    li.attr('basictip','Размер от отказана поръчка.');
                    li.addClass('cancelled');
                }
                
                if(intval(orderSize['is_delayed'])) li.addClass('delayed');
                
                var readRow = $('');
                if(intval(orderSize['is_read']) === 0) {
                    readRow = $('' +
                        '<table class="info-table" width="100%">'+
                            '<tr><td class="unreadRow">' +
                                '<div><div style="width:100%;text-align:center;"><div class="fa fa-angle-double-right" title="Нов размер"></div></div></div>' +
                            '</td></tr>' +
                        '</table>' +
                    '');
                    
                    readRow.find('.unreadRow').addClass('clickable');
                    readRow.find('.unreadRow').click(function() {
                        form.request('markRowAsRead', { id_order_size: orderSize['id_order_size'] });
                    });
                }
                
                var infoTable = $(
                 '<table class="info-table" width="100%">'+
                    '<tr><td class="order_num"></td></tr>'+
                    '<tr><td class="order_name"></td></tr>'+
                    '<tr><td class="size_code"></td></tr>'+
                '</table>'); 
                
                infoTable.find('.order_num').addClass('clickable');
                infoTable.find('.order_num').click(function(){
                    openOrder(orderSize['order_id']);
                });
                
                infoTable.find('.order_num').text(orderSize['order_num'] + '/' + Util.formatDate(Util.mysqlDateTimeToJS(orderSize['order_time']), 'D.M.Y'));
                infoTable.find('.order_name').text(orderSize['order_name']);
                infoTable.find('.size_code').addClass('clickable');
                infoTable.find('.size_code').click(function(){
                    openOrderSize(orderSize['id_order_size']);
                });
                infoTable.find('.size_code').text(orderSize['size_code']);
                 
                var timesTable = $(
                 '<table width="100%" class="times-table">'+
                    '<tr><td></td><td>Начало</td><td>Край</td></tr>'+
                    '<tr><td align="right">Желано&nbsp;</td><td class="d_start"></td><td class="d_end"></td></tr>'+
                    '<tr><td align="right">Планирано&nbsp;</td><td class="p_start"></td><td class="p_end"></td></tr>'+
                '</table>'); 
                
                $(timesTable).find('.d_start').text(orderSize['desired_production_start']);
                $(timesTable).find('.d_end').text(orderSize['desired_production_end'] );
            
                $(timesTable).find('.p_start').text(orderSize['time_from']);
                $(timesTable).find('.p_end').text(orderSize['time_to']);
                                
                var rowTable = $('<table></table>');
                if(intval(orderSize['is_read']) === 0) rowTable.addClass('orderUnread'); 
                
                var isReadTd = $('<td width="50px"></td>').append(readRow);
                var leftTd = $('<td width="250px"></td>').append(infoTable);
                var rightTd = $('<td width="350px"></td>').append(timesTable);
                var tr = $('<tr></tr>');
                tr.append(isReadTd);
                tr.append(leftTd);
                tr.append(rightTd);
                rowTable.append(tr);
                
                li.append(rowTable);
                 
                return li;
            };
           
           
            var openOrderSize = function(idOrderSize) {
                framework.createWindow({
                    tplName: 'emc/production/orders/sizes/Size',
                    baseParams: {
                        id: idOrderSize,
                        onSizeChange: function(){
                            form.request('load');
                            if(initParams.getParentGrid) initParams.getParentGrid().loadData();
                        }
                    }
                });
            };
           
            var openOrder = function(idOrder) {
                framework.createWindow({
                    tplName: 'emc/production/orders/Order',
                    baseParams: {
                        row: {
                            id:idOrder,
                        },
                        onOrderChange: function(){
                            form.request('load');
                            if(initParams.getParentGrid) initParams.getParentGrid().loadData();
                        }
                    }
                });
            };

            form.addListener('beforeRequest',function(request){
                request['orderSize'] = selectedOrderSize;
                request['orderSizes'] = getOrderSizes();
            });

            form.addListener('afterRequest', function(response){
                orderSizes = response['orderSizes'];
                updateStates();
            });
        
            setTimeout(function(){
                $(form).find('#sortable').css({height: Math.max(document.documentElement.clientHeight, window.innerHeight || 0) - 120 + 'px'});
                $(form).parents('.window-outer').css({top:'5px'});
            },0);
        
        });
JS;
    }

}