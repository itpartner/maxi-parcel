<?php
namespace emc\production\schedule;

use emc\nomenclatures\Profiles;
use main\db\DBC;
use main\grid\GridPanelHandler;
use main\Util;

/**
 * @accessLevelDescription Експорт на Производствени програми
 */
class ScheduleExport extends GridPanelHandler {

    public function __construct() {
        parent::__construct();

        $this->oBase = DBC::$main;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => 'group',
                'children' => array(
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_STRING,
                        'dataField' => 'client_name',
                        'width' => 125,
                        'headerText' => L('Име на клиента')
                    )),
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_STRING,
                        'dataField' => 'order_num',
                        'width' => 60,
                        'headerText' => L('№ на поръчка')
                    )),
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_STRING,
                        'dataField' => 'client_order_num',
                        'width' => 75,
                        'headerText' => L('Доставка №')
                    ))
                )
            )),
            $this->newColumn(array(
                'type' => 'group',
                'children' => array(
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_ENUM,
                        'dataField' => 'id_direction',
                        'width' => 75,
                        'headerText' => L('Направление'),
                        'items' => DBC::$slave->selectAssoc("SELECT id as _k, id, name as text FROM production_directions WHERE to_arc = 0")
                    )),
                    $this->newColumn(array(
                        'type' => 'group',
                        'headerText' => L('Размери на тръба/профил, mm'),
                        'children' => array(
                            $this->newColumn(array(
                                'type' => GridPanelHandler::CTYPE_STRING,
                                'dataField' => 'size',
                                'width' => 150,
                                'headerText' => L('Код')
                            )),
                            $this->newColumn(array(
                                'type' => GridPanelHandler::CTYPE_STRING,
                                'dataField' => 's_production',
                                'width' => 30,
                                'headerText' => L('S')
                            )),
                            $this->newColumn(array(
                                'type' => GridPanelHandler::CTYPE_STRING,
                                'dataField' => 'l',
                                'width' => 50,
                                'headerText' => L('L')
                            )),
                        )
                    )),
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_ENUM,
                        'dataField' => 'id_metal_brand',
                        'width' => 50,
                        'headerText' => L('Марка вид'),
                        'items' => DBC::$slave->selectAssoc("SELECT id as _k, id, name as text FROM production_metal_brands WHERE to_arc = 0")
                    )),
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_ENUM,
                        'dataField' => 'id_standard',
                        'width' => 70,
                        'headerText' => L('Стандарт'),
                        'items' => DBC::$slave->selectAssoc("SELECT id as _k, id, code as text FROM production_standards WHERE to_arc = 0")
                    )),
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_STRING,
                        'dataField' => 'total_mass',
                        'width' => 60,
                        'headerText' => L('Общо колич., кг')
                    )),
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_STRING,
                        'dataField' => 'work_shifts',
                        'width' => 75,
                        'headerText' => L('Смяна дата')
                    )),
                    $this->newColumn(array(
                        'type' => 'group',
                        'headerText' => L('Пренастройка'),
                        'children' => array(
                            $this->newColumn(array(
                                'type' => GridPanelHandler::CTYPE_STRING,
                                'dataField' => 'planned_setup_from',
                                'width' => 45,
                                'headerText' => L('начало')
                            )),
                            $this->newColumn(array(
                                'type' => GridPanelHandler::CTYPE_STRING,
                                'dataField' => 'planned_setup_to',
                                'width' => 45,
                                'headerText' => L('край')
                            ))
                        )
                    )),
                    $this->newColumn(array(
                        'type' => 'group',
                        'headerText' => L('Производство'),
                        'children' => array(
                            $this->newColumn(array(
                                'type' => GridPanelHandler::CTYPE_STRING,
                                'dataField' => 'planned_production_from',
                                'width' => 45,
                                'headerText' => L('начало')
                            )),
                            $this->newColumn(array(
                                'type' => GridPanelHandler::CTYPE_STRING,
                                'dataField' => 'planned_production_to',
                                'width' => 45,
                                'headerText' => L('край')
                            ))
                        )
                    ))
                )
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'production_speed',
                'width' => 50,
                'headerText' => L('Скорост ±10 m/min')
            )),
            $this->newColumn(array(
                'type' => 'group',
                'headerText' => L('ДК-05-38_01'),
                'children' => array(
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_INT,
                        'dataField' => 'planned_mass',
                        'width' => 70,
                        'headerText' => L('Количество за смяна, кг')
                    ))
                )
            ))
        );
    }

    public function getGridData($aRequest, $nTimeFrom = 0, $nTimeTo = 0) {
        $schedule = new Schedule();
        $result = [];

        $aRequest['confirmed_orders_only'] = true;

        $aData = $schedule->getGridData($aRequest);
        foreach ($aData as $comboRow) {
            foreach (array_values($comboRow['shifts']) as $i => $shiftRow) {
                if($i == 0) {
                    if($shiftRow['profile_type'] == Profiles::R) $shiftRow['size_dim'] = 'Ø ' . $shiftRow['d'];
                    else $shiftRow['size_dim'] = $shiftRow['a'] . ' x '  . $shiftRow['h'] . ' ' . $shiftRow['code_metal'];
                }

                $shiftRow['work_shifts'] = $shiftRow['work_shift_type'] . '/' . date('d.m.Y', strtotime($shiftRow['work_shift_date']));
                if(!is_null($shiftRow['planned_setup_from'])) $shiftRow['planned_setup_from'] = date('H:i', strtotime($shiftRow['planned_setup_from']));
                if(!is_null($shiftRow['planned_setup_to'])) $shiftRow['planned_setup_to'] = date('H:i', strtotime($shiftRow['planned_setup_to']));
                $shiftRow['planned_production_from'] = Util::SQLDateToTimeStamp($shiftRow['planned_production_from']) ? date('H:i', strtotime($shiftRow['planned_production_from'])) : '';
                $shiftRow['planned_production_to'] = Util::SQLDateToTimeStamp($shiftRow['planned_production_to']) ? date('H:i', strtotime($shiftRow['planned_production_to'])) : '';

                $result[] = $i == 0 ? array_merge($comboRow, $shiftRow) : $shiftRow;
            }
        }
        $totals = $schedule->setGridTotals($aData);

        $totals_row = array(
            'work_shifts' => L('пренастр.'),
            'planned_setup_from' => gmdate("H:i", $totals['planned_setup_duration']),
            'planned_setup_to' => L('часа'),
            'production_speed' => 'Σ',
            'planned_mass' => $totals['planned_mass'],
            'L' => ''
        );
        $result[] = $totals_row;

        return $result;
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
    }

}