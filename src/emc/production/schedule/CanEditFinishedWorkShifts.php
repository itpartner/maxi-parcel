<?php
namespace emc\production\schedule;

use emc\access\AccessLevel;

/**
 * @accessLevelDescription Право да се редактират приключили работни смени
 */
class CanEditFinishedWorkShifts implements AccessLevel{
    public static $exMessage = "Нямате права за редакция на работна смяна преди последно приключената.";
}