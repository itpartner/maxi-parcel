<?php
namespace emc\production\schedule;

use emc\access\profiles\ProfileMarketingManager;
use emc\nomenclatures\productionlines\ProductionLines;
use emc\nomenclatures\sizes\Sizes;
use emc\notifications\Notifications;
use main\Access;
use main\db\DBC;
use main\Exception;
use main\InvalidParameter;
use main\Util;

class Scheduler {

    public static function getLineState($idLine) {
        $idLine = intval($idLine);
        return DBC::$main->select(" 
            SELECT 
                ps.*,
                pos.total_mass,
                pos.produced_total_mass,
                pos.complete as order_size_complete,
                pos.l as order_size_l,
                pos.s as order_size_s,
                pos.desired_production_start,
                pos.desired_production_end,
                pos.complete,
                
                pos.complete + psr.real_production_duration + psr.real_setup_duration > 0 AS read_only,
                pos.is_delayed, 
                pos.is_read,
                
                psr.real_setup_duration,
                psr.real_production_duration,
                !ps.time_from AS is_new_size,
                
                COALESCE(psr.time_from, ps.time_from) as time_from,
                COALESCE(psr.time_to, ps.time_to) as time_to,
                
                prds.id as id_production_size,
                prds.m as size_m,
                prds.s as size_s,
                prds.code as size_code,
                
                po.id as order_id,
                po.num as order_num,
                po.name as order_name,
                po.created_time as order_time,
                po.status as order_status,
                
                IF(ps.production_speed > 0 AND ps.production_speed != pls.speed, ps.production_speed, pls.speed) as speed,
                GROUP_CONCAT(DISTINCT plsg.group) AS size_groups,
                
                pl.setup_time_big,
                pl.setup_time_small,
                pl.setup_time_length,
                pl.setup_time_thickness,
                ''
            FROM production_schedule ps
            LEFT JOIN (
                SELECT
                psr.id_schedule,
                SUM(IF(psr.type = 'real_setup', psr.duration,0)) as real_setup_duration,
                SUM(IF(psr.type = 'real_production', psr.duration,0)) as real_production_duration,
                MIN(psr.time_from) as time_from,
                MAX(psr.time_to) as time_to
                FROM production_schedule_rows psr
                JOIN production_schedule p ON p.id = psr.id_schedule AND p.id_line = $idLine AND p.to_arc = 0
                WHERE 1
                AND psr.to_arc = 0
                AND psr.type IN('real_setup', 'real_production')
                GROUP BY psr.id_schedule
            ) psr ON psr.id_schedule = ps.id
            JOIN production_orders_sizes pos ON pos.id = ps.id_order_size
            JOIN production_sizes prds ON prds.id = pos.id_size
            JOIN production_lines pl ON pl.id = ps.id_line
            JOIN production_lines_sizes pls ON pls.id_line = ps.id_line AND pls.id_size = prds.id AND pls.to_arc = 0
            LEFT JOIN production_orders po ON po.id = pos.id_order
            LEFT JOIN (
                SELECT
                    MAX(ps.id) as last_id
                FROM production_schedule ps
                JOIN production_schedule_rows psr ON psr.id_schedule = ps.id AND psr.to_arc = 0 AND psr.type IN ('real_production','real_setup')
                JOIN production_orders_sizes pos ON pos.id = ps.id_order_size AND pos.complete = 1 and pos.to_arc = 0
                WHERE 1
                AND ps.to_arc = 0
                AND ps.id_line = $idLine    
            ) AS pss ON ps.id >= pss.last_id
            LEFT JOIN production_lines_sizes_groups plsg ON plsg.id_line_size = pls.id AND plsg.group > 0
            WHERE 1
            AND ps.id_line = $idLine
            AND ps.to_arc = 0
            GROUP by ps.id
            ORDER by ps.id
            FOR UPDATE
        ");
    }

    public static function deleteSchedules($scheduleIDsForDel) {
        $scheduleIDsForDel = implode(',', array_map('intval', $scheduleIDsForDel));
        if(!empty($scheduleIDsForDel)) {
            DBC::$main->execute("UPDATE production_schedule ps SET ps.to_arc = 1 WHERE ps.id IN($scheduleIDsForDel) and ps.to_arc = 0;");
            DBC::$main->execute("UPDATE production_schedule_rows psr SET psr.to_arc = 1 WHERE psr.id_schedule IN($scheduleIDsForDel) and psr.to_arc = 0;");
        }
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    private static function schedulesCleanUp() {
        DBC::$main->execute("
            delete ps, psr 
            from production_schedule ps 
            join production_schedule_rows psr ON psr.id_schedule = ps.id
            where 1
            AND ps.to_arc = 1
            AND ps.created_time < NOW() - INTERVAL 3 DAY;
        ");
    }

    public static function scheduleLine($idLine) {
        return DBC::$main->withTrans(function ($idLine) {
            $idLine = intval($idLine);
            if(empty($idLine)) return;

            $line = DBC::$main->selectRow("SELECT * FROM production_lines WHERE id = $idLine");
            if(empty($line)) throw new InvalidParameter;

            $scheduler = new Scheduler();
            $lineState = $scheduler->getLineState($idLine);

            foreach ($lineState as $k => $v) {
                //EMC-47 закърпвация масата да се смята с линейна маса базирана на производствената дебелина, а не на номенклатурната
                $v['order_size_s'] = doubleval($v['order_size_s']);
                $v['size_s'] = doubleval($v['size_s']);

                if($v['order_size_s'] && $v['order_size_s'] != $v['size_s']) {
                    $size = Sizes::getSizeByID($v['id_production_size']);
                    $size['s_production'] = $v['order_size_s'];
                    $lineState[$k]['size_m'] = Sizes::calcLinearMass($size);;
                    if($lineState[$k]['size_m'] <= 0) throw new InvalidParameter();
                }
            }

            $sizeCodesWithoutThicknessAndMetalBrand = !empty($lineState) ? array_map(array('\emc\nomenclatures\sizes\Sizes', 'getSizeCodeWithoutThicknessAndMetalBrand'),
                DBC::$slave->selectAssoc("SELECT * FROM production_sizes WHERE id IN (" . implode(',', array_map(function ($v) {
                        return $v['id_production_size'];
                    }, $lineState)) . ")")
            ) : [];

            $scheduleIDsForDel = array_map(function ($row) {
                return intval($row['id']);
            }, array_filter($lineState, function ($e) {
                return empty($e['read_only']);
            }));

            self::deleteSchedules($scheduleIDsForDel);

            $delayedSizeIDsBefore = [];
            $newSizes = [];
            foreach ($lineState as $k => $v) {
                if(!empty($v['is_delayed'])) $delayedSizeIDsBefore[$v['id_order_size']] = $v['id_order_size'];
                if(empty($v['is_new_size'])) continue;
                $newSizes[] = $v;
                unset($lineState[$k]);
            }

            if(!empty($newSizes)) foreach ($lineState as $k => $row) {
                $nextRow = $lineState[$k + 1];
                if(empty($lineState[$k + 1]) || $row['id_order_size'] != $nextRow['id_order_size'] || !$k) {
                    if(!empty($nextRow['read_only'])) continue;
                    $bIsFirstRowWithGapBehind = !$k && strtotime($row['time_from']) > time();
                    if(Util::SQLDateToTimeStamp($row['time_to']) < Util::SQLDateToTimeStamp($nextRow['time_from']) || $bIsFirstRowWithGapBehind) {

                        if($bIsFirstRowWithGapBehind) {
                            $nextRow = $row;
                            $row = ['time_to' => date('Y-m-d')];
                        }

                        //imame dupka
                        //validna li e, da ne e krai na denq prosto ili pochivni dni?
                        if(Util::SQLDateToTimeStamp($nextRow['time_from'])) {
                            $testShifts = self::generateShifts($idLine, strtotime($row['time_to']), 1);
                            if(end($testShifts)['time_to'] > $nextRow['time_from']) continue; //ne validna dupkata, nqmame nalichno rabotno vreme v neq
                        }

                        //imame li podhodqsht newSize za taq dupka?
                        $gapSizes = array_filter($newSizes, function ($s, $key) use ($row, $nextRow, &$newSizes) {
                            $return = false;
                            if(!Util::SQLDateToTimeStamp($s['desired_production_start'])) $return = true;
                            else if(0
                                || Util::SQLDateToTimeStamp($s['desired_production_start']) <= Util::SQLDateToTimeStamp(substr($nextRow['time_from'], 0, 10))
                                || empty(Util::SQLDateToTimeStamp($nextRow['time_from']))
                            ) {
                                $return = true;
                            }
                            if($return) unset($newSizes[$key]);
                            return $return;
                        }, ARRAY_FILTER_USE_BOTH);

                        usort($gapSizes, function ($a, $b) {
                            $a = Util::SQLDateToTimeStamp($a['desired_production_start']);
                            $b = Util::SQLDateToTimeStamp($b['desired_production_start']);
                            if(!$a && $b) return 1;
                            if($a && !$b) return -1;
                            return $a <=> $b;
                        });

                        if(empty($gapSizes)) continue;

                        if($bIsFirstRowWithGapBehind) {
                            $lineState = array_merge($gapSizes, $lineState);
                        } else {
                            $lineState = array_merge(array_slice($lineState, 0, $k + 1, false), $gapSizes, array_slice($lineState, $k + 1, null, false));
                        }

                    }
                }
                if(empty($newSizes)) break;
            }

            if(!empty($newSizes)) $lineState = array_merge($lineState, $newSizes);

            $mass = 0;
            $realSetupTime = 0;
            $realProductionTime = 0;
            $lastTimeTo = '';
            $previousOrderSize = [];
            $orderSizesForUpdate = [];

            foreach ($lineState as $k => $row) {
                if($row['order_size_complete']) $previousOrderSize = $row;

                if(!empty($row['order_size_complete'])) continue;

                $mass += $row['produced_mass'];
                $realSetupTime += $row['real_setup_duration'];
                $realProductionTime += $row['real_production_duration'];

                if(empty($lineState[$k + 1]) || $row['id_order_size'] != $lineState[$k + 1]['id_order_size']) {
                    $remainingMass = round($row['total_mass'] - $mass, 3);

                    if($remainingMass < 0) $remainingMass = 0;

                    if(empty($remainingMass)) {
                        $mass = $realSetupTime = $realProductionTime = 0;
                        $previousOrderSize = $row;
                        continue;
                    }

                    $timeFrom = strtotime(end(array_filter($lineState, function ($s) {
                        return $s['read_only'];
                    }))['time_to']);

                    if(empty($timeFrom)) $timeFrom = time();
                    if(!empty($lastTimeTo)) $timeFrom = strtotime($lastTimeTo);

                    if($timeFrom < Util::SQLDateToTimeStamp($row['desired_production_start'])) {
                        $timeFrom = Util::SQLDateToTimeStamp($row['desired_production_start']);
                    }

                    $setupDurationInSeconds = 0;
                    $setupShiftsRows = [];
                    do {
                        if($sizeCodesWithoutThicknessAndMetalBrand[$previousOrderSize['id_production_size']] == $sizeCodesWithoutThicknessAndMetalBrand[$row['id_production_size']]) {
                            if($previousOrderSize['order_size_l'] != $row['order_size_l']) {
                                $setupDurationInSeconds += $row['setup_time_length'];
                            }
                            if($previousOrderSize['order_size_s'] != $row['order_size_s']) {
                                $setupDurationInSeconds += $row['setup_time_thickness'];
                            }
                            break;
                        }
                        $sameSizeGroup = !empty(array_intersect(array_filter(explode(',', $previousOrderSize['size_groups']), 'trim'), array_filter(explode(',', $row['size_groups']), 'trim')));
                        $setupDurationInSeconds += $sameSizeGroup ? $row['setup_time_small'] : $row['setup_time_big'];
                        break;
                    } while (false);
                    $setupDurationInSeconds *= 60;

                    $setupDurationInSeconds -= $realSetupTime > $setupDurationInSeconds ? $setupDurationInSeconds : $realSetupTime;
                    if(!empty($realProductionTime)) $setupDurationInSeconds = 0;

                    if($setupDurationInSeconds) {
                        $setupShiftsRows = array_map(function ($shift) use ($line, $row) {
                            $shift['type'] = 'planned_setup';
                            return $shift;
                        }, $scheduler->generateShifts($idLine, $timeFrom, $setupDurationInSeconds));
                        $timeFrom = strtotime(end($setupShiftsRows)['time_to']);
                    }

                    if(empty($row['speed'])) {
                        throw new InvalidParameter("Невалидна производствена скорост");
                    }

                    $productionDurationInSeconds = (($remainingMass / $row['size_m']) / $row['speed']) * 60;
                    $productionDurationInSeconds /= $line['efficiency'];

                    $productionShiftsRows = array_map(function ($shift) use ($line, $row) {
                        $shift['type'] = 'planned_production';
                        return $shift;
                    }, $scheduler->generateShifts($idLine, $timeFrom, $productionDurationInSeconds));

                    $shiftRows = array_merge($setupShiftsRows, $productionShiftsRows);

                    $schedules = [];
                    foreach ($shiftRows as $shift) {
                        $idx = $shift['date'] . '@' . $shift['work_shift_type'] . '@' . intval($shift['next_day']);
                        $schedules[$idx] = $schedules[$idx] ?? $shift;
                        $schedules[$idx]['rows'][] = $shift;
                    }

                    $verifyMass = 0;
                    foreach ($schedules as &$schedule) {
                        $schedule['work_shift_date'] = reset($schedule['rows'])['date'];
                        $schedule['time_from'] = reset($schedule['rows'])['time_from'];
                        $schedule['time_to'] = end($schedule['rows'])['time_to'];

                        $schedule['production_speed'] = $row['speed'];

                        $workDuration = array_reduce($schedule['rows'], function (&$res, $item) {
                            return $item['type'] == 'planned_production' ? $res + $item['duration'] : 0;
                        }, 0);

                        $schedule['planned_mass'] = round(($workDuration / 60) * $row['speed'] * $row['size_m'] * $line['efficiency'], 3);

                        $verifyMass += $schedule['planned_mass'];

                        $schedule['id_order_size'] = $row['id_order_size'];

                        $lastTimeTo = $schedule['time_to'];
                        DBC::$main->update('production_schedule', $schedule);
                        DBC::$main->multiInsert('production_schedule_rows', array_map(function ($s) use ($schedule) {
                            $s['id_schedule'] = $schedule['id'];
                            return $s;
                        }, $schedule['rows']));
                    }

                    $orderSizesForUpdate[$row['id_order_size']] = [
                        'id' => $row['id_order_size'],
                        'planned_production_end' => $lastTimeTo,
                        'is_delayed' => Util::SQLDateToTimeStamp($row['desired_production_end']) && empty($row['complete']) && strtotime($lastTimeTo) > strtotime($row['desired_production_end']) + ((24 * 3600) - 1)
                    ];

                    if(empty($row['produced_total_mass'])) $orderSizesForUpdate[$row['id_order_size']]['planned_production_start'] = reset($schedules)['time_from'];

                    if(round($verifyMass, 2, PHP_ROUND_HALF_DOWN) != round($remainingMass, 2, PHP_ROUND_HALF_DOWN)) {
                        throw new Exception(L("Планираното тегло не съвпада с реално оставащото тегло за производство. Очаквана маса %s, получена %s за линия id=%s.", round($remainingMass, 3), round($verifyMass, 3), $idLine));
                    }

                    $mass = 0;
                    $realSetupTime = 0;
                    $realProductionTime = 0;
                    $previousOrderSize = $row;
                }
            }

            $delayedSizeIDsAfter = array_map(function ($size) {
                return $size['id'];
            }, array_filter($orderSizesForUpdate, function ($size) use ($delayedSizeIDsBefore) {
                return $size['is_delayed'] && empty($delayedSizeIDsBefore[$size['id']]);
            }));

            if(!empty($delayedSizeIDsAfter)) Notifications::notifyDelayedProduction($delayedSizeIDsAfter);

            DBC::$main->multiInsert('production_orders_sizes', $orderSizesForUpdate);

            self::validateScheduleIntegrity($idLine);

        }, array($idLine));
    }


    private static function validateScheduleIntegrity($idLine) {
        //да валидираме, че производството на някой размер не е разбито на части, крайно недопустимо състояние
        $lineState = DBC::$main->selectColumn("select id_order_size from production_schedule where to_arc = 0 and id_line = $idLine order by id");

        $check = [];
        foreach ($lineState as $id) {
            if(end($check) == $id) continue;
            $check[] = $id;
        }

        $brokenSchedules = array_filter(array_count_values($check), function ($v) {
            return $v > 1;
        }, ARRAY_FILTER_USE_BOTH);

        if(!empty($brokenSchedules)) throw new InvalidParameter("Грешка при планиране. Опит за разбиване на части на производствена програма на размер. Моля, свържете се с поддръжка.");
    }

    public static function setOrderCancelled($idOrder) {
        DBC::$main->withTrans(function ($idOrder) {
            $idOrder = intval($idOrder);
            if(empty($idOrder)) throw new InvalidParameter;

            $order = DBC::$main->selectByID('production_orders', $idOrder);
            if($order['status'] == 'cancelled') throw new InvalidParameter('Заявката е отказана към момента. Не е възможно повторно отказване.');

            $orderSizes = DBC::$main->select("SELECT * FROM production_orders_sizes WHERE id_order = $idOrder AND to_arc = 0");

            foreach ($orderSizes as $orderSize) Scheduler::cancelOrderSizeProduction($orderSize['id']);

            DBC::$main->multiInsert('production_orders', [['id' => $idOrder, 'status' => 'cancelled', 'needs_review' => 0]]);

        }, [$idOrder]);
    }

    public static function cancelOrderSizeProduction($idOrderSize) {
        DBC::$main->withTrans(function ($idOrderSize) {
            $orderSize = DBC::$main->selectByID('production_orders_sizes', $idOrderSize);
            if(empty($orderSize)) throw new InvalidParameter();

            self::checkIfOrderSizeHasProduction($idOrderSize);

            $schedules = DBC::$main->select("
                SELECT
                    ps.id,
                    ps.id_line
                FROM production_orders_sizes pos
                JOIN production_schedule ps ON ps.id_order_size = pos.id AND ps.to_arc = 0
                WHERE 1
                AND pos.to_arc = 0 
                AND pos.complete = 0
                AND pos.id = {$orderSize['id']}
                GROUP BY ps.id
            ");

            if(!empty($schedules)) {
                Notifications::notifyChangedOrder($orderSize['id_order'], [$idOrderSize], [reset($schedules)['id_line']], time());
            }

            Scheduler::deleteSchedules(array_map(function ($s) {
                return $s['id'];
            }, $schedules));

            foreach (array_unique(array_map(function ($s) {
                return $s['id_line'];
            }, $schedules)) as $idLine) Scheduler::scheduleLine($idLine);

        }, [$idOrderSize]);
    }


    public static function confirmOrderSizeProduction($idOrderSize, $skipSchedule = false) {


        $currentSchedule = DBC::$main->selectRow("SELECT * FROM production_schedule ps WHERE id_order_size = $idOrderSize AND to_arc = 0 LIMIT 1");
        if(!empty($currentSchedule)) return $currentSchedule['id_line'];

        return DBC::$main->withTrans(function ($idOrderSize, $skipSchedule) {

            $idOrderSize = intval($idOrderSize);

            $orderSize = DBC::$main->selectByID('production_orders_sizes', $idOrderSize);
            if(empty($orderSize)) throw new InvalidParameter;
            if(!empty($orderSize['complete'])) throw new InvalidParameter("Oпит за планиране на вече произведен размер за производство.");

            $bestLine = reset(Scheduler::getScheduleForOrderSize($orderSize));
            DBC::$main->multiInsert('production_schedule', [['id_line' => $bestLine['id_line'], 'id_order_size' => $orderSize['id']]]);

            if(!$skipSchedule) Scheduler::scheduleLine($bestLine['id_line']);

            return $bestLine['id_line'];
        }, [$idOrderSize, $skipSchedule]);
    }


    public static function setOrderConfirmed($idOrder) {
        DBC::$main->withTrans(function ($idOrder) {

            Access::checkAccess(ProfileMarketingManager::class);

            $idOrder = intval($idOrder);
            if(empty($idOrder)) throw new InvalidParameter;

            $order = DBC::$main->selectByID('production_orders', $idOrder);

            if($order['status'] == 'confirmed') throw new InvalidParameter('Заявката е потвърдена към момента. Не е възможно повторно потвърждение.');
            if($order['status'] == 'cancelled') throw new InvalidParameter('Не е възможно потвърждаване на отказана заявка. Може да бъде клонирана.');

            $orderSizes = DBC::$main->select("
                SELECT
                    pos.*
                FROM production_orders po 
                JOIN production_orders_sizes pos ON pos.id_order = po.id AND pos.to_arc = 0 AND pos.complete = 0
                LEFT JOIN production_schedule ps ON ps.id_order_size = pos.id AND ps.to_arc = 0
                WHERE 1
                AND po.id = $idOrder
                GROUP BY pos.id
                ORDER BY IF(pos.desired_production_start != 0, pos.desired_production_start, '9999-12-12')
            ");

            if(empty($orderSizes)) throw new Exception("Не е възможно потвърждаване на поръчка, по която не са въведени размери.");

            $lineIDsForSchedule = [];
            foreach ($orderSizes as $orderSize) $lineIDsForSchedule[] = Scheduler::confirmOrderSizeProduction($orderSize['id'], true);
            foreach (array_unique($lineIDsForSchedule) as $idLine) Scheduler::scheduleLine($idLine);

            DBC::$main->multiInsert('production_orders', [['id' => $idOrder, 'status' => 'confirmed', 'confirmed_user' => $_SESSION['userdata']['id'], 'confirmed_time' => time()]]);

        }, array($idOrder));
    }

    public static function editWorkShift($idWorkShift, $producedMass = 0, $real_production = 0, $real_setup = 0, $isSizeComplete = false) {
        /** @noinspection PhpUnusedParameterInspection */
        return DBC::$main->withTrans(function ($idWorkShift, $producedMass = 0, $real_production = 0, $real_setup = 0, $isSizeComplete = false) {

            if(Access::hasAccessToNoneOfTheLevels([
                CanEditWorkShifts::class,
                CanEditFinishedWorkShifts::class
            ])
            ) throw new Exception("Нямате необходимите права за редакция на работна смяна.");

            if(!($real_production + $real_setup)) throw new InvalidParameter("Не е допустимо да няма въведено време.");

            $idWorkShift = intval($idWorkShift);

            $schedule = DBC::$main->selectRow("SELECT * FROM production_schedule WHERE id = $idWorkShift FOR UPDATE");
            $orderSize = DBC::$main->selectRow("SELECT * FROM production_orders_sizes WHERE id = {$schedule['id_order_size']} FOR UPDATE");

            $openWorkShift = DBC::$main->selectRow("
                SELECT
                    ps.id
                FROM production_schedule ps
                LEFT JOIN production_schedule_rows psr ON psr.id_schedule = ps.id AND psr.type IN('real_production','real_setup') and psr.to_arc = 0
                WHERE 1
                AND ps.time_from > 0
                AND ps.time_from < '{$schedule['time_from']}'
                AND ps.to_arc = 0
                AND ps.id_line = {$schedule['id_line']}
                AND psr.id IS NULL
                GROUP BY ps.id
                LIMIT 1
            ");
            if(!empty($openWorkShift)) throw new InvalidParameter(L("Налична е предходна неприключена работна смяна. Моля, приключете я."));

            if($idWorkShift < self::lastCompletedShiftIDForLine($schedule['id_line']) && !Access::hasAccess(CanEditFinishedWorkShifts::class)) {
                throw new InvalidParameter(CanEditFinishedWorkShifts::$exMessage);
            }

            DBC::$main->multiInsert('production_schedule_rows', DBC::$main->select("SELECT id, 1 AS to_arc FROM production_schedule_rows WHERE id_schedule = {$schedule['id']} AND to_arc = 0 AND type IN('real_setup', 'real_production')"));


            $validateShifts = Scheduler::generateShifts($schedule['id_line'], strtotime($schedule['time_from']), $real_production + $real_setup);
            $exceedingTime = 0;
            if(count(array_unique(array_map(function ($r) {
                    return $r['work_shift_type'] . $r['date'];
                }, $validateShifts))) > 1
            ) {
                //настъпваме друга смяна, да видим другата смяна непосредствено преди последната ли е
                $allowedDuration = 0;
                foreach ($validateShifts as $k => $v) {
                    $allowedDuration += $v['duration'];
                    $next = $validateShifts[$k + 1];
                    if($v['work_shift_type'] . $v['date'] == $next['work_shift_type'] . $next['date']) continue;
                    $exceedingTime = abs($allowedDuration - ($real_production + $real_setup));
                    if($exceedingTime <= 120) break;
                    $timeToNextShift = strtotime($next['time_from']) - strtotime($v['time_to']);
                    if($exceedingTime > $timeToNextShift) throw new InvalidParameter("Въведеното време надвиваша продължителността на текущата смяна и навлиза в следващата смяна.");
                    break;
                }
            }

            $timeRows = [];
            $durations = ['real_setup' => [$real_setup], 'real_production' => [$real_production]];
            foreach (array('real_setup', 'real_production') as $type) {
                if(empty($$type)) continue;
                if($exceedingTime && $exceedingTime < $$type) {
                    $actionTime = $$type - $exceedingTime;
                    $exceedingTime = 0;
                } else $actionTime = $$type;

                $timeRows = array_merge($timeRows, array_map(function ($s) use ($schedule, $type, $real_production, $real_setup, &$durations) {
                    $s['id_schedule'] = $schedule['id'];
                    $s['type'] = $type;
                    $s['duration'] = array_pop($durations[$type]);
                    return $s;
                }, Scheduler::generateShifts($schedule['id_line'], empty($timeRows) ? strtotime($schedule['time_from']) : strtotime(end($timeRows)['time_to']), $actionTime)));
            }

            if(!empty($timeRows)) DBC::$main->multiInsert('production_schedule_rows', $timeRows);

            $totalProducedMass = DBC::$main->selectField("SELECT SUM(produced_mass) FROM production_schedule WHERE to_arc = 0 AND id_order_size = {$orderSize['id']} AND id != $idWorkShift");

            $orderSize['complete'] = $isSizeComplete;
            $orderSize['produced_total_mass'] = $totalProducedMass + $producedMass;
            DBC::$main->update('production_orders_sizes', $orderSize);

            $schedule['produced_mass'] = $producedMass;
            $schedule['time_to'] = end($timeRows)['time_to'];
            DBC::$main->update('production_schedule', $schedule);

            if($isSizeComplete && self::isLastSizeInProductionGroup($orderSize['id'])) {
                self::validateProductionGroupLotConsume($orderSize['id']);
            }

            if(self::isFirstSizeInProductionGroup($orderSize['id'])) {
                self::validateProductionGroupLotConsume($orderSize['id'], true);
            }

            self::scheduleLine($schedule['id_line']);

            return $schedule;

        }, func_get_args());
    }

    public static function lastCompletedShiftIDForLine($idLine) {
        $idLine = intval($idLine);
        return DBC::$main->selectField("
                SELECT 
                    ps.id                  
                FROM production_schedule ps 
                JOIN production_schedule_rows psr ON psr.id_schedule = ps.id AND psr.type IN('real_production','real_setup') and psr.to_arc = 0
                WHERE 1
                AND ps.to_arc = 0
                AND ps.id_line = $idLine
                GROUP BY ps.id
                ORDER BY ps.id DESC
                LIMIT 1
            ");
    }

    public static function getScheduleForOrderSize($orderSize) {
        $result = [];
        try {
            DBC::$main->withTrans(function ($orderSize) use (&$result) {
                $size = Sizes::getSizeByID($orderSize['id_size']);
                if(empty($size)) throw new InvalidParameter;

                $possibleLines = ProductionLines::getPossibleLinesForSize($size['id']);

                if(empty($possibleLines)) throw new InvalidParameter(L("Не е намерена нито една производствена линия, която да обслужва размер %s", $size['code']));

                foreach (array_keys($possibleLines) as $idLine) {
                    DBC::$main->update('production_orders_sizes', $orderSize);
                    DBC::$main->multiInsert('production_schedule', [['id_line' => $idLine, 'id_order_size' => $orderSize['id']]]);
                    self::scheduleLine($idLine);
                }

                $result = DBC::$main->select("
                    SELECT
                        ps.id_line,
                        SUM(ps.planned_mass) as total_mass,
                        MIN(ps.time_from) as time_from,
                        MAX(ps.time_to) as time_to,
                        SUM(t.setup_duration) as setup_duration,
                        SUM(t.production_duration) as production_duration
                    FROM production_schedule ps
                    JOIN (
                        SELECT
                        psr.id_schedule,
                        SUM(IF(psr.type = 'planned_setup', psr.duration,0)) as setup_duration,
                        SUM(IF(psr.type = 'planned_production', psr.duration,0)) as production_duration
                        FROM production_schedule_rows psr
                        JOIN production_schedule p ON p.id = psr.id_schedule AND p.id_order_size = {$orderSize['id']} AND p.to_arc = 0
                        WHERE 1
                        AND psr.to_arc = 0
                        AND psr.type IN('planned_production', 'planned_setup')
                        GROUP BY psr.id_schedule
                    ) t ON t.id_schedule = ps.id
                    WHERE 1
                    AND ps.to_arc = 0
                    AND ps.id_line IN(" . implode(',', array_keys($possibleLines)) . ")
                    AND ps.id_order_size = {$orderSize['id']}
                    GROUP BY ps.id_line
                ");

                if(empty($result)) throw new Exception("Грешка при планиране.");

                usort($result, function ($a, $b) {
                    $cmp = $a['setup_duration'] <=> $b['setup_duration'];
                    if($cmp != 0) return $cmp;
                    return strtotime($a['time_to']) <=> strtotime($b['time_to']);
                });

                throw new Exception("shh silent!");
            }, [$orderSize]);

        } catch (Exception $ex) {
            if($ex->getMessage() != "shh silent!") throw $ex;
        }

        return $result;
    }


    public static function generateShifts($idLine, $startTime, $actionTime) {
        if(empty($actionTime)) return array();
        $startTime = !empty($startTime) ? $startTime : time();
        $idLine = intval($idLine);
        $sqlStartDate = date('Y-m-d', $startTime);
        $workShifts = [];
        foreach (DBC::$main->select("
            SELECT
                `type` as work_shift_type,
                `id` as id_work_shift,
                `date`,
                `from` as shift_from,
                `to` as shift_to
            FROM production_lines_work_shifts 
            WHERE 1
            AND id_line = {$idLine}
            AND to_arc = 0
            AND (`date` = '0000-00-00' OR `date` >= DATE_SUB('$sqlStartDate', INTERVAL 1 DAY))
            ORDER BY `date`, id
        ") as $shift) $workShifts[$shift['date']][$shift['work_shift_type']][] = $shift;


        foreach ($workShifts as $date => $shiftsByType) foreach ($shiftsByType as $type => $shifts) {
            foreach ($shifts as $k => $shift) if($shift['shift_from'] > $shift['shift_to']) {
                foreach ($shifts as $kk => $s) if($s['shift_from'] < '12:00:00') {
                    $workShifts[$date][$type][$kk]['next_day'] = true;
                }
                break;
            }
        }

        foreach ($workShifts as $date => $shiftsByType) uasort($workShifts[$date], function ($a, $b) {
            if(!$a['next_day'] && $b['next_day']) return -1;
            if($a['next_day'] && !$b['next_day']) return 1;
            return $a['shift_from'] <=> $b['shift_from'];
        });

        $schedule = [];
        $d = strtotime('-1 day', $startTime);
        $endDay = strtotime("+3 month", $d);

        while ($d < $endDay) {
            $day = date('Y-m-d', $d);
            $shifts = $workShifts[$day] ? $workShifts[$day] : $workShifts['0000-00-00'];
            $isException = !!$workShifts[$day];
            $weekDay = date('N', $d);
            $nextDay = false;
            foreach ($shifts as $type => $intervals) {
                if($type == 'holiday') continue;
                if(!$isException && ($weekDay == 6 || $weekDay == 7)) {
                    continue;
                }
                foreach ($intervals as $interval) {
                    $interval['date'] = ($nextDay ? date('Y-m-d', strtotime('+1 day', $d)) : $day);
                    if($interval['shift_to'] < $interval['shift_from']) {
                        $nextDay = true;
                        $interval['shift_from'] = $interval['date'] . ' ' . $interval['shift_from'];
                        $interval['from'] = strtotime($interval['shift_from']);
                        $interval['shift_to'] = date('Y-m-d', strtotime('+1 day', $d)) . ' ' . $interval['shift_to'];
                        $interval['to'] = strtotime($interval['shift_to']);
                        $schedule[] = $interval;
                        continue;
                    }
                    $interval['shift_from'] = $interval['date'] . ' ' . $interval['shift_from'];
                    $interval['shift_to'] = $interval['date'] . ' ' . $interval['shift_to'];
                    $interval['from'] = strtotime($interval['shift_from']);
                    $interval['to'] = strtotime($interval['shift_to']);
                    $schedule[] = $interval;
                }
                $nextDay = false;
            }
            $d = strtotime("+1 day", $d);
        }

        $productionShifts = array();
        $remainingActionTime = $actionTime;

        foreach ($schedule as $shift) {
            if($shift['to'] <= $startTime) continue;

            $start = $shift['from'] <= $startTime ? $startTime : $shift['from'];
            if($shift['from'] <= $startTime) $start = $startTime;
            $end = $shift['to'];

            $remainingActionTime -= $end - $start;
            if($remainingActionTime <= 0) $end += $remainingActionTime;
            $shift['time_from'] = date('Y-m-d H:i:s', $start);
            $shift['time_to'] = date('Y-m-d H:i:s', $end);
            $shift['duration'] = $end - $start;
            $shift['id_line'] = $idLine;
            $productionShifts[] = $shift;
            if($remainingActionTime <= 0) break;
        }

        return $productionShifts;
    }

    public static function checkIfOrderSizeHasProduction($idOrderSize, $exceptionMsg = null) {
        if(self::orderSizeHasProduction($idOrderSize)) {
            $idOrderSize = intval($idOrderSize);
            $orderNum = DBC::$main->selectField("
                SELECT
                    CONCAT(psz.code ,'/', ps.num)
                FROM production_orders_sizes pos
                JOIN production_sizes psz ON psz.id = pos.id_size
                JOIN production_orders ps ON ps.id = pos.id_order
                WHERE pos.id = $idOrderSize;
            ");
            throw new InvalidParameter($exceptionMsg ?? "Действието не е приложимо за размер $orderNum, по който има отразено производство.");
        }
    }

    public static function orderSizeHasProduction($idOrderSize) {
        return !empty(DBC::$main->selectField("
            SELECT 
                sum(psr.duration) + pos.complete
            FROM production_schedule ps
            JOIN production_orders_sizes pos ON pos.id = ps.id_order_size
            JOIN production_schedule_rows psr ON psr.id_schedule = ps.id AND psr.type IN('real_production','real_setup') and psr.to_arc = 0
            WHERE 1
            AND ps.to_arc = 0
            AND ps.id_order_size = $idOrderSize
            GROUP BY ps.id_order_size
        "));
    }

    public static function getLineIDsUsingMetal($idMetal) {
        $idMetal = intval($idMetal);
        return DBC::$main->selectColumn("
            SELECT
              pl.id
            FROM metals m
            JOIN production_sizes ps ON ps.id_metal = m.id AND ps.to_arc = 0
            JOIN production_lines_sizes pls ON pls.id_size = ps.id AND pls.to_arc = 0
            JOIN production_orders_sizes pos ON pos.id_size = ps.id AND pos.to_arc = 0 AND pos.complete = 0
            JOIN production_schedule psc ON psc.id_order_size = pos.id AND psc.to_arc = 0
            JOIN production_lines pl ON pl.id = psc.id_line AND pl.status = 'active' AND pl.to_arc = 0
            WHERE 1
            AND m.id = {$idMetal}
            GROUP BY pl.id
        ");
    }

    public static function getLineIDsUsingProductionSize($idSize) {
        $idSize = intval($idSize);
        return DBC::$main->selectColumn("
            SELECT
              pl.id
            FROM production_sizes ps
            JOIN production_lines_sizes pls ON pls.id_size = ps.id AND pls.to_arc = 0
            JOIN production_orders_sizes pos ON pos.id_size = ps.id AND pos.to_arc = 0 AND pos.complete = 0
            JOIN production_schedule psc ON psc.id_order_size = pos.id AND psc.to_arc = 0
            JOIN production_lines pl ON pl.id = psc.id_line AND pl.status = 'active' AND pl.to_arc = 0
            WHERE 1
            AND ps.id = {$idSize}
            GROUP BY ps.id
        ");
    }

    public static function transformLineStateToRows($lineState) {
        $orderSizes = [];

        foreach ($lineState as $schedule) {
            $last = end($orderSizes);
            $k = key($orderSizes);

            if($schedule['id_order_size'] != $last['id_order_size']) {
                $new = $schedule;
                $orderSizes[] = $new;
                $last = end($orderSizes);
                $k = key($orderSizes);
            }

            $orderSizes[$k]['read_only'] = $last['read_only'] || $schedule['read_only'];
            $orderSizes[$k]['time_from'] = min($last['time_from'], $schedule['time_from']);
            $orderSizes[$k]['time_to'] = max($last['time_to'], $schedule['time_to']);
        }

        $orderSizes = array_map(function ($orderSize) {
            foreach (['time_from', 'time_to'] as $k) $orderSize[$k] = Util::SQLDateToTimeStamp($orderSize[$k]) ? date('d.m.Y H:i:s', Util::SQLDateToTimeStamp($orderSize[$k])) : '';
            foreach (['desired_production_start', 'desired_production_end'] as $k) $orderSize[$k] = Util::SQLDateToTimeStamp($orderSize[$k]) ? date('d.m.Y', Util::SQLDateToTimeStamp($orderSize[$k])) : '';
            return $orderSize;
        }, $orderSizes);

        return $orderSizes;
    }


    public static function getLineByOrderSize($idOrderSize) {
        $idOrderSize = intval($idOrderSize);
        return DBC::$main->selectRow("
            SELECT
            pl.*
            FROM production_schedule ps
            JOIN production_lines pl ON pl.id = ps.id_line 
            WHERE 1
            AND ps.to_arc = 0 
            AND ps.id_order_size = $idOrderSize
            LIMIT 1
        ");
    }


    public static function getSizeGroupsForLine($idLine) {
        $idLine = intval($idLine);
        if(empty($idLine)) throw new InvalidParameter();

        $state = DBC::$main->select("
            SELECT
                psz.id as id_order_size,
                psz.id_size,
                psz.s as s,
                pz.s as s_nomenclature,
                pz.code as size_code,
                sum(ps.planned_mass) as planned_mass,
                sum(ps.produced_mass) as produced_mass,
                psz.total_mass,
                psz.complete
            FROM production_schedule ps 
            JOIN production_orders_sizes psz ON psz.id = ps.id_order_size AND psz.to_arc = 0
            JOIN production_sizes pz ON pz.id = psz.id_size
            WHERE 1
            AND ps.to_arc = 0
            AND ps.id_line = $idLine
            GROUP BY ps.id_order_size
            ORDER BY ps.time_to ASC
        ");

        $sizesByGroups = [];

        if(empty($state)) return ['sizes' => [], 'groups' => []];

        $sizeCodesWithoutThickness = array_map(array('\emc\nomenclatures\sizes\Sizes', 'getSizeCodeWithoutThickness'), DBC::$main->selectAssoc("SELECT s.id as __key, s.* FROM production_sizes s WHERE s.id IN (" . implode(',', array_map(function ($r) {
                return intval($r['id_size']);
            }, $state)) . ") GROUP BY s.id", 1));

        $cmpKey = function ($row) use ($sizeCodesWithoutThickness) {
            return implode(';', [$sizeCodesWithoutThickness[$row['id_size']], !empty($row['s']) ? $row['s'] : $row['s_nomenclature']]);
        };

        $groupID = 0;
        foreach ($state as $k => $row) {
            $prev = $state[$k - 1] ?? $row;
            if($cmpKey($prev) != $cmpKey($row)) $groupID++;
            $state[$k]['group'] = $groupID;
            $sizesByGroups[$groupID][] = &$state[$k];
        }

        return ['sizes' => $state, 'groups' => $sizesByGroups];
    }


    public static function getProductionGroupByIDSize($idOrderSize) {
        $res = self::getSizeGroupsForLine(self::getLineByOrderSize($idOrderSize)['id']);

        $size = reset(array_filter($res['sizes'], function ($s) use ($idOrderSize) {
            return $s['id_order_size'] == $idOrderSize;
        }));

        return $res['groups'][$size['group']];
    }

    public static function isFirstSizeInProductionGroup($idOrderSize) {
        $sizeGroup = self::getProductionGroupByIDSize($idOrderSize);
        return reset($sizeGroup)['id_order_size'] == $idOrderSize;
    }

    public static function isLastSizeInProductionGroup($idOrderSize) {
        $sizeGroup = self::getProductionGroupByIDSize($idOrderSize);
        return end($sizeGroup)['id_order_size'] == $idOrderSize;
    }

    public static function validateProductionGroupLotConsume($idOrderSize, $previusGroup = false) {

        $sizeGroup = self::getProductionGroupByIDSize($idOrderSize);

        if($previusGroup) {
            $groups = self::getSizeGroupsForLine(self::getLineByOrderSize($idOrderSize)['id']);
            $sizeGroup = $groups['groups'][reset($sizeGroup)['group'] - 1];
            if(empty($sizeGroup)) return;
        }

        $groupProducedMass = array_reduce($sizeGroup, function ($sum, $size) {
            $sum += $size['produced_mass'];
            return $sum;
        });

        $orderSizeIds = array_map(function ($s) {
            return intval($s['id_order_size']);
        }, $sizeGroup);

        $groupLotMass = DBC::$main->selectField("
            SELECT 
                sum(t.mass)
            FROM (
                SELECT 
                    plc.mass
                FROM production_lots_consume plc
                JOIN production_lots_consume_order_sizes plcos ON plcos.id_lot_consume = plc.id AND plcos.to_arc = 0 AND plcos.id_order_size IN(" . implode(',', $orderSizeIds) . ")
                GROUP BY plc.id
            ) t
        ");

        //Количеството не следва да е по малко от общото произведено количество в серията.
        if(round($groupLotMass, 2) < round($groupProducedMass, 2)) {
            $prevMsg = $previusGroup ? 'в предходната серия' : 'в серията';
            throw new InvalidParameter(L("Отразеното количество %.2f кг отразено по ЛОТ %s е по-малко от реално произведеното количество от %.2f кг.", $groupLotMass, $prevMsg, $groupProducedMass));
        }
    }

    public static function getProductionWeekType($firstShiftTime, $time = null) {
        $time = $time ?? time();
        if(!is_numeric($firstShiftTime)) $firstShiftTime = strtotime($firstShiftTime);
        $productionWeek = date('W', $firstShiftTime) + ((date('Y', $firstShiftTime) - date('Y')) * 52);
        $nowWeek = date('W', $time);
        return max($productionWeek - $nowWeek, 0);
    }

}