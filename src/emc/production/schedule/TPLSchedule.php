<?php
namespace emc\production\schedule;

use main\db\DBC;
use main\grid\TPLGridPanel;

class TPLSchedule extends TPLGridPanel {

    public function __construct($sPrefix, $aInitParams = array()) {

        $productionLines = array_map(function ($line) {
            return [
                'name' => $line['id'],
                'text' => $line['code'],
                'iconCls' => 'fa fa ' . ($line['status'] == 'active' ? 'fa-check-circle' : 'fa-ban')
            ];
        }, DBC::$slave->select("SELECT * FROM production_lines pl WHERE pl.to_arc = 0 ORDER BY pl.code"));

        $this->{'bNoDefaultTopBarItems'} = true;

        $this->{'aConfig'} = array(
            'autoVResize' => true,
            'disablePaging' => true,
            'topBarItems' => array(
                array('div', array('class' => 'top-bar-title', 'align' => 'left', 'text' => L('Производствени програми'))),
                array('br'),

                array('label', array('class' => 'icon fa fa-level-down  fa-flip-horizontal')),
                array('menubutton', array('style' => '', 'name' => 'operations_menu_btn'), array(
                    array(
                        'name' => 'change_speed',
                        'text' => L("Нова скорост"),
                        'iconCls' => 'fa fa fa-tachometer'
                    ),
                    array(
                        'name' => 'reschedule',
                        'text' => L("Препланиране"),
                        'iconCls' => 'fa fa-tasks'
                    ),
                )),
                array('menubutton', array('name' => 'id_line', 'style' => 'padding-left:10px;'), $productionLines),
                array('timemenubutton', array('name' => 'time', 'style' => 'padding-left:10px;', 'align' => 'left')),
            ),
            'bottomBarItems' => array(
                array('div', array('align' => 'left', 'class' => 'line_delay')),
                array('button', array('align' => 'right', 'iconCls' => 'icon fa fa-file-excel-o', 'text' => L("Производствена карта"), 'class' => 'export-production-schedule')),
                array('button', array('align' => 'right', 'iconCls' => 'icon fa fa-calculator', 'text' => L("Нужен метал"), 'class' => 'calc-required-metal')),
            )
        );

        $this->{'aConfig'}['selectionMode'] = 'checkbox';
        $this->{'aConfig'}['hideRowIndexes'] = true;

        parent::__construct($sPrefix, $aInitParams);
    }

    public function printStyles($sPrefix) { ?>

        <!--suppress CssUnusedSymbol -->
        <style type="text/css">

            #<?=$sPrefix;?> td.multiRows {
                vertical-align: top !important;
            }

            #<?=$sPrefix;?> li.subRow {
                list-style: none;
                line-height:24px;
                height: 24px;
            }

            #<?=$sPrefix;?> li.subRow > span {
                padding-right: 3px !important;
            }

            #<?=$sPrefix;?> li:not(:last-child).subRow {
                border-bottom: 1px solid rgba(0, 0, 0, 0.1);
            }

            #<?=$sPrefix;?> li.shiftComplete {
                background-color: #DCEDC8;
            }

            #<?=$sPrefix;?> .orderSizeDelayed {
                background-color: #F8BBD0;
            }

            #<?=$sPrefix;?> .orderCancelled {
                opacity: 0.5;
                font-style: italic;
            }

            #<?=$sPrefix;?> .orderUnread {
                font-style: italic;
                font-weight: bold;
            }

            #<?=$sPrefix;?> .productionHole {
                border-top: 3px solid #ffed08;
            }

        </style>
        <?php
    }


    public function getJSFunction() {
        return <<<'JS'
        (function(container, prefix, aBaseParams) {
            var grid = container.grid;

            container.fields['id_line'].addListener('change',function(){
                grid.loadData();
            });
            
            var isLineManager = false;
            container.addListener('afterRequest', function(result){
                isLineManager = result.isLineManager;
            });
            
            $(container).find('.export-production-schedule').click(function() {
                var request = jQuery.extend({}, container.baseParams);
                request.fields = container.getFieldValues();
                request.columns = grid.getColumnsForJSON();
                framework.doExport('emc/production/schedule/Export.xls', 'Excel5', request);
            });

            grid.itemRenderers['size'] = function(columnCfg, rowData, clickFn) {
                var el = framework.grid.itemRenderers.string(columnCfg,rowData, clickFn);
                
                if(rowData['is_size_delayed']) {
                   jQuery(el).css({'font-weight':'bold','color':'#E91E63'});
                }
                return el; 
            };
            
            grid.clickListeners['is_read'] = function(rowNum, fieldElement, htmlElement) {
                container.request('markRowAsRead', { id_order_size: container.grid.getCurrentData()[rowNum]['id_order_size'] });
            };

            grid.itemRenderers['total_mass'] = function(columnCfg, rowData, clickFn) {
                var div = $("<div></div>");
                var ul = $("<ul></ul>");

                var li = $("<li></li>").text(rowData['total_mass']).addClass('subRow');
                ul.append(li);
                var li2 = $("<li></li>").text('('+rowData['mass_in_stock']+')').addClass('subRow');
                li2.addClass('clickable');
                li2.click(function(){
                    framework.createWindow({
                        tplName: 'emc/production/schedule/EditMassInStock',
                        baseParams: {
                            row: rowData,
                            getParentGrid: function(){ return grid; }
                        }
                    });
                });
                ul.append(li2);

                div.append(ul);

                return div.get(0);
            };

            grid.itemRenderers['order_info'] = function(columnCfg, rowData, clickFn) {
                var div = $("<div></div>");
                div.attr({style:"padding:0"});
                
                var divOrderNum = $("<div></div>");
                divOrderNum.addClass("clickable");
                divOrderNum.text(rowData['order_num'] + '/' + Util.formatDate(Util.mysqlDateTimeToJS(rowData['order_time']), 'D.M.Y'));
                div.append(divOrderNum);
                
                var divClientName = $("<div></div>");
                divClientName.text(rowData['client_name']);
                div.append(divClientName);
                
                var divTimes = $("<div></div>");
                
                if(rowData['desired_production_start'] || rowData['desired_production_end']) {
                    var desiredStart = Util.mysqlDateTimeToJS(rowData['desired_production_start']+' 00:00:00');
                    var desiredEnd = Util.mysqlDateTimeToJS(rowData['desired_production_end']+' 00:00:00');
                    
                    if(desiredStart || desiredEnd) {
                        divTimes.text(Util.formatDate(desiredStart, 'D.M.Y') + ' - ' + Util.formatDate(desiredEnd,  'D.M.Y'));
                    } else divTimes.html('&nbsp;');
                }
                div.append(divTimes);
                
                return div.get(0);
            };
      
      
            var editWorkShift = function(schedule){
                framework.createWindow({
                    tplName: 'emc/production/schedule/WorkShiftEdit',
                    baseParams: {
                        parentGrid: function(){return grid;},
                        id_schedule: schedule['id_schedule']
                    }
                });
            };
      
            grid.itemRenderers['work_shifts'] = function(columnCfg, rowData, clickFn) {
                var div = $("<div></div>");
                var ul = $("<ul></ul>");
                var workShifts = rowData['shifts'];
               
                if(workShifts) $.each(workShifts, function(k, v){
                    var li = $("<li></li>");
                    
                    if(!empty(v['is_editable'])) {
                        li.addClass('clickable');
                        li.click(function(){editWorkShift(v);});
                    }
                    
                    if(!empty(v['is_shift_complete'])) li.addClass('shiftComplete');
                    
                    li.addClass('subRow');
                    li.css('text-align','center');
                    li.text(v['work_shift_label']);
                    ul.append(li);                        
                });
               
                div.append(ul);
            
                return div.get(0);
            };
            
            
            var subRowRenderer = function(content, columnCfg, rowData, clickFn) {
                var div = $("<div></div>");
                var ul = $("<ul></ul>");
               
                $.each(content, function(k, v){
                    var li = $("<li></li>");
                    li.addClass('subRow');
                    if(v['_liClass']) li.addClass(v['_liClass']);
                    li.append(v);
                    ul.append(li);                        
                });
               
                div.append(ul);
                return div.get(0);
            };
            
            
            var processTimesRenderer = function(process, columnCfg, rowData, clickFn) {
                var workShifts = rowData['shifts'];
                var content = [];
              
                var planned = 'planned_'+process;
                var real = 'real_'+process;
              
                if(workShifts) {
                    $.each(workShifts, function(k, v){
                        if(empty(v[planned+'_from']) && empty(v[planned+'_to']) && empty(v[real+'_from']) && empty(v[real+'_to'])){
                            content.push('');
                            return;
                        }

                        var displayTime = !empty(v['real_setup_duration'] + v['real_production_duration']) ? real : planned;
                        var timeFrom = Util.formatDate(Util.mysqlDateTimeToJS(v[displayTime+'_from']), 'H:i') || "__:__";
                        var timeTo = Util.formatDate(Util.mysqlDateTimeToJS(v[displayTime+'_to']), 'H:i') || '__:__';
                        var span = $("<span></span>");
                        if(displayTime == real) span['_liClass'] = 'shiftComplete';
                        span.text(timeFrom + ' - ' + timeTo);
                        
                        
                        var from = Util.mysqlDateTimeToJS(v[planned+'_from']) || new Date(0);
                        var to = Util.mysqlDateTimeToJS(v[planned+'_to']) || new Date(0);
                        var totalDuration = intval((to.getTime()-from.getTime())/1000);
                        var tooltip = 'Планирано време за действие: '+Util.jsTimeStampDiffToString(v[planned+'_duration']*1000)+' h<br/>Планирано време за почивка: ' + Util.jsTimeStampDiffToString((totalDuration-v[planned+'_duration'])*1000) + ' h';
                        
                        if(!empty(v['real_setup_duration'] + v['real_production_duration'])) {
                            tooltip += '<hr/>Отразено време на действие: '+Util.jsTimeStampDiffToString(v[real+'_duration']*1000)+' h';
                        }
                        span.attr('basictip', tooltip);
                        
                        content.push(span);
                    });
                    return subRowRenderer(content,columnCfg,rowData,clickFn);
                } else {
                    var count = rowData[planned+'_count'];
                    var duration = rowData[planned+'_duration'];
                    return $("<span></span>").html(count+' / '+Util.jsTimeStampDiffToString(duration*1000) + ' h').get(0);
                }
            };
            
            grid.itemRenderers['setup_times'] = function(columnCfg, rowData, clickFn) {
                return processTimesRenderer('setup', columnCfg, rowData, clickFn);
            };
            grid.itemRenderers['production_times'] = function(columnCfg, rowData, clickFn) {
                return processTimesRenderer('production', columnCfg, rowData, clickFn);
            };
            
            grid.itemRenderers['production_speed'] = function(columnCfg, rowData, clickFn) {
                var workShifts = rowData['shifts'];
                var content = [];
              
                if(workShifts) {
                    $.each(workShifts, function(k, v){
                        content.push(empty(v['production_speed']) ?  "&nbsp;" : v['production_speed']);
                    })
                }
                return subRowRenderer(content,columnCfg,rowData,clickFn);
            };
            
            var massRenderer = function(key, columnCfg, rowData, clickFn) {
                var workShifts = rowData['shifts'];
                if(workShifts) {
                    var content = [];
                    $.each(workShifts, function(k, v){
                        content.push(Math.round(v[key]));
                    });
                    return subRowRenderer(content,columnCfg,rowData,clickFn);
                } else {
                    return $("<span></span>").text(Math.round(rowData[key])).get(0);
                }
            };
            
            grid.itemRenderers['planned_mass'] = function(columnCfg, rowData, clickFn) {
               return massRenderer('planned_mass', columnCfg,rowData,clickFn);
            };
            
            grid.itemRenderers['produced_mass'] = function(columnCfg, rowData, clickFn) {
                return massRenderer('produced_mass', columnCfg,rowData,clickFn);
            };
            
            
            var delayTimesRenderer = function(key, columnCfg, rowData, clickFn) {
                var workShifts = rowData['shifts'];
                if(workShifts) {
                    var content = [];
                    $.each(workShifts, function(k, v){
                        content.push(!empty(v['is_shift_complete']) ? (Util.jsTimeStampDiffToString(v[key]*1000,true) + ' h') : '');
                    });
                    return subRowRenderer(content,columnCfg,rowData,clickFn);
                } else {
                    return $("<span></span>").text(Util.jsTimeStampDiffToString(rowData[key]*1000,true) + ' h').get(0);
                }
            };
            
            
            grid.itemRenderers['production_delay'] = function(columnCfg, rowData, clickFn) {
                return delayTimesRenderer('production_delay', columnCfg,rowData,clickFn);
            };
            
            grid.itemRenderers['setup_delay'] = function(columnCfg, rowData, clickFn) {
                return delayTimesRenderer('setup_delay', columnCfg, rowData,clickFn);
            };
            
            grid.clickListeners['order_info'] = function(rowNum, fieldElement, htmlElement) {
                framework.createWindow({
                    tplName: 'emc/production/orders/Order',
                    baseParams: {
                        onOrderChange: function(){
                            grid.loadData();
                        },
                        row: {
                            id:grid.getCurrentData()[rowNum]['id_order'],
                        },
                    }
                });
            };
      
            grid.clickListeners['size'] = function(rowNum, fieldElement, htmlElement) {
                framework.createWindow({
                    tplName: 'emc/production/orders/sizes/Size',
                    baseParams: {
                        id: container.grid.getCurrentData()[rowNum]['id_order_size'],
                        id_order: container.grid.getCurrentData()[rowNum]['id_order'],
                        onSizeChange: function(){ container.grid.loadData();}
                    }
                });
            };
            
            $(container).find('.calc-required-metal').click(function(){
                framework.createWindow({
                    tplName: 'emc/production/schedule/RequiredMetalQuantity',
                    baseParams: {
                        fields:container.getFieldValues(),
                    }
                });
            });
            
            container.fields['operations_menu_btn'].addListener('click',function(operation){
                var rows = container.grid.getSelectedRows();
                
                if(!isLineManager) {
                    framework.alert("Нямате достъп до тази функционалност, тъй като не сте мениджър на избраната линия.", 'error');
                    return;
                }
                
                if(rows.length > 1) {
                    framework.alert("Моля, изберете само един ред");
                    return;
                }
                switch(operation) {
                    case 'reschedule':
                        var rows = container.grid.getSelectedRows();
                        if(rows.length > 1) {
                            framework.alert("Моля, изберете само един ред");
                            return;
                        }
                        if(rows[0]) {
                            var readOnly = 0;
                            $.each(rows[0]['shifts'], function(k,v){if(readOnly) return; readOnly += intval(v['is_shift_complete']);});
                            if(readOnly || rows[0]['complete'] > 0) {
                                framework.alert("Операцията не е приложима за приключени или в производство размери.");
                                return;
                            }
                        }
                        
                        framework.createWindow({
                            tplName: 'emc/production/schedule/PlanningTool',
                            baseParams: {
                                fields: container.getFieldValues(),
                                rows: container.grid.getSelectedRows(),
                                getParentGrid: function(){return container.grid;}
                            }
                        });
                    break;
                    case 'change_speed':
                        if(!rows[0]) {
                            framework.alert("Моля, изберете ред.");
                            return;
                        }
                        if(rows[0] && rows[0]['complete'] > 0) {
                            framework.alert("Операцията не е приложима за приключени размери.");
                            return;
                        }
                        
                        var prompt = framework.prompt(
                            'Промяна на производствена скорост',
                            'Моля, въведете скорост за размер '+rows[0]['size'] + ' от поръчка ' +rows[0]['order_num']+':',
                            function(r){
                                if(!r) return;
                                container.request('changeProductionSpeed', {id_order_size:rows[0]['id_order_size'], speed:r});       
                            }
                        )
                        $(prompt).find('input').attr('keyrestriction','integer');
                        
                    break;
                }
            });
 
        });
JS;
    }

}