<?php
namespace emc\production\orders;

use emc\access\profiles\ProfileMarketingEmployee;
use emc\access\profiles\ProfileMarketingManager;
use emc\production\schedule\Scheduler;
use main\Access;
use main\db\DBC;
use main\Exception;
use main\form\FormPanelHandler;
use main\InvalidParameter;
use main\MultilineTextPromptException;

/**
 * Class OrderTabInfo
 * @package emc\production\orders
 * @accessLevelDescription Достъп до форма Информация в Редакция на Заявка
 */
class OrderTabInfo extends FormPanelHandler {
    public function init($aRequest = array(), $aInitParams = array()) {
        $aRequest['order'] = $aInitParams['row'];

        $aRequiredFields = $this->getRequiredFields();

        $this->oResponse->{'aRequiredFields'} = array_map(function ($aFields) {
            return array_keys($aFields);
        }, $aRequiredFields);

        return $this->loadData($aRequest);
    }

    public static function getOrderByID($id) {
        return DBC::$main->selectRow("
            SELECT
            po.*,
            (po.needs_review + sum(pos.needs_review)) > 0 as needs_review,
            null as update_note
            from production_orders po 
            LEFT JOIN production_orders_sizes pos ON pos.id_order = po.id AND pos.to_arc = 0
            WHERE po.id = $id
            group by po.id
        ");
    }

    public function loadData($aRequest) {
        $nIdOrder = intval($aRequest['order']['id']);

        if(!empty($nIdOrder)) {
            $aData = self::getOrderByID($nIdOrder);
            $this->oResponse->{'order'} = $aData;

            foreach ($aData as $key => $val) {
                if($key == 'created_time') $val = date("d.m.Y", strtotime($val));
                elseif($key == 'status') {
                    switch ($val) {
                        case 'waiting':
                            $val = L('Изчаква');
                            break;
                        case 'confirmed':
                            $val = L('Потвърдена');
                            break;
                        case 'production':
                            $val = L('В производство');
                            break;
                        case 'finished':
                            $val = L('Приключена');
                            break;
                        case 'cancelled':
                            $val = L('Отказана');
                            break;
                    }
                }
                $this->setField($key, $val);
            }

            $sConfirmedTime = $aData['confirmed_time'] == '0000-00-00 00:00:00' ? '' : date('d.m.Y H:i:s', strtotime($aData['confirmed_time']));
            $nConfirmedUser = intval($aData['confirmed_user']);
            $sConfirmed = trim(DBC::$slave->selectField("SELECT CONCAT_WS(' ', fname, mname, lname) FROM personnel WHERE id = {$nConfirmedUser}") . ' ' . $sConfirmedTime);
            if(!empty($sConfirmed)) $sConfirmed = 'Утвърдил: ' . $sConfirmed;

            $this->setField('confirmed', $sConfirmed);

            $sReviewed = '';
            if(!empty($aData['reviewed_user'])) {
                $sReviewed = "Корекциите са прегледани и потвърдени от: " . DBC::$slave->selectField("SELECT CONCAT_WS(' ', fname, mname, lname) FROM personnel WHERE id = {$aData['reviewed_user']}") . ' ' . date('d.m.Y H:i:s', strtotime($aData['reviewed_time']));
            } else if(!empty($aData['needs_review'])) {

            }

            $this->setField('reviewed', $sReviewed);

            $this->setLastEditInfo($aData);
        } else {
            // ако е клонирана
            if(!empty($aRequest['order']['parent_id'])) {
                $aRequest['order']['status'] = 'waiting';
                $aSkipFields = array_map('trim', explode(',', 'confirmed, client_order_num, num, created_time, status, last_edited_info, updated_count, updated_note'));
                foreach ($aRequest['order'] as $key => $val) {
                    // прескачане на ненужни полета
                    if(!in_array($key, $aSkipFields)) $this->setField($key, $val);
                }
            }
            $this->oResponse->{'order'} = $aRequest['order'];
        }
        return $this->oResponse;
    }

    public function save($aRequest) {
        return DBC::$main->withTrans(function ($aRequest) {
            Access::checkAccess(ProfileMarketingEmployee::class);

            $aData = $aRequest['fields'];

            $aData['id'] = intval($aRequest['order']['id']);

            $aData['name'] = $aData['name']['input_value'];
            $aData['ein'] = $aData['ein']['input_value'];
            $aData['delivery_deadline'] = trim($aData['delivery_deadline']);
            $aData['method_of_acceptance'] = trim($aData['method_of_acceptance']);
            $aData['letter_info'] = trim($aData['letter_info']);

            if($aData['region'] == 'BG') $aData['country'] = L('България');

            $aRequiredFields = $this->getRequiredFields();
            foreach ($aRequiredFields[$aData['region']] as $key => $val) {
                if(empty($aData[$key])) throw new Exception(L('Моля, въведете ' . $val));
            }

            if(empty($aData['id'])) {
                $aData['status'] = 'waiting';
                $aData['num'] = Order::getNextNum($aData['region'] == 'BG' ? 'BG' : 'I');
            } else {
                if(Orders::getOrderStatus($aData['id']) == 'finished') Access::checkAccess('/');
                unset($aData['status']);
            }

            $order = self::getOrderByID($aData['id']);
            if($order['status'] == 'confirmed' && empty($aRequest['fields']['update_note'])) throw new MultilineTextPromptException('Моля, посочетете причина за корекцията', 'update_note');

            DBC::$main->update('production_orders', $aData);

            if(!empty($aRequest['order']['parent_id'])) {
                $nIdOldOrder = intval($aRequest['order']['parent_id']);
                DBC::$main->multiInsert('production_orders_sizes', DBC::$main->select("
                    SELECT 
                        pos.*, 
                        null as id, 
                        {$aData['id']} as id_order, 
                        null as desired_production_start,
                        null as desired_production_end,
                        null as created_time,
                        null as created_user,
                        0 as is_read,
                        0 as complete,
                        0 as produced_total_mass
                    FROM production_orders_sizes pos 
                    WHERE pos.id_order = $nIdOldOrder AND pos.to_arc = 0
                "));
            }
            $aRequest['order'] = $aData;

            return $this->loadData($aRequest);
        }, array($aRequest));
    }

    private function setOrderState($aRequest, $state) {
        return DBC::$main->withTrans(function ($aRequest, $state) {
            $nIdOrder = intval($aRequest['order']['id']);
            if(empty($nIdOrder)) throw new Exception('Моля, запишете заявката преди да промените статуса.');

            Access::checkAccess(ProfileMarketingManager::class);

            $order = self::getOrderByID($nIdOrder);

            if($order['needs_review']) {
                $order['needs_review'] = 0;
                $order['reviewed_time'] = time();
                $order['reviewed_user'] = $_SESSION['userdata']['id'];
                DBC::$main->execute("update production_orders_sizes set needs_review = 0 WHERE id_order = {$order['id']}");
                DBC::$main->multiInsert('production_orders', [$order]);
            } else {
                switch ($state) {
                    case 'confirmed':
                        Scheduler::setOrderConfirmed($nIdOrder);
                        break;
                    case 'cancelled':
                        Scheduler::setOrderCancelled($nIdOrder);
                        break;
                    default:
                        throw new InvalidParameter;
                }
            }

            $aRequest['order']['id'] = $nIdOrder;
            return $this->loadData($aRequest);
        }, [$aRequest, $state]);
    }

    public function confirm($aRequest) {
        return $this->setOrderState($aRequest, 'confirmed');
    }

    public function cancel($aRequest) {
        $order = self::getOrderByID(intval($aRequest['order']['id']));
        if($order['status'] == 'confirmed' && empty($aRequest['fields']['update_note'])) throw new MultilineTextPromptException('Моля, посочетете причина за корекцията', 'update_note');
        return $this->setOrderState($aRequest, 'cancelled');
    }

    private function getRequiredFields() {
        return [
            'BG' => [
                'name' => L('име'),
                'address' => L('адрес'),
                'region' => L('регион'),
                'city' => L('град'),
                'country' => L('държава'),
                'ein' => L('булстат'),
                'mol' => L('МОЛ'),
                'delivery_deadline' => L('Срок на доставка'),
            ],
            'EU' => [
                'name' => L('име'),
                'address' => L('адрес'),
                'region' => L('регион'),
                'city' => L('град'),
                'country' => L('държава'),
                'delivery_deadline' => L('Срок на доставка'),
            ],
            'NON-EU' => [
                'name' => L('име'),
                'address' => L('адрес'),
                'region' => L('регион'),
                'city' => L('град'),
                'country' => L('държава'),
                'delivery_deadline' => L('Срок на доставка'),
            ]
        ];
    }

}