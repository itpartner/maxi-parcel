<?php
namespace emc\production\orders;

use emc\production\orders\sizes\Size;
use main\db\DBC;
use main\grid\GridPanelHandler;
use main\InvalidParameter;

/**
 * Class OrderTabSizes
 * @package emc\production\orders
 * @accessLevelDescription Достъп до форма Размери в Редакция на Заявка
 */
class OrderTabSizes extends GridPanelHandler {
    public function __construct() {
        $this->oBase = DBC::$main;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'code',
                'width' => 125,
                'headerText' => L('Размер'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'complete',
                'width' => 100,
                'headerText' => L('Приключен'),
                'items' => [
                    ['text' => L('не'), 'iconCls' => 'fa fa fa-ban'],
                    ['text' => L('да'), 'iconCls' => 'fa fa fa-check-circle'],
                ],
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_FLOAT,
                'dataField' => 'l',
                'width' => 75,
                'headerText' => L('Дължина'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_FLOAT,
                'dataField' => 'total_mass',
                'width' => 75,
                'headerText' => L('Килограми'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_FLOAT,
                'dataField' => 'produced_total_mass',
                'width' => 75,
                'headerText' => L('Произведено к-во'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_DATE,
                'dataField' => 'desired_production_start',
                'width' => 100,
                'headerText' => L('Дата на производство'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_DATE,
                'dataField' => 'desired_production_end',
                'width' => 100,
                'headerText' => L('Желан край'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'code_line',
                'width' => 75,
                'headerText' => L('Линия'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_DATETIME,
                'dataField' => 'time_from',
                'width' => 125,
                'headerText' => L('Старт'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_DATETIME,
                'dataField' => 'time_to',
                'width' => 125,
                'headerText' => L('Край'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            ))
        );

        parent::__construct();
    }

    public function init($aRequest, $aInitParams) {
        $aRequest['fields']['id'] = $aInitParams['row']['id'];
        return $this->loadData($aRequest, $aInitParams);
    }

    public function loadData($aRequest, $aInitParams = []) {
        $nId = intval($aRequest['fields']['id']);

        $this->sReportSelectStatement = "
            SELECT SQL_CALC_FOUND_ROWS
              ps.code,
              pos.id,
              pos.l,
              pos.total_mass,
              pos.produced_total_mass,
              pos.complete,
              pos.desired_production_start,
              pos.desired_production_end,
              pl.code             AS code_line,
              MIN(pscr.time_from) AS time_from,
              MAX(pscr.time_to)   AS time_to
            FROM production_orders po
            JOIN production_orders_sizes pos        ON pos.id_order = po.id AND pos.to_arc = 0
            JOIN production_sizes ps                ON ps.id = pos.id_size
            LEFT JOIN production_schedule psc       ON psc.id_order_size = pos.id AND psc.to_arc = 0
            LEFT JOIN production_schedule_rows pscr ON pscr.id_schedule = psc.id AND pscr.to_arc = 0 AND pscr.type IN('planned_setup', 'planned_production')
            LEFT JOIN production_lines pl           ON pl.id = psc.id_line
        ";

        $this->aReportWhereStatement[] = 'po.id = ' . $nId;
        $this->aReportWhereStatement[] = 'pos.to_arc = 0';
        $this->sReportGroupStatement = 'pos.id';

        return parent::loadData($aRequest);
    }

    public function removeSelectedRows($aRequest) {
        return DBC::$main->withTrans(function ($aRequest) {
            if(empty($aRequest['ids'])) throw new InvalidParameter;

            $Size = new Size();
            foreach ($aRequest['ids'] as $idOrderSize) $Size->save(['fields' => ['id' => $idOrderSize, 'id_order' => $aRequest['fields']['id'], 'to_arc' => 1, 'update_note' => $aRequest['fields']['update_note']]]);

            return $this->loadData($aRequest);
        }, [$aRequest]);
    }
}