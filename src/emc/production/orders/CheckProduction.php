<?php
namespace emc\production\orders;

use emc\production\orders\sizes\Size;
use emc\production\schedule\Scheduler;
use main\db\DBC;
use main\Exception;
use main\grid\GridPanelHandler;

/**
 * Class CheckProduction
 * @package emc\production\orders
 * @accessLevelDescription Достъп до инструмент "Проверка за производство"
 */
class CheckProduction extends GridPanelHandler {
    public function __construct() {
        $this->oBase = DBC::$main;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'size',
                'width' => 200,
                'headerText' => L('Размер'),
                'resizable' => false,
                'sortable' => false,
                'filterable' => false
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_FLOAT,
                'dataField' => 'total_mass',
                'width' => 60,
                'headerText' => L('Тегло'),
                'resizable' => false,
                'sortable' => false,
                'filterable' => false
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'id_line',
                'width' => 100,
                'items' => DBC::$slave->selectAssoc("SELECT id, '', code AS text FROM production_lines WHERE to_arc = 0"),
                'headerText' => L('Линия'),
                'resizable' => false,
                'sortable' => false,
                'filterable' => false
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_DATETIME,
                'dataField' => 'time_from',
                'width' => 115,
                'headerText' => L('Начало'),
                'resizable' => false,
                'sortable' => false,
                'filterable' => false
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_DATETIME,
                'dataField' => 'time_to',
                'width' => 115,
                'headerText' => L('Край'),
                'resizable' => false,
                'sortable' => false,
                'filterable' => false
            ))
        );

        parent::__construct();
    }

    function getGridData($aRequest, $nTimeFrom = 0, $nTimeTo = 0) {
        $orderSizes = array_filter($aRequest['order_sizes'], function ($size) {
            if(empty($size['size']['id']) && !empty($size['size']['input_value'])) throw new Exception("Моля, избере размер от съджест.");
            return !empty($size['size']['id']);
        });
        $scheduler = new Scheduler();

        $gridData = [];
        foreach ($orderSizes as $orderSize) {
            $orderSize['id_size'] = $orderSize['size']['id'];
            $orderSize['l'] = $orderSize['length'];
            $orderSize['desired_production_start'] = $aRequest['fields']['desired_production_start'];
            $orderSize['total_mass'] = Size::calcTotalMass($orderSize, $orderSize['size']);
            foreach ($scheduler->getScheduleForOrderSize($orderSize) as $line) {
                $line['size'] = $orderSize['size']['code'];
                $gridData[] = $line;
            }
        }

        usort($gridData, function ($a, $b) {
            if($a['size'] != $b['size']) return strcmp($a['size'], $b['size']);
            return strtotime($a['time_to']) - strtotime($b['time_to']);
        });

        return $gridData;
    }

}