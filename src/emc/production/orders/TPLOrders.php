<?php
namespace emc\production\orders;

use main\grid\TPLGridPanel;

class TPLOrders extends TPLGridPanel {
    public function __construct($sPrefix, $aInitParams = array()) {
        $this->{'aConfig'} = array(
            'title' => L('Заявки - ДК-05-37_01'),
            'autoVResize' => true,
            'topBarItems' => array(
                array('menuButton',array('name' => 'status'), array(
                    array(
                        'name' => 'all',
                        'text' => L("Всички"),
                        'iconCls' => 'fa fa-list fa-fw'
                    ),
                    array(
                        'name' => 'waiting',
                        'text' => L("Изчакващи"),
                        'iconCls' => 'fa fa-hourglass-2 fa-fw'
                    ),
                    array(
                        'name' => 'confirmed',
                        'text' => L("Потвърдени"),
                        'iconCls' => 'fa fa-check-circle fa-fw'
                    ),
                    array(
                        'name' => 'production',
                        'text' => L("В производство"),
                        'iconCls' => 'fa fa-refresh fa-fw'
                    ),
                    array(
                        'name' => 'finished',
                        'text' => L("Приключени"),
                        'iconCls' => 'fa fa-cubes fa-fw'
                    ),
                    array(
                        'name' => 'cancelled',
                        'text' => L("Отказани"),
                        'iconCls' => 'fa fa-ban fa-fw'
                    ),
                    array(
                        'name' => 'needs_review',
                        'text' => L("Необходим преглед"),
                        'iconCls' => 'fa fa-exclamation fa-fw'
                    ),
                )),
                array('timeMenuButton', array('name' => 'time_period', 'align' => 'left')),
                array('button', array('name' => 'new', 'class' => 'add', 'iconCls' => 'icon fa fa-plus-circle', 'text' => L('Нова'))),
                array('button', array('name' => 'check_production', 'style' => 'margin-left: 50px', 'iconCls' => 'icon fa fa-check-square-o', 'text' => L('Проверка за производство')))
            ),
            'bottomBarItems' => array(
                array('exportButton', array('align' => 'right'))
            )
        );
        parent::__construct($sPrefix, $aInitParams);
    }

    public function printStyles($sPrefix) { ?>

        <!--suppress CssUnusedSymbol -->
        <style type="text/css">

            #<?=$sPrefix;?> .orderSizeDelayed {
                                background-color: #F8BBD0;
                            }

            #<?=$sPrefix;?> .orderCancelled {
                                opacity: 0.5;
                                font-style: italic;
                            }

        </style>
        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, aBaseParams) {
            var form = container;
            var grid = container.grid;

            form.fields['status'].addListener('click', function(){
                grid.loadData();
            });
            
            grid.itemRenderers['size_code'] = function(columnCfg, rowData, clickFn) {
               var el = framework.grid.itemRenderers.string(columnCfg,rowData, clickFn);
               if(rowData['marker'] == 'delayed') {
                   jQuery(el).css({'font-weight':'bold','color':'#E91E63'});
               }
               return el; 
            };

            $(form).find('.add').click(function() {
                framework.createWindow({
                    tplName: 'emc/production/orders/OrderTabInfo',
                    baseParams: {
                        onOrderChange: function(){ grid.loadData();},
                    }
                });
            });

            $(form).find('button[name=check_production]').click(function() {
                framework.createWindow({
                    tplName: 'emc/production/orders/CheckProduction',
                    baseParams: {}
                });
            });

            grid.clickListeners['num'] = function(rowNum) {
                framework.createWindow({
                    tplName: 'emc/production/orders/Order',
                    baseParams: {
                        row: container.grid.getCurrentData()[rowNum],
                        onOrderChange: function(){ grid.loadData(); },
                    }
                });
            };

            grid.clickListeners['size_code'] = function(rowNum, rowData) {
                framework.createWindow({
                    tplName: 'emc/production/orders/sizes/Size',
                    baseParams: {
                        id: rowData.id_size,
                        id_order: rowData.id,
                        onSizeChange: function () { grid.loadData(); }
                    }
                });
            };
        });
JS;
    }
}