<?php
namespace emc\production\orders\sizes;

use emc\access\profiles\ProfileMarketingEmployee;
use emc\access\profiles\ProfileMarketingManager;
use emc\access\profiles\ProfileProductionManager;
use emc\nomenclatures\productionlines\ProductionLines;
use emc\nomenclatures\sizes\Sizes;
use emc\notifications\Notifications;
use emc\production\schedule\Scheduler;
use main\Access;
use main\AccessDenied;
use main\db\DBC;
use main\Exception;
use main\form\FormPanelHandler;
use main\InvalidParameter;
use main\MultilineTextPromptException;
use main\Util;

/**
 * Class EditSize
 * @package emc\production\orders\sizes
 * @accessLevelDescription Достъп до форма Размер в Заявка за производство
 */
class Size extends FormPanelHandler {

    public function init($aRequest = array(), $aInitParams = array()) {
        $aRequest['id'] = $aInitParams['id_to_clone'] ?? $aInitParams['id'];
        $aRequest['id_to_clone'] = $aInitParams['id_to_clone'];
        return $this->loadData($aRequest);
    }

    public function loadData($aRequest) {
        $nId = intval($aRequest['id']);
        if(empty($nId)) return $this->oResponse;

        $aData = DBC::$main->selectRow("
            SELECT
              pd.name   AS direction,
              pd.id     AS id_direction,
              pst.code  AS standard,
              pst.id    AS id_standard,
              pa.name   AS application,
              pa.id     AS id_application,
              ps.id     AS id_size,
              ps.code   AS size,
              po.status,
              pos.complete,
              ps.a,
              pos.a_minus,
              pos.a_plus,
              ps.h,
              pos.h_minus,
              pos.h_plus,
              ps.d,
              pos.d_minus,
              pos.d_plus,
              ps.profile_type,
              ps.id_metal,
              pmb.id    AS id_metal_brand,
              pmb.name  AS metal_brand,
              ps.s,
              ps.m,
              pos.s                               AS s_production,
              pos.id_size,
              pos.id,
              pos.s_minus,
              pos.s_plus,
              pos.l,
              pos.l_minus,
              pos.l_plus,
              pos.quantity,
              pos.quantity_type,
              pos.quantity_minus,
              pos.quantity_plus,
              pos.cleaned_ends,
              pos.inner_cleaned_seam,
              pos.coated_seam,
              pos.zinc_galvanize_ability,
              pos.chroming_ability,
              pos.painting_ability,
              pos.bending_ability,
              pos.vortex_currents_test,
              pos.pressure_test,
              pos.special_mechanical_properties,
              pos.special_chemical_properties,
              pos.special_packing,
              pos.special_requirements,
              pos.article_num,
              DATE(pos.desired_production_start) AS desired_production_start,
              DATE(pos.desired_production_end)   AS desired_production_end,
              pos.total_mass,
              pos.created_time,
              pos.updated_time,
              CONCAT_WS(' ',pc.fname,pc.mname,pc.lname) AS created_user,
              CONCAT_WS(' ',pu.fname,pu.mname,pu.lname) AS updated_user,
              pos.id_order,
              pos.quantity_correction,
              IF(f.file IS NOT NULL, CONCAT(fd.public_url, '/', f.file, '.', f.ext),
                  CASE ps.profile_type
                      WHEN 'FOV' THEN CONCAT('images/profiles/profile_FOV.jpg')
                      WHEN 'OV' THEN CONCAT('images/profiles/profile_OV.jpg')
                      WHEN 'R' THEN CONCAT('images/profiles/profile_R.jpg')
                      WHEN 'REC' THEN CONCAT('images/profiles/profile_REC.jpg')
                      WHEN 'SFOV' THEN CONCAT('images/profiles/profile_SFOV.jpg')
                      WHEN 'SQ' THEN CONCAT('images/profiles/profile_SQ.jpg')
                      ELSE 'images/no_photo.png'
                  END
                ) AS scheme,
              pos.to_arc
            FROM production_orders_sizes pos
            JOIN production_orders         po ON po.id = pos.id_order
            JOIN production_directions     pd ON pd.id = pos.id_direction
            JOIN production_standards      pst ON pst.id = pos.id_standard
            LEFT JOIN production_metal_brands pmb ON pmb.id = pos.id_metal_brand
            LEFT JOIN production_applications   pa ON pa.id = pos.id_application
            JOIN production_sizes          ps ON ps.id = pos.id_size
            LEFT JOIN personnel pc ON pc.id = pos.created_user AND pc.to_arc = 0
            LEFT JOIN personnel pu ON pu.id = pos.updated_user AND pu.to_arc = 0
            LEFT JOIN files f ON f.file = ps.scheme_file
            LEFT JOIN file_devices fd ON fd.id = f.id_device
            WHERE 1
            AND pos.id = $nId
        ");


        foreach ($aData as $key => $val) {
            switch ($key) {
                case 'size':
                    $this->setFieldValue($key, ['id' => $aData['id_size'], 'm' => $aData['m'], 'profile_type' => $aData['profile_type'],
                        'value' => $aData['size'], 'scheme' => $aData['scheme']]);
                    break;
                case 'direction':
                case 'standard':
                case 'application':
                case 'metal_brand':
                    $this->setFieldValue($key, ['id' => $aData['id_' . $key], 'value' => $aData[$key]]);
                    break;
                default:
                    $this->setField($key, $val);
                    break;
            }
        }

        if(!empty($aRequest['id_to_clone'])) {
            $this->setFieldValue('id', null);
            $this->setFieldValue('desired_production_start', 0);
            $this->setFieldValue('desired_production_end', 0);
            $this->setField('to_arc', 0);
        } else {
            $this->setLastEditInfo($aData);
        }

        return $this->oResponse;
    }

    public function save($aRequest) {
        return DBC::$main->withTrans(function ($aRequest) {

            Access::hasAccessToSomeOfTheLevels([
                ProfileMarketingEmployee::class,
                ProfileProductionManager::class
            ]);

            $orderSize = $aRequest['fields'];
            $originalOrderSize = DBC::$main->selectByID('production_orders_sizes', $orderSize['id']);
            if(!empty($originalOrderSize['to_arc'])) throw new InvalidParameter("Не може да редактирате изтрит размер");

            $orderSize['id_order'] = $orderSize['id_order'] ?? $originalOrderSize['id_order'];

            if(empty($orderSize['id'])) Access::checkAccess(ProfileMarketingEmployee::class);

            if(empty($orderSize['id_order'])) throw new InvalidParameter;
            $order = DBC::$main->selectByID('production_orders', $orderSize['id_order']);
            if(empty($order)) throw new InvalidParameter();

            if(!empty($originalOrderSize['is_complete']) && !Access::isSuperAdmin()) throw new AccessDenied("Нямате право да редактирате размер, който е приключен.");

            if(!empty($orderSize['id']) && $order['status'] == 'confirmed' && empty($aRequest['fields']['update_note'])) throw new MultilineTextPromptException('Моля, посочетете причина за корекцията', 'update_note');

            if(!empty($orderSize['to_arc'])) {
                Access::checkAccess(ProfileMarketingEmployee::class);

                if($order['status'] == 'confirmed') {
                    Access::checkAccess(ProfileMarketingManager::class);
                    $firstShiftTime = $originalOrderSize['planned_production_start'];
                    $weeksToFirstShift = Scheduler::getProductionWeekType($firstShiftTime);
                    if(!Access::isSuperAdmin() && $weeksToFirstShift == 0) throw new AccessDenied("Не може да се изтрива размер, който е в седмицата за производство.");
                }

                Scheduler::cancelOrderSizeProduction($orderSize['id']);
                DBC::$main->update('production_orders_sizes', $orderSize);
                return $this->loadData(['id' => $orderSize['id']]);
            }


            $orderSize['id_direction'] = intval($orderSize['direction']['id']);
            if(empty($orderSize['id_direction']) && !empty($orderSize['direction']['input_value'])) {
                $direction = ['id' => 0, 'name' => $orderSize['direction']['input_value']];
                DBC::$main->update('production_directions', $direction);
                $orderSize['id_direction'] = $direction['id'];
            }

            $orderSize['id_standard'] = intval($orderSize['standard']['id']);
            if(empty($orderSize['id_standard']) && !empty($orderSize['standard']['input_value'])) {
                $standard = ['id' => 0, 'code' => $orderSize['standard']['input_value']];
                DBC::$main->update('production_standards', $standard);
                $orderSize['id_standard'] = $standard['id'];
            }

            $orderSize['id_application'] = intval($orderSize['application']['id']);
            if(empty($orderSize['id_application']) && !empty($orderSize['application']['input_value'])) {
                $application = ['id' => 0, 'name' => $orderSize['application']['input_value']];
                DBC::$main->update('production_applications', $application);
                $orderSize['id_application'] = $application['id'];
            }

            $orderSize['id_metal_brand'] = intval($orderSize['metal_brand']['id']);
            if(empty($orderSize['id_metal_brand']) && !empty($orderSize['metal_brand']['input_value'])) {
                throw new InvalidParameter("Моля, изберете от възможните стойности.", ['metal_brand']);
            }

            $orderSize['article_num'] = trim($orderSize['article_num']);
            $orderSize['metal_brand'] = $orderSize['metal_brand']['input_value'];
            $orderSize['id_size'] = intval($orderSize['size']['id'] ?? $originalOrderSize['id_size']);

            $size = Sizes::getSizeByID($orderSize['id_size']);
            if(empty($size)) throw new InvalidParameter("Моля, изберете размер", array('size'));

            $orderSize['total_mass'] = self::calcTotalMass($orderSize, $size);

            $ex = [];
            foreach ([
                         'id_direction' => L('направление'),
                         'id_standard' => L('стандарт'),
                         'id_size' => L('размер'),
                         'l' => L('дължина на тръбата'),
                         'quantity_type' => L('количество желано от клиента'),
                         'quantity' => L('количество желано от клиента'),
                         'id_metal_brand' => L('Метал - Марка/Код')
                     ] as $field => $name) {
                try {
                    if(empty($orderSize[$field])) throw new InvalidParameter($name, array(str_replace('id_', '', $field)));
                } catch (Exception $e) {
                    $ex[] = $e;
                }
            }
            Exception::checkThrow($ex, "Моля, попълнете стойност за:");

            foreach (['d', 'a', 'h'] as $k) $orderSize[$k] = $size[$k];
            $orderSize['s'] = !empty($orderSize['s_production']) ? $orderSize['s_production'] : $size['s'];

            $filterFields = $orderSize['profile_type'] == 'R' ? ['a', 'h'] : ['d'];
            foreach ($filterFields as $k) foreach (['', '_minus', '_plus'] as $f) unset($orderSize[$k . $f]);

            if(!Access::isSuperAdmin()) {
                if(empty($orderSize['id'])) {
                    foreach (['start', 'end'] as $k) {
                        if(empty($orderSize['desired_production_' . $k])) continue;
                        if(strtotime($orderSize['desired_production_' . $k]) < strtotime(date('Y-m-d'))) {
                            throw new InvalidParameter("Моля, изберете дата по-голяма или равна на днешната.", ['desired_production_' . $k]);
                        }
                    }
                }
            }

            if(1
                && !empty($orderSize['desired_production_start'])
                && !empty($orderSize['desired_production_end'])
                && strtotime($orderSize['desired_production_end']) <= strtotime($orderSize['desired_production_start'])
                && !Access::hasAccess(ProfileProductionManager::class)
            ) throw new InvalidParameter("Моля, посочете коректни желани времена за производство");

            // emc 54
            foreach(array('s','d', 'a', 'h') as $key) {
                //ako i dvata sa 0 to togava stavat na NULL
                //ako ediniq e != 0, a drugiq 0, to tozi, koito e 0 si ostava 0
                if(empty($orderSize[$key.'_minus']) && empty($orderSize[$key.'_plus'])) {
                    $orderSize[$key.'_minus'] = null;
                    $orderSize[$key.'_plus'] = null;
                    continue;
                }
            }

            DBC::$main->update('production_orders_sizes', $orderSize);
            $orderSize = Size::getSizeByID($orderSize['id']);

            if($order['status'] == 'confirmed') {
                if(empty($originalOrderSize) && empty(Util::SQLDateToTimeStamp($orderSize['desired_production_start']))) throw new InvalidParameter("При добавяне на размер в потвърдена поръчка задължително се попълва дата за производство", ['desired_production_start']);

                $firstShiftTime = $originalOrderSize['planned_production_start'] ?? $orderSize['desired_production_start'];

                if(!empty($firstShiftTime)) {
                    $weeksToFirstShift = Scheduler::getProductionWeekType($firstShiftTime);

                    if($weeksToFirstShift == 0) {
                        //в седмицата за произвдство до приключване на произвдството
                        //Мениджър Произвдоство
                        Access::checkAccess(ProfileProductionManager::class);

                        $ignoredFields = ['quantity_type', 'quantity', 'quantity_plus', 'quantity_minus', 'l', 'total_mass'];
                        if(strtotime($orderSize['planned_production_start']) > time()) $ignoredFields[] = 'desired_production_start';

                        $this->checkForChange($orderSize, $originalOrderSize, $ignoredFields);

                    } else if($weeksToFirstShift == 1) {
                        if(Access::hasAccessToNoneOfTheLevels([ProfileMarketingManager::class, ProfileProductionManager::class])) {
                            throw new AccessDenied("Нямате права за редакция на поръчка в седмицата преди производство");
                        }

                        //в седмицата непосредствено преди произвдство
                        if(Access::hasAccess(ProfileProductionManager::class)) {
                            $this->checkForChange($orderSize, $originalOrderSize, ['desired_production_start']);
                        } else if(Access::hasAccess(ProfileMarketingManager::class)) {
                            if(!empty($orderSize['desired_production_start'])) {
                                if(!(Scheduler::getProductionWeekType($orderSize['desired_production_start']) >= 1)) {
                                    throw new InvalidParameter("Желаното време за производство трябва да бъде в следващата работна седмица или по-голямо.");
                                };
                            }
                        }

                    } else if($weeksToFirstShift >= 2) {
                        if(Access::hasAccess(ProfileMarketingEmployee::class)) {
                            $week = Access::hasAccess(ProfileMarketingManager::class) ? 1 : 2;

                            if(!empty($orderSize['desired_production_start'])) {
                                if(!(Scheduler::getProductionWeekType($orderSize['desired_production_start']) >= $week)) {
                                    if(!Access::hasAccess('/')) throw new InvalidParameter(L("Желаното време за производство трябва да бъде %s или повече след текущата работна седмица.", $week == 1 ? 'една седмица' : 'две седмици'));
                                };
                            }

                            if(!Access::hasAccess(ProfileMarketingManager::class)) DBC::$main->multiInsert('production_orders_sizes', [['id' => $orderSize['id'], 'needs_review' => 1]]);

                        } else if(Access::hasAccess(ProfileProductionManager::class)) {
                            $this->checkForChange($orderSize, $originalOrderSize, ['desired_production_start']);
                        } else throw new AccessDenied();
                    }
                }

                $idLine = Scheduler::confirmOrderSizeProduction($orderSize['id'], true);
                $possibleLines = ProductionLines::getPossibleLinesForSize($orderSize['id_size']);
                if(empty($possibleLines[$idLine])) throw new InvalidParameter("Избраният размер не се произвежда на текущата тръбна линия. Изберете друг размер или преди това планирайте размера на другия линия.");

                Scheduler::scheduleLine($idLine);

                if(self::hasChanges($orderSize, $originalOrderSize)) {
                    Notifications::notifyChangedOrder($orderSize['id_order'], [$orderSize['id']], [$idLine], time());
                }
            }

            $this->oResponse->{'size_id'} = $orderSize['id'];

            return $this->loadData(['id' => $orderSize['id']]);
        }, array($aRequest));
    }

    public function hasChanges($orderSize, $originalOrderSize, $ignoredFields = []) {
        $baseIgnoredFields = [
            "planned_production_start",
            "planned_production_end",
            "is_delayed",
            "needs_review",
            "created_user",
            "created_time",
            "updated_user",
            "updated_time",
            "updated_count",
            "update_note"
        ];

        $ignoredFields = array_merge($baseIgnoredFields, $ignoredFields);
        $ignoredFields = array_combine($ignoredFields, $ignoredFields);

        $changedFields = [];
        foreach (array_keys(DBC::$main->getTableFields('production_orders_sizes')) as $k) {
            if($ignoredFields[$k]) continue;
            if($orderSize[$k] != $originalOrderSize[$k]) {
                $changedFields[$k] = $k;
            }
        }
        if(!empty($changedFields)) return $changedFields;
        return false;
    }

    private function checkForChange($orderSize, $originalOrderSize, $ignoredFields = null) {
        if(Access::isSuperAdmin()) return;
        if(($changedFields = self::hasChanges($orderSize, $originalOrderSize, $ignoredFields))){
            throw new AccessDenied("Нямате право да редактирате посочените данни по размер.", $changedFields);
        }
    }


    public static function calcTotalMass($orderSize, $size) {
        $orderSize['s_production'] = doubleval($orderSize['s_production']);

        if($orderSize['s_production'] != $size['s']) {
            $size = Sizes::getSizeByID($size['id']);
            $size['s_production'] = $orderSize['s_production'];
            $size['m'] = Sizes::calcLinearMass($size);
        }

        switch ($orderSize['quantity_type']) {
            case "count":
                $orderSize['total_mass'] = ($orderSize['quantity'] * ($orderSize['l'] / 1000)) * $size['m'];
                break;
            case "mass":
                $orderSize['total_mass'] = $orderSize['quantity'];
                break;
            case "length":
                $orderSize['total_mass'] = $orderSize['quantity'] * $size['m'];
        }

        if($orderSize['quantity_type'] != 'mass' && !empty($orderSize['quantity_correction'])) {
            $orderSize['total_mass'] = $orderSize['total_mass']  * (($orderSize['quantity_correction']/100)+1);
        }

        return round($orderSize['total_mass'], 2);
    }


    public static function getSizeByID($id) {
        $id = intval($id);
        return DBC::$main->selectRow("SELECT * FROM production_orders_sizes WHERE id = $id");
    }


}