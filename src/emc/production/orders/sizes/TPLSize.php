<?php
namespace emc\production\orders\sizes;

use emc\nomenclatures\Profiles;
use main\Access;
use main\db\DBC;
use main\TPLBase;

class TPLSize extends TPLBase {

    public function __construct($sPrefix, $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);

        $this->aFrameConfig['title'] = L('Производствен размер');
    }

    public function printHtml() {
        $aTypes = array(
            array('id' => 'count', 'value' => L('бр')),
            array('id' => 'mass', 'value' => L('кг')),
            array('id' => 'length', 'value' => L('м'))
        );
        ?>
        <style>
            #<?=$this->sPrefix?> .customLabel {
                display: block;
            }

            #<?=$this->sPrefix?> .float-left {
                float: left;
            }

            #<?=$this->sPrefix?> .float-right {
                float: right;
            }

            #<?=$this->sPrefix?> .width40 {
                width: 40px;
            }

            #<?=$this->sPrefix?> .width25 {
                width: 25px;
            }

            #<?=$this->sPrefix?> .width65 {
                width: 65px;
            }

            #<?=$this->sPrefix?> .width8 {
                width: 8px;
                text-align: center;
            }

            #<?=$this->sPrefix?> .width97 {
                width: 97%;
            }

            #<?=$this->sPrefix?> .widthFull {
                width: 100%;
            }

            #<?=$this->sPrefix?> .size {
                width: 97%;
                font-weight: bold;
            }

            #<?=$this->sPrefix?> .editButtons {
                vertical-align: bottom;
                text-align: right;
                width: 100px;
            }

            #<?=$this->sPrefix?> .props  td {
                line-height:24px;
                padding-top:1px;
                padding-bottom:1px;
                padding-left:5px;
            }

            #<?=$this->sPrefix?> td {
                padding: 1px;
            }

            #<?=$this->sPrefix?> .input-border-box-26 {
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
                height: 26px !important;
            }

        </style>
        <table>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <? $this->printLabel(L('Направление:'), 'direction', array('class' => 'customLabel')) ?>
                                <? $this->printElement('suggestcombo', array(
                                    'name' => 'direction',
                                    'suggest' => 'method: emc\nomenclatures\directions\Edit.suggestDirection;'
                                )) ?>
                            </td>
                            <td class="editButtons">
                                <? $this->printElement('button', array('name' => 'add_dir', 'iconCls' => 'icon fa fa-plus-square')) ?>
                                <? $this->printElement('button', array('name' => 'rem_dir', 'iconCls' => 'icon fa fa-minus-square')) ?>
                                <? $this->printElement('button', array('name' => 'edit_dir', 'iconCls' => 'icon fa fa-pencil-square')) ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <? $this->printLabel(L('Стандарт:'), 'standard', array('class' => 'customLabel')) ?>
                                <? $this->printElement('suggestcombo', array(
                                    'name' => 'standard',
                                    'suggest' => 'method: emc\nomenclatures\standards\Edit.suggestStandard;'
                                )) ?>
                            </td>
                            <td class="editButtons">
                            <? $this->printElement('button', array('name' => 'add_st', 'iconCls' => 'icon fa fa-plus-square')) ?>
                            <? $this->printElement('button', array('name' => 'rem_st', 'iconCls' => 'icon fa fa-minus-square')) ?>
                            <? $this->printElement('button', array('name' => 'edit_st', 'iconCls' => 'icon fa fa-pencil-square')) ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <? $this->printLabel(L('Ще се използва за:'), 'application', array('class' => 'customLabel')) ?>
                                <? $this->printElement('suggestcombo', array(
                                    'name' => 'application',
                                    'suggest' => 'method: emc\nomenclatures\applications\Edit.suggestApplication;'
                                )) ?>
                            </td>
                            <td class="editButtons">
                            <? $this->printElement('button', array('name' => 'add_appl', 'iconCls' => 'icon fa fa-plus-square')) ?>
                            <? $this->printElement('button', array('name' => 'rem_appl', 'iconCls' => 'icon fa fa-minus-square')) ?>
                            <? $this->printElement('button', array('name' => 'edit_appl', 'iconCls' => 'icon fa fa-pencil-square')) ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <fieldset>
                                    <legend><?= L('Размер') ?></legend>
                                    <table style="width: 100%">
                                        <tr>
                                            <td colspan="9">
                                                <? $this->printElement('input', array(
                                                    'name' => 'size',
                                                    'class' => 'size',
                                                    'suggest' => 'method: emc\nomenclatures\sizes\Sizes.suggestSize'
                                                )) ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="65px">
                                                <? $this->printLabel(L('Ширина:'), 'a') ?>
                                            </td>
                                            <td class="width25">
                                                <? $this->printElement('input', array('name' => 'a', 'class' => 'width25', 'disabled' => 'disabled', 'keyRestriction' => 'float')) ?>
                                            </td>
                                            <td class="width8">
                                                -
                                            </td>
                                            <td class="width25">
                                                <? $this->printElement('input', array('name' => 'a_minus', 'class' => 'width25', 'keyRestriction' => 'float')) ?>
                                            </td>
                                            <td class="width8">
                                                +
                                            </td>
                                            <td class="width25">
                                                <? $this->printElement('input', array('name' => 'a_plus', 'class' => 'width25', 'keyRestriction' => 'float')) ?>
                                            </td>
                                            <td>
                                                <?= L('mm') ?>
                                            </td>
                                            <td rowspan="3" width="120px" style="border: solid 1px rgba(204, 204, 203, 0.42);">
                                                <div><? $this->printElement('img', array('name' => 'scheme', 'src' => 'images/no_photo.png', 'style' => 'width: 120px; min-height: 55px')); ?></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="65px">
                                                <? $this->printLabel(L('Височина:'), 'h') ?>
                                            </td>
                                            <td>
                                                <? $this->printElement('input', array('name' => 'h', 'class' => 'width25', 'disabled' => 'disabled', 'keyRestriction' => 'float')) ?>
                                            </td>
                                            <td class="width8">
                                                -
                                            </td>
                                            <td>
                                                <? $this->printElement('input', array('name' => 'h_minus', 'class' => 'width25', 'keyRestriction' => 'float')) ?>
                                            </td>
                                            <td class="width8">
                                                +
                                            </td>
                                            <td>
                                                <? $this->printElement('input', array('name' => 'h_plus', 'class' => 'width25', 'keyRestriction' => 'float')) ?>
                                            </td>
                                            <td>
                                                <?= L('mm') ?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <? $this->printLabel(L('Диаметър:'), 'd') ?>
                                            </td>
                                            <td>
                                                <? $this->printElement('input', array('name' => 'd', 'class' => 'width25', 'disabled' => 'disabled', 'keyRestriction' => 'float')) ?>
                                            </td>
                                            <td class="width8">
                                                -
                                            </td>
                                            <td>
                                                <? $this->printElement('input', array('name' => 'd_minus', 'class' => 'width25', 'keyRestriction' => 'float')) ?>
                                            </td>
                                            <td class="width8">
                                                +
                                            </td>
                                            <td>
                                                <? $this->printElement('input', array('name' => 'd_plus', 'class' => 'width25', 'keyRestriction' => 'float')) ?>
                                            </td>
                                            <td>
                                                <?= L('mm') ?>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>

                            <td colspan="2">
                                <? $this->printLabel(L('Профил:'), 'profile_type') ?>
                                <? $this->printElement('select', array('name' => 'profile_type', 'style'=>'width: 315px;', 'disabled' => 'disabled'), Profiles::$selectContent) ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <fieldset>
                                    <legend><?= L('Метал') ?></legend>
                                    <table style="width: 100%">
                                        <tr>
                                            <td>
                                                <? $this->printLabel(L('Вид:'), 'id_metal') ?>
                                            </td>
                                            <td>
                                                <? $this->printElement('select', array('name' => 'id_metal', 'class' => 'widthFull', 'disabled' => 'disabled'), DBC::$slave->select("SELECT id, CONCAT('(', code, ') ', name) AS value FROM metals WHERE to_arc = 0 ORDER BY code")) ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <? $this->printLabel(L('Марка/код:'), 'metal_brand') ?>
                                            </td>
                                            <td colspan="2">
                                                <? $this->printElement('suggestcombo', array(
                                                    'name' => 'metal_brand',
                                                    'suggest' => 'method: emc\nomenclatures\metalbrands\Edit.suggestMetalBrand;'
                                                )) ?>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <fieldset>
                                    <legend><?= L('Дебелина в mm') ?></legend>
                                    <table style="width: 100%">
                                        <tr>
                                            <td>
                                                <? $this->printLabel(L('От клиента:'), 's') ?>
                                            </td>
                                            <td>
                                                <? $this->printElement('input', array('name' => 's', 'class' => 'width40', 'disabled' => 'disabled', 'keyRestriction' => 'float')) ?>
                                            </td>
                                            <td>
                                                -
                                            </td>
                                            <td>
                                                <? $this->printElement('input', array('name' => 's_minus', 'class' => 'width40', 'keyRestriction' => 'float')) ?>
                                            </td>
                                            <td>
                                                +
                                            </td>
                                            <td>
                                                <? $this->printElement('input', array('name' => 's_plus', 'class' => 'width40', 'keyRestriction' => 'float')) ?>
                                            </td>
                                            <td>
                                                <?= L('mm') ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <? $this->printLabel(L('Производство:'), 's_production') ?>
                                            </td>
                                            <td>
                                                <? $this->printElement('input', array('name' => 's_production', 'class' => 'width40', 'keyRestriction' => 'float')) ?>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <fieldset>
                                    <legend><?= L('Тръби') ?></legend>
                                    <table style="width: 100%">
                                        <tr>
                                            <td>
                                                <? $this->printLabel(L('Дължина:'), 'l') ?>
                                            </td>
                                            <td>
                                                <? $this->printElement('input', array('name' => 'l', 'class' => 'width40', 'keyRestriction' => 'float')) ?>
                                            </td>
                                            <td>
                                                -
                                            </td>
                                            <td>
                                                <? $this->printElement('input', array('name' => 'l_minus', 'class' => 'width40', 'keyRestriction' => 'float')) ?>
                                            </td>
                                            <td>
                                                +
                                            </td>
                                            <td>
                                                <? $this->printElement('input', array('name' => 'l_plus', 'class' => 'width40', 'keyRestriction' => 'float')) ?>
                                            </td>
                                            <td>
                                                <?= L('mm') ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <? $this->printLabel(L('К-во:'), 'quantity') ?>
                                            </td>
                                            <td>
                                                <? $this->printElement('input', array('name' => 'quantity', 'class' => 'width40', 'keyRestriction' => 'float')) ?>
                                                <? $this->printElement('select', array('name' => 'quantity_type', 'class' => 'width40 quant'), $aTypes) ?>
                                            </td>
                                            <td>
                                                -
                                            </td>
                                            <td>
                                                <? $this->printElement('input', array('name' => 'quantity_minus', 'class' => 'width40', 'keyRestriction' => 'float')) ?>
                                            </td>
                                            <td>
                                                +
                                            </td>
                                            <td>
                                                <? $this->printElement('input', array('name' => 'quantity_plus', 'class' => 'width40', 'keyRestriction' => 'float')) ?>
                                            </td>
                                            <td>
                                                <?= L('%%') ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <? $this->printLabel(L('Корекция:'), 'quantity_correction') ?>
                                            </td>
                                            <td colspan="2">
                                                <? $this->printElement('input', array('name' => 'quantity_correction', 'class' => 'width40', 'keyRestriction' => 'float')) ?>
                                                <?= L('%%') ?>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="vertical-align: top">
                    <table class="props">
                        <?php
                            $additionalProperties = [
                                ['label' => 'Зачистване на краища', 'key'=> 'cleaned_ends'],
                                ['label' => 'Вътрешно зачистване на шева', 'key'=> 'inner_cleaned_seam'],
                                ['label' => 'Покрит заваръчен шев', 'key'=> 'coated_seam'],
                                ['label' => 'Годност на горещо поцинковане', 'key'=> 'zinc_galvanize_ability'],
                                ['label' => 'Годност за хромиране', 'key'=> 'chroming_ability'],
                                ['label' => 'Годност за боядисване', 'key'=> 'painting_ability'],
                                ['label' => 'Годност за огъване', 'key'=> 'bending_ability'],
                                ['label' => 'Проверка с вихрови токове', 'key'=> 'vortex_currents_test'],
                                ['label' => 'Тест под налягане', 'key'=> 'pressure_test'],
                            ];
                        ?>
                        <?php foreach($additionalProperties as $prop): ?>
                            <tr>
                                <td>
                                    <? $this->printElement('input', array('type' => 'checkbox', 'name' => $prop['key'])) ?>
                                    <? $this->printLabel($prop['label'], $prop['key']) ?>
                                </td>
                            </tr>
                        <?php endforeach;?>

                        <?php
                            $additionalProperties = [
                                ['label' => 'Специални механични показатели', 'key'=> 'special_mechanical_properties'],
                                ['label' => 'Специални химически показатели', 'key'=> 'special_chemical_properties'],
                                ['label' => 'Специална опаковка', 'key'=> 'special_packing'],
                                ['label' => 'Други специални изисквания, чертеж номер', 'key'=> 'special_requirements'],
                            ];
                        ?>
                        <?php foreach($additionalProperties as $prop): ?>
                            <tr>
                                <td>
                                    <? $this->printLabel($prop['label'].':', $prop['key'],array('class' => 'customLabel')) ?>
                                    <? $this->printElement('input', array('class' => 'width97', 'name' => $prop['key'])) ?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                        <tr>
                            <td>
                                <? $this->printLabel(L('Артикулен №:') . ' ', 'article_num', array('class' => 'float-left', 'style' => 'margin-top: 2px;')) ?>
                                <span style="display: block; overflow: hidden; padding: 2px;">
                                    <? $this->printElement('input', array('name' => 'article_num', 'class' => 'input-border-box-26 widthFull', 'style' => 'margin-left: 2px;')) ?>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <? $this->printLabel(L('Дата на производство:'), 'desired_production_start') ?>
                                <? $this->printElement('input', array('class' => 'datePicker float-right', 'name' => 'desired_production_start')) ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <? $this->printLabel(L('Желано излизане от пр-во:'), 'desired_production_end') ?>
                                <? $this->printElement('input', array('class' => 'datePicker float-right', 'name' => 'desired_production_end')) ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <? $this->printLabel(L('Количество:'), 'total_mass', array('style' => 'font-weight: bold')) ?>
                                <? $this->printElement('input', array('name' => 'total_mass', 'disabled' => 'disabled', 'keyRestriction' => 'float')) ?>
                                <?= L('кг') ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="toolbar align-right" style="margin-top: 5px">
                        <? $this->printElement('lasteditedinfo'); ?>
                        <? $this->printElement('closebutton'); ?>
                        <? $this->printElement('confirmbutton'); ?>
                        <? $this->printElement('button', array('text' => L('Клонирай'), 'class' => 'clone align-right', 'iconCls' => 'fa fa-clone fa-fw')); ?>
                    </div>
                </td>
            </tr>
        </table>

        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, baseParams) {
            var form = container;

            if (!baseParams['id']) {
                $(form).find('.clone').css('display', 'none');
            }

            $(form).find('input[keyRestriction="float"]').css('text-align','right');
            
            form.addListener('beforeRequest', function(request){
                request['fields']['id'] = baseParams['id'];
                request['fields']['id_order'] = baseParams['id_order'];
                return request;
            });

            form.fields.size.addListener('suggestSelect', function(size) {
                $.each(size, function(k, v){
                    if(!form.fields[k]) return;
                    form.fields[k].setValue(v);
                    if(k == 's') form.fields[k+'_production'].setValue(v);
                });

                setTimeout(function(){
                    updateStates();
                    recalcTotalMass();
                },0);

            });

            $(form).find("input[name=quantity], select[name=quantity_type], input[name=l], input[name=s_production],input[name=quantity_correction]").change(function() {
                updateStates();
                recalcTotalMass();
            });

            $(form).find('.confirm').click(function() {
                form.request('save', {}, null, function(result) {
                    if(baseParams['onSizeChange']) baseParams['onSizeChange']();
                    baseParams['id'] = result['size_id'];
                    $(form).find('.clone').css('display', 'block');
                });
            });

            form.addListener('afterRequest', function(response) {
                setTimeout(function(){
                    $(form).find('input').blur();
                    
                    if(!empty(response['fields']['to_arc']['value']) > 0) {
                        $(form).find('.confirm').attr('disabled','disabled');
                    }
                
                    updateStates();
                    recalcTotalMass();
                },0);
            });

            $(form).find('img[name=scheme]').click(function() {
                window.open(this.src);
            });

            var updateStates = function(){
                var size = form.fields['size'].getValue();
                var scheme = $(form).find('img[name=scheme]');

                scheme[0].src = size['scheme'];
                scheme.css('cursor', 'pointer');
                scheme.attr('title', 'Виж схемата в цял размер');

                if(size.profile_type === "R") {
                    $(form.fields.h_plus).attr('disabled', 'disabled');
                    $(form.fields.h_minus).attr('disabled', 'disabled');
                    $(form.fields.a_plus).attr('disabled', 'disabled');
                    $(form.fields.a_minus).attr('disabled', 'disabled');

                    $(form.fields.d_plus).removeAttr('disabled');
                    $(form.fields.d_minus).removeAttr('disabled');
                }
                else {
                    $(form.fields.d_plus).attr('disabled', 'disabled');
                    $(form.fields.d_minus).attr('disabled', 'disabled');

                    $(form.fields.h_plus).removeAttr('disabled');
                    $(form.fields.h_minus).removeAttr('disabled');
                    $(form.fields.a_plus).removeAttr('disabled');
                    $(form.fields.a_minus).removeAttr('disabled');
                }
                
                if(form.fields.quantity_type.getValue() !== 'mass') {
                     $(form.fields.quantity_correction).removeAttr('disabled');
                     if(empty(form.fields.quantity_correction.getValue()) && empty(baseParams['id'])) form.fields.quantity_correction.setValue(2);
                } else {
                     $(form.fields.quantity_correction).attr('disabled', 'disabled');
                }
                
                if(empty(baseParams['id'])) {
                    if(empty(form.fields.quantity_plus.getValue())) form.fields.quantity_plus.setValue(10);
                    if(empty(form.fields.quantity_minus.getValue())) form.fields.quantity_minus.setValue(10);
                }
                
            };

            var recalcTotalMass = function() {
                Server.call('emc/production/orders/sizes/Size.calcTotalMass',[form.getFieldValues(), form.fields['size'].getValue()],function(result){
                    $(form).find('input[name=total_mass]').val(result);
                },null,null,false);
            };

            var openEditWnd = function(id, tpl, field) {
                framework.createWindow({
                    tplName: tpl,
                    baseParams: { id: id, reloadParentField: function(res) { 
                        res['value'] = res['name'];
                        form.fields[field].setValue(res);
                    }}
                });
            };

            $(form).find('.clone').click(function() {
                baseParams['id_to_clone'] = baseParams['id'];
                baseParams['id'] = '';
                framework.createWindow({
                    tplName: 'emc/production/orders/sizes/Size',
                    baseParams: baseParams
                });

                $(container).find('.close').click();
            });

            // direction
            $(form).find('button[name=add_dir]').click(function() {
                openEditWnd(0, 'emc/nomenclatures/directions/Edit', 'direction');
            });

            $(form).find('button[name=rem_dir]').click(function() {
                framework.confirm('', L('Изтриване на направление?'), function(sure) {
                    if(sure) {
                        var id = form.fields.direction.getValue()['id']
                        Server.call('emc/nomenclatures/directions/Edit.remove', { id: id}, function(res) {
                            form.fields.direction.setValue({id: 0, value: ''});
                        });
                    }
                });
            });

            $(form).find('button[name=edit_dir]').click(function() {
                openEditWnd(form.fields.direction.getValue().id, 'emc/nomenclatures/directions/Edit', 'direction');
            });

            // standard
            $(form).find('button[name=add_st]').click(function() {
                openEditWnd(0, 'emc/nomenclatures/standards/Edit', 'standard');
            });

            $(form).find('button[name=rem_st]').click(function() {
                framework.confirm('', L('Изтриване на стандарт?'), function(sure) {
                    if(sure) {
                        var id = form.fields.standard.getValue()['id']
                        Server.call('emc/nomenclatures/standards/Edit.remove', { id: id}, function(res) {
                            form.fields.standard.setValue({id: 0, value: ''});
                        });
                    }
                });
            });

            $(form).find('button[name=edit_st]').click(function() {
                openEditWnd(form.fields.standard.getValue().id, 'emc/nomenclatures/standards/Edit', 'standard');
            });

            // application
            $(form).find('button[name=add_appl]').click(function() {
                openEditWnd(0, 'emc/nomenclatures/applications/Edit', 'application');
            });

            $(form).find('button[name=rem_appl]').click(function() {
                framework.confirm('', L('Изтриване на приложение?'), function(sure) {
                    if(sure) {
                        var id = form.fields.application.getValue()['id']
                        Server.call('emc/nomenclatures/applications/Edit.remove', { id: id }, function() {
                            form.fields.application.setValue({id: 0, value: ''});
                        });
                    }
                });
            });

            $(form).find('button[name=edit_appl]').click(function() {
                openEditWnd(form.fields.application.getValue().id, 'emc/nomenclatures/applications/Edit', 'application');
            });

            
        });
JS;
    }
}
