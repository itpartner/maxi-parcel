<?php
namespace emc\production\orders;

use main\db\DBC;
use main\form\FormPanelHandler;
use main\InvalidParameter;

/**
 * Class Order
 * @accessLevelDescription Достъп до форма Редакция на заявка
 */
class Order extends FormPanelHandler {

    public function __construct() {
        parent::__construct();
    }

    public static function getNextNum($sType) {
        $sEscapedType = DBC::$main->escape($sType);
        DBC::$main->Execute("UPDATE next_production_order_num SET num = last_insert_id(IF(date_format(now(),'%y') > (num DIV 10000), (date_format(now(),'%y') * 10000) + 1, num + 1)) WHERE type = '$sEscapedType'");
        if(!DBC::$main->affectedRows()) throw new InvalidParameter(L('Невалиден тип на номер.'));
        return $sType . DBC::$main->lastInsertID();
    }
}