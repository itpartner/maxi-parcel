<?php
namespace emc\production\orders;

use main\grid\TPLGridPanel;
use main\TPLBase;

class TPLCheckProduction extends TPLGridPanel {
    public function __construct($sPrefix, $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);

        $this->{'aConfig'} = array(
            'title' => 'Проверка за производство',
            'autoVResize' => true,
            'disablePaging' => true,
            'dataHeight' => 200,
            'hideRowIndexes' => true,
            'topBarItems' => array(),
            'bottomBarItems' => array()
        );

        $this->{'bNoDefaultTopBarItems'} = true;

    }

    public function printHtml() {
        ?>
        <style>
            #<?=$this->sPrefix?> .main_tbl {
                width: 660px;
                height: 480px;
            }

            #<?=$this->sPrefix?> .upper_tbl {
                border-collapse: collapse;
                width: 100%;
                max-height: 200px;
            }

            #<?=$this->sPrefix?> .bottom_tbl {
                height: 200px;
            }

            #<?=$this->sPrefix?> .upper_div {
                width: 100%;
                height: 200px;
                overflow: auto;
            }

            #<?=$this->sPrefix?> .bottom_div {
                width: 100%;
                height: 250px;
                overflow: auto;
            }

            #<?=$this->sPrefix?> .check {
                float: right;
            }

            #<?=$this->sPrefix?> #queryTable th {
                line-height: 22px;
                border: 1px solid rgba(0, 0, 0, 0.04);
                white-space: nowrap;
                background: rgba(0, 104, 154, 0.11);
                padding: 3px 0 3px 6px;
            }

            #<?=$this->sPrefix?> #queryTable td {
                white-space: nowrap;
                padding: 3px 6px;
                border: 1px solid rgba(0, 0, 0, 0.02);
                line-height: 24px;
            }

            #<?=$this->sPrefix?> #queryTable tr:nth-child(odd) {
                background: rgba(96, 125, 139, 0.09);
            }

            #<?=$this->sPrefix?> #queryTable td:first-child {
                padding-left: 0px;
            }

            #<?=$this->sPrefix?> #queryTable td:last-child {
                text-align: center;
            }

            #<?=$this->sPrefix?> input.quantity {
                width: 50px;
                margin-right: 3px !important;
            }

            #<?=$this->sPrefix?> input.length {
                width: 60px;
            }

            #<?=$this->sPrefix?> input.size {
                width: 200px;

            }

            #<?=$this->sPrefix?> div.grid-headers-inner {
                padding-right: 0 !important;
            }
        </style>

            <table class="main_tbl">
                <tr>
                    <td>
                        <? $this->printLabel(L('Дата на производство:'), 'desired_production_start') ?>
                        <? $this->printElement('input', array('class' => 'datePicker float-right', 'name' => 'desired_production_start', 'value' => date('Y-m-d'))) ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="form upper_div">
                            <table id="queryTable" class="upper_tbl">
                                <thead style="border-bottom: 2px solid rgba(96, 125, 139, 0.6); color:#37474F">
                                <tr>
                                    <th>Размер</th>
                                    <th>Дължина</th>
                                    <th style="width: 95px">Количество</th>
                                    <th style="text-align: center; padding-left: 0"><? $this->printElement('button', array('class' => 'addRow', 'iconCls' => 'icon icon fa fa-plus')) ?></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php $this->printResultTable(empty($this->oGridPanelHandler) ? $this->sServerClassName : $this->oGridPanelHandler, $this->{'aConfig'}); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="toolbar align-right" style="margin-top: 5px">
                            <? $this->printElement('closeButton') ?>
                            <? $this->printElement('button', array('class' => 'check', 'iconCls' => 'icon fa fa-history', 'text' => L('Провери'))) ?>
                        </div>
                    </td>
                </tr>
            </table>
        <?php
    }

    public function getJSFunction() {
        return <<<'JS'
        (function(container, prefix, baseParams) {
            form = container;

            var quantityOptions = [
                {'id' : 'count', 'value' : 'бр'},
                {'id' : 'mass', 'value' : 'кг'},
                {'id' : 'length', 'value' : 'м'}
            ];
            
            var getOrderSizes = function(){
                rows = [];
                $(form).find('#queryTable').find('tbody').find('tr').each(function(k,el){
                    var row = {};
                    $(['size','quantity','quantity_type','length']).each(function(k,v){
                      row[v] = $(el).find('.'+v).get(0).getValue();   
                    });
                    rows.push(row);
                });
                return rows;
            };
            

            var addRow = function() {
                var row = document.createElement('tr');
                
                var suggest = $("<input/>").attr({suggest:'method: emc/nomenclatures/sizes/Sizes.suggestSize',class:'size',requestFocus:'requestFocus'});
                suggest[0].addListener = framework.addListener;
                suggest[0].removeListener = framework.removeListener;
                suggest[0].callListeners = framework.callListeners;
                
                $(row).append($('<td></td>').append(suggest));
                $(row).append($('<td></td>').append($('<input/>').attr({class:'length','keyRestriction':'integer'})).append('<span>&nbsp;'+L('mm')+'</span>'));
                
                td = document.createElement('td');
                $(td).append($('<input/>').attr({class:'quantity','keyRestriction':'float'}));

                var select = document.createElement('select');
                select.className = 'quantity_type';
                $.each(quantityOptions, function(k,v) {
                    $(select).append($("<option></option>").attr({value:v['id']}).text(v['value']));
                });
                $(td).append(select);
                row.appendChild(td);
                
                var delBtn = $('<button/>').attr({class:'delRow'}).append($('<span></span>').attr({class:'icon fa fa-close'})); 
                delBtn.click(function(){
                    if(getOrderSizes().length == 1) return;
                    $(row).remove();
                });
                $(row).append($('<td></td>').append(delBtn));
                
                $(form).find('#queryTable').append(row);
                framework.attachMethods(row);
            }

            addRow();

            $(form).find('.addRow').click(function() {
                addRow();
            });
            
            
            form.addListener('beforeRequest',function(request){
                  request['order_sizes'] = getOrderSizes();
            });
            
            $(form).find('.check').click(function() {
                if(validateRows()) return;
                form.grid.loadData();
            });
            
            var validateRows = function(){
                var hasErrors = false;
                $(form).find('#queryTable').find('tbody').find('tr').each(function(k, el){
                    var size = $(el).find('.size').get(0);
                    if(!size.getValue()['id']) return;
                    
                    $(['quantity','quantity_type','length']).each(function(k,v){
                      var field = $(el).find('.'+v).get(0);
                      if(empty(field.getValue())) {
                        field.setError('Моля, попълнете стойност');
                        hasErrors = true;
                      }   
                    });
                });
                return hasErrors;
            };
            
        });
JS;
    }
}