<?php

namespace emc\production\orders;

use emc\nomenclatures\productionlines\ProductionLines;
use main\db\DBC;
use main\grid\GridPanelHandler;

/**
 * Class OrderTabHistory
 * @accessLevelDescription Достъп до справка История на промени
 */
class OrderTabHistory extends GridPanelHandler {


    public function __construct() {
        $this->oBase = DBC::$main;

        $this->bUseSQLFilters = false;
        $this->bUseSQLPaging = false;
        $this->bUseSQLSort = false;

        $this->aColumns = array(

            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'item',
                'width' => 110,
                'headerText' => L('Елемент'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),

            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_DATETIME,
                'dataField' => 'updated_time',
                'width' => 110,
                'headerText' => L('Време на промяна'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),

            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'field',
                'width' => 100,
                'headerText' => L('Поле'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'old_value',
                'width' => 150,
                'headerText' => L('Стара стойност'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),

            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'new_value',
                'width' => 150,
                'headerText' => L('Нова стойност'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_HTML,
                'dataField' => 'update_note',
                'toolTipField' => 'update_note_full',
                'width' => 150,
                'headerText' => L('Причина'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),

            $this->newColumnEditedBy()
        );

        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        $aRequest['fields']['id'] = $aInitParams['row']['id'];
        return $this->loadData($aRequest);
    }

    public function loadData($aRequest) {
        return parent::loadData($aRequest);
    }

    protected function getGridData($aRequest, $nTimeFrom = 0, $nTimeTo = 0) {
        return self::getHistoryForOrder($aRequest['fields']['id']);
    }

    public static function getHistoryForOrder($idOrder, $fromTime = 0) {
        $idOrder = intval($idOrder);
        $fromTime -= 30;
        $sizesHistory = DBC::$main->select("
            SELECT 
                pos.id_order_size as _k, 
                pos.id as _k2,
                pos.*,
                CONCAT_WS(' ',pu.fname,pu.mname,pu.lname) AS updated_user,
                pmb.name  AS metal_brand,
                pst.code  AS standard,
                pd.name   AS direction,
                ps.code   AS `size`,
                pa.name   AS application,
                CONCAT_WS(' ',pu.fname,pu.mname,pu.lname) AS updated_user,
                sch.id_line
            FROM production_orders_sizes_history pos
            JOIN production_directions     pd ON pd.id = pos.id_direction
            JOIN production_standards      pst ON pst.id = pos.id_standard
            LEFT JOIN production_metal_brands pmb ON pmb.id = pos.id_metal_brand
            LEFT JOIN production_applications   pa ON pa.id = pos.id_application
            JOIN production_sizes          ps ON ps.id = pos.id_size
            LEFT JOIN personnel pu ON pu.id = pos.updated_user AND pu.to_arc = 0
            LEFT JOIN production_schedule sch ON sch.id_order_size = pos.id_order_size AND sch.to_arc = 0
            
            WHERE 1
            AND pos.id_order = $idOrder
            GROUP BY pos.id_order_size, pos.updated_count
            ORDER BY pos.id_order_size, pos.updated_count
        ", null, 2);

        $orderFields = array(
            'client_order_num' =>
                array(
                    'name' => 'client_order_num',
                    'title' => 'Номер на поръчката при клиента',
                ),
            'status' =>
                array(
                    'name' => 'status',
                    'title' => 'Статус',
                ),
            'region' =>
                array(
                    'name' => 'region',
                    'title' => 'Регион на клиента',
                ),
            'name' =>
                array(
                    'name' => 'name',
                    'title' => 'Име на клиента',
                ),
            'address' =>
                array(
                    'name' => 'address',
                    'title' => 'Адрес на клиента',
                ),
            'city' =>
                array(
                    'name' => 'city',
                    'title' => 'Нас. място на клиента',
                ),
            'country' =>
                array(
                    'name' => 'country',
                    'title' => 'Държава на клиента',
                ),
            'VAT_num' =>
                array(
                    'name' => 'VAT_num',
                    'title' => 'ДДС №',
                ),
            'ein' =>
                array(
                    'name' => 'ein',
                    'title' => 'Булстат/ЕИН/ЕГН',
                ),
            'mol' =>
                array(
                    'name' => 'mol',
                    'title' => 'МОЛ на фирмата',
                ),
            'additional_info' =>
                array(
                    'name' => 'additional_info',
                    'title' => 'Допълнителна информация ',
                ),
            'delivery_deadline' =>
                array(
                    'name' => 'delivery_deadline',
                    'title' => 'Срок на доставка',
                ),
            'method_of_acceptance' =>
                array(
                    'name' => 'method_of_acceptance',
                    'title' => 'Начин на приемане',
                ),
            'letter_info' =>
                array(
                    'name' => 'letter_info',
                    'title' => 'Писмо №:/дата',
                ),
        );


        $orderSizesFields = array(
            'size' =>
                array(
                    'name' => 'id_size',
                    'title' => 'Размер',
                ),
            'standard' =>
                array(
                    'name' => 'id_standard',
                    'title' => 'Стандарт',
                ),
            'direction' =>
                array(
                    'name' => 'id_direction',
                    'title' => 'Направление',
                ),
            'metal_brand' =>
                array(
                    'name' => 'metal_brand',
                    'title' => 'Марка/модел на метала',
                ),
            's' =>
                array(
                    'name' => 's',
                    'title' => 'Договорена дебелина за производство в мм',
                ),
            's_plus' =>
                array(
                    'name' => 's_plus',
                    'title' => 'Допуск в + на дебелина ',
                ),
            's_minus' =>
                array(
                    'name' => 's_minus',
                    'title' => 'Допуск в - на дебелина ',
                ),
            'd' =>
                array(
                    'name' => 'd',
                    'title' => 'Договорен външен диаметър в мм',
                ),
            'd_plus' =>
                array(
                    'name' => 'd_plus',
                    'title' => 'Допуск в + за външен диаметър в мм',
                ),
            'd_minus' =>
                array(
                    'name' => 'd_minus',
                    'title' => 'Допуск в - за външен диаметър в мм',
                ),
            'a' =>
                array(
                    'name' => 'a',
                    'title' => 'Договорена ширина в мм, ако е SQ профил - страна в мм',
                ),
            'a_plus' =>
                array(
                    'name' => 'a_plus',
                    'title' => 'Допуск в + за ширина в мм, ако е SQ профил - страна в мм',
                ),
            'a_minus' =>
                array(
                    'name' => 'a_minus',
                    'title' => 'Допуск в - за ширина в мм, ако е SQ профил - страна в мм',
                ),
            'h' =>
                array(
                    'name' => 'h',
                    'title' => 'Договорена височина в мм',
                ),
            'h_plus' =>
                array(
                    'name' => 'h_plus',
                    'title' => 'Допуск в + за височина в мм',
                ),
            'h_minus' =>
                array(
                    'name' => 'h_minus',
                    'title' => 'Допуск в - за височина в мм',
                ),
            'l' =>
                array(
                    'name' => 'l',
                    'title' => 'Дължина на тръбата/профила в мм',
                ),
            'l_plus' =>
                array(
                    'name' => 'l_plus',
                    'title' => 'Допуск в + за дължина на тръбата/профила в мм',
                ),
            'l_minus' =>
                array(
                    'name' => 'l_minus',
                    'title' => 'Допуск в - за дължина на тръбата/профила в мм',
                ),
            'quantity_type' =>
                array(
                    'name' => 'quantity_type',
                    'title' => 'Тип на количеството (бр,кг,м)',
                ),
            'quantity' =>
                array(
                    'name' => 'quantity',
                    'title' => 'Количество',
                ),
            'quantity_plus' =>
                array(
                    'name' => 'quantity_plus',
                    'title' => 'Допуск в + % за посоченото количество',
                ),
            'quantity_minus' =>
                array(
                    'name' => 'quantity_minus',
                    'title' => 'Допуск в - % за посоченото количество',
                ),
            'total_mass' =>
                array(
                    'name' => 'total_mass',
                    'title' => 'Изчислена обща маса на размера',
                ),
            'quantity_correction' =>
                array(
                    'name' => 'quantity_correction',
                    'title' => '% на корекция приложена върху изчисленото тегло',
                ),
            'special_mechanical_properties' =>
                array(
                    'name' => 'special_mechanical_properties',
                    'title' => 'Специални механични показатели',
                ),
            'special_chemical_properties' =>
                array(
                    'name' => 'special_chemical_properties',
                    'title' => 'Специални механични показатели ',
                ),
            'special_packing' =>
                array(
                    'name' => 'special_packing',
                    'title' => 'Специална опаковка',
                ),
            'special_requirements' =>
                array(
                    'name' => 'special_requirements',
                    'title' => 'Други специални изисквания',
                ),
            'cleaned_ends' =>
                array(
                    'name' => 'cleaned_ends',
                    'title' => 'Зачистване на краища',
                ),
            'inner_cleaned_seam' =>
                array(
                    'name' => 'inner_cleaned_seam',
                    'title' => 'Вътрешно зачистване на шева',
                ),
            'coated_seam' =>
                array(
                    'name' => 'coated_seam',
                    'title' => 'Покрит заваръчен шев',
                ),
            'zinc_galvanize_ability' =>
                array(
                    'name' => 'zinc_galvanize_ability',
                    'title' => 'Годност за горещо поцинковане',
                ),
            'chroming_ability' =>
                array(
                    'name' => 'chroming_ability',
                    'title' => 'Годност за хромиране',
                ),
            'painting_ability' =>
                array(
                    'name' => 'painting_ability',
                    'title' => 'Годност за боядисване',
                ),
            'bending_ability' =>
                array(
                    'name' => 'bending_ability',
                    'title' => 'Годност за огъване',
                ),
            'vortex_currents_test' =>
                array(
                    'name' => 'vortex_currents_test',
                    'title' => 'Проверка с вихрови токове',
                ),
            'pressure_test' =>
                array(
                    'name' => 'pressure_test',
                    'title' => 'Тест под налягане',
                ),
            'article_num' =>
                array(
                    'name' => 'article_num',
                    'title' => 'Артикулен номер',
                ),
            'desired_production_start' =>
                array(
                    'name' => 'desired_production_start',
                    'title' => 'Дата за производство',
                ),
            'desired_production_end' =>
                array(
                    'name' => 'desired_production_end',
                    'title' => 'Желано излизане от производство',
                ),
            'to_arc' =>
                array(
                    'name' => 'to_arc',
                    'title' => 'Изтрит размер от поръчката',
                ),
        );


        $response = [];

        foreach ($sizesHistory as $idSize => $historyRows) {
            $prev = reset($historyRows);
            if($prev['action'] == 'insert') {
                $response[] = [
                    'item' => $prev['size'],
                    'item_type' => 'order_size',
                    'id_order_size' => $prev['id_order_size'],
                    'field' => "Добавен размер в поръчката",
                    'updated_user' => $prev['updated_user'],
                    'updated_time' => $prev['updated_time'],
                ];
            }

            foreach ($historyRows as $historyRow) {
                foreach ($historyRow as $field => $val) {
                    if(!empty($orderSizesFields[$field]) && $val != $prev[$field]) {
                        if(substr($field, 0, 3) == 'id_') continue;

                        $r = [
                            'item' => $historyRow['size'],
                            'item_type' => 'order_size',
                            'id_order_size' => $historyRow['id_order_size'],
                            'id_line' => $historyRow['id_line'],
                            'field' => ($orderSizesFields[$field]['title']),
                            'old_value' => $prev[$field],
                            'new_value' => $val,
                            'update_note' => nl2br($historyRow['update_note']),
                            'updated_count' => $historyRow['updated_count'],
                            'updated_user' => $historyRow['updated_user'],
                            'updated_time' => $historyRow['updated_time'],
                        ];

                        switch ($field) {
                            default:
                                $r['old_value'] = $prev[$field];
                                $r['new_value'] = $val;
                        }
                        $response[] = $r;
                    }
                }
                $prev = $historyRow;
            }
        }

        $orderHistory = DBC::$slave->select("
            SELECT
            pos.*,
            CONCAT_WS(' ',pu.fname,pu.mname,pu.lname) AS updated_user
            FROM production_orders_history pos
            LEFT JOIN personnel pu ON pu.id = pos.updated_user
            WHERE 1
            AND pos.id_order = $idOrder
        ");

        $prev = reset($orderHistory);
        foreach ($orderHistory as $historyRow) {
            foreach ($historyRow as $field => $val) {
                if(!empty($orderFields[$field]) && $val != $prev[$field]) {
                    $response[] = [
                        'item' => $historyRow['num'],
                        'item_type' => 'order',
                        'field' => ($orderFields[$field]['title']),
                        'old_value' => $prev[$field],
                        'new_value' => $val,
                        'update_note' => nl2br($historyRow['update_note']),
                        'updated_count' => $historyRow['updated_count'],
                        'updated_user' => $historyRow['updated_user'],
                        'updated_time' => $historyRow['updated_time'],
                    ];
                }
            }
            $prev = $historyRow;
        }

        usort($response, function ($a, $b) {
            return $a['updated_time'] <=> $b ['updated_time'];
        });

        if(!empty($fromTime)) {
            $response = array_filter($response, function ($row) use ($fromTime) {
                return $row['updated_time'] >= date('Y-m-d H:i:s', $fromTime);
            });
        }

        return $response;
    }

}