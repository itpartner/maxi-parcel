<?php
namespace emc\production\orders;

use main\Access;
use main\TPLBase;

class TPLOrderTabInfo extends TPLBase {
    public function __construct($sPrefix, $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);
    }

    protected function printStyles($sPrefix) { ?>
        <style>
            #<?=$sPrefix?> .confirmed {
                display: block;
                height: 20px;
            }
            #<?=$sPrefix?> .width94 {
                width: 94%;
            }
            #<?=$sPrefix?> .confirm {
                width: 100px;
                float: left;
            }
            #<?=$sPrefix?> .cancel {
                width: 100px;
                float: right;
            }
            #<?=$sPrefix?> .width97 {
                width: 97%;
            }
            #<?=$sPrefix?> .statusField {
                width: 97%;
                font-size: 17px !important;
                font-weight: bold;
                text-align: center;
                opacity: 0.8 !important;
                background: lightgray !important;
            }
            #<?=$sPrefix?> fieldset td {
                padding: 1px;
            }
            #<?=$sPrefix?> .required-field {
                background-color: #C7DDE6 !important;
            }
        </style>

        <?php
    }

    public function printHtml() { ?>
        <table style="width: 100%">
            <tr>
                <td style="vertical-align: top">
                    <table style="width: 100%;">
                        <tr>
                            <td><? $this->printLabel(L('Статус:'), 'status') ?></td>
                            <td colspan="3"><? $this->printElement('input', array('name' => 'status', 'class' => 'statusField', 'disabled' => 'disabled')) ?></td>
                        </tr>
                        <tr>
                            <td><? $this->printLabel(L('Номер:'), 'num') ?></td>
                            <td><? $this->printElement('input', array('name' => 'num', 'class' => 'width94', 'disabled' => 'disabled')) ?></td>
                            <td>/</td>
                            <td><? $this->printElement('input', array('name' => 'created_time', 'class' => 'width94', 'disabled' => 'disabled')) ?></td>
                        </tr>
                        <tr>
                            <td colspan="3"><? $this->printElement('label', array('fieldName' => 'client_client_num', 'text' => L('Номер от клиента:'))) ?></td>
                            <td><? $this->printElement('input', array('name' => 'client_order_num', 'class' => 'width94', 'title' => 'Номер от регистъра на клиента')) ?></td>
                        </tr>
                        <tr>
                            <td colspan="4" style="padding: 8px 0">
                                <? $this->printElement('button', array('text' => L('Потвърди'), 'class' => 'confirm', 'iconCls' => 'fa fa-check fa-fw')); ?>
                                <? $this->printElement('button', array('text' => L('Откажи'), 'class' => 'cancel', 'iconCls' => 'fa fa-ban fa-fw')); ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4"><? $this->printLabel(L('Допълнителна информация:'), 'additional_info') ?>
                            <? $this->printElement('textarea', array('name' => 'additional_info', 'style' => 'width: 98%; height: 79px;', 'maxlength' => '2048')) ?></td>
                        </tr>
                        <tr>
                            <td colspan="3"><? $this->printElement('label', array('fieldName' => 'delivery_deadline', 'text' => L('Срок на доставка:'))) ?></td>
                            <td><? $this->printElement('input', array('name' => 'delivery_deadline', 'class' => 'width94')) ?></td>
                        </tr>
                        <tr>
                            <td colspan="3"><? $this->printLabel(L('Начин на приемане:'), 'method_of_acceptance'); ?></td>
                            <td><? $this->printElement('input', array('class' => 'width94', 'name' => 'method_of_acceptance')); ?></td>
                        </tr>
                        <tr>
                            <td colspan="3"><? $this->printLabel(L('Писмо №:/дата:'), 'letter_info'); ?></td>
                            <td><? $this->printElement('input', array('class' => 'width94', 'name' => 'letter_info')); ?></td>
                        </tr>
                    </table>
                </td>
                <td style="vertical-align: top">
                    <fieldset>
                        <legend><?=L('Клиент')?></legend>
                        <table style="width: 100%;">
                            <tr>
                                <td><? $this->printLabel(L('Име:'), 'name');?></td>
                                <td><? $this->printElement('input', array('class' => 'width97', 'name' => 'name', 'suggest' => 'method: emc\nomenclatures\clients\Clients.suggestClient;')); ?></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <? $this->printElement('input', array('type' => 'radio', 'checked' => 'checked', 'value' => 'BG', 'name' => 'region', 'text' => L("България"))) ?>
                                    <? $this->printElement('input', array('type' => 'radio', 'name' => 'region', 'value' => 'EU', 'text' => L("Европейски съюз"))) ?>
                                    <? $this->printElement('input', array('type' => 'radio', 'name' => 'region', 'value' => 'NON-EU', 'text' => L("Извън ЕС"))) ?>
                                </td>
                            </tr>
                            <tr>
                                <td><? $this->printLabel(L('Адрес:'), 'address'); ?></td>
                                <td><? $this->printElement('input', array('class' => 'width97', 'name' => 'address')); ?></td>
                            </tr>
                            <tr>
                                <td><? $this->printLabel(L('Град:'), 'city'); ?></td>
                                <td><? $this->printElement('input', array('class' => 'width97', 'name' => 'city')); ?></td>
                            </tr>
                            <tr>
                                <td><? $this->printLabel(L('Държава:'), 'country'); ?></td>
                                <td><? $this->printElement('input', array('class' => 'width97', 'name' => 'country')); ?></td>
                            </tr>
                            <tr>
                                <td><? $this->printLabel(L('ДДС №:'), 'VAT_num'); ?></td>
                                <td><? $this->printElement('input', array('class' => 'width97', 'name' => 'VAT_num')); ?></td>
                            </tr>
                            <tr>
                                <td><? $this->printLabel(L('Булстат №:'), 'ein'); ?></td>
                                <td><? $this->printElement('input', array('class' => 'width97', 'name' => 'ein', 'suggest' => 'method: emc\nomenclatures\clients\Clients.suggestEIN;')); ?></td>
                            </tr>
                            <tr>
                                <td><? $this->printLabel(L('МОЛ:'), 'mol'); ?></td>
                                <td><? $this->printElement('input', array('class' => 'width97', 'name' => 'mol')); ?></td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <span name="confirmed" class="confirmed">&nbsp;</span>
                </td>
            </tr>
            <tr class="reviewed">
                <td colspan="2">
                    <span name="reviewed">&nbsp</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="toolbar align-right" style="margin-top: 5px">
                        <? $this->printElement('lasteditedinfo'); ?>
                        <? $this->printElement('button', array('text' => L('Печат'), 'class' => 'print align-left', 'iconCls' => 'fa fa-print fa-fw')); ?>
                        <? $this->printElement('button', array('text' => L('Клонирай'), 'class' => 'clone align-left', 'iconCls' => 'fa fa-clone fa-fw')); ?>

                        <? $this->printElement('closebutton'); ?>
                        <?$this->printElement('button', array('text' => L('Запази'), 'class' => 'save align-right', 'iconCls' => 'fa fa-check fa-fw')); ?>
                    </div>
                </td>
            </tr>
        </table>
        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, baseParams) {
            var form = container;
            var tabsContainer = baseParams['getTabsContainer'] ? baseParams['getTabsContainer']() : null;
            var order = baseParams['row'];
             
            form.addListener('beforeRequest', function(request){
                request['order'] = order;
            });

            var requiredFields = []; // region => [fields]
            var highlightRequiredFields = function(requiredFields) {
                for (var i in form.fields) {
                    $(form.fields[i]).removeClass('required-field');
                }
                for (var i in requiredFields) {
                    $(form).find('[name=' + requiredFields[i] + ']').addClass('required-field');
                }
            }

            setTimeout(function() {
                var region = form.fields.region.getValue();
                if (region == 'BG') form.fields.country.value = L('България');
                highlightRequiredFields(requiredFields[region]);
            }, 20);

            form.addListener('afterRequest', function(response){
                order = response['order'];
                if(order){
                    var status = order['status'];
                    if(status == 'confirmed') {
                        var confirmBtn = $(form).find('button.confirm');
                        $(form).find('button.cancel').removeAttr('disabled');
                        confirmBtn.attr('disabled','disabled');
                        if(intval(order['needs_review'])) {
                            confirmBtn.attr('disabled', '').find('span').text('Потвърди корекциите')
                            confirmBtn.css('width', '190px');
                        }
                    } else if(status == 'cancelled') {
                        $(form).find('button.confirm').removeAttr('disabled');
                        $(form).find('button.cancel').attr('disabled','disabled');
                    }
                    if(!order['id']) $(form).find('button.confirm, button.cancel, button.print, button.clone').attr('disabled','disabled');
                }
                if(tabsContainer) {
                    var tabSizes = $(tabsContainer).find('div[name="emc/production/orders/OrderTabSizes"]').find('.grid-container')[0];
                    if(tabSizes) tabSizes.loadData();
                }

                if (response.aRequiredFields) {
                    requiredFields = response.aRequiredFields;
                }
            });

            form.fields.name.addListener('suggestSelect', function(client) {
                $.each(client, function(k, v){
                    if(!form.fields[k]) return;
                    form.fields[k].setValue(v);
                });
            })

            form.fields.region.addListener('change', function(region) {
                if(region == 'BG') form.fields.country.value = L('България');
                else form.fields.country.value = '';

                highlightRequiredFields(requiredFields[region]);
            });

            $(form).find('.confirm').click(function() {
                form.request('confirm', {}, null, function () {
                    if(baseParams['onOrderChange']) baseParams['onOrderChange']();
                });
            });

            $(form).find('.cancel').click(function() {
                form.request('cancel', {}, null, function () {
                    if(baseParams['onOrderChange']) baseParams['onOrderChange']();
                });
            });

            $(form).find('.close').click(function() {
                if(baseParams['onClose']) baseParams['onClose'](); 
                if(form.window) form.window.close();
            });

            $(form).find('.clone').click(function() {
                if(!intval(order['id'])) {
                    framework.alert("Моля, запишете заявката, преди да клонирате.");
                    return;
                }
                clonedOrder = JSON.parse(JSON.stringify(order));
                clonedOrder['parent_id'] = clonedOrder['id'];
                clonedOrder['id'] = 0;
                framework.createWindow({
                    tplName: 'emc/production/orders/OrderTabInfo',
                    baseParams: {
                        row: clonedOrder,
                        onOrderChange: function(){if(baseParams['onOrderChange'])baseParams['onOrderChange']();},
                        onClose: function(){if(baseParams['onClose'])baseParams['onClose']();},
                    }
                });
            });

            $(form).find('.print').click(function() {
                window.open(BASE_URL + '/export.php?exportMethod=emc/production/orders/Export.pdf&order_num=' + form.fields.num.value);
            });

            $(form).find('.save').click(function() {
                form.request('save', {}, null, function (resData) {
                    if(baseParams['onOrderChange']) baseParams['onOrderChange']();
                        
                    if(!tabsContainer) {
                        form.window.close();
                        framework.createWindow({
                            tplName: 'emc/production/orders/Order',
                            baseParams: {
                                row: resData['order'],
                                getParentGrid: function() { return grid; },
                                refreshParentGrid: function() { return baseParams.refreshParentGrid();},
                                onOrderChange: function(){if(baseParams['onOrderChange']) baseParams['onOrderChange']();},
                                onClose: function(){if(baseParams['onClose']) baseParams['onClose']();},
                            }
                        });
                    }
                });
            });
        });
JS;
    }
}