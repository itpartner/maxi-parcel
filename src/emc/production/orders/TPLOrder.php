<?php
namespace emc\production\orders;

use main\TPLBase;

class TPLOrder extends TPLBase {
    public function __construct($sPrefix, array $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);
        $this->aFrameConfig['title'] = empty($aInitParams['row']['id']) ? L('Нова заявка') : L('Редакция на заявка: ') . $aInitParams['row']['num'];
    }


    public function printHtml() {
        $aTabs = array(
            array('tplName' => 'emc/production/orders/OrderTabInfo', 'title' => L('Информация'), 'tooltip' => L('Информация')),
            array('tplName' => 'emc/production/orders/OrderTabSizes', 'title' => L('Размери'), 'tooltip' => L('Размери')),
            array('tplName' => 'emc/production/orders/OrderTabHistory', 'title' => L('История'), 'tooltip' => L('История на промените'))
        );
        ?>

        <style>
            #<?=$this->sPrefix?> .tab {
                width: 900px;
                height: 400px;
            }
        </style>
        <div id="<?= $this->sPrefix ?>">
            <div id="<?= $this->sPrefix ?>-tabs">
                <ul>
                    <?php $i = 0;
                    foreach ($aTabs as $aTab): ?>
                        <li tooltip="<?= htmlspecialchars($aTab['tooltip']) ?>">
                            <a href="#<?= $this->sPrefix ?>-tab-<?= $i ?>">
                                <?= $aTab['title']; ?>
                            </a>
                        </li>
                        <?php $i++; endforeach; ?>
                </ul>
                <?php $i = 0;
                foreach ($aTabs as $aTab): ?>
                    <div class="tab" id="<?= $this->sPrefix ?>-tab-<?= $i ?>" name="<?= $aTab['tplName'] ?>"></div>
                    <?php $i++; endforeach; ?>
            </div>
        </div>

        <?php
    }

    public function getJSFunction() {
        return <<<'JS'
        (function (container, prefix, initParams) {
            var form = $(container);
    
            initParams.closeTabs = function() {
                container.window.close();
            };
            initParams['getTabsContainer'] = function(){return container}; 
            initParams['onClose'] = function(){initParams.closeTabs();}; 
            
            //при показване на таб се активира събитие activate, което може да се проследява (слуша) от JS на самият таб
            var initTabs = function() {
                jQuery('#' + prefix + '-tabs').bind('tabsshow', function(e,ui) {
                    if(!ui.panel.innerHTML) {
                        layout.loadContent({
                                tplName:ui.panel.getAttribute('name'),
                                baseParams:initParams
                            },
                            ui.panel
                        );
                    } else {
                        jQuery(ui.panel).children('.content-wrapper').trigger('tab_activate');
                    }
            
                });
            }();
            jQuery('#' + prefix + '-tabs').tabs({selected:initParams.selectedTab ? jQuery('#'+prefix+'-tabs').children('.tab').index(jQuery('#'+prefix+'-tabs .tab[name='+initParams.selectedTab+']')) : 0});
        });
JS;
    }
}