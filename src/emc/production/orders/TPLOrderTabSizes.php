<?php
namespace emc\production\orders;

use emc\access\profiles\ProfileMarketingEmployee;
use main\Access;
use main\grid\TPLGridPanel;

class TPLOrderTabSizes extends TPLGridPanel {
    public function __construct($sPrefix, $aInitParams) {
        $this->{'bNoDefaultTopBarItems'} = true;

        $this->{'aConfig'} = array(
            'dataHeight' => '295px',
            'selectionMode' => 'checkbox',
            'hideRowIndexes' => true,
            'disablePaging' => true,
            'bottomBarItems' => array(
                array('closebutton')
            )
        );

        if(Access::hasAccess(ProfileMarketingEmployee::class)) $this->{'aConfig'}['topBarItems'] = [
            array('button', array('name' => 'remove', 'iconCls' => 'icon fa fa-remove', 'text' => L('Премахни'))),
            array('button', array('name' => 'new', 'class' => 'align-right', 'iconCls' => 'icon fa fa-plus-square', 'text' => L('Нов')))
        ];

        parent::__construct($sPrefix, $aInitParams);
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, baseParams) {
            var form = container;
            var grid = container.grid;

            form.addListener('beforeRequest', function(request) {
                request['fields']['id'] = baseParams.row.id;
                return request;
            });

            var openEditWnd = function(id) {
                framework.createWindow({
                    tplName: 'emc/production/orders/sizes/Size',
                    baseParams: {
                        id: id,
                        id_order: baseParams.row.id,
                        onSizeChange: function(){ 
                            grid.loadData();
                            if(baseParams['onOrderChange']) baseParams['onOrderChange']();
                        },
                    }
                });
            };

            $(form).find("button[name=remove]").click(function() {
                var selectedData = form.grid.getSelectedRows();
                
                if(!selectedData.length) {
                    framework.alert(L('Не са избрани редове.'), 'error');
                    return;
                }
                
                framework.confirm('', L('Изтриване на избраните редове?'), function(sure) {
                    if(!sure) return;
                    form.request('removeSelectedRows', { ids: form.grid.getSelectedRows().map(function(r){return r['id'];}) },null,function(){
                        if(baseParams['onOrderChange']) baseParams['onOrderChange']();
                    });
                });
            });

            $(form).find('.close').click(function() {
                baseParams.closeTabs();
            });

            form.grid.clickListeners['code'] = function(rowIndex, rowData) {
                openEditWnd(rowData.id);
            };

            $(form).find('button[name=new]').click(function() {
                openEditWnd(0);
            });
        });
JS;

    }
}