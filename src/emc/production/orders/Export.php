<?php
namespace emc\production\orders;

use main\db\DBC;
use main\InvalidParameter;
use main\Util;

require_once(BASE_PATH . '/src/tcpdf/tcpdf.php');
/** @noinspection PhpUndefinedClassInspection */

/**
 * Class PDFExport
 * @package emc\production\orders
 */
class Export extends \TCPDF {

    public function Header() {

    }

    public function Footer() {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('dejavusans', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Стр. ' . $this->PageNo(), 0, 0, 'C');
    }

    private function getData($orderNum) {
        $orderNum = DBC::$main->quote($orderNum);

        $order = DBC::$main->selectRow("
          SELECT
            po.*,
            CONCAT_WS(' ', p.fname, p.lname) AS confirmed_username,
            CONCAT_WS(' ', p2.fname, p2.lname) AS created_username,
            ''
          FROM production_orders po
          LEFT JOIN personnel p ON p.id = po.confirmed_user
          LEFT JOIN personnel p2 ON p2.id = po.created_user
          WHERE po.to_arc = 0
          AND po.num = $orderNum
        ");

        if(is_null($order['id'])) throw new InvalidParameter;
        $orderSizes = DBC::$main->select("
          SELECT
            pos.*,
            if(s.d, 1, 0) AS has_diameter,
            s.code,
            ps.code AS standard,
            pd.name AS direction,
            pa.name AS application,
            pmb.name AS metal_brand,
            ''
          FROM production_orders_sizes pos
          JOIN production_sizes s ON s.id = pos.id_size AND s.to_arc = 0
          LEFT JOIN production_standards ps ON ps.id = pos.id_standard AND ps.to_arc = 0
          LEFT JOIN production_directions pd ON pd.id = pos.id_direction AND pd.to_arc = 0
          LEFT JOIN production_applications pa ON pa.id = pos.id_application AND pa.to_arc = 0
          LEFT JOIN production_metal_brands pmb ON pmb.id = pos.id_metal_brand AND pmb.to_arc = 0
          WHERE 1
          AND pos.to_arc = 0
          AND pos.id_order = {$order['id']}
          GROUP BY pos.id
        ");

        $order['sizes'] = $orderSizes;

        $historyRows = [];
        $orderConfirmTimestamp = Util::SQLDateToTimeStamp($order['confirmed_time']);
        if($orderConfirmTimestamp) {
            foreach(OrderTabHistory::getHistoryForOrder($order['id'], $orderConfirmTimestamp) as $historyRow) {
                $historyRows[$historyRow['updated_time'].'_'.$historyRow['item']][] = $historyRow;
            }
        }

        $order['history'] = $historyRows;
        return $order;
    }

    public function pdf($request) {
        ob_start();
        $this->generateHtml($request);
        $html = ob_get_clean();

        $this->SetFont('dejavusans', '', 8);
        $this->setListIndentWidth(4);

        $this->AddPage();
        $this->writeHTML($html);
        ob_get_clean();
        $this->Output(DBC::$main->quote($request['order_num']) . '.pdf', 'I');
        die();
    }

    public function html($request) {
        ob_start();
        $this->generateHtml($request);
        $html = ob_get_clean();

        die($html);
    }

    private function getToleranceValue($nValue, $bIsPositive = true) {
        if(is_null($nValue)) return 'EN';
        if(empty($nValue)) return 0;
        else return !$bIsPositive ? '-' . $nValue : '+' . $nValue;
    }

    public function generateHtml($request) {
        $order = array_map(function ($v) {
            return is_string($v) ? htmlspecialchars($v) : $v;
        }, $this->getData($request['order_num']));
        $BG = $NOTBG = '&nbsp;';
        $order['region'] == 'BG' ? $BG = '&#10004;' : $NOTBG = '&#10004;';

        $emptyCells = '
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        ';

        $aAdditionalProperties = [
            'cleaned_ends' => 'Зачистване на краища',
            'inner_cleaned_seam' => 'Вътрешно зачистване на шева',
            'coated_seam' => 'Покрит заваръчен шев',
            'zinc_galvanize_ability' => 'Годност на горещо поцинковане',
            'chroming_ability' => 'Годност за хромиране',
            'painting_ability' => 'Годност за боядисване',
            'bending_ability' => 'Годност за огъване',
            'vortex_currents_test' => 'Проверка с вихрови токове',
            'pressure_test' => 'Тест под налягане'
        ];
        $aAdditionalSpecificProperties = [
            'special_mechanical_properties' => 'Специални механични показатели',
            'special_chemical_properties' => 'Специални химически показатели',
            'special_packing' => 'Специална опаковка',
            'special_requirements' => 'Други специални изисквания, чертеж №'
        ];
        $aQuantityType = [
            'count' => 'броя',
            'mass' => 'kg',
            'length' => 'метра',
        ];
        ?>
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <style type="text/css">
                table:not(#direction) {
                    page-break-inside: inherit;
                    width: 100%;
                    border-collapse: collapse;
                    border: 0.5pt solid black;
                }

                table#content, table#content > tr > td {
                    border: none;
                }

                td, th {
                    border: 0.5pt solid black;
                    height: 16px;
                }

                th {
                    font-weight: bold;
                }

                table#direction td {
                    border: none;
                }

                tr {
                    page-break-after: inherit;
                }
            </style>
        </head>
        <body>

        <table cellpadding="3" align="center" width="100%" border="1pt" id="header">
            <tr>
                <td><!--suppress CheckImageSize -->
                    <img src="images/emc-logo.png" width="120" height="41"></td>
                <td colspan="2" style="font-size: 13px; font-weight: bold; line-height: 41px">ЗАЯВКА ЗА ПРОИЗВОДСТВО</td>
            </tr>
            <tr style="font-weight: bold;">
                <td>БДС EN ISO 9001:2015 <br/> IATF 16949:2016</td>
                <td style="line-height: 20px">ДОКУМЕНТ</td>
                <td style="line-height: 20px">ДК-04-01_01</td>
            </tr>
        </table>

        <table id="content" cellpadding="5" border="0">
            <tr>
                <td>
                    <p align="center" style="font-size:10px; font-weight: bold;"><?= '№: ' . $order['num'] . '/' . date("d.m.Y", strtotime($order['created_time'])) . ' година' ?></p>
                    <p><?= 'КЛИЕНТ: ' . $order['name'] ?></p>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="3" width="100%">
                        <tr>
                            <th width="23%">начин на приемане</th>
                            <th width="12%">тел.</th>
                            <th width="25%">писмо №:/дата</th>
                            <th width="14%">факс №:/дата</th>
                            <th width="14%">дог. №:/дата</th>
                            <th width="12%">на място</th>
                        </tr>
                        <tr>
                            <td><?=$order['method_of_acceptance']?></td>
                            <td>&nbsp;</td>
                            <td><?=$order['letter_info']?></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <th width="23%">Поръчка на клиента №</th>
                            <td><?= $order['client_order_num']; ?></td>
                        </tr>
                    </table>

                    <div></div>
                    <table id="direction" cellspacing="3">
                        <tr width="100%">
                            <td style="line-height: 15px" width="15%">НАПРАВЛЕНИЕ:</td>
                            <td style="line-height: 15px" align="right" width="20%">ВЪТРЕШЕН ПАЗАР</td>
                            <td style="line-height: 10px" align="left" width="10%">
                                <table style="height: 15px; width: 15px; font-size: 13px; text-align: center;">
                                    <tr>
                                        <td><?= $BG ?></td>
                                    </tr>
                                </table>
                            </td>
                            <td style="line-height: 15px" align="right" width="10%">ИЗНОС</td>
                            <td style="line-height: 10px" align="left" width="50%">
                                <table style="height: 15px; width: 15px; font-size: 13px; text-align: center;">
                                    <tr>
                                        <td><?= $NOTBG ?></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div></div>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" cellpadding="3">
                        <tbody>
                        <tr>
                            <th width="21%">Размери на изделието (mm)</th>
                            <th width="10%">Дължина (mm)</th>
                            <th width="12%">Количество</th>
                            <th width="12%">Направление</th>
                            <th width="10%">Стандарт</th>
                            <th width="15%">Материал (марка, вид, ишлеме)</th>
                            <th width="14%">Артикулен №</th>
                            <th width="6%">Дата на пр-во</th>
                        </tr>
                        <?php foreach ($order['sizes'] as $size):
                            $size = array_map(function ($v) {
                                return is_string($v) ? htmlspecialchars($v) : $v;
                            }, $size);

                            $sSizesCell = $size['code'] . ' (' . $size['s'] . ')';
                            if(empty($size['has_diameter'])) {
                                $sSizesCell .= "<br>";
                                $aMinus = $this->getToleranceValue($size['a_minus'], false);
                                $aPlus = $this->getToleranceValue($size['a_plus']);
                                $hMinus = $this->getToleranceValue($size['h_minus'], false);
                                $hPlus = $this->getToleranceValue($size['h_plus']);
                                $sMinus = $this->getToleranceValue($size['s_minus'], false);
                                $sPlus = $this->getToleranceValue($size['s_plus']);
                                if(!($aMinus === 0 && $aPlus === 0))
                                    $sSizesCell .= '(' . $aMinus . '/' . $aPlus . ')';
                                if(!($hMinus === 0 && $hPlus === 0))
                                    $sSizesCell .= '(' . $hMinus . '/' . $hPlus . ')';
                                if(!($sMinus === 0 && $sPlus === 0))
                                    $sSizesCell .= '(' . $sMinus . '/' . $sPlus . ')';
                            }
                            else {
                                $sSizesCell .= "<br>";
                                $dMinus = $this->getToleranceValue($size['d_minus'], false);
                                $dPlus = $this->getToleranceValue($size['d_plus']);
                                $sMinus = $this->getToleranceValue($size['s_minus'], false);
                                $sPlus = $this->getToleranceValue($size['s_plus']);
                                if(!($dMinus === 0 && $dPlus === 0))
                                    $sSizesCell .= '(' . $dMinus . '/' . $dPlus . ')';
                                if(!($sMinus === 0 && $sPlus === 0))
                                    $sSizesCell .= '(' . $sMinus . '/' . $sPlus . ')';
                            }

                            $sLengthCell = $size['l'];
                            $sLengthCell .= "<br>";

                            $lMinus = $this->getToleranceValue($size['l_minus'], false);
                            $lPlus = $this->getToleranceValue($size['l_plus']);
                            if(!($lMinus === 0 && $lPlus === 0))
                                $sLengthCell .= '(' . $lMinus . '/' . $lPlus . ')';

                            if($size['quantity_type'] != 'mass') {
                                $sQuantityCell = $size['quantity'] . ' ' . $aQuantityType[$size['quantity_type']] . ' = ' . intval($size['total_mass']) . ' ' . 'kg';
                            } else {
                                $sQuantityCell = intval($size['total_mass']) . ' ' . 'kg';
                            }
                            $sQuantityCell .= "<br>";
                            $qMinus = $this->getToleranceValue($size['quantity_minus'], false);
                            $qPlus = $this->getToleranceValue($size['quantity_plus']);
                            if(!($qMinus === 0 && $qPlus === 0)) $sQuantityCell .= '(' . $qMinus . '% / ' . $qPlus . '%)';

                            ?>
                            <tr>
                                <td style="font-weight: bold"><?= $sSizesCell ?></td>
                                <td style="font-weight: bold"><?= $sLengthCell; ?></td>
                                <td><?= $sQuantityCell; ?></td>
                                <td><?= $size['direction']; ?></td>
                                <td><?= $size['standard']; ?></td>
                                <td><?= $size['metal_brand']; ?></td>
                                <td><?= $size['article_num']; ?></td>
                                <td><?= !(empty(strtotime($size['desired_production_start']))) ? nl2br(date("d.m\nY", strtotime($size['desired_production_start']))) : ''; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>

                    <p><?= !empty($order['additional_info']) ? 'Допълнителна информация за заявката: ' . nl2br($order['additional_info']) : '' ?></p>

                    <table width="100%" cellpadding="3">
                        <tbody>
                        <?php foreach ($order['sizes'] as $size):
                            $size = array_map(function ($v) {
                                return is_string($v) ? htmlspecialchars($v) : $v;
                            }, $size);
                            if(1
                                && !array_sum(array_map(function ($key) use ($size) {
                                    return !empty($size[$key]);
                                }, array_keys($aAdditionalProperties)))
                                && !array_sum(array_map(function ($key) use ($size) {
                                    return !empty($size[$key]);
                                }, array_keys($aAdditionalSpecificProperties)))
                            ) continue;

                            if($size['quantity_type'] != 'mass') {
                                $sQuantityCell = $size['quantity'] . ' ' . $aQuantityType[$size['quantity_type']] . ' = ' . intval($size['total_mass']) . ' ' . 'kg';
                            } else {
                                $sQuantityCell = intval($size['total_mass']) . ' ' . 'kg';
                            }

                            ?>
                            <tr>
                                <td width="25%" style="font-weight: bold"><?= $size['code']; ?></td>
                                <td width="12%" style="font-weight: bold"><?= $size['l']; ?></td>
                                <td width="20%"><?= $sQuantityCell ?></td>
                                <td width="18%"><?= $size['metal_brand']; ?></td>
                                <td width="25%"></td>
                            </tr>
                            <tr>
                                <td colspan="5">
                                    <ul>
                                        <?= !empty($size['application']) ? '<li>Ще се използва за: <span>' . $size['application'] . '</span></li>' : '' ?>
                                        <?
                                        foreach ($aAdditionalProperties as $key => $prop) {
                                            if(!empty($size[$key])) echo '<li>' . $prop . '</li>';
                                        }
                                        ?>
                                    </ul>
                                    <ul>
                                        <?
                                        foreach ($aAdditionalSpecificProperties as $key => $sProp) {
                                            if(!empty($size[$key])) echo '<li>' . $sProp . ':<br><span>' . $size[$key] . '</span></li>';
                                        }
                                        ?>
                                    </ul>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>

                    <div></div>

                    <table width="100%" cellpadding="3" border="0">
                        <tr>
                            <td align="left" width="40%" style="border: none;">СРОК ЗА ТОВАРЕНЕ:&nbsp;<?= $order['delivery_deadline']; ?></td>
                            <td align="right" width="60%" style="border: none;">ПРИЕЛ ЗАЯВКАТА: <?= $order['created_username'] ?> ............ /име, подпис/</td>
                        </tr>
                    </table>
                    <p align="right">УТВЪРДИЛ ЗАЯВКАТА:
                        <?=
                        $order['confirmed_time'] == '0000-00-00 00:00:00' ?
                            '..............................................' :
                            (date("d.m.Y", strtotime($order['confirmed_time'])) . ' ' . $order['confirmed_username'] . " ............");
                        ?> /дата, име, подпис/
                    </p>

                    <div></div>

                    <hr style="height: 2pt;">
                    <p>НАСТЪПИЛИ ПРОМЕНИ ПО РЕШЕНИЕ НА КЛИЕНТА</p>
                    <table cellpadding="3">
                        <tr>
                            <th width="20%">Размер / Поръчка</th>
                            <th width="30%">Настъпили промени</th>
                            <th width="35%">Причина</th>
                            <th width="15%">Дата</th>
                        </tr>

                        <?php
                            foreach($order['history'] as $k => $rows) {
                                $sizeCode = reset($rows)['item'];
                                $date = reset($rows)['updated_time'];
                                $note = reset($rows)['update_note'];
                                ?>
                                <tr>
                                    <td><?=$sizeCode;?></td>
                                    <td>
                                        <ul>
                                            <?php foreach($rows as $row) {
                                                if(isset($row['old_value']) && isset($row['new_value'])) {
                                                    echo "<li>{$row['field']} (\"{$row['old_value']}\"→\"{$row['new_value']}\")</li>";
                                                } else {
                                                    echo "<li>{$row['field']}</li>";
                                                }
                                            }?>
                                        </ul>
                                    </td>
                                    <td><?=$note;?></td>
                                    <td><?=Util::formatTime(Util::SQLDateToTimeStamp($date), 'long');?></td>
                                </tr>
                                <?php
                            }
                        ?>
                    </table>
                    <br>

                    <p>ПРИЕЛ ПРОМЕНИТЕ: .............................................. /дата, име, подпис/</p>
                    <p>УТВЪРДИЛ ПРОМЕНИТЕ: .............................................. /дата, име, подпис/</p>
                </td>
            </tr>
        </table>
        </body>
        </html>
        <?php
    }
}