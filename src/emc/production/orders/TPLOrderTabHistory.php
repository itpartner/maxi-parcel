<?php

namespace emc\production\orders;

use main\grid\TPLGridPanel;

class TPLOrderTabHistory extends TPLGridPanel {
    public function __construct($sPrefix, $aInitParams = array()) {

        $this->{'bNoDefaultTopBarItems'} = true;

        $this->{'aConfig'} = array(
            'title' => L('История на промени'),
            'dataHeight' => '320px',
            'autoVResize' => true,
            'hideRowIndexes' => false,
            'disablePaging' => false,
        );
        $this->{'aConfig'}['topBarItems'] = [array('span')];

        parent::__construct($sPrefix, $aInitParams);
    }

    public function printStyles($sPrefix) { ?>
        <!--suppress CssUnusedSymbol -->
        <style type="text/css">
        </style>
        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, baseParams) {
            var form = container;
            var grid = container.grid;
 
            $(form).parent('div.content-wrapper').bind('tab_activate', function(e,ui) {
                form.loadData();
            });
            
            form.addListener('tab_activate', function(){
                form.loadData();
            });
            
            form.addListener('beforeRequest', function(request) {
                request['fields']['id'] = baseParams.row.id;
                return request;
            });
            
            var openEditWnd = function(id) {
             framework.createWindow({
                tplName: 'emc/production/orders/sizes/Size',
                    baseParams: {
                        id: id,
                        id_order: baseParams.row.id,
                        onSizeChange: function(){ 
                            grid.loadData();
                            if(baseParams['onOrderChange']) baseParams['onOrderChange']();
                        },
                    }
                });
            };
               
             form.grid.clickListeners['item'] = function(rowIndex, rowData) {
                if(rowData['item_type'] == 'order_size') {
                    openEditWnd(rowData['id_order_size']);
                    return;
                } else {
                    framework.createWindow({
                        tplName: 'emc/production/orders/Order',
                        baseParams: {
                            row: baseParams.row,
                            onOrderChange: function(){ grid.loadData(); },
                        }
                    });
                }
            };
            
        });
JS;
    }
}