<?php
namespace emc\production\orders;

use main\db\DBC;
use main\grid\GridPanelHandler;

/**
 * Class Order
 * @accessLevelDescription Достъп до справка Заявки
 */
class Orders extends GridPanelHandler {

    public function __construct() {
        $this->oBase = DBC::$main;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'num',
                'width' => 100,
                'headerText' => L('№'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_DATE,
                'dataField' => 'po_created_time',
                'width' => 150,
                'headerText' => L('Дата на завеждане'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'row_status',
                'width' => 100,
                'headerText' => L('Статус'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true,
                'style' => 'text-align:center',
                'items' => array(
                    'waiting' => array('text' => L('Изчаква'), 'iconCls' => 'fa fa-hourglass-2 fa-fw'),
                    'confirmed' => array('text' => L('Потвърдена'), 'iconCls' => 'fa fa-check-circle fa-fw'),
                    'production' => array('text' => L('В производство'), 'iconCls' => 'fa fa-refresh fa-fw'),
                    'finished' => array('text' => L('Приключена'), 'iconCls' => 'fa fa-cubes fa-fw'),
                    'cancelled' => array('text' => L('Отказана'), 'iconCls' => 'fa fa-ban fa-fw'),
                    'needs_review' => array('text' => L('Необходим е преглед и потвърждаване'), 'iconCls' => 'fa fa-exclamation fa-fw')
                )
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'marker',
                'width' => 15,
                'headerText' => L('Забавена'),
                'sortable' => false,
                'resizable' => true,
                'filterable' => true,
                'style' => 'text-align:center',
                'items' => array(
                    '' => array('text' => L('Не забавена'), 'iconCls' => 'fa'),
                    'delayed' => array('text' => L('Забавена'), 'iconCls' => 'fa fa-clock-o fa-fw fa-red'),
                )
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'name',
                'width' => 150,
                'headerText' => L('Клиент'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'standard',
                'width' => 100,
                'headerText' => L('Стандарт'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => 'group',
                'headerText' => L('Производствен размер'),
                'children' => array(
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_STRING,
                        'dataField' => 'size_code',
                        'width' => 100,
                        'headerText' => L('Код'),
                        'sortable' => true,
                        'resizable' => true,
                        'filterable' => true
                    )),
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_FLOAT,
                        'dataField' => 'l',
                        'width' => 150,
                        'headerText' => L('Дължина на тръбата'),
                        'sortable' => true,
                        'resizable' => true,
                        'filterable' => true
                    )),
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_STRING,
                        'dataField' => 'metal_brand',
                        'width' => 150,
                        'headerText' => L('Метал - Марка/Код'),
                        'sortable' => true,
                        'resizable' => true,
                        'filterable' => true
                    )),
                    $this->newColumn(array(
                        'type' => 'group',
                        'headerText' => L('Ширина'),
                        'children' => array(
                            $this->newColumn(array(
                                'type' => GridPanelHandler::CTYPE_FLOAT,
                                'dataField' => 'a_minus',
                                'width' => 50,
                                'headerText' => '-',
                                'sortable' => false,
                                'filterable' => false,
                                'resizable' => true,
                            )),
                            $this->newColumn(array(
                                'type' => GridPanelHandler::CTYPE_FLOAT,
                                'dataField' => 'a_plus',
                                'width' => 50,
                                'headerText' => '+',
                                'sortable' => false,
                                'filterable' => false,
                                'resizable' => true,
                            )),
                        )
                    )),
                    $this->newColumn(array(
                        'type' => 'group',
                        'headerText' => L('Височина'),
                        'children' => array(
                            $this->newColumn(array(
                                'type' => GridPanelHandler::CTYPE_FLOAT,
                                'dataField' => 'h_minus',
                                'width' => 50,
                                'headerText' => '-',
                                'sortable' => false,
                                'filterable' => false,
                                'resizable' => true,
                            )),
                            $this->newColumn(array(
                                'type' => GridPanelHandler::CTYPE_FLOAT,
                                'dataField' => 'h_plus',
                                'width' => 50,
                                'headerText' => '+',
                                'sortable' => false,
                                'filterable' => false,
                                'resizable' => true,
                            )),
                        )
                    )),
                    $this->newColumn(array(
                        'type' => 'group',
                        'headerText' => L('Диаметър'),
                        'children' => array(
                            $this->newColumn(array(
                                'type' => GridPanelHandler::CTYPE_FLOAT,
                                'dataField' => 'd_minus',
                                'width' => 50,
                                'headerText' => '-',
                                'sortable' => false,
                                'filterable' => false,
                                'resizable' => true,
                            )),
                            $this->newColumn(array(
                                'type' => GridPanelHandler::CTYPE_FLOAT,
                                'dataField' => 'd_plus',
                                'width' => 50,
                                'headerText' => '+',
                                'sortable' => false,
                                'filterable' => false,
                                'resizable' => true,
                            )),
                        )
                    ))
                ),
            )),

            $this->newColumn(array(
                'type' => 'group',
                'headerText' => L('Дебелина на метала'),
                'children' => array(
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_FLOAT,
                        'dataField' => 's',
                        'width' => 150,
                        'headerText' => L('Дебелина'),
                        'sortable' => false,
                        'filterable' => true,
                        'resizable' => true,
                    )),
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_FLOAT,
                        'dataField' => 's_minus',
                        'width' => 100,
                        'headerText' => '-',
                        'sortable' => false,
                        'filterable' => true,
                        'resizable' => true,
                    )),
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_FLOAT,
                        'dataField' => 's_plus',
                        'width' => 100,
                        'headerText' => '+',
                        'sortable' => false,
                        'filterable' => true,
                        'resizable' => true,
                    )),

                )
            )),
            $this->newColumn(array(
                'type' => 'group',
                'headerText' => L('Количество'),
                'children' => array(
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_FLOAT,
                        'dataField' => 'quantity',
                        'width' => 75,
                        'headerText' => L('Количество'),
                        'sortable' => false,
                        'filterable' => true,
                        'resizable' => true,
                    )),
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_ENUM,
                        'dataField' => 'quantity_type',
                        'width' => 25,
                        'headerText' => L('Тип'),
                        'sortable' => false,
                        'filterable' => true,
                        'resizable' => true,
                        'items' => array(
                            'count' => array('text' => L('бр.')),
                            'mass' => array('text' => L('кг')),
                            'length' => array('text' => L('м')),
                        )
                    )),
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_FLOAT,
                        'dataField' => 'quantity_minus',
                        'width' => 105,
                        'headerText' => '-',
                        'sortable' => false,
                        'filterable' => false,
                        'resizable' => true,
                    )),
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_FLOAT,
                        'dataField' => 'quantity_plus',
                        'width' => 105,
                        'headerText' => '+',
                        'sortable' => false,
                        'filterable' => false,
                        'resizable' => true,
                    )),
                )
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_FLOAT,
                'dataField' => 'total_mass',
                'width' => 125,
                'headerText' => L('Количество в кг'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_FLOAT,
                'dataField' => 'produced_total_mass',
                'width' => 125,
                'headerText' => L('Произведено в кг'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_DATE,
                'dataField' => 'desired_production_start',
                'width' => 110,
                'headerText' => L('Дата на производство'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_DATE,
                'dataField' => 'desired_production_end',
                'width' => 110,
                'headerText' => L('Желан край'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'line_name',
                'width' => 100,
                'headerText' => L('Линия'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_DATETIME,
                'dataField' => 'production_time_from',
                'width' => 100,
                'headerText' => L('Начало'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_DATETIME,
                'dataField' => 'production_time_to',
                'width' => 100,
                'headerText' => L('Край'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumnEditedBy()
        );

        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
    }

    public function loadData($aRequest) {
        $sTimePeriod = $this->getWhereFromTimeButton('po.created_time', $aRequest['fields']['time_period']);

        $this->sReportSelectStatement = "
            SELECT SQL_CALC_FOUND_ROWS
                po.id,
                po.num,
                po.created_time AS po_created_time,
                po.name,
                pst.code  AS standard,
                ps.code   AS size_code,
                pl.code   AS line_name,

                pos.id    AS id_size,
                pos.l,
                #pos.metal_brand,
                pmb.name AS metal_brand,
                pos.a_plus,   # ширина
                pos.a_minus,
                pos.h_plus,   # височина
                pos.h_minus,
                pos.d_plus,   # диаметър
                pos.d_minus,
                pos.s,        # дебелина
                pos.s_plus,
                pos.s_minus,
                pos.quantity,
                pos.quantity_type,
                pos.total_mass,
                pos.produced_total_mass,
                pos.quantity_plus,
                pos.quantity_minus,
                pos.desired_production_start,
                pos.desired_production_end,
                MIN(pscr.time_from) AS production_time_from,
                MAX(pscr.time_to)   AS production_time_to,
                IF(pos.id,
                  CONCAT_WS(' ', pcs.fname, pcs.mname, pcs.lname),
                  CONCAT_WS(' ', pco.fname, pco.mname, pco.lname)) AS created_user,
                IF(pos.id,
                  CONCAT_WS(' ', pus.fname, pus.mname, pus.lname),
                  CONCAT_WS(' ', puo.fname, puo.mname, puo.lname)) AS updated_user,
                po.created_time,
                po.updated_time,
                IF(po.status = 'cancelled', 
                    po.status, 
                    IF(po.needs_review OR sum(pos.needs_review)>0,
                        'needs_review',
                        IF(pos.complete, 
                             'finished', 
                             IF(COALESCE(s.real_setup_duration + s.real_production_duration,0) != 0,  
                                'production',
                                 IF(COALESCE(s.planned_production_duration + s.planned_setup_duration,0) != 0,
                                    'confirmed',
                                    po.status
                                 )
                             )	
                        )
                    )
                ) as row_status,
                IF(pos.is_delayed AND po.status = 'confirmed', 'delayed', '') as marker,
                ''
            FROM production_orders po
            LEFT JOIN production_orders_sizes pos ON pos.id_order = po.id AND pos.to_arc = 0
            LEFT JOIN production_standards  pst ON pst.id = pos.id_standard
            LEFT JOIN production_sizes        ps ON ps.id = pos.id_size
            LEFT JOIN production_metal_brands pmb ON pmb.id = pos.id_metal_brand AND pmb.to_arc = 0
            LEFT JOIN production_schedule   psc ON psc.id_order_size = pos.id AND psc.to_arc = 0
            LEFT JOIN production_schedule_rows pscr ON pscr.id_schedule = psc.id AND pscr.to_arc = 0 AND pscr.type IN('planned_setup','planned_production')
            LEFT JOIN production_lines        pl ON pl.id = psc.id_line
            LEFT JOIN personnel pco ON pco.id = po.created_user
            LEFT JOIN personnel puo ON puo.id = po.updated_user
            LEFT JOIN personnel pcs ON pcs.id = pos.created_user
            LEFT JOIN personnel pus ON pus.id = pos.updated_user
            LEFT JOIN (					
                SELECT 
                    ps.id_order_size,
                    SUM(IF(psr.type = 'planned_production',psr.duration,0)) as planned_production_duration,
                    SUM(IF(psr.type = 'planned_setup',psr.duration,0)) as planned_setup_duration,
                    SUM(IF(psr.type = 'real_production',psr.duration,0)) as real_production_duration,
                    SUM(IF(psr.type = 'real_setup',psr.duration,0)) as real_setup_duration,
                    ''
                FROM production_schedule ps
                JOIN production_schedule_rows psr ON psr.id_schedule = ps.id AND psr.to_arc = 0
                WHERE 1
                AND ps.to_arc = 0
                GROUP BY ps.id_order_size
            ) s ON s.id_order_size = pos.id
        ";

        $this->aReportWhereStatement[] = "po.to_arc = 0";
        $this->aReportWhereStatement[] = $sTimePeriod;

        if(!empty($aRequest['fields']['status']) && $aRequest['fields']['status'] != 'all') $this->aReportHavingStatement[] = "row_status = " . DBC::$main->quote($aRequest['fields']['status']);

        $this->sReportGroupStatement = 'pos.id, po.id';

        $this->aDefaultSort = array(
            array('field' => 'po_created_time', 'dir' => 'ASC'),
            array('field' => 'pos.id', 'dir' => 'ASC')
        );

        return parent::loadData($aRequest);
    }

    public static function getOrderStatus($idOrder) {
        $idOrder = intval($idOrder);
        return DBC::$main->selectField("
            SELECT 
            IF(po.status = 'cancelled', 
                    po.status, 
                    IF(po.needs_review OR sum(pos.needs_review) > 0,
                        'needs_review',
                        IF(SUM(pos.complete) = count(pos.id), 
                             'finished', 
                             IF(COALESCE(s.real_setup_duration + s.real_production_duration,0) != 0,  
                                'production',
                                 IF(COALESCE(s.planned_production_duration + s.planned_setup_duration,0) != 0,
                                    'confirmed',
                                    po.status
                                 )
                             )	
                        )
                    )
            ) as row_status
            FROM production_orders po 
            JOIN production_orders_sizes pos ON pos.id_order = po.id AND pos.to_arc = 0
            LEFT JOIN (					
                    SELECT 
                        ps.id_order_size,
                        SUM(IF(psr.type = 'planned_production',psr.duration,0)) as planned_production_duration,
                        SUM(IF(psr.type = 'planned_setup',psr.duration,0)) as planned_setup_duration,
                        SUM(IF(psr.type = 'real_production',psr.duration,0)) as real_production_duration,
                        SUM(IF(psr.type = 'real_setup',psr.duration,0)) as real_setup_duration,
                        ''
                    FROM production_schedule ps
                    JOIN production_schedule_rows psr ON psr.id_schedule = ps.id AND psr.to_arc = 0
                    WHERE 1
                    AND ps.to_arc = 0
                    GROUP BY ps.id_order_size
            ) s ON s.id_order_size = pos.id
            WHERE 1
            AND po.id = $idOrder
            GROUP BY po.id
        ");
    }

    public function setGridTotals($aData, $aRequest = array(), $nTimeFrom = 0, $nTimeTo = 0) {
        $gridTotals = [];

        array_walk($aData, function ($row) use (&$gridTotals) {
            $gridTotals['total_mass'] += $row['total_mass'];
        });

        $this->oResponse->gridTotals = $gridTotals;
        return $gridTotals;
    }

}