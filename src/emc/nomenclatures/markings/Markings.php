<?php
namespace emc\nomenclatures\markings;

use main\db\DBC;
use main\form\FormPanelHandler;
use main\grid\GridPanelHandler;

/**
 * @accessLevelDescription Маркировки
 */
class Markings extends GridPanelHandler {

    public function __construct() {
        $this->oBase = DBC::$slave;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'code',
                'width' => 100,
                'headerText' => L('Код'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'name',
                'width' => 300,
                'headerText' => L('Име'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumnEditedBy()
        );

        $this->sReportSelectStatement = "
            SELECT SQL_CALC_FOUND_ROWS
                m.*,
                CONCAT_WS(' ',pc.fname,pc.mname,pc.lname) AS created_user,
                CONCAT_WS(' ',pu.fname,pu.mname,pu.lname) AS updated_user
            FROM markings m
            LEFT JOIN personnel pc ON pc.id = m.created_user
            LEFT JOIN personnel pu ON pu.id = m.updated_user
        ";

        $this->aReportWhereStatement[] = "m.to_arc = 0";
        $this->aDefaultSort = array(
            array('field' => 'created_time', 'dir' => 'ASC')
        );

        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
    }

    public function loadData($aRequest) {
        return parent::loadData($aRequest);
    }

    public static function getMarkingByID($id) {
        return DBC::$slave->selectByID('markings', $id, 300);
    }
}