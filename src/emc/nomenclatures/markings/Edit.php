<?php
namespace emc\nomenclatures\markings;

use main\db\DBC;
use main\Exception;
use main\form\FormPanelHandler;
use main\InvalidParameter;

/**
 * Class Edit
 * @accessLevelDescription Редактиране на маркировка
 */
class Edit extends FormPanelHandler {

    public function init($aRequest = array(), $aInitParams = array()) {
        foreach ($aInitParams['row'] as $k => $v) $this->setField($k, $v);
        $this->setLastEditInfo($aInitParams['row']);
        return $this->oResponse;
    }

    public function save($aRequest) {
        $marking = $aRequest['fields'];
        $marking['id'] = intval($aRequest['id']);
        $marking = array_map('trim', $marking);

        foreach (array('name', 'code') as $k) if(empty($marking[$k])) throw new InvalidParameter('Необходимо е да въведете стойност', array($k));

        $marking['code'] = mb_strtoupper($marking['code'], 'UTF-8');

        if(DBC::$slave->selectRow("SELECT id FROM markings WHERE id != {$marking['id']} AND to_arc = 0 AND code = " . DBC::$slave->quote($marking['code']))) throw new InvalidParameter(L('Вече съществува маркировка с този код.'));

        DBC::$main->update('markings', $marking);

        return $this->oResponse;
    }

    public function delete($aRequest) {
        $marking = $aRequest['fields'];
        $marking['id'] = intval($aRequest['id']);

        if(empty($marking['id'])) throw new InvalidParameter;

        if(DBC::$slave->selectRow("SELECT id_metal FROM production_sizes WHERE id_marking = {$marking['id']} AND to_arc = 0")) {
            throw new Exception(L('Този маркировка не може да бъде изтрита, защото се използва в производствени размери.'));
        }

        DBC::$main->delete('markings', $marking['id']);
        return $this->oResponse;
    }

}