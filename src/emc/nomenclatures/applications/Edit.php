<?php

namespace emc\nomenclatures\applications;

use main\Exception;
use main\form\FormPanelHandler;
use \main\db\DBC;
use main\suggest\Suggest;

/**
 * Class Edit
 * @package emc\nomenclatures\applications
 * @accessLevelDescription Достъп за редакция на стойности за "Ще се използва за"
 */
class Edit extends FormPanelHandler {
    public function init($aRequest, $aInitData) {
        return $this->loadData($aRequest, $aInitData);
    }

    public function loadData($aRequest, $aInitData) {
        $nId = intval($aInitData['id']);

        $this->setField('name', DBC::$slave->selectField("SELECT name FROM production_applications WHERE id = $nId AND to_arc = 0"));

        return $this->oResponse;
    }

    public function save($aRequest) {
        $nId = intval($aRequest['id']);
        $sEscapedName = DBC::$slave->quote($aRequest['fields']['name']);

        if(empty($sEscapedName)) throw new Exception(L('Моля въведете име.'));

        if(empty($nId)) {
            if (DBC::$slave->selectField("SELECT id FROM production_applications pd WHERE name = $sEscapedName AND to_arc = 0"))
                throw new Exception(L('Съществува приложение с това име.'));
        }

        $aData = array(
            'id' => $nId,
            'name' => $aRequest['fields']['name']
        );

        DBC::$main->update('production_applications', $aData);

        return $aData;
    }

    public function remove($nId) {
        $nId = intval($nId);

        if(!empty($nId)) {
            $nUsed = DBC::$slave->selectField("SELECT id FROM production_orders_sizes pos WHERE pos.id_application = $nId AND pos.to_arc = 0");
            if (!empty($nUsed)) throw new Exception(L('Приложението се използва от други заявки.'));

            DBC::$main->delete('production_applications', $nId);
        }
    }

    public function suggestApplication($aRequest) {
        $aResult = array();
        $aResult['select'] 	= "SELECT id, name as value, name as html_value FROM production_applications pa ";
        $aResult['where'][] = "pa.name LIKE '%{$aRequest['query']}%' AND pa.to_arc = 0";
        $aResult['group'] 	= 'html_value';

        return Suggest::select($aResult);
    }
}