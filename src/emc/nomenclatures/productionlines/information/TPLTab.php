<?php
namespace emc\nomenclatures\productionlines\information;

use main\TPLBase;

class TPLTab extends TPLBase {

    public function __construct($sPrefix, array $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);

        $this->bPrintWithTitle = true;
        $this->aFrameConfig['title'] = empty($aInitParams['row']['id']) ? L('Нова тръбна линия') : L('Редакция на тръбна линия: %s', $aInitParams['row']['code']);
    }

    public function printHtml() {

        $aStatuses = array(
            array('id' => 'active', 'value' => L('Работи')),
            array('id' => 'idle', 'value' => L('Не работи'))
        );

        $min = L('мин');

        ?>

        <style>
            #<?= $this->sPrefix; ?> td {
                padding: 4px;
            }

            #<?= $this->sPrefix; ?> .sides {
                vertical-align: top;
            }

            #<?= $this->sPrefix; ?> .input-width {
                width: 50px;
            }

            #<?= $this->sPrefix; ?> .full-parent-width {
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
                width: 100%;
            }

            #<?= $this->sPrefix; ?> .toolbar:after {
                clear: both;
                display: block;
                content: ''
            }
        </style>

        <table style="width: 100%">
            <tr>
                <td class="sides" style="width:35%;">
                    <table style="width: 100%;">
                        <tr>
                            <td><? $this->printElement('label', array('fieldName' => 'code', 'text' => L('Име/Код'))); ?></td>
                            <td><? $this->printElement('input', array('name' => 'code', 'class' => 'full-parent-width', 'style' => 'height: 26px;')); ?></td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName' => 'status', 'text' => L('Статус'))); ?></td>
                            <td><? $this->printElement('select', array('name' => 'status'), $aStatuses); ?></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <? $this->printElement('label', array('fieldName' => 'efficiency', 'text' => L('Коефициент на уплътняване'))); ?>
                                <? $this->printElement('input', array('name' => 'efficiency', 'class' => 'input-width', 'keyRestriction' => 'integer')); ?>
                                <? $this->printElement('span', array('text' => '%')); ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <fieldset>
                                    <legend><?= L('Времена за пренастройки'); ?></legend>
                                    <table style="height: 189px;">
                                        <tr>
                                            <td><? $this->printElement('label', array('fieldName' => 'setup_time_small', 'text' => L('Малка'))); ?></td>
                                            <td>
                                                <? $this->printElement('input', array('name' => 'setup_time_small', 'class' => 'input-width', 'keyRestriction' => 'integer')); ?>
                                                <? $this->printElement('span', array('text' => $min)); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><? $this->printElement('label', array('fieldName' => 'setup_time_big', 'text' => L('Голяма'))); ?></td>
                                            <td>
                                                <? $this->printElement('input', array('name' => 'setup_time_big', 'class' => 'input-width', 'keyRestriction' => 'integer')); ?>
                                                <? $this->printElement('span', array('text' => $min)); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><? $this->printElement('label', array('fieldName' => 'setup_time_length', 'text' => L('Друга дължина'))); ?></td>
                                            <td>
                                                <? $this->printElement('input', array('name' => 'setup_time_length', 'class' => 'input-width', 'keyRestriction' => 'integer')); ?>
                                                <? $this->printElement('span', array('text' => $min)); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><? $this->printElement('label', array('fieldName' => 'setup_time_thickness', 'text' => L('Друга дебелина'))); ?></td>
                                            <td>
                                                <? $this->printElement('input', array('name' => 'setup_time_thickness', 'class' => 'input-width', 'keyRestriction' => 'integer')); ?>
                                                <? $this->printElement('span', array('text' => $min)); ?>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </td>
                <td class="sides" width="65%;">
                    <table width="100%">
                        <tr>
                            <td>
                                <? $this->printElement('label', array('fieldName' => 'line_manager', 'text' => L('Мениджър на линия:'))); ?>
                                <? $this->printElement('suggestcombo', array(
                                    'name' => 'line_manager',
                                    'suggest' => 'method: emc\nomenclatures\productionlines\information\Tab.suggestLineManager;'
                                )) ?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <? $this->printElement('label', array('fieldName' => 'line_production_manager', 'text' => L('Мениджър Производство на линия:'))); ?>
                                <? $this->printElement('suggestcombo', array(
                                    'name' => 'line_production_manager',
                                    'suggest' => 'method: emc\nomenclatures\productionlines\information\Tab.suggestLineProductionManager;'
                                )) ?>
                            </td
                        </tr>
                        <tr>
                            <td>
                                <? $this->printElement('p', array('text' => L('Друга информация:'))); ?>
                                <? $this->printElement('textarea', array('name' => 'notes', 'class' => 'full-parent-width', 'style' => 'height: 200px;')); ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 0;">
                    <div class="toolbar align-right" style="margin-top: 16px">
                        <? $this->printElement('lasteditedinfo'); ?>
                        <? $this->printElement('closebutton'); ?>
                        <? $this->printElement('confirmbutton'); ?>
                    </div>
                </td>
            </tr>
        </table>

        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, baseParams) {
            form = container;

            $(form).find('.confirm').click(function(){
                form.request('save', { id: baseParams['row']['id'] }, null, function(result) {
                    if (result.newId) {
                        baseParams.enableTabs([1, 2]);
                        baseParams.setNewLineId(result.newId);
                        baseParams.close();
                    }
                    baseParams.getParentGrid().loadData();
                });
            });

            $(form).find('.close').click(function() { baseParams.close(); });
        });
JS;
    }


}