<?php
namespace emc\nomenclatures\productionlines\information;

use emc\access\AccessLevel;
use emc\nomenclatures\productionlines\CanEditProductionLine;
use emc\production\schedule\Scheduler;
use main\Access;
use main\db\DBC;
use main\Exception;
use main\form\FormPanelHandler;
use main\suggest\Suggest;

/**
 * Class Edit
 * @accessLevelDescription Таб информация на тръбна линия
 */
class Tab extends FormPanelHandler {

    public function init($aRequest = array(), $aInitParams = array()) {
        foreach ($aInitParams['row'] as $sField => $mValue) {
            switch($sField){
                case 'line_manager':
                case 'line_production_manager':
                    $this->setFieldValue($sField, ['id' => $aInitParams['row']['id_' . $sField], 'value' => $aInitParams['row'][$sField]]);
                    break;
                default:
                    $this->setField($sField, $mValue);
            }
        }

        $this->setLastEditInfo($aInitParams['row']);

        return $this->oResponse;
    }

    public function save($aRequest) {
        return DBC::$main->withTrans(function ($aRequest) {
            Access::checkAccess(CanEditProductionLine::class);

            $aLineInformation = $aRequest['fields'];

            foreach ($aLineInformation as $mKey => $sField) {
                $sField = trim($sField);
                $aLineInformation[$mKey] = $sField;
                if($mKey != 'notes' && $mKey != 'last_edited_info' && $mKey != 'line_manager' && $mKey != 'line_production_manager' && empty($sField)) {
                    throw new Exception(L('Липсва стойност за поле: %s', $sField), array($mKey));
                }
            }

            $aLineInformation['id'] = intval($aRequest['id']);
            $aLineInformation['code'] = strtoupper($aLineInformation['code']);
            $aLineInformation['efficiency'] = $aLineInformation['efficiency'] / 100;
            $sCode = DBC::$slave->quote($aLineInformation['code']);
            $aProductionLine = DBC::$slave->selectRow("SELECT code FROM production_lines WHERE code = $sCode AND id != {$aLineInformation['id']} AND to_arc = 0");
            if(!empty($aProductionLine)) throw new Exception(L('Вече съществува линия с този код.'));

            $aLineInformation['id_line_manager'] = $aRequest['fields']['line_manager']['id'];
            $aLineInformation['id_line_production_manager'] = $aRequest['fields']['line_production_manager']['id'];

            $this->oResponse->newId = 0;

            DBC::$main->update('production_lines', $aLineInformation);

            if(empty($aRequest['id'])) $this->oResponse->newId = $aLineInformation['id'];

            if($aLineInformation['status'] == 'idle' && !empty(DBC::$main->selectField("
                SELECT
                ps.id_line
                FROM production_schedule ps
                JOIN production_orders_sizes pos ON pos.id = ps.id_order_size AND pos.to_arc = 0 AND pos.complete = 0
                WHERE 1
                AND ps.to_arc = 0
                AND ps.id_line = ".$aLineInformation['id']."
                GROUP BY ps.id_line
            "))) throw new Exception("Не е възможно линия да бъде направена неактивна, докато са налични планирани и/или в производство размери към нея.");

            Scheduler::scheduleLine($aLineInformation['id']);

            return $this->oResponse;
        }, [$aRequest]);
    }

    public function delete($aRequest) {
    }

    public function suggestLineProductionManager($aRequest) {
        $aRequest['production_managers_only'] = true;
        return $this->suggestManager($aRequest);
    }
    public function suggestLineManager($aRequest) {
        return $this->suggestManager($aRequest);
    }

    protected function suggestManager($aRequest) {
        $aResult = array();
        $aResult['select'] = "
            SELECT 
                p.id, 
                CONCAT_WS(' ',p.fname,p.mname,p.lname, '(',p.email,')') as html_value,
                CONCAT_WS(' ',p.fname,p.mname,p.lname) as value
            from personnel p
            join access_account ac ON ac.id_person = p.id and ac.to_arc = 0
            join access_account_profiles aap ON aap.id_account = ac.id
            join access_profiles_levels apl ON apl.id_profile = aap.id_profile
        ";

        $aResult['where'][] = "1";
        $aResult['where'][] = "p.`status` = 'active'";
        $aResult['where'][] = "p.to_arc = 0";

        if($aRequest['production_managers_only']) {
            $aResult['where'][] = 'apl.`level` in ("\\\", "\\\emc\\\access\\\profiles\\\ProfileProductionManager")';
        }

        $aResult['having'][] = "html_value like '%{$aRequest['query']}%' ";
        $aResult['group'] = 'html_value';
        $aResult['order'] = 'html_value';

        return Suggest::select($aResult);
    }

}