<?php
namespace emc\nomenclatures\productionlines\documentation;

use main\TPLBase;

/**
 * Class TPLTab
 */
class TPLTab extends TPLBase {
    public function __construct($sPrefix, array $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);
    }

    public function printHtml() {
        ?>
        <style>
            #<?=$this->sPrefix?> #queryTable th {
                line-height: 22px;
                border: 1px solid rgba(0, 0, 0, 0.04);
                white-space: nowrap;
                background: rgba(0, 104, 154, 0.11);
                padding: 3px 0 3px 6px;
                text-align: center;
            }

            #<?=$this->sPrefix?> #queryTable td {
                white-space: nowrap;
                padding: 3px 6px;
                border: 1px solid rgba(0, 0, 0, 0.02);
                line-height: 24px;
            }

            #<?=$this->sPrefix?> #queryTable tr:nth-child(odd) {
                background: rgba(96, 125, 139, 0.09);
            }


            #<?=$this->sPrefix?> #queryTable td:last-child {
                text-align: center;
            }

            #<?=$this->sPrefix?> input.quantity {
                width: 50px;
                margin-right: 3px !important;
            }

            #<?=$this->sPrefix?> input.length {
                width: 60px;
            }

            #<?=$this->sPrefix?> input.size {
                width: 200px;
            }

            #<?=$this->sPrefix?> div.grid-headers-inner {
                padding-right: 0 !important;
            }
        </style>

        <div style="height: 350px;">
            <br/>
            <div style="overflow: auto; height: 100%;">
                <table style="width: 100%; border-collapse: collapse;" id="queryTable" border="1px solid black"></table>
            </div>
            <div class="toolbar" style="text-align: right; margin:5px; height: 30px">
                <button name="export" >
                    <div class="icon fa fa-file-excel-o fa-fw"></div>
                    <span>Експорт</span>
                </button>
            </div>
        </div>

        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, baseParams) {
            form = container;
            id_line = baseParams['row']['id'];

            var export_button = $(form).find('button[name=export]');

            export_button.click(function() {
                var fields = {id_line: id_line};

                var request = jQuery.extend({}, fields);
                framework.doExport('emc/nomenclatures/productionlines/documentation/Export.xls', 'Excel5', request);
            });

            form.addListener('afterRequest', function(result) {
                drawTable(result);
            });

            form.addListener('beforeRequest', function(request) {
                request['id_line'] = id_line;
                return request;
            });

            var deleteFile = function(id) {
                id_row = id;

                form.request('deleteFile', { row_id: id }, null, function(result) {
                    var inner_div = $('div[name=inner]').find('li');
                    
                    inner_div.each(function(idx, data) {
                        if($(data).attr('id_file') == id_row) {
                            if(inner_div.length == 1) inner_div.parents('div.panel-frame')[0].close();
                            inner_div[idx].remove();
                            return false;
                        }
                    });
                }, { id_row: id_row });
            };

            var makeFileCategoryLi = function(files) {
                var li = $("<li></li>");
                var href = $("<a></a>");

                li.addClass('clickable');

                var type = '';
                switch (files.type) {
                    case 'settings':
                        type = "Настройка";
                        href.text(type + ' (' + files.count + ')');
                    break;
                    case 'drawing':
                        type = "Чертеж";
                        href.text(type + ' (' + files.count + ')');
                    break;
                    case 'blank':
                        type = "Заготовка";
                        href.text(type + ' (' + files.count + ')');
                    break;
                }

                var ul_inner = $("<ul></ul>");
                for(var f in files.rows) {
                    var file = files.rows[f];
                    var li_inner = $("<li></li>");

                    var title = file['file_name'];
                    if(file['description'] != '') title += '-' + file['description'];
                    var href_inner = $("<a></a>").attr({"href": file['url'], title: title, target:'_blank'});
                    href_inner.text(file['file_name']);

                    var del = $("<div class=\"icon fa fa-trash\"></div>");
                    del.css({'display': 'inline', 'padding-left': '5px'});

                    del.click(function() {
                        deleteFile($(this.parentElement).attr('id_file'));
                    });

                    li_inner.append(href_inner);
                    li_inner.append(del);
                    li_inner.attr('id_file', file['id']);

                    ul_inner.append(li_inner);
                }

                var content = $('<div class="form" name="inner"></div>');
                content.css({'max-width': '300px', 'max-height': '400px', 'overflow': 'auto'});
                content.append(ul_inner);
                href.click(function() {
                    var category_wnd = framework.createWindow({
                        closable: true,
                        title: "Преглед на " + type.toLowerCase(),
                        content: content
                    }).window;
                });

                li.append(href);

                return li;
            };

            var th_new = function() { return $('<th></th>'); }
            var tr_new = function() { return $('<tr></tr>'); }
            var td_new = function() { return $('<td></td>'); }
            var tbody_new = function() { return $('<tbody></tbody>'); }

            var drawTable = function(result) {
                var table_data = result.table;
                var thicknesses = result.thicknesses;
                var files = result.files;

                var table = $(form).find('table');
                table.empty();

                var thead = $('<thead></thead>');
                var tbody = $('<tbody></tbody>');
                var tr = tr_new();

                tr.append($('<th style="width: 200px; padding-left: 0;"></th>').append('<input type="text" name="size_filter" style="width: 180px; margin: 0 5px;">'));
                tr.append(th_new().css("width","100px").text("Файлове"));

                for(var i=0; i<thicknesses.length; i++) {
                    tr.append(th_new().css("min-width","45px").text(thicknesses[i]));
                }
                thead.append(tr);

                for(baseSize in table_data) {
                    var data = table_data[baseSize];
                    tr = tr_new().attr('baseSize', baseSize);
                    tr.append(td_new().text(baseSize));

                    var td = td_new();
                    var ul = $("<ul style='list-style: none;'></ul>");

                    ul.append($("<li class=\"clickable\"><span style=\"padding-right:5px\">Добави</span><div class=\"icon fa fa-plus\"></div></li>").click(function(){
                        var content = $('<div class="form"><select>' +
                            '<option value="drawing">Чертеж</option>' +
                            '<option value="settings">Настройка</option>' +
                            '<option value="blank">Заготовка</option>' +
                            '</select>' +
                            '<input type="file" multiple="multiple" name="file">' +
                            '<button class="confirm"><div class="icon fa fa-check"></div></button>' +
                            '<br>' +
                            '<br>' + '<textarea name="description" placeholder="Описание" style="width: 98%"></textarea>' +
                            '</div>'
                        );

                        var wnd = framework.createWindow({
                            closable: true,
                            title: "Добавяне на снимка",
                            content: content
                        }).window;
                        framework.attachMethods(wnd);
                        $(wnd)[0].parentTr = $(this).parents('tr')[0];
                        $(wnd).find('button.confirm').click(function(){
                            var files = $(wnd).find('input')[0].getValue();
                            var type = $(wnd).find('select')[0].getValue();
                            var description = $(wnd).find('textarea')[0].getValue();
                            var baseSize = $(wnd.parentTr).attr('baseSize');

                            form.request('save', {files: files, type: type, description: description, baseSize: baseSize}, null, function() {
                                wnd.close();
                            });
                        });
                    }));

                    var sizeFiles = files[baseSize];

                    var settings = [];
                    var drawings = [];
                    var blanks = [];
                    var settings_count = 0;
                    var drawings_count = 0;
                    var blanks_count = 0;
                    for(var f in sizeFiles) {
                        switch(sizeFiles[f].type) {
                            case 'settings': settings.push(sizeFiles[f]); settings_count++; break;
                            case 'drawing': drawings.push(sizeFiles[f]); drawings_count++; break;
                            case 'blank': blanks.push(sizeFiles[f]); blanks_count++; break;
                        }
                    }
                    if(settings_count) ul.append(makeFileCategoryLi({rows: settings, type: 'settings', count: settings_count}));
                    if(drawings_count) ul.append(makeFileCategoryLi({rows: drawings, type: 'drawing', count: drawings_count}));
                    if(blanks_count) ul.append(makeFileCategoryLi({rows: blanks, type: 'blank', count: blanks_count}));

                    td.append(ul);
                    tr.append(td);

                    for(i=0; i<thicknesses.length; i++) {
                        if(data[i] != undefined && data[i].s == thicknesses[i]) tr.append(td_new().text(data[i].m));
                        else tr.append(td_new());
                    }

                    tbody.append(tr);
                }

                table.append(thead);
                table.append(tbody);

                // add filter listener
                $(table).find('input[name=size_filter]').bind('keyup', function() {
                    var filter_input = $(table).find('input[name=size_filter]')[0].value;
                    $(tbody).find('tr[basesize]').each(function(index, data) {
                        var basesize = $(this).attr("basesize");
                        $(this).css('display', basesize.toLowerCase().indexOf(filter_input.toLowerCase()) != -1 ? '' : 'none');
                    });
                });
            }
        });
JS;
    }
}