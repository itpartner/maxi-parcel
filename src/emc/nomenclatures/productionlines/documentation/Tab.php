<?php
namespace emc\nomenclatures\productionlines\documentation;;

use emc\nomenclatures\sizes\Sizes;
use main\Access;
use main\db\DBC;
use main\FileUpload;
use main\form\FormPanelHandler;
use main\InvalidParameter;

/**
 * Class Tab
 * @package emc\nomenclatures\productionlines\documentation
 * @accessLevelDescription Право за преглед на документация на тръбна линия
 */

class Tab extends FormPanelHandler {


    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest, $aInitParams);
    }

    public function loadData($aRequest, $aInitParams) {
        $idLine = intval($aInitParams['row']['id']);
        $thicknesses = [];
        $sizesByBaseSize = [];

        foreach(DBC::$main->select("
			SELECT
			  pl.id   AS id_line,
			  pl.code AS line_code,
			  ps.*,
			  round(ps.m, 3) AS m
			FROM production_lines pl
			JOIN production_lines_sizes pls ON pls.id_line = pl.id AND pls.to_arc = 0
			JOIN production_sizes ps ON ps.id = pls.id_size
			WHERE pl.id = $idLine
			GROUP BY ps.id
		") as $size) {
            $size['base_size'] = Sizes::getSizeCodeWithoutThicknessAndMetalBrand($size);
            $thicknesses[] = $size['s'];
            $sizesByBaseSize[$size['base_size']][] = $size;
        };

        $aFiles = DBC::$main->select("
            SELECT
              plf.basesize AS _key,
              plf.id      AS __key,
              plf.id,
              plf.basesize,
              plf.type,
              plf.description,
              f.name      AS file_name,
              f.type      AS mime_type,
              IF(f.file IS NOT NULL, CONCAT(fd.public_url,'/',f.file,'.',f.ext), '') AS url
            FROM production_lines_files plf
            JOIN files f ON f.file = plf.file
            JOIN file_devices fd ON fd.id = f.id_device
            WHERE plf.id_line = $idLine
            AND plf.to_arc = 0
        ", 0, 2);

        $thicknesses = (array_unique($thicknesses));
        sort($thicknesses);

        uksort($sizesByBaseSize, function($a, $b) {
            $sProfileType1 = substr($a, 0, strpos($a, '-'));
            $sProfileType2 = substr($b, 0, strpos($b, '-'));
            if($sProfileType1 != $sProfileType2)
                return $sProfileType1 > $sProfileType2 ? 1 : -1;
            else {
                if(!(strpos($a, 'x') != false && strpos($a, 'x') != false)) {
                    $sTemp1 = substr($a, strpos($a, '-') + 1, strpos($a, 'x') == false ? strlen($a) : strpos($a, 'x') - strpos($a, '-') - 1);
                    $sTemp2 = substr($b, strpos($b, '-') + 1, strpos($b, 'x') == false ? strlen($b) : strpos($b, 'x') - strpos($b, '-') - 1);

                    return $sTemp1 > $sTemp2 ? 1 : -1;
                } else {
                    $sTemp1 = substr($a, strpos($a, '-') + 1, strpos($a, 'x') - strpos($a, '-') - 1);
                    $sTemp2 = substr($b, strpos($b, '-') + 1, strpos($b, 'x') - strpos($b, '-') - 1);

                    if($sTemp1 > $sTemp2) return 1;
                    elseif($sTemp2 > $sTemp1) return -1;
                    else {
                        $sTemp1 = substr($a, strpos($a, 'x') + 1, strlen($a));
                        $sTemp2 = substr($b, strpos($b, 'x') + 1, strlen($b));

                        return $sTemp1 > $sTemp2 ? 1 : -1;
                    }
                }
            }
        });

        $table = [];

        foreach ($sizesByBaseSize as $baseSize => $sizes) foreach ($sizes as $k => $size) {
            $table[$baseSize][array_search($size['s'], $thicknesses)] = $size;
        }

        $this->oResponse->{'files'} = $aFiles;
        $this->oResponse->{'table'} = $table;
        $this->oResponse->{'thicknesses'} = $thicknesses;
        return $this->oResponse;
    }

    public function save($aRequest) {
        Access::checkAccess(CanEditDocumentationFiles::class);
        return DBC::$main->withTrans(function($aRequest) {
            if(!intval($aRequest['id_line']) || empty($aRequest['baseSize']) || empty($aRequest['type']))  throw new InvalidParameter;

            $aFiles = array();
            foreach($aRequest['files'] as $key => $file) {
                $uploaded_file = FileUpload::uploadFile($file);

                $uploaded_file['id_line'] = $aRequest['id_line'];
                $uploaded_file['baseSize'] = $aRequest['baseSize'];
                $uploaded_file['type'] = $aRequest['type'];
                $uploaded_file['description'] = DBC::$slave->escape($aRequest['description']);

                $aFiles[] = $uploaded_file;
            }

            DBC::$main->multiInsert('production_lines_files', $aFiles);

            return $this->loadData(array(), array('row' => array('id' => $aRequest['id_line'])));
        }, [$aRequest]);
    }

    public function deleteFile($aRequest) {
        Access::checkAccess(CanEditDocumentationFiles::class);

        if(!intval($aRequest['row_id'])) throw new InvalidParameter;

        DBC::$main->withTrans(function($aRequest) {
            DBC::$main->delete('production_lines_files', $aRequest['row_id']);
        }, [$aRequest]);

        return $this->loadData(array(), array('row' => array('id' => $aRequest['id_line'])));
    }
}