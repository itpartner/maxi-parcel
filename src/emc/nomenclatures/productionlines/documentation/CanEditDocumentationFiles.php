<?php
namespace emc\nomenclatures\productionlines\documentation;
use emc\access\AccessLevel;

/**
 * Class CanEditDocumentationFiles
 * @package emc\nomenclatures\productionlines\documentation
 * @accessLevelDescription Право за редакция на файлове в Документация на тръбна линия
 */
class CanEditDocumentationFiles implements AccessLevel {

}