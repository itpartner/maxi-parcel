<?php
namespace emc\nomenclatures\productionlines\documentation;

use main\Access;
use main\db\DBC;
use main\grid\GridExcelExport;

/**
 * Class Export
 * @package emc\nomenclatures\productionlines\documentation
 */
class Export {

    public function xls($aRequest, $oExport) {
        Access::checkAccess(DocumentationExport::class);

        $aParams = json_decode($aRequest['request']);
        $nIdLine = intval($aParams->id_line);

        $target = new DocumentationExport();
        $target->setData((new Tab())->loadData($aRequest, array('row' => array('id' => $nIdLine))));

        $oExport->{'aRequest'}['columns'] = null;

        $sLineCode = DBC::$slave->selectField("SELECT code FROM production_lines WHERE id = $nIdLine");
        $oExport->{'aRequest'}['fields']['line_code'] = $sLineCode;
        $target->aColumns[0]->headerText = "Документация за тръбна линия " . $sLineCode;

        $excelExport = new DocumentationExcelExport($oExport, $target);

        $excelExport->doExport();
    }
}

class DocumentationExcelExport extends GridExcelExport {
    public function doExport() {
        try {
            $this->oExcelBook->getProperties()->setCreator('')
                ->setLastModifiedBy("")
                ->setTitle("")
                ->setSubject("")
                ->setDescription("")
                ->setKeywords("")
                ->setCategory("");

            GridExcelExport::$aColumnTypes['header'] = array(
                'cellDataType' => \PHPExcel_Cell_DataType::TYPE_STRING,
                'format' => array(
                    'alignment' => array(
                        'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical'   => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        'wrap'       => TRUE,
                    ),
                    'font' => array(
                        'bold' 	=> false,
                        'size' 	=> 8,
                        'name'	=> GridExcelExport::ARIAL,
                        'color'	=>  array('argb' => \PHPExcel_Style_Color::COLOR_BLACK)
                    ),
                    'borders' => array(
                        'outline' => array(
                            'style' => \PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => \PHPExcel_Style_Color::COLOR_BLACK)
                        ),
                    ),
                    'fill' => array(
                        'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'DDDDDD')
                    )
                )
            );

            $oWorksheet = $this->oExcelBook->setActiveSheetIndex(0);
            $oWorksheet->getRowDimension('1')->setRowHeight(50);
            $this->setupSheet($oWorksheet, 'sheet 1');
            $this->addTable($oWorksheet, $this->sGridTitle, $this->oTarget, 0, 1);

            /*$objDrawing = new \PHPExcel_Worksheet_Drawing();
            $objDrawing->setName('emc_logo');
            $objDrawing->setDescription('emc_logo');
            $logo = BASE_PATH . '/images/emc-logo.png'; // Provide path to your logo file
            $objDrawing->setPath($logo);
            $objDrawing->setOffsetX(40);    // setOffsetX works properly
            $objDrawing->setOffsetY(300);  // setOffsetY has no effect
            $objDrawing->setCoordinates('A1');
            $objDrawing->setHeight(65); // logo height
            $objDrawing->setWorksheet($oWorksheet);

            $this->addTable($oWorksheet, $this->sGridTitle, $this->oTarget, 0, 2);*/

            //$oWorksheet->getColumnDimensions()->setAutoSize(true);//setWidth(20);

            foreach($oWorksheet->getColumnDimensions() as $key => $val) {
                $oWorksheet->getColumnDimension($key)->setAutoSize(true);
            }

            $this->sFileName = $this->oExport->aRequest['fields']['line_code'];
            switch ($this->oExport->sExportType) {
                case 'ExcelOpenXML':
                    $oWriter = \PHPExcel_IOFactory::createWriter($this->oExcelBook, 'Excel2007');
                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition: attachment;filename="'.$this->sFileName.'.xlsx"');
                    header('Cache-Control: max-age=0');

                    break;
                case 'Excel5' :
                    header('Content-Type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment;filename="'.$this->sFileName.'.xls"');
                    header('Cache-Control: max-age=0');

                    $oWriter = \PHPExcel_IOFactory::createWriter($this->oExcelBook, 'Excel5');
                    break;
                default:
                    header('Content-Type: application/pdf');
                    header('Content-Disposition: attachment;filename="'.$this->sFileName.'.pdf"');
                    header('Cache-Control: max-age=0');

                    $oWriter = \PHPExcel_IOFactory::createWriter($this->oExcelBook, 'PDF');
            }
            $oWriter->save('php://output');
            die();
        } catch (\Throwable $e) {
            header_remove();
            header('Content-Type: text/plain');
            die($e->getMessage());
        }
    }

    public function setData($oWorksheet, $aData, $nStartRow, $bGray = FALSE) {
        $nDataRow = 0;
        $rowIndex=0;
        foreach ($aData as $aRow) {
            $rowIndex++;
            foreach ($this->{'aXLSColumns'} as $nCol => $oColumn) {
                $sColType = $oColumn->type;
                if($sColType == 'rowIndex') $sColType = 'integer';
                else if(!array_key_exists($sColType,self::$aColumnTypes)) $sColType = 'string';

                $oCell = $oWorksheet->getCellByColumnAndRow($nCol, $nStartRow + $nDataRow );

                // style, format
                $aStyle = self::$aColumnTypes[$sColType]['format'];

                if($sColType == 'float') {
                    $aStyle['numberformat'] = array(
                        'code'	=> \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_000
                    );
                }

                $oWorksheet->getStyleByColumnAndRow($nCol, $nStartRow + $nDataRow)
                    ->applyFromArray($aStyle);

                //number format callback
                if(isset(self::$aColumnTypes[$sColType]['numberFormatFn'])) {
                    $aFormat = call_user_func_array(self::$aColumnTypes[$sColType]['numberFormatFn'],array($aRow,$oColumn));
                    $oWorksheet->getStyleByColumnAndRow($nCol, $nStartRow + $nDataRow)->getNumberFormat()->setFormatCode($aFormat['numberFormat']);
                } else {
                    $aFormat =array();
                }

                //value
                if($oColumn->type == 'rowIndex') {
                    $value = $rowIndex;
                } elseif(isset(self::$aColumnTypes[$sColType]['valueFn'])) {
                    $value = call_user_func_array(self::$aColumnTypes[$sColType]['valueFn'],array($aRow,$oColumn));
                } else {
                    $value = $aRow[(string)$oColumn->dataField];
                    //$value = $aRow[$oColumn->dataField];
                }

                if(!empty($aFormat['dataType'])) {
                    $oCell->setValueExplicit($value, $aFormat['dataType']);
                } else if(!empty(self::$aColumnTypes[$sColType]['cellDataType'])) {
                    if(empty($value)) $oCell->setValue(''); // хак? да излизат празни полетата вместо 0.000
                    else $oCell->setValueExplicit($value, self::$aColumnTypes[$sColType]['cellDataType']);
                } else {
                    $oCell->setValue($value);
                }
            }

            $nDataRow++;
        }
        return $nDataRow;
    }
}
