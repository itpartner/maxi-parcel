<?php
namespace emc\nomenclatures\productionlines\documentation;

use main\db\DBC;
use main\grid\GridPanelHandler;

/**
 * @accessLevelDescription Експорт на Документация за линия
 */
class DocumentationExport extends GridPanelHandler {

    private $data = [];

    public function __construct() {
        parent::__construct();
        $this->oBase = DBC::$main;
    }

    public function getGridData($aRequest, $nTimeFrom = 0, $nTimeTo = 0) {
        $result = [];
        foreach ($this->data->{'table'} as $baseSize => $columns) {
            $row = [];
            $row['base_size'] = $baseSize;
            foreach($this->data->{'thicknesses'} as $k => $thickness) {
              $row[''.$thickness] = $columns[$k]['m'];
            }
            $result[] = $row;
        }

        return $result;
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
    }

    /**
     * @return array
     */
    public function getData() {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data) {
        $this->data = $data;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => 'group',
                'children' => array(
                    $this->newColumn(array(
                        'type' => GridPanelHandler::CTYPE_STRING,
                        'dataField' => 'base_size',
                        'width' => 100,
                        'headerText' => L('Размер'),
                        'filterable' => true,
                        'resizable' => true,
                        'sortable' => true
                    ))
                )
            )
        ));

        foreach ($data->{'thicknesses'} as $t) {
            $this->aColumns[0]->children[] = $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_FLOAT,
                //'dataField' => ''.$t, // нарочно понеже newColumn разбива полето на няколко части при наличие на .
                'width' => 30,
                'headerText' => $t
            ));
        }
    }

}