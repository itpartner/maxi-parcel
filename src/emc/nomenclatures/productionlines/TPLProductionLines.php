<?php
namespace emc\nomenclatures\productionlines;

use main\grid\TPLGridPanel;

class TPLProductionLines extends TPLGridPanel {
    public function __construct($sPrefix, $aInitParams = array()) {
        $this->{'aConfig'} = array(
            'title' => L('Тръбни линии'),
            'autoVResize' => true,
            'topBarItems' => array(
                array('button', array('name' => 'add', 'class' => 'add', 'iconCls' => 'icon fa fa-plus-circle', 'text' => L('Добави')))
            ),
            'bottomBarItems' => array(
                array('exportbutton', array('align' => 'right'))
            )
        );
        parent::__construct($sPrefix, $aInitParams);
    }


    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, aBaseParams) {
            var openProductionLine = function(productionLine, tabToOpen) {
                framework.createWindow({
                    tplName: 'emc/nomenclatures/productionlines/Edit',
                    baseParams: {
                        row: productionLine || {},
                        selectedTab: tabToOpen,
                        getParentGrid:function(){return container.grid;},
                    }
                });
            };

            var setClickListener = function (fieldName) {
                container.grid.clickListeners[fieldName] = function(rowNum, fieldElement, htmlElement) {
                    openProductionLine(container.grid.getCurrentData()[rowNum], fieldName);
                };
            }

            $(container).find('.add').click(function() {
                openProductionLine();
            });

            setClickListener('code');
            setClickListener('sizes_count');
            setClickListener('work_shifts');
        });
JS;
    }

}