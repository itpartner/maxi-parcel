<?php
namespace emc\nomenclatures\productionlines\workshifts;

use main\grid\TPLGridPanel;

class TPLTab extends TPLGridPanel {

    public function __construct($sPrefix, $aInitParams = array()) {
        $this->{'aConfig'} = array(
            'title' => 'Смени',
            'autoVResize' => true,
            'disablePaging' => true,
            'dataHeight' => '219px',
            'hideRowIndexes' => true,
            'topBarItems' => array(),
            'bottomBarItems' => array()
        );

        $this->{'bNoDefaultTopBarItems'} = true;

        parent::__construct($sPrefix, $aInitParams);
    }


    protected function printStyles($sPrefix) {
        ?>
        <style>
            #<?=$this->sPrefix?> .width100 {
                box-sizing: border-box;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                width: 100%;
            }

            #<?=$this->sPrefix?> td {
                vertical-align: top;;
                padding-right: 3px;
                padding-bottom: 10px;
            }

            #<?=$this->sPrefix?> input.shift[type="text"] {
                width: 40px;
            }

            #<?=$this->sPrefix?> .disabled {
                opacity: 0.8;
            }

            #<?= $this->sPrefix; ?> .toolbar:after {
                clear: both;
                display: block;
                content: ''
            }

        </style>
        <?php
    }


    public function printHtml() {
        ?>
        <table style="width: 100%">
            <tr>
                <td style="height: 305px;">
                    <fieldset style="height: 100%; overflow: auto;">
                        <legend>Смени</legend>
                        <table>
                            <?php foreach (array('R' => 'Редовна смяна (Р)', '1' => 'Първа смяна (1)', '2' => 'Втора смяна (2)', '3' => 'Трета смяна (3)') as $k => $v): ?>
                                <tr>
                                    <td>
                                        <span style="line-height: 26px;">
                                        <? $this->printElement('input', array('type' => 'checkbox', 'name' => $k, 'class' => 'shift shift_' . $k)); ?>
                                        <? $this->printElement('label', array('fieldName' => $k, 'text' => $v, 'class' => 'shift shift_' . $k)); ?>
                                        </span>
                                    </td>
                                    <td>
                                        <? $this->printElement('keyvaluepairs', array(
                                                'name' => $k . '_times',
                                                'style' => 'width:250px',
                                                'options' => array(
                                                    array('key' => 'from', 'label' => 'от:', 'style' => 'width:40px', 'keyRestriction' => 'time'),
                                                    array('key' => 'to', 'label' => 'до:', 'style' => 'width:40px', 'keyRestriction' => 'time'),
                                                )
                                            )
                                        ); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </fieldset>
                </td>
                <td style="min-height: 160px;">
                    <fieldset style="height: 100%;">
                        <legend>Изключения</legend>
                        <table style="width: 100%;">
                            <tr>
                                <td><? $this->printElement('select', array('name' => 'month')); ?></td>
                                <td style="text-align: right;"><? $this->printElement('button', array('class' => 'new_exception', 'iconCls' => 'icon fa fa-plus-circle', 'text' => L('Ново'))) ?></td>
                            </tr>
                        </table>
                        <?php $this->printResultTable(empty($this->oGridPanelHandler) ? $this->sServerClassName : $this->oGridPanelHandler, $this->{'aConfig'}); ?>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <? $this->printElement('span', array('text' => 'Работни дни от понеделник до петък, почивни - събота и неделя.')); ?>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 0;">
                    <div class="toolbar align-right" style="margin-top: 5px">
                        <? $this->printElement('closebutton'); ?>
                        <? $this->printElement('confirmbutton'); ?>
                    </div>
                </td>
            </tr>
        </table>

        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, baseParams) {
            var $form = $(container);
            
            container.addListener('beforeRequest',function(request){
                request['id'] = baseParams['row']['id'];
                return request;
            });
            
            var openWorkShiftExceptionsWnd = function(row) {
                framework.createWindow({
                    tplName: 'emc/nomenclatures/productionlines/workshifts/WorkShiftException',
                    width: 400,
                    baseParams: {
                        id_line: baseParams['row']['id'],
                        date: row['date'] || 0,  
                        getParentGrid: function() { return container.grid; }
                    }
                });
            }
            
            $form.find('.new_exception').click(function() {
                openWorkShiftExceptionsWnd([]);
            });
            
            container.grid.clickListeners['date'] = function(rowNum, fieldElement, htmlElement) {
                openWorkShiftExceptionsWnd(container.grid.getCurrentData()[rowNum]);
            }

            // add zeroes (12 --> 12:00)
            $form.find('.add').click(function() {
                $form.find('.keyvaluepairs .options span').each(function(k, el) {
                    var pattern = '00:00 - 00:00';
                    var time = el.textContent.split(' - ');
                    var j = 0;
                    for (var t in time) {
                        var value = time[t].toString();
                        var l = value.length;
                        for (var i = 0; i < l; i++) {
                            pattern = pattern.substr(0, j) + value[i] + pattern.substr((j + 1));
                            j++;
                        }
                        j = 8;
                    }

                    el.textContent = pattern;
                });
            });

            $form.find('.shift[type="checkbox"]').change(function(){
                enableDisable();
            });

            $form.find('[name=month]').change(function() {
                container.grid.loadData();
            })

            container.grid.clickListeners['del'] = function(rowNum, fieldElement, htmlElement) {
                container.request('deleteWorkShiftException', {date: container.grid.getCurrentData()[rowNum].date}, null, function() {
                    container.grid.loadData({id:baseParams['row']['id']});
                });
            }

            var enableDisable = function(){
                $form.find('.shift[type="checkbox"]').each(function(){
                    $els = $(this).closest('tr').find('[type!="checkbox"]');
                    if(container.fields[this.name].getValue()) {
                        $els.removeAttr('disabled');
                    } else {
                        $els.attr('disabled','disabled');
                        
                    }
                });  
                 
            };

            window.setTimeout(function(){
                enableDisable();
            },0);

            $form.find('.confirm').click(function(){
                container.request('save', {id: baseParams['row']['id'], exeptions: container.grid.getCurrentData()}, null, function() {
                    baseParams.getParentGrid().loadData();
                });
            });

            $form.find('.close').click(function() { baseParams.close();});
        });
JS;
    }


}