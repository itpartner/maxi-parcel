<?php
namespace emc\nomenclatures\productionlines\workshifts;

use main\TPLBase;

class TPLWorkShiftException extends TPLBase {

    public function __construct($sPrefix, array $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);

        $this->bPrintWithTitle = true;
        $this->aFrameConfig['title'] = L('Нови изключения за тръбна линия');
    }

    protected function printStyles($sPrefix) { ?>
        <style>
            #<?= $sPrefix; ?> .align-top {
                vertical-align: top;
            }
        </style>
        <?php
    }

    public function printHtml() {

        ?>

        <table style="width: 100%;">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td><? $this->printElement('label', array('for' => $this->sPrefix . '-date', 'text' => L('Дата'))); ?></td>
                            <td><? $this->printElement('input', array('id' => $this->sPrefix . '-date', 'name' => 'date', 'class'=>'datePicker')); ?></td>
                        </tr>

                    </table>
                    <div style="height: 300px; max-height: 300px; overflow: auto;">
                        <table>
                            <tr>
                                <td class="align-top" colspan="2">
                                            <span style="line-height: 26px;">
                                            <? $this->printElement('input', array('type' => 'checkbox', 'name' => 'holiday', 'class' => 'shift shift_holiday')); ?>
                                            <? $this->printElement('label', array('fieldName' => 'holiday', 'text' => 'Почивен ден', 'class' => 'shift shift_holiday')); ?>
                                            </span>
                                </td>
                            </tr>
                            <?php foreach (array('R' => 'Редовна смяна (Р)', '1' => 'Първа смяна (1)', '2' => 'Втора смяна (2)', '3' => 'Трета смяна (3)') as $k => $v): ?>
                                <tr>
                                    <td class="align-top">
                                            <span style="line-height: 26px;">
                                            <? $this->printElement('input', array('type' => 'checkbox', 'name' => $k, 'class' => 'shift shift_' . $k)); ?>
                                            <? $this->printElement('label', array('fieldName' => $k, 'text' => $v, 'class' => 'shift shift_' . $k)); ?>
                                            </span>
                                    </td>
                                    <td>
                                        <? $this->printElement('keyvaluepairs', array(
                                                'name' => $k . '_times',
                                                'style' => 'width:250px',
                                                'options' => array(
                                                    array('key' => 'from', 'label' => 'от:', 'style' => 'width:40px', 'keyRestriction' => 'time'),
                                                    array('key' => 'to', 'label' => 'до:', 'style' => 'width:40px', 'keyRestriction' => 'time'),
                                                )
                                            )
                                        ); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="toolbar align-right" style="margin-top: 5px">
                        <? $this->printElement('input', array('type' => 'checkbox', 'name' => 'apply_to_all')); ?>
                        <? $this->printElement('label', array('fieldName' => 'apply_to_all', 'text' => L('Приложи към всички линии'))); ?>

                        <? $this->printElement('closebutton'); ?>
                        <? $this->printElement('confirmbutton'); ?>
                    </div>
                </td>
            </tr>
        </table>
        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

            (function(form, prefix, baseParams) {
                var $form = $(form);
                var idLine = baseParams['id_line'];
                
                $form.find('.confirm').click(function(){
                    form.request('save', { id: idLine}, null, function() {
                        baseParams.getParentGrid().loadData({id_line: baseParams['id']});
                        form.window.close();
                    });
                });

                $form.find('input[name="holiday"], .shift[type="checkbox"]').change(function(){
                    enableDisable();
                });

                // add zeroes (12 --> 12:00)
                $form.find('.add').click(function() {
                    $form.find('.keyvaluepairs .options span').each(function(k, el) {
                        var pattern = '00:00 - 00:00';
                        var time = el.textContent.split(' - ');
                        var j = 0;
                        for (var t in time) {
                            var value = time[t].toString();
                            var l = value.length;
                            for (var i = 0; i < l; i++) {
                                pattern = pattern.substr(0, j) + value[i] + pattern.substr((j + 1));
                                j++;
                            }
                            j = 8;
                        }

                        el.textContent = pattern;
                    });
                });

                var enableDisable = function(){
                    $.each(['1','2','3','R'], function(k,t){
                        if(form.fields['holiday'].getValue()) {
                            form.fields[t].setValue(false);
                            $(form.fields[t]).attr('disabled','disabled');
                        } else {
                            $(form.fields[t]).removeAttr('disabled');
                        }
                    });
                
                    $form.find('.shift[type="checkbox"]').each(function(){
                        $els = $(this).closest('tr').find('[type!="checkbox"]');
                        if(form.fields[this.name].getValue() && !$(this).is(':disabled')) {
                            $els.removeAttr('disabled');
                        } else {
                            $els.attr('disabled','disabled');
                        }
                    });
                };

                window.setTimeout(function(){
                    enableDisable();
                },0);
            });
JS;
    }


}