<?php
namespace emc\nomenclatures\productionlines\workshifts;

use emc\nomenclatures\productionlines\CanEditProductionLine;
use emc\production\schedule\Scheduler;
use main\Access;
use main\db\DBC;
use main\Exception;
use main\grid\GridPanelHandler;
use main\InvalidParameter;
use main\Util;

/**
 * Class Edit
 * @accessLevelDescription Таб работни смени на машини
 */
class Tab extends GridPanelHandler {


    public function __construct() {
        $this->oBase = DBC::$main;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'date',
                'width' => 70,
                'headerText' => 'Дата',
                'sortable' => true,
                'resizable' => false,
                'filterable' => false,
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_HTML,
                'dataField' => 'work_shifts',
                'width' => 150,
                'headerText' => 'Смени',
                'sortable' => false,
                'resizable' => true,
                'filterable' => false
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_BTN,
                'dataField' => 'del',
                'width' => 30,
                'iconCls' => 'icon fa fa-trash',
                'headerText' => '',
                'sortable' => false,
                'resizable' => false,
                'filterable' => false
            ))
        );

        $this->sReportSelectStatement = "
            SELECT
                id, 
                DATE_FORMAT(`date`, '%d-%m-%Y') AS `date`,
                if(`type`='holiday', 
                        'Почивен',
                        GROUP_CONCAT(IF(`type` = 'R', 'Р', `type`), '(', SUBSTR(`from`, 1, 5), ' - ', SUBSTR(`to`, 1, 5), ')' ORDER BY `type`,`from` SEPARATOR '<br/>')
                    ) AS work_shifts
            FROM production_lines_work_shifts
        ";

        $this->aDefaultSort[] = ['field' => 'date', 'dir' => 'ASC'];

        $this->aReportWhereStatement[] = "`date` != 0";
        $this->aReportWhereStatement[] = "to_arc = 0";
        $this->sReportGroupStatement = "`date`";

        parent::__construct();
    }

    private function updateMonths($aRequest) {
        $aRequest['id'] = intval($aRequest['id']);
        $months = array_map(function ($m) {
            $m['value'] = Util::$monthNames[$m['month']] . ' ' . $m['value'];
            return $m;
        }, DBC::$main->selectAssoc("
            select t.id as _k, t.* from (
                select 
                date_format(date, '%Y-%m-01') as id,
                date_format(date,'%Y') as value,
                date_format(date,'%c') as month
                from production_lines_work_shifts
                where id_line = {$aRequest['id']} and date != 0 and to_arc = 0
                order by date asc
            ) t group by id
        "));

        $months = empty($months) ? [['id' => date('Y-m-01'), 'value' => Util::$monthNames[date('m')] . ' ' . date('Y')]] : $months;
        $this->setFieldContent('month', array_values($months));

        $aRequest['fields']['month'] = $aRequest['fields']['month'] ? $aRequest['fields']['month'] : ($months[date('Y-m-01')] ? $months[date('Y-m-01')]['id'] : end($months)['id']);
        $this->setFieldValue('month', $aRequest['fields']['month']);

        return $aRequest;
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        $nLineId = intval($aInitParams['row']['id']);
        $aRequest['id'] = $nLineId;

        $this->setShifts($this->getShifts($nLineId));

        return $this->loadData($aRequest);
    }

    protected function setShifts($shifts) {
        foreach ($shifts as $type => $times) {
            $this->setFieldValue($type . '_times', array_values($times));
            $this->setFieldValue($type, !empty($times));
        }
    }


    public static function getShifts($nLineId, $date = 0) {
        $date = !empty($date) ? DBC::$main->quote($date) : 0;
        return DBC::$main->select("
            select 
                plws.type as _k,
                plws.id as __k,
                plws.*,
                DATE_FORMAT(plws.from,  '%H:%i') as `from`,
                DATE_FORMAT(plws.to,  '%H:%i') as `to`
            FROM production_lines_work_shifts plws
            WHERE 1
            AND plws.to_arc = 0
            AND plws.date = $date
            AND plws.id_line = $nLineId
            ORDER BY plws.id
        ", null, 2);
    }


    public function loadData($aRequest) {
        $aRequest = $this->updateMonths($aRequest);

        $sFromDate = DBC::$main->quote(date('Y-m-d', strtotime($aRequest['fields']['month'])));

        $this->aReportWhereStatement[] = "id_line = " . intval($aRequest['id']);
        $this->aReportWhereStatement[] = "`date` BETWEEN $sFromDate AND LAST_DAY($sFromDate)";

        return parent::loadData($aRequest);
    }

    public function save($aRequest) {
        $nLineId = intval($aRequest['id']);

        return DBC::$main->withTrans(function ($aRequest, $nLineId) {
            Access::checkAccess(CanEditProductionLine::class);

            $shiftsTimes = [];

            $aRequest['date'] = $aRequest['date'] ? date('Y-m-d', strtotime($aRequest['date'])) : '0000-00-00';

            DBC::$main->multiInsert('production_lines_work_shifts', array_map(function ($s) {
                $s['to_arc'] = 1;
                return $s;
            }, DBC::$main->select("SELECT id FROM production_lines_work_shifts plws WHERE plws.to_arc = 0 AND plws.date = '{$aRequest['date']}' AND plws.id_line = $nLineId")));

            $shiftTypes = array('R', '1', '2', '3');
            if(in_array('holiday', array_keys($aRequest['fields']))) $shiftTypes[] = 'holiday';
            foreach ($shiftTypes as $type) {
                if(empty($aRequest['fields'][$type . '_times'])) continue;
                $aRequest['fields'][$type . '_times'] = array_map(function ($time) use ($type, $nLineId, $aRequest) {
                    $time['type'] = $type;
                    $time['id_line'] = $nLineId;
                    $time['date'] = $aRequest['date'];
                    $time['to_arc'] = intval(empty($aRequest['fields'][$type]));
                    return $time;
                }, $aRequest['fields'][$type . '_times']);
                $shiftsTimes = array_merge($shiftsTimes, $aRequest['fields'][$type . '_times']);
            }

            foreach ($shiftsTimes as $k => $time) {
                $shiftsTimes[$k]['id'] = null;
                $shiftsTimes[$k]['from'] = $this->getTimeString($time['from']);
                $shiftsTimes[$k]['to'] = $this->getTimeString($time['to']);
            }

            foreach ($shiftsTimes as $k => $time) {
                foreach (array('from', 'to') as $key) {
                    $t = strtotime($time[$key]);
                    if($key == 'to' && $time['to'] < $time['from']) $t += 24 * 60 * 60;
                    foreach ($shiftsTimes as $kk => $time2) {
                        if($k == $kk) continue;
                        $from = strtotime($time2['from']);
                        $to = strtotime($time2['to']);
                        if($to < $from) $to += 24 * 60 * 60;
                        if(($t > $from && $t < $to) || ($key == 'to' && $t == $to)) throw new InvalidParameter("Часови интервал " . $time['from'] . ' - ' . $time['to'] . ' се застъпва с ' . $time2['from'] . ' - ' . $time2['to']);
                    }
                }
            }

            if(empty($shiftsTimes)) throw new InvalidParameter('Не са посочени смени и/или часови диапазони');

            DBC::$main->multiInsert('production_lines_work_shifts', $shiftsTimes);

            $day = $aRequest['date'] != '0000-00-00' ? strtotime($aRequest['date']) : time();
            $start = strtotime("-1 week", $day);
            $end = strtotime("+1 week", $day);

            $validationShifts = Scheduler::generateShifts($nLineId, $start, $end - $start);

            $ex = [];
            foreach ($validationShifts as $k => $shift) {
                if(empty($k) || empty($validationShifts[$k + 1])) continue;
                try {
                    if($validationShifts[$k + 1]['shift_from'] < $shift['shift_to']) {
                        throw new InvalidParameter("Недопустимо застъпване на интервали: \n" .
                            $shift['shift_from'] .' - '. $shift['shift_to'] . "\n с интервал \n" .
                            $validationShifts[$k + 1]['shift_from'] .' - '. $validationShifts[$k + 1]['shift_to']);
                    }
                } catch (InvalidParameter $e) {
                    $ex[] = $e;
                }
            }

            Exception::checkThrow($ex);

            Scheduler::scheduleLine($nLineId);

            $this->setShifts($this->getShifts($nLineId));

            return $this->oResponse;

        }, array($aRequest, $nLineId));
    }

    public function deleteWorkShiftException($aRequest) {
        $wse = new WorkShiftException();
        $wse->delete($aRequest);

        return $this->loadData($aRequest);
    }

    private function getTimeString($time) {
        $sPattern = '00:00';
        $nSymbols = strlen($time);
        for ($i = 0; $i < $nSymbols; $i++) $sPattern[$i] = $time[$i];

        return $sPattern;
    }

}