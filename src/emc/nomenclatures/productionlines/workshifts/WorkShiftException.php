<?php
namespace emc\nomenclatures\productionlines\workshifts;

use emc\production\schedule\Scheduler;
use main\Access;
use main\db\DBC;
use main\form\FormPanelHandler;
use main\InvalidParameter;
use main\Util;

/**
 * @accessLevelDescription Работни смени (изключения) за тръбна линия
 */
class WorkShiftException extends FormPanelHandler {

    function __construct() {
        parent::__construct();
        Access::checkAccess(__CLASS__);
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        if (!empty($aInitParams['date'])) {
            $aInitParams['date'] = date('Y-m-d', strtotime($aInitParams['date']));
            $this->setFieldValue('date', $aInitParams['date']);
            $this->setShifts(Tab::getShifts($aInitParams['id_line'], $aInitParams['date']));
        }

        return $this->oResponse;
    }

    protected function setShifts($shifts) {
        foreach ($shifts as $type => $times) {
            $this->setFieldValue($type . '_times', array_values($times));
            $this->setFieldValue($type, !empty($times));
        }
    }


    public function save($aRequest) {
        $aRequest['date'] = $aRequest['fields']['date'];

        DBC::$main->withTrans(function ($aRequest) {
            if(empty($aRequest['id'])) throw new InvalidParameter;
            if(!Util::SQLDateToTimeStamp($aRequest['date'])) throw new InvalidParameter("Моля, посочете дата.", array('date'));

            if(!empty($aRequest['fields']['holiday'])) {
                $aRequest['fields']['holiday_times'][] = ['from' => 0, 'to' => 0];
            }

            $oWorkShifts = new Tab();
            if(!empty($aRequest['fields']['apply_to_all'])) {
                foreach (DBC::$main->selectColumn("SELECT id FROM production_lines WHERE to_arc = 0 AND status = 'active'") as $id_line) {
                    $r = $aRequest;
                    $r['id'] = $id_line;
                    $oWorkShifts->save($r);
                }
            } else {
                $oWorkShifts->save($aRequest);
            }

            $this->setShifts(Tab::getShifts($aRequest['id'], $aRequest['date']));
        }, array($aRequest));

        return $this->oResponse;
    }

    public function delete($aRequest) {
        if(empty($aRequest['id']) || empty($aRequest['date'])) throw new InvalidParameter;

        $idLine = intval($aRequest['id']);
        $aRequest['date'] = date('Y-m-d', strtotime($aRequest['date']));
        $date = DBC::$slave->quote($aRequest['date']);

        DBC::$main->multiInsert('production_lines_work_shifts', array_map(function ($row) {
            $row['to_arc'] = 1;
            return $row;
        }, DBC::$main->select("SELECT * FROM production_lines_work_shifts WHERE `date` = {$date} AND id_line = {$idLine} AND to_arc = 0")));
        Scheduler::scheduleLine($idLine);
    }

}