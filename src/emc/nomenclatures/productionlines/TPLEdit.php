<?php
namespace emc\nomenclatures\productionlines;

use main\TPLBase;

class TPLEdit extends TPLBase {

    public function __construct($sPrefix, array $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);

        $this->bPrintWithTitle = true;
        $this->aFrameConfig['title'] = empty($aInitParams['row']['id']) ? L('Нова тръбна линия') : L('Редакция на тръбна линия: %s', $aInitParams['row']['code']);
    }

    public function printHtml() {

        $aTabs = array(
            'code' => array('tplName' => 'information', 'title' => L('Информация')),
            'sizes_count' => array('tplName' => 'linesizes', 'title' => L('Размери')),
            'work_shifts' => array('tplName' => 'workshifts', 'title' => L('Работен график')),
            'documentation' => array('tplName' => 'documentation', 'title' => L('Документация'))
        );

        ?>
        <style>
            #<?=$this->sPrefix?>
            .tab {
                width: 909px;
                height: 400px;
            }
        </style>
        <div id="<?= $this->sPrefix ?>">
            <div id="<?= $this->sPrefix ?>-tabs">
                <ul>
                    <?php
                    foreach ($aTabs as $id => $aTab): ?>
                        <li tooltip="<?= htmlspecialchars($aTab['tooltip']) ?>">
                            <a href="#<?= $this->sPrefix ?>-tab-<?= $id ?>">
                                <?= $aTab['title']; ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <?php
                foreach ($aTabs as $id => $aTab): ?>
                    <div class="tab" id="<?= $this->sPrefix ?>-tab-<?= $id ?>" name="<?= $aTab['tplName'] ?>"></div>
                <?php endforeach; ?>
            </div>
        </div>


        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, initParams) {
            var form = $(container);
    
            initParams.closeTabs = function() {
                container.window.close();
            };

            var initTabs = function() {
                jQuery('#' + prefix + '-tabs').bind('tabsshow', function(e,ui) {
                    if(!ui.panel.innerHTML) {
                        layout.loadContent({
                                tplName: 'emc/nomenclatures/productionlines/' + ui.panel.getAttribute('name') + '/Tab',
                                baseParams: {
                                    row: initParams['row'],
                                    enableTabs: function(tabs) {debugger; for(var i in tabs) {jQuery('#' + prefix + '-tabs').tabs('enable', tabs[i]);}},
                                    setNewLineId: function(newId) {initParams['row']['id'] = newId; newLineId = newId;},
                                    getParentGrid: function() { return initParams.getParentGrid(); },
                                    close: function() {container.window.close();}
                                }
                            },
                            ui.panel
                        );
                    } else {
                        jQuery(ui.panel).children('.content-wrapper').trigger('tab_activate');
                    }
                });
            }();
            jQuery('#' + prefix + '-tabs').tabs({
                selected:initParams.selectedTab ? jQuery('#'+prefix+'-tabs').children('.tab').index(jQuery('#'+prefix+'-tabs  #'+prefix+'-tab-'+initParams.selectedTab)) : 0,
                disabled: !initParams['row']['id'] ? [1, 2] : []
            });
        });
JS;
    }


}