<?php
    namespace emc\nomenclatures\productionlines\linesizes;

    use main\grid\TPLGridPanel;

    class TPLTab extends TPLGridPanel {

        public function __construct($sPrefix, array $aInitParams) {
            $this->{'aConfig'} = array(
                'autoVResize' => true,
                'hideRowIndexes' => true,
                'dataHeight' => '249px',
                'topBarItems' => array(
                    array('button', array('name' => 'add', 'class' => 'add', 'iconCls' => 'icon fa fa-plus-circle', 'text' => L('Добави')))
                ),
                'bottomBarItems' => array(
                    array('exportbutton', array('align' => 'right'))
                )
            );

            $this->{'bNoDefaultTopBarItems'} = true;

            parent::__construct($sPrefix, $aInitParams);
        }

        protected function printStyles($sPrefix) { ?>
            <style>
                #<?= $this->sPrefix; ?> .toolbar:after {
                    clear: both;
                    display: block;
                    content: ''
                }
            </style>
        <?php }

        public function printHtml() { ?>
            <table style="width: 100%;">
                <tr>
                    <td>
                        <?php $this->printResultTable(empty($this->oGridPanelHandler) ? $this->sServerClassName : $this->oGridPanelHandler, $this->{'aConfig'});?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="toolbar align-right" style="margin-top: 9px">
                            <? $this->printElement('lasteditedinfo'); ?>
                            <? $this->printElement('closebutton'); ?>
                        </div>
                    </td>
                </tr>
            </table>
        <?php
        }

        public function getJSFunction() {
            return <<<'JS'

            (function(container, prefix, baseParams) {
                form = container;

                $(container).find('.confirm').click(function(){
                    container.request('save', { id: baseParams['row']['id'] }, null, function() {
                        baseParams.getParentGrid().loadData();
                    });
                });

                $(container).find('.close').click(function() { baseParams.close(); });

                $(container).find('.add').click(function() {
                    framework.createWindow({
                        tplName: 'emc/nomenclatures/productionlines/linesizes/Edit',
                        baseParams: {id_line: baseParams['row']['id'], getParentGrid: function() { return container.grid; }}
                    });
                });

                container.grid.clickListeners['code'] = function(rowNum, fieldElement, htmlElement) {
                    framework.createWindow({
                        tplName: 'emc/nomenclatures/sizes/Edit',
                        baseParams: { row: {id: container.grid.getCurrentData()[rowNum]['id_size']}, getParentGrid:function(){return container.grid;}}
                    });
                }
                
                container.addListener('beforeRequest',function(request){
                    request.id_line = baseParams['row']['id'];
                });
                
            });
JS;
        }


    }