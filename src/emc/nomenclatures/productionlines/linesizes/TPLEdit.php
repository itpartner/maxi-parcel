<?php
    namespace emc\nomenclatures\productionlines\linesizes;

    use main\TPLBase;

    class TPLEdit extends TPLBase {

        public function __construct($sPrefix, array $aInitParams) {
            parent::__construct($sPrefix, $aInitParams);

            $this->bPrintWithTitle = true;
            $this->aFrameConfig['title'] = L('Размери за линия');
        }

        protected function printStyles($sPrefix) { ?>
            <style>

                #<?= $sPrefix; ?> .sides {
                    border-collapse: collapse;
                    height: 100%;
                }

                #<?= $sPrefix; ?> .sizes {
                    max-height: 350px;
                    overflow: auto;
                }

                #<?= $sPrefix; ?> .sides td, th {
                    padding: 2px;
                }


                #<?= $sPrefix; ?> > table tr > td {
                    vertical-align: top;
                    padding-left: 5px;
                    padding-right: 5px;
                }

                #<?= $sPrefix; ?> .input-full-parent-width {
                    -moz-box-sizing: border-box;
                    -webkit-box-sizing: border-box;
                    box-sizing: border-box;
                    width: 100%;
                }

                #<?= $sPrefix; ?> .size-code-column {
                    width: 75%;
                }

                #<?= $sPrefix; ?> .table-header {
                    text-align: center;
                }

                #<?= $sPrefix; ?> .info {
                    font-size: 10px;
                    width: 250px;
                    float: right;
                }

                #<?= $sPrefix; ?> .info-container:after {
                    display: block;
                    clear: both;
                    content: ''
                }

            </style>
            <?php
        }

        public function printHtml() {

            ?>

            <table style="width: 100%;">
                <tr>
                    <td style="width:235px;">
                        <div class="sizes">
                            <table class="sides">
                                <tr>
                                    <td>
                                        <? $this->printElement('input', array('name' => 'size', 'class' => 'input-full-parent-width',
                                            'style' => 'height: 26px;', 'placeholder' => L('Филтриране на размери'))); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 300px;"><? $this->printElement('select', array('name' => 'sizes', 'multiple' => 'multiple', 'style' => 'height: 100%;width:250px;')); ?></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td style="vertical-align: middle; width: 105px;">
                        <table class="sides" style="border: 1px solid #888;">
                            <tr><td><? $this->printElement('label', array('fieldName' => 'speed', 'text' => L('Скорост:'))); ?></td></tr>
                            <tr>
                                <td><? $this->printElement('input', array('name' => 'speed', 'style' => 'width: 50px;', 'keyRestriction' => 'float')); ?>
                                    <? $this->printElement('span', array('style' => 'margin-left: 2px;', 'text' => L('м/мин'))); ?></td></tr>
                            <tr>
                                <td style="text-align: center;"><? $this->printElement('button', array('class' => 'add', 'iconCls' => 'icon fa fa-angle-double-right',
                                        'text' => L('Добави'))); ?></td>
                            </tr>
                        </table>
                        <div style="text-align: center; margin-top: 10px;">
                            <? $this->printElement('button', array('class' => 'remove', 'iconCls' => 'icon fa fa-angle-double-left',
                                'text' => L('Премахни'))); ?>
                        </div>
                    </td>
                    <td>
                        <div class="sizes">
                            <table class="sides">
                                <tr>
                                    <td>
                                        <? $this->printElement('label', array('fieldName' => 'groups', 'text' => 'Група')); ?>
                                        <? $this->printElement('select', array('name' => 'groups')); ?>
                                        <? $this->printElement('input', array('name' => 'group_filter', 'placeholder' => L('Филтър'))); ?>
                                        <? $this->printElement('button', array('name' => 'change_speed', 'iconCls' => 'icon fa fa fa-tachometer', 'tooltip' => L('Редакция на скорост'))); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 300px;"><? $this->printElement('select', array('name' => 'groups_sizes', 'multiple' => 'multiple', 'style' => 'height: 100%; width: 310px;')); ?></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <div class="toolbar align-right" style="margin-top: 5px">
                            <? $this->printElement('closebutton'); ?>
                            <? $this->printElement('confirmbutton'); ?>
                        </div>
                    </td>
                </tr>
            </table>
            <?php
        }

        public function getJSFunction() {
            return <<<'JS'

            (function(container, prefix, baseParams) {
                var form = container;

                form.fields.sizes.valueField = 'id_size';
                form.fields.sizes.labelField = 'code';

                form.fields.groups_sizes.valueField = 'id_size';
                form.fields.groups_sizes.labelField = 'code';

                var sizes = [];
                var groupsSizes = [];
                var filteredGroupsSizes = [];
                var init = function(result){
                    init = function(){};
                    sizes = result.sizes;
                    groupsSizes = result.groupsSizes;
                    groupsSizes[0] = [];
                    form.fields.sizes.setContent(Object.keys(sizes).map(function(key){
                        return sizes[key];
                    }));
                };

                form.addListener('afterRequest', function(result){
                    init(result);
                    enableDisable();
                });

                // Филтриране на размерите
                $(form).find('[name="size"]').bind('change', function() {
                    form.request('filterSizes', null, null, function(result) {
                        sizes = result.sizes;
                        form.fields.sizes.setContent(Object.keys(sizes).map(function(key){
                            return sizes[key];
                        }));
                        enableDisable();
                    }, null, null);
                });

                // Промяна на скорост за избрани размери
                $(form).find('button[name=change_speed]').click(function(event) {
                    framework.prompt("Промяна на скорост", "Нова скорост за избраните размери", function(r) {
                        if (!r) return;

                        $(form.fields["groups_sizes"].getValue()).each(function(index, id_size) {
                            groupsSizes[currentGroup][id_size].speed = parseInt(r);
                            var code = groupsSizes[currentGroup][id_size].code;
                            groupsSizes[currentGroup][id_size].code = "[" + parseInt(r) + " " + code.substring(code.indexOf('m'), code.length);
                        });
                        enableDisable();
                    });
                    $(".framework_prompt_input").attr("keyrestriction", "float");
                });

                // Филтриране на размерите
                $(form).find('[name="group_filter"]').bind('keyup', function() {
                    filteredGroupsSizes = [];

                    var groupSizes_ = Object.keys(groupsSizes[currentGroup]).map(function(key){
                        return groupsSizes[currentGroup][key];
                    })

                    $(groupSizes_).each(function(index, data){
                        if(data.value.toLowerCase().indexOf(form.fields.group_filter.getValue().toLowerCase()) != -1)
                            filteredGroupsSizes[data.id_size] = groupsSizes[currentGroup][data.id_size];
                    });

                    enableDisable();
                });

                var enableDisable = function(){
                    var group_filter = form.fields["group_filter"].getValue();

                    if(group_filter == "") {
                        form.fields.groups_sizes.setContent(
                            Object.keys(groupsSizes[currentGroup]).map(function(key){
                                return groupsSizes[currentGroup][key];
                            })
                        );
                    } else {
                        form.fields.groups_sizes.setContent(
                            Object.keys(filteredGroupsSizes).map(function(key){
                                return filteredGroupsSizes[key];
                            })
                        );
                    }
                };

                var currentGroup = form.fields.groups.getValue() || 0;
                $(form).find('[name="groups"]').change(function() {
                    currentGroup = form.fields.groups.getValue();
                    enableDisable();
                });

                $(form).find('.confirm').click(function() {
                    form.request('save', {id_line: baseParams['id_line'], groups_sizes: groupsSizes}, null, function() {
                        baseParams.getParentGrid().loadData();
                        form.window.close();
                    });
                });

                $(form).find('.add').click(function() {
                    if (!currentGroup) currentGroup = 0;
                    
                    if (!form.fields.speed.getValue()) {
                        framework.alert('Трябва да въведете скорост за обработка на размерите.', 'warning');
                        return;
                    }
                    
                    var selectedSizes = form.fields.sizes.getValue();
                    var alreadyAddedSizes = [];
                    
                    $.each(selectedSizes, function(k, v){
                        var size = $.extend({}, sizes[v]);
                        
                        size['speed'] = form.fields.speed.getValue();
                        size['code'] = ' [' + size['speed'] + ' m/min] ' + size['code'];
                        size['id'] = 0;
                        
                        var inOtherGroups = searchSizeInOtherGroups(size);
                        
                        var speeds = Object.keys(inOtherGroups);
                        var groups = [];

                        var bReplaceSpeeds = false;

                        $.each(speeds, function(k, speed) {
                            if (speed != size['speed']) {
                                groups.push(inOtherGroups[speed]);
                            }
                        });
                        var groupNumbers = groups.join().split(',');
                        
                        if(groups.length) {
                            framework.confirm('',"Размер "+size['value']+" присъства с различни скорости в групи " + groups.join(',') + ', които ще бъдат заменени с текущата. <br/> Желаете ли да продължите с добавянето?', function(r){
                                if(!r) return;
                                
                                $.each(groupNumbers, function(k,group){
                                    groupsSizes[group][size['id_size']]['speed'] = size['speed'];
                                    groupsSizes[group][size['id_size']]['code'] = size['code'];
                                    
                                });
                                groupsSizes[currentGroup][size['id_size']] = size;
                                enableDisable();
                            });
                            return;
                        }
                        
                        groupsSizes[currentGroup][size['id_size']] = size;
                        enableDisable();
                    });
                });

                $(form).find('.remove').click(function() {
                    var selectedSizes = form.fields.groups_sizes.getValue();
                    form.fields.groups_sizes.setValue([]);

                    $.each(selectedSizes, function(k, id_size){
                        delete groupsSizes[currentGroup][id_size];
                        delete filteredGroupsSizes[id_size];
                    });

                    enableDisable();
                });

                var searchSizeInOtherGroups = function(size) {
                    var groups = form.fields.groups.getContent();
                    var result = {};
                    
                    $.each(groupsSizes, function(group, sizes){
                        if(!sizes[size['id_size']]) return;
                        result[sizes[size['id_size']]['speed']] = result[sizes[size['id_size']]['speed']] || [];
                        result[sizes[size['id_size']]['speed']].push(group);
                    });
                    
                    return result;
                }

            });
JS;
        }


    }