<?php
namespace emc\nomenclatures\productionlines\linesizes;

use main\db\DBC;
use main\grid\GridPanelHandler;

/**
 * @accessLevelDescription Таб размери на тръбна линия
 */
class Tab extends GridPanelHandler {

    public function __construct() {
        $this->oBase = DBC::$main;

        $this->aColumns = array(

            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_INT,
                'dataField' => 'plsg.group',
                'width' => 80,
                'headerText' => L('Група'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'code',
                'width' => 200,
                'headerText' => L('Размер'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_FLOAT,
                'dataField' => 'speed',
                'width' => 90,
                'headerText' => L('Скорост'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_FLOAT,
                'dataField' => 'real_speed',
                'width' => 140,
                'headerText' => L('Реална скорост'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumnEditedBy()
        );

        $this->sReportSelectStatement = "
            SELECT SQL_CALC_FOUND_ROWS
                pls.*,
                ps.code,
                if(plsg.group > 0, plsg.group, '') as `group`, 
                ROUND(s.real_speed,2) as real_speed,
                CONCAT_WS(' ',pc.fname,pc.mname,pc.lname) AS created_user,
                CONCAT_WS(' ',pu.fname,pu.mname,pu.lname) AS updated_user
            FROM production_lines_sizes pls
            JOIN production_sizes ps ON ps.id = pls.id_size
            LEFT JOIN production_lines_sizes_groups plsg ON plsg.id_line_size = pls.id
            LEFT JOIN personnel pc ON pc.id = pls.created_user
            LEFT JOIN personnel pu ON pu.id = pls.updated_user
            LEFT JOIN (
                SELECT
                ps.id_line,
                pos.id_size,
                (sum(ps.produced_mass) / psz.m) / (SUM(psr.real_production_duration)/60) as real_speed
                FROM production_schedule ps
                JOIN production_orders_sizes pos ON pos.id = ps.id_order_size and pos.to_arc = 0 AND pos.complete = 1
                JOIN production_sizes psz ON psz.id = pos.id_size 
                JOIN (
                  SELECT
                    psr.id_schedule,
                    SUM(IF(psr.type = 'real_production' AND ps.produced_mass > 0, psr.duration, 0))  AS real_production_duration
                  FROM production_schedule_rows psr
                  JOIN production_schedule ps ON ps.id = psr.id_schedule AND ps.to_arc = 0
                  WHERE 1
                  AND psr.to_arc = 0 
                  AND psr.type = 'real_production'
                  GROUP BY ps.id
                ) psr ON psr.id_schedule = ps.id
                GROUP BY ps.id_line, pos.id_size
            ) s ON s.id_line = pls.id_line AND s.id_size = pls.id_size            
        ";

        $this->aReportWhereStatement[] = 'pls.to_arc = 0';
        $this->sReportGroupStatement = 'pls.id, plsg.group';

        $this->aDefaultSort = array(
            array('field' => 'plsg.group', 'dir' => 'ASC'),
            array('field' => 'profile_type', 'dir' => 'ASC'),
            array('field' => 'd', 'dir' => 'ASC'),
            array('field' => 'a', 'dir' => 'ASC'),
            array('field' => 'h', 'dir' => 'ASC'),
            array('field' => 's', 'dir' => 'ASC'),
        );

        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        $aRequest['id_line'] = $aInitParams['row']['id'];
        return $this->loadData($aRequest);
    }

    public function loadData($aRequest, $nTimeFrom = 0, $nTimeTo = 0) {
        $aRequest['id_line'] = intval($aRequest['id_line']);
        $this->aReportWhereStatement[] = 'pls.id_line = ' . $aRequest['id_line'];
        return parent::loadData($aRequest, $nTimeFrom, $nTimeTo);
    }

}