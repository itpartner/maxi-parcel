<?php
namespace emc\nomenclatures\productionlines\linesizes;

use emc\nomenclatures\productionlines\CanEditProductionLine;
use emc\nomenclatures\sizes\Sizes;
use emc\production\schedule\Scheduler;
use main\Access;
use main\db\DBC;
use main\form\FormPanelHandler;
use main\InvalidParameter;
use main\Util;

/**
 * Class Edit
 * @accessLevelDescription Размери за тръбна линия
 */
class Edit extends FormPanelHandler {

    public function init($aRequest = array(), $aInitParams = array()) {
        $aGroupsSizes = $this->getGroupsSizes($aInitParams['id_line']);

        $aGroups = array_keys($aGroupsSizes);
        sort($aGroups);
        $aGroups = array_map(function ($e) {
            return array('id' => $e, 'value' => ($e < 0 ? 'Без група' : $e) );
        }, $aGroups);
        array_unshift($aGroups, array('id' => 0, 'value' => L('Нова')));
        $this->setFieldContent('groups', $aGroups);

        $this->oResponse->{'groupsSizes'} = $aGroupsSizes;
        $this->filterSizes(array());

        return $this->oResponse;
    }

    protected function getGroupsSizes($idLine) {
        $idLine = intval($idLine);
        $aGroupsSizes = DBC::$main->select("
                SELECT
                    if(plsg.`group` < 0 || plsg.`group` IS NULL, -1, plsg.`group`) AS __k,
                    pls.id_size AS _k,
                    pls.id_size,
                    pls.id,
                    CONCAT('[',pls.speed,' m/min] ',ps.code) AS code,
                    ps.code AS value,
                    pls.speed
                FROM production_lines_sizes pls
                JOIN production_sizes ps ON ps.id = pls.id_size
                LEFT JOIN production_lines_sizes_groups plsg ON plsg.id_line_size = pls.id
                WHERE 1
                AND pls.to_arc = 0
                AND pls.id_line = $idLine
            ", null, 2);

        foreach ($aGroupsSizes as $k => $v) $aGroupsSizes[$k] = $v;
        if(empty($aGroupsSizes['-1'])) $aGroupsSizes['-1'] = [];
        return $aGroupsSizes;
    }

    public function save($aRequest) {
        DBC::$main->withTrans(function ($aRequest) {
            Access::checkAccess(CanEditProductionLine::class);

            $idLine = intval($aRequest['id_line']);
            $currentGroupsSizes = $this->getGroupsSizes($idLine);

            foreach ($aRequest['groups_sizes'] as $group => $sizes) {
                foreach ($sizes as $id_size => $size) {
                    if(empty($size)) {
                        unset($aRequest['groups_sizes'][$group][$id_size]);
                        continue;
                    }

                    $size['id_line'] = $idLine;
                    $size['to_arc'] = 0;

                    $aAlreadyAddedSizeIds = $this->getAlreadyAddedSizeIds($size, $currentGroupsSizes);
                    if ($aAlreadyAddedSizeIds) {
                        foreach ($aAlreadyAddedSizeIds as $mValue) {
                            $updatedSize = array('id' => $mValue, 'speed' => $size['speed']);
                            DBC::$main->update('production_lines_sizes', $updatedSize);
                            $size['id'] = $updatedSize['id'];
                        }
                    } else {
                        DBC::$main->update('production_lines_sizes', $size);
                    }

                    $aRequest['groups_sizes'][$group][$id_size]['id'] = $size['id'];
                    $aRequest['groups_sizes'][$group][$id_size]['id_line'] = $size['id_line'];
                }
            }

            $forDelete = [];
            foreach ($currentGroupsSizes as $group => $sizes) {
                foreach ($sizes as $id_size => $size) {
                    if(empty($aRequest['groups_sizes'][$group][$id_size]) && empty($aRequest['groups_sizes'][0][$id_size])) {
                        $forDelete[] = ['id' => $size['id'], 'to_arc' => 1];
                    }
                }
            }
            if(!empty($forDelete)) DBC::$main->multiInsert('production_lines_sizes', $forDelete);

            DBC::$main->execute("DELETE FROM production_lines_sizes_groups WHERE id_line_size IN (SELECT id FROM production_lines_sizes WHERE id_line = $idLine AND to_arc = 0)");

            if(!empty($aRequest['groups_sizes'][0])) {
                $aRequest['groups_sizes'][max(array_keys($aRequest['groups_sizes'])) + 1] = $aRequest['groups_sizes'][0];
                unset($aRequest['groups_sizes'][0]);
            }

            $groups = [];
            foreach ($aRequest['groups_sizes'] as $group => $sizes) {
                foreach ($sizes as $id_size => $size) {
                   $groups[] = ['id_line_size' => $size['id'], 'group' => $group];
                }
            }

            DBC::$main->multiInsert('production_lines_sizes_groups', $groups);

            $missingSizesOnTheLine = DBC::$main->selectAssoc("
                select 
                    psz.code
                from production_schedule ps
                join production_orders_sizes pos ON pos.id = ps.id_order_size and pos.to_arc = 0 and pos.complete = 0
                join production_sizes psz ON psz.id = pos.id_size
                left join production_lines_sizes pls ON pls.id_line = ps.id_line and pls.id_size = psz.id and pls.to_arc = 0
                where 1
                and ps.to_arc = 0
                and ps.id_line = $idLine
                and pls.id is null
                group by psz.code
            ");

            if(!empty($missingSizesOnTheLine)) throw new InvalidParameter("Следните размери не могат да бъдат премахнати от линията, понеже се използват в неприключени поръчки:<br/>".implode("<br/>",$missingSizesOnTheLine));

            Scheduler::scheduleLine($idLine);
        }, array($aRequest));

        return $this->oResponse;
    }

    public function filterSizes($aRequest) {
        $this->oResponse->{'sizes'} = [];
        if(empty($aRequest['fields']['size'])) return $this->oResponse;

        $r['query'] = $aRequest['fields']['size'];
        $r['limit'] = 500;
        $r['all_sizes'] = true;

        $this->oResponse->{'sizes'} = array_map(function ($size) {
            $size['code'] = $size['value'];
            $size['id_size'] = $size['id'];
            return $size;
        }, Sizes::suggestSize($r));

        $this->oResponse->{'sizes'} = Util::arrayAssocify($this->oResponse->{'sizes'}, 1);

        return $this->oResponse;
    }

    private function getAlreadyAddedSizeIds($size, $aDBSizes) {
        $aAlreadyAddedSizeIds = array();
        foreach ($aDBSizes as $group => $sizes) {
            foreach ($sizes as $id_size => $db_size) {
                if ($id_size == $size['id_size']) {
                    $aAlreadyAddedSizeIds[$db_size['id']] = $db_size['id'];
                }
            }
        }

        return $aAlreadyAddedSizeIds;
    }

}