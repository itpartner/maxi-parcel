<?php
namespace emc\nomenclatures\productionlines;

use main\Access;
use main\AccessDenied;
use main\db\DBC;
use main\grid\GridPanelHandler;

/**
 * @accessLevelDescription Тръбни линии
 */
class ProductionLines extends GridPanelHandler {

    public function __construct() {
        $this->oBase = DBC::$main;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'code',
                'width' => 100,
                'headerText' => L('Код'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'status',
                'width' => 100,
                'headerText' => L('Статус'),
                'items' => [
                    'active' => ['text' => L('работи'), 'iconCls' => 'fa fa fa-check-circle'],
                    'idle' => ['text' => L('не работи'), 'iconCls' => 'fa fa fa-ban']
                ],
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_CURRENCY,
                'dataField' => 'setup_time_small',
                'currencyField' => 'minutes_string',
                'width' => 160,
                'headerText' => L('Малка пренастройка'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_CURRENCY,
                'dataField' => 'setup_time_big',
                'currencyField' => 'minutes_string',
                'width' => 160,
                'headerText' => L('Голяма пренастройка'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_CURRENCY,
                'dataField' => 'setup_time_length',
                'currencyField' => 'minutes_string',
                'width' => 250,
                'headerText' => L('Преминаване към друга дължина'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_CURRENCY,
                'dataField' => 'setup_time_thickness',
                'currencyField' => 'minutes_string',
                'width' => 250,
                'headerText' => L('Преминаване към друга дебелина'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_INT,
                'dataField' => 'sizes_count',
                'width' => 100,
                'headerText' => L('Бр. размери'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'work_shifts',
                'width' => 100,
                'headerText' => L('Смени'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumnEditedBy()
        );

        $this->sReportSelectStatement = "
            SELECT SQL_CALC_FOUND_ROWS
                pl.*,
                (pl.efficiency * 100) AS efficiency,
                COUNT(DISTINCT pls.id) AS sizes_count,
                GROUP_CONCAT(DISTINCT IF(plws.type = 'R', 'Р', plws.type) ORDER BY type SEPARATOR '/') AS work_shifts,
                CONCAT_WS(' ',pc.fname,pc.mname,pc.lname) AS created_user,
                CONCAT_WS(' ',pu.fname,pu.mname,pu.lname) AS updated_user,
                CONCAT_WS(' ',lm.fname,lm.mname,lm.lname) AS line_manager,
                CONCAT_WS(' ',lpm.fname,lpm.mname,lpm.lname) AS line_production_manager,
                'мин' AS minutes_string
            FROM production_lines pl
            LEFT JOIN production_lines_sizes pls ON pls.id_line = pl.id AND pls.to_arc = 0
            LEFT JOIN production_lines_work_shifts plws ON plws.id_line = pl.id AND plws.date = 0 AND plws.type != 'holiday' AND plws.to_arc = 0
            LEFT JOIN personnel     pc ON pc.id = pl.created_user AND pc.to_arc = 0
            LEFT JOIN personnel     pu ON pu.id = pl.updated_user AND pu.to_arc = 0
            LEFT JOIN personnel     lm ON lm.id = pl.id_line_manager
            LEFT JOIN personnel     lpm ON lpm.id = pl.id_line_production_manager
        ";

        $this->aReportWhereStatement[] = "pl.to_arc = 0";
        $this->sReportGroupStatement = 'pl.id';

        $this->aDefaultSort = array(
            array('field' => 'created_time', 'dir' => 'ASC')
        );

        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
    }

    protected function processData($aData) {
        foreach ($aData as $mKey => $aRow) {
            if (empty($aRow['work_shifts'])) {
                $aData[$mKey]['work_shifts'] = '-----';
            }
        }

        return parent::processData($aData);
    }


    public static function getLineById($idLine){
        return DBC::$slave->selectByID('production_lines', $idLine);
    }

    public static function isUserLineProductionManager($idLine, $idUser = 0) {
        if(empty($idUser)) $idUser = $_SESSION['userdata']['id'];
        return self::getLineById($idLine)['id_line_production_manager'] == $idUser || Access::hasAccess('/');
    }

    public static function checkIsUserLineProductionManager($idLine, $idUser = 0) {
        if(!self::isUserLineProductionManager($idLine, $idUser)){
            throw new AccessDenied("Потребителя не е зададен като мениджър производство за избраната производствена линия");
        }
    }

    public static function getPossibleLinesForSize($idSize) {
        $idSize = intval($idSize);
        return DBC::$main->selectAssoc("
            SELECT 
            pl.id as _key,
            pl.*
            FROM production_lines_sizes pls
            JOIN production_lines pl ON pl.id = pls.id_line AND pl.to_arc = 0 AND pl.status = 'active'
            WHERE 1
            AND pls.to_arc = 0
            AND pls.id_size = $idSize
            GROUP BY pl.id
        ");
    }

}