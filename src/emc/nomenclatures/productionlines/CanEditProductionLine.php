<?php
namespace emc\nomenclatures\productionlines;
use emc\access\AccessLevel;

/**
 * Class CanEditProductionLine
 * @package emc\nomenclatures\productionlines
 * @accessLevelDescription Право за редакция на параметри по тръбна линия
 */
class CanEditProductionLine implements AccessLevel {

}