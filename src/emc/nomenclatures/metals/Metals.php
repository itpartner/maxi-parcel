<?php
namespace emc\nomenclatures\metals;

use main\db\DBC;
use main\grid\GridPanelHandler;

/**
 * @accessLevelDescription Производствени материали
 */
class Metals extends GridPanelHandler {

    public function __construct() {
        $this->oBase = DBC::$slave;

        $this->aColumns = array(

            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'code',
                'width' => 100,
                'headerText' => L('Код'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'name',
                'width' => 200,
                'headerText' => L('Име'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_FLOAT,
                'dataField' => 'rate',
                'width' => 70,
                'headerText' => L('Норма'),
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumnEditedBy()
        );

        $this->sReportSelectStatement = "
            SELECT SQL_CALC_FOUND_ROWS
                m.*,
                CONCAT_WS(' ',pc.fname,pc.mname,pc.lname) AS created_user,
                CONCAT_WS(' ',pu.fname,pu.mname,pu.lname) AS updated_user
            FROM metals m
            LEFT JOIN personnel pc ON pc.id = m.created_user
            LEFT JOIN personnel pu ON pu.id = m.updated_user
        ";

        $this->aReportWhereStatement[] = 'm.to_arc = 0';
        $this->sReportGroupStatement = 'm.id';
        $this->aDefaultSort = array(
            array('field' => 'created_time', 'dir' => 'ASC')
        );

        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
    }

    public function load($aRequest = array(), $aInitParams = array()) {
        return parent::load($aRequest, $aInitParams);
    }

    public static function getMetalByID($id) {
        return DBC::$slave->selectByID('metals', $id, 300);
    }
}