<?php
namespace emc\nomenclatures\metals;

use emc\production\schedule\Scheduler;
use main\db\DBC;
use main\Exception;
use main\form\FormPanelHandler;
use main\InvalidParameter;

/**
 * Class Edit
 * @accessLevelDescription Редактиране на метал
 */
class Edit extends FormPanelHandler {

    public function init($aRequest = array(), $aInitParams = array()) {
        foreach ($aInitParams['row'] as $k => $v) $this->setField($k, $v);
        $this->setLastEditInfo($aInitParams['row']);
        return $this->oResponse;
    }

    public function save($aRequest) {
        return DBC::$main->withTrans(function ($aRequest) {
            $metal = $aRequest['fields'];
            $metal['id'] = intval($aRequest['id']);
            $metal = array_map('trim', $metal);

            foreach (array('code', 'name', 'rate') as $k) if(empty($metal[$k])) throw new InvalidParameter('Необходимо е да въведете стойност', array($k));

            $metal['code'] = strtoupper($metal['code']);

            if(DBC::$slave->selectRow("SELECT id FROM metals WHERE to_arc = 0 AND id != {$metal['id']} AND code = " . DBC::$slave->quote($metal['code']))) throw new InvalidParameter(L('Вече съществува метал с този код.'));

            if($metal['rate'] < 1) throw new InvalidParameter(L('Нормата не може да бъде по-малка от 1.'));

            DBC::$main->update('metals', $metal);

            foreach (Scheduler::getLineIDsUsingMetal($metal['id']) as $idLine) Scheduler::scheduleLine($idLine);

            return $this->oResponse;
        }, [$aRequest]);
    }

    public function delete($aRequest) {
        $metal = $aRequest['fields'];
        $metal['id'] = intval($aRequest['id']);

        if(empty($metal['id'])) throw new InvalidParameter;

        if(DBC::$slave->selectRow("SELECT id_metal FROM production_sizes WHERE id_metal = {$metal['id']} AND to_arc = 0")) {
            throw new Exception(L('Този метал не може да бъде изтрит, защото се използва в производствени размери.'));
        }

        DBC::$main->delete('metals', $metal['id']);
        return $this->oResponse;
    }

}