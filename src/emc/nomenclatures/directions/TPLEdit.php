<?php
namespace emc\nomenclatures\directions;
use main\TPLBase;

class TPLEdit extends TPLBase {

    public function __construct($sPrefix, array $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);

        $this->bPrintWithTitle = true;
        $this->aFrameConfig['title'] = empty($aInitParams['id']) ? L('Ново направление') : L('Редакция на направление');
    }

    public function printHtml() { ?>

        <table style="width: 100%">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td><? $this->printLabel(L('Име'), 'name'); ?></td>
                            <td><? $this->printElement('input', array('name' => 'name')); ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="toolbar align-right" style="margin-top: 5px">
                        <? $this->printElement('lasteditedinfo'); ?>
                        <? $this->printElement('closebutton'); ?>
                        <? $this->printElement('confirmbutton'); ?>
                    </div>
                </td>
            </tr>
        </table>

        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, baseParams) {
            $(container).find('.confirm').click(function() {
                container.request('save', { id: baseParams.id }, null, function(res) {
                    $(container).find('.close').click();
                    baseParams.reloadParentField(res);
                });
            });
        });

JS;
    }
    
}