<?php

namespace emc\nomenclatures\metalbrands;

use main\db\DBC;
use main\form\FormPanelHandler;
use main\InvalidParameter;
use main\suggest\Suggest;

/**
 * Class Edit
 * @accessLevelDescription Достъп за редакция на номенклатури за "Марка/Код"
 */
class Edit extends FormPanelHandler {

    public function init($aRequest = array(), $aInitParams = array()) {
        foreach ($aInitParams['row'] as $k => $v) $this->setField($k, $v);
        $this->setLastEditInfo($aInitParams['row']);
        return $this->oResponse;
    }

    public function save($aRequest) {
        return DBC::$main->withTrans(function ($aRequest) {
            $metal = $aRequest['fields'];
            $metal['id'] = intval($aRequest['id']);
            $metal = array_map('trim', $metal);

            foreach (array('name') as $k) if(empty($metal[$k])) throw new InvalidParameter('Необходимо е да въведете стойност', array($k));

            if(DBC::$slave->selectRow("SELECT id FROM production_metal_brands WHERE to_arc = 0 AND id != {$metal['id']} AND name = " . DBC::$slave->quote($metal['name']))) throw new InvalidParameter(L('Вече съществува марка/код с това име.'));

            DBC::$main->update('production_metal_brands', $metal);

            return $this->oResponse;
        }, [$aRequest]);
    }

    public function delete($aRequest) {
        $metal = $aRequest['fields'];
        $metal['id'] = intval($aRequest['id']);

        if(empty($metal['id'])) throw new InvalidParameter;

        DBC::$main->delete('metals', $metal['id']);
        return $this->oResponse;
    }


    public function suggestMetalBrand($aRequest) {
        $aResult = array();
        $aResult['select'] = "SELECT id, name as value, name as html_value FROM production_metal_brands pmb ";
        $aResult['where'][] = "pmb.name LIKE '%{$aRequest['query']}%' AND pmb.to_arc = 0";
        $aResult['group'] = 'html_value';

        return Suggest::select($aResult);
    }
}