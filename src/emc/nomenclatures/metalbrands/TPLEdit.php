<?php
namespace emc\nomenclatures\metalbrands;
use main\TPLBase;

class TPLEdit extends TPLBase {

    public function __construct($sPrefix, array $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);

        $this->bPrintWithTitle = true;
        $this->aFrameConfig['title'] = empty($aInitParams['row']['id']) ? L('Нова марка/код') : L('Редакция на марка/код');
    }

    public function printHtml() { ?>

        <table style="width: 100%">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td><? $this->printLabel(L('Име'), 'name'); ?></td>
                            <td><? $this->printElement('input', array('name' => 'name')); ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="toolbar align-right" style="margin-top: 5px">
                        <? $this->printElement('lasteditedinfo'); ?>
                        <? $this->printElement('closebutton'); ?>
                        <? $this->printElement('confirmbutton'); ?>
                        <? $this->printElement('button', array('class' => 'delete_metal_brand align-right', 'text' => L('Изтрий'), 'name' => 'delete_metal_brand', 'iconCls' => 'icon fa fa-trash')) ?>
                    </div>
                </td>
            </tr>
        </table>

        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, baseParams) {

            if (baseParams['row']['id'] == 0) {
                $(container).find('.delete_metal_brand').css('display', 'none');
            }

            $(container).find('.confirm').click(function(){
                container.request('save',{id: baseParams['row']['id']},null, function(){
                    $(container).find('.close').click();
                    baseParams.getParentGrid().loadData();
                });
            });

            $(container).find('.delete_metal_brand').click(function() {
                container.request('delete', {id: baseParams['row']['id']}, null, function() {
                    $(container).find('.close').click();
                    baseParams.getParentGrid().loadData();
                });
            });
        });
 
JS;
    }

}