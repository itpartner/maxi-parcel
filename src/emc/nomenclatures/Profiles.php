<?php
namespace emc\nomenclatures;

class Profiles {
    /** квадратен */
    const SQ    = 'SQ';
    /** правоъгълен */
    const REC   = 'REC';
    /**  кръгъл */
    const R     = 'R';
    /** овален */
    const OV    = 'OV';
    /** плоско-овален */
    const FOV   = 'FOV';
    /** полу плоско-овален */
    const SFOV  = 'SFOV';
    /** специален затворен */
    const SP    = 'SP';
    /** специален отворен */
    const OP    = 'OP';

    public static $types = [
        self::SQ   => self::SQ,
        self::REC  => self::REC,
        self::R    => self::R,
        self::OV   => self::OV,
        self::FOV  => self::FOV,
        self::SFOV => self::SFOV,
        self::SP   => self::SP,
        self::OP   => self::OP,
    ];

    public static $gridEnumItems = [
        self::SQ   => array('text' => '(SQ) квадратен'),
        self::REC  => array('text' => '(REC) правоъгълен'),
        self::R    => array('text' => '(R) кръгъл'),
        self::OV   => array('text' => '(OV) овален'),
        self::FOV  => array('text' => '(FOV) плоско-овален'),
        self::SFOV => array('text' => '(SFOV) полу плоско-овален'),
        self::SP   => array('text' => '(SP) специален затворен'),
        self::OP   => array('text' => '(OP) специален отворен'),
    ];

    public static $selectContent = [
        ['id' => self::SQ  , 'value' => '(SQ) квадратен'],
        ['id' => self::REC , 'value' => '(REC) правоъгълен'],
        ['id' => self::R   , 'value' => '(R) кръгъл'],
        ['id' => self::OV  , 'value' => '(OV) овален'],
        ['id' => self::FOV , 'value' => '(FOV) плоско-овален'],
        ['id' => self::SFOV, 'value' => '(SFOV) полу плоско-овален'],
        ['id' => self::SP  , 'value' => '(SP) специален затворен'],
        ['id' => self::OP  , 'value' => '(OP) специален отворен'],
    ];



}