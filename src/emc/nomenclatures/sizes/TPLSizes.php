<?php
namespace emc\nomenclatures\sizes;

use main\grid\TPLGridPanel;

class TPLSizes extends TPLGridPanel {
    public function __construct($sPrefix, $aInitParams = array()) {
        $this->{'aConfig'} = array(
            'title' => L('Размери'),
            'autoVResize' => true,
            'topBarItems' => array(
                array('button', array('name' => 'add', 'class' => 'add', 'iconCls' => 'icon fa fa-plus-circle', 'text' => L('Добави')))
            ),
            'bottomBarItems' => array(
                array('exportbutton', array('align' => 'right'))
            )
        );
        parent::__construct($sPrefix, $aInitParams);
    }


    public function getJSFunction() {
        return <<<'JS'
        (function(container, prefix, aBaseParams) {
            var openSize = function(size) {
                framework.createWindow({
                    tplName: 'emc/nomenclatures/sizes/Edit',
                    baseParams: { row: size || {}, getParentGrid:function(){return container.grid;}}
                });
            }
        
            $(container).find('.add').click(function() {
               openSize();
            });

            container.grid.clickListeners['code'] = function(rowNum, fieldElement, htmlElement) {
                openSize(container.grid.getCurrentData()[rowNum]);
            };
        });
JS;
    }

}