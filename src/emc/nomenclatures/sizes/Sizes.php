<?php

namespace emc\nomenclatures\sizes;

use emc\nomenclatures\markings\Markings;
use emc\nomenclatures\metals\Metals;
use emc\nomenclatures\Profiles;
use main\db\DBC;
use main\grid\GridPanelHandler;
use main\InvalidParameter;
use main\suggest\Suggest;

/**
 * @accessLevelDescription Производствени размери
 */
class Sizes extends GridPanelHandler {

    public static $selectStatement = "
        SELECT SQL_CALC_FOUND_ROWS
            ps.*,
            CASE
              WHEN ps.profile_type = 'SQ' THEN CONCAT(ps.a, 'x', ps.a)
              WHEN ps.profile_type = 'R' THEN ps.d
              WHEN (ps.profile_type = 'SP' OR ps.profile_type = 'OP') AND ps.d != 0 THEN ps.d
              ELSE CONCAT(ps.a, 'x', ps.h)
            END AS `size`,
            CONCAT_WS(' ',pc.fname,pc.mname,pc.lname) AS created_user,
            CONCAT_WS(' ',pu.fname,pu.mname,pu.lname) AS updated_user,
            IF(f.file IS NOT NULL, CONCAT(fd.public_url,'/',f.file,'.',f.ext), '') AS scheme,
            IF(f.file IS NOT NULL, 1, 0) AS has_uploaded_file,
            GROUP_CONCAT(pl.code ORDER BY pl.code) AS line_code
        FROM production_sizes ps
        LEFT JOIN personnel     pc ON pc.id = ps.created_user
        LEFT JOIN personnel     pu ON pu.id = ps.updated_user
        LEFT JOIN files f ON f.file = ps.scheme_file
        LEFT JOIN file_devices fd ON fd.id = f.id_device
        LEFT JOIN production_lines_sizes pls ON pls.id_size = ps.id AND pls.to_arc = 0
        LEFT JOIN production_lines pl ON pl.id = pls.id_line AND pl.to_arc = 0
    ";


    public function __construct() {

        $this->oBase = DBC::$slave;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'code',
                'width' => 200,
                'headerText' => L('Код'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'id_metal',
                'width' => 180,
                'items' => DBC::$slave->selectAssoc("SELECT id, '', CONCAT('(',code,') ',name) AS text FROM metals WHERE to_arc = 0"),
                'headerText' => L('Метал'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'size',
                'width' => 100,
                'headerText' => L('Размер(mm)'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_FLOAT,
                'dataField' => 's',
                'width' => 100,
                'headerText' => L('Дебелина(mm)'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'profile_type',
                'width' => 180,
                'items' => Profiles::$gridEnumItems,
                'headerText' => L('Профил'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_FLOAT,
                'dataField' => 'm',
                'width' => 100,
                'headerText' => L('Л. маса(кг)'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'line_code',
                'width' => 250,
                'headerText' => L('Тръбна линия'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumnEditedBy()
        );

        $this->sReportSelectStatement = self::$selectStatement;

        $this->aReportWhereStatement[] = "ps.to_arc = 0";
        $this->sReportGroupStatement = "ps.id";

        $this->aDefaultSort = array(
            array('field' => 'created_time', 'dir' => 'ASC')
        );

        parent::__construct();
    }

    private static $SIZE_CACHE_TIMEOUT = 60;

    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
    }

    public static function getSizeCodeWithoutThickness($size) {
        return self::getSizeCode($size, 'skipThickness');
    }

    public static function getSizeCodeWithoutThicknessAndMetalBrand($size) {
        return self::getSizeCode($size, 'baseSize');
    }

    public static function getSizeCode($size, $format = '') {
        switch ($size['profile_type']) {
            case Profiles::SQ:
                switch ($format) {
                    case 'skipThickness':
                        return sprintf("%s-%sx%s-%s", $size['profile_type'], doubleval($size['a']), doubleval($size['a']), Metals::getMetalByID($size['id_metal'])['code']);
                    case 'baseSize':
                        return sprintf("%s-%sx%s", $size['profile_type'], doubleval($size['a']), doubleval($size['a']));
                    default:
                        return sprintf("%s-%sx%sx%s-%s", $size['profile_type'], doubleval($size['a']), doubleval($size['a']), doubleval($size['s']), Metals::getMetalByID($size['id_metal'])['code']);
                }
                break;
            case Profiles::R:
                switch ($format) {
                    case 'skipThickness':
                        return sprintf("%s-%s-%s", $size['profile_type'], doubleval($size['d']), Metals::getMetalByID($size['id_metal'])['code']);
                    case 'baseSize':
                        return sprintf("%s-%s", $size['profile_type'], doubleval($size['d']));
                    default:
                        return sprintf("%s-%sx%s-%s", $size['profile_type'], doubleval($size['d']), doubleval($size['s']), Metals::getMetalByID($size['id_metal'])['code']);
                }
                break;
            case Profiles::REC:
            case Profiles::OV:
            case Profiles::FOV:
            case Profiles::SFOV:
                switch ($format) {
                    case 'skipThickness':
                        return sprintf("%s-%sx%s-%s", $size['profile_type'], doubleval($size['a']), doubleval($size['h']), Metals::getMetalByID($size['id_metal'])['code']);
                    case 'baseSize':
                        return sprintf("%s-%sx%s", $size['profile_type'], doubleval($size['a']), doubleval($size['h']));
                    default:
                        return sprintf("%s-%sx%sx%s-%s", $size['profile_type'], doubleval($size['a']), doubleval($size['h']), doubleval($size['s']), Metals::getMetalByID($size['id_metal'])['code']);
                }
            case Profiles::SP:
            case Profiles::OP:
                $sMarkingPart = !empty($size['id_marking']) ? 'x' . Markings::getMarkingByID($size['id_marking'])['code'] : '';
                if(empty($size['d'])) {
                    switch ($format) {
                        case 'skipThickness':
                            return sprintf("%s-%sx%s%s-%s", $size['profile_type'], doubleval($size['a']), doubleval($size['h']), $sMarkingPart, Metals::getMetalByID($size['id_metal'])['code']);
                        case 'baseSize':
                            return sprintf("%s-%sx%s%s", $size['profile_type'], doubleval($size['a']), doubleval($size['h']), $sMarkingPart);
                        default:
                            return sprintf("%s-%sx%s%sx%s-%s", $size['profile_type'], doubleval($size['a']), doubleval($size['h']), $sMarkingPart, $size['s'], Metals::getMetalByID($size['id_metal'])['code']);
                    }
                } else {
                    switch ($format) {
                        case 'skipThickness':
                            return sprintf("%s-%s%s-%s", $size['profile_type'], doubleval($size['d']), $sMarkingPart, Metals::getMetalByID($size['id_metal'])['code']);
                        case 'baseSize':
                            return sprintf("%s-%s%s", $size['profile_type'], doubleval($size['d']), $sMarkingPart);
                        default:
                            return sprintf("%s-%s%sx%s-%s", $size['profile_type'], doubleval($size['d']), $sMarkingPart, $size['s'], Metals::getMetalByID($size['id_metal'])['code']);
                    }
                }
                break;
            default:
                throw new InvalidParameter("Непознат профил");
        }
    }

    public static function suggestSize($aRequest) {
        $aResult = array();
        $aRequest['query'] = mb_strtolower($aRequest['query']);
        $aRequest['query'] = str_replace('х', 'x', $aRequest['query']);
        $aRequest['query'] = str_replace(',', '.', $aRequest['query']);

        $aWords = array_merge(preg_split("/\\s/ui", $aRequest['query']), preg_split("/[^a-zа-яø\\d]/ui", $aRequest['query']));
        $aWords = array_map(function ($w) {
            return '( t.search_text LIKE "%' . $w . '%" )';
        }, $aWords);
        $where = implode(' AND ', $aWords);

        $aResult['limit'] = $aRequest['limit'];

        $aResult['select'] = "
            SELECT 
                t.id,
                t.profile_type,
                t.code AS value, 
                t.code AS html_value,
                t.code,
                t.s,
                t.d,
                t.a,
                t.h,
                t.m,
                t.id_metal,
                IF(f.file IS NOT NULL, CONCAT(fd.public_url,'/',f.file,'.',f.ext),
                  CASE t.profile_type
                      WHEN 'FOV' THEN CONCAT('images/profiles/profile_FOV.jpg')
                      WHEN 'OV' THEN CONCAT('images/profiles/profile_OV.jpg')
                      WHEN 'R' THEN CONCAT('images/profiles/profile_R.jpg')
                      WHEN 'REC' THEN CONCAT('images/profiles/profile_REC.jpg')
                      WHEN 'SFOV' THEN CONCAT('images/profiles/profile_SFOV.jpg')
                      WHEN 'SQ' THEN CONCAT('images/profiles/profile_SQ.jpg')
                      ELSE 'images/no_photo.png'
                  END
                ) AS scheme,
                ''
            FROM production_sizes t
            LEFT JOIN files f ON f.file = t.scheme_file
            LEFT JOIN file_devices fd ON fd.id = f.id_device
        ";

        if(empty($aRequest['all_sizes'])) {
            $aResult['select'] .= "
                JOIN production_lines_sizes pls ON pls.id_size = t.id AND pls.to_arc = 0
                JOIN production_lines pl ON pl.id = pls.id_line AND pl.to_arc = 0
            ";
        }

        $aResult['where'][] = "t.to_arc = 0";
        $aResult['where'][] = $where;
        $aResult['group'] = 'id';
        $aResult['order'] = 't.profile_type ASC, t.d ASC, t.a ASC, t.h ASC, t.s ASC';

        return Suggest::select($aResult);
    }

    /**
     * @param $aRequest
     * @return float
     * @throws InvalidParameter
     */
    public static function calcLinearMass($size) {
        $metal = Metals::getMetalByID($size['id_metal'])['code'];

        $const0 = $metal == 'AL' ? 0.271 : 0.785;
        $const = $const0 * 10;

        $divider = $metal == 'AL' ? 100 : 98.95;
        $dividerOV = $metal == 'AL' ? 2.897 : 1;

        define('PI', 3.141593); //ползваме тяхното PI, да няма разлики с изчисленията на клиента
        list($s, $d, $a, $h, $L) = array(doubleval($size['s']), doubleval($size['d']), doubleval($size['a']), doubleval($size['h']), doubleval($size['L']));

        if(!empty($size['s_production'])) $s = doubleval($size['s_production']);

        //За овални, плоскоовални и полуплоскоовални профили при пресмятане за ширина да се взема по голямата стойност от ширината и височината. Т.е. ако размера е 30х60(ширина х височина) да се смята по формула за 60х30.
        if(in_array($size['profile_type'], array(Profiles::OV, Profiles::FOV, Profiles::SFOV)) && $h > $a) {
            $a = $a + $h;
            $h = $a - $h;
            $a = $a - $h;
        }

        switch ($size['profile_type']) {
            case Profiles::SQ:
                $R = $s <= 2.5 ? $s * 1.25 : $s * 2;
                $r = $R - $s;
                $m = ((4 * $s * ($a - 2 * $R) + (PI * (($R * $R) - ($r * $r)))) / $divider) * $const0;
                break;
            case Profiles::R:
                $m = ((PI * ($s * $d - ($s * $s))) / 100) * $const0;
                break;
            case Profiles::REC:
                $R = $s <= 2.5 ? $s * 1.25 : $s * 2;
                $r = $R - $s;
                $m = ((2 * $s * (($a + $h) - 4 * $R) + (PI * (($R * $R) - ($r * $r)))) / $divider) * $const0;
                break;
            case Profiles::OV:
                $m = 0.01233 / $dividerOV * $s * ($a + $h - (2 * $s));
                break;
            case Profiles::FOV:
                $m = 0.0157 / $dividerOV * ($a + (0.57 * $h) - (1.57 * $s)) * $s;
                break;
            case Profiles::SFOV:
                $R = $s <= 1.5 ? $s * 1.25 : $s * 2;
                $m = $const * $s * ($h - 2 * $R + 2 * ($a - $R - $h / 2) + PI * ($R + $h / 2 - $s)) / 1000;
                break;
            case Profiles::SP:
            case Profiles::OP:
                $F = $s * $L;
                $m = ($F * $const) / 1000;
                break;
            default:
                throw new InvalidParameter("Непознат профил");
        }
        return round($m, 3);
    }

    public static function getSizeByCode($code) {
        $code = DBC::$slave->quote($code);
        return DBC::$slave->selectRow("SELECT * FROM production_sizes WHERE to_arc = 0 AND code = {$code}", self::$SIZE_CACHE_TIMEOUT);
    }

    public static function getSizeByID($id) {
        return DBC::$slave->selectByID('production_sizes', $id, self::$SIZE_CACHE_TIMEOUT);
    }

}