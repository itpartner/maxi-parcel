<?php
namespace emc\nomenclatures\sizes;

use main\db\DBC;
use main\TPLBase;

class TPLEdit extends TPLBase {

    public function __construct($sPrefix, array $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);

        $this->bPrintWithTitle = true;
        $this->aFrameConfig['title'] = empty($aInitParams['row']['id']) ? L('Нов размер') : L('Редакция на размер');
    }

    public function printHtml() {
        $sOutputKG = L('кг');
        $sOutputMM = L('mm');

        $aMetals = DBC::$main->select("SELECT id, CONCAT('(', code, ')', ' ', name) AS value FROM metals WHERE to_arc = 0");
        $aProfiles = DBC::$main->select("SELECT type AS id, CONCAT('(', type, ')', ' ', name) AS value FROM profile_types");
        $aMarkings = DBC::$main->select("SELECT id, CONCAT('(', code, ')', ' ', name) AS value FROM markings WHERE to_arc = 0");
        array_unshift($aMarkings, array('id' => 0, 'value' => '-- Изберете --'));
        ?>

        <style>
            #<?= $this->sPrefix; ?> .dimensions {
                width: 80px;
            }

            #<?= $this->sPrefix; ?> .full-parent-width {
                width: 100%;
            }

            #<?= $this->sPrefix; ?> .input-full-parent-width {
                width: 100%;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
            }

            #<?= $this->sPrefix; ?> .sides td {
                padding: 2px;
            }
        </style>

        <table style="width: 100%">
            <tr>
                <td style="vertical-align: top;">
                    <table class="sides"> <!-- dimensions / metals / etc -->
                        <tr>
                            <td><? $this->printElement('label', array('fieldName' => 'code', 'text' => L('Код'))); ?></td>
                            <td><? $this->printElement('input', array('name' => 'code', 'readonly' => 'readonly', 'class' => 'input-full-parent-width',
                                    'style' => 'height: 26px;')); ?></td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName' => 'm', 'text' => L('Л. маса'))); ?></td>
                            <td>
                                <? $this->printElement('input', array('name' => 'm', 'readonly' => 'readonly', 'class' => 'dimensions')); ?>
                                <? $this->printElement('span', array('text' => $sOutputKG)); ?>
                                <? $this->printElement('button', array('name' => 'calc', 'iconCls' => 'icon fa fa-refresh', 'title' => L('Изчисли линейна маса'))); ?>
                            </td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName' => 'id_metal', 'text' => L('Метал'))); ?></td>
                            <td><? $this->printElement('select', array('name' => 'id_metal', 'id' => $this->sPrefix . 'metal', 'class' => 'full-parent-width'), $aMetals); ?></td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName' => 'profile_type', 'text' => L('Профил'))); ?></td>
                            <td><? $this->printElement('select', array('name' => 'profile_type', 'class' => 'full-parent-width profile'), $aProfiles); ?></td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName' => 'id_marking', 'text' => L('Маркировка'))); ?></td>
                            <td><? $this->printElement('select', array('name' => 'id_marking', 'id' => $this->sPrefix . 'marking', 'class' => 'full-parent-width'), $aMarkings); ?></td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName' => 'L', 'text' => L('Разгъвка'))); ?></td>
                            <td>
                                <? $this->printElement('input', array('name' => 'L', 'class' => 'dimensions', 'keyRestriction' => 'float')); ?>
                                <? $this->printElement('span', array('text' => $sOutputMM)); ?>
                            </td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName' => 's', 'text' => L('Дебелина'))); ?></td>
                            <td>
                                <? $this->printElement('input', array('name' => 's', 'class' => 'dimensions', 'keyRestriction' => 'float')); ?>
                                <? $this->printElement('span', array('text' => $sOutputMM)); ?>
                            </td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName' => 'd', 'text' => L('Диаметър'))); ?></td>
                            <td>
                                <? $this->printElement('input', array('name' => 'd', 'class' => 'dimensions', 'keyRestriction' => 'float')); ?>
                                <? $this->printElement('span', array('text' => $sOutputMM)); ?>
                            </td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName' => 'a', 'text' => L('Ширина'))); ?></td>
                            <td>
                                <? $this->printElement('input', array('name' => 'a', 'class' => 'dimensions', 'keyRestriction' => 'float')); ?>
                                <? $this->printElement('span', array('text' => $sOutputMM)); ?>
                            </td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName' => 'h', 'text' => L('Височина'))); ?></td>
                            <td>
                                <? $this->printElement('input', array('name' => 'h', 'class' => 'dimensions', 'keyRestriction' => 'float')); ?>
                                <? $this->printElement('span', array('text' => $sOutputMM)); ?>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="vertical-align: top;">
                    <table class="sides"> <!-- scheme field, description -->
                        <tr>
                            <td>
                                <fieldset>
                                    <legend><? echo L('Схема') ?></legend>
                                    <div><? $this->printElement('img', array('name' => 'scheme', 'src' => 'images/no_photo.png', 'style' => 'width: 400px;min-height:300px')); ?></div>
                                    <div style="text-align: left;">
                                        <input type="file" name="scheme_fileupload"/>
                                    </div>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="sides" style="width: 100%"> <!-- scheme field, description -->
                        <tr>
                            <td><? $this->printElement('label', array('fieldName' => 'description', 'text' => L('Описание'))); ?></td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('textarea', array('name' => 'description', 'class' => 'input-full-parent-width')); ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="toolbar align-right" style="margin-top: 5px">
                        <? $this->printElement('lasteditedinfo'); ?>
                        <? $this->printElement('closebutton'); ?>
                        <? $this->printElement('confirmbutton'); ?>
                        <? $this->printElement('button', array('iconCls' => 'icon fa fa-files-o', 'text' => L('Клонирай'), 'class' => 'clone align-right')); ?>
                        <? $this->printElement('button', array('iconCls' => 'icon fa fa-trash', 'text' => L('Изтрий'), 'class' => 'delete align-right')); ?>
                    </div>
                </td>
            </tr>
        </table>

        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, baseParams) {
            var form = container;
            var imageData;
            var hasUploadedFile = false;
            
            container.fields['scheme_fileupload'].addListener('change', function(files){
                if(!files.length) return;
                if(["image/jpeg", "image/png", "image/gif", "image/x-ms-bmp"].indexOf(files[0].type) < 0) {
                    framework.alert("Моля, изберете графично изображение от типа jpeg, gif, png или bmp.");
                    return;
                }
                
                $(container).find('img[name="scheme"]').attr('src',files[0].dataURL);
                $(container).find('img[name="scheme"]').css('cursor','pointer');
                $(container).find('img[name="scheme"]').attr('title','Виж схемата в цял размер');

                imageData = files[0].dataURL;
            });

            var setSchemeSrc = function(result){
                setSchemeSrc = function(){};
                if (result.schemeSrc) {
                    $(container).find('img[name="scheme"]').attr('src', result.schemeSrc);
                    $(container).find('img[name="scheme"]').css('cursor','pointer');
                    $(container).find('img[name="scheme"]').attr('title','Виж схемата в цял размер');
                }
            };

            form.addListener('afterRequest', function(result){
                if(result.fields.has_uploaded_file.value == 1) hasUploadedFile = true;

                setSchemeSrc(result);
            });
            
            if(!baseParams['row']['id']) {
                $(container).find('.clone').css('display', 'none');
                $(container).find('.delete').css('display', 'none');
            }

            $(container).find('[name=scheme]').click(function() {
                window.open(this.src);
            });

            $(container).find('.confirm').click(function(){
                container.request('save', { id: baseParams['row']['id'], image_data: imageData }, null, function() {
                        baseParams.getParentGrid().loadData();
                        $(container).find('.close').click();
                });
            });

            $(container).find('.delete').click(function(){
                container.request('delete', { id: baseParams['row']['id'] }, null, function() {
                    baseParams.getParentGrid().loadData();
                    $(container).find('.close').click();
                });
            });

            $(container).find('.clone').click(function(){
                baseParams['row']['id_to_clone'] = baseParams['row']['id'];
                baseParams['row']['id'] = '';
                framework.createWindow({
                    tplName: 'emc/nomenclatures/sizes/Edit',
                    baseParams: { row: baseParams['row'], getParentGrid: baseParams.getParentGrid }
                });

                $(container).find('.close').click();
            });

            $(container).find('button[name=calc]').click(function() {
                Server.call('emc/nomenclatures/sizes/Sizes.calcLinearMass',[container.getFieldValues()],function(r){
                    container.fields.m.setValue(r);
                },null, null, true);
            });

            $(container).find('[name=profile_type]').change(function() {disableUnnecessaryFields(false);});
            $(container).find('[name=profile_type],[name=s],[name=d],[name=a],[name=h],[name=id_metal],[name=id_marking]').change(function(event){
                var fieldName = event.target.name;

                if (fieldName == 'd' && container.fields.d.getValue() != 0) {
                    container.fields.a.setValue(0);
                    container.fields.h.setValue(0);
                } else if ((fieldName == 'a' || fieldName == 'h') && (container.fields.a.getValue() != 0 || container.fields.h.getValue() != 0)) {
                    container.fields.d.setValue(0);
                }
                showCode();
            });

            var showCode = function() {
                Server.call('emc/nomenclatures/sizes/Sizes.getSizeCode',[container.getFieldValues()],function(r){
                    container.fields.code.setValue(r);
                },null, null, false);
            }

            var disableUnnecessaryFields = function(isNewSize) {
                var profile_type = container.fields.profile_type.getValue();
                var scheme = jQuery(document).find('[name=scheme]');

                // проверка има ли ъплоудната снимка, иначе зареждаме според профила
                if(!hasUploadedFile) {
                    if(profile_type != "SP" && profile_type != "OP") {
                        scheme.attr('src', 'images/profiles/profile_' + profile_type + '.jpg');
                    } else {
                        scheme.attr('src', 'images/no_photo.png');
                    }
                }

                switch (profile_type) {
                    case 'SQ':
                        $(container).find('select[name=id_marking]').attr('disabled', 'disabled');
                        container.fields['id_marking'].setValue(0);
                        
                        $(container).find('[name=L],[name=d],[name=h]').each(function(k, el){
                            if (isNewSize) el.setValue(0);
                            $(el).attr('disabled','disabled');
                        })

                        $(container).find('input[name=a]').attr('disabled', false);
                        if (isNewSize) {
                            $(container).find('[name=s], [name=a]').each(function(k, el) {
                                el.setValue(0);
                            });
                        }
                        break;
                    case 'R':
                        $(container).find('select[name=id_marking]').attr('disabled', 'disabled');
                        container.fields['id_marking'].setValue(0);
                        $(container).find('[name=L],[name=a],[name=h]').each(function(k, el){
                            el.setValue(0);
                            $(el).attr('disabled','disabled');
                        });
                        $(container).find('input[name=d]').attr('disabled', false);
                        break;
                    case 'REC':
                    case 'OV':
                    case 'FOV':
                    case 'SFOV':
                        $(container).find('select[name=id_marking]').attr('disabled', 'disabled');
                        container.fields['id_marking'].setValue(0);
                        $(container).find('[name=L],[name=d]').each(function(k, el){
                            el.setValue(0);
                            $(el).attr('disabled','disabled');
                        });
                        $(container).find('[name=a],[name=h]').each(function(k, el){
                            if (isNewSize) el.setValue(0);
                            $(el).attr('disabled',false);
                        });
                        break;
                    case 'SP':
                    case 'OP':
                        $(container).find('select[name=id_marking]').attr('disabled', false);
                        container.fields['id_marking'].setValue(0);
                        $(container).find('[name=L],[name=d],[name=a],[name=h]').each(function(k, el){
                            if (isNewSize) el.setValue(0);
                            $(el).attr('disabled',false);
                        });
                        break;
                }
            }
            
            window.setTimeout(function(){
                disableUnnecessaryFields($.isEmptyObject(baseParams['row']));
            },0);
        });
JS;
    }


}