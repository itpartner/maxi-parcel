<?php
namespace emc\nomenclatures\sizes;

use emc\nomenclatures\Profiles;
use emc\production\schedule\Scheduler;
use main\Access;
use main\db\DBC;
use main\Exception;
use main\FileUpload;
use main\form\FormPanelHandler;
use main\InvalidParameter;

/**
 * @accessLevelDescription Форма производствен размер
 */
class Edit extends FormPanelHandler {

    public function init($aRequest = array(), $aInitParams = array()) {
        $id = intval($aInitParams['row']['id']);
        if(!empty($aInitParams['row']['id_to_clone'])) {
            $id = intval($aInitParams['row']['id_to_clone']);

        }

        $size = DBC::$slave->selectRow(Sizes::$selectStatement . " WHERE ps.id = $id");

        if(!empty($aInitParams['row']['id_to_clone'])) {
            $size['updated_user'] = '';
            $size['updated_time'] = '';
            $size['created_user'] = '';
            $size['created_time'] = '';
        }

        foreach ($size as $k => $v) $this->setField($k, $v);

        if(!empty($size['scheme']) && empty($aInitParams['row']['id_to_clone'])) $this->oResponse->schemeSrc = $size['scheme'];

        $this->setLastEditInfo($size);
        return $this->oResponse;
    }

    public function delete($aRequest) {
        Access::checkAccess(CanEditSize::class);
        $id = intval($aRequest['id']);

        $size = DBC::$slave->selectRow(Sizes::$selectStatement . " WHERE ps.id = $id");
        if(empty($size)) throw new InvalidParameter;

        $aProductionLineSizes = DBC::$slave->selectRow("SELECT id_size FROM production_lines_sizes WHERE to_arc = 0 AND id_size = {$size['id']}");
        if(!empty($aProductionLineSizes)) throw new Exception(L('Размерът не може да бъде изтрит. Използва се в производствена линия.'));

        $aProductionOrdersSizes = DBC::$slave->selectRow("SELECT id_size FROM production_orders_sizes WHERE to_arc = 0 AND id_size = {$size['id']}");
        if(!empty($aProductionOrdersSizes)) throw new Exception(L('Размерът не може да бъде изтрит. Използва се в поръчка.'));

        $size['to_arc'] = 1;
        DBC::$main->update('production_sizes', $size);
        return $this->oResponse;
    }

    public function save($aRequest) {
        Access::checkAccess(CanEditSize::class);
        return DBC::$main->withTrans(function($aRequest){
            $size = $aRequest['fields'];
            $size['id'] = $aRequest['id'];

            $aRequest['fields']['scheme_fileupload'][0]['dataURL'] = $aRequest['image_data'];

            $aRequiredFields = array(
                Profiles::SQ => array('s', 'a'),
                Profiles::R => array('s', 'd'),
                Profiles::REC => array('s', 'a', 'h'),
                Profiles::OV => array('s', 'a', 'h'),
                Profiles::FOV => array('s', 'a', 'h'),
                Profiles::SFOV => array('s', 'a', 'h'),
                Profiles::SP => array('s', 'L'),
                Profiles::OP => array('s', 'L')
            );

            $aFieldText = array(
                'a' => L('Ширина'),
                'h' => L('Височина'),
                'd' => L('Диаметър'),
                's' => L('Дебелина'),
                'L' => L('Разгъвка'),
            );

            if (($size['profile_type'] == Profiles::SP) || ($size['profile_type'] == Profiles::OP)) {
                if (empty($size['d']) && empty($size['a']) && empty($size['h'])) {
                    throw new Exception(L('Моля, въведете стойност за поле %s или за полета %s и %s.', $aFieldText['d'], $aFieldText['a'], $aFieldText['h']));
                }

                $emptyField = empty($size['a']) ? 'a' : (empty($size['h']) ? 'h' : '');
                if ($emptyField && empty($size['d'])) {
                    throw new Exception(L('Липсва стойност за поле: %s.', $aFieldText[$emptyField]), array($emptyField));
                }
            }


            foreach ($aRequiredFields[$size['profile_type']] as $mKey => $sField) {
                if(empty($size[$sField])) throw new Exception(L('Липсва стойност за поле: %s.', $aFieldText[$sField]), array($sField));
            }

            $size['id'] = intval($size['id']);
            $size['m'] = Sizes::calcLinearMass($size);
            $size['code'] = Sizes::getSizeCode($size);

            $aProductionSize = DBC::$main->selectRow("SELECT id, code FROM production_sizes WHERE to_arc = 0 AND id != {$size['id']} AND code = " . DBC::$slave->quote($size['code']));
            if(!empty($aProductionSize)) throw new Exception(L('Вече съществува размер с такъв код.'));

            if($size['profile_type'] == 'SQ') $size['h'] = $size['a'];

            if(!empty($size['scheme_fileupload'])) {
                $file = FileUpload::uploadFile(reset($aRequest['fields']['scheme_fileupload']));
                $size['scheme_file'] = $file['file'];
            }

            if($size['profile_type'] == Profiles::SP || $size['profile_type'] == Profiles::OP) {
                if(empty($size['scheme_file'])) {
                    $sDBShemeFile = DBC::$slave->selectField("SELECT scheme_file FROM production_sizes WHERE id = {$size['id']} AND to_arc = 0");
                    if(empty($sDBShemeFile)) {
                        throw new Exception(L('Задължително е качването на схема за специалните размери.'), 'scheme_fileupload');
                    }
                }
            }

            foreach(Scheduler::getLineIDsUsingProductionSize($size['id']) as $idLine) Scheduler::scheduleLine($idLine);

            DBC::$main->update('production_sizes', $size);
            return $this->oResponse;
        },[$aRequest]);
    }

}