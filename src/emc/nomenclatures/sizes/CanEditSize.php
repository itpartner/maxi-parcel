<?php
namespace emc\nomenclatures\sizes;

use emc\access\AccessLevel;

/**
 * @accessLevelDescription Право за редакция на производствен размер
 */
class CanEditSize implements AccessLevel {

}