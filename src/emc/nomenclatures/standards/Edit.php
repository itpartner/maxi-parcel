<?php

namespace emc\nomenclatures\standards;

use main\Exception;
use main\form\FormPanelHandler;
use \main\db\DBC;
use main\suggest\Suggest;

/**
 * Class Edit
 * @package emc\nomenclatures\standards
 * @accessLevelDescription Достъп за редакция на стойности за "Стандарт"
 */

class Edit extends FormPanelHandler {
    public function init($aRequest, $aInitData) {
        return $this->loadData($aRequest, $aInitData);
    }

    public function loadData($aRequest, $aInitData) {
        $nId = intval($aInitData['id']);

        $this->setField('code', DBC::$slave->selectField("SELECT code FROM production_standards WHERE id = $nId AND to_arc = 0"));

        return $this->oResponse;
    }

    public function save($aRequest) {
        $nId = intval($aRequest['id']);
        $sEscapedName = DBC::$slave->quote($aRequest['fields']['name']);

        if(empty($sEscapedName)) throw new Exception(L('Моля въведете име.'));

        if(empty($nId)) {
            if (DBC::$slave->selectField("SELECT id FROM production_standards pd WHERE code = $sEscapedName AND to_arc = 0"))
                throw new Exception(L('Съществува стандарт с това име.'));
        }

        $aData = array(
            'id' => $nId,
            'code' => $aRequest['fields']['code'],
            'name' => $aRequest['fields']['code']
        );

        DBC::$main->update('production_standards', $aData);
        return $aData;
    }

    public function remove($nId) {
        $nId = intval($nId);

        if(!empty($nId)) {
            $nUsed = DBC::$slave->selectField("SELECT id FROM production_orders_sizes pos WHERE pos.id_standard = $nId AND pos.to_arc = 0");
            if (!empty($nUsed)) throw new Exception(L('Стандарта се използва от други заявки.'));

            DBC::$main->delete('production_standards', $nId);
        }
    }

    public function suggestStandard($aRequest) {
        $aResult = array();
        $aResult['select'] 	= "SELECT id, code as value, code as html_value FROM production_standards ps ";
        $aResult['where'][] = "ps.code LIKE '%{$aRequest['query']}%' AND ps.to_arc = 0";
        $aResult['group'] 	= 'html_value';

        return Suggest::select($aResult);
    }
}