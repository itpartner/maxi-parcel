<?php
namespace emc\nomenclatures\clients;

use main\db\DBC;
use main\Exception;
use main\form\FormPanelHandler;
use main\InvalidParameter;

/**
 * Class Edit
 * @accessLevelDescription Редактиране на клиент
 */
class Edit extends FormPanelHandler {

    public function init($aRequest = array(), $aInitParams = array()) {

        foreach ($aInitParams['row'] as $k => $v) $this->setField($k, $v);

        $this->setLastEditInfo($aInitParams['row']);
        return $this->oResponse;
    }

    public function save($aRequest) {
        $aClient = $aRequest['fields'];

        unset($aClient['last_edited_info']);

        $aRequiredFields = [
            'BG' => [
                'name'    => L('Име'),
                'mol'     => L('МОЛ'),
                'ein'     => L('БУЛСТАТ/ЕГН/ЕИН'),
                'region'  => L('Местонахождение'),
                'country' => L('Държава'),
                'city'    => L('Град'),
                'address' => L('Адрес'),
            ],
            'EU' => [
                'name'    => L('Име'),
                'region'  => L('Местонахождение'),
                'country' => L('Държава'),
                'city'    => L('Град'),
                'address' => L('Адрес'),
            ],
            'NON-EU' => [
                'name'    => L('Име'),
                'region'  => L('Местонахождение'),
                'country' => L('Държава'),
                'city'    => L('Град'),
                'address' => L('Адрес'),
            ],
        ];
        
        foreach ($aClient as $mKey => $sField) {
            $sField = trim($sField);
            $aClient[$mKey] = $sField;
            if ($mKey == 'region') {
                if (empty($sField)) throw new InvalidParameter(L('Изберете местонахождение на клиента.'), array('region'));
                $aClient[$mKey] = strtoupper($sField);
            }
        }

        $ex = [];
        foreach ($aRequiredFields[$aClient['region']] as $sField => $sName) {
            try {
                if (empty($aClient[$sField])) throw new InvalidParameter($sName, array($sField));
            } catch (Exception $e) {
                $ex[] = $e;
            }
        }

        Exception::checkThrow($ex, L('Моля, въведете стойности за:'));

        $aClient['id'] = intval($aRequest['id']);
        $sEinField = DBC::$slave->quote($aClient['ein']);
        $sEin = DBC::$slave->selectField("SELECT ein FROM clients WHERE ein = $sEinField AND id != {$aClient['id']} AND to_arc = 0");
        if($sEin) throw new Exception(L('Вече съществува клиент с такъв БУЛСТАТ/ЕГН/ЕИН.'), array('ein'));

        if(!empty($aClient['VAT_num'])) {
            $sVatField = DBC::$slave->quote($aClient['VAT_num']);
            $sVATNum = DBC::$slave->selectField("SELECT VAT_num FROM clients WHERE VAT_num = $sVatField AND id != {$aClient['id']} AND to_arc = 0");
            if($sVATNum) throw new Exception(L('Вече съществува клиент с такъв ДДС номер.'), array('VAT_num'));
        }

        DBC::$main->update('clients', $aClient);

        return $this->oResponse;
    }

    public function delete($aRequest) {
        if(!empty($aRequest['id'])) DBC::$main->delete('clients',$aRequest['id']);
        return $this->oResponse;
    }

}