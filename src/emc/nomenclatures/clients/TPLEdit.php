<?php
namespace emc\nomenclatures\clients;

use main\db\DBC;
use main\TPLBase;

class TPLEdit extends TPLBase {

    public function __construct($sPrefix, array $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);

        $this->bPrintWithTitle = true;
        $this->aFrameConfig['title'] = empty($aInitParams['row']['id']) ? L('Нов клиент') : L('Редакция на клиент:') . ' ' . $aInitParams['row']['name'];
    }

    public function printHtml() {
        $aRegions = array(
            array('id' => 'BG', 'value' => L('България')),
            array('id' => 'EU', 'value' => L('Европейски съюз')),
            array('id' => 'NON-EU', 'value' => L('Извън Европейски съюз')),
        );
        ?>

        <style type="text/css">
            #<?= $this->sPrefix ?> td {
                padding: 2px;
            }

            #<?= $this->sPrefix ?> .full-width-input {
                box-sizing: border-box;
                width: 100%;
            }
        </style>

        <table style="width: 100%">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName' => 'name', 'text' => L('Име'))); ?></td>
                            <td colspan="3"><? $this->printElement('input', array('name' => 'name', 'class' => 'full-width-input', 'style' => 'height: 26px;')); ?></td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName' => 'mol', 'text' => L('МОЛ'))); ?></td>
                            <td colspan="3"><? $this->printElement('input', array('name' => 'mol', 'class' => 'full-width-input', 'style' => 'height: 26px;')); ?></td>
                        </tr>
                        <tr>
                            <td><? $this->printElement('label', array('fieldName' => 'VAT_num', 'text' => L('ДДС номер'))); ?></td>
                            <td><? $this->printElement('input', array('name' => 'VAT_num', 'class' => 'full-width-input', 'style' => 'height: 26px;')); ?></td>
                            <td><? $this->printElement('label', array('fieldName' => 'ein', 'text' => L('БУЛСТАТ/ЕГН/ЕИН'))); ?></td>
                            <td><? $this->printElement('input', array('name' => 'ein', 'class' => 'full-width-input', 'style' => 'height: 26px;')); ?></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <fieldset>
                                    <legend><?= L('Адрес'); ?></legend>
                                    <table>
                                        <tr>
                                            <td><? $this->printElement('label', array('fieldName' => 'region', 'text' => L('Местонахождение'))); ?></td>
                                            <td><? $this->printElement('select', array('name' => 'region'), $aRegions); ?></td>
                                        </tr>
                                        <tr>
                                            <td><? $this->printElement('label', array('fieldName' => 'country', 'text' => L('Държава'))); ?></td>
                                            <td><? $this->printElement('input', array('name' => 'country', 'class' => 'full-width-input', 'style' => 'height: 26px;')); ?></td>
                                            <td><? $this->printElement('label', array('fieldName' => 'city', 'text' => L('Град'))); ?></td>
                                            <td><? $this->printElement('input', array('name' => 'city', 'class' => 'full-width-input', 'style' => 'height: 26px;')); ?></td>
                                        </tr>
                                        <tr>
                                            <td><? $this->printElement('label', array('fieldName' => 'address', 'text' => L('Адрес'))); ?></td>
                                            <td colspan="3">
                                                <? $this->printElement('input', array('name' => 'address', 'class' => 'full-width-input', 'style' => 'height: 26px;')); ?>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="toolbar align-right" style="margin-top: 5px">
                        <? $this->printElement('closebutton'); ?>
                        <? $this->printElement('confirmbutton'); ?>
                        <? $this->printElement('lasteditedinfo', array('class' => 'align-left')); ?>
                        <? $this->printElement('button', array('class' => 'delete_client align-left', 'text' => L('Изтрий'), 'name' => 'delete_marking', 'iconCls' => 'icon fa fa-trash')) ?>
                    </div>
                </td>
            </tr>
        </table>

        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, baseParams) {

            if (baseParams['row']['id'] == 0) {
                $(container).find('.delete_client').css('display', 'none');
            }

            var setBulgaria = function(){
                if (container.fields.region.getValue() == 'BG') {
                    container.fields.country.setValue('България');
                } else {
                    container.fields.country.setValue('');
                }
            };

            setBulgaria();

            $(container).find('[name=region]').change(function() {
                setBulgaria();
            });

            $(container).find('.confirm').click(function(){
                container.request('save',{id: baseParams['row']['id']},null, function(){
                    $(container).find('.close').click();
                    baseParams.getParentGrid().loadData();
                });
            });

            $(container).find('.delete_client').click(function() {
                container.request('delete', {id: baseParams['row']['id']}, null, function() {
                    $(container).find('.close').click();
                    baseParams.getParentGrid().loadData();
                });
            });
        });

JS;
    }


}