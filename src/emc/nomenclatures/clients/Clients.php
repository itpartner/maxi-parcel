<?php
namespace emc\nomenclatures\clients;

use main\db\DBC;
use main\grid\GridPanelHandler;
use main\suggest\Suggest;

/**
 * @accessLevelDescription Клиенти
 */
class Clients extends GridPanelHandler {

    public function __construct() {
        $this->oBase = DBC::$slave;

        $aRegions = array(
            'BG' => L('България'),
            'EU' => L('Европейски съюз'),
            'NON-EU' => L('Извън Европейски съюз')
        );

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'name',
                'width' => 200,
                'headerText' => L('Име'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'region',
                'width' => 150,
                'items' => array_map(function ($txt) {
                    return array('text' => $txt);
                }, $aRegions),
                'headerText' => L('Местонахождение'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'address',
                'width' => 300,
                'headerText' => L('Адрес'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'city',
                'width' => 100,
                'headerText' => L('Град'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'country',
                'width' => 130,
                'headerText' => L('Държава'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'VAT_num',
                'width' => 100,
                'headerText' => L('ДДС номер'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'ein',
                'width' => 100,
                'headerText' => L('БУЛСТАТ'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'mol',
                'width' => 170,
                'headerText' => L('МОЛ'),
                'filterable' => true,
                'resizable' => true,
                'sortable' => true
            )),
            $this->newColumnEditedBy()
        );

        $this->sReportSelectStatement = "
            SELECT SQL_CALC_FOUND_ROWS
                c.*,
                CONCAT_WS(' ',pc.fname,pc.mname,pc.lname) AS created_user,
                CONCAT_WS(' ',pu.fname,pu.mname,pu.lname) AS updated_user
            FROM clients c
            LEFT JOIN personnel pc ON pc.id = c.created_user
            LEFT JOIN personnel pu ON pu.id = c.updated_user
        ";

        $this->aReportWhereStatement[] = "c.to_arc = 0";

        $this->aDefaultSort = array(
            array('field' => 'created_time', 'dir' => 'ASC')
        );

        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
    }

    public function loadData($aRequest) {
        return parent::loadData($aRequest);
    }

    public static function getClientByID($id) {
        return DBC::$slave->selectByID('clients', $id, 300);
    }

    public function suggestClient($aRequest) {
        $aResult = array();
        $aResult['select'] = "
          SELECT
            c.id,
            c.name AS value,
            c.name AS html_value,
            c.region,
            c.address,
            c.city,
            c.country,
            c.VAT_num,
            c.ein,
            c.mol
          FROM clients c
        ";
        $aResult['where'][] = "c.name LIKE '%{$aRequest['query']}%' AND c.to_arc = 0";
        $aResult['group'] = 'html_value';

        return Suggest::select($aResult);
    }

    public function suggestEIN($aRequest) {
        $aResult = array();
        $aResult['select'] = "SELECT id, ein AS value, ein AS html_value FROM clients c ";
        $aResult['where'][] = "c.ein LIKE '{$aRequest['query']}%' AND c.to_arc = 0";
        $aResult['group'] = 'html_value';

        return Suggest::select($aResult);
    }
}