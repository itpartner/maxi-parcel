<?php
namespace emc\nomenclatures\clients;

use main\grid\TPLGridPanel;

class TPLClients extends TPLGridPanel {

    public function __construct($sPrefix, $aInitParams = array()) {
        $this->{'aConfig'} = array(
            'title' => L('Клиенти'),
            'autoVResize' => true,
            'topBarItems' => array(
                array('button', array('name' => 'add', 'class' => 'new', 'iconCls' => 'icon fa fa-plus-circle', 'text' => L('Нов')))
            ),
            'bottomBarItems' => array(
            )
        );
        parent::__construct($sPrefix, $aInitParams);
    }


    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, aBaseParams) {
            $(container).find('.new').click(function() {
                framework.createWindow({
                    tplName: 'emc/nomenclatures/clients/Edit',
                    baseParams: {row: {id: 0}, getParentGrid: function() {return container.grid;}}
                });
            });

            container.grid.clickListeners['name'] = function(rowNum, fieldElement, htmlElement) {
                framework.createWindow({
                    tplName: 'emc/nomenclatures/clients/Edit',
                    baseParams: { row: container.grid.getCurrentData()[rowNum], getParentGrid:function(){return container.grid;}}
                });
            };
        });
JS;
    }

}