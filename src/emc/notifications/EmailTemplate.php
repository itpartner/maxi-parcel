<?php

namespace emc\notifications;

use emc\production\orders\OrderTabHistory;
use emc\production\orders\OrderTabInfo;
use main\db\DBC;
use main\InvalidParameter;
use main\System;
use main\Util;

class EmailTemplate {

    private $message = [];

    function __construct($message) {
        $this->message = $message;
    }


    public function getMessage() {
        $messageContent = call_user_func_array([$this, 'getMessage_' . $this->message['event_code']], []);
        if(empty(trim($messageContent))) throw new InvalidParameter('getMessage_' . $this->message['event_code'] . ' has returned empty response. ' . json_encode($this->message, JSON_PRETTY_PRINT));
        ob_start();
        ?>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        </head>
        <body>
        <table align="center" border="0" cellspacing="0" cellpadding="0" style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #2e2e2e;">
            <tr>
                <td align="center" bgcolor="#f8f8f8" style="padding: 0 0 40px 0;">
                    <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="padding: 10px 20px; font-family: Verdana, Arial, Helvetica, sans-serif;">
                                <?= $messageContent ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        </body>
        </html>
        <?php
        return ob_get_clean();
    }

    public function getSubject() {
        return (System::isReal() ? '' : '[DEMO] ') . call_user_func_array([$this, 'getSubject_' . $this->message['event_code']], []);
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    private function getSubject_delayed_order_sizes() {
        return "Известие за забавяне на производствени размери";
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    private function getMessage_delayed_order_sizes() {
        $orderSizesByLine = [];

        foreach ($this->message['items'] as $notification) {
            foreach ($notification['additional_params'] as $size) {
                $orderSizesByLine[$size['line_code']][$size['id_order_size']] = $size;
            }
        }

        ob_start();
        ?>

        <?php foreach ($orderSizesByLine as $line => $sizes) : ?>
            <?php if(count($sizes) > 1): ?>
                <span>На линия <?= $line ?> са налични следните нови закъснения:</span>
            <?php else: ?>
                <span>На линия <?= $line ?> е налично ново закъснение:</span>
            <?php endif; ?>
            <table border="1" cellpadding="5" style="border-collapse: collapse;">
                <tr>
                    <th rowspan="2" width="400">Размер</th>
                    <th colspan="2">Излизане от производство</th>
                </tr>
                <tr>
                    <th>Желано</th>
                    <th>Планирано</th>
                </tr>
                <?php foreach ($sizes as $size): ?>
                    <tr>
                        <td style="line-height: 1.5em;">
                            <span><?= $size['size_code']; ?></span><br>
                            <span><?= $size['order_num'] . '/' . date('d.m.Y', strtotime($size['order_date'])); ?></span><br/>
                            <?= $size['client_name']; ?>
                        </td>
                        <td>
                            <?= date('d.m.Y', strtotime($size['desired_production_end'])); ?>
                        </td>
                        <td>
                            <?= date('d.m.Y', strtotime($size['real_time_to'])); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <br>
        <?php endforeach; ?>

        <?php
        return ob_get_clean();
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    private function getSubject_confirmed_order_change() {
        return "Настъпили промени в потвърдени поръчки";
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    private function getMessage_confirmed_order_change() {
        $data = [];

        foreach ($this->message['items'] as $item) {
            foreach ($item['additional_params']['idLines'] as $idLine) {
                $data[$idLine][$item['additional_params']['idOrder']]['updated_time'] = $data[$idLine][$item['additional_params']['idOrder']]['updated_time'] ? min($data[$idLine][$item['additional_params']['idOrder']]['updated_time'], $item['additional_params']['updatedTime']) : $item['additional_params']['updatedTime'];
                foreach ($item['additional_params']['orderSizeIDs'] as $idOrderSize) {
                    $data[$idLine][$item['additional_params']['idOrder']]['order_sizes'][$idOrderSize] = $idOrderSize;
                }

            }
        }

        foreach ($data as $idLine => $orders) {
            foreach ($orders as $idOrder => $order) {
                $data[$idLine][$idOrder]['order'] = OrderTabInfo::getOrderByID($idOrder);
                if(empty($data[$idLine][$idOrder]['order'])) throw new InvalidParameter();

                $historyRows = [];
                foreach (OrderTabHistory::getHistoryForOrder($idOrder, $order['updated_time'] - 30) as $historyRow) {
                    $historyRows[$historyRow['updated_time'] . '_' . $historyRow['item']][] = $historyRow;
                }
                $data[$idLine][$idOrder]['history'] = $historyRows;
            }
        }

        $lines = DBC::$main->selectAssoc("SELECT * FROM production_lines WHERE id IN(" . implode(',', array_keys($data)) . ")");

        ob_start();

        foreach ($data as $idLine => $orders) {
            ?>
            <h3>Настъпили промени в поръчки на производствена линия <strong><?= $lines[$idLine]['code']; ?></strong>:</h3>
            <?php
            foreach ($orders as $idOrder => $details) {
                echo "<span style='font-weight: bold'>- Поръчка {$details['order']['num']}/" . date('d.m.Y', Util::SQLDateToTimeStamp($details['order']['created_time'])) . "</span><br/>";
                ?>
                <table cellpadding="3" border="1px" style="border-collapse: collapse;">
                    <tr style="text-align: left;">
                        <th width="20%">Размер / Поръчка</th>
                        <th width="30%">Настъпили промени</th>
                        <th width="35%">Причина</th>
                        <th width="15%">Дата</th>
                    </tr>

                    <?php
                    foreach ($details['history'] as $k => $rows) {
                        $sizeCode = reset($rows)['item'];
                        $date = reset($rows)['updated_time'];
                        $note = reset($rows)['update_note'];
                        $idOrderSize = reset($rows)['id_order_size'];
                        if($idOrderSize && empty($details['order_sizes'][$idOrderSize])) continue;
                        ?>
                        <tr>
                            <td><?= $sizeCode; ?></td>
                            <td>
                                <ul style="padding-left: 1.5em;">
                                    <?php foreach ($rows as $row) {
                                        if(isset($row['old_value']) && isset($row['new_value'])) {
                                            echo "<li>{$row['field']} (\"{$row['old_value']}\"→\"{$row['new_value']}\")</li>";
                                        } else {
                                            echo "<li>{$row['field']}</li>";
                                        }
                                    } ?>
                                </ul>
                            </td>
                            <td><?= $note; ?></td>
                            <td><?= Util::formatTime(Util::SQLDateToTimeStamp($date), 'short'); ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                <br/>
                <br/>
                <?php
            }
        }
        return ob_get_clean();
    }
}