<?php

namespace emc\notifications;

use emc\production\orders\OrderTabHistory;
use emc\production\orders\sizes\Size;
use main\daemons\BaseRobot;
use main\db\DBC;

class RobotCreateNotifications extends BaseRobot {

    public function run($nTime, $aParams = array()) {
        $notificationRows = $this->loadNotifications($nTime);
        $this->createMessages($notificationRows);
    }


    private function loadNotifications($nTime) {
        return DBC::$main->withTrans(function ($nTime) {
            DBC::$main->select("SELECT * FROM automatic_notification LIMIT 1 FOR UPDATE");
            $notificationRows = DBC::$main->select("
                SELECT
                    an.*,
                    ane.code as event_code
                FROM automatic_notification an 
                JOIN automatic_notification_event ane ON ane.id = an.event_id
                WHERE 1
                AND an.status = 'wait'
                AND an.send_after < '" . date("Y-m-d H:i:s", $nTime) . "'
			    ORDER BY an.id
            ");

            if(empty($notificationRows)) return [];
            $nRobotTaskID = (int)$this->taskID;

            $ids = [];
            $notificationRows = array_map(function ($row) use (&$ids) {
                $ids[] = $row['id'];
                $row['additional_params'] = json_decode($row['additional_params'], true);
                return $row;
            }, $notificationRows);

            DBC::$main->execute("UPDATE automatic_notification SET `status` = 'processing', robot_task_id = $nRobotTaskID WHERE id IN(" . implode(',', $ids) . ")");

            return $notificationRows;
        }, [$nTime]);
    }


    private function createMessages($notificationRows) {
        if(empty($notificationRows)) return;

        DBC::$main->withTrans(function ($notificationRows) {
            $messagesToSend = [];
            $notificationsToUpdate = [];

            foreach ($notificationRows as $k => $row) {
                if($row['event_code'] == 'delayed_order_sizes') {
                    $size = DBC::$main->selectByID('production_orders_sizes', reset($row['additional_params']));

                    if(empty($size['is_delayed'])) {
                        $notificationsToUpdate[] = [
                            'id' => $row['id'],
                            'status' => 'canceled',
                            'robot_task_id' => 0
                        ];
                        unset($notificationRows[$k]);
                        continue;
                    }

                    $notificationRows[$k]['additional_params'] = DBC::$main->select("
                        SELECT
                            pos.id AS id_order_size,
                            pl.code AS line_code,
                            po.num AS order_num,
                            po.created_time as order_date,
                            po.name as client_name,
                            psz.code AS size_code,
                            pos.desired_production_end,
                            pos.planned_production_end AS real_time_to
                        FROM production_schedule ps 
                        JOIN production_orders_sizes pos ON pos.id = ps.id_order_size
                        JOIN production_sizes psz ON psz.id = pos.id_size
                        JOIN production_orders po ON po.id = pos.id_order
                        JOIN production_lines pl ON pl.id = ps.id_line
                        WHERE 1
                        AND ps.to_arc = 0
                        AND ps.id_order_size = {$size['id']}
                        GROUP BY pos.id
                    ");
                } else if($row['event_code'] == 'confirmed_order_change') {
                    $notificationRows[$k] = array_merge($notificationRows[$k], $notificationRows[$k]['additional_params']);
                }
            }

            foreach ($notificationRows as $messageRow) {
                foreach (['event_id', 'send_after', 'target', 'event_code', 'event_id'] as $field) $messagesToSend[$messageRow['target']][$messageRow['event_id']][$field] = $messageRow[$field];
                $messagesToSend[$messageRow['target']][$messageRow['event_id']]['items'][$messageRow['id']] = $messageRow;
                $messagesToSend[$messageRow['target']][$messageRow['event_id']]['count']++;
            }

            foreach ($messagesToSend as $target => $messages) {
                foreach ($messages as $eventID => $message) {
                    $tpl = new EmailTemplate($message);

                    $readyMessage = [
                        'status' => 'wait',
                        'send_after' => $message['send_after'],
                        'target' => $message['target'],
                        'subject' => $tpl->getSubject(),
                        'message' => $tpl->getMessage(),
                    ];

                    DBC::$main->update('automatic_notification_messages', $readyMessage);

                    foreach ($message['items'] as $notification) {
                        $notificationsToUpdate[] = [
                            'id' => $notification['id'],
                            'status' => 'processed',
                            'message_id' => $readyMessage['id'],
                            'robot_task_id' => 0,
                        ];
                    }
                }
            }

            DBC::$main->multiInsert('automatic_notification', $notificationsToUpdate);

        }, [$notificationRows]);
    }
}