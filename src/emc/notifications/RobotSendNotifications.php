<?php
namespace emc\notifications;

use main\daemons\BaseRobot;
use main\db\DBC;
use main\notifications\Mailer;

class RobotSendNotifications extends BaseRobot {

    public function run($nTime, $aParams = array()) {
        $messages = $this->loadMessages($nTime);
        $this->sendMessages($messages);
    }

    private function loadMessages($nTime) {
        return DBC::$main->withTrans(function ($nTime) {
            DBC::$main->select("SELECT * FROM automatic_notification_messages LIMIT 1 FOR UPDATE");
            $messages = DBC::$main->selectAssoc("
                SELECT 
                    anm.id as _k,
                    anm.*
                FROM automatic_notification_messages anm
                JOIN automatic_notification anb ON anb.message_id = anm.id
                WHERE anm.status = 'wait'
                AND anb.status = 'processed'
                AND anm.send_after < '" . date('Y-m-d H:i:s', $nTime) . "'
                LIMIT 1000
            ");

            if(!empty($aMessageIDs)) {
                $nRobotTaskID = (int)$this->taskID;
                DBC::$main->execute("UPDATE automatic_notification_messages SET status = 'sending', robot_task_id = $nRobotTaskID WHERE id IN(" . implode(',', array_keys($messages)) . ")");
            }

            return $messages;
        }, [$nTime]);
    }


    private function sendMessages($messages) {
        if(empty($messages)) return;

        foreach ($messages as $id => $message) {
            $error = Mailer::send(explode(',', $message['target']), $message['subject'], $message['message']);
            $messages[$id]['status'] = 'sent';
            $messages[$id]['send_attempt_error'] = json_encode($error);
        }

        DBC::$main->multiInsert('automatic_notification_messages', $messages);
    }


}