<?php

namespace emc\notifications;

use main\db\DBC;
use main\InvalidParameter;
use main\Settings;

class Notifications {

    public static function addNotification($eventType, $target, $params, $nSendAfter = 0) {
        return DBC::$main->withTrans(function ($eventType, $target, $params, $nSendAfter = 0) {

            $event = self::getEventByCode($eventType);

            $nSendAfter = $nSendAfter ? $nSendAfter : mktime(date('H'), ((int)(date('i') / 10)) * 10 + 10, 0, date('m'), date('d'), date('Y'));
            if($nSendAfter <= time()) $nSendAfter = time();

            $notification = [
                'event_id' => $event['id'],
                'status' => 'wait',
                'send_after' => $nSendAfter,
                'target' => $target,
                'additional_params' => json_encode($params, JSON_UNESCAPED_UNICODE)
            ];

            DBC::$main->update('automatic_notification', $notification);

            return $notification;
        }, [$eventType, $target, $params, $nSendAfter]);
    }

    private static function getEventByCode($code) {
        return DBC::$slave->selectRow("SELECT * FROM automatic_notification_event WHERE code = " . DBC::$slave->quote($code));
    }

    public static function notifyDelayedProduction($idOrderSizes) {
        if(empty($idOrderSizes)) return;
        foreach ($idOrderSizes as $idOrderSize) {
            Notifications::addNotification('delayed_order_sizes', reset(Settings::$aSettings['Notifications']['delayed_order_mails']), [$idOrderSize], strtotime('+1 hour', mktime(date('H'), 0, 0, date('m'), date('d'), date('Y'))));
        }
    }

    public static function notifyChangedOrder($idOrder, $orderSizeIDs = [], $idLines = [], $updatedTime) {
        if(empty($idLines)) throw new InvalidParameter();
        if(empty($idOrder) && empty($orderSizeIDs)) throw new InvalidParameter();

        $idLines = array_map('intval', $idLines);

        $lines = DBC::$main->select("
            SELECT
                pl.id,                   
                p.email
            FROM production_lines pl 
            JOIN personnel p ON p.id = pl.id_line_production_manager
            WHERE 1
            AND pl.id IN(" . implode(',', $idLines) . ")
        ");

        foreach ($lines as $line) {
            Notifications::addNotification('confirmed_order_change', $line['email'], ['idOrder' => $idOrder, 'orderSizeIDs' => $orderSizeIDs, 'idLines' => $idLines, 'updatedTime' => $updatedTime]);
        }
    }

}