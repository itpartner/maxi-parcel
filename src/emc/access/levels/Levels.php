<?php
namespace emc\access\levels;

use main\Access;
use main\db\DBC;
use main\grid\GridPanelHandler;
use main\InvalidParameter;
use main\Util;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;

/**
 * Class Levels
 * @package emc\access\levels
 * @accessLevelDescription Нива на достъп в системата
 */
class Levels extends GridPanelHandler {

    public function __construct() {
        $this->oBase = DBC::$main;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'check',
                'width' => 30,
                'headerText' => '',
                'sortable' => false,
                'resizable' => false,
                'filterable' => false,
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'type',
                'width' => 50,
                'headerText' => L('Тип'),
                'items' => [
                    'dir' => ['text' => L('директория'), 'iconCls' => 'fa fa fa-folder-open-o'],
                    'file' => ['text' => L('файл'), 'iconCls' => 'fa fa fa-file-o'],
                ],
                'filterable' => false,
                'resizable' => false,
                'sortable' => false
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'level',
                'width' => 350,
                'headerText' => 'Ниво',
                'sortable' => false,
                'resizable' => true,
                'filterable' => false
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'description',
                'width' => 350,
                'headerText' => 'Описание',
                'sortable' => false,
                'resizable' => true,
                'filterable' => false
            ))
        );
        $this->bUseSQLFilters = false;
        $this->bUseSQLPaging = false;
        $this->bUseSQLSort = false;

        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        $aRequest['row'] = $aInitParams['row'];
        return $this->loadData($aRequest);
    }

    public function loadData($aRequest) {
        $aData = $this->getGridData($aRequest);
        $aData = $this->processData($aData);

        $aRequest['sortFields'][] = ['field' => 'level', 'dir' => 'ASC'];
        $this->sortData($aData, $aRequest['sortFields']);

        $this->setGridData($aData);
        return $this->oResponse;
    }

    protected function getGridData($aRequest) {
        $idProfile = intval($aRequest['row']['id']);
        if(empty(($idProfile))) throw new InvalidParameter;

        $profileLevels = $this->oBase->selectAssoc("SELECT ap.level as __key, ap.level FROM access_profiles_levels ap WHERE ap.id_profile = {$idProfile}");

        $objects['\\'] = array('level' => '\\', 'description' => 'Достъп до всички функционалности на системата');
        foreach (array('/src/main', '/src/pallex') as $dir) foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator(BASE_PATH . $dir)) as $level => $object) {
            /** @var SplFileInfo $object */
            if($object->isDir() || $object->getExtension() != 'php' || $object->getFilename() == 'autoload.php') continue;
            $level = str_replace(array(BASE_PATH . '/src/', '.php'), '', $level);
            $target = parseNS($level);
            $level = $target['class'];
            if(!class_exists($target['class'])) continue;

            $interfaces = class_implements($target['class']);
            if(!$interfaces['main\rpc\RPCHandler'] && !$interfaces['emc\access\AccessLevel']) continue;
            if($target['ns'] == '\main\form' || $target['ns'] == '\main\grid') continue;

            if(!$objects[$target['ns']]) {
                $path = explode('\\', $target['ns']);
                while (!!($l = implode('\\', $path))) {
                    $objects[$l] = array('level' => $l, 'type' => 'dir');
                    array_pop($path);
                }
            }

            $objects[$level] = array('level' => $level, 'type' => 'file');
            $annotations = Util::getClassAnnotations($target['class']);
            $objects[$level]['description'] = $annotations['@accessLevelDescription'];
        }

        $objects = array_map(function ($l) use ($profileLevels) {
            $l['check'] = !!$profileLevels[$l['level']];
            return $l;
        }, $objects);

        $aData = $objects;
        return $aData;
    }

    public function save($aRequest) {
        Access::checkAccess(__CLASS__);
        $this->oBase->withTrans(function ($aRequest) {
            $idProfile = intval($aRequest['row']['id']);
            $this->oBase->multiInsert('access_profiles', $this->oBase->select("SELECT id,name FROM access_profiles WHERE id = {$idProfile}"));
            if(empty(($idProfile))) throw new InvalidParameter;
            $this->oBase->execute("DELETE FROM access_profiles_levels WHERE id_profile = {$idProfile}");
            $this->oBase->multiInsert('access_profiles_levels', array_map(function ($level) use ($idProfile) {
                return array('id_profile' => $idProfile, 'level' => $level);
            }, $aRequest['levels']));
        }, array($aRequest));

        return $this->loadData($aRequest);
    }

}