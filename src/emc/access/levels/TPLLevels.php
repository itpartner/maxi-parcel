<?php
namespace emc\access\levels;

use main\grid\TPLGridPanel;

class TPLLevels extends TPLGridPanel {

    public function __construct($sPrefix, $aInitParams = array()) {
        $this->{'aConfig'} = array(
            'title' => L('Нива на достъп за профил: %s', $aInitParams['row']['name']),
            'autoVResize' => true,
            'disablePaging' => true,
            'dataHeight' => '300px',
            'hideRowIndexes' => true,
            'topBarItems' => array(
            ),
            'bottomBarItems' => array(
                array('confirmbutton'),
                array('closebutton')
            )
        );

        $this->{'bNoDefaultTopBarItems'} = true;

        parent::__construct($sPrefix, $aInitParams);
    }

    /**
     * @param $sPrefix
     */
    public function printStyles($sPrefix) {
        ?>
        <!--suppress CssUnusedSymbol -->
        <style type="text/css">
            #<?=$sPrefix?> .column-check div {
                text-align: center;
            }

            #<?=$sPrefix?> tr.disabled {
                               opacity: 0.6;
                               font-style: italic;
                           }
        </style>
        <?php
    }


    public function getJSFunction() {
        return <<<'JS'
        (function(container, prefix, aBaseParams) {
            var grid = container.grid;

            var rowCheck = function(checkbox, rowData) {
                if(rowData['_row_class_name'] == 'disabled'){
                    checkbox.checked = false;
                    return;
                }

                var gridData = grid.getCurrentData();

                for(var i in gridData) {
                    var row = gridData[i];

                    if(row['level'] == rowData['level']) {
                        row['check'] = !!checkbox.checked;
                    } else if(row['level'].indexOf(rowData['level']+'\\') === 0) {
                        row['check'] = 0;
                        row['_row_class_name'] = checkbox.checked ? 'disabled' : '';
                    }
                }

                grid.setData(gridData);
            };

            grid.addListener('beforeSetData', function(){
                grid.itemRenderers['check'] = function(columnCfg, rowData, clickFn){
                    var checkbox = document.createElement('input');
                    checkbox.setAttribute('type', 'checkbox');
                    checkbox.checked = !!rowData['check'];
                    $(checkbox).click(function(){rowCheck.apply({},[checkbox, rowData]);});
                    return checkbox;
                };
            });


            $(container).find('.confirm').click(function(){
                var levels = [];
                $(grid.getCurrentData()).each(function(k,v){
                    if(v['check']) levels.push(v['level']);
                });
                container.request('save',{row:aBaseParams.row, levels:levels});
            });

            container.addListener('afterRequest',function(result, method, requestParams){
                $(grid).find("[type='checkbox']:checked").click();
            });

        });
JS;
    }

}