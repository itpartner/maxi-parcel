<?php
namespace emc\access\profiles;

use main\Access;
use main\db\DBC;
use main\form\FormPanelHandler;
use main\InvalidParameter;

/**
 * Class Edit
 * @accessLevelDescription Редактиране на профил за достъп
 */
class Edit extends FormPanelHandler {

    public function __construct() {
        Access::checkAccess(__CLASS__);
        return parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        $this->setField('name', $aInitParams['row']['name']);
        return $this->oResponse;
    }

    public function save($aRequest) {
        if(empty($aRequest['fields']['name'])) throw new InvalidParameter(L('Моля, попълнете име на профила'), array('name'));
        DBC::$main->multiInsert('access_profiles',array(array('id'=>$aRequest['row']['id'], 'name' => $aRequest['fields']['name'])));
        return $this->oResponse;
    }

    public function delete($aRequest) {
        $idProfile = intval($aRequest['row']['id']);
        if(empty(($idProfile))) throw new InvalidParameter;
        if(!empty(DBC::$main->selectField("SELECT id_profile FROM access_account_profiles WHERE id_profile = {$idProfile}"))) throw new InvalidParameter(L("Профила е назначен на служител(и) и не може да бъде изтрит."));
        DBC::$main->delete('access_profiles',$idProfile);
        return $this->oResponse;
    }

}