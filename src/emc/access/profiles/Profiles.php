<?php
namespace emc\access\profiles;

use main\Access;
use main\db\DBC;
use main\grid\GridPanelHandler;

/**
 * Class Profiles
 * @package emc\access\profiles
 * @accessLevelDescription Управление на потребителски профили за достъп
 *
 */
class Profiles extends GridPanelHandler {

    public function __construct() {
        Access::checkAccess(__CLASS__);

        $this->oBase = DBC::$main;

        $this->aColumns = array(
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'name',
                'width' => 150,
                'headerText' => 'Профил',
                'sortable' => true,
                'resizable' => true,
                'filterable' => true
            )),
            $this->newColumn(
                array(
                    'type' => GridPanelHandler::CTYPE_BTN,
                    'iconCls' => 'icon fa fa-pencil',
                    'dataField' => 'edit',
                    'headerText' => '',
                    'width' => 30,
                    'sortable'  => false,
                    'resizable' => false,
                    'filterable'=> false
                )),
            $this->newColumn(
                array(
                    'type' => GridPanelHandler::CTYPE_BTN,
                    'iconCls' => 'icon fa fa-trash',
                    'dataField' => 'delete',
                    'headerText' => '',
                    'width' => 30,
                    'sortable'  => false,
                    'resizable' => false,
                    'filterable'=> false
                )),
            $this->newColumnEditedBy()
        );

        $this->sReportSelectStatement = "
            SELECT SQL_CALC_FOUND_ROWS 
                p.*,
                CONCAT_WS(' ',pc.fname,pc.mname,pc.lname) as created_user,
                CONCAT_WS(' ',pu.fname,pu.mname,pu.lname) as updated_user
            FROM access_profiles p
            LEFT JOIN personnel pc ON pc.id = p.created_user
            LEFT JOIN personnel pu ON pu.id = p.updated_user
        ";
        $this->aReportWhereStatement[] = 'p.to_arc = 0';
        $this->aDefaultSort[] = array('field' => 'p.name', 'dir' => 'ASC');

        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
    }

    public function loadData($aRequest) {
        return parent::loadData($aRequest);
    }

    public function delete($aRequest) {
        $o = new Edit();
        $o->delete($aRequest);
        return $this->loadData($aRequest);
    }

}