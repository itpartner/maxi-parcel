<?php
namespace emc\access\profiles;

use main\TPLBase;

class TPLEdit extends TPLBase {

    public function __construct($sPrefix, array $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);

        $this->bPrintWithTitle = true;
        $this->aFrameConfig['title'] = empty($aInitParams['row']['id']) ? L('Нов профил') : L('Редакция на профил:') . $aInitParams['row']['name'];
    }

    protected function printStyles($sPrefix) {
        ?>
        <style>
            #<?=$this->sPrefix?> .width100 {
                box-sizing: border-box;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                width: 100%;
            }
        </style>
        <?php
    }

    public function printHtml() { ?>
        <table style="width: 100%">
            <tr>
                <td><? $this->printElement('label', array('fieldName'=>'name', 'text' => L('Профил:')));?></td>
                <td><? $this->printElement('input', array('name'=>'name', 'class'=>'width100')); ?></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: right">
                        <? $this->printElement('confirmbutton'); ?>
                        <? $this->printElement('closebutton'); ?>
                </td>
            </tr>
        </table>
        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, baseParams) {
            $(container).find('.confirm').click(function(){
                container.request('save', {row:baseParams['row']}, null, function(){
                    $(container).find('.close').click();
                    baseParams.getParentGrid().loadData();
                });
            });
        });
JS;
    }


}