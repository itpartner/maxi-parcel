<?php
namespace emc\access\profiles;

use main\grid\TPLGridPanel;

class TPLProfiles extends TPLGridPanel {

    public function __construct($sPrefix, $aInitParams = array()) {
        $this->{'aConfig'} = array(
            'title' => L('Профили за достъп'),
            'autoVResize' => true,
            'topBarItems' => array(
                array('button',array('iconCls'=>'icon fa fa-plus', 'text' => L("Нов профил"), 'class' => 'new-profile')),
            ),
            'bottomBarItems' => array(
                array('exportbutton', array('align' => 'right'))
            )
        );
        parent::__construct($sPrefix, $aInitParams);
    }


    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, aBaseParams) {
            var grid = container.grid;

           grid.clickListeners['name'] = function(rowNum, rowData, htmlElement) {
                framework.createWindow({
                    tplName: 'emc/access/levels/Levels',
                    baseParams: {row: rowData}
                });
            };

           grid.clickListeners['delete'] = function(rowNum, rowData, htmlElement) {
                framework.confirm('', 'Потвърждавате ли изтриване на профила?', function(r){
                    if(!r) return;
                    container.request('delete', {row:rowData});
                });
            };


           var openProfile = function(rowData){
                 framework.createWindow({
                    tplName: 'emc/access/profiles/Edit',
                    baseParams: {
                        row:rowData,
                        getParentGrid:function(){return grid;}
                    }
                });
           };

           grid.clickListeners['edit'] = function(rowNum, rowData, htmlElement) {
                openProfile(rowData);
           };

           $(container).find('.new-profile').click(function(){openProfile({});});
        });
JS;
    }

}