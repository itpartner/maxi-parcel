<?php
namespace emc\personnel;

use main\db\DBC;
use main\TPLBase;
use pallex\PallexUtils;

class TPLEdit extends TPLBase {

    public function __construct($sPrefix, array $aInitParams) {
        parent::__construct($sPrefix, $aInitParams);

        $this->bPrintWithTitle = true;
        if (empty($aInitParams['row']['id'])) {
            $this->aFrameConfig['title'] = L('Нов служител');
        } else {
            $sName = $aInitParams['row']['fname'] . ' ' . $aInitParams['row']['mname'] . ' ' . $aInitParams['row']['lname'];
            $this->aFrameConfig['title'] = L('Редакция на служител:') . ' ' . $sName;
        }
    }

    protected function printStyles($sPrefix) { ?>
        <style>
            #<?= $sPrefix; ?> .fieldsets {
                vertical-align: top;
                height: 235px;
            }

            #<?= $sPrefix; ?> fieldset {
                height: 100%;
            }

            #<?= $sPrefix; ?> .full-parent-width {
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
                width: 100%;
            }
            #<?= $sPrefix; ?> .sel-height {
                height: 200px;
                              }
        </style>
    <?php }

    public function printHtml() {

        $aAccessProfiles = DBC::$slave->select("SELECT id, name AS value FROM access_profiles WHERE to_arc = 0");
        $aStatuses = array(
            array('id' => 'active', 'value' => L('Активен')),
            array('id' => 'vacаte', 'value' => L('Неактивен'))
        );

        ?>

        <table style="width: 100%">
            <tr>
                <td class="fieldsets">
                    <fieldset>
                        <legend>Основни</legend>
                        <table>
                            <tr>
                                <td><? $this->printElement('label', array('fieldName' => 'fname', 'text' => L('Име'))); ?></td>
                                <td><? $this->printElement('input', array('name' => 'fname')); ?></td>
                            </tr>
                            <tr>
                                <td><? $this->printElement('label', array('fieldName' => 'mname', 'text' => L('Презиме'))); ?></td>
                                <td><? $this->printElement('input', array('name' => 'mname')); ?></td>
                            </tr>
                            <tr>
                                <td><? $this->printElement('label', array('fieldName' => 'lname', 'text' => L('Фамилия'))); ?></td>
                                <td><? $this->printElement('input', array('name' => 'lname')); ?></td>
                            </tr>
                            <tr>
                                <td><? $this->printElement('label', array('fieldName' => 'phone', 'text' => 'Телефон')); ?></td>
                                <td><? $this->printElement('input', array('name' => 'phone')); ?></td>
                            </tr>
                            <tr>
                                <td><? $this->printElement('label', array('fieldName' => 'email', 'text' => 'E-mail')); ?></td>
                                <td><? $this->printElement('input', array('name' => 'email', 'keyRestriction' => 'email')); ?></td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
                <td class="fieldsets">
                    <fieldset>
                        <legend>Системни</legend>
                        <table>
                            <tr>
                                <td style="vertical-align: top; padding-top: 2px;"><? $this->printElement('label', array('fieldName' => 'profile_id', 'text' => L('Потребителски профил'))); ?></td>
                                <td><? $this->printElement('select', array('name' => 'profile_id', 'multiple' => 'multiple', 'style' => 'height: 80px;'), $aAccessProfiles); ?></td>
                            </tr>
                            <tr>
                                <td><? $this->printElement('label', array('fieldName' => 'status', 'text' => L('Статус'))); ?></td>
                                <td><? $this->printElement('select', array('name' => 'status'), $aStatuses); ?></td>
                            </tr>
                            <tr>
                                <td><? $this->printElement('label', array('fieldName' => 'username', 'text' => L('Потребителско име'))); ?></td>
                                <td><? $this->printElement('input', array('name' => 'username', 'class' => 'full-parent-width', 'style' => 'height: 26px;')); ?></td>
                            </tr>
                            <tr>
                                <td><? $this->printElement('label', array('fieldName' => 'password', 'text' => L('Нова парола'))); ?></td>
                                <td><? $this->printElement('input', array('name' => 'password', 'type' => 'password', 'class' => 'full-parent-width', 'style' => 'height: 26px;')); ?></td>
                            </tr>
                            <tr>
                                <td><? $this->printElement('label', array('fieldName' => 'repeat_password', 'text' => L('Повтори парола'))); ?></td>
                                <td><? $this->printElement('input', array('name' => 'repeat_password', 'type' => 'password', 'class' => 'full-parent-width', 'style' => 'height: 26px;')); ?></td>
                            </tr>
                            <tr>
                                <td><? $this->printElement('label', array('fieldName' => 'row_limit', 'text' => L('Брой редове в справка'))); ?></td>
                                <td><? $this->printElement('input', array('name' => 'row_limit', 'keyRestriction' => 'integer', 'style' => 'width: 30px;')); ?></td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td class="fieldsets" colspan="2">
                    <fieldset>
                        <legend>Посредници</legend>
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 50%;">
                                    <?php
                                    $leftMediators = PallexUtils::dbLoadMediators();
                                    $rightMediators = PallexUtils::dbLoadMediatorAccess($this->aInitParams['row']['id']);
repeat_label:
                                    foreach ($leftMediators as $lk => $lv) {
                                        $found = false;
                                        foreach ($rightMediators as $rk => $rv) {
                                            if ($lv == $rv) {
                                                $found = true; break;
                                            }
                                        }
                                        if ($found) {
                                            unset($leftMediators[$lk]);
                                            goto repeat_label;
                                        }
                                    }

                                    ?>
                                    <? $this->printElement('select',
                                        [
                                                'multiple' => 'multiple',
                                                'name' => 'all_mediators',
                                                'class' => 'full-parent-width sel-height'
                                        ], $leftMediators); ?>
                                </td>
                                <td style="width: 50%;">
                                    <? $this->printElement('select',
                                        [
                                            'multiple' => 'multiple',
                                            'name' => 'access_mediators',
                                            'class' => 'full-parent-width sel-height'
                                        ], $rightMediators); ?>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="toolbar align-right" style="margin-top: 5px">
                        <? $this->printElement('closebutton'); ?>
                        <? $this->printElement('confirmbutton'); ?>
                        <? $this->printElement('lasteditedinfo', array('class' => 'align-left')); ?>
                        <? $this->printElement('button', array('class' => 'delete align-left', 'iconCls' => 'icon fa fa-trash', 'text' => L('Изтрий'))); ?>
                    </div>
                </td>
            </tr>
        </table>

        <?php
    }

    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, baseParams) {

            if(!baseParams['row']['id']) $(container).find('.delete').css('display', 'none');

            var updateParent = function() {
                baseParams.getParentGrid().loadData();
                $(container).find('.close').click();
            };

            $(container).find('.confirm').click(function(){
                $('select[name="access_mediators"] option').each(function() {
                    $(this).attr('selected', 'selected');
                });
                container.request('save', {id: baseParams['row']['id']}, null, function() {
                    updateParent();
                });
            });

            $(container).find('.delete').click(function() {
                container.request('delete', {id_person: baseParams['row']['id'], id_account: baseParams['row']['id_account']}, null, function() {
                    updateParent();
                });
            });
            
            $(container).find('.sel-height').dblclick(function() {
                if (this.getAttribute('name') == 'all_mediators') {
                    let val = $(this).val();
                    if (val == null) return;
                    
                    let txt = $(this).find('option[value="' + val + '"]').text();
                    $(this).find('option[value="' + val + '"]').remove();
                    $('select[name="access_mediators"]').append(
                        $('<option>', {
                            value: val,
                            text: txt
                        })
                    );
                }
                else
                    {
                        let val = $(this).val();
                        if (val == null) return;
                    
                        let txt = $(this).find('option[value="' + val + '"]').text();
                        $(this).find('option[value="' + val + '"]').remove();
                        $('select[name="all_mediators"]').append(
                            $('<option>', {
                                value: val,
                                text: txt
                            })
                        ); 
                        }
            });
        });
JS;
    }


}