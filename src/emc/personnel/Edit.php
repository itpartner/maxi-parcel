<?php
namespace emc\personnel;

use main\Access;
use main\db\DBC;
use main\Exception;
use main\form\FormPanelHandler;
use main\InvalidParameter;
use pallex\PallexUtils;

/**
 * Class Edit
 * @accessLevelDescription Редактиране на служител
 */
class Edit extends FormPanelHandler {

    public function __construct() {
        parent::__construct();

        Access::checkAccess(__CLASS__);
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        foreach ($aInitParams['row'] as $k => $v) $this->setField($k, $v);

        $nIdAccount = intval($aInitParams['row']['id_account']);
        $aProfileIds = DBC::$main->selectColumn("SELECT id_profile FROM access_account_profiles WHERE id_account = $nIdAccount");
        $this->setFieldValue('profile_id', $aProfileIds);

        $this->setLastEditInfo($aInitParams['row']);
        return $this->oResponse;
    }

    public function save($aRequest) {
        unset($aRequest['fields']['last_edited_info']);
        DBC::$main->withTrans(function ($aRequest) {
            $aPerson = $aRequest['fields'];
            foreach ($aPerson as $mKey => $sField) {
                if ($mKey != 'profile_id') {
                    $sField = trim($sField);
                }
                $aPerson[$mKey] = $sField;
                if ($mKey != 'password' && $mKey != 'repeat_password' &&
                    $mKey != 'access_mediators' && $mKey != 'all_mediators' && empty($sField)) {
                    throw new InvalidParameter(L('Моля, въведете стойности за всички полета. Изключение правят полета "Нова парола" и "Повтори парола", при редакция на служител.'), array($mKey));
                }
            }
            $aPerson['id'] = intval($aRequest['id']);
            if (empty($aPerson['id']) && (empty($aPerson['password']) || empty($aPerson['repeat_password']))) {
                throw new InvalidParameter(L('Моля, въведете парола при създаване на нов служител.'), array('password'));
            }

            if (!filter_var($aPerson['email'], FILTER_VALIDATE_EMAIL)) {
                throw new InvalidParameter(L('Не сте въвели валидаен E-mail.'), array('email'));
            }

            $sEnteredEmail = DBC::$slave->quote($aPerson['email']);
            $sEmail = DBC::$slave->selectField("SELECT email FROM personnel WHERE email = $sEnteredEmail AND id != {$aPerson['id']} AND to_arc = 0");
            if ($sEmail) throw new InvalidParameter(L('Вече съществува служител с такъв Email'), array('email'));

            if ($aPerson['row_limit'] <= 0) {
                throw new InvalidParameter(L('Моля въведете брой на редовете, по-голям от нула.'), array('row_limit'));
            }

            $sEnteredUsername = DBC::$slave->quote($aPerson['username']);
            $sUsername = DBC::$slave->selectField("SELECT username FROM access_account
                                                  WHERE username = $sEnteredUsername AND id_person != {$aPerson['id']} AND to_arc = 0");
            if ($sUsername) throw new Exception(L('Вече съществува служител с това потребителско име.'), array('username'));

            if (empty($aRequest['fields']['access_mediators'])) {
                throw new Exception(L('Не сте избрали достъп до посредник.'), 'access_mediators');
            }

            $bIsNewUser = empty($aPerson['id']);
            DBC::$main->update('personnel', $aPerson);

            // create/edit account
            $aPerson['id'] = intval($aPerson['id']);
            $aAccountInfo = DBC::$main->selectRow("SELECT * FROM access_account WHERE id_person = {$aPerson['id']} AND to_arc = 0");

            if ($bIsNewUser && !empty($aAccountInfo)) throw new Exception();

            $aAccountInfo['id_person'] = $aPerson['id'];
            $aAccountInfo['username'] = $aPerson['username'];
            $aAccountInfo['row_limit'] = $aPerson['row_limit'];
            if (!empty($aPerson['password'])) {
                $aPerson['password'] = password_hash($aPerson['password'], CRYPT_BLOWFISH);
                if (!password_verify($aPerson['repeat_password'], $aPerson['password'])) {
                    throw new InvalidParameter(L('Паролите се различават.'), array('repeat_password'));
                }
                $aAccountInfo['password'] = $aPerson['password'];
            }
            DBC::$main->update('access_account', $aAccountInfo);

            // set profiles
            $nIdAccount = intval($aAccountInfo['id']);
            DBC::$main->execute("DELETE access_account_profiles FROM access_account_profiles WHERE id_account = $nIdAccount");
            $aAccountProfiles = array();
            foreach ($aPerson['profile_id'] as $mKey => $mProfileId) {
                $aAccountProfiles[] = array(
                    'id_account' => intval($aAccountInfo['id']),
                    'id_profile' => intval($mProfileId)
                );
            }
            DBC::$main->multiInsert('access_account_profiles', $aAccountProfiles);

            $qry = "delete from access_mediators where id_person = ".intval($aPerson['id']).";";
            DBC::$main->execute($qry);
            foreach ($aRequest['fields']['access_mediators'] as $k => $v) {
                $qry = "insert into access_mediators set id_person = ".intval($aPerson['id']).", id_mediator = ".intval($v).";";
                DBC::$main->execute($qry);
            }
        }, array($aRequest));

        return $this->oResponse;
    }

    public function delete($aRequest) {
        DBC::$main->withTrans(function($aRequest) {
            $nIdAccount = intval($aRequest['id_account']);
            $nIdPerson = intval($aRequest['id_person']);
            if (empty($nIdAccount) || empty($nIdPerson)) throw new InvalidParameter();

            DBC::$main->delete('access_account', $nIdAccount);
            DBC::$main->delete('personnel', $nIdPerson);
        }, array($aRequest));
    }

}