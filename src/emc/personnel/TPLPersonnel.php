<?php
namespace emc\personnel;

use main\grid\TPLGridPanel;

class TPLPersonnel extends TPLGridPanel {

    public function __construct($sPrefix, $aInitParams = array()) {
        $this->{'aConfig'} = array(
            'title' => L('Служители'),
            'autoVResize' => true,
            'topBarItems' => array(
                array('button', array('name' => 'add', 'class' => 'add', 'iconCls' => 'icon fa fa-plus-circle', 'text' => L('Добави')))
            ),
            'bottomBarItems' => array(
                array('exportbutton', array('align' => 'right'))
            )
        );
        parent::__construct($sPrefix, $aInitParams);
    }


    public function getJSFunction() {
        return <<<'JS'

        (function(container, prefix, aBaseParams) {

            var openEditPersonnel = function(row) {
                framework.createWindow({
                    tplName: 'emc/personnel/Edit',
                    baseParams: { row: row || {}, getParentGrid:function(){return container.grid; }}
                });
            };

            container.grid.itemRenderers['name'] = function(content, rowData, clickFn) {
                var div = $("<div></div>");
                if(rowData['name']) div.html(rowData['name'].split('@@@').join('<br/>'));
                return div.get(0);
            };
            

            $(container).find('.add').click(function() {
                openEditPersonnel([]);
            });

            container.grid.clickListeners['btnEdit'] = function(rowNum, fieldElement, htmlElement) {
                openEditPersonnel(container.grid.getCurrentData()[rowNum]);
            };

            container.grid.clickListeners['fname'] = function(rowNum, fieldElement, htmlElement) {
                openEditPersonnel(container.grid.getCurrentData()[rowNum]);
            };

        });
JS;
    }

}