<?php
namespace emc\personnel;

use main\db\DBC;
use main\grid\GridPanelHandler;
use main\suggest\Suggest;

/**
 * Class Personnel
 * @accessLevelDescription Представя служителите в системата
 */
class Personnel extends GridPanelHandler {

    public function __construct() {
        $this->oBase = DBC::$slave;

        $this->bUseSQLFilters = false;
        $this->bUseSQLPaging = false;
        $this->bUseSQLSort = false;

        $this->aColumns = array(

            $this->newColumn(array(
                    'type' => 'group',
                    'headerText' => L('Служител'),
                    'children' => array(
                        $this->newColumn(array(
                            'type' => GridPanelHandler::CTYPE_STRING,
                            'dataField' => 'fname',
                            'width' => 100,
                            'headerText' => 'Име',
                            'sortable' => true,
                            'resizable' => true,
                            'filterable' => true
                        )),
                        $this->newColumn(array(
                            'type' => GridPanelHandler::CTYPE_STRING,
                            'dataField' => 'mname',
                            'width' => 100,
                            'headerText' => 'Презиме',
                            'sortable' => true,
                            'resizable' => true,
                            'filterable' => true
                        )),
                        $this->newColumn(array(
                            'type' => GridPanelHandler::CTYPE_STRING,
                            'dataField' => 'lname',
                            'width' => 100,
                            'headerText' => 'Фамилия',
                            'sortable' => true,
                            'resizable' => true,
                            'filterable' => true
                        )),
                    )
                )
            ),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_ENUM,
                'dataField' => 'status',
                'width' => 80,
                'headerText' => 'Статус',
                'sortable' => true,
                'resizable' => true,
                'filterable' => true,
                'items' => array(
                    'active' => array('text' => 'Активен'),
                    'vacаte' => array('text' => 'Неактивен')
                ),
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'email',
                'width' => 100,
                'headerText' => 'Email',
                'sortable' => true,
                'resizable' => true,
                'filterable' => true,
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'phone',
                'width' => 130,
                'headerText' => 'Телефон',
                'sortable' => true,
                'resizable' => true,
                'filterable' => true,
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'name',
                'width' => 150,
                'headerText' => 'Потебителски профил',
                'sortable' => true,
                'resizable' => true,
                'filterable' => true,
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_STRING,
                'dataField' => 'username',
                'width' => 130,
                'headerText' => 'Потребителско име',
                'sortable' => true,
                'resizable' => true,
                'filterable' => true,
            )),
            $this->newColumn(array(
                'type' => GridPanelHandler::CTYPE_DATETIME,
                'dataField' => 'created_time',
                'width' => 80,
                'headerText' => 'Назначен',
                'sortable' => true,
                'resizable' => true,
                'filterable' => true,
            )),
            $this->newColumn(
                array(
                    'type' => 'button',
                    'iconCls' => 'icon fa fa-pencil',
                    'iconClsField' => 'img_class',
                    'dataField' => 'btnEdit',
                    'toolTipField' => 'tooltip',
                    'headerText' => '',
                    'width' => 80,
                    'resizable' => true,
                    'sortable' => false
                )),
            $this->newColumnEditedBy()
        );


        $this->sReportSelectStatement = "
            SELECT SQL_CALC_FOUND_ROWS
                p.*,
                aa.username,
                aa.row_limit,
                ap.id as profile_id,
                GROUP_CONCAT(ap.name ORDER BY ap.name SEPARATOR '@@@') as name,
                aap.id_account,
                CONCAT_WS(' ',pc.fname,pc.mname,pc.lname) as created_user,
                CONCAT_WS(' ',pu.fname,pu.mname,pu.lname) as updated_user
            FROM personnel p
            LEFT JOIN personnel pc ON pc.id = p.created_user
            LEFT JOIN personnel pu ON pu.id = p.updated_user
            LEFT JOIN access_account aa ON aa.id_person = p.id
            JOIN access_account_profiles aap ON aap.id_account = aa.id
            LEFT JOIN access_profiles ap ON ap.id = aap.id_profile AND ap.to_arc = 0
        ";

        $this->aReportWhereStatement[] = 'p.to_arc = 0';
        $this->sReportGroupStatement = 'p.id';

        $this->aDefaultSort = array(
            array('field' => 'created_time', 'dir' => 'ASC')
        );

        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
    }

    public function loadData($aRequest) {
        return parent::loadData($aRequest);
    }

    public function suggestPerson($aRequest) {
        $aResult = array();
        $aResult['select'] = "SELECT id, fname as value, fname as html_value FROM personnel t ";
        $aResult['where'][] = "t.fname like '" . $aRequest['query'] . "%'";
        $aResult['group'] = 'html_value';

        return Suggest::select($aResult);
    }
}