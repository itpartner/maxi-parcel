<?php
namespace main\validator;
use main\InvalidParameter;

class Validate {

    public static function checkMySQLDateTimeToTimeStamp($sDateTime,$sFieldName = null, $sErrorMessage = null) {
        if(empty($sErrorMessage)) $sErrorMessage = L('Невалидна дата');
        $nDate = strtotime($sDateTime);
        if(empty($nDate)) throw new InvalidParameter($sErrorMessage, $sFieldName);
        return $nDate;
    }
    public static function checkMySQLDateTime($sDateTime,$sFieldName = null, $sErrorMessage = null) {
        return date("Y-m-d H:i:s",self::checkMySQLDateTimeToTimeStamp($sDateTime, $sFieldName, $sErrorMessage));
    }

}