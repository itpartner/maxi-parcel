<?php
namespace main;

use main\rpc\RPCHandler;

class Access implements RPCHandler {

    public static function isLoggedIn() {
        return (!empty($_SESSION['userdata']) && !empty($_SESSION['userdata']['id']));
    }

    public static function hasMenuAccess($aRow) {
        if($aRow['target_type'] == 'none') return true;
        return self::hasAccess($aRow['target']);
    }

    public static function hasAccess($sName) {
        try {
            return self::checkAccess($sName);
        } catch (AccessDenied $e) {
            return false;
        } catch (EmptySession $e) {
            return false;
        }
    }

    public static function isSuperAdmin() {
        return self::hasAccess('/');
    }

    public static function checkAccess($sName) {
        if ($sName == "pallex/client/Calculator" || $sName == "\\pallex\\client\\Calculator") return true;

        if(empty($_SESSION['userdata']['id'])) throw new EmptySession;
        if($_SESSION['userdata']['access_levels']['\\']) return true;
        $target = parseNS($sName);
        $tree = explode('\\',$target['class']);
        while($l=implode('\\',$tree)) {
            if(!empty($_SESSION['userdata']['access_levels'][$l])) return true;
            array_pop($tree);
        }

        $accessLevelDescription = Util::getClassAnnotations($sName)['@accessLevelDescription'];
        if(!empty($accessLevelDescription)) {
            throw new AccessDenied(L("Нямате право: \"%s\" ", $accessLevelDescription));
        } else {
            throw new AccessDenied();
        }
    }

    public static function hasAccessToSomeOfTheLevels($levels) {
        $hasAccess = false;
        foreach ($levels as $level) {
            if($hasAccess) break;
            $hasAccess = $hasAccess || self::hasAccess($level);
        }
        return $hasAccess;
    }


    public static function hasAccessToNoneOfTheLevels($levels) {
        $hasNoAccess = true;
        foreach ($levels as $level) {
            if(!$hasNoAccess) break;
            $hasNoAccess = $hasNoAccess && !self::hasAccess($level);
        }
        return $hasNoAccess;
    }

}
