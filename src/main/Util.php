<?php
namespace main;

use DateTime;
use ReflectionClass;

class Util {
    public static $monthNames = array(
        1 => 'Януари',
        2 => 'Февруари',
        3 => 'Март',
        4 => 'Април',
        5 => 'Май',
        6 => 'Юни',
        7 => 'Юли',
        8 => 'Август',
        9 => 'Септември',
        10 => 'Октомври',
        11 => 'Ноември',
        12 => 'Декември',
    );

    public static function SQLDateToTimeStamp($sqlDate) {
        if (is_numeric($sqlDate)) return $sqlDate;

        if (!empty($sqlDate) && !in_array($sqlDate, array('0000-00-00 00:00:00', '0000-00-00'))) {
            if (($nRes = strtotime($sqlDate)) != -1)
                return $nRes;
        }

        return 0;
    }

    public static function arrayAssocify(&$data, $depth) {
        if(empty($data) || $depth <= 0) return $data;
        $firstKey = @reset(array_keys(reset($data)));
        if(empty($firstKey)) return $data;
        $result = array();
        foreach($data as $row) {
            $sKey = $row[$firstKey];
            unset($row[$firstKey]);
            if($depth > 1) {
                if(!array_key_exists($sKey,$result)) $result[$sKey] = array();
                $result[$sKey][] = $row;
            } else {
                $result[$sKey] = $row;
            }
        }
        if($depth > 1) foreach($result as $k => $row) {
            $result[$k] = self::arrayAssocify($row,$depth - 1);
        }
        return $result;
    }

	public static function rksort(array &$array, $sort_flags = null) {
		foreach ($array as $nKey => $aArray) if (is_array($aArray)) self::rksort($array[$nKey], $sort_flags);
		return ksort($array, $sort_flags);
	}

    public static function stripslashes($data) {
        return is_array($data) ? array_map(__METHOD__, $data) : stripslashes($data);
    }

    public static function formatTime($time,$mode = 'smart') {
        if(!is_numeric($time)) $time = strtotime($time);
        if($mode == 'longdate') {
            $m = Util::$monthNames[date('n',$time)];
            return date('j',$time).' '.$m.' '.date('Y',$time);
        }
        if($mode == 'remaining') {
            $diff = (new DateTime())->diff(new DateTime(is_numeric($time) ? '@'.$time : $time), true);
            if($diff->days) return $diff->format("%aд %hч %iм");
            if($diff->h) return $diff->format("%hч %iм");
            return $diff->format("%iм %sс");
        }
        if($mode == 'long') {
            $m = Util::$monthNames[date('n',$time)];
            return date('j',$time).' '.$m.' '.date('Y H:i:s',$time);
        }
        if($mode == 'short') {
            return date("d.m.Y H:i",$time);
        }
        if(date('Y-m-d',$time) == date('Y-m-d')) {
            if($time < time() && time() - $time < 1800) {
                return (new DateTime())->diff(new DateTime(is_numeric($time) ? '@'.$time : $time), true)->format("преди %i минути");
            } else {
                return date("H:i", $time);
            }
        }

        return date("d.m.Y H:i",$time);
    }

    public static function getTempDir() {
        $dir = sys_get_temp_dir();
        if(substr($dir,-1,1) != '\\' && substr($dir,-1,1) != '/') {
            $dir.='/';
        }
        return $dir;
    }

    public static function htmlEscape($txt) {
        return htmlentities($txt,null,'UTF-8');
    }

    public static function checkIP($sIP, $aAllowedIPs) {
        $nIP = ip2long($sIP);
        if (is_array($aAllowedIPs) && $nIP) foreach ($aAllowedIPs as $sAllowedIP) {
            list ($sAllowedIP, $sAllowedMask) = explode('/', $sAllowedIP);
            $nAllowedIP = ip2long($sAllowedIP);
            if (is_numeric($sAllowedMask) && intval($sAllowedMask) > 0 && intval($sAllowedMask) <= 32) {
                $nAllowedMask = 0xFFFFFFFF << (32 - intval($sAllowedMask));
            } else if (false === ($nAllowedMask = ip2long($sAllowedMask))) {
                $nAllowedMask = 0xFFFFFFFF;
            }
            if (($nIP & $nAllowedMask) == ($nAllowedIP & $nAllowedMask)) return true;
        }
        return false;
    }

    public static function currentURL($params = array(),$fragment = null) {
        $url = 'http';
        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') $url .= 's';
        $url .= '://';
        if ($_SERVER['SERVER_PORT'] != '80' && $_SERVER['SERVER_PORT'] != '443') $url .= $_SERVER['HTTP_HOST'] . ':' . $_SERVER['SERVER_PORT'];
        else $url .= $_SERVER['HTTP_HOST'];

        list($path,$getString) = explode('?',$_SERVER['REQUEST_URI']);
        if($getString) parse_str($getString,$get);
        else $get = array();
        foreach($params as $k => $v) $get[$k] = $v;
        $url .= $path;
        if(!empty($get)) $url .= '?'.http_build_query($get,null,'&');
        if($fragment !== null) $url.= '#'.$fragment;
        return $url;
    }

    public static function compareIPs($sFirstIP, $sSecondIP, $sNetMask = '255.255.255.255')
    {
        $nFirstIP = ip2long($sFirstIP);
        if ($nFirstIP === false) return false;
        $nSecondIP = ip2long($sSecondIP);
        if ($nSecondIP === false) return false;
        $nNetMask = ip2long($sNetMask);
        if ($nNetMask === false) return false;
        return ($nFirstIP & $nNetMask) == ($nSecondIP & $nNetMask);
    }

    public static function getCPUTime() {
        if(!function_exists('getrusage')) return null;
        $r = getrusage();
        return $r['ru_utime.tv_sec'] + $r['ru_utime.tv_usec'] / 1000000 + $r['ru_stime.tv_sec'] + $r['ru_stime.tv_usec'] / 1000000;
    }

    public static function isCli() {
        return ((php_sapi_name() == 'cli' && empty($argv[0])) ? true : false);
    }
    private static /** @noinspection PhpUnusedPrivateFieldInspection */ $stdOut;
    private static /** @noinspection PhpUnusedPrivateFieldInspection */ $stdErr;
    public static function writeCli($string, $error = false) {
        $std = (!$error ? 'stdOut' : 'stdErr');
        if (!self::${$std}) self::${$std} = fopen(sprintf('php://%s', strtolower($std)), 'w');

        return fwrite(self::${$std}, $string);
    }
    private static $stdIn;
    public static function readCli() {
        if (!self::$stdIn) self::$stdIn = fopen('php://stdin', 'r');

        return trim(fgets(self::$stdIn));
    }

    public static function getClassAnnotations($class) {
        try {
            $r = new ReflectionClass($class);
            $doc = $r->getDocComment();
            preg_match_all('#@(.*?)\n#s', $doc, $annotations);
            $result = [];
            if(empty($annotations)) return $result;
            foreach($annotations[0] as $annotation) {
                $e = explode(' ',trim($annotation),2);
                $result[$e[0]] = $e[1];
            }
            return $result;
        } catch (\Exception $e) {
            return;
        }

    }
} 