<?php
namespace main\grid;
use main\Access;
use main\Exception;
use main\InvalidParameter;

class Export {

    private static $aGridExportClasses = array(
        'Excel5'		=> 'main\grid\GridExcelExport',
        'ExcelOpenXML'	=> 'main\grid\GridExcelExport',
        'CSV'			=> 'main\grid\GridCSVExport',
        'PDF'			=> 'main\grid\GridPdfExport',
        'vcard'			=> 'main\grid\GridVCardExport',
    );

    public $aRequest = array();
    public $aState = array();
    public $sExportType;

    public function __construct() {
        $this->sExportType = $_REQUEST['exportType'];
        $this->aRequest = json_decode($_REQUEST['request'],true);
    }

    public function doExport() {
        $target = parseNS($_REQUEST['exportMethod']);
        if(is_callable(array($target['class'],$target['method']))) {
            $o = new $target['class']();
            return $o->{$target['method']}($_REQUEST, $this);
        }

        if(!is_callable(array($this,$_REQUEST['exportMethod']))) {
            throw new InvalidParameter(L('Няма такъв метод.'));
        }
        $this->{$_REQUEST['exportMethod']}();
    }

    public function exportGridData() {
        if(empty($this->aRequest)) throw new InvalidParameter;
        $sName = $this->aRequest['serverClassName'];
        Access::checkAccess($sName);
        //check api
        if(empty($this->aRequest['serverClassName']) || !class_exists($this->aRequest['serverClassName'],true)) throw new Exception(L('Клас %s не е намерен.',$this->aRequest['serverClassName']));
        $oTarget = new $this->aRequest['serverClassName'];
        if(!($oTarget instanceof GridPanelHandler)) throw new Exception(L('Заявки към клас %s не са позволени.',$this->aRequest['serverClassName']));

        if(!empty(self::$aGridExportClasses[$this->sExportType]) && class_exists(self::$aGridExportClasses[$this->sExportType])) {
            $sClassName = self::$aGridExportClasses[$this->sExportType];
            /** @var GridExport $oGridExport */
            $oGridExport = new $sClassName($this, $oTarget);
            $oGridExport->doExport();
        } else {
            throw new Exception('Невалиден експорт на данни');
        }
    }
}