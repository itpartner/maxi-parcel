<?php
namespace main\grid;

use main\Exception;
use main\form\FormPanelHandler;
use main\InvalidParameter;

class GridFilterWindow extends FormPanelHandler {
    /** @var DBGridFilters */
    public $oDBGridFilters;

    public function __construct() {
        $this->oDBGridFilters = new DBGridFilters();
        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        return !empty($aInitParams['id']) ? $this->load(array('fields' => array('id'=>$aInitParams['id']))) : null;
    }

    public function load($aRequest) {
        $aFilter = $this->oDBGridFilters->getRecord($aRequest['fields']['id']);
        if(empty($aFilter)) throw new Exception(L('Филтъра не е намерен.'));
        $this->setFieldValue('filter_name', $aFilter['filter_name']);
        $this->oResponse->filterItems = unserialize($aFilter['filter_items']);
        return $this->oResponse;
    }

    public function save($aRequest) {
        if(empty($aRequest) || empty($aRequest['fields']) || empty($aRequest['gridName'])) throw new InvalidParameter();
        if(empty($aRequest['fields']['filter_name'])) throw new InvalidParameter('Моля въведете име на филтъра.');
        $aFilter = array(
            'id' => empty($aRequest['fields']['id']) ? 0 : intval($aRequest['fields']['id']),
            'id_user' => $_SESSION['userdata']['id'],
            'filter_name' => $aRequest['fields']['filter_name'],
            'grid_name' => $aRequest['gridName'],
            'filter_items' => serialize($aRequest['filterItems'])
        );

        $sHandlerClass = $aRequest['gridName'];
        if(!class_exists($sHandlerClass, true) || !in_array('main\\grid\\GridPanelHandler', class_parents($sHandlerClass))) throw new InvalidParameter;
        /** @var GridPanelHandler $oHandler */
        $oHandler = new $sHandlerClass;

        $aColumnsAssoc = $oHandler->getColumnsAssoc();
        foreach ($aRequest['filterItems'] as $aFilterItem) GridFilterCreateSQL::create($aFilterItem, $aColumnsAssoc[$aFilterItem['dataField']]);

        $this->oDBGridFilters->update($aFilter);
        $this->setFieldValue('id', $aFilter['id']);

        $this->oResponse->gridFilters = DBGridFilters::getFiltersForCurrentUser($aRequest['gridName']);

        return $this->oResponse;
    }

    public function del($nIDFilter) {
        $aFilter = $this->oDBGridFilters->getRecord($nIDFilter);
        if(empty($aFilter)) throw new Exception(L('Филтъра не е намерен.'));
        if($aFilter['id_user'] != $_SESSION['userdata']['id']) throw new Exception(L('Филтъра принадлежи на друг потребител.'));

        $this->oDBGridFilters->delete($aFilter['id']);
        return DBGridFilters::getFiltersForCurrentUser($aFilter['grid_name']);
    }
}
