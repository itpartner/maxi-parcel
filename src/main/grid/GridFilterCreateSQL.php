<?php
namespace main\grid;
use main\db\DBC;
use main\InvalidParameter;
use main\language\Transliteration;
use main\validator\Validate;


class GridFilterCreateSQL {
	public static function create($aItem, $oColumn) {
		if(!is_callable(__CLASS__,$aItem['type'])) throw new InvalidParameter(L('Невалиден тип поле във филтъра.'));
		if(!preg_match("/^[a-z\._\d]{1,}$/i", $aItem['dataField'])) throw new InvalidParameter(L('Невалидно поле %s във филтъра.',$aItem['dataField']));
		return self::{$aItem['type']}($aItem, $oColumn);
	}

    public static function string($aItem) {
        $sEscapedVal        = DBC::$main->escape($aItem['compareValue']);
        $sEscapedCyrilicVal = DBC::$main->escape(Transliteration::LatinToCyr($aItem['compareValue']));
        $sEscapedLatinVal   = DBC::$main->escape(Transliteration::convertCyr2PhoLower($aItem['compareValue']));
        switch ($aItem['compareType']) {
            case 'eq' :
                return $sEscapedVal == '' ? "({$aItem['dataField']} = '' OR {$aItem['dataField']} IS NULL )" : " ({$aItem['dataField']}  LIKE '$sEscapedVal' OR {$aItem['dataField']} LIKE '$sEscapedCyrilicVal' OR {$aItem['dataField']} LIKE '$sEscapedLatinVal') ";
                break;
            case 'bg' :
                return " ({$aItem['dataField']}  LIKE '$sEscapedVal%' OR {$aItem['dataField']} LIKE '$sEscapedCyrilicVal%' OR {$aItem['dataField']} LIKE '$sEscapedLatinVal%') ";
                break;
            case 'ct' :
                return " ({$aItem['dataField']}  LIKE '%$sEscapedVal%'  OR {$aItem['dataField']} LIKE '%$sEscapedCyrilicVal%' OR {$aItem['dataField']} LIKE '%$sEscapedLatinVal%') ";
                break;
            case 'ne' :
                return $sEscapedVal == '' ? " ({$aItem['dataField']} != '' AND {$aItem['dataField']} IS NOT NULL )" : "(( {$aItem['dataField']} NOT LIKE '$sEscapedVal' AND {$aItem['dataField']} NOT LIKE '$sEscapedCyrilicVal' AND {$aItem['dataField']} NOT LIKE '$sEscapedLatinVal') OR {$aItem['dataField']} IS NULL)";
                break;
            case 'nbg' :
                return "(( {$aItem['dataField']} NOT LIKE '$sEscapedVal%' AND {$aItem['dataField']} NOT LIKE '$sEscapedCyrilicVal%' AND {$aItem['dataField']} NOT LIKE '$sEscapedLatinVal%') OR {$aItem['dataField']} IS NULL )";
                break;
            case 'nct' :
                return "(( {$aItem['dataField']} NOT LIKE '%$sEscapedVal%' AND {$aItem['dataField']} NOT LIKE '%$sEscapedCyrilicVal%' AND {$aItem['dataField']} NOT LIKE '%$sEscapedLatinVal%') OR {$aItem['dataField']} IS NULL )";
                break;
            default: throw new InvalidParameter(L('Невалиден тип сравнение във филтъра.'));
        }
    }

	public static function integer($aItem) {
		return self::__number($aItem);
	}
	public static function float($aItem) {
		return self::__number($aItem);
	}
	protected static function __number($aItem) {
		$aItem['compareValue'] = trim($aItem['compareValue']);
		if($aItem['compareValue'] === '') {
			if($aItem['compareType'] == 'eq') return " {$aItem['dataField']}  IS NULL ";
			if($aItem['compareType'] == 'ne') return " {$aItem['dataField']}  IS NOT NULL ";
			$aItem['compareValue'] = '0';
		}
		if(!is_numeric($aItem['compareValue'])) throw new InvalidParameter(L('Невалидна стойност за сравнение във филтъра.'));
		switch ($aItem['compareType']) {
			case 'eq' :
				return " {$aItem['dataField']}  = {$aItem['compareValue']} ";
				break;
			case 'ne' :
				return "( {$aItem['dataField']}  != {$aItem['compareValue']} OR {$aItem['dataField']} IS NULL)";
				break;
			case 'gt' :
				return " {$aItem['dataField']}  > {$aItem['compareValue']} ";
				break;
			case 'gte' :
				return " {$aItem['dataField']}  >= {$aItem['compareValue']} ";
				break;
			case 'lt' :
				return " {$aItem['dataField']}  < {$aItem['compareValue']} ";
				break;
			case 'lte' :
				return " {$aItem['dataField']}  <= {$aItem['compareValue']} ";
				break;
			default: throw new InvalidParameter(L('Невалиден тип сравнение във филтъра.'));
		}
	}
	
	public static function date($aItem) {
        $sDate1 = $sDate2 = '';
		if($aItem['compareType'] != 'dl' && $aItem['compareType'] != 'today') $sDate1 = date('Y-m-d', Validate::checkMySQLDateTimeToTimeStamp($aItem['compareValue']['date1']));
		switch ($aItem['compareType']) {
			case 'deq' :
				return " {$aItem['dataField']}  BETWEEN '$sDate1' AND '$sDate1 23:59:59' ";
				break;
			case 'dgt' :
				return " {$aItem['dataField']}  > '$sDate1' ";
				break;
			case 'dlt' :
				return " {$aItem['dataField']}  < '$sDate1' ";
				break;
			case 'dbt' :
				$sDate2 = substr(Validate::checkMySQLDateTime($aItem['compareValue']['date2']),0,10);
				return " {$aItem['dataField']}  BETWEEN '$sDate1' AND '$sDate2 23:59:59' ";
				break;
			case 'dl' :
				if($aItem['compareValue']['interval_type'] == 'h') $sType = 'hours';
				elseif($aItem['compareValue']['interval_type'] == 'w') $sType = 'weeks';
				elseif($aItem['compareValue']['interval_type'] == 'm') $sType = 'months';
				else $sType = 'days';
				$nInterval = abs(intval($aItem['compareValue']['interval']));
				$sDate = date('Y-m-d H:i:s', strtotime("-$nInterval $sType"));
				return " {$aItem['dataField']} >= '$sDate'";
				break;
			case 'today':
				return " {$aItem['dataField']}  BETWEEN '".date('Y-m-d')."' AND '".date('Y-m-d 23:59:59')."' ";
				break;
			default: throw new InvalidParameter(L('Невалиден тип сравнение във филтъра.'));
		}
	}
	public static function enum($aItem, $oColumn) {
		$sEscapedVal = DBC::$main->escape($aItem['compareValue']);
		if(!isset($aItem['compareValue'])) throw new InvalidParameter(L('Невалидна стойност за сравнение във филтъра.'));
		switch ($aItem['compareType']) {
			case 'eq' :
				return " {$aItem['dataField']}  = '$sEscapedVal' ";
				break;
			case 'ne' :
				return "( {$aItem['dataField']}  != '$sEscapedVal' OR {$aItem['dataField']} IS NULL)";
				break;
			case 'ct' :
				$aValuesQuoted = array();
				if(is_array($oColumn->items)) foreach($oColumn->items as $val => $aEnumItem) {
					if(strpos(mb_strtolower($aEnumItem['text'], 'UTF-8'), mb_strtolower($aItem['compareValue'], 'UTF-8')) !== false) {
						$aValuesQuoted[] = DBC::$main->quote($val);
					}
				}
				if(!empty($aValuesQuoted)) return sprintf(" {$aItem['dataField']} IN (%s) ",implode(',',$aValuesQuoted));
				else return ' 0 ';
				break;
			default: throw new InvalidParameter(L('Невалиден тип сравнение във филтъра.'));
		}
	}

}
