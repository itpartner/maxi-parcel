<?php
namespace main\grid;
use main\db\DBC;
use main\Exception;

class DBGridFilters {
    public static function getFilterByID($nIDFilter) {
        if(!intval($nIDFilter)) throw new Exception;
        $aFilter = DBC::$main->selectRow("
			SELECT
				id,
				id_user,
				grid_name,
				filter_name,
				filter_items
			FROM users_grid_filters
			WHERE 
				id = ".intval($nIDFilter)
        );
        if(empty($aFilter)) throw new Exception(L('Филтъра не е намерен.'));
        $aFilter['filter_items'] = unserialize($aFilter['filter_items']);
        return $aFilter;
    }

    public static function getFiltersForCurrentUser($sGridName) {
        return DBC::$main->select("
			SELECT
				id,
				filter_name
			FROM users_grid_filters
			WHERE 1
            AND id_user = ".intval($_SESSION['userdata']['id'])."
            AND grid_name = ".DBC::$main->Quote($sGridName)."
        ");
    }

    public function update(&$aData) {
        DBC::$main->update('users_grid_filters', $aData);
    }
    public function delete($nID) {
        DBC::$main->delete('users_grid_filters',$nID);
    }
    public function getRecord($id) {
        return DBC::$main->selectRow("SELECT * FROM users_grid_filters WHERE id = {$id}");
    }


}