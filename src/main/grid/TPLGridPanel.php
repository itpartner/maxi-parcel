<?php
namespace main\grid;
use main\form\FormStates;
use main\TPLBase;

class TPLGridPanel extends TPLBase {

	public function __construct($sPrefix, $aInitParams = array()) {
		parent::__construct($sPrefix, $aInitParams);
		$aFrameConfig = array(
			'vResizable' => true
		);
		if(class_exists($this->sServerClassName)) {
			$this->{'oGridPanelHandler'} = new $this->sServerClassName;
			$sTitle = $this->{'oGridPanelHandler'}->getReportTitle();
			if(!empty($sTitle)) $this->{'aConfig'}['title'] = $sTitle;
		}
		$this->aFrameConfig = array_merge($aFrameConfig,$this->aFrameConfig);
	}
	protected function printStyles ($sPrefix) {}
	protected function printBeforeGrid($sPrefix) {}
	public function printHtml() {
		$this->printBeforeGrid($this->sPrefix);
		$this->printResultTable(empty($this->oGridPanelHandler) ? $this->sServerClassName : $this->oGridPanelHandler, $this->{'aConfig'});
	}
}