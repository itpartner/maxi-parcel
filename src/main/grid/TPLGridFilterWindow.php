<?php
namespace main\grid;
use main\TPLBase;

class TPLGridFilterWindow extends TPLBase {

    public function printHTML () {
        $this->aInitParams['id'] = empty($this->aInitParams['id']) ? 0 : $this->aInitParams['id'];
        ?>
        <input type="hidden" name="id" value="<?=$this->aInitParams['id']?>" />
        <div class="filter-toolbar toolbar flat-buttons"><?=$this->createToolBarHTML(array(
                array('label',array('text' => 'Име на филтъра:', 'fieldName' => 'filter_name')),
                array('input',array('name' => 'filter_name', 'type' => 'text')),
                array('label',array('text' => 'Добави Колона:', 'fieldName' => 'columns')),
                array('select',array('name' => 'columns', 'labelField' => 'headerText', 'valueField' => 'dataField', 'groupField' => 'groupName')),
                array('button',array('class' => 'add-btn', 'iconCls' => 'icon fa fa-plus'))
            ));?></div>
        <div class="filter-items-container flat-buttons">
            <table class="filter-items"></table>
        </div>
        <div style="margin-top:5px" class="toolbar"><?=$this->createToolBarHTML(array(
                array('button',array('text' => 'Приложи', 'class' => 'apply','align' => 'right','iconCls'=>'icon fa fa-refresh')),
                array('closeButton'),
                array('confirmButton')

            ));?></div>
        <?php
    }

    public function getJSFunction() { return <<<'JS'
            (function(container,prefix,initParams) {
                var jContainer = jQuery(container);
                var form = container;
                var parentGrid = initParams.getGrid();
                form.baseParams.gridName = initParams.gridName;
                var jColumnsSelect = jContainer.find('.filter-toolbar select[name=columns]');
                var jFilterItems = jContainer.find('.filter-items');
                var filterableColumns = [];
                var columnsByName = {};
                parentGrid._openFilterWindow = container.window;
                container.window.addListener('close', function(){
                    parentGrid._openFilterWindow = false;
                });
                var createColumnsArray = function(columns, depth) {
                    depth = depth || 0;
                    var groupNames = [];
                    if (columns[0]) {
                        var col = columns[0];
                        while (col.parentColumn) {
                            groupNames.unshift(col.parentColumn.columnListText || col.parentColumn.headerText);
                            col = col.parentColumn;
                        }
                    }
                    for(var i=0; i < columns.length; i++) {
                        if(columns[i].children && columns[i].children.length) {
                            createColumnsArray(columns[i].children, depth + 1);
                        } else {
                            columnsByName[columns[i].dataField] = columns[i];
                            if (columns[i].filterable && columns[i].type && framework.grid.filterRowRenderers[columns[i].type]) {
                                filterableColumns.push({
                                    dataField:columns[i].dataField,
                                    headerText:columns[i].columnListText || columns[i].headerText,
                                    groupName:groupNames.join(' > ')
                                });
                            }
                        }
                    }
                };
                createColumnsArray(parentGrid.columns);
                var addDeleteButtonToRow = function(row) {
                    if(jQuery(row).find('td.delete-item').length == 0) {
                        var deleteBtnCell = document.createElement('td');
                        deleteBtnCell.className = 'delete-cell';
                        deleteBtnCell.innerHTML = '<button class=\"delete-item\"><div class="icon fa fa-close"></div></button>';
                        row.appendChild(deleteBtnCell);
                    }
                    return row;
                };
                jColumnsSelect[0].setContent(filterableColumns);
                jColumnsSelect.find('option:first').attr('selected',true);

                form.addListener('beforeRequest',function(params){
                    params.filterItems = [];
                    jFilterItems.find('tr.filter-item').each(function(i,row){
                        params.filterItems.push(row.getItemValue());
                    });
                });
                form.addListener('afterRequest',function(response) {
                    if(!response) return false;
                    if(response.filterItems) {
                        jFilterItems.html('');
                        var tbody = document.createElement('tbody');
                        if(response.filterItems.length) for(var i=0; i < response.filterItems.length; i++) {
                            if (framework.grid.filterRowRenderers[response.filterItems[i].type]) {
                                var row = framework.grid.filterRowRenderers[response.filterItems[i].type](parentGrid, columnsByName[response.filterItems[i].dataField], response.filterItems[i]);
                                tbody.appendChild(addDeleteButtonToRow(row));
                            }
                        }
                        jFilterItems.append(tbody);
                    }
                });
                jFilterItems.click(function(event) {
                    var jTarget = jQuery(event.target);
                    if(jTarget.is('button.delete-item')) {
                        jTarget.parent().parent().remove();
                    } else if(jTarget.parent().is('button.delete-item')) {
                        jTarget.parent().parent().parent().remove();
                    }
                });
                jContainer.find('.filter-toolbar .add-btn').click(function(event){
                    var selected = jColumnsSelect[0].getValue();
                    if(!selected || !columnsByName[selected]) return;
                    var row = framework.grid.filterRowRenderers[columnsByName[selected].type].apply(jFilterItems, [parentGrid, columnsByName[selected]]);
                    var jTbody = jFilterItems.find('tbody');
                    if(!jTbody.length) {
                        jTbody = jQuery(document.createElement('tbody'));
                        jFilterItems.append(jTbody);
                    }
                    jFilterItems.find('tbody').append(addDeleteButtonToRow(row));
                });
                jContainer.find('button.confirm').click(function(event){
                    form.request('save',null,null,function(response){
                        parentGrid.setFilters(response.gridFilters);
                        parentGrid.setSelectedFilter(response.fields.id.value,true);
                        container.window.close();
                    });
                });
                jContainer.find('button.apply').click(function(event){
                    form.request('save',null,null,function(response){
                        parentGrid.setFilters(response.gridFilters);
                        parentGrid.setSelectedFilter(response.fields.id.value,true);
                    });
                });
                jContainer.find('button.close').click(function(){jQuery(container.parentNode).dialog('close');});
            });
JS;
    }


}
