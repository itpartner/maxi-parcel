<?php
namespace main\grid;
use main\Exception;
use main\form\ResponseColumn;
use main\language\Language;
use main\Util;

class GridCSVExport extends GridExport {
	public static function arrayToCSV($aData, $sDelimiter = "\t", $sEnclosure = '"') {
		
		$outstream = @fopen('php://temp/maxmemory:'. (64*1024*1024), 'r+');
		if(!$outstream) throw new Exception(L('Грешка при създаване на файла.'));
		if(!is_array($aData)) $aData = array();
		foreach ($aData as $aRow) {
			fputcsv($outstream, $aRow, $sDelimiter, $sEnclosure);
		}
		rewind($outstream);
		$output = stream_get_contents($outstream);
		fclose($outstream);
		return $output;
	}
	
	protected function createHeaders($aColumns = array()) {
		$aHeader = array();
		foreach ($aColumns as $oColumn) $aHeader[] = $oColumn->headerText;
		return array($aHeader);
	}
	
	protected function createDataRows($aData, $aColumns) {
		$aCSVRows = array();
		$i=0;
		foreach ($aData as $aDataRow) {
			$aCSVRow = array();
			foreach ($aColumns as $oColumn) {
				$aCSVRow[] = $this->createCell($aDataRow, $oColumn, $i);
			}
			$aCSVRows[] = $aCSVRow;
			$i++;
		}
		return $aCSVRows;
	}
	
	public function doExport() {
		$this->aColumns = $this->oTarget->getColumnsAssoc($this->aColumns);
		$aCSVRows = $this->createHeaders($this->aColumns);
		$aCSVRows = array_merge($aCSVRows, $this->createDataRows($this->getData(),$this->aColumns));
		
		header('Expires: 0');
		header('Cache-control: private');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header("Content-Type: text/csv");
		header('Content-Disposition: attachment; filename="'.$this->sFileName.'.csv"');
		
		echo "\xFF\xFE".mb_convert_encoding(self::arrayToCSV($aCSVRows), 'UCS-2LE', 'UTF-8');
		die();
	}
	
	/**
	 * @param array $aRow
	 * @param ResponseColumn $oColumn
	 */
	protected function createCell($aRow, $oColumn, $nIndex) {
		switch ($oColumn->type) {
			case 'rowIndex':
				$sCell = (string)($nIndex + 1);
				break;
//			case GridPanelHandler::CTYPE_CURRENCY:
//				$sCell = DBCurrency::getFormattedPrice($aRow[$oColumn->dataField], $aRow[$oColumn->currencyField]);
//				break;
			case GridPanelHandler::CTYPE_DATE:
			case GridPanelHandler::CTYPE_DATETIME:
			case GridPanelHandler::CTYPE_TIME:
				$nTime = Util::SQLDateToTimeStamp($aRow[$oColumn->dataField]);
				if(empty($nTime)) $sCell = '';
				else {
					if($oColumn->type == GridPanelHandler::CTYPE_DATE ) $sFormat = Language::getPHPDateFormat();
					elseif($oColumn->type == GridPanelHandler::CTYPE_TIME) $sFormat = Language::getPHPTimeFormat();
					else $sFormat = Language::getPHPDateTimeFormat();
					$sCell = date($sFormat, $nTime);
				}
				break;
			case GridPanelHandler::CTYPE_ENUM:
				$sCell = (string) $oColumn->items[$aRow[$oColumn->dataField]]['text'];
				break;
			case GridPanelHandler::CTYPE_EDITEDBY:
				$sCell = (string) $this->createEditedByText($aRow, $oColumn);
				break;
			default:
				$sCell = $aRow[$oColumn->dataField];
		}
		return (string)$sCell;
	}
}