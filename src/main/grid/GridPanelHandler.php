<?php
namespace main\grid;
use main\db\DBC;
use main\db\DBConnection;
use main\Exception;
use main\form\FormPanelHandler;
use main\form\ResponseColumn;
use main\InvalidParameter;
use main\language\Language;
use main\settings;
use main\System;
use main\Util;

class GridPanelHandler extends FormPanelHandler  {
	const CTYPE_STRING		= 'string';
	const CTYPE_INT			= 'integer';
	const CTYPE_FLOAT		= 'float';
	const CTYPE_IMG			= 'image';
	const CTYPE_BTN			= 'button';
	const CTYPE_CURRENCY	= 'currency';
	const CTYPE_DATE		= 'date';
	const CTYPE_DATETIME	= 'dateTime';
	const CTYPE_TIME		= 'time';
	const CTYPE_EDITEDBY	= 'editedBy';
	const CTYPE_ENUM		= 'enum';
	const CTYPE_HTML		= 'html';
	const CTYPE_ICONS		= 'icons';

	private static $aNumericColumnTypes = array('integer', 'float', 'currency' , 'office');

	public $aColumns = array();

	/**
	 * @var DBConnection
	 */
	public $oBase = null;

	/**
	 * Дали да се използва SQL sort
	 *
	 * @var boolean
	 */
	protected $bUseSQLSort = true;

	/**
	 * Дали да се използва LIMIT за paging-a
	 *
	 * @var boolean
	 */
	protected $bUseSQLPaging = true;
	
	/**
	 * Дали да се използва WHERE и HAVING за филтър
	 *
	 * @var boolean
	 */
	protected $bUseSQLFilters = true;

	public $aDefaultSort = array();

	protected $sDateFromField 	= 'dateFrom';
	protected $sDateToField 	= 'dateTo';
	protected $sPeriodField 	= 'time_period';

    /**
     * @var ResponseColumn[]
     */
	protected $aColumnsAssoc = null;
	protected $sReportTitle = '';

    private $aSortFields = null;

    public $sReportSelectStatement 	= NULL;
    public $aReportWhereStatement 	= array();
    public $aReportHavingStatement 	= array();
    public $sReportGroupStatement 	= NULL;

    /** @var array */
    public $aDefaultQuickFilters = array();


	public function __construct() {
		if(empty($this->aDefaultSort) && !empty($this->aColumns)) {
			$this->aDefaultSort = array(
				array('field' => $this->aColumns[0]->dataField,'dir' => 'ASC')
			);
		}
		parent::__construct();
		$this->aColumnsAssoc = $this->getColumnsAssoc();
	}

	public function getReportTitle() {
		return Language::getTranslationForText($this->sReportTitle, get_class($this));
	}

    /**
     * Задава $gridData на Response обекта
     *
     * @param array $aData - данните на справката
     * @param array $aRequest - заявката
     * @param int	$nTimeFrom - време от (за месечни таблици)
     * @param int	$nTimeTo - време до (за месечни таблици)
     * @return array
     */
    protected function setGridData($aData) {
        if(empty($aData)) $aData = array();
        $this->oResponse->gridData = array_merge($aData, array());
    }

    /**
     * Допълнителна обработка на данните
     *
     * @param array $aData - данните на справката
     * @param array $aRequest - заявката
     * @param int	$nTimeFrom - време от (за месечни таблици)
     * @param int	$nTimeTo - време до (за месечни таблици)
     * @return array - данните на справката
     */
    protected function processData($aData) {
        return $aData;
    }

	/**
	 * Задаване на тотали
	 *
	 * @param array $aData - данните на справката
	 * @return array
	 */
	protected function setGridTotals($aData, $aRequest = array(), $nTimeFrom = 0, $nTimeTo = 0) {}
	
	/**
	 * създава нова колона (ResponseColumn), атрибутите на колоната се описват в $aConfig
	 * полето 'headerText' се превежда автоматично, ако е празно се слага превода на 'dataField',
	 * пример: array('type' => 'number', 'dataField' => 'id', 'width'	=> 70)
	 *
	 * @param array $aConfig
	 */
	public function newColumn($aConfig) {
		$aConfig['width'] = empty($aConfig['width']) || intval($aConfig['width']) <= 0? 100 : intval($aConfig['width']);

		$aDataField = !empty($aConfig['dataField']) ? explode('.', $aConfig['dataField']) : array($aConfig['headerText']);
		$aConfig['DBFieldName'] = sizeof($aDataField) == 1 ? '' : $aConfig['dataField'];
		$aConfig['dataField'] = end($aDataField);

		return new ResponseColumn($aConfig);
	}

	public function newColumnEditedBy($aConfig = array()) {
		$aConfig['headerText'] = empty($aConfig['headerText']) ? ' ' : $aConfig['headerText'];
		$aConfig['type'] = 'editedBy';
		$aConfig['width'] = 40;
		$aConfig['resizable'] = false;
		$aConfig['sortable'] = false;
		$aConfig['dataField'] = 'updatedAndCreatedUserAndTime';
		$aConfig['createdUserField'] = empty($aConfig['createdUserField']) ? 'created_user' : $aConfig['createdUserField'];
		$aConfig['createdTimeField'] = empty($aConfig['createdTimeField']) ? 'created_time' : $aConfig['createdTimeField'];
		$aConfig['updatedUserField'] = empty($aConfig['updatedUserField']) ? 'updated_user' : $aConfig['updatedUserField'];
		$aConfig['updatedTimeField'] = empty($aConfig['updatedTimeField']) ? 'updated_time' : $aConfig['updatedTimeField'];

		return $this->newColumn($aConfig);
	}

	public function newColumnLang($aConfig = array()) {
		$aConfig['type'] = self::CTYPE_ENUM;
		$aConfig['items'] = array();
		if(empty($aConfig['style'])) $aConfig['style'] = array();
		$aConfig['style']['textAlign'] = 'center';
		$aLangs = Settings::$aSettings['SYSTEM']['supported_languages'][0];
		foreach($aLangs as $sCode => $sLang) {
			$aConfig['items'][$sCode] = array(
				'text' => Language::getTranslationForText($sLang, 'default'),
				'img' => 'flags/'.$sCode.'.png',
			);
		}
		return $this->newColumn($aConfig);
	}

	public function getHeaderRowCount($aColumns = null) {
		if(!$aColumns) $aColumns = $this->aColumns;
		$nMaxChildrenDepth = 0;
		foreach ($aColumns as $oColumn) {
			if(!empty($oColumn->children)) {
				$nChidlrenDepth = $this->getHeaderRowCount($oColumn->children);
				if($nChidlrenDepth > $nMaxChildrenDepth) $nMaxChildrenDepth = $nChidlrenDepth;
			}
		}
		return $nMaxChildrenDepth + 1;
	}

	public function getColumnsAssoc($aColumns = null) {
		if(!$aColumns) $aColumns = $this->aColumns;
		$aColumnsAssoc = array();
		foreach ($aColumns as $oColumn) {
			if(!empty($oColumn->children)) {
				$aColumnsAssoc = array_merge($aColumnsAssoc, $this->getColumnsAssoc($oColumn->children));
			} else {
				$aColumnsAssoc[$oColumn->dataField] = $oColumn;
			}
		}
		return $aColumnsAssoc;
	}

	protected function getGridData($aRequest, $nTimeFrom = 0, $nTimeTo = 0) {
        $sOrderBy = $sLimit = '';
        //sort
		if($this->bUseSQLSort) {
			$aOrderBy = array();
			foreach ($aRequest['sortFields'] as $aSortField) {
				$prefix = '';
				$field = explode('.',$aSortField['field']);
				if(count($field) > 1) $prefix = array_shift($field).'.';
				$field = reset($field);
				if(!preg_match("/^[a-z0-9_]+$/i", $field)) {
					throw new Exception;
				}
				$sDir = $aSortField['dir'] == 'ASC' ? 'ASC' : 'DESC';
				$aOrderBy[] = $prefix.'`'.$field.'` '.$sDir;
			}
			$sOrderBy = implode(', ',$aOrderBy);
			$this->oResponse->sortFields = $aRequest['sortFields'];
		}


		//paging
		if(isset($aRequest['paging']) && $this->bUseSQLPaging) {
			$nStart = intval($aRequest['paging']['start']);
			$nLimit = intval($aRequest['paging']['limit']);
			$sLimit = $nStart.', '.$nLimit;
		}

		//filters
		if($this->bUseSQLFilters)
		{
			$aFilterWhereStatements = array();
			$aFilterHavingStatements = array();
			//normal filter
			if(!empty($aRequest['filter']) && !empty($aRequest['filter']['id'])) {
				$aFilterStatemens = $this->createFilterStatements(DBGridFilters::getFilterByID($aRequest['filter']['id']));
				if(!empty($aFilterStatemens['where'])) {
					$aFilterWhereStatements = array_merge($aFilterStatemens['where'], $aFilterWhereStatements);
				}
				if(!empty($aFilterStatemens['having'])) {
					$aFilterHavingStatements = array_merge($aFilterStatemens['having'], $aFilterHavingStatements);
				}
			}
	
			//quick filter
			if(!empty($aRequest['filterItems']) && is_array($aRequest['filterItems'])) {
				if(empty($this->aColumnsAssoc)) $this->aColumnsAssoc = $this->getColumnsAssoc();
				foreach ($aRequest['filterItems'] as $aFilterItem) {
					if(empty($aFilterItem)) continue;
					$aStatement = $this->createFilterItemStatement($aFilterItem);
					if($aStatement['type'] == 'where') {
						$aFilterWhereStatements[] = '('.$aStatement['statement'].')';
					} else if($aStatement['type'] == 'having') {
						$aFilterHavingStatements[] = '('.$aStatement['statement'].')';
					}
				}
			}
	
			if(!is_array($this->aReportWhereStatement))  $this->aReportWhereStatement  = array();
			if(!is_array($this->aReportHavingStatement)) $this->aReportHavingStatement = array();
			if(is_array($this->sReportSelectStatement)) {
				foreach ($this->sReportSelectStatement as $k => $select) {
					if(empty($this->aReportWhereStatement[$k])) $this->aReportWhereStatement[$k] = array();
					$this->aReportWhereStatement[$k] = array_merge($this->aReportWhereStatement[$k], $aFilterWhereStatements);
				}
				foreach ($this->sReportSelectStatement as $k => $select) {
					if(empty($this->aReportHavingStatement[$k])) $this->aReportHavingStatement[$k] = array();
					$this->aReportHavingStatement[$k] = array_merge($this->aReportHavingStatement[$k], $aFilterHavingStatements);
				}
			} else {
				$this->aReportWhereStatement = array_merge($this->aReportWhereStatement, $aFilterWhereStatements);
				$this->aReportHavingStatement = array_merge($this->aReportHavingStatement, $aFilterHavingStatements);
			}
		}
		
		//get result
		if($this->oBase instanceof DBConnection ) {
			$aData = $this->getResult(
				$this->sReportSelectStatement,
				$this->aReportWhereStatement,
				$sOrderBy,
				$sLimit,
				$this->sReportGroupStatement,
				$this->aReportHavingStatement
			);
		} else {
			throw new Exception('Invalid base class - '.get_class($this->oBase));
		}

		if(isset($aRequest['paging']) && $this->bUseSQLPaging) {
			$this->oResponse->paging['totalRows'] = $this->oBase->selectField("SELECT FOUND_ROWS()");
		}

		return $aData;
	}

	public function loadData($aRequest, $nTimeFrom = 0, $nTimeTo = 0) {
		//time from/to
		if(isset($aRequest['fields'][$this->sPeriodField]) && !$nTimeFrom) {
			$nTimeFrom = $this->timeButtonToTimestampFrom($aRequest['fields'][$this->sPeriodField]);
		}
		if(isset($aRequest['fields'][$this->sPeriodField]) && !$nTimeTo) {
			$nTimeTo = $this->timeButtonToTimestampTo($aRequest['fields'][$this->sPeriodField]);
		}
		if(isset($aRequest['fields'][$this->sDateFromField]) && !$nTimeFrom) {
			$nTimeFrom = Util::SQLDateToTimeStamp($aRequest['fields'][$this->sDateFromField]);
		}
		if(isset($aRequest['fields'][$this->sDateToField]) && !$nTimeTo) {
			$nTimeTo = Util::SQLDateToTimeStamp($aRequest['fields'][$this->sDateToField]);
		}

		$nTimeFrom = !empty( $nTimeFrom ) ? $nTimeFrom : mktime( 0, 0, 0, date('m')-1, date('d'), date('Y'));

		if(empty($aRequest['sortFields'])) {
			$aRequest['sortFields'] = $this->aDefaultSort;
		}
		$this->aColumnsAssoc = $this->getColumnsAssoc();
		foreach ($aRequest['sortFields'] as $aSortField) {
			if(empty($this->aColumnsAssoc[$aSortField['field']]) && $this->aColumnsAssoc[$aSortField['field']]->sortable === false) throw new InvalidParameter('Невалидна колона за сортиране.');
		}
		$aData = $this->getGridData($aRequest, $nTimeFrom, $nTimeTo);

		$aData = $this->processData($aData);

		if(!$this->bUseSQLFilters)
		{
			$aFilterItems = array();
			
			if(!empty($aRequest['filter']) && !empty($aRequest['filter']['id'])) {
				$aFilterItems = DBGridFilters::getFilterByID($aRequest['filter']['id']);
				$aFilterItems = $aFilterItems['filter_items'];
			}
			
			if(!empty($aRequest['filterItems']) && is_array($aRequest['filterItems'])) {
				$aFilterItems = array_merge($aFilterItems, $aRequest['filterItems']);
			}
			
			$this->filterData($aData, $aFilterItems);
		}
		
		$this->setGridTotals($aData, $aRequest, $nTimeFrom, $nTimeTo);
		
		//sort
		if(!$this->bUseSQLSort) {
			$this->sortData($aData, $aRequest['sortFields']);
		}

		//oshte paging
		if(isset($aRequest['paging'])) {
			if(!$this->bUseSQLPaging) {
				$this->oResponse->paging['totalRows'] = count($aData);
				$aData = array_slice($aData, intval($aRequest['paging']['start']), intval($aRequest['paging']['limit']));
			}
		}

		$this->setGridData($aData);

		return $this->oResponse;
	}

	protected function filterData(&$aData, $aFilterItems)
	{
		if(empty($aFilterItems)) return;

		foreach($aFilterItems as $aFilterItem) {
            if(!($oColumn = $this->aColumnsAssoc[$aFilterItem['dataField']])) throw new InvalidParameter(L('Колона %s не е намерена.', $aFilterItem['dataField']));
            if($oColumn->type == self::CTYPE_ENUM && $aFilterItem['compareType'] == 'ct') {
                $aEnumCTValues = array();
                foreach($oColumn->{"items"} as $sEnumKey => $aEnumItem) {
                    if(strpos(mb_strtolower($aEnumItem['text'], 'UTF-8'), mb_strtolower($aFilterItem['compareValue'], 'UTF-8')) !== false) {
                        $aEnumCTValues[$sEnumKey] = true;
                    }
                }
            } else {
                $aEnumCTValues = null;
            }
		    foreach($aData as $key=>$row) {
                if($aEnumCTValues ?
                    !$aEnumCTValues[$row[$aFilterItem['dataField']]] :
                    !$this->checkValue($row[$aFilterItem['dataField']], $aFilterItem['compareType'], $aFilterItem['compareValue'])
                  ) {
                    unset($aData[$key]);
                }
			}
		}
	}
	
	/**
	 *
	 * @param array $aData - данни
	 * @param array $aSortFields  - полета за сортиране
	 */
	protected function sortData(&$aData, $aSortFields) {
		if(!is_array($aData)) return;
		$this->aSortFields = $aSortFields;
		if(!empty($this->aSortFields)) usort($aData, array($this,'sortCompare'));
		$this->oResponse->sortFields =  $aSortFields;
	}

	private function sortCompare($aRow1, $aRow2) {
		foreach($this->aSortFields as $aSortField) {
			if(empty($this->aColumnsAssoc[$aSortField['field']]->type) || !in_array($this->aColumnsAssoc[$aSortField['field']]->type, self::$aNumericColumnTypes)) {
				$cmp = strcmp(mb_strtolower($aRow1[$aSortField['field']]), mb_strtolower($aRow2[$aSortField['field']]));
			} else {
				$cmp = $aRow1[$aSortField['field']] - $aRow2[$aSortField['field']];
			}
			if($aSortField['dir'] != 'ASC') $cmp = -$cmp;
			if($cmp != 0) return $cmp;
		}
		return 0;
	}

	/**
	 * Създава масиви с условията за търсене според филтъра
	 *
	 * @param array $aFilter
	 * @return array - array('where' => array(..),'having' => array(..))
	 */
	protected function createFilterStatements($aFilter) {
		$aStatements = array(
			'where' => array(),
			'having' => array()
		);
		foreach ($aFilter['filter_items'] as $aFilterItem) {
			$aStatement =  $this->createFilterItemStatement($aFilterItem);
			if($aStatement['type'] == 'where') {
				$aStatements['where'][$aFilterItem['dataField']][$aStatement['compareType']][] = $aStatement['statement'];
			} else {
				$aStatements['having'][$aFilterItem['dataField']][$aStatement['compareType']][] = $aStatement['statement'];
			}
		}

		$result = [];
		foreach($aStatements as $type => $statements) {
            foreach ($statements as $field => $compareTypes) {
                foreach($compareTypes as $t => $v) {
                    $operation = strtolower(substr($t, 0, 1)) == 'n' ? ' AND ' : ' OR ';
		            $result[$type][] = count($v) > 1 ? '('.implode($operation, $v). ' )' : reset($v);
                }
            }
        }

		return $result;
	}

	/**
	 * Създава условиe за търсене според елемент от филтъра
	 *
	 * @param array $aFilterItem
	 * @return array - array('type' => 'where'/'having' , 'statement' => ' sender_city != 'Варна')
	 */
	protected function createFilterItemStatement($aFilterItem) {
		if(!array_key_exists($aFilterItem['dataField'], $this->aColumnsAssoc)) throw new InvalidParameter(L('Колона %s не е намерена.',$aFilterItem['dataField']));
		$aRes = array();
		if($this->aColumnsAssoc[$aFilterItem['dataField']]->DBFieldName) {
			$aFilterItem['dataField'] = $this->aColumnsAssoc[$aFilterItem['dataField']]->DBFieldName;
			$aRes['type'] = 'where';
		} else {
			$aRes['type'] = 'having';
		}

		$aRes['statement'] = GridFilterCreateSQL::create($aFilterItem, $this->aColumnsAssoc[$aFilterItem['dataField']]);
        $aRes['compareType'] = $aFilterItem['compareType'];
		return $aRes;
	}

    private function getResult($sSelectStatement = NULL, $aWhereStatement = NULL, $sOrderStatement = NULL, $sLimitStatement = NULL, $sGroupStatement = NULL, $aHavingStatement = NULL ) {
        $sQuery = $sSelectStatement;
        if(!empty($aWhereStatement ) && is_array( $aWhereStatement ) ) $sQuery = sprintf( " %s \n WHERE \n %s \n ", $sQuery, implode( ' AND ', $aWhereStatement) );
        if(!empty($sGroupStatement)) $sQuery = sprintf( " %s \n GROUP BY \n %s \n ", $sQuery, $sGroupStatement );
        if(!empty($aHavingStatement) && is_array($aHavingStatement)) $sQuery = sprintf( " %s \n HAVING \n %s \n ", $sQuery, implode( ' AND ', $aHavingStatement) );
        if(!empty($sOrderStatement ) ) $sQuery = sprintf( " %s \n ORDER BY %s \n ", $sQuery, trim($sOrderStatement,'_') );
        if(!empty($sLimitStatement ) ) $sQuery = sprintf( " %s \n LIMIT %s \n ", $sQuery, $sLimitStatement );
        return $this->oBase->select($sQuery);
    }

	public function load($aRequest = array(), $aInitParams = array()) {
		// TODO: Implement load() method.
	}
}



