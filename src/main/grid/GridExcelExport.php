<?php
namespace main\grid;
use main\form\ResponseColumn;
use main\language\Language;
use main\Util;

require_once(BASE_PATH.'/src/PHPExcel/PHPExcel.php');

class GridExcelExport extends GridExport {
    const WIDTH_MULTIPLIER = 0.142857;
    const MS_SANS_SERIF = 'MS Sans Serif';
    const ARIAL			= 'arial';
    public static $aColumnTypes = array();
    public static $aCurrencyFormats = array();

    private static $sDateFormat = '';
    private static $sDateTimeFormat = '';
    public static function getDateFormat() {
        if(!self::$sDateFormat) {
            self::$sDateFormat = Language::getTranslationForText('excel_date_format') == 'excel_date_format' ? 'dd.mm.yy' : Language::getTranslationForText('excel_date_format');
        }
        return self::$sDateFormat;
    }
    public static function getDateTimeFormat() {
        if(!self::$sDateTimeFormat) {
            self::$sDateTimeFormat = Language::getTranslationForText('excel_date_time_format') == 'excel_date_time_format' ? 'dd.mm.yy hh:mm' : Language::getTranslationForText('excel_date_time_format');
        }
        return self::$sDateTimeFormat;
    }

    /**
     * @var \PHPExcel
     */
    public $oExcelBook;

    public $aData = array();

    public function __construct($oExport, $oTarget) {
        parent::__construct($oExport, $oTarget);
        $this->oExcelBook = new \PHPExcel();
    }

    /**
     * @param array $aRow
     * @param ResponseColumn $oColumn
     * @return string
     */
    public static function enumRenderer($aRow, $oColumn) {
        if ( $oColumn->toolTipField == $oColumn->items[$aRow[$oColumn->dataField]]['text'] ) {
            return (string) $aRow[$oColumn->toolTipField];
        }
        return (string) $oColumn->items[$aRow[$oColumn->dataField]]['text'];
    }

    private static $nTimeZoneGMTOffset = null;
    private static function getTimeZoneGMTOffset() {
        if(self::$nTimeZoneGMTOffset === null) {
            $currentZone = new \DateTimeZone(date_default_timezone_get());
            $currentDate = new \DateTime("now", $currentZone);
            self::$nTimeZoneGMTOffset = $currentZone->getOffset($currentDate);
        }
        return self::$nTimeZoneGMTOffset;
    }

    /**
     * @param array $aRow
     * @param ResponseColumn $oColumn
     * @return mixed
     */
    public static function mysqlDateToExcelDate($aRow,$oColumn) {
        $nDate = Util::SQLDateToTimeStamp($aRow[$oColumn->dataField]);
        if(!$nDate) return '';
        else return \PHPExcel_Shared_Date::PHPToExcel($nDate + self::getTimeZoneGMTOffset());
    }

    /**
     * ako e prazna datata se slaga format za string za da ne izliza 01.01.1900
     *
     * @param array $aRow
     * @param ResponseColumn $oColumn
     * @return string
     */
    public static function getCellDateFormat($aRow, $oColumn) {
        $nDate = Util::SQLDateToTimeStamp($aRow[$oColumn->dataField]);
        if(!$nDate) return array(
            'numberFormat' => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
            'dataType' => \PHPExcel_Cell_DataType::TYPE_STRING
        );
        else return array(
            'numberFormat' => self::getDateFormat(),
            'dataType' => \PHPExcel_Cell_DataType::TYPE_NUMERIC
        );
    }

    /**
     * ako e prazna datata se slaga format za string za da ne izliza 01.01.1900
     *
     * @param array $aRow
     * @param ResponseColumn $oColumn
     * @return string
     */
    public static function getCellDateTimeFormat($aRow, $oColumn) {
        $nDate = Util::SQLDateToTimeStamp($aRow[$oColumn->dataField]);
        if(!$nDate) return array(
            'numberFormat' => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
            'dataType' => \PHPExcel_Cell_DataType::TYPE_STRING
        );
        else return array(
            'numberFormat' => self::getDateTimeFormat(),
            'dataType' => \PHPExcel_Cell_DataType::TYPE_NUMERIC
        );
    }

    /**
     * @param array $aRow
     * @param ResponseColumn $oColumn
     * @return array
     */
    public static function getCellCurrencyFormat($aRow,$oColumn) {
        $aFormat = array(
            'dataType' => \PHPExcel_Cell_DataType::TYPE_NUMERIC
        );
        if(isset($oColumn->currencyField) && array_key_exists($aRow[$oColumn->currencyField], self::$aCurrencyFormats)) {
            $aFormat['numberFormat'] = self::$aCurrencyFormats[$aRow[$oColumn->currencyField]];
        } else {
            $aFormat['numberFormat'] = '0.00';
        }
        return $aFormat;
    }



    /**
     * @param \PHPExcel_Worksheet $oWorksheet
     * @param string $sCaption
     */
    function setupSheet($oWorksheet, $sCaption)
    {
        $oWorksheet->setTitle( str_replace('/', ' ', $sCaption ));

        /** @noinspection PhpUndefinedMethodInspection */
        $oWorksheet->getSheetView()->setZoomScale(100);
        $oWorksheet->getPageSetup()->setOrientation(\PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $oWorksheet->getPageMargins()->setTop(0.25);
        $oWorksheet->getPageMargins()->setRight(0.25);
        $oWorksheet->getPageMargins()->setBottom(0.5);
        $oWorksheet->getPageMargins()->setFooter(0.25);
        $oWorksheet->getHeaderFooter()->setOddFooter("Справката е генерирана със софтуер изработен от IT Partner © ".date('Y')." www.it-partner.bg. Време на генериране на справката: ".date('d.m.y H:i:s'));
    }

    /**
     * @param \PHPExcel_Worksheet $oWorksheet
     * @param string $sTableCaption
     * @param GridPanelHandler $oGridHandler
     * @param integer $nStartCol
     * @param integer $nStartRow
     */
    public function addTable( $oWorksheet, $sTableCaption, $oGridHandler, $nStartCol, $nStartRow ) {
        $nStartRow += $this->setTitleRows($oWorksheet, $sTableCaption, 0, $nStartRow, 0);

        $this->{'aXLSColumns'} = array();
        $this->{'nLastHeaderRow'} = $oGridHandler->getHeaderRowCount() + $nStartRow - 1;
        $this->createHeaders($oWorksheet, $this->aColumns ,$nStartCol ,$nStartRow);

        $nTotalRows = $this->setData($oWorksheet, $this->getData(), $this->{'nLastHeaderRow'} + 1);
        $this->setTotals($oWorksheet, $this->{'nLastHeaderRow'} + 1 + $nTotalRows);
    }

    /**
     * @param \PHPExcel_Worksheet $oWorksheet
     * @param integer $nRow
     */
    public function setTotals($oWorksheet, $nRow) {}

    /**
     * @param \PHPExcel_Worksheet $oWorksheet
     * @param string $sTableCaption
     * @param integer $nColumn
     * @param integer $nRow
     * @return integer row count
     */
    public function setTitleRows(/** @noinspection PhpUnusedParameterInspection */
        $oWorksheet, $sTableCaption, $nStartCol, $nStartRow, $nColumnCount = 1) {
        return 0;
    }

    /**
     * @param \PHPExcel_Worksheet $oWorksheet
     * @param GridPanelHandler $oGridHandler
     * @param integer $nStartCol
     * @param integer $nStartRow
     * @return integer
     */
    public function createHeaders($oWorksheet, $aColumns, $nStartCol, $nStartRow) {
        $nCol = 0;
        foreach ($aColumns as $oColumn) {

            if(in_array($oColumn->type,array('button'))) continue;

            $oWorksheet->setCellValueByColumnAndRow($nStartCol + $nCol, $nStartRow, $oColumn->headerText );

            if(!empty($oColumn->children)) {
                $nChildColCount = $this->createHeaders($oWorksheet,$oColumn->children, $nStartCol + $nCol, $nStartRow + 1);
                //merge cols
                $oWorksheet->mergeCellsByColumnAndRow($nStartCol + $nCol, $nStartRow, $nStartCol + $nCol + $nChildColCount -1,$nStartRow);
                $oWorksheet->getStyle(
                    \PHPExcel_Cell::stringFromColumnIndex($nStartCol + $nCol) . $nStartRow . ':' .
                    \PHPExcel_Cell::stringFromColumnIndex($nStartCol + $nCol + $nChildColCount -1) .$nStartRow
                )->applyFromArray(self::$aColumnTypes['header']['format']);
                $nCol+= $nChildColCount;
            } else {
                $nWidth = 100;
                if(!empty($oColumn->width)) $nWidth = $oColumn->width ;
                if(!empty($oColumn->exportWidth)) $nWidth = $oColumn->exportWidth ;
                if(!empty($oColumn->hidden)) $nWidth = 0;
                $this->{'aXLSColumns'}[$nStartCol + $nCol] = $oColumn;
                $oWorksheet->getColumnDimensionByColumn($nStartCol + $nCol)->setWidth($nWidth * self::WIDTH_MULTIPLIER);
                //merge rows
                $oWorksheet->mergeCellsByColumnAndRow($nStartCol + $nCol, $nStartRow, $nStartCol + $nCol, $this->{'nLastHeaderRow'});
                $oWorksheet->getStyle(
                    \PHPExcel_Cell::stringFromColumnIndex($nStartCol + $nCol) . $nStartRow . ':' .
                    \PHPExcel_Cell::stringFromColumnIndex($nStartCol + $nCol) . $this->{'nLastHeaderRow'}
                )->applyFromArray(self::$aColumnTypes['header']['format']);
                $nCol++;
// 				\PHPExcel_Style_Font::setName($pValue);
            }

        }
        return $nCol;
    }

    /**
     * @param \PHPExcel_Worksheet $oWorksheet
     * @param array $aData
     * @param integer $nStartRow
     * @return integer row count
     */
    public function setData($oWorksheet, $aData, $nStartRow, $bGray = FALSE) {
        $nDataRow = 0;
        $rowIndex=0;
        foreach ($aData as $aRow) {
            $rowIndex++;
            foreach ($this->{'aXLSColumns'} as $nCol => $oColumn) {
                $sColType = $oColumn->type;
                if($sColType == 'rowIndex') $sColType = 'integer';
                else if(!array_key_exists($sColType,self::$aColumnTypes)) $sColType = 'string';

                $oCell = $oWorksheet->getCellByColumnAndRow($nCol, $nStartRow + $nDataRow );

                //style , format
                $aStyle = self::$aColumnTypes[$sColType]['format'];
                if( $bGray ){
                    $aStyle['fill'] = array(
                        'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'DDDDDD')
                    );
                }

                $oWorksheet->getStyleByColumnAndRow($nCol, $nStartRow + $nDataRow)
                    ->applyFromArray($aStyle);

                //number format callback
                if(isset(self::$aColumnTypes[$sColType]['numberFormatFn'])) {
                    $aFormat = call_user_func_array(self::$aColumnTypes[$sColType]['numberFormatFn'],array($aRow,$oColumn));
                    $oWorksheet->getStyleByColumnAndRow($nCol, $nStartRow + $nDataRow)->getNumberFormat()->setFormatCode($aFormat['numberFormat']);
                } else {
                    $aFormat =array();
                }

                //value
                if($oColumn->type == 'rowIndex') {
                    $value = $rowIndex;
                } elseif(isset(self::$aColumnTypes[$sColType]['valueFn'])) {
                    $value = call_user_func_array(self::$aColumnTypes[$sColType]['valueFn'],array($aRow,$oColumn));
                } else {
                    $value = $aRow[$oColumn->dataField];
                }

                if(!empty($aFormat['dataType'])) {
                    $oCell->setValueExplicit($value, $aFormat['dataType']);
                } else if(!empty(self::$aColumnTypes[$sColType]['cellDataType'])) {
                    $oCell->setValueExplicit($value, self::$aColumnTypes[$sColType]['cellDataType']);
                } else {
                    $oCell->setValue($value);
                }
            }

            $nDataRow++;
        }
        return $nDataRow;
    }


    public function doExport() {
        try {
            $this->oExcelBook->getProperties()->setCreator('')
                ->setLastModifiedBy("")
                ->setTitle("")
                ->setSubject("")
                ->setDescription("")
                ->setKeywords("")
                ->setCategory("");

            $oWorksheet = $this->oExcelBook->setActiveSheetIndex(0);
            $this->setupSheet($oWorksheet,'sheet 1');
            $this->addTable($oWorksheet,$this->sGridTitle,$this->oTarget, 0,1);

            switch ($this->oExport->sExportType) {
                case 'ExcelOpenXML':
                    $oWriter = \PHPExcel_IOFactory::createWriter($this->oExcelBook, 'Excel2007');
                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition: attachment;filename="'.$this->sFileName.'.xlsx"');
                    header('Cache-Control: max-age=0');

                    break;
                case 'Excel5' :
                    header('Content-Type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment;filename="'.$this->sFileName.'.xls"');
                    header('Cache-Control: max-age=0');

                    $oWriter = \PHPExcel_IOFactory::createWriter($this->oExcelBook, 'Excel5');
                    break;
                default:
                    header('Content-Type: application/pdf');
                    header('Content-Disposition: attachment;filename="'.$this->sFileName.'.pdf"');
                    header('Cache-Control: max-age=0');

                    $oWriter = \PHPExcel_IOFactory::createWriter($this->oExcelBook, 'PDF');
            }
            $oWriter->save('php://output');
            die();
        } catch (\Throwable $e) {
            header_remove();
            header('Content-Type: text/plain');
            die($e->getMessage());
        }
    }

}
GridExcelExport::$aColumnTypes = array(
    'title' => array(
        'cellDataType' => \PHPExcel_Cell_DataType::TYPE_STRING,
        'format' => array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'font' => array(
                'bold' 	=> true,
                'size' 	=> 11,
                'name'	=> GridExcelExport::ARIAL,
                'color'	=>  array('argb' => \PHPExcel_Style_Color::COLOR_BLACK)
            ),
            'borders' => array(
                'outline' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_NONE
                ),
            ),
        ),
    ),
    'header' => array(
        'cellDataType' => \PHPExcel_Cell_DataType::TYPE_STRING,
        'format' => array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical'   => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'wrap'       => TRUE,
            ),
            'font' => array(
                'bold' 	=> false,
                'size' 	=> 8,
                'name'	=> GridExcelExport::ARIAL,
                'color'	=>  array('argb' => \PHPExcel_Style_Color::COLOR_BLUE)
            ),
            'borders' => array(
                'outline' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => \PHPExcel_Style_Color::COLOR_BLACK)
                ),
            ),
        ),
    ),
    'string' => array(
        'cellDataType' => \PHPExcel_Cell_DataType::TYPE_STRING,
        'format' => array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT ,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'wrap'       => TRUE,
            ),
            'font' => array(
                'bold' 	=> false,
                'size' 	=> 8,
                'name'	=> GridExcelExport::ARIAL,
                'color'	=> array('argb' => \PHPExcel_Style_Color::COLOR_BLACK)
            ),
            'borders' => array(
                'outline' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => \PHPExcel_Style_Color::COLOR_BLACK)
                ),
            ),
            'numberformat' => array(
                'code'	=> \PHPExcel_Style_NumberFormat::FORMAT_TEXT
            )
        ),
    ),
    'integer' => array(
        'cellDataType' => \PHPExcel_Cell_DataType::TYPE_NUMERIC ,
        'format' => array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT ,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'font' => array(
                'bold' 	=> false,
                'size' 	=> 8,
                'name'	=> GridExcelExport::ARIAL,
                'color'	=> array('argb' => \PHPExcel_Style_Color::COLOR_BLACK)
            ),
            'borders' => array(
                'outline' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color'	=> array('argb' => \PHPExcel_Style_Color::COLOR_BLACK)
                ),
            ),
            'numberformat' => array(
                'code'	=> \PHPExcel_Style_NumberFormat::FORMAT_NUMBER
            )
        )
    ),
    'float' => array(
        'cellDataType' => \PHPExcel_Cell_DataType::TYPE_NUMERIC ,
        'format' => array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT ,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'font' => array(
                'bold' 	=> false,
                'size' 	=> 8,
                'name'	=> GridExcelExport::ARIAL,
                'color'	=> array('argb' => \PHPExcel_Style_Color::COLOR_BLACK)
            ),
            'borders' => array(
                'outline' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color'	=> array('argb' => \PHPExcel_Style_Color::COLOR_BLACK)
                ),
            ),
            'numberformat' => array(
                'code'	=> \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00
            )
        ),
    ),
    'date' => array(
        'cellDataType' => \PHPExcel_Cell_DataType::TYPE_NUMERIC ,
        'format' => array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT ,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'font' => array(
                'bold' 	=> false,
                'size' 	=> 8,
                'name'	=> GridExcelExport::ARIAL,
                'color'	=> array('argb' => \PHPExcel_Style_Color::COLOR_BLACK)
            ),
            'borders' => array(
                'outline' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => \PHPExcel_Style_Color::COLOR_BLACK)
                ),
            ),
        ),
        'numberFormatFn' => array('main\\grid\\GridExcelExport','getCellDateFormat'),
        'valueFn' => array('main\\grid\\GridExcelExport','mysqlDateToExcelDate')
    ),
    'dateTime' => array(
        'cellDataType' => \PHPExcel_Cell_DataType::TYPE_NUMERIC ,
        'format' => array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT ,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'font' => array(
                'bold' 	=> false,
                'size' 	=> 8,
                'name'	=> GridExcelExport::ARIAL,
                'color'	=> array('argb' => \PHPExcel_Style_Color::COLOR_BLACK)
            ),
            'borders' => array(
                'outline' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => \PHPExcel_Style_Color::COLOR_BLACK)
                ),
            ),
        ),
        'numberFormatFn' => array('main\\grid\\GridExcelExport','getCellDateTimeFormat'),
        'valueFn' => array('main\\grid\\GridExcelExport','mysqlDateToExcelDate')
    ),
    'currency' => array(
        'cellDataType' => \PHPExcel_Cell_DataType::TYPE_NUMERIC ,
        'format' => array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT ,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'font' => array(
                'bold' 	=> false,
                'size' 	=> 8,
                'name'	=> GridExcelExport::ARIAL,
                'color'	=> array('argb' => \PHPExcel_Style_Color::COLOR_BLACK)
            ),
            'borders' => array(
                'outline' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color'	=> array('argb' => \PHPExcel_Style_Color::COLOR_BLACK)
                ),
            ),
            'numberformat' => array(
                'code'	=> \PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00
            )
        ),
        'numberFormatFn' => array('main\\grid\\GridExcelExport','getCellCurrencyFormat')
    ),
    'enum' => array(
        'cellDataType' => \PHPExcel_Cell_DataType::TYPE_STRING,
        'format' => array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT ,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'font' => array(
                'bold' 	=> false,
                'size' 	=> 8,
                'name'	=> GridExcelExport::ARIAL,
                'color'	=> array('argb' => \PHPExcel_Style_Color::COLOR_BLACK)
            ),
            'borders' => array(
                'outline' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => \PHPExcel_Style_Color::COLOR_BLACK)
                ),
            ),
            'numberformat' => array(
                'code'	=> \PHPExcel_Style_NumberFormat::FORMAT_TEXT
            )
        ),
        'valueFn' => array('main\\grid\\GridExcelExport','enumRenderer')
    ),
    'editedBy' => array(
        'cellDataType' => \PHPExcel_Cell_DataType::TYPE_STRING,
        'format' => array(
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_LEFT ,
                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ),
            'font' => array(
                'bold' 	=> false,
                'size' 	=> 8,
                'name'	=> GridExcelExport::ARIAL,
                'color'	=> array('argb' => \PHPExcel_Style_Color::COLOR_BLACK)
            ),
            'borders' => array(
                'outline' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => \PHPExcel_Style_Color::COLOR_BLACK)
                ),
            ),
            'numberformat' => array(
                'code'	=> \PHPExcel_Style_NumberFormat::FORMAT_TEXT
            )
        ),
        'valueFn' => array('main\\grid\\GridExport','createEditedByText')
    ),
);

GridExcelExport::$aCurrencyFormats['BGN'] = '# ##0.00 лв';
GridExcelExport::$aCurrencyFormats['лв'] = GridExcelExport::$aCurrencyFormats['BGN'];

GridExcelExport::$aCurrencyFormats['USD'] = ' [$$-409] # ##0.00';
GridExcelExport::$aCurrencyFormats['$'] = GridExcelExport::$aCurrencyFormats['USD'];

GridExcelExport::$aCurrencyFormats['EUR'] = ' [$€-2] # ##0.00';
GridExcelExport::$aCurrencyFormats['€'] = GridExcelExport::$aCurrencyFormats['EUR'];

GridExcelExport::$aCurrencyFormats['GBP'] = ' [$£-809] # ##0.00';
GridExcelExport::$aCurrencyFormats['₤'] = GridExcelExport::$aCurrencyFormats['GBP'];

GridExcelExport::$aCurrencyFormats['RON'] = '# ##0.00 [$leu-418]';
GridExcelExport::$aCurrencyFormats['leu'] = GridExcelExport::$aCurrencyFormats['RON'];

GridExcelExport::$aCurrencyFormats['CZK'] = '# ##0.00 [$Kč-405]';
GridExcelExport::$aCurrencyFormats['Kč'] = GridExcelExport::$aCurrencyFormats['CZK'];

GridExcelExport::$aCurrencyFormats['HUF'] = '# ##0.00 [$Ft-40E]';
GridExcelExport::$aCurrencyFormats['Ft'] = GridExcelExport::$aCurrencyFormats['HUF'];
