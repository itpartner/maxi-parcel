<?php
namespace main\grid;
use main\Exception;

require_once(BASE_PATH . '/src/PHPPdf/PHPPdf.php');
	
class GridPdfExport extends GridExport {
	private $sPDFInputType = 'I';
	private $aGridData     = '';
	private $aGridColumns  = '';
	private $aGridColumnsF = '';
	private $nDepth        = 0;
	private $nLavel        = 0;	
	public	$bReturnFile   = false;
	public	$sTempFilePathAndName = '';

	public function __construct($oExport, $oTarget) {
		parent::__construct($oExport, $oTarget);
	}
	
    protected function fCellData($aColumns) {
        foreach ($aColumns as $aGridColumnData) {
            if ($aGridColumnData['type'] === 'enum') {
                foreach ($this->aGridData['data'] as $aGridDataKey => $aGridDataVal) {
                	$this->aGridData['data'][$aGridDataKey][$aGridColumnData['dataField']] = $aGridColumnData['items'][$aGridDataVal[$aGridColumnData['dataField']]]['text'];	
                }
                
            } else if ($aGridColumnData['type'] === 'group') {
                $this->fCellData($aGridColumnData['children']);
            } 
        }
    }
    
	public function doExport() {
		$this->aGridData = $this->getData();

		if (empty($this->aGridData['data'])) {
			throw new Exception(L('Липсват данни за експорт'));
		}
		
		$this->aGridColumns  = $this->oExport->aRequest['columns'];

		if(empty($this->aGridColumns) && !empty($this->aColumns))
			$this->aGridColumns  = json_decode(json_encode($this->aColumns), true);

        $this->fCellData($this->aGridColumns); // заради enum ама ще влезе и за друго в употреба :)
  		foreach ($this->aGridColumns as $aGridColumnData) {
            if ($aGridColumnData['type'] === 'enum') {
                foreach ($this->aGridData['data'] as $aGridDataKey => $aGridDataVal) {
                	if ( $aGridColumnData['toolTipField'] == $this->aGridData['data'][$aGridDataKey][$aGridColumnData['dataField']] ) { 
                		if ( empty($aGridDataVal[$aGridColumnData['toolTipField']]) ) {
           					$this->aGridData['data'][$aGridDataKey][$aGridColumnData['dataField']] = ' ';
                		} else {
                			$this->aGridData['data'][$aGridDataKey][$aGridColumnData['dataField']] = $aGridDataVal[$aGridColumnData['toolTipField']];
               			}
                	}
                }
            }
			if ($aGridColumnData['type'] == 'rowIndex') {
				$i=0;
				foreach ($this->aGridData['data'] as $aGridDataKey => $aGridDataVal) {
					$this->aGridData['data'][$aGridDataKey][$aGridColumnData['dataField']] = ++$i.'  ';//intervalite sa zaradi pochti vernite smetki za shirinata na kletkite
				}
			}
		}

        $this->sPDFInputType = empty($this->oExport->aRequest['pdf_attach']) ? 'I' : 'D';
		
		$this->aGridColumnsF = $this->formateGridColumns($this->aGridColumns) ;
		
		$oPdf = new \PDF('l');
		
		$oPdf->SetTitle($this->sGridTitle);
		
		$aTableColumnsWidths = $oPdf->MakeColWidth(
			array(
				'fields' => $this->aGridColumnsF,
				'data'   => $this->aGridData['data']
			), 
			$oPdf->_PageWidth - $oPdf->aMargin['left'] - $oPdf->aMargin['right']
		);
		
		$oPdf->AddPage();
		
		$oPdf->PrintTableHeader($this->aGridColumns, $aTableColumnsWidths);
		
		$aTableColumnsHeader = $this->myGirdData($oPdf->aMyGridData);
		
		$gridtotals = (!empty($this->aGridData['totals']) ? $this->aGridData['totals'] : NULL);
        $oPdf->RealPrintTableHeader($aTableColumnsHeader, $this->nDepth, $this->nLavel, $gridtotals);
		
		$oPdf->SetX($oPdf->aMargin['left']);
        $oPdf->PrintTableData($this->aGridData['data'], $aTableColumnsHeader);
		
		$oPdf->SetX($oPdf->aMargin['left']);
		$oPdf->PrintTotal($this->aGridData['data']);

		if(empty($this->bReturnFile))
        $oPdf->Output($this->sFileName . '.pdf', $this->sPDFInputType);
        else
        	$oPdf->Output($this->sTempFilePathAndName, 'F');
	}
	
	private function myGirdData($aMyGridData) {
		$aElemtsX = array();
		$aElemtsY = array();
		$nTableColumnsLavelSum = 0;
		$nTableColumnsLavelDepth = 0;
		
		foreach ($aMyGridData as $sMyGridDataKey => $aMyGridDataValue) {
			$aElemtsX[$sMyGridDataKey] = $aMyGridDataValue['x'];
			$aElemtsY[$sMyGridDataKey] = $aMyGridDataValue['y'];
			
			if ($aMyGridDataValue['y'] > $nTableColumnsLavelDepth) {
				$nTableColumnsLavelSum++;
				$nTableColumnsLavelDepth = $aMyGridDataValue['y'];		
			}
		}
		
		sort($aElemtsX);
		sort($aElemtsY);
		
		$aElemtsY = array_unique($aElemtsY);

		$aSortedGirdData = array();
		foreach ($aElemtsY as $sElemtsYValue) {
			foreach ($aElemtsX as $sElemtsXValue) {
				foreach ($aMyGridData as $sMyGridDataKey => $aMyGridDataValue) {
					if ($aMyGridDataValue['x'] == $sElemtsXValue && $aMyGridDataValue['y'] == $sElemtsYValue) {
						$aSortedGirdData[] = array(
							'text'      => $this->aGridColumnsF[$sMyGridDataKey]['headerText'],
							'dataField' => $sMyGridDataKey,
							'x'         => $aMyGridDataValue['x'    ],
							'y'		    => $aMyGridDataValue['y'	],
							'width'     => $aMyGridDataValue['width'],
							'type'      => $this->aGridColumnsF[$sMyGridDataKey]['type'],
							'sign'		=> !empty($this->aGridColumnsF[$sMyGridDataKey]['currencyField']) ? $this->aGridColumnsF[$sMyGridDataKey]['currencyField'] : null,
						);
					}
				}
			}
		}

		// проверка за нулеви колони и за повтарящи се колони
		$aMachList = array();
		foreach ($aSortedGirdData as $nSortedGirdDataKey => $aSortedGirdDataVal) {
			if ($aSortedGirdDataVal['width'] == 0) {
				unset($aSortedGirdData[$nSortedGirdDataKey]);
			}
			
			if (in_array($aSortedGirdDataVal['dataField'], $aMachList)) {
				unset($aSortedGirdData[$nSortedGirdDataKey]);
			} else {
				$aMachList[] = $aSortedGirdDataVal['dataField'];
			}
		}
		
		$this->nDepth = $nTableColumnsLavelDepth;
		$this->nLavel = $nTableColumnsLavelSum;
		return $aSortedGirdData;
	}
	
	private function formateGridColumns($aColumns) {
		$aNewColumns = array();
		foreach ($aColumns as $aColumnsValue) {
			if(!empty($aColumnsValue['hidden'])) continue;
			switch ($aColumnsValue['type']) {
				case 'string':
				case 'integer':
				case 'float':
				case 'date':
				case 'dateTime':
				case 'time':
				case 'enum':
				case 'currency':
				case 'rowIndex':
					if (!empty($aColumnsValue['dataField'])) $aNewColumns[$aColumnsValue['dataField']] = $aColumnsValue;
					break;
				case 'group':
					$aChild = $this->formateGridColumns($aColumnsValue['children']);
					if(count($aChild) != 0) $aNewColumns = array_merge($aNewColumns, $aChild);
				break;
			}
		}
		
		return $aNewColumns;
	}
	
	protected function getData() {
		$aDataObject = parent::getData();
		$aData = $aDataObject['data'];
        foreach ($aData as $k => $aRow) {
            foreach ($aRow as $sField => $sValue) {
				if(empty($sValue)) $aData[$k][$sField] = ' ';
			}
		}
        return $aDataObject;
	}
	
}
?>