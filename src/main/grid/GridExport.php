<?php
namespace main\grid;

use main\InvalidParameter;
use main\language\Language;
use main\language\Transliteration;
use main\Util;

// DATA FORMATS
define('DF_STRING', 1);    // 'string'
define('DF_FLOAT', 2);    // 0.000
define('DF_DIGIT', 3);    // 0.00
define('DF_NUMBER', 4);    // 0
define('DF_CURRENCY', 5);    // 0.00 лв
define('DF_PERCENT', 6);    // 0.00%
define('DF_TITLE', 7);        //zaglavie (nai gore)
define('DF_CAPTION', 8);        //zaglavie na kolona
define('DF_TOTAL', 9);        //totali

abstract class GridExport {
    /** @var GridPanelHandler */
    protected $oTarget = null;
    /** @var Export */
    protected $oExport;
    protected $aColumns = array();
    protected $sFileName = '';
    protected $sGridTitle = '';

    private $aOfficeColumnNames = array();

    public function __construct($oExport, $oTarget) {
        $this->oExport = $oExport;
        $this->oTarget = $oTarget;
        if(!($this->oTarget instanceof GridPanelHandler)) throw new InvalidParameter;
        $this->aColumns = $this->oTarget->aColumns;
        if(!empty($this->oExport->aRequest['columns'])) $this->updateColumnState($this->aColumns, $this->oExport->aRequest['columns']);

        $this->sFileName = $this->oTarget->getReportTitle();
        if(empty($this->sFileName) && !empty($this->oExport->aRequest['gridTitle'])) $this->sFileName = $this->oExport->aRequest['gridTitle'];
        $this->sGridTitle = $this->sFileName;
        if(empty($this->sFileName)) $this->sFileName = 'report';
        $this->sFileName = Transliteration::convertCyr2PhoLower($this->sFileName);
    }

    protected function updateColumnState(&$aOrgColumns, &$aClientColumns) {
        //remove checkbox
        foreach ($aClientColumns as $k => $aClientColumn) {
            if($aClientColumn['dataField'] == 'check_box_selection' || $aClientColumn['dataField'] == '__row_index') {
                array_splice($aClientColumns, $k, 1);
            }
            if($aClientColumn['dataField'] == '__row_index') {
                array_unshift($aOrgColumns, (object)$aClientColumn);
            }
        }

        foreach ($aOrgColumns as $k => $oOrgColumn) {
            if(!empty($oOrgColumn->columnListText)) $oOrgColumn->headerText = $oOrgColumn->columnListText;
            foreach ($aClientColumns as $aCColumn) {
                if($aCColumn['dataField'] == $oOrgColumn->dataField) {
                    $aClientColumn = $aCColumn;
                }
            }
            if(empty($aClientColumn)) continue;

            if(!empty($oOrgColumn->children)) {
                $this->updateColumnState($oOrgColumn->children, $aClientColumn['children']);
            } else {
                foreach ($aClientColumn as $kk => $v) {
                    if(in_array($kk, array('width', 'hidden')) && is_object($oOrgColumn)) {
                        $oOrgColumn->$kk = $v;
                    }
                }
            }
        }
    }

    protected function getData() {
        $aRequest = $this->oExport->aRequest;
        $aRequest['paging']['start'] = 0;
        $aRequest['paging']['limit'] = 999999;
        $oResponse = $this->oTarget->loadData($aRequest);

        // Заради тоталите в пдф-а
        if(strtolower($this->oExport->sExportType) == 'pdf') {
            return array('data' => $oResponse->gridData, 'totals' => (isset($oResponse->bShowTotal) ? $oResponse->gridTotals : NULL));
        } else {
            return $oResponse->gridData;
        }
    }

    public static function createEditedByText($aRow, $oColumn) {
        $sCreatedUser = empty($aRow[$oColumn->createdUserField]) ? '' : $aRow[$oColumn->createdUserField];
        $nCreatedTime = Util::SQLDateToTimeStamp($aRow[$oColumn->createdTimeField]);
        $sCreatedTime = empty($nCreatedTime) ? '' : date(Language::getPHPDateTimeFormat(), $nCreatedTime);
        $sUpdatedUser = empty($aRow[$oColumn->updatedUserField]) ? '' : $aRow[$oColumn->updatedUserField];
        $nUpdatedTime = Util::SQLDateToTimeStamp($aRow[$oColumn->updatedTimeField]);
        $sUpdatedTime = empty($nUpdatedTime) ? '' : date(Language::getPHPDateTimeFormat(), $nUpdatedTime);

        $aTxt = array();
        if(!empty($sCreatedUser) || !empty($sCreatedTime)) {
            $sTxt = L("създаден");
            if(!empty($sCreatedTime)) $sTxt .= ' ' . L('на %s', $sCreatedTime);
            if(!empty($sCreatedUser)) $sTxt .= ' ' . L('от %s', $sCreatedUser);
            $aTxt[] = $sTxt;
        }
        if(!empty($sUpdatedTime) || !empty($sUpdatedUser)) {
            $sTxt = L("последно редактиран");
            if(!empty($sUpdatedTime)) $sTxt .= ' ' . L('на %s', $sUpdatedTime);
            if(!empty($sUpdatedUser)) $sTxt .= ' ' . L('от %s', $sUpdatedUser);
            $aTxt[] = $sTxt;
        }

        return implode(', ', $aTxt);
    }

    abstract public function doExport();
}