<?php
namespace main\db;

class DBSQLException extends DBException{
    protected $sql;
    protected $errno;
    protected $error;

    /**
     * @param string $sql
     * @param int $errno
     * @param string $error
     */
    public function __construct($sql,$errno,$error) {
        $this->sql = $sql;
        $this->errno = $errno;
        $this->error = $error;
        parent::__construct('Database error.');
    }

    public function getSQLError() {
        return $this->error;
    }
} 