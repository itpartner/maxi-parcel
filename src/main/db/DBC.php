<?php
namespace main\db;

class DBC {
    /**
     * @var DBConnection
     */
    public static $main = null;
    /**
     * @var DBConnection
     */
    public static $slave = null;
	/**
	 * @var DBConnection
	 */
	public static $suggest = null;
    /**
     * @var DBConnection
     */
    public static $log = null;
    /**
     * @var DBConnection[]
     */
    public static $connections = array();
} 