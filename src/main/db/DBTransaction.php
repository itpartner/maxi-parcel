<?php
/**
 * Created by PhpStorm.
 * UserTemplate: Ismata
 * Date: 10.5.2016 г.
 * Time: 10:53 ч.
 */

namespace main\db;


class DBTransaction {

	private $_commit;
	private $_rollback;

	public function __construct($commit, $rollback) {
		$this->_commit = $commit;
		$this->_rollback = $rollback;
	}

	public function commit() {
		call_user_func($this->_commit);
	}

	public function rollback() {
		call_user_func($this->_rollback);
	}
}