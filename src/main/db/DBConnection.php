<?php
namespace main\db;

use main\Exception;
use main\InvalidParameter;
use main\Util;

class DBConnection {
    /**
     * @var \mysqli
     */
    private $link = null;

    private $DSN;

    private $DBName = null;

    /**
     * @var DBSQLException
     */
    private $connError = null;

    private $failedTrans = false;

    private $transCount = 0;

    /**
     * @var \Memcached
     */
    private $memcache = null;

    public function __construct($DSNURI, \Memcached $memcache = null) {
        $this->DSN = parse_url($DSNURI);
        $this->DBName = basename($this->DSN['path']);
        $this->memcache = $memcache;
    }

    private function checkThrow($sQuery = null) {
        if(!$this->link) throw new DBException("Not connected.");
        if($this->link->errno) {
            throw new DBSQLException($sQuery, $this->link->errno, $this->link->error);
        }
        if($this->link->connect_errno) {
            throw new DBSQLException($sQuery, $this->link->connect_errno, $this->link->connect_error);
        }
    }

    private function connect() {
        if($this->link) return;
        if($this->connError) throw $this->connError;
        try {
            if(!empty($this->DSN)) {

                $this->link = mysqli_init();
                foreach (array(
                             MYSQLI_OPT_CONNECT_TIMEOUT => 3,
                             MYSQLI_OPT_INT_AND_FLOAT_NATIVE => true
                         ) as $opt => $val) {
                    if(!$this->link->options($opt, $val)) {
                        $this->checkThrow();
                        throw new DBException(sprintf("Unable to set option %s:%s.", $opt, $val));
                    };
                }


                if(!$this->link->real_connect('p:' . $this->DSN['host'], $this->DSN['user'], $this->DSN['pass'], $this->DBName, $this->DSN['port'])) {
                    $this->checkThrow();
                    throw new DBException("Unable to connect to server.");
                }

                $this->link->set_charset('utf8');
                $this->checkThrow();
                $this->_Execute("SET NAMES utf8");

                $this->resetTrans();

            } else {
                throw new DBException('DSN is empty.');
            }
        } catch (DBException $e) {
            $this->connError = $e;
            throw $this->connError;
        }
    }

    public function getHost() {
        return $this->DSN['host'];
    }

    private function _Execute($query) {
        $res = $this->link->query($query);
        $this->checkThrow($query);
        return $res;
    }

    public function resetTrans() {
        $this->_rollbackTrans(1);
    }

    public function isConnected() {
        return !!$this->link;
    }

    public function setTimeZone($zoneOffset) {
        if($this->isConnected()) $this->_Execute("SET time_zone = " . $this->quote($zoneOffset));
    }

    private $queryPlaceholderRegex = "/(FROM|JOIN|INTO|REPLACE)(\\s+)([\\da-z_]+)(\\.)/i";

    private function replaceQueryPlaceholders($aMatches) {
        $dbName = $aMatches[3];
        if(!empty(DBC::$$dbName)) {
            return $aMatches[1] . $aMatches[2] . DBC::$$dbName->DBName . $aMatches[4];
        } else {
            return $aMatches[0];
        }
    }

    public function selectObjects($query, $class, $cacheSeconds = null) {
        return array_map(((is_object($class) ? get_class($class) : $class) . '::fromFlatArray'), $this->select($query, $cacheSeconds, 0));
    }

    public function select($query, $cacheSeconds = null, $assocDepth = 0) {
        $result = null;
        $query = preg_replace_callback($this->queryPlaceholderRegex, array($this, 'replaceQueryPlaceholders'), $query);
        if($cacheSeconds) $result = $this->cacheGet(__METHOD__, $query);

        if($result === NULL) {
            $this->connect();
            $rs = $this->_Execute($query);
            $result = array();
            while ($row = $rs->fetch_assoc()) $result[] = $row;

            $result = Util::arrayAssocify($result, $assocDepth);

            if($cacheSeconds) $this->cacheSet(__METHOD__, $query, $result, $cacheSeconds);
        }

        return $result;
    }

    public function selectAssoc($query, $cacheSeconds = null) {
        $arrayResult = $this->select($query, $cacheSeconds);
        if(empty($arrayResult)) return $arrayResult;

        $result = array();
        $firstRowKeys = array_keys(reset($arrayResult));
        if(count($firstRowKeys) <= 2) {
            $keyField = $firstRowKeys[0];
            $valueField = count($firstRowKeys) == 2 ? $firstRowKeys[1] : $firstRowKeys[0];
            foreach ($arrayResult as $row) $result[$row[$keyField]] = $row[$valueField];
        } else {
            $result = Util::arrayAssocify($arrayResult, 1);
        }

        return $result;
    }

    public function selectRow($query, $cacheSeconds = null) {
        $res = $this->select($query, $cacheSeconds);
        return empty($res) ? array() : reset($res);
    }

    public function selectColumn($query, $cacheSeconds = null) {
        $queryResult = $this->select($query, $cacheSeconds);
        $result = array();
        foreach ($queryResult as $aRow) $result[] = reset($aRow);
        return $result;
    }

    public function selectField($query, $cacheSeconds = null) {
        $aColumn = $this->selectColumn($query, $cacheSeconds);
        return empty($aColumn) ? null : reset($aColumn);
    }

    public function selectByID($table, $id, $cacheSeconds = null) {
        $table = $this->escape($table);
        $id = intval($id);
        return $this->selectRow("SELECT * FROM {$table} WHERE id = {$id}", $cacheSeconds);
    }

    public function affectedRows() {
        $this->connect();
        return $this->link->affected_rows;
    }

    public function lastInsertID() {
        return $this->isConnected() ? $this->link->insert_id : null;
    }

    public function insert($tableName, &$data) {
        if($this->multiInsert($tableName, [$data]) && empty($data['id'])) $data['id'] = $this->lastInsertID();
    }

    public function multiInsert($tableName, $data, $mode = 'update', $updateFlag = true) {
        if(empty($data)) return 0;
        $fields = $this->getTableFields($tableName);

        $dataByColumns = array();

        //ako update-vame redovete se razdelqt na otdelni query-ta v zavisimost koi poleta gi ima
        if($mode == 'update') {
            foreach ($data as $row) {
                $columnNames = array();
                foreach ($row as $colName => $mVal) {
                    $colNameLower = strtolower($colName);
                    if(empty($fields[$colNameLower]) || ($mVal === null && !$fields[$colNameLower]['allow_null'])) continue;
                    $columnNames[] = $colNameLower;
                }
                sort($columnNames);
                $columnNamesKey = implode(':', $columnNames);
                if(!array_key_exists($columnNamesKey, $dataByColumns)) $dataByColumns[$columnNamesKey] = array();
                $dataByColumns[$columnNamesKey][] = $row;
            }
        } else {
            $dataByColumns['all'] = $data;
        }

        $affectedRows = 0;
        foreach ($dataByColumns as $data) {
            if($updateFlag) {
                $sTime = date('Y-m-d H:i:s');
                $nUser = defined('IS_ROBOT') && IS_ROBOT ? 1 : (int) $_SESSION['userdata']['id'];
                foreach ($data as $k => $row) {
                    $data[$k]['created_time'] = $data[$k]['updated_time'] = $sTime;
                    $data[$k]['created_user'] = $data[$k]['updated_user'] = $nUser;
                }
            }
            $insertFieldNamesQuoted = array();
            $rowsQuoted = array();
            foreach ($data as $row) {
                $rowValuesQuoted = array();
                foreach ($row as $fieldName => $value) {
                    $fieldNameLower = strtolower($fieldName);
                    if(empty($fields[$fieldNameLower])) continue;
                    if(empty($insertFieldNamesQuoted[$fieldNameLower])) $insertFieldNamesQuoted[$fieldNameLower] = '`' . $fields[$fieldNameLower]['name'] . '`';
                    if($value === NULL) {
                        $rowValuesQuoted[$fieldNameLower] = NULL;
                    } else {
                        if(!is_scalar($value)) continue;
                        switch ($fields[$fieldNameLower]['type']) {
                            case 'tinyint':
                            case 'smallint':
                            case 'mediumint':
                            case 'bigint':
                            case 'int':
                            case 'float':
                            case 'double':
                            case 'decimal':
                                if(is_numeric($value)) $rowValuesQuoted[$fieldNameLower] = $value;
                                else $rowValuesQuoted[$fieldNameLower] = $this->quote($value);
                                break;

                            case 'set':
                            case 'enum':
                            case 'char':
                            case 'varchar':
                            case 'tinytext':
                            case 'text':
                            case 'mediumtext':
                            case 'longtext':
                            case 'blob':
                            case 'year':
                            case 'json':
                                $rowValuesQuoted[$fieldNameLower] = $this->quote($value);
                                break;

                            case 'date':
                                $rowValuesQuoted[$fieldNameLower] = $this->quote(is_numeric($value) ? date("Y-m-d", $value) : $value);
                                break;

                            case 'time':
                                $rowValuesQuoted[$fieldNameLower] = $this->quote(is_numeric($value) ? date("H:i:s", $value) : $value);
                                break;

                            case 'datetime':
                            case 'timestamp':
                                $rowValuesQuoted[$fieldNameLower] = $this->quote(is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value);
                                break;

                            default:
                                throw new DBException(sprintf("Unknown field type (%s).", $fields[$fieldNameLower]['type']));
                        }
                    }
                }
                $rowsQuoted[] = $rowValuesQuoted;
            }
            $SQLRows = array();
            foreach ($rowsQuoted as $rowQuoted) {
                if(empty($rowQuoted)) continue;
                $tmpRow = array();
                foreach ($insertFieldNamesQuoted as $fieldNameLower => $SQLFieldName) {
                    if(!array_key_exists($fieldNameLower, $rowQuoted)) {
                        $tmpRow[] = 'default';
                    } elseif($rowQuoted[$fieldNameLower] === NULL) {
                        $tmpRow[] = $fields[$fieldNameLower]['allow_null'] ? 'null' : 'default';
                    } else {
                        $tmpRow[] = $rowQuoted[$fieldNameLower];
                    }
                }
                if(empty($tmpRow)) continue;
                $SQLRows[] = '(' . implode(',', $tmpRow) . ')';
            }

            if(empty($SQLRows)) return 0;

            if($mode == 'replace') $query = "REPLACE INTO";
            elseif($mode == 'ignore') $query = "INSERT IGNORE INTO";
            elseif($mode == 'delayed') $query = "INSERT DELAYED INTO";
            else $query = "INSERT INTO";

            $query .= " " . $tableName . " ";
            $query .= " (" . implode(',', $insertFieldNamesQuoted) . ") VALUES \n";
            $query .= implode(",\n", $SQLRows) . "\n";

            if($mode == 'update') {
                $updateFields = array();
                foreach ($insertFieldNamesQuoted as $fieldNameLower => $fieldName) {
                    if($updateFlag && ($fieldNameLower == 'created_time' || $fieldNameLower == 'created_user')) continue;
                    $updateFields[] = "$fieldName = VALUES($fieldName)";
                }
                $query .= "ON DUPLICATE KEY UPDATE " . implode(',', $updateFields) . " \n";
            }

            $this->execute($query);
            $affectedRows += $this->affectedRows();
        }
        return $affectedRows;
    }

    private $tableFieldsCache = array();

    public function getTableFields($tableName) {
        $this->connect();
        if(!preg_match("/^[a-zA-Z\\d_\\.]+$/", $tableName)) throw new DBException("Invalid table name.");
        if(empty($this->tableFieldsCache[$tableName])) {
            $cacheKey = $this->getHost() . '-' . $tableName;
            $fields = $this->cacheGet(__METHOD__, $cacheKey);
            if(!$fields) {
                $SQLFields = $this->select("SHOW FIELDS FROM $tableName");
                $fields = array();
                foreach ($SQLFields as $SQLField) {
                    $field = array();
                    $matches = array();
                    preg_match("/^([a-zA-Z]+).*/", $SQLField['Type'], $matches);
                    $field['type'] = strtolower($matches[1]);
                    $field['allow_null'] = $SQLField['Null'] == 'YES';
                    $field['has_default'] = $SQLField['Default'] === NULL;
                    $field['name'] = $SQLField['Field'];
                    $fields[strtolower($field['name'])] = $field;
                }
                $this->cacheSet(__METHOD__, $cacheKey, $fields, 600);
            }
            $this->tableFieldsCache[$tableName] = $fields;
        }

        return $this->tableFieldsCache[$tableName];
    }

    private function addDebugInfoToQuery($sQuerey) {
        if(defined('DEBUG_BACKTRACE_IGNORE_ARGS'))
            foreach (array_slice(@debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS), 1) as $aTraceItem) {
                if(!empty($aTraceItem['file'])) {
                    $sQuerey .= "#" . preg_replace("/[^a-zA-Z0-9.\\-_\\/\\\\]/", '', $aTraceItem['file']) . ':' . $aTraceItem['line'] . "\n";
                }
            }

        return $sQuerey;
    }

    public function escape($sValue) {
        $this->connect();
        return $this->link->real_escape_string($sValue);
    }

    public function quote($sValue) {
        return "'" . $this->escape($sValue) . "'";
    }

    /**
     * @param string $query
     */
    public function execute($query) {
        $this->connect();
        $query = $this->addDebugInfoToQuery($query);
        $this->_Execute($query);
    }

    public function withTrans(Callable $func, array $args = array()) {
        $this->startTrans();
        $c = $this->transCount;
        try {
            $r = call_user_func_array($func, $args);
            $this->completeTrans($c);
            return $r;
        } catch (Exception $e) {
            $this->_rollbackTrans($c);
            throw $e;
        }
    }

    public function startTrans() {
        $this->connect();
        $c = ++$this->transCount;
        try {
            if($this->transCount == 1) {
                $this->execute("SET AUTOCOMMIT=0");
                $this->execute("BEGIN");
            }
            $this->execute("SAVEPOINT t" . ($this->transCount));
        } catch (\Exception $e) {
            $this->failedTrans = true;
            throw $e;
        }
        return new DBTransaction(
            function () use ($c) {
                $this->completeTrans($c);
            },
            function () use ($c) {
                $this->_rollbackTrans($c);
            }
        );
    }

    private function completeTrans($c) {
        if(!$this->transCount) return;
        if($c <= 0) throw new DBException("Invalid trans level.");
        if($c != $this->transCount) throw new DBException("Missed complete trans.");
        $this->transCount--;
        if(!$this->transCount) {
            if($this->failedTrans) {
                unset($this->cache[1]);//tunkajno zabursvame cache-a predi da se e zapisal
                $this->execute("ROLLBACK");
            } else {
                $this->execute("COMMIT");
            }
            $this->execute("SET AUTOCOMMIT = 1");
            $this->failedTrans = false;
        }

        $this->mergeCache($this->transCount);
    }

    /**
     * merge-va cache-a s 1 nivo ot tranzakciqta. ako nivoto e 0 znachi commit-vame i go zapisva v memcache
     * @param $nTransCount
     */
    private function mergeCache($nTransCount) {
        if($this->cache[$nTransCount + 1]) foreach ($this->cache[$nTransCount + 1] as $sKey => $aVal) {//cache schemi
            $this->cache[$nTransCount][$sKey] = $aVal;
            if(!$nTransCount && $this->memcache) $this->memcache->set($sKey, $aVal, $aVal['expireSeconds']);
        }
        unset($this->cache[$nTransCount + 1]);
    }

    private function _rollbackTrans($c) {
        if(!$this->transCount) return;
        if($c <= 0) throw new DBException("Invalid trans level.");
        while ($c <= $this->transCount) {
            if($this->transCount == 1) {
                $this->failedTrans = true;
                $this->completeTrans(1);
            } else {
                unset($this->cache[$this->transCount]);//cache schemi
                $this->transCount--;
                try {
                    $this->execute("ROLLBACK TO SAVEPOINT t" . ($this->transCount + 1));
                    $this->execute("RELEASE SAVEPOINT t" . ($this->transCount + 1));
                } catch (\Exception $e) {
                    $this->failedTrans = true;
                    throw $e;
                }
            }
        }
    }

    public function update($tableName, &$data, $updateFlag = true) {
        if(empty($data) || !is_array($data)) throw new Exception();
        if($this->multiInsert($tableName, array($data), 'update', $updateFlag) && empty($data['id'])) $data['id'] = $this->lastInsertID();
    }

    public function delete($tableName, $id) {
        if (!($id = intval($id))) throw new InvalidParameter();

        $tableFields = $this->getTableFields($tableName);

        if(empty($tableFields['to_arc'])) {
            $this->execute("DELETE FROM {$tableName} WHERE id = {$id} LIMIT 1");
            return;
        }

        $data = array('id' => $id, 'to_arc' => 1);
        $this->update($tableName, $data);
    }

    //cache-a e tuka zashtoto e svurzan s tranzakciite. inache nqma nishto obashto s DB
    /**
     * cache[0] e commit-natiq cache 1,2,3... e po savepoint-i
     * @var array
     */
    private $cache = array();

    public function cacheSet($name, $key, $value, $expire = NULL) {
        $sKey = $name . ':' . self::cacheGetHash($key);
        if(self::$cacheExpireTimes[$expire]) $expire = self::$cacheExpireTimes[$expire];
        if($expire = intval($expire)) {
            $val = array('value' => $value, 'time' => time(), 'expireSeconds' => $expire, 'expireTime' => time() + $expire);
            $this->cache[$this->transCount][$sKey] = $val;
            if(!$this->transCount && $this->memcache) $this->memcache->set($sKey, $val, $val['expireSeconds']);//nqma tranzakciq i se zapisva directno
        }
    }

    public function cacheGet($name, $key) {
        $sKey = $name . ':' . self::cacheGetHash($key);

        $val = null;
        $i = empty($this->cache) ? 0 : max(array_keys($this->cache));
        while ($i-- > 0 && !$val) if($this->cache[$i] && array_key_exists($sKey, $this->cache[$i])) $val = $this->cache[$i][$sKey];//izdirvame localno
        if(!$val) $this->cache[0][$sKey] = $val = $this->memcache ? $this->memcache->get($sKey) : null;//ako go nqma izdirvame v memcache

        if($val['expireTime'] < time()) return null;
        return $val['value'];
    }

    public static function cacheGetHash() {
        return md5(md5(BASE_URL).serialize(self::cacheGetHashArray(func_get_args())));
    }

    private static function cacheGetHashArray($var) {
        if(is_object($var)) $var = (array)$var;
        if(is_array($var)) foreach ($var as $k => $v) {
            if($v === null) continue;
            else if(is_scalar($v)) $var[$k] = (string)$v;
            else $var[$k] = self::cacheGetHashArray($v);
        }
        ksort($var);
        return $var;
    }

    private static $cacheExpireTimes = array(
      	'settings' => 60
    );
}