<?php

namespace main\logs;

use main\daemons\BaseRobot;
use main\db\DBC;

class RobotArchiveLogs extends BaseRobot {

    public function run($nTime, $aParams = array()) {
        $sYear = date('Y');
        $sLogsNew = uniqid('logs_tmp_new_');
        $sLogsOld = uniqid('logs_tmp_old_');

        DBC::$log->execute("CREATE TABLE IF NOT EXISTS logs_archive_$sYear LIKE logs_archive_origin");
        DBC::$log->execute("CREATE TABLE $sLogsNew LIKE logs_current");

        DBC::$log->execute("ALTER TABLE $sLogsNew AUTO_INCREMENT = " . DBC::$log->selectField("
            SELECT CONCAT('',GREATEST(
                DATE_FORMAT(NOW(),'%Y0000')*10000000000,
                COALESCE((SELECT MAX(id) FROM logs_current),0) + 10000
            ))
        "));

        DBC::$log->execute("RENAME TABLE logs_current TO $sLogsOld, $sLogsNew TO logs_current");

        DBC::$log->execute("
            INSERT INTO log_targets (target)
            SELECT DISTINCT l.target
            FROM $sLogsOld l
            LEFT JOIN log_targets lt ON lt.target = l.target
            WHERE lt.target IS NULL
        ");

        DBC::$log->execute("
            INSERT INTO logs_archive_$sYear
            SELECT
                l.id,
                l.type,
                l.id_user,
                COALESCE(INET_ATON(l.ip),0),
                COALESCE(INET_ATON(l.server),0),
                lt.id,
                duration,
                cpu_time,
                session_id,
                created_time
            FROM $sLogsOld l
            LEFT JOIN log_targets lt ON lt.target = l.target
        ");

        DBC::$log->execute("DROP TABLE $sLogsOld");
        DBC::$log->execute("DELETE FROM user_history WHERE created_time < DATE_SUB(NOW(), INTERVAL 6 MONTH)");
        DBC::$main->execute("DELETE FROM robot_tasks WHERE created_time < DATE_SUB(NOW(), INTERVAL 6 MONTH)");
    }
}