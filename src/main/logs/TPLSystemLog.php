<?php

namespace main\logs;
use main\grid\TPLGridPanel;
use main\language\Language;

class TPLSystemLog extends TPLGridPanel {

    public function __construct ( $sPrefix, $aInitParams = array() ) {

        $this->aConfig      = array(
            'title'         => L('Логове на системата')    ,
            'autoVResize'   => true ,
            'hideRowIndexes' => true,
            'topBarItems'   => array(
                array('label', array('text' => L('от:'))),
                array('input', array('class' => 'dateTimePicker', 'name' => 'time_from', 'value' => date(Language::getPHPDateTimeFormat(),strtotime("-5 minutes")))),
                array('label', array('text' => L('до:'))),
                array('input', array('class' => 'dateTimePicker', 'name' => 'time_to', 'value' => date(Language::getPHPDateTimeFormat(),strtotime("+30 minutes")))),
                array('button',array('class' => 'btn-search','iconCls' => 'icon fa fa-search' )),
                array('filtersButton',array('align' => 'center')),
                array('columnsButton',array('align' => 'center')),
            ),
            'bottomBarItems'=> array(
                array( 'exportbutton'   , array( 'align' => 'right' ) )
            )
        );

        if(!empty($aInitParams['error']['log_id'])) {
            $this->aConfig['topBarItems'] = $this->aConfig['bottomBarItems'] = array();
            $this->{'bNoDefaultTopBarItems'} = true;
            $this->aConfig['disablePaging'] = true;
        }

        return parent::__construct( $sPrefix, $aInitParams );
    }

    protected function getJSFunction() { return <<<'JS'

        (function(container,prefix,initParams){
            var form = container;
            var jForm = jQuery(form);
            jForm.find('.btn-search').click(function(){
                form.grid.loadData();
            });
            form.grid.addListener('beforeSetData',function(data){
                if (data && data.length) for(var i=0;i < data.length; i++) {
                    data[i].dataToolTip = '<div style="max-width:450px;max-height: 350px;overflow: hidden;"><pre style="margin: 0;">'+data[i].data+'</pre></div>';
                }
            });
            form.grid.clickListeners.info = function(rowIndex,rowData,cell,event) {
                var created = '';
                try {
                    var date = jQuery.datepicker.parseDate( 'yy-mm-dd hh:ii:ss', rowData.created_time );
                    var format = framework.getDateTimeFormat();
                    created = jQuery.datepicker.formatDate( format, date );
                } catch(e){
                }
                framework.createWindow({
                    title:created + ' - ' + rowData.target,
                    maxWidth:jQuery(window).width() - 150,
                    maxHeight:jQuery(window).height() - 150,
                    content:'<pre style="margin: 0 20px 0 0;">'+rowData.data+'</pre>'
                });
            };

            form.grid.addListener('afterSetData',function(){
                if(initParams['error']){
                   form.window.close();
                   form.grid.clickListeners.info(null,form.grid.getCurrentData()[0]);
                }
            });
        });

JS;
    }


}