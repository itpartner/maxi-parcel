<?php
namespace main\logs;
use main\db\DBC;
use main\grid\DBGridFilters;
use main\grid\GridPanelHandler;

/**
 * Class SystemLog
 * @package main\logs
 * @accessLevelDescription Достъп до логовете на системата
 */
class SystemLog extends GridPanelHandler {

    public function __construct( $aParams = array() ) {
        $this->bUseSQLPaging = false;
        $this->bUseSQLSort = false;
        $this->aColumns[] = $this->newColumn(array(
            'type' => self::CTYPE_INT ,
            'dataField' => 'l.id',
            'headerText' => '#',
            'width' => 125,
            'filterable' => true,
        ));
        $this->aColumns[] = $this->newColumn(array(
            'type' => self::CTYPE_DATETIME ,
            'dataField' => 'l.created_time',
            'headerText' => L('Време'),
            'width' => 115,
            'filterable' => true,
        ));
        $this->aColumns[] = $this->newColumn(array(
            'type' => self::CTYPE_ENUM ,
            'dataField' => 'l.type',
            'items' => array(
                'error' => array('text' => L('грешка')),
                'debug' => array('text' => L('лог / дебъг')),
                'request' => array('text' => L('заявка към сървъра')),
                'login' => array('text' => L('вход в системата')),
                'logout' => array('text' => L('изход от системата')),
                'user_history' => array('text' => L('потребителско действие')),
            ),
            'headerText' => L('Тип'),
            'width' => 140,
            'style' => array('textAlign' => 'center'),
            'filterable' => true,
        ));
        $this->aColumns[] = $this->newColumn(array(
            'type' => self::CTYPE_STRING ,
            'dataField' => 'full_name',
            'headerText' => L('Потребител'),
            'width' => 133,
            'filterable' => true,
        ));
        $this->aColumns[] = $this->newColumn(array(
            'type' => self::CTYPE_INT ,
            'dataField' => 'l.id_user',
            'headerText' => L('ID на потребител'),
            'width' => 60,
            'filterable' => true,
        ));
        $this->aColumns[] = $this->newColumn(array(
            'type' => self::CTYPE_STRING ,
            'dataField' => 'ip',
            'headerText' => 'IP',
            'width' => 95,
            'filterable' => true,
        ));
        $this->aColumns[] = $this->newColumn(array(
            'type' => self::CTYPE_STRING ,
            'dataField' => 'server',
            'headerText' => L('Сървър'),
            'width' => 95,
            'filterable' => true,
        ));
        $this->aColumns[] = $this->newColumn(array(
            'type' => self::CTYPE_STRING ,
            'dataField' => 'target',
            'headerText' => L('Файл'),
            'width' => 400,
            'filterable' => true,
        ));
        $this->aColumns[] = $this->newColumn(array(
            'type' => self::CTYPE_BTN,
            'iconCls' => 'icon fa fa-info-circle',
            'dataField' => 'info',
            'toolTipField' => 'dataToolTip',
            'headerText' => L('Данни'),
            'width' => 66,
            'sortable' => false,
            'filterable' => false,
        ));
        $this->aColumns[] = $this->newColumn(array(
            'type' => self::CTYPE_STRING ,
            'dataField' => 'l.session_id',
            'headerText' => L('Сесия'),
            'width' => 160,
            'filterable' => true,
        ));


        $this->sDateFromField = 'time_from';
        $this->sDateToField = 'time_to';
        parent::__construct();
    }

    public function init($aRequest = array(), $aInitParams = array()) {
        if(!empty($aInitParams['error']['log_id'])){
            $aRequest['filterItems']['id'] = [
                'type' => 'integer',
                'dataField' => 'id',
                'compareType' => 'eq',
                'compareValue' => $aInitParams['error']['log_id']
            ];
        }
        return $this->loadData($aRequest);
    }

    protected function getGridData($aRequest = array(), $nTimeFrom = 0, $nTimeTo = 0) {
        $aData = array();

        $sTimeFrom = date('Y-m-d H:i:s',$nTimeFrom);
        $sTimeTo = date('Y-m-d H:i:s',$nTimeTo);

        $aWhere = array("l.created_time BETWEEN '$sTimeFrom' AND '$sTimeTo'");
        if(!empty($aRequest['filterItems']['id'])) $aWhere = array(1);
        $aHaving = array(1);

        //quick filter
        if(!empty($aRequest['filterItems']) && is_array($aRequest['filterItems'])) {
            if(empty($this->aColumnsAssoc)) $this->aColumnsAssoc = $this->getColumnsAssoc();
            foreach ($aRequest['filterItems'] as $aFilterItem) {
                if(empty($aFilterItem)) continue;
                $aStatement = $this->createFilterItemStatement($aFilterItem);
                if($aStatement['type'] == 'where') {
                    $aWhere[] = $aStatement['statement'];
                } else if($aStatement['type'] == 'having') {
                    $aHaving[] = $aStatement['statement'];
                }
            }
        }

        //normal filter
        if(!empty($aRequest['filter']) && !empty($aRequest['filter']['id'])) {
            $aFilterStatemens = $this->createFilterStatements(DBGridFilters::getFilterByID($aRequest['filter']['id']));
            if(!empty($aFilterStatemens['where'])) {
                $aWhere = array_merge($aFilterStatemens['where'], $aWhere);
            }
            if(!empty($aFilterStatemens['having'])) {
                $aHaving = array_merge($aFilterStatemens['having'], $aHaving);
            }
        }

        $sWhere = implode(' AND ', $aWhere);
        $sHaving = implode(' AND ', $aHaving);

        $aData = array_merge($aData, DBC::$log->select("
            SELECT
                l.*,
                CONCAT_WS(' ',p.fname,p.mname,p.lname) as full_name
            FROM log.logs_current l
            LEFT JOIN main.personnel p ON p.id = l.id_user
            WHERE $sWhere
            HAVING $sHaving
        "));

        return $aData;
    }

    protected function setGridData($aData, $aRequest = array(), $nTimeFrom = 0, $nTimeTo = 0) {
        foreach($aData as $k => $aRow) {
            if($aRow['data']) {
                ob_start();
                var_dump(unserialize($aRow['data']));
                $aData[$k]['data'] = ob_get_clean();
            }
        }
        return parent::setGridData($aData);
    }
}