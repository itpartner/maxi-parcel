<?php
namespace main\logs;
use main\db\DBC;
use main\Exception;
use main\Settings;
use main\System;

class Logs
{
    /**
	 * @var DBLogs
	 */
    public static $oDBLogs = null;

    private static $aUserLogTypes = array('user_history','login','logout');
    public static function insertLog($aRow) {
        try {
            if(in_array($aRow['type'],self::$aUserLogTypes)) DBC::$log->multiInsert('user_history', array($aRow), 'insert');
            else DBC::$log->multiInsert('logs_current', array($aRow), 'insert');
            return DBC::$log->lastInsertID();
        } catch (Exception $e) {

        }
        return 0;
    }

    public static function logRequest($sTarget, $mRequest, $mResponse,$nDuration = 0,$nCPUTime = 0)
    {
        if (
        (!isset(Settings::$aSettings['SYSTEM']['log_requests']) || empty(Settings::$aSettings['SYSTEM']['log_requests'][0]))
        ) return 0;
        $aLogItem = array(
            'type' => 'request',
            'ip' => $_SERVER['REMOTE_ADDR'],
            'server' => $_SERVER['SERVER_ADDR'],
            'referer' => !empty(System::$aGetParams['referer']) ? System::$aGetParams['referer'] : null,
            'target' => $sTarget,
            'duration' => $nDuration,
            'cpu_time' => $nCPUTime,
            'data' => serialize(array(
                'request' => $mRequest,
                'response' => $mResponse
            ))
        );
        if (session_id()) {
            $aLogItem['session_id'] = session_id();
        }

        if (isset($_SESSION['userdata']) && isset($_SESSION['userdata']['id'])) $aLogItem['id_user'] = $_SESSION['userdata']['id'];
        if (defined('IS_ROBOT') && IS_ROBOT) $aLogItem['id_user'] = 1;

        return self::insertLog($aLogItem);
    }

    /**
     * @param \Exception $oException
     * @return int
     */
    public static function logException($oException)
    {
        if (
        (!isset(Settings::$aSettings['SYSTEM']['log_errors']) || empty(Settings::$aSettings['SYSTEM']['log_errors'][0]))
            #&& !(isset($_SESSION['userdata']) && isset($_SESSION['userdata']['debug']) && !empty($_SESSION['userdata']['debug']) )
        ) return 0;
        $sTarget = $oException->getFile() . ' line ' . $oException->getLine();

        if(!empty($oException->bIsLogError)) return 0;
        if(isset($oException->nLogID)) return $oException->nLogID;

        $aLogItem = array(
            'type' => 'error',
            'ip' => $_SERVER['REMOTE_ADDR'],
            'server' => $_SERVER['SERVER_ADDR'],
            'referer' => System::$aGetParams['referer'],
            'target' => $sTarget,
            'data' => serialize(self::getExceptionLogObject($oException))
        );
        if (session_id()) {
            $aLogItem['session_id'] = session_id();
        }

        if (isset($_SESSION['userdata']) && isset($_SESSION['userdata']['id'])) $aLogItem['id_user'] = $_SESSION['userdata']['id'];
        if (defined('IS_ROBOT') && IS_ROBOT) $aLogItem['id_user'] = 1;

        $nID = self::insertLog($aLogItem);
        if($nID) $oException->{'logID'} = (string) $nID;
        else $oException->{'logID'} = '0';
        return $nID;
    }

    public static function log($mData)
    {
        if (
        (!isset(Settings::$aSettings['SYSTEM']['log_debug']) || empty(Settings::$aSettings['SYSTEM']['log_debug'][0]))
            #&& !(isset($_SESSION['userdata']) && isset($_SESSION['userdata']['debug']) && !empty($_SESSION['userdata']['debug']) )
        ) return 0;
        $aTrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        $aTrace = $aTrace[0];
        $sTarget = $aTrace['file'] . ' line ' . $aTrace['line'];
        $aLogItem = array(
            'type' => 'debug',
            'ip' => $_SERVER['REMOTE_ADDR'],
            'server' => $_SERVER['SERVER_ADDR'],
            'referer' => System::$aGetParams['referer'],
            'target' => $sTarget,
            'data' => serialize($mData)
        );
        if (session_id()) {
            $aLogItem['session_id'] = session_id();
        }

        if (isset($_SESSION['userdata']) && isset($_SESSION['userdata']['id'])) $aLogItem['id_user'] = $_SESSION['userdata']['id'];
        if (defined('IS_ROBOT') && IS_ROBOT) $aLogItem['id_user'] = 1;

        return self::insertLog($aLogItem);
    }

    public static function logLogin($mData)
    {
        $aLogItem = array(
            'type' => 'login',
            'ip' => $_SERVER['REMOTE_ADDR'],
            'server' => $_SERVER['SERVER_ADDR'],
            'referer' => System::$aGetParams['referer'],
            'target' => $mData['target'],
            'data' => serialize($mData)
        );
        if (session_id()) {
            $aLogItem['session_id'] = session_id();
        }

        if (isset($_SESSION['userdata']) && isset($_SESSION['userdata']['id'])) $aLogItem['id_user'] = $_SESSION['userdata']['id'];
        if (defined('IS_ROBOT') && IS_ROBOT) $aLogItem['id_user'] = 1;

        return self::insertLog($aLogItem);
    }

    public static function logUncaughtException($e) {
        $aMsg = array();
        $aMsg[] = "Uncaught exception ".get_class($e)." with message .'".$e->getMessage()."' at ".$e->getFile()." line ".$e->getLine().".";
        $aMsg[] = "Server: ".`hostname`;
        $aMsg[] = "Trace:";
        foreach($e->getTrace() as $k => $aTraceRow) {
            $aMsg[] = "$k: {$aTraceRow['file']} {$aTraceRow['line']};";
        }
        error_log(implode("\n",$aMsg));
        error_log("Log ID: ".self::logException($e));
    }

    public static function getExceptionLogObject($e) {
        if($e instanceof Exception) {
            return $e->getLogObject();
        } else {
            $oObj = (object)get_object_vars($e);
            $aTrace = $e->getTrace();
            foreach ($aTrace as $k => $aItem) {
                unset($aTrace[$k]['args']);
            }
            $oObj->trace = $aTrace;
            return $oObj;
        }
    }

    private static $aDebugTimers = array();
    private static $nDebugTimer = null;

    public static function startDebugTimer($info = null) {
        if(self::$nDebugTimer === null) self::$nDebugTimer = microtime(true);
        $aTrace = @debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        $t = count(self::$aDebugTimers);
        self::$aDebugTimers[$t] = array(
            'start' => array(
                'name' => $info,
                'file' => $aTrace[0]['file'],
                'line' => $aTrace[0]['line'],
                'function' => $aTrace[1]['function'],
                'class' => $aTrace[1]['class'],
                'time' => microtime(true) - self::$nDebugTimer,
            )
        );
        return $t;
    }

    public static function stopDebugTimer($t, $info = null) {
        $time = microtime(true) - self::$nDebugTimer;
        if(!self::$aDebugTimers[$t] || self::$aDebugTimers[$t]['end']) return;
        $aTrace = @debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        self::$aDebugTimers[$t]['end'] = array(
            'name' => $info,
            'file' => $aTrace[0]['file'],
            'line' => $aTrace[0]['line'],
            'function' => $aTrace[1]['function'],
            'class' => $aTrace[1]['class'],
            'time' => $time,
        );
        self::$aDebugTimers[$t]['duration'] = self::$aDebugTimers[$t]['end']['time'] - self::$aDebugTimers[$t]['start']['time'];
    }

    public static function flushDebugTimers($info = null) {
        if(!empty(self::$aDebugTimers)) Logs::log(array(
            'requestInfo' => $info,
            'timers' => self::$aDebugTimers
        ));
        self::$aDebugTimers = array();
        self::$nDebugTimer = microtime(true);
    }

    private static $nSlowQueryTime = null;
    public static function setSlowQueryTime($nSlowQueryTime) {
        if($nSlowQueryTime === null) self::$nSlowQueryTime = null;
        else self::$nSlowQueryTime = doubleval($nSlowQueryTime);
    }
    public static function logSlowQuery($nDuration, $sQuery) {
        if(self::$nSlowQueryTime === null) return;
        $nDuration = doubleval($nDuration);
        if($nDuration >= self::$nSlowQueryTime) {
            Logs::log(array(
                'duration' => $nDuration,
                'query' => $sQuery,
                'trace' => @debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS)
            ));
        }
    }
}
