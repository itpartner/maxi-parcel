<?php
namespace main;

use main\db\DBC;

class FileUpload {
    /** @var FileUpload */
    private static $instance;
    private $device;

    function __construct() {
        $this->device = DBC::$slave->selectRow("SELECT * FROM file_devices WHERE is_default = 1");
    }

    protected function instance(){
        if(self::$instance) return self::$instance;
        self::$instance = new self();
        return self::$instance;
    }

    public static function uploadFile($file) {
        return DBC::$main->withTrans(function($file){
            $device = self::instance()->device;

            if(!empty($file['data'])) $data = $file['data'];
            else if(!empty($file['dataURL'])) $data = base64_decode(substr(end(explode(';', $file['dataURL'], 2)), 7));
            if(empty($data)) throw new InvalidParameter('Empty file content');

            $stream_context = stream_context_create(array('ftp' => array('overwrite' => true)));
            $hash = md5($data);
            $ext = pathinfo($file['name'], PATHINFO_EXTENSION);

            if(false === ($r = file_put_contents($device['upload_url'].'/'.$hash.'.'.$ext, $data, null, $stream_context))) throw new Exception("Неуспешно качване на файл.");

            $row = array(
                'file'      => $hash,
                'name'      => $file['name'],
                'ext'       => $ext,
                'type'      => $file['type'],
                'id_device' => $device['id'],
            );
            DBC::$main->multiInsert('files', array($row));

            return $row;
        }, array($file));
    }

}