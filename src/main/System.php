<?php
namespace main;
class System {

	public static $aGetParams = array();
	private static $aIPs = array(
		'127.0.0.0/24',
		'192.168.0.0/24',
		'192.168.1.0/24',
    );

    public static function isReal() {
        return !empty(reset(Settings::$aSettings['SYSTEM']['is_real']));
    }

	public static function isLocalIP($sIP=NULL)	{
		$sIP = $sIP===NULL ? $_SERVER['REMOTE_ADDR'] : $sIP;
		return Util::checkIP($sIP, self::$aIPs);
	}

	public static function isLoggedIn() {
		return Access::isLoggedIn();
	}

	public static function getHMACKey() {
		return '9!Hv@X/M~g>1-=4EEQILUI7|9{)3|imH8!j5x.PEJ4=oi#)Li210+_g64)~/Vel';
	}

	public static function genHMAC($aData) {
		if(empty($aData)) throw new InvalidParameter();
		foreach($aData as $k => $value) {
			$aData[$k] = (string)$value;
		}
		ksort($aData);
		return hash_hmac('sha1',serialize($aData),self::getHMACKey());
	}

	public static function addHMAC(&$aData) {
		$aData['_hmac'] = self::genHMAC($aData);
	}

	public static function verifyHMAC($aData,$aKeysToIgnore = array()) {
		foreach ($aKeysToIgnore as $sKey) unset($aData[$sKey]);
		$sHMAC = $aData['_hmac'];
		unset($aData['_hmac']);
		return $sHMAC === self::genHMAC($aData);
	}

	/**
	 * po masiv s parametri syzdava query za URL s dopulnitelen key za da se razbere dali e promenqno
	 *
	 * @param array $aParams - parametri pr. array('key1' => 'value1', 'key2' => 'value2')
	 * @return string - pr. key1=value1&key2=value2&_key=132f26aa961ec656009aaaa15f81521d72b67225
	 */
	public static function createProtectedURLParams($aParams) {
		$aParams['_key'] = self::genHMAC($aParams);
		return http_build_query($aParams,'','&');
	}

	/**
	 * proverqva dali da promenqni parametrite na URL-a suzdaden sys createProtectedURLParams
	 *
	 * @param array $aParams parametrite
	 * @param array $aKeysToIgnore - imenata na dopulnitelni parametri koito trqbva da se ignorirat pr. array('lang')
	 * @return boolean
	 */
	public static function checkProtectedURLParams($aParams, $aKeysToIgnore = array()) {
		foreach ($aKeysToIgnore as $sKey) unset($aParams[$sKey]);
		$sHashKey = $aParams['_key'];
		unset($aParams['_key']);

        return self::genHMAC($aParams) === $sHashKey;
	}

	public static function getCacheHash() {
		$var = func_get_args();
		return md5(serialize(self::getCacheHashArray($var)));
	}
	private static function getCacheHashArray($var) {
		if(is_object($var)) $var = (array) $var;
		if(is_array($var)) {
			foreach ($var as $k => $v) {
				if($v === null) continue;
				else if(is_scalar($v)) $var[$k] = (string) $v;
				else $var[$k] = self::getCacheHashArray($v);
			}
			ksort($var);
		}
		return $var;
	}

	public static function getTempDir() {
		$sDir = sys_get_temp_dir();
		if(substr($sDir,-1,1) != '\\' && substr($sDir,-1,1) != '/') {
			$sDir.='/';
		}
		return $sDir;
	}

}
