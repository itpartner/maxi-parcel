<?php
namespace main\notifications;

use main\Exception;
use main\Settings;

require_once BASE_PATH . '/src/swiftmailer-5.x/swift_required.php';

class Mailer {

    public static function send(array $to, $subject = null, $body = null, array $from = null, array $replyTo = null, array $attachments = null) {
        return call_user_func_array('self::' . (mb_strtolower(trim(Settings::$aSettings['Mailer']['transport'][0])) !== 'smtp' ? 'sendViaMail' : 'sendViaSmtp'), func_get_args());
    }

    /** @var \Swift_Mailer */
    private static $mailMailer;

    public static function sendViaMail(array $to, $subject = null, $body = null, array $from = null, array $replayTo = null, array $attachments = null) {
        if(!self::$mailMailer) self::$mailMailer = new \Swift_Mailer(new \Swift_MailTransport());

        return call_user_func_array('self::sendMessage', array_merge(array(self::$mailMailer), func_get_args()));
    }

    /** @var \Swift_Mailer */
    private static $smtpMailer;

    public static function sendViaSmtp(array $to, $subject = null, $body = null, $from = null, $replyTo = null, array $attachments = null) {
        if(!self::$smtpMailer) {
            $smtpTransport = new \Swift_SmtpTransport(Settings::$aSettings['Mailer']['smtp']['host'], Settings::$aSettings['Mailer']['smtp']['port'], Settings::$aSettings['Mailer']['smtp']['security']);
            if(($smtpUser = Settings::$aSettings['Mailer']['smtp']['user'])) {
                $smtpTransport->setUsername($smtpUser);
                if(($smtpPass = Settings::$aSettings['Mailer']['smtp']['pass'])) $smtpTransport->setPassword($smtpPass);
            }
            self::$smtpMailer = new \Swift_Mailer($smtpTransport);
        }

        return call_user_func_array('self::sendMessage', array_merge(array(self::$smtpMailer), func_get_args()));
    }

    /** @var \Swift_Message */
    private static $message;
    /** @noinspection PhpUnusedPrivateMethodInspection */
    /**
     * @param \Swift_Mailer $mailer
     * @param array $to
     * @param null $subject
     * @param null $body
     * @param array $from
     * @param array $replyTo
     * @param array|null $attachments
     *
     * @return array
     *
     * @throws Exception
     */
    private static function sendMessage($mailer, array $to, $subject = null, $body = null, array $from = null, array $replyTo = null, array $attachments = null) {
        if(!$mailer instanceof \Swift_Mailer) throw new Exception(L('Невалиден инстанция за изпращане на имейл'));
        if(!$to) throw new Exception(L('Липсва/т получател/и на имейл/ите'));

        if(!self::$message) {
            self::$message = new \Swift_Message();

            self::$message->setEncoder(\Swift_Encoding::getBase64Encoding());

            self::$message->setCharset('UTF-8');
            self::$message->setContentType('text/html');
            self::$message->setMaxLineLength(1000);

            self::$message->setDate(time());

            if(($returnPath = Settings::$aSettings['Mailer']['returnPath'][0])) self::$message->setReturnPath($returnPath);
        }

        self::$message->setFrom(($from ? $from : (($defaultFrom = Settings::$aSettings['Mailer']['defaultFrom'][0]) ? array($defaultFrom => Settings::$aSettings['Mailer']['defaultFromDisplayName'][0]) : array())));
        self::$message->setReplyTo(($replyTo ? $replyTo : (($defaultReplyTo = Settings::$aSettings['Mailer']['defaultReplyTo'][0]) ? array($defaultReplyTo => Settings::$aSettings['Mailer']['defaultReplyToDisplayName'][0]) : array())));

        self::$message->setSubject($subject);
        self::$message->setBody($body);

        if($attachments) foreach ($attachments as $attachment) self::$message->attach(\Swift_Attachment::fromPath($attachment));

        $failedRecipients = [];
        foreach ($to as $recipients) $mailer->send(self::$message->setTo($recipients), $failedRecipients);

        return $failedRecipients;
    }

}