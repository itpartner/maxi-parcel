<?php
namespace main;
class TPLError extends TPLBase {
	/**
	 * @var Exception
	 */
	public $oErr = null;
	public function __construct($sPrefix, $oErr = null) {
		if(!$oErr) {
			$oErr = new Exception('Грешка при извличане на съобщението за грешка.');
		}
		$this->oErr = $oErr;
		parent::__construct($sPrefix);
	}
	public function printHtml() {
		echo '<div id="'.$this->sPrefix.'" isError="true">';
		echo $this->oErr->getMessage();
		echo '</div>';
	}
}

