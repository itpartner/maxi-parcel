<?php
namespace main\menu;

use main\TPLBase;

class TPLMenu extends TPLBase {

	public function __construct( $sPrefix, $aInitParams = array() ) {
		return parent::__construct($sPrefix,$aInitParams);
	}

	public function printMenu() {
		$aMenus	= Menu::getMenuTree();
		self::printMenuElement($aMenus);
	}

	public function printMenuElement($aMenus) {
		if ( empty($aMenus) ) return;
		foreach ( $aMenus AS $key => $aMenu ) {
			if ( empty($aMenu['id_parent']) ) {
				$sSubClass = 'close';
			} else {
				$sSubClass = !empty($aMenu['children']) ? 'close' : 'last';
			}
			?>
			<ul class="slideToggle <?php echo $sSubClass; ?>">
				<li>
					<span class="icon-image <?php echo $sSubClass; ?>">&nbsp;</span>
					<img class="item-icon" src="<?php echo BASE_URL; ?>/images/<?php echo !empty($aMenu['icon']) ? $aMenu['icon'] : 'icons/none.gif'; ?>" />
					<span class="edit item-name">Меню: <span><?php echo !empty($aMenu['name']) ? $aMenu['name'] : 'none'; ?></span> </span>
					<span class="edit item-target">Файл: <span><?php echo !empty($aMenu['target']) ? $aMenu['target'] : 'none'; ?></span></span>
					<span class="edit item-tooltip">Подсказка: <span><?php echo !empty($aMenu['tooltip']) ? $aMenu['tooltip'] : 'none'; ?></span></span>
					<img src="<?php echo BASE_URL; ?>/images/icons/down-arrow.png" class="button-icon move-down" title="Премести надолу" />
					<img src="<?php echo BASE_URL; ?>/images/icons/up-arrow.png" class="button-icon move-up" title="Премести нагоре" />
					<img src="<?php echo BASE_URL; ?>/images/icons/add.png" class="button-icon add-new" title="Добави подменю" />
					<img src="<?php echo BASE_URL; ?>/images/icons/bin_closed.png" class="button-icon remove" title="Изтрий" />
					<?php if ( !empty($aMenu['children']) ) : ?>
						<?php self::printMenuElement($aMenu['children']); ?>
					<?php endif; ?>
				</li>
			</ul>
			<?php
		}

	}

	public function printHtml() { ?>

		<style type="text/css">
			#<?php echo $this->sPrefix; ?> ul.slideToggle { }

			#<?php echo $this->sPrefix; ?> ul.slideToggle li { margin-left: 16px; list-style-type: none; background-repeat: repeat-y; background-position: -90px 0; }
			#<?php echo $this->sPrefix; ?> ul.slideToggle li { background-image: url('images/icons/d.png'); }

			#<?php echo $this->sPrefix; ?> ul.slideToggle li img { vertical-align: text-bottom; width: 16px; height: 16px; padding-top: 4px; }

			#<?php echo $this->sPrefix; ?> ul.slideToggle li span.icon-image { background-image: url('images/icons/d.png'); background-position: 0 -1px; }
			#<?php echo $this->sPrefix; ?> ul.slideToggle li span.icon-image { width: 16px; height: 16px; padding: 0; display: inline-block; }

			#<?php echo $this->sPrefix; ?> ul.slideToggle li span.close { background-position: -72px -1px; }
			#<?php echo $this->sPrefix; ?> ul.slideToggle li span.last { background-position: -36px -1px; background-repeat: no-repeat; }

			#<?php echo $this->sPrefix; ?> ul.slideToggle li span.edit span { font-weight: bold; margin-right: 5px; }

			#<?php echo $this->sPrefix; ?> ul.slideToggle ul.open li ul { display: none; }

			#<?php echo $this->sPrefix; ?> ul.slideToggle span.edit,
			#<?php echo $this->sPrefix; ?> ul.slideToggle img.item-icon,
			#<?php echo $this->sPrefix; ?> ul.slideToggle .button-icon { cursor: pointer; }

		</style>

		<div id="<?php echo $this->sPrefix; ?>" class="form" serverClassName="<?=$this->sServerClassName?>">

			<table border="0" cellspacing="10" cellpadding="10" class="tableMenu">
				<tr>
					<td colspan="2">
						<?php $this->printElement('confirmbutton',array('align' => 'left')); ?>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<ul class="slideToggle close">
							<li>
								<span class="icon-image close">&nbsp;</span>
								<img class="item-icon" src="<?php echo BASE_URL; ?>/images/icons/none.gif" />
								<span class="">Меню</span>
								<img src="<?php echo BASE_URL; ?>/images/icons/add.png" class="button-icon add-new" title="Добави подменю" />
								<div class="menu-tree">
									<?php $this->printMenu(); ?>
								</div>
							</li>
						</ul>
					</td>
				</tr>
			</table>

		</div>
	<?php }


	public function getJSFunction() { return <<<'JS'
		(function(content,prefix,params){

			var form = content;

			var createTree = function(jUls,items) {
				items = items || [];
				jUls.each(function(i,ul){
					jLi = jQuery(ul).children('li');
					items[i] = {};
					items[i].children = [];
					if ( !empty(jLi.children('.item-name').children('span').text()) && jLi.children('.item-name').children('span').text() != 'none' ) {
						items[i].name = jLi.children('.item-name').children('span').text();
					}
					if ( !empty(jLi.children('.item-target').children('span').text()) && jLi.children('.item-target').children('span').text() != 'none' ) {
						items[i].target = jLi.children('.item-target').children('span').text();
					}
					if ( !empty(jLi.children('.item-tooltip').children('span').text()) && jLi.children('.item-tooltip').children('span').text() != 'none' ) {
						items[i].tooltip = jLi.children('.item-tooltip').children('span').text();
					}
					if ( !empty(jLi.children('.item-icon').attr('src')) && jLi.children('.item-icon').attr('src') != BASE_URL + '/images/none.gif' ) {
						items[i].icon = jLi.children('.item-icon').attr('src');
					}
					createTree(jLi.children('ul'),items[i].children);
				});
				return items;
			};

			var newMenu = function(step) {
				return '' +
				( !empty(step) ? '' : '<li>' ) +
					'<ul class="slideToggle close">' +
						'<li>' +
							'<span class="icon-image close">&nbsp;</span>' +
							'<img class="item-icon" src="' + BASE_URL + '/images/icons/none.gif" />' +
							'<span class="edit item-name"> Меню: <span>none</span> </span>' +
							'<span class="edit item-target">Файл: <span>none</span></span>' +
							'<span class="edit item-tooltip">Подсказка: <span>none</span></span>' +
							'<img src="' + BASE_URL + '/images/icons/down-arrow.png" class="button-icon move-down" title="Премести надолу" />' +
							'<img src="' + BASE_URL + '/images/icons/up-arrow.png" class="button-icon move-up" title="Премести нагоре" />' +
							'<img src="' + BASE_URL + '/images/icons/add.png" class="button-icon add-new" title="Добави подменю" />' +
							'<img src="' + BASE_URL + '/images/icons/bin_closed.png" class="button-icon remove" title="Изтрий" />' +
						'</li>' +
					'</ul>' +
				( !empty(step) ? '' : '</li>' );
			};

			jQuery(content).delegate('span.edit,img:not(.button-icon)','click',function(event){
				var sMessage = jQuery(event.target).hasClass('item-icon') ? 'Въведете пътя до иконката' : 'Въведете желаната стойност';
				var sPrevImg = jQuery(event.target).find('.item-icon').attr('src');
				if ( !empty(jQuery(event.target).attr('src')) && jQuery(event.target).attr('src') != BASE_URL + '/images/icons/none.gif' ) {
					if ( jQuery(event.target).attr('src').indexOf(BASE_URL) != -1 ) {
						var sValueImg = jQuery(event.target).attr('src').replace(BASE_URL + '/images/','');
					} else {
						var sValueImg = jQuery(event.target).attr('src');
					}
				} else {
					var sValueImg = '';
				}
				if ( jQuery(event.target).hasClass('item-icon') ) {
					var sValue = sValueImg;
				} else {
					var sValue = ( jQuery(event.target).hasClass('edit') ) ? jQuery(event.target).find('span').html() : jQuery(event.target).html();
					if ( sValue == 'none' ) sValue = '';
				}
				framework.prompt(L('Потвърждение'),L(sMessage),function(choise){
					var span = jQuery(event.target).hasClass('edit') ? jQuery(event.target).find('span') : jQuery(event.target);
					if ( jQuery(event.target).hasClass('item-icon') ) {
						if ( choise != false ) {
							choise = choise.indexOf('http://') != -1 ? choise : BASE_URL + '/images/' + choise;
						} else if ( choise == '' ) {
							choise = BASE_URL + '/images/icons/none.gif';
						} else {
							choise = sPrevImg;
						}
						jQuery(event.target).attr({'src':choise});
					} else if ( choise != false ) {
						jQuery(span).html(choise ? choise : 'none');
					}
				},false,sValue);
			});

			jQuery(content).delegate('span.close,span.open','click',function(event){
				var sAdd = jQuery(event.target).hasClass('open') ? 'close' : 'open';
				var sRem = sAdd == 'open' ? 'close' : 'open';
				jQuery(event.target).addClass(sAdd).removeClass(sRem);
				jQuery(event.target).siblings('ul').slideToggle(300,function(){

				});
			});

			jQuery(content).find('button[name=collapse_all]').click(function(){
				jQuery(content).find('ul.slideToggle .close').click()
			});

			jQuery(content).find('button[name=expand_all]').click(function(){
				jQuery(content).find('ul.slideToggle .open').click()
			});

			jQuery(content).delegate('.button-icon','click',function(event){
				var jTarget = jQuery(event.target);
				var jParent = jTarget.parent().parent();
				var jMenuTree = jQuery('.menu-tree');
				var jParentUL = jQuery(jTarget).parents('ul.slideToggle:first');
				var jParentImg = jQuery(jTarget).parents('ul.slideToggle:first').find('span.icon-image:first');
				if ( jTarget.hasClass('move-down') ) {
					if ( jParent.next().length > 0 ) {
						jParent.insertAfter(jParent.next());
					}
				} else if ( jTarget.hasClass('move-up') ) {
					if ( jParent.prev().length > 0 ) {
						jParent.insertBefore(jParent.prev());
					}
				} else if ( jTarget.hasClass('add-new') ) {
					var step = 0;
					if ( jQuery(jParent).find('.menu-tree').length == 1 ) {
						step = 1;
						jParent = jQuery(content).find('.menu-tree');
					}
					if ( jQuery(jParentImg).hasClass('open') ) {
						jQuery(jParentImg).attr({'class':'icon-image close'});
						jQuery(jParentImg).siblings('ul').slideToggle(300,function(){
							jQuery(jParent).append(newMenu(step));
						});
					} else {
						jQuery(jParent).append(newMenu(step));
						jQuery(jParentImg).addClass('close').removeClass('open').removeClass('last');
					}
				} else if ( jTarget.hasClass('remove') ) {
					jParent.remove();
				} else {
					return true;
				}
			});

			jQuery(content).find('button.confirm').click(function(){
				var menu = createTree(jQuery(content).find('.menu-tree').children('ul'));
				form.request('save',{menu:menu});
			});

		});

JS;
	}

}

