<?php
namespace main\menu;

use main\db\DBC;
use main\form\FormPanelHandler;

/**
 * Class Menu
 * @package main\menu
 * @accessLevelDescription Управление на менюто на системата
 */
class Menu extends FormPanelHandler {

	public static $cacheTime = 60;

	private $nAutoID = 1;
	
	public static function getMenuTree() {
		$aMenus	= DBC::$main->selectAssoc("SELECT m.id AS _key_,m.* FROM menu m ORDER BY id_parent,menu_order,id ASC");
		foreach ( $aMenus AS $nKey => $aCategory ) { $aMenus[$nKey]['children'] = array(); }
		$aResults	= array();
		foreach ( $aMenus AS $nKey => $aCategory ) { 
			if ( !empty($aCategory['id_parent']) ) { 
				if ( !empty($aMenus[$aCategory['id_parent']]) ) $aMenus[$aCategory['id_parent']]['children'][] = &$aMenus[$nKey];
			} else { 
				$aResults[] = &$aMenus[$nKey];
			}
		}
		return !empty($aResults) ? $aResults : array();
	}
	
	public function save($aRequest) {
		DBC::$main->withTrans(function($aRequest){
			$aMenu = $this->menuTreeToArray($aRequest['menu']);
			DBC::$main->execute("DELETE FROM menu");
			DBC::$main->multiInsert('menu',$aMenu,'insert');
		},array($aRequest));
		return $this->oResponse;
	}
	
	private function menuTreeToArray($aMenuTree, &$aMenu = array(), $nParentID = 0) {
		if(empty($aMenu)) $aMenu = array();
		$nOrder = 1;
		foreach($aMenuTree as $aMenuTreeItem) {
			$nID = $this->nAutoID++;
            $aMenu[] = array(
				'id' => $nID,
				'id_parent' => $nParentID,
				'menu_order' => $nOrder++,
				'target' => $aMenuTreeItem['target'],
				'target_type' => strpos($aMenuTreeItem['target'],'http') ===0 ? 'link' : (empty($aMenuTreeItem['target']) || $aMenuTreeItem['target'] == 'none' ? 'none' : 'file'),
				'name' => $aMenuTreeItem['name'],
				'icon' => strchr($aMenuTreeItem['icon'],'none.gif') ? NULL : str_replace(BASE_URL.'/images/','',$aMenuTreeItem['icon']),
				'tooltip' => $aMenuTreeItem['tooltip']
			);
			$this->menuTreeToArray($aMenuTreeItem['children'], $aMenu, $nID);
		}
		return $aMenu;
	}
}