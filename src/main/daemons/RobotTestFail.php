<?php
namespace main\daemons;

use main\Exception;

class RobotTestFail extends BaseRobot {
    public function run($time, $params = array()) {
        throw new Exception("An error has occured :) ");
    }
}