<?php
namespace main\daemons;

use main\db\DBC;
use main\Exception;

class RobotRunTasks extends BaseRobot {

	public $mutexRobots = [__CLASS__];
    public function run($time, $params = array()) {
		while(true) {
            $t = microtime(true);

			DBC::$main->execute("LOCK TABLES robot_cron WRITE, robot_tasks WRITE");
            $trans = DBC::$main->startTrans();

            $cron = DBC::$main->select("
                SELECT
                	t.*
                FROM (
                    SELECT
                    	rc.*,
                    	DATE_FORMAT(NOW(),'%Y-%m-%d %H:%i:00') as run_time
                    FROM robot_cron AS rc
                    WHERE TRUE
                    	AND rc.to_arc = 0
                    	AND (
                    			rc.month = '*'
                    		OR 	IF (SUBSTR(rc.month, 1, 2) = '*/', MONTH(NOW()) % SUBSTR(rc.month, 3) = 0, FIND_IN_SET(MONTH(NOW()), rc.month))
						)
                    	AND (
								rc.day = '*'
							OR  IF (SUBSTR(day, 1, 2) = '*/', DAY(NOW()) % SUBSTR(rc.day, 3) = 0, FIND_IN_SET(DAY(NOW()), rc.day))
						)
                    	AND (
                    			rc.hour = '*'
                    		OR 	IF (SUBSTR(hour, 1, 2) = '*/', HOUR(NOW()) % SUBSTR(rc.hour, 3) = 0, FIND_IN_SET(HOUR(NOW()), rc.hour))
						)
                    	AND (
                    			rc.minute = '*'
                    		OR 	IF (SUBSTR(minute, 1, 2) = '*/', MINUTE(NOW()) % SUBSTR(rc.minute, 3) = 0, FIND_IN_SET(MINUTE(NOW()), rc.minute))
						)
                    	AND FIND_IN_SET(DAYNAME(NOW()), rc.weekday)
                    	AND rc.base_url != ''
                ) AS t
                LEFT JOIN robot_tasks AS rt ON TRUE
                	AND rt.run_after = t.run_time
                	AND rt.id_cron = t.id
                WHERE TRUE
                	AND rt.run_after IS NULL
            ");

            $tasks = DBC::$main->select("
                SELECT
                    t.*,
                    IF (pl.id AND sql_info.start_time = t.sql_start_time, 1, 0) AS is_running
                FROM robot_tasks AS t
                JOIN (
                	SELECT
                		unix_timestamp() - gs.variable_value AS start_time
					FROM performance_schema.global_status AS gs
					WHERE TRUE
						AND gs.variable_name = 'uptime'
				) AS sql_info
                LEFT JOIN (
                	SELECT
                		pl.ID AS id
					FROM information_schema.PROCESSLIST AS pl
				) pl ON TRUE
					AND pl.id = t.sql_cid
                WHERE TRUE
					AND (
							t.status = 'running'
						OR 	(
								t.status = 'wait'
							AND t.run_after <= NOW()
						)
						OR 	(
								t.retries_left > 0
							AND t.status = 'failed'
							AND t.run_after <= NOW()
						)
					)
            ");

            $runningTasksByIdentifier = [];
            $waitingTasksByIdentifier = [];
            foreach ($tasks as &$task) {
                $task['max_instance_identifier'] = $task['id_parent'] ? 'parent@'.$task['id_parent'] : ($task['id_cron'] ? 'cron@'.$task['id_cron'] : $task['robot_name']);
                if($task['status'] != 'wait') {
                    if($task['is_running']) {
                        $runningTasksByIdentifier[$task['max_instance_identifier']]++;
                    } else {
                        if($task['retries_left'] > 0) {
                            $task['status'] = 'wait';
                            $task['retries_left']--;
							DBC::$main->insert('robot_tasks',$task);
                        } else {
                            if($task['status'] != 'failed') {
                                $task['status'] = 'failed';
                                $task['error'] = "Unknown error";
								DBC::$main->insert('robot_tasks',$task);
                            }
                        }
                    }
                } else {
					if($task['base_url']) {
						$waitingTasksByIdentifier[$task['max_instance_identifier']]++;
					} else {
						$task['status'] = 'failed';
						$task['error'] = "Missing base url";
						DBC::$main->insert('robot_tasks',$task);
					}
                }
            }
            unset($task);

            foreach ($cron as $cronRow) {
                if ($cronRow['max_instances'] > ($waitingTasksByIdentifier['cron@'.$cronRow['id']] + $runningTasksByIdentifier['cron@'.$cronRow['id']])) {
                    $task = array(
                        'id_cron' => $cronRow['id'],
                        'base_url' => $cronRow['base_url'],
                        'robot_name' => $cronRow['robot_name'],
                        'params' => $cronRow['params'],
                        'status' => 'wait',
                        'run_after' => $cronRow['run_time'],
                        'retries_left' => $cronRow['retry_attempts'],
                        'max_instances' => $cronRow['max_instances'],
                    );
					DBC::$main->insert('robot_tasks',$task);
                    $waitingTasksByIdentifier['cron@'.$task['id_cron']]++;
                    $tasks[] = $task;
                }
            }

            $tasksToRun = array();
            foreach ($tasks as $task) {
                if ($task['status'] == 'wait') {
                    if (!$task['max_instances'] || $runningTasksByIdentifier[$task['max_instance_identifier']] < $task['max_instances']) {
                        $runningTasksByIdentifier[$task['max_instance_identifier']]++;
                        $tasksToRun[] = $task;
                    }
                }
            }

            $trans->commit();
            DBC::$main->execute("UNLOCK TABLES");

			$mch = curl_multi_init();
            $chs = array();
            foreach ($tasksToRun as $task) {
                $ch = curl_init("{$task['base_url']}/run_robot.php?task_id={$task['id']}");
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($ch, CURLOPT_TIMEOUT, 4);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_multi_add_handle($mch,$ch);
                $chs[] = $ch;
            }

            do {
                if (($ret = curl_multi_exec($mch,$running)) > 0) throw new Exception("CURL Error {$ret}.");
                usleep(10000);
            } while($running > 0);

            foreach ($chs as $ch) {
                if (($ret = curl_multi_remove_handle($mch,$ch)) > 0) throw new Exception("CURL Error {$ret}.");
                curl_close($ch);
            }
            curl_multi_close($mch);

            $us = 10000000 - (microtime(true) - $t) * 1000000;
            if ($us > 0) usleep($us);
        }
    }

}