<?php
namespace main\daemons;

abstract class BaseRobot implements IRobot {

    public $mutexRobots = [];

    public $baseURL;
    public $robotName;
    public $taskID;

    abstract public function run($time, $params = array());

}