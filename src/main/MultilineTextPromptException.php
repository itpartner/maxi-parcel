<?php
namespace main;

class MultilineTextPromptException extends Exception {
    function __construct($message = '', $responseFieldName = '', $fields = array(), $inner = array()) {
        parent::__construct($message, $fields, $inner);
        $this->setAdditionalData($responseFieldName);
    }
}