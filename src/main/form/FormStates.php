<?php
namespace main\form;
use main\db\DBC;
use main\Exception;
use main\rpc\RPCHandler;
use main\System;

class FormStates implements RPCHandler {
	public static function saveStates($aStates) {
		if(!System::isLoggedIn()) return;
		if(empty($aStates)) return;
		if(!is_array($aStates)) throw new Exception();
		
		$aRows = array();
		foreach ($aStates as $sStateName => $aState) {
			$aRows[] = "({$_SESSION['userdata']['id']},".DBC::$main->Quote($sStateName).",".DBC::$main->Quote(serialize($aState)).")";
		}
		if(!empty($aRows)) {
			DBC::$main->Execute("INSERT INTO users_form_states (id_user,state_name,state) VALUES ".implode(",\n",$aRows)."\n ON DUPLICATE KEY UPDATE state=VALUES(state)");
		}
		foreach ($aStates as $sStateName => $aState) {
			$_SESSION['userdata']['form_states'][$sStateName] = $aState;
		}
	}
	
	public static function getState($sStateName) {
		if(!System::isLoggedIn()) return array();
		if(empty($_SESSION['userdata']['form_states'])) {
			$_SESSION['userdata']['form_states'] = array();
			$aStates = DBC::$main->select("SELECT * FROM users_form_states WHERE id_user = {$_SESSION['userdata']['id']}");
			foreach ($aStates as $aState) {
				$_SESSION['userdata']['form_states'][$aState['state_name']] = unserialize($aState['state']);
			}
		}
		return empty($_SESSION['userdata']['form_states'][$sStateName]) ? null : $_SESSION['userdata']['form_states'][$sStateName];
	}
}