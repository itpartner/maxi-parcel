<?php
namespace main\form;
class ResponseAlert {
	/**
	 * @var string
	 */
	public $title = '';
	
	/**
	 * @var string
	 */
	public $msg = '';
	
	/**
	 * @var string
	 */
	public $type = '';
	
	/**
	 * @var string
	 */
	public $focusField = '';
	
	public function __construct($sMessage, $sTitle = '', $sType = '', $sFocusField = '') {
		$this->msg = $sMessage;
		$this->title = $sTitle;
		$this->type = $sType;
		$this->focusField = $sFocusField;
	}
}

