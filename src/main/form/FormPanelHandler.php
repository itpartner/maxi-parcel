<?php
namespace main\form;
use main\db\DBC;
use main\db\DBConnection;
use main\Exception;
use main\InvalidParameter;
use main\rpc\RPCHandler;
use main\TPLBase;
use main\language\Transliteration;
use main\Util;
use main\validator\Validate;

abstract class FormPanelHandler implements RPCHandler {
	/**
	 * Име/описание на формата
	 *
	 * @var unknown_type
	 */
	public $sFormDescription;
	/**
	 * Обекта който трябва да се върне като резултат
	 *
	 * @var Response
	 */
	public $oResponse = null;

	public function __construct() {
		$this->sFormDescription = empty($this->sFormDescription) ? get_class($this) : $this->sFormDescription;
		$this->oResponse = new Response();
	}

    /**
     * @param array $aRequest
     * @param array $aInitParams
     * @return Response $response
     */
    public function init($aRequest = array(), $aInitParams = array()){
		return $this->oResponse;
	}

	/**
	 * @param string $sMessage - съобщение
	 * @param string $sTtile - заглавие
	 * @param string $sType - тип (икона) - 'info', 'warning', 'error', 'question'
	 * @param string $sFocusField - поле в което се измества фокуса
	 */
	public function setAlert($sMessage, $sTtile = '', $sType = 'info', $sFocusField = '') {
		$this->oResponse->alert = new ResponseAlert($sMessage, $sTtile, $sType, $sFocusField);
	}

	/**
	 * Задава стойностите на Form field
	 *
	 * @param string $sName - име на полето
	 * @param mixed $mValue - стойност
	 * @param mixed $mContent - съдържание ( за list/combobox/dropdownlist...)
	 * @param array $aAttributes - атрибути
	 */
	public function setField($sName, $mValue, $mContent = null, $aAttributes = array()) {
		if(!array_key_exists($sName, $this->oResponse->fields)) $this->oResponse->fields[$sName] = new ResponseField;

		$this->oResponse->fields[$sName]->set($mValue, $mContent);
		foreach ($aAttributes as $sAttrName => $value) {
			$this->oResponse->fields[$sName]->$sAttrName = $value;
		}
	}

	public function setFieldAttributes($sName, $aAttributes) {
		$this->setField($sName, null, null, $aAttributes);
	}

	/**
	 * Задава стоиността за дадено поле
	 *
	 * @param string $sName - име на полето
	 * @param mixed $mValue - стойност
	 */
	public function setFieldValue($sName, $mValue) {
		$this->setField($sName, $mValue);
	}

	/**
	 * Задава атрибути за дадено поле
	 *
	 * @param string $sName - име на полето
	 * @param string $aAttributes - грешка
	 */
	public function setFieldError($sName, $aAttributes) {
		$this->setField($sName, null, null, $aAttributes);
	}

	/**
	 * Задава елементите на поле (list,combobox,dropdownlist)
	 *
	 * @param string $sName - име на полето
	 * @param mixed $mContent
	 */
	public function setFieldContent($sName, $mContent) {
		$this->setField($sName, null, $mContent);
	}

	protected function checkValue($mValue, $sOperation, $mCompareValue) {
		if(is_array($mValue)) {
			if(array_key_exists('input_value', $mValue)) {
				$mValue = $mValue['input_value'];
			} else {
				return false;
			}
		}
		switch ($sOperation) {
			# string, int, float
			case 'eq'  : if(($mValue == $mCompareValue) || ($mValue == Transliteration::LatinToCyr($mCompareValue)) || ($mValue == Transliteration::convertCyr2PhoLower($mCompareValue))) return true;	break;
			case 'ne'  : if(($mValue != $mCompareValue) || ($mValue == Transliteration::LatinToCyr($mCompareValue)) || ($mValue == Transliteration::convertCyr2PhoLower($mCompareValue)))  return true;	break;
			case 'gt'  : if((double)$mValue > (double)$mCompareValue) return true; break;
			case 'gte' : if((double)$mValue >= (double)$mCompareValue) return true; break;
			case 'lt'  : if((double)$mValue < (double)$mCompareValue) return true; break;
			case 'lte' : if((double)$mValue <= (double)$mCompareValue) return true; break;
			case 'bg'  : if(empty($mCompareValue) || (@mb_strpos(@mb_strtolower($mValue), @mb_strtolower($mCompareValue)) === 0) || (@mb_strpos(@mb_strtolower($mValue), @mb_strtolower(Transliteration::LatinToCyr($mCompareValue))) === 0) || (@mb_strpos(@mb_strtolower($mValue), @mb_strtolower(Transliteration::convertCyr2PhoLower($mCompareValue))) === 0) ) return true; break;
			case 'nbg' : if(!empty($mCompareValue) && ((@mb_strpos(@mb_strtolower($mValue), @mb_strtolower($mCompareValue)) !== 0) || (@mb_strpos(@mb_strtolower($mValue), @mb_strtolower(Transliteration::LatinToCyr($mCompareValue))) !== 0) || (@mb_strpos(@mb_strtolower($mValue), @mb_strtolower(Transliteration::convertCyr2PhoLower($mCompareValue))) !== 0))) return true; break;
			case 'ct'  : if(empty($mCompareValue) || (@mb_strpos(@mb_strtolower($mValue), @mb_strtolower($mCompareValue)) !== false) || (@mb_strpos(@mb_strtolower($mValue), @mb_strtolower(Transliteration::LatinToCyr($mCompareValue))) !== false) || (@mb_strpos(@mb_strtolower($mValue), @mb_strtolower(Transliteration::convertCyr2PhoLower($mCompareValue))) !== false)) return true; break;
			case 'nct' : if(!empty($mCompareValue) && ((@mb_strpos(@mb_strtolower($mValue), @mb_strtolower($mCompareValue)) === false) && (@mb_strpos(@mb_strtolower($mValue), @mb_strtolower(Transliteration::LatinToCyr($mCompareValue))) === false) && (@mb_strpos(@mb_strtolower($mValue), @mb_strtolower(Transliteration::convertCyr2PhoLower($mCompareValue))) === false))) return true; break;
			case 'regexp' : if(@preg_match($mCompareValue,$mValue)) return true; break;
			# date
			case 'deq': if(substr($mValue,0,10)==substr($mCompareValue['date1'],0,10)) return true; break;
			case 'dlt': if(Util::SQLDateToTimeStamp($mValue) < Validate::checkMySQLDateTimeToTimeStamp($mCompareValue['date1'])) return true; break;
			case 'dgt': if(Util::SQLDateToTimeStamp($mValue) > Validate::checkMySQLDateTimeToTimeStamp($mCompareValue['date1'])) return true; break;
			case 'dbt':
				$nTime = Util::SQLDateToTimeStamp($mValue);
				if(
					$nTime >= Validate::checkMySQLDateTimeToTimeStamp($mCompareValue['date1'])
					&&	$nTime <= Validate::checkMySQLDateTimeToTimeStamp(substr($mCompareValue['date2'],0,10).' 23:59:59')
				) return true;
				break;
			case 'dl':
				if($mCompareValue['interval_type'] == 'h') $sType = 'hours';
				elseif($mCompareValue['interval_type'] == 'w') $sType = 'weeks';
				elseif($mCompareValue['interval_type'] == 'm') $sType = 'months';
				else $sType = 'days';
				$nInterval = abs(intval($mCompareValue['interval']));
				return Util::SQLDateToTimeStamp($mValue) >= strtotime("-$nInterval $sType");
				break;
			case 'today':
				return substr($mValue,0,10) == date('Y-m-d');
			default:throw new InvalidParameter('Невалидна операция за проверка.');
		}
		return false;
	}

	/**
	 * Връща качените файлове от file input пр. "$this->getUploadedFiles($aRequest['fields']['my_file_field']"
	 *
	 * @param array $aValue стойността на полето
	 * @return mixed съдържанието на файла/овете
	 */
	public static function getUploadedFiles($aValue) {
		if(empty($aValue) || !is_array($aValue)) return $aValue;
		return $aValue;
	}
	public function getWhereFromTimeButton($sDBField,$mValue) {
		$nTimeFrom = $this->timeButtonToTimestampFrom($mValue);
		$nTimeTo = $this->timeButtonToTimestampTo($mValue);
		$sTimeFrom = date('Y-m-d H:i:s', $nTimeFrom);
		$sTimeTo = date('Y-m-d 23:59:59', $nTimeTo);
		return "$sDBField BETWEEN '$sTimeFrom' AND '$sTimeTo'";
	}
	public function timeButtonToTimestampFrom($mValue) {
		switch ($mValue) {
			case TPLBase::TIME_BUTTON_TODAY    : $mValue = mktime( 0, 0, 0, date('m'), date('d'), date('Y'));break;
			case TPLBase::TIME_BUTTON_YESTERDAY : $mValue = mktime( 0, 0, 0, date('m'), date('d')-1, date('Y'));break;
			case TPLBase::TIME_BUTTON_MONTH  : $mValue = mktime(0, 0, 0, date('m'), 1, date('Y'));break;
			case TPLBase::TIME_BUTTON_PREV_MONTH : $mValue = mktime(0, 0, 0, date('m')-1, 1, date('Y'));break;
			case TPLBase::TIME_BUTTON_NEXT_MONTH : $mValue = mktime(0, 0, 0, date('m')+1, 1, date('Y'));break;
			case TPLBase::TIME_BUTTON_PREV_WEEK :
                $monday = strtotime('monday last week');
                $mValue = mktime(0, 0, 0, date('m', $monday), date('d', $monday), date('Y', $monday));
                break;
            case TPLBase::TIME_BUTTON_NEXT_WEEK :
                $monday = strtotime('monday next week');
                $mValue = mktime(0, 0, 0, date('m', $monday), date('d', $monday), date('Y', $monday));
                break;
			case TPLBase::TIME_BUTTON_WEEK :
                $monday = strtotime('monday this week');
				$mValue = mktime(0, 0, 0, date('m', $monday), date('d', $monday), date('Y', $monday));
                break;
			default:
				if(is_array($mValue)) {
					if($mValue['compareType'] == 'dl') {
						if($mValue['compareValue']['interval_type'] == 'h') $sType = 'hours';
						elseif($mValue['compareValue']['interval_type'] == 'w') $sType = 'weeks';
						elseif($mValue['compareValue']['interval_type'] == 'm') $sType = 'months';
						else $sType = 'days';
						$nInterval = abs(intval($mValue['compareValue']['interval']));
						$mValue = strtotime("-$nInterval $sType");
					} else {
						$mValue = Validate::checkMySQLDateTimeToTimeStamp($mValue['compareValue']['date1']);
					}
				}
		}

		return (int) $mValue;
	}
	public function timeButtonToTimestampTo($mValue) {
		switch ($mValue) {
			case TPLBase::TIME_BUTTON_YESTERDAY : $mValue = mktime( 23, 59, 59, date('m'), date('d')-1, date('Y'));break;
			case TPLBase::TIME_BUTTON_PREV_MONTH : $mValue = mktime( 23, 59, 59, date('m'), 0, date('Y'));break;
			case TPLBase::TIME_BUTTON_NEXT_MONTH : $mValue = mktime( 23, 59, 59, date('m')+2, 0, date('Y'));break;
			case TPLBase::TIME_BUTTON_MONTH :
				$mValue = mktime( 23, 59, 59, date('m')+1, 0, date('Y'));
				break;
			case TPLBase::TIME_BUTTON_TODAY :
				$mValue = time();
				break;
            case TPLBase::TIME_BUTTON_PREV_WEEK :
                $sunday = strtotime('sunday last week');
                $mValue = mktime(0, 0, 0, date('m', $sunday), date('d', $sunday), date('Y', $sunday));
                break;
            case TPLBase::TIME_BUTTON_NEXT_WEEK :
                $sunday = strtotime('sunday next week');
                $mValue = mktime(0, 0, 0, date('m', $sunday), date('d', $sunday), date('Y', $sunday));
                break;
            case TPLBase::TIME_BUTTON_WEEK :
                $sunday = strtotime('sunday this week');
                $mValue = mktime(0, 0, 0, date('m', $sunday), date('d', $sunday), date('Y', $sunday));
                break;
			default:
				if(is_array($mValue)) {
					if($mValue['compareType'] == 'dl') {
						$mValue = time();
					} elseif($mValue['compareType'] == 'dgt'){
						$mValue = time();
					} elseif($mValue['compareType'] == 'dbt') {
						$mValue = Validate::checkMySQLDateTimeToTimeStamp($mValue['compareValue']['date2']);
					} else {
						$mValue = Validate::checkMySQLDateTimeToTimeStamp($mValue['compareValue']['date1']);
					}
				}
		}
		return (int) mktime(23,59,59,date('m',$mValue),date('d',$mValue),date('Y',$mValue));
	}

	public function setLastEditInfo($aInfo = null) {
		$this->setField('last_edited_info', self::createLastEditInfo($aInfo));
	}
	public static function createLastEditInfo($aInfo = null) {
		if($aInfo === null) {
			$aInfo = array(
				'updated_time' => date('Y-m-d H:i:s'),
				'updated_user' => self::getEditInfoUser($_SESSION['userdata']['id'])
			);
		} else {
			$aInfo = array(
				'created_time' => empty($aInfo['created_time'])			?	''												: $aInfo['created_time'],
				'created_user' => is_numeric($aInfo['created_user'])	?	self::getEditInfoUser($aInfo['created_user'])	: $aInfo['created_user'],
				'updated_time' => empty($aInfo['updated_time'])			?	''												: $aInfo['updated_time'],
				'updated_user' => is_numeric($aInfo['updated_user'])	?	self::getEditInfoUser($aInfo['updated_user'])	: $aInfo['updated_user'],
			);
		}
		return $aInfo;
	}
	private static function getEditInfoUser($nIDUser) {
        $nIDUser = (int) $nIDUser;
        return DBC::$slave->selectField("SELECT CONCAT_WS(' ',fname, mname, lname) FROM personnel WHERE id = {$nIDUser}", 300);
	}
}