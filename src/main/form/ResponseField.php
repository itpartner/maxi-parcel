<?php
namespace main\form;
class ResponseField {
	public $content = null;
	public $value = null;
	
	/**
	 * Задава стойностите на Form field-а  
	 *
	 * @param mixed $mValue - стойност
	 * @param mixed $mContent - съдържание ( за list/combobox/dropdownlist...)
	 * @param string $sErrorString - грешка която се изписва до полето
	 * @param string $sToolTip
	 */
	public function __construct($mValue = null, $mContent = null, $sToolTip = null, $sErrorString = null) {
		//propertytata se iztrivat za da ne se izprashtat praznite na clienta
		foreach ($this as $k=>$v) {
			unset($this->$k);
		}
		
		$this->set($mValue, $mContent, $sToolTip, $sErrorString);
	}
	
	/**
	 * Задава стойностите на Form field-а  
	 *
	 * @param mixed $mValue - стойност
	 * @param mixed $mContent - съдържание ( за list/combobox/dropdownlist...)
	 * @param string $sErrorString - грешка която се изписва до полето
	 * @param string $sToolTip
	 */
	public function set($mValue, $mContent = null, $sToolTip = null, $sErrorString = null) {
		if(!is_null($mValue)) $this->value = $mValue;
		if(!is_null($mContent)) $this->content = $mContent;
		if(!is_null($sToolTip)) $this->toolTip = $sToolTip;
		if(!is_null($sErrorString)) $this->errorString = $sErrorString;
	}
}
