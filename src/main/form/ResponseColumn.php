<?php
namespace main\form;
class ResponseColumn {
	/**
	 * Име на полето в recordset-a (пр. loading_num)
	 *
	 * @var string
	 */
	public $dataField;
	
	/**
	 * Име на колоната което ще се покаже на потребителя
	 *
	 * @var string
	 */
	public $headerText;
	
	/**
	 * Тип на колоната 
	 *
	 * @var string
	 */
	public $type;
	
	/**
	 * Колоните които се съдържат в тази колона (за групи)
	 *
	 * @var ResponseColumn[]
	 */
	public $children;
	
	/**
	 * Ширина на колоната
	 *
	 * @var integer
	 */
	public $width;

    /** @var  array */
    public $items;

    public $toolTipField;

	/**
	 * Колоната се описва в $aConfig пр. array('type' => 'number', 'dataField' => 'id', 'width'	=> 70)
	 *
	 * @param array $aConfig
	 */
	public function __construct($aConfig) {
		//propertytata se iztrivat za da ne se izprashtat praznite na clienta
		foreach ($this as $k=>$v) {
			unset($this->$k);
		}
		$this->children = array();
		$this->set($aConfig);
	}
	
	
	/**
	 * Колоната се описва в $aConfig пр. array('type' => 'number', 'dataField' => 'id', 'width'	=> 70)
	 *
	 * @param array $aConfig
	 */
	public function set($aConfig) {
		foreach ($aConfig as $sProperty => $mValue) {
			$this->$sProperty = $mValue;
		}
	}
}
