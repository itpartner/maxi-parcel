<?php
namespace main\form;
class Response{
	/**
	 * Масив с преведени текстове (изпозлва се само при init на формата)
	 *
	 * @var string[]
	 */
	public $translations = array();
	
	/**
	 * обект/асоциативен масив със състоянието на компонентата
	 *
	 * @var stdClass
	 */
	public $componentState;
	
	/**
	 * масив в всички стойности които ще се сет-нат при клиента. Примерно поле 'name' е $fields['name']
	 *
	 * @var ResponseField[]
	 */
	public $fields = array();
	
	/**
	 * @var ResponseAlert
	 */
	public $alert = null;
	
	/**
	 * Масив с колоните на таблицата
	 *
	 * @var ResponseColumn[]
	 */
	public $gridColumns = array();
	
	/**
	 * Двумерен масив с данните на таблицата
	 *
	 * @var array
	 */
	public $gridData = null;

	/** @var array */
	public $gridFilters = null;

	/** @var array */
	public $filterItems = null;

    /** @var array */
    public $sortFields = null;

    /** @var array */
    public $paging = null;
    /** @var array */
    public $filters = null;

    /** @var string */
    public $gridPrefix = null;
    /** @var string */
    public $containerPrefix = null;
    /** @var array */
    public $tplCfg = null;
    /** @var array */
    public $defaultQuickFilters = null;
    /** @var int */
    public $headerRowCount = null;


	/**
	 * Тотали на таблицата
	 *
	 * @var array
	 */
	public $gridTotals = null;
	
	public function __construct() {
		$this->componentState = new \stdClass();
	}
}

