<?php
namespace main;

class AccessDenied extends Exception{
    function __construct($message = '', $fields = array(), $inner = array()) {
        if(empty($message)) $message = L('Нямате достъп до ресурса');
        parent::__construct($message, $fields, $inner);
    }
}