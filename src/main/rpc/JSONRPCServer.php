<?php
namespace main\rpc;
use main\Access;
use main\db\DBC;
use main\EmptySession;
use main\Exception;
use main\logs\Logs;
use main\Util;

class JSONRPCServer {

    public function handleRequest($request, $bLog = true) {
        try {
            if (is_object($request)) $response = $this->processRequest($request, $bLog);
            else if(is_array($request)) {
                $response = array();
                foreach ($request as $req) $response[] = $this->processRequest($req, $bLog);
            } else throw new Exception('Parse error.', -32700);
        } catch (Exception $e) {
            $response = array(
                'result' => null,
                'error' => $this->createError($e),
                'id' => null,
                'jsonrpc' => '2.0'
            );
        }
        return $response;
    }

    public function handle() {
        $requestString = file_get_contents('php://input');
        $request = json_decode($requestString);

        $response = $this->handleRequest($request);

        header("Content-type: application/json");
        header("Access-Control-Allow-Origin: *");
        echo json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    private function processRequest($request, $bLog = true) {
        $startTime = microtime(true);
        $startCPUTime = Util::getCPUTime();

        $request = json_decode(json_encode($request), true);
        $response = array(
            'id' => $request['id'],
            'jsonrpc' => '2.0'
        );

        try {
            $target = parseNS($request['method']);
            $class = $target['class'];
            $method = $target['method'];

            if(!class_exists($class, true)) {
                throw new \Exception(sprintf("Class %s not found.", $class), -32601);
            }

            $handler = new $class;
            $classImplements = class_implements($handler);
            if(!$classImplements['main\rpc\RPCHandler']) throw new \Exception(sprintf("Class '%s' does not implement RPCHandler.", $class), -32601);
            if(!is_callable(array($handler, $method))) throw new \Exception(sprintf("Method '%s' not found in %s.", $method, $class), -32601);

            Access::checkAccess($class);

            foreach (DBC::$connections as $dbConnection) $dbConnection->resetTrans();
            if(!is_array($request['params'])) $request['params'] = [];
            $response['result'] = call_user_func_array(array($handler, $method), $request['params']);
        } catch (\Exception $e) {
            $response['error'] = $this->createError($e);
        }

        if($bLog) Logs::logRequest($request['method'], $request['params'], $response, microtime(true) - $startTime, Util::getCPUTime() - $startCPUTime);

        return $response;
    }

    private function createError(\Exception $ex) {
        Logs::logException($ex);
        return array(
            'code' => $ex->getCode() ? $ex->getCode() : 1000,
            'message' => $ex->getMessage(),
            'data' => Exception::convertToJSObject($ex)
        );
    }
}