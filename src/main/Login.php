<?php
namespace main;
use main\db\DBC;
use main\logs\Logs;

class Login {
    public static $changePassPeriod = '6 month';
    
    public static function check() {
        if ($_SESSION['access']!==TRUE) {
            if(isset($_GET['session_expired'])) {
                $url = BASE_URL."/login.php?session_expired";
            } else {
                $sTargetURL = self::getURL();
                if(!empty($_SERVER['HTTP_REFERER'])) {
                    $sTargetURL .= (strpos($sTargetURL, '?') === false ? '?' : '&').'referer='.urlencode($_SERVER['HTTP_REFERER']);
                }
                $url = BASE_URL."/login.php?redirect_to=".urlencode($sTargetURL);
                if (isset($_GET['target'])) {
                    $url.="&target=".$_GET['target'];
                }
            }

            if((!defined('SSL') || SSL)) $url = str_replace('http://','https://',$url);
            header("Location: {$url}");
            die;
        }
    }

    public static function pass($aPost, $bSendHeaders = true) {
        if(empty($aPost['userlogin']) || empty($aPost['userpass'])) return;

        if (!isset($aPost, $aPost['inputstring']) || $_SESSION['captcha'] != $aPost['inputstring'])
            throw new Exception("Невалидно потребителско име или парола.");

        $sEscapedUserName = DBC::$main->escape($aPost['userlogin']);
		$aUser = DBC::$main->selectRow("
			SELECT 
				aa.*,
				aa.id as id_account,
				p.*,
			    CONCAT_WS(' ',p.fname,p.mname,p.lname) name,
                GROUP_CONCAT(aapl.level) as access_levels
			FROM access_account aa
            JOIN personnel p ON p.id = aa.id_person
            LEFT JOIN access_account_profiles aap ON aap.id_account = aa.id
            LEFT JOIN access_profiles_levels aapl ON aapl.id_profile = aap.id_profile
            WHERE 1
            AND aa.username = '$sEscapedUserName'
            AND aa.to_arc = 0
            AND p.to_arc = 0
            AND p.status = 'active'
            GROUP BY aa.id
		");

        $_SESSION['access'] = !empty(($aUser)) ? password_verify($aPost['userpass'], $aUser['password']) : false;
        if(!$_SESSION['access']) throw new Exception("Невалидно потребителско име или парола.");

        $_SESSION['userdata'] = $aUser;
        $_SESSION['userdata']['access_levels'] = explode(',',$aUser['access_levels']);
        $_SESSION['userdata']['access_levels'][] = '\main\form';
        $_SESSION['userdata']['access_levels'][] = '\main\grid';
        $_SESSION['userdata']['access_levels'] = array_combine($_SESSION['userdata']['access_levels'], $_SESSION['userdata']['access_levels']);

        Logs::logLogin(array('target'=>'emc'));

        $sHTTPURL = empty($_REQUEST['redirect_to']) ? str_replace('https://','http://',BASE_URL) : $_REQUEST['redirect_to'];
        if($bSendHeaders) header("Location: {$sHTTPURL}");
	}

	public static function getURL() {
		$pageURL = 'http';
     	if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
       		$pageURL .= "s";
    	}
     	$pageURL .= "://";
     	if ($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443") {
        	$pageURL .= $_SERVER["HTTP_HOST"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
     	} else {
        	$pageURL .= $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
     	}
 		return $pageURL;
	}

}
