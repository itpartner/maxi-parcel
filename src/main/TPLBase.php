<?php
namespace main;

use DOMDocument;
use main\form\FormStates;
use main\form\Response;
use main\grid\DBGridFilters;
use main\grid\GridPanelHandler;
use main\rpc\JSONRPCServer;
use stdClass;

class TPLBase {
    public $sPrefix = '';
    public $aInitParams = array();

    /**
     * dali da se printira s ramka kogato se pokazva v centralnata zona
     *
     * @var boolean
     */
    public $bAlwaysPrintWithFrame = false;
    /**
     * dali da se printira s title kogato e v load_direct
     *
     * @var boolean
     */
    public $bPrintWithTitle = false;
    /**
     * dali da se samoresaizva prozoreca kogato e v load_direct
     *
     * @var boolean
     */
    public $bLoadDirectAutoResize = false;
    /**
     * default config na ramkata
     *
     * @var array
     */
    public $aFrameConfig = array();

    /**
     * @var string
     */
    public $sServerClassName = '';
    public $oHandler;

    /**
     * @var DOMDocument
     */
    private $oDom;
    private $aTemplatePath = array();
    protected $oLoadResponse;
    protected $aLoadError;

    public function __construct($sPrefix, $aInitParams = array()) {
        $this->oDom = new DOMDocument('1.0', 'UTF-8');

        $this->oDom->formatOutput = false;
        $this->sPrefix = $sPrefix;
        $this->aInitParams = $aInitParams;

        if(empty($this->sServerClassName)) {
            $sParentClassName = get_class($this);
            $target = parseNS($sParentClassName);
            $this->sServerClassName = $target['ns'] . '\\' . (strpos(strtolower($target['className']), 'tpl') === 0 ? substr($target['className'], 3) : $target['className']);
        }
    }

    public function getTemplatePath() {
        $aPath = [];
        $aPath = array_merge($aPath, $this->aTemplatePath);
        if($this->sServerClassName) {
            $aPath[] = $this->getName();
        }
        return $aPath;
    }

    public function getName() {
        return $this->sServerClassName;
    }

    public function getStateName() {
        return implode('.', $this->getTemplatePath());
    }

    protected function findTemplatePath() {
        $aTrace = debug_backtrace();
        /** @var TPLBase $oParent */
        $oParent = false;
        foreach ($aTrace as $aTraceItem) {
            if(!empty($aTraceItem['object']) && $aTraceItem['object'] instanceof TPLBase && $aTraceItem['object'] != $this) {
                $oParent = $aTraceItem['object'];
                break;
            }
        }
        if($oParent) {
            $this->aTemplatePath = $oParent->getTemplatePath();
        } else {
            $this->aTemplatePath = '';
        }
    }

    protected function printStyles($sPrefix) {
    }

    protected function getJSFunction() {
        return 'null';
    }

    public function printBaseHtml() {
        $this->printStyles($this->sPrefix);
        ?>
        <div id="<?= $this->sPrefix ?>" class="form" style="<?= !empty($this->{'aConfig'}['autoVResize']) ? 'height:100%;' : '' ?>">
            <? $this->printHtml(); ?>
            <script type="text/javascript">
                <?php $aFormState = FormStates::getState($this->getStateName()); ?>
                document.getElementById("<?=$this->sPrefix?>").serverClassName = <?=json_encode($this->sServerClassName, JSON_UNESCAPED_UNICODE);?>;
                document.getElementById("<?=$this->sPrefix?>").stateName = <?=json_encode($this->getStateName(), JSON_UNESCAPED_UNICODE);?>;
                document.getElementById("<?=$this->sPrefix?>")._state = <?=json_encode($aFormState, JSON_UNESCAPED_UNICODE);?>;
                document.getElementById("<?=$this->sPrefix?>").fn = <?=$this->getJSFunction();?>;

                <?php
                $aInitResponse = $aInitError = null;
                if(is_callable(array($this->sServerClassName, 'init'))) {
                    $aRequest = array();
                    $aRequest['sortFields'] = $aFormState['grid.sort'];
                    $aRequest['filter']['id'] = $aFormState['grid.filter.selectedFilter'];
                    $aRequest['paging']['start'] = 0;
                    $aRequest['paging']['limit'] = !empty($aFormState['grid.paging.perPage']) ? $aFormState['grid.paging.perPage'] : $_SESSION['userdata']['row_limit'];

                    foreach(array_filter(array_keys($aFormState),function($k){return strpos($k,'menuButton-') === 0;}) as $k) {
                        $aRequest['fields'][str_replace(array('menuButton-','.value'),'',$k)] = $aFormState[$k];
                    }

                    $rpc = new JSONRPCServer();
                    $request = new stdClass();
                    $request->{'method'} = $this->sServerClassName.'.init';
                    $request->{'params'} = array($aRequest,$this->aInitParams);
                    $response = $rpc->handleRequest($request);

                    $aInitResponse = $response['result'];
                    $aInitError = $response['error'] ? $response['error']['data'] : null;
                }

                ?>
                document.getElementById("<?=$this->sPrefix?>").initResponse = <?=json_encode($aInitResponse, JSON_UNESCAPED_UNICODE);?>;
                document.getElementById("<?=$this->sPrefix?>").initError = <?=json_encode($aInitError, JSON_UNESCAPED_UNICODE);?>;
            </script>
        </div>
        <?php
    }

    public function printHtml() {
        echo 'empty template';
    }

    public function printHTMLWithoutFrame() {
        $this->findTemplatePath();
        $this->printBaseHtml();
    }

    public function printHTMLWithFrame($aFrameConfig = array()) {
        $this->printFrameStart($aFrameConfig);
        $this->printBaseHtml();
        $this->printFrameEnd($aFrameConfig);
    }

public function printFrameStart($aFrameConfig = array()) {
    $this->findTemplatePath();
    $aFrameConfig = array_merge($aFrameConfig, $this->aFrameConfig);
    $aFrameClasses = array('panel-frame');
    if(!empty($aFrameConfig['frameClass'])) $aFrameClasses[] = $aFrameConfig['frameClass'];
    $sTplName = strpos(get_class($this), 'TPL') === 0 ? substr(get_class($this), 3) : get_class($this);
    $aButtonsHTML = array();
    if(!empty($aFrameConfig) && !empty($aFrameConfig['title'])) {
        $sTitle = $aFrameConfig['title'];
    } else if(!empty($this->aConfig) && !empty($this->aConfig['title'])) {
        $sTitle = $this->aConfig['title'];
    } else {
        $sTitle = '';
    }
    if(!empty($aFrameConfig['showTitleRowCount']) && !empty($sTitle)) {
        $sTitleRowCountSpan = '<span class="title-row-count"></span>';
    } else {
        $sTitleRowCountSpan = '';
    }

    if(!empty($aFrameConfig) && !empty($aFrameConfig['tplName'])) {
        $sTplName = $aFrameConfig['tplName'];
    }

    if(!empty($aFrameConfig['collapsible'])) {
        $aFrameClasses[] = 'collapsible';
        $aButtonsHTML[] = '<button class="collapse"><div class="icon fa fa-caret-up"></div></button>';
        if(!empty($aFrameConfig['collapsed'])) {
            $aFrameClasses[] = 'collapsed';
        }
    }

    if(!empty($aFrameConfig['isWindow'])) {
        $aFrameClasses[] = 'frame-window';
    }

    if(!empty($aFrameConfig['closable'])) {
        $aButtonsHTML[] = '<button class="close"><div class="icon fa fa-close"></div></button>';
    }
    if(!empty($aFrameConfig['vResizable'])) {
        $aFrameClasses[] = 'v-resizable';
    }
    ?>
    <div id="<?= $this->sPrefix ?>-frame-container" class="<?= implode(' ', $aFrameClasses) ?>" tplName="<?= $sTplName ?>">
        <div class="panel-frame-top">
            <div class="panel-frame-buttons"><?= implode('', $aButtonsHTML) ?></div>
            <div class="panel-frame-title"><span class="title"><?= $sTitle ?></span><?= $sTitleRowCountSpan ?></div>
        </div>
        <div class="panel-frame-body">
            <?php
            }
            public function printFrameEnd(/** @noinspection PhpUnusedParameterInspection */
                $aFrameConfig = array()) {
            ?>

            <div class="clr"></div>
        </div>
        <div class="frame-v-resizer"></div>
    </div>
    <?php
}

    private $nGridCounter = 0;

    public function printResultTable($mHandler, $aConfig = array()) {
        //rows per page
        if(!isset($aConfig['possibleRowsPerPage'])) $aConfig['possibleRowsPerPage'] = array(10, 20, 30, 50, 100);
        if(!empty($_SESSION['userdata']['row_limit']) && !in_array($_SESSION['userdata']['row_limit'], $aConfig['possibleRowsPerPage'])) {
            $aConfig['possibleRowsPerPage'][] = $_SESSION['userdata']['row_limit'];
            sort($aConfig['possibleRowsPerPage'], SORT_NUMERIC);
        }
        if(!empty($_SESSION['userdata']['row_limit'])) {
            if($aConfig['rowsPerPage'] != "*")
                $aConfig['rowsPerPage'] = $_SESSION['userdata']['row_limit'];
        } else {
            if(!isset($aConfig['rowsPerPage']) || !in_array($aConfig['rowsPerPage'], $aConfig['possibleRowsPerPage'])) {
                $aConfig['rowsPerPage'] = reset($aConfig['possibleRowsPerPage']);
            }
        }

        $oHandler = '';
        if(is_string($mHandler)) {
            if(class_exists($mHandler)) {
                $oHandler = new $mHandler;
            }
        } elseif(is_object($mHandler)) {
            $oHandler = $mHandler;
        }

        $sGridPrefix = $this->sPrefix . '-grid-' . $this->nGridCounter++;
        if(!($oHandler instanceof GridPanelHandler)) {
            echo "handler class must inherit GridPanelHandler";
            return;
        }

        $oInitResponse = new Response();
        $oInitResponse->gridPrefix = $sGridPrefix;
        $oInitResponse->containerPrefix = $this->sPrefix;
        $oInitResponse->gridColumns = $oHandler->aColumns;
        $oInitResponse->tplCfg = $aConfig;
        $oInitResponse->defaultQuickFilters = $oHandler->aDefaultQuickFilters;
        $oInitResponse->headerRowCount = $oHandler->getHeaderRowCount($oInitResponse->gridColumns);

        $sTitle = '';
        if(!empty($aConfig['autoVResize'])) {
            if(!empty($aFrameConfig) && !empty($aFrameConfig['title'])) {
                $sTitle = $aFrameConfig['title'];
            } else if(!empty($this->aConfig) && !empty($this->aConfig['title'])) {
                $sTitle = $this->aConfig['title'];
            } else {
                $sTitle = get_class($oHandler);
            }
            if(empty($aConfig['topBarItems']) || !is_array($aConfig['topBarItems'])) $aConfig['topBarItems'] = array();
            foreach ($aConfig['topBarItems'] as $k => $aItem) {
                if(in_array(strtolower($aItem[0]), array('columnsbutton', 'filtersbutton'))) unset($aConfig['topBarItems'][$k]);
            }
            if(empty($this->bNoDefaultTopBarItems)) {
                $aDefaultTopBarItems = array(
                    array('div', array('class' => 'top-bar-title', 'align' => 'left', 'text' => $sTitle)),
                    array('filtersButton', array('align' => 'right')),
                    array('br')
                );
                $aConfig['topBarItems'] = array_merge($aDefaultTopBarItems, $aConfig['topBarItems']);

                //columns butona trqbva da e predi purviq align-right element v posledniq red :)
                $aConfig['topBarItems'] = array_reverse($aConfig['topBarItems']);
                $pos = 0;
                foreach ($aConfig['topBarItems'] as $k => $aItem) {
                    if(strtolower($aItem[0]) == 'br' || empty($aItem[1]['align']) || $aItem[1]['align'] != 'right') break;
                    $pos = $k + 1;
                }
                $aColumnsBtn = array('columnsButton', array('align' => 'right'));
                array_splice($aConfig['topBarItems'], $pos, 0, array($aColumnsBtn));
                $aConfig['topBarItems'] = array_reverse($aConfig['topBarItems']);
            }
        }

        $bHasFilters = false;
        if(!empty($aConfig['topBarItems'])) foreach ($aConfig['topBarItems'] as $item) {
            if(strtolower($item[0]) == 'filtersbutton') {
                $bHasFilters = true;
                break;
            }
        }
        if(!$bHasFilters && !empty($aConfig['bottomBarItems'])) foreach ($aConfig['bottomBarItems'] as $item) {
            if(strtolower($item[0]) == 'filtersbutton') {
                $bHasFilters = true;
                break;
            }
        }
        if($bHasFilters) {
            try {
                $oInitResponse->filters = DBGridFilters::getFiltersForCurrentUser($this->getName());
            } catch (Exception $e) {
                $oInitResponse->filters = array();
            }
        }
        ?>
        <style type="text/css" id="<?= $sGridPrefix ?>-styles"></style>
        <div id="<?= $sGridPrefix ?>-container" class="grid-container <?= !empty($aConfig['autoVResize']) ? 'auto-vresize' : '' ?>" gridname="<?= $this->getName() ?>" gridTitle="<?= htmlentities($sTitle, ENT_QUOTES, "UTF-8") ?>">
            <div class="grid-top-bar toolbar"><?= $this->createTopBarHTML($aConfig) ?></div>
            <div class="locked-columns">
                <div class="grid-headers-inner">
                    <table class="grid-headers">
                    </table>
                </div>
                <div class="grid-data">
                    <table class="grid-data"></table>
                    <div class="resizer-line"></div>
                </div>
            </div>
            <div class="normal-columns">
                <div class="grid-headers-outer">
                    <div class="grid-headers-inner">
                        <table class="grid-headers"></table>
                    </div>
                </div>
                <div class="grid-data">
                    <table class="grid-data"></table>
                    <div class="resizer-line"></div>
                </div>
            </div>
            <div class="grid-bottom-bar toolbar"><?= $this->createBottomBarHTML($aConfig) ?></div>
        </div>

        <script type="text/javascript">
            document.getElementById("<?=$sGridPrefix?>-container").initConfig = <?=json_encode($oInitResponse)?>;
        </script>
        <?php
    }

    protected function createToolBarHTML($aItems) {
        $aToolBarItemsHTML = array();
        foreach ($aItems as $item) {
            if(!is_array($item)) continue;
            if(empty($item[1]['align']) || !in_array($item[1]['align'], array('left', 'right', 'center'))) {
                $item[1]['align'] = 'left';
            }
            $aToolBarItemsHTML[] = call_user_func_array(array($this, 'createElementHTML'), $item);
        }
        $aToolBarItemsHTML[] = '<div class="clr"></div>';
        return implode('', $aToolBarItemsHTML);
    }

    protected function createBottomBarHTML($aConfig) {
        if(empty($aConfig['disablePaging'])) {
            if(empty($aConfig) || empty($aConfig['bottomBarItems'])) {
                return '<div class="paging-bar align-left"></div><div class="clr"></div>';
            } else {
                return '<div class="paging-bar align-left"></div>' . $this->createToolBarHTML($aConfig['bottomBarItems']) . '<div class="clr"></div>';
            }
        } else {
            if(empty($aConfig) || empty($aConfig['bottomBarItems'])) {
                return '';
            } else {
                return $this->createToolBarHTML($aConfig['bottomBarItems']);
            }
        }
    }

    protected function createTopBarHTML($aConfig) {
        if(empty($aConfig) || empty($aConfig['topBarItems'])) return '';
        return $this->createToolBarHTML($aConfig['topBarItems']);
    }

    public function printLabel($sText, $sFieldName = '', $aAttributes = array()) {
        if(!empty($sText)) $aAttributes['text'] = $sText;
        if(!empty($sFieldName)) $aAttributes['fieldName'] = $sFieldName;
        print $this->createLabelHTML($aAttributes);
    }

    public function createLabelHTML($aAttributes = array()) {
        $oLabel = $this->oDom->createElement('label');
        $oLabel->appendChild($this->oDom->createTextNode($aAttributes['text']));

        foreach ($aAttributes as $sName => $sValue) {
            $oLabel->setAttribute($sName, $sValue);
        }
        $this->oDom->appendChild($oLabel);
        return $this->oDom->saveXML($oLabel, LIBXML_NOEMPTYTAG);
    }

    /**
     *
     *
     * @param string $sTagName
     * @param string[] $aAttributes
     * @param array $aContent
     */
    public function printElement($sTagName, $aAttributes = array(), $aContent = array()) {
        print $this->createElementHTML($sTagName, $aAttributes, $aContent);
    }

    public function createElementHTML($sTagName, $aAttributes = array(), $aContent = array()) {
        if(empty($sTagName)) return '';


        unset($aAttributes['onclick']);
        unset($aAttributes['onchange']);
        unset($aAttributes['onkeypress']);
        unset($aAttributes['onkeydown']);
        unset($aAttributes['onselect']);
        foreach ($aAttributes as $k => $value) {
            if(is_null($value)) unset($aAttributes[$k]);
        }
        //align attr
        $aClasses = empty($aAttributes['class']) ? array() : explode(' ', $aAttributes['class']);
        if(!empty($aAttributes['align'])
            && !in_array('align-left', $aClasses)
            && !in_array('align-right', $aClasses)
            && !in_array('align-center', $aClasses)
        ) {
            if($aAttributes['align'] == 'left') $aClasses[] = 'align-left';
            if($aAttributes['align'] == 'right') $aClasses[] = 'align-right';
            if($aAttributes['align'] == 'center') $aClasses[] = 'align-center';
        }
        $aAttributes['class'] = implode(' ', $aClasses);
        unset($aAttributes['align']);
        $oEl = $this->oDom->createElement($sTagName);
        switch (strtolower($sTagName)) {
            case 'label'            :
                return $this->createLabelHTML($aAttributes);
                break;
            case 'menubutton'        :
                return $this->createMenuButtonHTML($aAttributes, $aContent);
                break;
            case 'splitbutton'        :
                return $this->createSplitButtonHTML($aAttributes, $aContent);
                break;
            case 'timemenubutton'    :
                return $this->createTimeMenuButtonHTML($aAttributes, $aContent);
                break;
            case 'columnsbutton'    :
                return $this->createColumnsButtonHTML($aAttributes);
                break;
            case 'filtersbutton'    :
                return $this->createFiltersButtonHTML($aAttributes);
                break;
            case 'suggestcombo'        :
                return $this->createSuggestComboHTML($aAttributes);
                break;
            case 'exportbutton'    :
                return $this->createExportButtonHTML($aAttributes);
                break;
            case 'input_select'        :
                return $this->createInputSelectHTML($aAttributes, $aContent);
                break;
            case 'lasteditedinfo'    :
                $oEl = $this->oDom->createElement('span');

                $aAttributes['name'] = empty($aAttributes['name']) ? 'last_edited_info' : $aAttributes['name'];
                if(strpos($aAttributes['class'], 'align-') === false) $aAttributes['class'] .= ' align-left';
                $aAttributes['class'] .= ' edited-info';

                $aAttributes = $aAttributes + $aContent;

                $oIcon = $this->oDom->createElement('div');
                $oIcon->appendChild($this->oDom->createTextNode(''));
                $style = "font-size: 20px;width: 26px;height: 26px;line-height:26px";
                $oIcon->setAttribute('class', 'icon icon-created-info fa fa-plus-square-o');
                $oIcon->setAttribute('style', $style);
                $oEl->appendChild($oIcon);
                $oIcon = $this->oDom->createElement('div', '');
                $oIcon->setAttribute('class', 'icon icon-updated-info fa fa-edit');
                $oIcon->setAttribute('style', $style);
                $oEl->appendChild($oIcon);
                break;
            case 'closebutton'        :
                $oEl = $this->oDom->createElement('button');
                $aAttributes['class'] = str_replace('align-left', '', $aAttributes['class']);
                $aAttributes['class'] .= ' close align-right';
                $oIcon = $this->oDom->createElement('div');
                $oIcon->appendChild($this->oDom->createTextNode(''));
                $oIcon->setAttribute('class', 'icon fa fa-close');
                $oEl->appendChild($oIcon);
                $oEl->appendChild($this->oDom->createElement('span', htmlentities(L('Затвори'), null, 'UTF-8')));
                break;
            case 'confirmbutton'    :
                $aAttributes['class'] = str_replace('align-left', '', $aAttributes['class']);
                $oEl = $this->oDom->createElement('button');
                $aAttributes['class'] .= ' confirm align-right';
                $oIcon = $this->oDom->createElement('div');
                $oIcon->appendChild($this->oDom->createTextNode(''));
                $oIcon->setAttribute('class', 'icon fa fa-check');
                $oEl->appendChild($oIcon);
                $oEl->appendChild($this->oDom->createElement('span', htmlentities(L('Потвърди'), null, 'UTF-8')));
                break;
            case 'deletebutton'    :
                $aAttributes['class'] = str_replace('align-left', '', $aAttributes['class']);
                $oEl = $this->oDom->createElement('button');
                $aAttributes['class'] .= ' delete align-right';
                $oIcon = $this->oDom->createElement('div');
                $oIcon->appendChild($this->oDom->createTextNode(''));
                $oIcon->setAttribute('class', 'icon fa fa-trash');
                $oEl->appendChild($oIcon);
                $oEl->appendChild($this->oDom->createElement('span', htmlentities(L('Изтрий'), null, 'UTF-8')));
                break;
            case 'select'            :
                if(!empty($aContent)) {
                    $aSelectValues = array();
                    if(!empty($aAttributes['value'])) {
                        if(is_array($aAttributes['value'])) {
                            foreach ($aAttributes['value'] as $val) {
                                $aSelectValues[$val] = $val;
                            }
                        } else {
                            $aSelectValues[$aAttributes['value']] = $aAttributes['value'];
                        }
                    }
                    $sValueField = !empty($aAttributes['valueField']) ? $aAttributes['valueField'] : 'id';
                    $sLabelField = !empty($aAttributes['labelField']) ? $aAttributes['labelField'] : 'value';
                    $sTitleField = !empty($aAttributes['titleField']) ? $aAttributes['titleField'] : 'value';
                    $sCustomField = !empty($aAttributes['customField']) ? $aAttributes['customField'] : 'custom_field';
                    $sGroupField = !empty($aAttributes['groupField']) ? $aAttributes['groupField'] : '';
                    /** @var \DOMElement[] $aGroups */
                    $aGroups = array(
                        '__select_' => $oEl
                    );

                    foreach ($aContent as $aItem) {
                        if(!is_array($aItem)) {
                            $aItem = array(
                                $sLabelField => $aItem,
                                $sValueField => $aItem
                            );
                        }
                        $oOption = $this->oDom->createElement('option', htmlentities($aItem[$sLabelField], null, 'UTF-8'));
                        if(!empty($aItem[$sTitleField])) $oOption->setAttribute('title', $aItem[$sTitleField]);
                        $oOption->setAttribute('value', $aItem[$sValueField]);
                        if(array_key_exists($sCustomField, $aItem)) {
                            $oOption->setAttribute('custom_field', $aItem[$sCustomField]);
                        }
                        if(array_key_exists($aItem[$sValueField], $aSelectValues)) {
                            $oOption->setAttribute('selected', true);
                        }
                        if($sGroupField && $aItem[$sGroupField]) {
                            if(empty($aGroups[$aItem[$sGroupField]])) {
                                $aGroups[$aItem[$sGroupField]] = $this->oDom->createElement('optgroup');
                                $aGroups[$aItem[$sGroupField]]->setAttribute('label', $aItem[$sGroupField]);
                                $aGroups['__select_']->appendChild($aGroups[$aItem[$sGroupField]]);
                            }
                            $aGroups[$aItem[$sGroupField]]->appendChild($oOption);
                        } else {
                            $aGroups['__select_']->appendChild($oOption);
                        }
                    }

                    unset($aAttributes['value']);
                } else {
                    $oEl->appendChild($this->oDom->createTextNode(''));
                }
                break;
            case 'textarea'            :
                if(!empty($aAttributes['value'])) {
                    $oEl->appendChild($this->oDom->createTextNode($aAttributes['value']));
                    unset($aAttributes['value']);
                } else {
                    $oEl->appendChild($this->oDom->createTextNode(''));
                }
                break;
            case 'button'            :
                if(!empty($aAttributes['iconCls'])) {
                    $oIcon = $this->oDom->createElement('div', '');
                    $oIcon->appendChild($this->oDom->createTextNode(''));
                    $oIcon->setAttribute('class', 'icon ' . $aAttributes['iconCls']);
                    $oEl->appendChild($oIcon);
                    unset($aAttributes['iconCls']);
                }
                if(!empty($aAttributes['iconImg'])) {
                    $oIcon = $this->oDom->createElement('img');
                    $oIcon->setAttribute('src', strpos($aAttributes['iconImg'], 'http:') === 0 ? $aAttributes['iconImg'] : BASE_URL . '/images/' . $aAttributes['iconImg']);
                    $oEl->appendChild($oIcon);
                    unset($aAttributes['iconImg']);
                }
                if(!empty($aAttributes['text'])) {

                    $oEl->appendChild($this->oDom->createElement('span', htmlentities($aAttributes['text'], null, 'UTF-8')));
                    unset($aAttributes['text']);
                }
                break;
            case "fileupload":
                $oEl = $this->oDom->createElement('span');
                $oEl->setAttribute('class', $aAttributes['class'] . ' file-upload');
                $oInput = $this->oDom->createElement('input');
                foreach ($aAttributes as $sName => $sValue) {
                    $oInput->setAttribute($sName, $sValue);
                }
                $oEl->appendChild($oInput);
                $this->oDom->appendChild($oEl);
                return $this->oDom->saveXML($oEl);
                break;
            case "keyvaluepairs":
                return $this->createKeyValuePairs($aAttributes);
                break;
            default :
                if(isset($aAttributes['text'])) {
                    $oEl->appendChild($this->oDom->createTextNode($aAttributes['text']));
                    unset($aAttributes['text']);
                }
                break;
        }
        foreach ($aAttributes as $sName => $sValue) {
            $oEl->setAttribute($sName, $sValue);
        }
        $this->oDom->appendChild($oEl);
        $sElHTML = $this->oDom->saveXML($oEl);

        return $sElHTML;
    }

    public function createSuggestComboHTML($aAttributes) {
        $aAttributes['class'] = empty($aAttributes['class']) ? 'suggest-combo' : 'suggest-combo ' . $aAttributes['class'];
        $aAttributes['type'] = 'text';
        $oDiv = $this->oDom->createElement('div');
        $oDiv->setAttribute('class', $aAttributes['class']);

        $oInputDiv = $this->oDom->createElement('div');
        $oInputDiv->setAttribute('class', 'input-div');
        $oInput = $this->oDom->createElement('input');
        $oInput->setAttribute('class', 'suggest-combo-input');

        foreach ($aAttributes as $attribute => $value) {
            if(0
                || $attribute == 'suggest'
                || $attribute == 'onSuggest'
                || $attribute == 'onsuggest'
                || $attribute == 'name'
                || $attribute == 'value'
                || $attribute == 'type'
            ) {
                $oInput->setAttribute($attribute, $value);
            } else {
                $oDiv->setAttribute($attribute, $value);
            }
        }

        $oInputDiv->appendChild($oInput);
        $oDiv->appendChild($oInputDiv);

        $oSpan = $this->oDom->createElement('span');
        $oSpan->setAttribute('class', 'icon fa fa-caret-down');
        $oSpan->appendChild($this->oDom->createTextNode(' '));
        $oDiv->appendChild($oSpan);

        return $this->oDom->saveXML($oDiv);
    }

    public function createSplitButtonHTML($aAttributes = array(), $aItems = array()) {
        if(empty($aAttributes)) $aAttributes = array();
        $aAttributes['class'] = empty($aAttributes['class']) ? 'split-button' : 'split-button ' . $aAttributes['class'];
        $oDiv = $this->oDom->createElement('div', '');

        foreach ($aAttributes as $k => $v) {
            $oDiv->setAttribute($k, $v);
        }

        $bHasSelected = false;

        foreach ($aItems as $aItem) {
            $oBtn = $this->oDom->createElement('button');
            if(!empty($aItem['selected']) && !$bHasSelected) {
                $aItem['class'] = empty($aItem['class']) ? 'selected' : $aItem['class'] . ' selected';
                $bHasSelected = true;
            }
            if(!empty($aItem['iconCls'])) {
                $oIcon = $this->oDom->createElement('div');
                $oIcon->appendChild($this->oDom->createTextNode(''));
                $oIcon->setAttribute('class', 'icon ' . $aItem['iconCls']);
                $oBtn->appendChild($oIcon);
                unset($aAttributes['iconCls']);
            }
            if(!empty($aItem['iconImg'])) {
                $oIcon = $this->oDom->createElement('img');
                $oIcon->setAttribute('src', strpos($aItem['iconImg'], 'http:') === 0 ? $aItem['iconImg'] : BASE_URL . '/images/' . $aItem['iconImg']);
                $oBtn->appendChild($oIcon);
                unset($aItem['iconImg']);
            }
            if(!empty($aItem['text'])) {
                $oBtn->appendChild($this->oDom->createElement('span', htmlentities($aItem['text'], null, 'UTF-8')));
                unset($aItem['text']);
            }
            foreach ($aItem as $k => $v) {
                $oBtn->setAttribute($k, $v);
            }
            $oDiv->appendChild($oBtn);
        }

        return $this->oDom->saveXML($oDiv);
    }


    /**
     * $aAttributes - атрибути на основния елемент, $aItems елементи в меню-то пр. array(array('name'=>'pdf','text'=>'export to pdf','iconCls'=>'icon-pdf'))
     *
     * @param string[] $aAttributes
     * @param array $aItems
     */
    public function createMenuButtonHTML($aAttributes, $aItems = array()) {
        if(empty($aAttributes)) $aAttributes = array();

        $aAttributes['class'] = empty($aAttributes['class']) ? 'menu-button' : 'menu-button ' . $aAttributes['class'];

        if(empty($aItems)) return '';

        $aSelectedItem = reset($aItems);
        if(!empty($aAttributes['selectedItem'])) {
            foreach ($aItems as $aItem) {
                if($aAttributes['selectedItem'] == $aItem['name']) {
                    $aSelectedItem = $aItem;
                    unset($aAttributes['selectedItem']);
                    break;
                }
            }
        }

        $bHasIcons = false;
        foreach ($aItems as $aItem) {
            if(!empty($aItem['iconCls'])) {
                $bHasIcons = true;
                break;
            }
        }

        $oMainUl = $this->oDom->createElement('ul', '');
        foreach ($aAttributes as $sName => $sValue) {
            $oMainUl->setAttribute($sName, $sValue);
        }
        $oBtnLi = $this->oDom->createElement('li', '');
        $oBtnLi->setAttribute('class', 'btn');
        $oBtn = $this->oDom->createElement('button');
        if($bHasIcons) {
            $oI = $this->oDom->createElement('div');
            $oI->appendChild($this->oDom->createTextNode(''));
            $oI->setAttribute('class', 'icon ' . (empty($aSelectedItem['iconCls']) ? '' : ' ' . $aSelectedItem['iconCls']));
            $oBtn->appendChild($oI);
        }
        $oBtn->appendChild($this->oDom->createElement('span', htmlentities($aSelectedItem['text'], null, 'UTF-8')));
        $oBtn->setAttribute('itemname', $aSelectedItem['name']);
        $oBtnLi->appendChild($oBtn);
        $oArrow = $this->oDom->createElement('span', '');
        $oArrow->setAttribute('class', 'arrow fa fa-caret-down');
        $oBtnLi->appendChild($oArrow);
        $oMainUl->appendChild($oBtnLi);

        $oListLi = $this->oDom->createElement('li');
        $oListLi->setAttribute('class', 'list');
        $oListTable = $this->oDom->createElement('table', '');


        //foreach po trudniq nachin zaradi $aNextItem
        reset($aItems);
        while ($aItem = current($aItems)) {
            $aNextItem = next($aItems);

            if(!empty($aItem['removed']))
                continue;

            $oRow = $this->oDom->createElement('tr');
            $aRowClass = array();
            if($aItem['name'] == "_") {
                //ako e posledniq ili ako sledvashtiq pak e separator ne se printira
                if(!$aNextItem) break;
                if($aNextItem['name'] == '_') continue;

                $oCell = $this->oDom->createElement('td');
                $oCell->setAttribute('colspan', $bHasIcons ? 2 : 1);
                $oCell->appendChild($this->oDom->createElement('div', ''));
                $aRowClass[] = 'separator';
                $oRow->appendChild($oCell);
            } else {
                if($bHasIcons) {
                    $oIconCell = $this->oDom->createElement('td');
                    $oIconCell->setAttribute('class', 'icon-cell');
                    $oI = $this->oDom->createElement('div');
                    $oI->appendChild($this->oDom->createTextNode(''));
                    $oI->setAttribute('class', 'icon' . (empty($aItem['iconCls']) ? '' : ' ' . $aItem['iconCls']));
                    $oIconCell->appendChild($oI);
                    $oRow->appendChild($oIconCell);
                }
                $oLabelCell = $this->oDom->createElement('td');
                $oLabelCell->setAttribute('class', 'label-cell');
                $oLabelCell->appendChild($this->oDom->createElement('span', htmlentities($aItem['text'], null, 'UTF-8')));
                $oRow->appendChild($oLabelCell);
                $oRow->setAttribute('itemname', $aItem['name']);
            }
            if(!empty($aItem['disabled'])) $aRowClass[] = 'disabled';
            $oRow->setAttribute('class', implode(' ', $aRowClass));
            $oListTable->appendChild($oRow);
        }
        $oListTable->setAttribute('cellspacing', '0');
        $oListLi->appendChild($oListTable);
        $oMainUl->appendChild($oListLi);

        return $this->oDom->saveXML($oMainUl);
    }

    const TIME_BUTTON_TODAY = 'today';
    const TIME_BUTTON_YESTERDAY = 'yesterday';
    const TIME_BUTTON_WEEK = 'week';
    const TIME_BUTTON_PREV_WEEK = 'prev_week';
    const TIME_BUTTON_NEXT_WEEK = 'next_week';
    const TIME_BUTTON_MONTH = 'month';
    const TIME_BUTTON_PREV_MONTH = 'prev_month';
    const TIME_BUTTON_NEXT_MONTH = 'next_month';

    public function createTimeMenuButtonHTML($aAttributes, $aItems = array()) {
        if(empty($aAttributes)) $aAttributes = array();
        $aAttributes['class'] = empty($aAttributes['class']) ? 'time-period menu-button' : 'time-period menu-button ' . $aAttributes['class'];

        $aDefaultItems = array(
            array(
                'name' => self::TIME_BUTTON_TODAY,
                'text' => L("Днес"),
                'iconCls' => 'fa fa-calendar fa-fw'
            ),
            array(
                'name' => self::TIME_BUTTON_YESTERDAY,
                'text' => L("Вчера"),
                'iconCls' => 'fa fa-calendar fa-fw'
            ),
            array(
                'name' => self::TIME_BUTTON_WEEK,
                'text' => L("Тази седмица"),
                'iconCls' => 'fa fa-calendar fa-fw'
            ),
            array(
                'name' => self::TIME_BUTTON_PREV_WEEK,
                'text' => L("Предходната седмица"),
                'iconCls' => 'fa fa-calendar fa-fw'
            ),
            array(
                'name' => self::TIME_BUTTON_NEXT_WEEK,
                'text' => L("Следващата седмица"),
                'iconCls' => 'fa fa-calendar fa-fw'
            ),
            array(
                'name' => self::TIME_BUTTON_MONTH,
                'text' => L("Този месец"),
                'iconCls' => 'fa fa-calendar fa-fw'
            ),
            array(
                'name' => self::TIME_BUTTON_PREV_MONTH,
                'text' => L("Предходния месец"),
                'iconCls' => 'fa fa-calendar fa-fw'
            ),
            array(
                'name' => self::TIME_BUTTON_NEXT_MONTH,
                'text' => L("Следващия месец"),
                'iconCls' => 'fa fa-calendar fa-fw'
            ),
            array(
                'name' => 'range',
                'text' => L("Интервал"),
                'iconCls' => 'fa fa-calendar fa-fw'
            ),
        );
        if(!empty($aAttributes['addOptName'])) {
            array_unshift($aDefaultItems, array(
                'name' => $aAttributes['addOptName'],
                'text' => L($aAttributes['addOptText']),
                'iconCls' => $aAttributes['addOptIconCls']
            ));
        }
        $aItems = array_merge($aItems, $aDefaultItems);

        return $this->createMenuButtonHTML($aAttributes, $aItems);
    }

    public function createColumnsButtonHTML($aAttributes) {
        $oUl = $this->oDom->createElement('ul', '');
        $oUl->appendChild($this->oDom->createTextNode(''));
        $aAttributes['class'] = empty($aAttributes['class']) ? 'columns-button' : 'columns-button ' . $aAttributes['class'];
        foreach ($aAttributes as $sName => $sValue) {
            $oUl->setAttribute($sName, $sValue);
        }
        $oBtnLi = $this->oDom->createElement('li', '');
        $oBtnLi->appendChild($this->oDom->createTextNode(''));
        $oBtnLi->setAttribute('class', 'btn');
        $oBtn = $this->oDom->createElement('button', '');
        $oBtn->appendChild($this->oDom->createTextNode(''));

        $oIcon = $this->oDom->createElement('i', '');
        $oIcon->appendChild($this->oDom->createTextNode(''));
        $oIcon->setAttribute('class', 'icon fa fa-table');
        $oBtn->appendChild($oIcon);

        $oText = $this->oDom->createElement('span', htmlentities(L('колони'), null, 'UTF-8'));
        $oText->appendChild($this->oDom->createTextNode(''));
        $oBtn->appendChild($oText);
        $oBtnLi->appendChild($oBtn);

        $oArrowIcon = $this->oDom->createElement('span', '');
        $oArrowIcon->appendChild($this->oDom->createTextNode(''));
        $oArrowIcon->setAttribute('class', 'arrow fa fa-caret-down');
        $oBtnLi->appendChild($oArrowIcon);

        $oUl->appendChild($oBtnLi);
        $oListLi = $this->oDom->createElement('li', '');
        $oListLi->appendChild($this->oDom->createTextNode(''));
        $oListLi->setAttribute('class', 'list');
        $oListDiv = $this->oDom->createElement('div', '');
        $oListDiv->appendChild($this->oDom->createTextNode(''));
        $oListDiv->setAttribute('class', 'list-container');
        $oListInnerDiv = $this->oDom->createElement('div', '');
        $oListInnerDiv->appendChild($this->oDom->createTextNode(''));
        $oListInnerDiv->setAttribute('class', 'list-inner');
        $oListTable = $this->oDom->createElement('table', '');
        $oListTable->appendChild($this->oDom->createTextNode(''));
        $oListTable->setAttribute('cellspacing', '0');
        $oListInnerDiv->appendChild($oListTable);
        $oListDiv->appendChild($oListInnerDiv);

        $oListLi->appendChild($oListDiv);
        $oUl->appendChild($oListLi);
        return $this->oDom->saveXML($oUl);
    }

    protected function createFiltersButtonHTML($aAttributes) {
        $oUl = $this->oDom->createElement('ul', '');
        $oUl->appendChild($this->oDom->createTextNode(''));
        $aAttributes['class'] = empty($aAttributes['class']) ? 'filters-button' : 'filters-button ' . $aAttributes['class'];
        foreach ($aAttributes as $sName => $sValue) {
            $oUl->setAttribute($sName, $sValue);
        }
        $oBtnLi = $this->oDom->createElement('li', '');
        $oBtnLi->appendChild($this->oDom->createTextNode(''));
        $oBtnLi->setAttribute('class', 'btn');
        $oBtn = $this->oDom->createElement('button', '');
        $oBtn->appendChild($this->oDom->createTextNode(''));
        $oBtn->setAttribute('class', 'filter-btn');
        $oIcon = $this->oDom->createElement('div', '');
        $oIcon->appendChild($this->oDom->createTextNode(''));
        $oIcon->setAttribute('class', 'icon fa fa-filter');
        $oBtn->appendChild($oIcon);
        $oText = $this->oDom->createElement('span', htmlentities(L('Избери филтър'), null, 'UTF-8'));
        $oText->appendChild($this->oDom->createTextNode(''));
        $oBtn->appendChild($oText);

        $oBtnLi->appendChild($oBtn);
        $oArrowIcon = $this->oDom->createElement('span', '');
        $oArrowIcon->appendChild($this->oDom->createTextNode(''));
        $oArrowIcon->setAttribute('class', 'arrow fa fa-caret-down');
        $oBtnLi->appendChild($oArrowIcon);
        $oUl->appendChild($oBtnLi);

        $oListLi = $this->oDom->createElement('li', '');
        $oListLi->appendChild($this->oDom->createTextNode(''));
        $oListLi->setAttribute('class', 'list');
        $oListDiv = $this->oDom->createElement('div', '');
        $oListDiv->appendChild($this->oDom->createTextNode(''));
        $oListDiv->setAttribute('class', 'list-container');
        $oListInnerDiv = $this->oDom->createElement('div', '');
        $oListInnerDiv->appendChild($this->oDom->createTextNode(''));
        $oListInnerDiv->setAttribute('class', 'list-inner');

        $oListTable = $this->oDom->createElement('table', '');
        $oListTable->appendChild($this->oDom->createTextNode(''));
        $oListTable->setAttribute('cellspacing', '0');
        $oListInnerDiv->appendChild($oListTable);

        $oAddNewButton = $this->oDom->createElement('button', '');
        $oAddNewButton->appendChild($this->oDom->createTextNode(''));
        $oAddNewButton->setAttribute('class', 'new-filter');

        $oAddNewButtonIcon = $this->oDom->createElement('div', '');
        $oAddNewButtonIcon->appendChild($this->oDom->createTextNode(''));
        $oAddNewButtonIcon->setAttribute('class', 'icon fa fa-plus-circle');

        $oAddNewButton->appendChild($oAddNewButtonIcon);

        $oAddNewButton->appendChild($this->oDom->createElement('span', htmlentities(L('Добави'), null, 'UTF-8')));
        $oListInnerDiv->appendChild($oAddNewButton);
        $oListDiv->appendChild($oListInnerDiv);

        $oListLi->appendChild($oListDiv);
        $oUl->appendChild($oListLi);
        return $this->oDom->saveXML($oUl);
    }

    protected function createKeyValuePairs($aAttributes) {
        ob_start();
        ?>
        <ul name="<?=$aAttributes['name']?>" class="keyvaluepairs <?=$aAttributes['class'];?>" style="<?=$aAttributes['style'];?>">
            <li class="inputs">
                <?php foreach($aAttributes['options'] as $option):?>
                    <span name="<?=$option['key']?>" suffix="<?=$option['suffix']?>">
                        <label><?=$option['label']?></label>
                        <input class="<?=$option['class']?>" style="<?=$option['style']?>" keyRestriction="<?=$option['keyRestriction']?>" type="text"/>
                        <?=$option['suffix'];?>
                    </span>
                <?php endforeach;?>
                <?=$this->createElementHTML('button', array('iconCls'=>'icon fa fa-plus', 'class' => 'add'));?>
            </li>
            <li><ul class="options"></ul></li>
        </ul>
        <?php return ob_get_clean();
    }


    public function createExportButtonHTML($aAttributes) {
        $aClasses = empty($aAttributes['class']) ? array() : explode(' ', $aAttributes['class']);
        if(!in_array('export-button', $aClasses)) $aClasses[] = 'export-button';
        $aAttributes['class'] = implode(' ', $aClasses);
        return $this->createMenuButtonHTML(
            $aAttributes,
            array(
//                array('text' => L('pdf'),'name' => 'PDF','iconCls' => 'fa fa-file-pdf-o fa-fw'),
                array('text' => L('xls'), 'name' => 'Excel5', 'iconCls' => 'fa fa-file-excel-o fa-fw'),
                array('text' => L('xlsx'), 'name' => 'ExcelOpenXML', 'iconCls' => 'fa fa-file-excel-o fa-fw'),
                array('text' => L('csv'), 'name' => 'CSV', 'iconCls' => 'fa fa-file-text-o fa-fw'),
            )
        );
    }

    public function createInputSelectHTML($aAttributes, $aContent) {
        //kopirame align-left/right ot select-a v inputa
        $sInputAlignClass = '';
        if(!empty($aAttributes['class'])) foreach (explode(' ', $aAttributes['class']) as $sClass) {
            if(strpos($sClass, 'align') === 0) {
                $sInputAlignClass = $sClass;
                break;
            }
        }
        $sInput = $this->createElementHTML('input', array('type' => 'text', 'input_select' => $aAttributes['name'], 'style' => 'width:50px;', 'class' => $sInputAlignClass));
        $sSelect = $this->createElementHTML('select', $aAttributes, $aContent);

        return $sInput . '&nbsp;' . $sSelect;
    }


}