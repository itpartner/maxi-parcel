<?php
namespace main;

class Exception extends \Exception {
    public $inner;
    public $fields;
    public $logID;
    public $additionalData;

    public function __construct($message='',$fields = array(),$inner = array()) {
        $this->fields = $fields;
        $this->inner = $inner;
        parent::__construct($message,1);
    }

    public function getLogObject() {
        return self::convertToLogObject($this);
    }

    public static function convertToLogObject(\Exception $e) {
        $obj = (object)get_object_vars($e);
        unset($obj->inner);
        $trace = $e->getTrace();
        foreach ($trace as $k => $aItem) {
            unset($trace[$k]['args']);
        }
        $obj->trace = $trace;
        $obj->inner = array();
        if(is_array($e->{'inner'})) foreach($e->{'inner'} as $ee) if($ee instanceof \Exception) $obj->inner[] = self::convertToLogObject($ee);
        return $obj;
    }

    public function getJSObject() {
        return self::convertToJSObject($this);
    }

    public static function convertToJSObject(\Exception $e) {
        $aJSObj = array(
            'type' => end(explode('\\', get_class($e))),
            'message' => nl2br($e->getMessage()),
            'log_id' => $e->{'logID'},
            'targetFields' => is_array($e->{'fields'}) ? array_map(function($v){return (string)$v;},$e->{'fields'}) : (empty($e->{'fields'}) ? array() : array((string)$e->{'fields'})),
            'inner' => array(),
            'additional_data' => $e instanceof Exception ? $e->{'additionalData'} : '',
            'has_logs_access' => Access::hasAccess('main/logs/SystemLog')
        );
        if(is_array($e->{'inner'})) foreach($e->{'inner'} as $ee) if($ee instanceof \Exception)  $aJSObj['inner'][] = self::convertToJSObject($ee);
        return $aJSObj;
    }

    public static function exceptionFromJSObject($jsObject) {
        if(is_object($jsObject)) $jsObject = json_decode(json_encode($jsObject),true);
        $e = new Exception($jsObject['message'],$jsObject['targetFields']);
        $e->{'externalLogID'} = $jsObject['log_id'];
        $e->inner = array();
        if(is_array($jsObject['inner'])) foreach($jsObject['inner'] as $innerObject) $e->inner[] = self::exceptionFromJSObject($innerObject);
        return $e;
    }

	public static function checkThrow($errors, $msg = '') {
		if (!empty($errors)) throw new InvalidParameter($msg, null, $errors);
	}

    /**
     * @return mixed
     */
    public function getAdditionalData() {
        return $this->additionalData;
    }

    /**
     * @param mixed $additionalData
     */
    public function setAdditionalData($additionalData) {
        $this->additionalData = $additionalData;
    }
} 