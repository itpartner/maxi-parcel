<?php
namespace main\language;
use main\InvalidParameter;

class Language {
	private static $aTranslations = array();
	
	private static $sLanguage = '';
	
	private static function init() {
	}

	/**
	 * връща преведено име на ден от седмицата по номер пр: 2 -> вторник
	 * @param integer $nDay
	 * @return string
	 */
	public static function getWeekDayName($nDay) {
		global $aWeekDay;
		if(count($aWeekDay) != 7) throw new InvalidParameter;
		if($nDay < 1 || $nDay > 7) throw new InvalidParameter;
		return self::getTranslationForText($aWeekDay[$nDay],'default');
	}

	public static function getPHPDateFormat($sLang = '') {
		$sFormat = self::getTranslationForText('php_date_format','default',$sLang);
		return $sFormat == 'php_date_format' ? 'd.m.Y' : $sFormat;
	}
	
	public static function getPHPDateTimeFormat($sLang = '') {
		$sFormat = self::getTranslationForText('php_date_time_format','default',$sLang);
		return $sFormat == 'php_date_time_format' ? 'd.m.Y H:i:s' : $sFormat;
	}
	
	public static function getPHPTimeFormat($sLang = '') {
		$sFormat = self::getTranslationForText('php_time_format','default',$sLang);
		return $sFormat == 'php_time_format' ? 'H:i:s' : $sFormat;
	}
	
	public static function getMySQLDateTimeFormat($sLang = '') {
		$sFormat = self::getTranslationForText('mysql_date_time_format','default',$sLang);
		return $sFormat == 'mysql_date_time_format' ? '%d.%m.%Y %H:%i:%s' : $sFormat;
	}
	
	public static function getMySQLDateFormat($sLang = '') {
		$sFormat = self::getTranslationForText('mysql_date_format','default',$sLang);
		return $sFormat == 'mysql_date_format' ? '%d.%m.%Y' : $sFormat;
	}
	
	public static function getMySQLTimeFormat($sLang = '') {
		$sFormat = self::getTranslationForText('mysql_time_format','default',$sLang);
		return $sFormat == 'mysql_time_format' ? '%H:%i:%s' : $sFormat;
	}
	
	public static function getCurrentLanguage() {
		self::init();
		return self::$sLanguage;
	}
	
	public static function setCurrentLanguage($sLang) {
		self::init();
		if(array_key_exists($sLang, self::$aTranslations)) {
			self::$sLanguage = $sLang;
		}
		return self::$sLanguage;
	}
	
	public static function getLangSuffix() {
		return self::getCurrentLanguage() == 'bg' ? '' : '_en';
	}
	
	public static function getTranslationsForFile($sFileName, $sLang = '') {
		self::init();
		if(!array_key_exists($sLang, self::$aTranslations)) $sLang = self::$sLanguage;
		if(!array_key_exists($sFileName, self::$aTranslations[$sLang])) {
			self::loadTranslations($sFileName);
		}
		return self::$aTranslations[$sLang][$sFileName];
	}

	public static function getTranslationForText($sText, $sFileName = '',$sLang = '') {
		self::init();
		return $sText;
	}
	
	protected static function loadTranslations($sFileName) {
		self::init();
	}
}
