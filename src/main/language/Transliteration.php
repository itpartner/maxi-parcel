<?php
namespace main\language;

class Transliteration {

    public static function convertCyr2PhoLower($sString) {
        $aConv = array();
        $aConv['А'] = 'A';
        $aConv['Б'] = 'B';
        $aConv['В'] = 'V';
        $aConv['Г'] = 'G';
        $aConv['Д'] = 'D';
        $aConv['Е'] = 'E';
        $aConv['Ж'] = 'Zh';
        $aConv['З'] = 'Z';
        $aConv['И'] = 'I';
        $aConv['Й'] = 'Y';
        $aConv['К'] = 'K';
        $aConv['Л'] = 'L';
        $aConv['М'] = 'M';
        $aConv['Н'] = 'N';
        $aConv['О'] = 'O';
        $aConv['П'] = 'P';
        $aConv['Р'] = 'R';
        $aConv['С'] = 'S';
        $aConv['Т'] = 'T';
        $aConv['У'] = 'U';
        $aConv['Ф'] = 'F';
        $aConv['Х'] = 'H';
        $aConv['Ц'] = 'Ts';
        $aConv['Ч'] = 'Ch';
        $aConv['Ш'] = 'Sh';
        $aConv['Щ'] = 'Sht';
        $aConv['Ь'] = 'A';
        $aConv['Ъ'] = 'Y';
        $aConv['Ю'] = 'Yu';
        $aConv['Я'] = 'Ya';
        $aConv['а'] = 'a';
        $aConv['б'] = 'b';
        $aConv['в'] = 'v';
        $aConv['г'] = 'g';
        $aConv['д'] = 'd';
        $aConv['е'] = 'e';
        $aConv['ж'] = 'zh';
        $aConv['з'] = 'z';
        $aConv['и'] = 'i';
        $aConv['й'] = 'y';
        $aConv['к'] = 'k';
        $aConv['л'] = 'l';
        $aConv['м'] = 'm';
        $aConv['н'] = 'n';
        $aConv['о'] = 'o';
        $aConv['п'] = 'p';
        $aConv['р'] = 'r';
        $aConv['с'] = 's';
        $aConv['т'] = 't';
        $aConv['у'] = 'u';
        $aConv['ф'] = 'f';
        $aConv['х'] = 'h';
        $aConv['ц'] = 'ts';
        $aConv['ч'] = 'ch';
        $aConv['ш'] = 'sh';
        $aConv['щ'] = 'sht';
        $aConv['ь'] = 'a';
        $aConv['ъ'] = 'y';
        $aConv['ю'] = 'yu';
        $aConv['я'] = 'ya';

        $aConv['Ă'] = 'A';
        $aConv['ă'] = 'a';
        $aConv['Â'] = 'A';
        $aConv['â'] = 'a';
        $aConv['Î'] = 'I';
        $aConv['î'] = 'i';
        $aConv['Ș'] = 'S';
        $aConv['ș'] = 's';
        $aConv['Ț'] = 'T';
        $aConv['ț'] = 't';

        // ХАК според закона за транслация
        $sNewString = str_replace( 'ия ', 'иа ', $sString." ");
        $sString = substr( $sNewString, 0, -1 );

        $nLength = mb_strlen($sString, "UTF-8");
        $sOutput = '';


        for ($i = 0; $i < $nLength; $i++) {

            $sChar = mb_substr($sString, $i, 1, "UTF-8");
            $sCharTranslated = isset($aConv[$sChar]) ? $aConv[$sChar] : $sChar;
            $nCharLength = mb_strlen($sCharTranslated, "UTF-8");

            if ($nCharLength > 1 && self::isUpper($sChar)) {
                // имаме главна буква проверяваме предходната и следващата
                // ако са главни правим и тазиглвана

                if (
                    (isset($sString{$i - 1}) && self::isUpper(mb_substr($sString, $i - 1, 1, "UTF-8")))
                    ||
                    (isset($sString{$i + 1}) && self::isUpper(mb_substr($sString, $i + 1, 1, "UTF-8")))
                ) {
                    $sCharTranslated = strtoupper($sCharTranslated);
                }

            }
            $sOutput .= $sCharTranslated;
        }

        return $sOutput;
    }

    //Фунцкия за конвертиране от латиница на кирилица.
    public static function LatinToCyr($sString){
        mb_internal_encoding("UTF-8");
        $sString = (mb_convert_case($sString,MB_CASE_LOWER));
        $Tmp = array();
        for ($i = 0; $i < mb_strlen($sString); $i++ ) {
            $Tmp[] = mb_substr($sString, $i, 1);
        }
        $sNewString = '';
        $cyrilic = array('ч','щ','ш','ю','а','б','в','г','д','е','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ъ','ь','я'/*,'Ч','Щ','Ш','Ю','А','Б','В', 'Г','Д','Е','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ъ','Ь','Я','У'*/);
        $latin = array('ch','sht','sh','yu','a','b','v','g','d','e','zh','z','i','y','k','l','m','n','o','p','r','s','t','u','f','h','ts','a','y','ya'/*,'Ch','SHT','SH','IU','A','B','V', 'G','D','E','J','Z','I','J','K','L','M','N','O','P','R','S','T','U','F','H','C','Y','Y','Q','W'*/);
        $i = 0;
        $nLength = count($Tmp);
        while($i < $nLength) {
            if(($Tmp[$i] === $latin[21]) || ($Tmp[$i] === 'c') || ($Tmp[$i] === $latin[13]) || ($Tmp[$i] === $latin[11]) || ($Tmp[$i] === $latin[22])) {
                //За случаите на 'Ш и Щ'
                if ($Tmp[$i] === $latin[21]) {
                    if ($Tmp[$i + 1] === $latin[25]) {
                        if ($Tmp[$i + 2] === $latin[22]) {
                            $sNewString .= $cyrilic[1];
                            $i = $i + 3;
                        } else {
                            $sNewString .= $cyrilic[2];
                            $i = $i + 2;
                        }
                    } else {
                        $sNewString .= $cyrilic[21];
                        $i = $i + 1;
                    }
                }else
                    //За случаите на 'ч'
                    if ($Tmp[$i] === 'c') {
                        if ($Tmp[$i + 1] === $latin[25]) {
                            $sNewString .= $cyrilic[0];
                            $i = $i + 2;
                        } else {
                            $sNewString .= $cyrilic[26];
                            $i = $i + 1;
                        }
                    }else
                        //За случаите на 'ю'
                        if ($Tmp[$i] === $latin[13]) {
                            if ($Tmp[$i + 1] === $latin[23]) {
                                $sNewString .= $cyrilic[3];
                                $i = $i + 2;
                            } else //За случаите на 'я'
                                if ($Tmp[$i + 1] === $latin[4]) {
                                    $sNewString .= $cyrilic[29];
                                    $i = $i + 2;
                                } else {
                                    $sNewString .= $cyrilic[13];
                                    $i = $i + 1;
                                }
                        }

                //За случаите на 'ж'
                if ($Tmp[$i] === $latin[11]) {
                    if ($Tmp[$i + 1] === $latin[25]) {
                        $sNewString .= $cyrilic[10];
                        $i = $i + 2;
                    } else {
                        $sNewString .= $cyrilic[11];
                        $i = $i + 1;
                    }
                }
                //За случаите на 'ц'
                if ($Tmp[$i] === $latin[22]) {
                    if ($Tmp[$i + 1] === $latin[21]) {
                        $sNewString .= $cyrilic[26];
                        $i = $i + 2;
                    } else {
                        $sNewString .= $cyrilic[22];
                        $i = $i + 1;
                    }
                }
            }
            else {
                $sNewString .= (array_search($Tmp[$i],$latin) !== false) ? $cyrilic[array_search($Tmp[$i],$latin)] : $Tmp[$i];
                $i++;
            }
        }

        $sNewString = str_replace( 'иа ', 'ия ', $sNewString." ");
        return trim($sNewString);
    }


    public static function isUpper($sString) {
        $sString = trim($sString);
        return (!empty($sString) && mb_strtoupper($sString, "UTF-8") == $sString);
    }

}