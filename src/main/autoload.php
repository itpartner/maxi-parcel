<?php

spl_autoload_register(function($name) {
    if (preg_match("/^main\\\\([a-zA-Z\\d\\\\]+)$/", $name, $matches)) include dirname(__FILE__) . '/' . str_replace('\\', '/', $matches[1]) . '.php';
});

$GLOBALS['parseNSCache'] = array();
function parseNS($path) {
    if($GLOBALS['parseNSCache'][$path]) return $GLOBALS['parseNSCache'][$path];
    $path = str_replace('\\','/',$path);
    $pathInfo = pathinfo($path);
    $result['ns'] = $pathInfo['dirname'][0] == '/' ? $pathInfo['dirname'] : '/'.$pathInfo['dirname'];
    $result['class'] = $result['ns'].'/'.$pathInfo['filename'];
    $result['className'] = $pathInfo['filename'];
    $result['method'] = $pathInfo['extension'];
    $result = array_map(function($v){return str_replace('/','\\',$v);},$result);
    $GLOBALS['parseNSCache'][$path] = $result;
    return $result;
}

function L($sText) {
    return @call_user_func_array('sprintf', func_get_args());
}