<?php
namespace main;

class InvalidParameter extends Exception {
    function __construct($message = '', $fields = array(), $inner = array()) {
        if(empty($message)) $message = L('Невалиден параметър');
        parent::__construct($message, $fields, $inner);
    }
}