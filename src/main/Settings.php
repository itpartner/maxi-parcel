<?php
namespace main;
use main\db\DBC;

class Settings{
	public static $aSettings = array();

	public static function loadSettings() {
		$aRows = DBC::$slave->select("SELECT * FROM settings WHERE is_static = 1");
		$aSettings = array();
		foreach ($aRows as $aRow) {
			if(!array_key_exists($aRow['type'], $aSettings)) $aSettings[$aRow['type']] = array();
			if(!array_key_exists($aRow['name'], $aSettings[$aRow['type']])) $aSettings[$aRow['type']][$aRow['name']] = array();
			if($aRow['subtype'] == 'structure') {
				$aDecoded = @json_decode($aRow['value'],true);
				if(!empty($aDecoded)) {
					$aSettings[$aRow['type']][$aRow['name']][] = self::convertJSONTreeToArray($aDecoded[0]);
				} else  {
					$aSettings[$aRow['type']][$aRow['name']][] = array();
				}
			} else {
				$aSettings[$aRow['type']][$aRow['name']][] = $aRow['value'];
			}
		}
		self::$aSettings = $aSettings;
	}
	
	public static function convertJSONTreeToArray($node) {
		$aRes = array();
		if($node['attr']['rel'] == 'key') {
			if(!empty($node['children'][0]['data']))
			return $node['children'][0]['data'];
		} else {
			if(!empty($node['children'])) foreach ($node['children'] as $child) {
				$aRes[$child['data']] = self::convertJSONTreeToArray($child);
			}
		}
		return $aRes;
	}
}
Settings::loadSettings();