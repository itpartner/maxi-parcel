<?php
namespace main\suggest;

use main\db\DBC;
use main\Exception;
use main\rpc\JSONRPCServer;
use main\Settings;
use stdClass;

class Suggest {
    const SUGGEST_MIN_WORD_LENGTH = 3;
    const SUGGEST_RESULT_MAX_COUNT = 8;

    public function handle() {
        $aRequest = $_REQUEST;

        try {
            $rpc = new JSONRPCServer();
            $request = new stdClass();
            $request->{'method'} = $aRequest['method'];
            unset($aRequest['method']);
            $request->{'params'} = array($aRequest);
            $response = $rpc->handleRequest($request, false);

            $this->printSuggest($aRequest, $response);
        } catch (Exception $e) {
            $this->printSuggest($aRequest, array('error' => $e->getJSObject()));
        }
    }

    private function printSuggest($aRequest, $response) {
        $oResponse = new \stdClass();
        $oResponse->query = $aRequest['query'];
        $oResponse->data = $response['result'];
        if($response['error']) $oResponse->error = $response['error'];
        header("Content-type: application/json");
        die(json_encode($oResponse, JSON_UNESCAPED_UNICODE));
    }

    public static function select($aResult) {
        $aResult['limit'] = intval($aResult['limit']) > 0 ? intval($aResult['limit']) : self::SUGGEST_RESULT_MAX_COUNT;
        $aResult['order'] = !empty($aResult['order']) ? $aResult['order'] : 'value';

        $sQuery = $aResult['select'];
        if(!empty($aResult['where']) && is_array($aResult['where'])) $sQuery = sprintf(" %s \n WHERE \n %s \n ", $sQuery, implode(' AND ', $aResult['where']));
        if(!empty($aResult['group'])) $sQuery = sprintf(" %s \n GROUP BY \n %s \n ", $sQuery, $aResult['group']);
        if(!empty($aResult['having']) && is_array($aResult['having'])) $sQuery = sprintf(" %s \n HAVING \n %s \n ", $sQuery, implode(' AND ', $aResult['having']));
        if(!empty($aResult['order'])) $sQuery = sprintf(" %s \n ORDER BY %s \n ", $sQuery, trim($aResult['order'], '_'));
        if(!empty($aResult['limit'])) $sQuery = sprintf(" %s \n LIMIT %s \n ", $sQuery, $aResult['limit']);
        return DBC::$suggest->select($sQuery);
    }
}
