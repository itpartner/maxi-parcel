<?php
namespace main\settings;
use main\grid\TPLGridPanel;

class TPLSettings extends TPLGridPanel {

    public function __construct( $sPrefix, $aInitParams = array() ) {
        $this->{'aConfig'} = array (
            'title'         => L('Параметри')   ,
            'autoVResize'   => true ,
            'bottomBarItems'=> array(
                array('exportbutton' , array( 'align' => 'right' ) )
            )
        );
        parent::__construct($sPrefix, $aInitParams);
    }

    public function getJSFunction() {return <<<'JS'

    (function(container, prefix, aBaseParams) {

        container.grid.clickListeners['btnEdit'] = function (rowNum, fieldElement, htmlElement) {
            var conf={};
            conf.baseParams={};
            conf.baseParams['editID'] = fieldElement['id'];
            conf.baseParams['subtype'] = fieldElement['subtype'];
            conf.baseParams['value'] = fieldElement['value'];
            conf.baseParams['pf'] = function(){return container};
            conf.baseParams['parentForm'] = function(){return container};
            conf.tplName = 'main/settings/SettingsEdit';
            conf.title = L('Редактиране на системни настройки') + " (" + fieldElement['name'] +  ")";
            framework.createWindow(conf);
        };

        $(container).find("button[name=btnSearch]").click(function(){
            container.grid.pagingBar.setPage(1);
        });
    });

JS;
    }
}
