<?php
namespace main\settings;
use main\TPLBase;

class TPLSettingsEdit extends TPLBase {

    public function printHtml() {
        $sPrefix = $this->sPrefix;
        ?>
        <style type="text/css" id="<?=$sPrefix?>-styles">
            #<?=$sPrefix?> .editValueArea {
                margin-top:21px;
                padding-right:1px;
                display:none;
            }
            #<?=$sPrefix?> .editStructureArea {
                               margin-top:21px;
                               width:100%;
                               display:none
                           }
        </style>
        <div id="<?=$sPrefix?>" >
            <div class="editValueArea">
                <label><?=L("Стойност: ")?></label><br /><br />
                <? $this->printElement('textarea',array('name'=>'txtValue','rows'=>'10','cols'=>'70')); ?>
            </div>
            <div class="editStructureArea">
                <? ?>
                <button name="btnAddKnot"><?=L("Добави възел")?></button>
                <button name="btnAddKey"><?=L("Добави ключ")?></button>
                <button name="btnDelete"><?=L("Изтрий ключ/възел")?></button>
                <div style="margin:10px 0 10px; background:none;"></div>
            </div>
            <br />
            <div class="toolbar">
                <? 	echo $this->createToolBarHTML(array(
                    array("closeButton"),
                    array("confirmButton")
                ));
                ?>
            </div>
        </div>
        <?php
    }

    public function getJSFunction() { return <<<'JS'
			(function(container, prefix, aBaseParams) {
				var aData={};
				var $sTree={};
				var flagNewKey=0;
				var flagCreateNew=0;
				var rlbkInit={};
				var editValueArea = $("#"+prefix).find(".editValueArea");
				var editStructureArea = $("#"+prefix).find(".editStructureArea");
				var txtValue = editValueArea.find("textarea[name=txtValue]");

				container.window.addListener("close",function(){ aBaseParams.pf().loadData(); }, container);

				aData['id'] = aBaseParams['editID'];

				if (aBaseParams['subtype']=='value') {
					editValueArea.css("display","block");
					txtValue.val(aBaseParams['value']);
				} else if (aBaseParams['subtype']=='structure') {
					editStructureArea.css("display","block");

					$sTree = editStructureArea.find("div").jstree({
						"themes" : { "theme" : "classic"},
						"core" : {"animation":0},
						"types" : {"types":{
											"root":{
													   "icon":{"image":"images/icons/db.png"},
														"create_node" : true,
														"valid_children" :  [ "key","node" ],
														"select_node" : true
											},
											"value":{
													   "icon":{"image":"images/icons/sym_right.png"},
														"create_node" : false,
														"valid_children" : "none",
														"delete_node" : false
											},
											"key":{
													  "icon":{"image":"images/icons/link.png"},
													  "create_node" : true,
													  "valid_children" : [ "value" ]
											},
											"tab":{
													 "icon":{"image":"images/icons/tab.png"},
													 "create_node" : true,
													 "valid_children" : [ "key","node","value" ]
											},
											"node":{
													 "icon":{"image":"images/icons/cog.png"},
													 "create_node" : true,
													 "valid_children" : [ "key","node" ]
											}
												}
						},
						"json_data" : { "data" : JSON.parse(aBaseParams['value'])},
						"plugins" : [ "themes", "json_data", "types", "crrm", "ui" ]
					})
					.bind("dblclick.jstree", function(){ $sTree.jstree("rename"); })
					.bind("rename_node.jstree", function (e, data){
						if (flagNewKey) {
							flagNewKey=0;
							data.inst.create_node(null, "last", { "attr" : { "rel" : "value"}, "data":"New value" });
						}
					})
					.bind("create_node.jstree", function (e, data){
						data.inst.deselect_all();
						data.inst.select_node(data.rslt.obj);
						data.inst.open_node(data.inst._get_parent(data.rslt.obj));
						data.inst.rename();
					});
					rlbkInit=$sTree.jstree('get_rollback');
				}

				$(container).find("button.confirm").click(function(){
					(aBaseParams['subtype']=='value') && (aData['value']=txtValue.val());
					(aBaseParams['subtype']=='structure') && (aData['value']=JSON.stringify($sTree.jstree("get_json",-1)));
					aBaseParams.parentForm().request('updateValues',aData,null,function(){ container.window.close(); });
				});

				editStructureArea.find("button[name=btnAddKnot]").click(function(){
					$sTree.jstree("create_node", null, "last", { "attr" : { "rel" : "node"}, "data":"New node" });
				});

				editStructureArea.find("button[name=btnAddKey]").click(function(){
					$sTree.jstree("create_node", null, "last", { "attr" : { "rel" : "key"}, "data":"New key" });
					flagNewKey=1;
				});

				editStructureArea.find("button[name=btnDelete]").click(function(){
					var selName = $sTree.jstree('get_selected').find("a").html();
					if (!selName) return;
					if ($sTree.jstree('get_selected').attr('rel')=="value") {
						framework.alert(L("Не може да триете стойност на ключ!"),"warning");
						return;
					}
					if ($sTree.jstree('get_selected').attr('rel')=="root") {
						framework.alert(L("Не може да триете цялото дърво!"),"warning");
						return;
					}
					$sTree.jstree("remove");
				});
			});
JS;
    }

}
