<?php
namespace main\settings;
use main\db\DBC;
use main\grid\GridPanelHandler;

/**
 * Class Settings
 * @package main\settings
 * @accessLevelDescription Управление на системни настройки
 */
class Settings extends GridPanelHandler {

	public function __construct() {
		$this->oBase = DBC::$main;
		$this->sReportSelectStatement = "SELECT SQL_CALC_FOUND_ROWS * FROM settings";
		
		$this->aColumns = array(			
			$this->newColumn(array(
				'type' => 'string',
				'dataField' => 'type',
				'width'	=> 150,				
				'exportWidth' => 100,
				'headerText' => 'Тип',
				'sortable'	=> true	,
				'resizable'	=> true	,
				'filterable'=> true
			)),						
			$this->newColumn(array(
				'type' => 'string',											
				'dataField' => 'name',
				'width'	=> 300,
				'exportWidth' => 200,
				'headerText' => 'Име',
				'sortable'	=> true	,
				'resizable'	=> true	,
				'filterable'=> true
			)),			
			$this->newColumn(array(
				'type' => 'string',
				'dataField' => 'description',
				'width'	=> 400,
				'exportWidth' => 300,
				'headerText' => 'Описание',
				'sortable'	=> true	,
				'resizable'	=> true	,
				'filterable'=> true
			)),
			$this->newColumn(
				array(
					'type' => 'button',
					'text' => '',
					'iconCls' => 'icon fa fa-pencil',
					'dataField' => 'btnEdit',
					'headerText' => ' ',
					'width' => 30,
					'disabledField' => 'btn_enabled',
					'resizable' => false,
					'sortable' => false	
			))
		);

		$this->aDefaultSort = array(
			array('field' => 'type', 'dir' => 'ASC')
		);
		
		parent::__construct();
	}

	public function init($aRequest = array(), $aInitParams = array()) {
        return $this->loadData($aRequest);
	}

	public function loadData($aRequest) {
		if (!empty($aRequest['fields']['txtSearch'])) {			
			$likeString = "%".DBC::$main->escape($aRequest['fields']['txtSearch'])."%";
			$this->aReportWhereStatement[] = sprintf("name LIKE '%s' OR description LIKE '%s'",$likeString,$likeString);
		}	
		return parent::loadData($aRequest,time()-60*60*24*30,time());
	}
	public function updateValues($aData){		
		$aUpdate['id']=$aData['id'];
		$aUpdate['value']=$aData['value'];
		DBC::$main->update('settings',$aData);
	}	
}
