<?php
require_once 'init.php';

use main\grid\Export;
use main\logs\Logs;
use main\System;

try {
	System::$aGetParams = @json_decode($_REQUEST['getParams'], true);
	$oExport = new Export();
	$oExport->doExport();
}
catch( Exception $e)
{
	Logs::logException($e);
	?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Export error</title>
	</head>
	<body>
	<?=$e->getMessage();?>
	</body>
	</html>
	<?php
}
?>