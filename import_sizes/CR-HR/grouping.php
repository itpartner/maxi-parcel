<?php
use main\db\DBC;

require_once '../../init.php';

$hGroup = [
    'R-9.75;					',
    'R-10;					',
    'R-11;					',
    'R-12;SQ-10х10					',
    'R-12.6;					',
    'R-12.7;					',
    'R-13;					',
    'R-13.25;					',
    'R-13.5;					',
    'R-14;SQ-12х12					',
    'R-15;					',
    'R-15.7;					',
    'R-15.85;					',
    'R-16;SQ-13x13					',
    'R-17.2;					',
    'R-18;REC-20x10					',
    'R-18.3;					',
    'R-19;REC-10x20;SQ-15x15;FOV-23x12					',
    'R-19.85;					',
    'R-20;FOV-25x10;FOV-24x12;					',
    'R-20.3;					',
    'R-21;					',
    'R-21.3;REC-15x20;REC-20x15;REC-25x10;					',
    'R-22;SQ-18x18					',
    'R-22.2;					',
    'R-22.4;					',
    'R-23;FOV-30x10					',
    'R-24;REC-30x10;OV-30x15					',
    'R-24.7;					',
    'R-24.85;FOV-30x15					',
    'R-25;REC-15x25;SQ-19x19;REC-25x15;SQ-20x20;FOV-15x30;					',
    'R-25.4;					',
    'R-26;OV-30x22;FOV-31.75x15.88					',
    'R-26.9;SQ-22x22					',
    'R-27.4;					',
    'R-28;REC-15x30;REC-20x25;REC-30x15;					',
    'R-28.6;					',
    'R-30;					',
    'R-31.8;					',
    'R-32;REC-20x30;SQ-25.4x25.4;FOV-38x20;REC-30x20;FOV-20x38;					',
    'R-32.15;					',
    'R-33.5;					',
    'R-33.7;REC-20x30;REC-30x20;					',
    'R-34;REC-35x20					',
    'R-34.4;					',
    'R-35;					',
    'R-36;					',
    'R-38;REC-40x20;SQ-30x30;OV-43x33;REC-20x40;REC-25x35;REC-35х25;					',
    'R-39.8;					',
    'R-40;REC-45x20;SFOV-30x40;REC-40x25;					',
    'R-41.8;					',
    'R-42;FOV-50x30;OV-50x30					',
    'R-42.4;OV-50x35					',
    'R-44.5;					',
    'R-45;REC-25x45;SQ-35x35;FOV-60x20;REC-30x40;REC-40x30;					',
    'R-47;REC-25x50;SQ-38x38;REC-50x25;REC-60x15;					',
    'R-48;					',
    'R-48.3;REC-50.8x25.4					',
    'R-49.8;					',
    'R-50;OV-60x30					',
    'R-50.8;					',
    'R-51;REC-30x50;REC-50x30;REC-60x20;					',
    'R-52.4;					',
    'R-54;REC-60x25;SQ-45x45;					',
    'R-57;					',
    'R-59.2;					',
    'R-60;REC-70x25x<=2.5;REC-80x20x>=2.2;					',
    'R-62.8;REC-40x60x>=1.8;REC-80x20x<=2.2;					',
    'R-63.5;REC-40x60x<1.8;SQ-50x50;REC-60x40;SQ-50.8x50.8;REC-70x30x>=1.5 AND s <=2.2;REC-80x25x>=2.2;					',
    'R-70;REC-40x70;SQ-55x55;FOV-100x20;REC-50x60;REC-60x50;REC-70x40;REC-80x30;					',
    'R-76.2;REC-40x80;SQ-60x60;REC-50x70;REC-70x50;REC-80x40;					',
    'R-80;REC-50x80;REC-80x50;REC-90x40;					',
    'R-86;REC-80x60;SQ-70x70					',
    'R-88.9;REC-60x80;SQ-70x70;REC-100x40;REC-90x50;					',
    'R-95;REC-100x50					',
    'R-101.6;REC-90x70;SQ-80x80;REC-100x60;REC-120x40;					',
    'R-108;					',
    'R-114.3;REC-100x80;SQ-90x90;REC-120x60;					',
    'R-127;REC-80x120;SQ-100x100;REC-120x80;REC-140x60;REC-140x70;					',
    'R-139.7;REC-140x80					',
    'R-159;REC-150x100					',
    'R-168.3;					',
];

$vR_group = [
    '10;9.75',
    '11;',
    '12;',
    '12.6;',
    '12.7;',
    '13;',
    '13.25;',
    '13.5;',
    '14;',
    '15;',
    '16;15.7;15;15.85',
    '17.2;',
    '18;18.3',
    '20;19.85;20.3;19;',
    '21.3;21;21.8;23;22.4;22;22.2;22.4;',
    '25;24.85;24.7;24;25.4;',
    '26.9;27.4;26;',
    '28;28.6',
    '30;32;32.15;31.8;',
    '33.7;34;34.4;33.5;35;34.4;',
    '38;36',
    '40;39.8',
    '42;41.8;42.4;',
    '45;42;44.5;47;',
    '48;',
    '48.3;',
    '49.8;',
    '51;50.8;50;52.4;54;49.8;',
    '57;',
    '60;59.2',
    '63.5;62.8',
    '76.2;70',
    '86;80',
    '88.9;',
    '95;',
    '101.6;',
    '108;',
    '114.3;',
    '127;',
    '139.7;',
    '159;',
    '168.3;',
];

$vg = array_map(function ($s) {
    return array_filter(array_map(function ($ss) {
        $ss = trim($ss);
        if(empty($ss)) return null;
        return 'R-' . trim($ss);
    }, explode(';', $s)), 'strlen');
}, $vR_group);

$hgTemp = array_map(function ($s) {
    return array_filter(array_map(function ($ss) {
        if(empty($ss)) return null;
        return trim($ss);
    }, explode(';', $s)), 'strlen');
}, $hGroup);

foreach ($hgTemp as $v) $hg[reset($v)] = $v;

foreach ($hg as $r => $g) {
    $filtered = array_filter($vg, function ($vgroup) use ($r) {
        return !empty(array_keys($vgroup, $r));
    });

    foreach ($filtered as $row) {
        foreach ($row as $vgg) {
            if(!empty($hg[$vgg])) $hg[$r] = array_merge($hg[$r], $hg[$vgg]);
        }
    }
}

$hg = array_map(function ($j) {
    $j = array_values(array_unique($j));
    sort($j);
    return $j;
}, $hg);


$groups = [];

foreach ($hg as $group) {
    $groups[implode('@@@', $group)] = $group;
}

$groups = array_values($groups);


foreach (DBC::$main->selectColumn("select id from production_lines") as $id_line) {

    $gg = [];

    foreach ($groups as $kk => $group) {
        foreach ($group as $k => $size) {
            $range = '';
            if(substr_count($size, 'x') > 1) {
                $range = end(explode('x', $size));
                $size = str_replace($range, '', $size);
            }

            $sizes = DBC::$main->selectColumn("
                SELECT 
                 pls.id
                FROM production_sizes ps 
                JOIN production_lines_sizes pls ON pls.id_size = ps.id AND pls.id_line = $id_line
                WHERE 1
                AND ps.id_metal IN(4,5)
                AND ps.code like '{$size}%'
        " . (!empty($range) ? "AND s " . $range : '') . " group by ps.code");
            $groupsNew[$kk] = !$groupsNew[$kk] ? [] : $groupsNew[$kk];
            $groupsNew[$kk] = array_merge($groupsNew[$kk],$sizes);
        }
    }

    $i = 1;
    foreach($groupsNew as $group) {
        if(empty($group)) continue;

        $data = array_map(function($k) use($i){return ['id_line_size' => $k, 'group' => $i] ;},$group);

        DBC::$main->multiInsert('production_lines_sizes_groups', $data);

        $i++;
    }

    $bla = 4;

    $groupsNew = [];

}


$Bla = 4;
