<?php
use emc\nomenclatures\Profiles;
use emc\nomenclatures\sizes\Sizes;

ini_set('error_reporting','E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED & ~E_WARNING');

require_once '../../init.php';

try {
    require_once(BASE_PATH . '/src/PHPExcel/PHPExcel.php');


    $excel = new PHPExcel_Reader_Excel5();
    $doc = $excel->load('SQ.xls');

    $sizes = [];

    foreach ($doc->getAllSheets() as $sheet) {
        $data = $sheet->toArray('', true);

        $ss = array_slice($data[5], 3, 41, true);

        foreach ($data as $rowIndex => $columns) {
            if($rowIndex < 8) continue;

            foreach ($ss as $columnIndex => $s) {
                if(empty($data[$rowIndex][$columnIndex])) continue;
                foreach (array('4','5') as $id_metal) {
                    $size = [];
                    $size['profile_type'] = Profiles::SQ;
                    $size['s'] = $s;
                    $size['a'] = $data[$rowIndex][1];
                    $size['id_metal'] = $id_metal;
                    $size['code'] = Sizes::getSizeCode($size);
                    $size['m'] = Sizes::calcLinearMass($size);
                    if(round($size['m'],3) != round($data[$rowIndex][$columnIndex],3)) {
                        throw new Exception("diff in mass for $rowIndex / $columnIndex, expected {$data[$rowIndex][$columnIndex]}, got {$size['m']}");
                    }
                    $sizes[] = $size;
                }
            }
        }
        break;
    }

    \main\db\DBC::$main->multiInsert('import.production_sizes_tmp', $sizes);


} catch (\main\Exception $e) {
    die(print_r(\main\Exception::convertToJSObject($e)));
}
