<?php
use main\db\DBC;

require_once '../../init.php';

$groupsCounterByLine = DBC::$main->selectAssoc("
select pl.id_line,max(g.group) as g from production_lines_sizes_groups g 
join production_lines_sizes pl ON pl.id = g.id_line_size
group by pl.id_line
");


$groups = [];

foreach (DBC::$main->select("
    select 
    pls.id,
    pls.id_line,
    ''
    from production_lines_sizes pls
    join production_sizes ps on ps.id = pls.id_size
    left join production_lines_sizes_groups plsg on plsg.id_line_size = pls.id
    where 1
    and plsg.id_line_size is null
") as $lineSize) {
    $groups[] = [
        'id_line_size' => $lineSize['id'],
        'group' => ++$groupsCounterByLine[$lineSize['id_line']],
        'id_line' => $lineSize['id_line'],
    ];
}


$b = 3;



