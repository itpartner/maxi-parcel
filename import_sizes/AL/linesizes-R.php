<?php

ini_set('error_reporting', 'E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED & ~E_WARNING');
require_once '../../init.php';

try {
    require_once(BASE_PATH . '/src/PHPExcel/PHPExcel.php');

    $excel = new PHPExcel_Reader_Excel5();
    $doc = $excel->load('linesizes-R+.xls');

    $sizes = [];

    $lines = ['DMS' => 9, 'MTM' => 10];

    foreach ($doc->getAllSheets() as $sheet) {
        $data = $sheet->toArray('', true);
        $ss = array_slice($data[5], 4, 24, true);

        foreach ($data as $rowIndex => $columns) {
            if($rowIndex < 6) continue;
            if($rowIndex > 63) break;

            foreach ($ss as $columnIndex => $s) {
                if(empty($data[$rowIndex][$columnIndex])) continue;

                $r = $data[$rowIndex][1];
                $lineCodes = array_map('trim',explode('/',trim($data[$rowIndex][2])));

                $code = \emc\nomenclatures\sizes\Sizes::getSizeCode(['profile_type' => \emc\nomenclatures\Profiles::R, 'd' => $r, 's' => $s, 'id_metal' => 8]);

                $speeds = array_map('trim', explode('/',$data[$rowIndex][$columnIndex]));

                foreach($lineCodes as $k => $lineCode) {
                    $sizes[$code][$lines[$lineCode]] = $speeds[$k] ? $speeds[$k] : $speeds[0];
                }
            }
        }
        break;
    }

    $pSizes = \main\db\DBC::$main->selectAssoc("SELECT code, id from production_sizes WHERE code in(".implode(',',array_map(array(\main\db\DBC::$main,'quote'),array_keys($sizes))).")");


    $forInsert = [];

    foreach($sizes as $code => $l) {
        foreach($l as $id_line => $speed) {
            if(empty($speed) || empty($id_line) || empty($pSizes[$code])) throw new Exception($speed.'-'.$id_line.'-'.$pSizes[$code].'-'.$code);
            $forInsert[] = [
                'id_size' => $pSizes[$code],
                'id_line' => $id_line,
                'speed' => $speed
            ];
        }

    }

    \main\db\DBC::$main->multiInsert('production_lines_sizes', $forInsert);


    $bla = 3;

} catch (\main\Exception $e) {
    die(print_r(\main\Exception::convertToJSObject($e)));
}
