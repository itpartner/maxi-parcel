<?php
use main\db\DBC;

require_once '../../init.php';

$hGroup = [
    'R-8;',
    'R-10;',
    'R-12;',
    'R-12.7;',
    'R-13;',
    'R-14;',
    'R-15;',
    'R-16;',
    'R-17;SQ-14x14',
    'R-17.5;',
    'R-17.75;',
    'R-18;OV-22x14',
    'R-19;SQ-15x15',
    'R-20;OV-24x16',
    'R-20.5;',
    'R-21;',
    'R-22;REC-20x15',
    'R-22.2;',
    'R-22.5;',
    'R-23;',
    'R-23.1;',
    'R-23.5;',
    'R-23.95;',
    'R-24;',
    'R-25;SQ-20x20;OV-30.3x18.3;',
    'R-25.15;',
    'R-25.4;',
    'R-26;',
    'R-27;',
    'R-28;REC-15x30;FOV-30x15;',
    'R-29;',
    'R-30;FOV-32x25;OV-35.3x23.3;',
    'R-31.6;',
    'R-32;REC-20x33;FOV-40x20;REC-20x35;FOV-40x20;',
    'R-33;',
    'R-34.8;',
    'R-35;FOV-37.5x29;OV-44x24;',
    'R-36;REC-20x40;OV-45x28;',
    'R-37.7;',
    'R-38;',
    'R-38.1;',
    'R-39;',
    'R-40;REC-20x50',
    'R-42;',
    'R-45;REC-22x50',
    'R-50;REC-20x61.5;REC-17x63;',
    'R-50.8;',
    'R-51.5;',
    'R-56.3;',
    'R-60;',
    'R-62.8;REC-20x80',
    'R-66;',
    'R-69.2;FOV-100x15',
];

$vR_group = [
    '10;8',
    '12;',
    '12.7;',
    '13;',
    '14;',
    '15;',
    '16;',
    '17;',
    '17.5;',
    '17.75;',
    '18;19',
    '20;22.2;22;',
    '20.5;',
    '21;',
    '22.5;',
    '23;',
    '23.1;',
    '23.5;',
    '23.95;',
    '25;25.4;25.15;24;26;',
    '28;27;',
    '29;',
    '30;',
    '31.6;',
    '33;32;',
    '36;34.8;35;',
    '37.7;',
    '39;',
    '40;38.1;38;',
    '42;',
    '45;',
    '50;50.8;',
    '51.5;',
    '56.3;',
    '60;',
    '62.8;',
    '66;',
    '69.2;',
];

$vg = array_map(function ($s) {
    return array_filter(array_map(function ($ss) {
        $ss = trim($ss);
        if(empty($ss)) return null;
        return 'R-' . trim($ss);
    }, explode(';', $s)), 'strlen');
}, $vR_group);

$hgTemp = array_map(function ($s) {
    return array_filter(array_map(function ($ss) {
        if(empty($ss)) return null;
        return trim($ss);
    }, explode(';', $s)), 'strlen');
}, $hGroup);

foreach ($hgTemp as $v) $hg[reset($v)] = $v;

foreach ($hg as $r => $g) {
    $filtered = array_filter($vg, function ($vgroup) use ($r) {
        return !empty(array_keys($vgroup, $r));
    });

    foreach ($filtered as $row) {
        foreach ($row as $vgg) {
            if(!empty($hg[$vgg])) $hg[$r] = array_merge($hg[$r], $hg[$vgg]);
        }
    }
}

$hg = array_map(function ($j) {
    $j = array_values(array_unique($j));
    sort($j);
    return $j;
}, $hg);


$groups = [];

foreach ($hg as $group) {
    $groups[implode('@@@', $group)] = $group;
}

$groups = array_values($groups);


foreach (DBC::$main->selectColumn("select id from production_lines") as $id_line) {

    $gg = [];

    foreach ($groups as $kk => $group) {
        foreach ($group as $k => $size) {
            $range = '';
            if(substr_count($size, 'x') > 1) {
                $range = end(explode('x', $size));
                $size = str_replace($range, '', $size);
            }

            $sizes = DBC::$main->selectColumn("
                SELECT 
                 pls.id
                FROM production_sizes ps 
                JOIN production_lines_sizes pls ON pls.id_size = ps.id AND pls.id_line = $id_line
                WHERE 1
                AND ps.id_metal IN(8)
                AND ps.code like '{$size}%'
        " . (!empty($range) ? "AND s " . $range : '') . " group by ps.code");
            $groupsNew[$kk] = !$groupsNew[$kk] ? [] : $groupsNew[$kk];
            $groupsNew[$kk] = array_merge($groupsNew[$kk],$sizes);
        }
    }

    $i = 1;
    foreach($groupsNew as $group) {
        if(empty($group)) continue;

        $data = array_map(function($k) use($i){return ['id_line_size' => $k, 'group' => $i] ;},$group);

        DBC::$main->multiInsert('production_lines_sizes_groups', $data);

        $i++;
    }

    $bla = 4;

    $groupsNew = [];

}


$Bla = 4;
