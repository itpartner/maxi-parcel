<?php

use emc\nomenclatures\Profiles;
use emc\nomenclatures\sizes\Sizes;

ini_set('error_reporting', 'E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED & ~E_WARNING');
require_once '../../init.php';

try {
    require_once(BASE_PATH . '/src/PHPExcel/PHPExcel.php');

    $excel = new PHPExcel_Reader_Excel5();
    $doc = $excel->load('linesizes-FOV.xls');
    $sizes = [];
    $lines = ['DMS' => 9, 'MTM' => 10];

    foreach ($doc->getAllSheets() as $sheet) {
        $data = $sheet->toArray('', true);
        $ss = array_slice($data[6], 4, 27, true);

        foreach ($data as $rowIndex => $columns) {
            if($rowIndex < 7) continue;
            if($rowIndex > 21) break;

            foreach ($ss as $columnIndex => $s) {
                if(empty($data[$rowIndex][$columnIndex])) continue;
                $a = $data[$rowIndex][0];
                $h = $data[$rowIndex][1];

                $idline = $lines[trim($data[$rowIndex][3])];

                $sizes[Sizes::getSizeCode(['profile_type' => Profiles::FOV, 'a' => $a, 'h' => $h, 's' => $s,'id_metal' => 8])] = $idline;
            }
        }
        break;
    }

    $pSizes = \main\db\DBC::$main->selectAssoc("SELECT code, id from production_sizes WHERE code in(" . implode(',', array_map(array(\main\db\DBC::$main, 'quote'), array_keys($sizes))) . ")");
    $forInsert = [];

    foreach($sizes as $code => $idLine) $forInsert[] = ['id_size' => $pSizes[$code], 'id_line' => $idLine, 'speed' => 40];

    \main\db\DBC::$main->multiInsert('production_lines_sizes', $forInsert);

    $bla = 3;

} catch (\main\Exception $e) {
    die(print_r(\main\Exception::convertToJSObject($e)));
}
