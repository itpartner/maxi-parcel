<?php
require_once 'init.php';

use main\Access;
use main\EmptySession;
use main\Exception;
use main\form\FormStates;
use main\logs\Logs;
use main\System;
use main\TPLBase;
use main\TPLError;

$sPrefix = '';
try {
    ob_start();

    FormStates::saveStates(json_decode($_REQUEST['formStates'],true));

    $sPrefix = $_REQUEST['prefix'] ? $_REQUEST['prefix'] : $_REQUEST['file'];
    $sName = $_REQUEST['file'] ? $_REQUEST['file'] : $_REQUEST['target'];

    $aFrameConfig = isset($_REQUEST['frameConfig']) ? json_decode($_REQUEST['frameConfig'],true) : array();

    if(empty($sPrefix)) $sPrefix = 'content';

    if(!empty($_REQUEST['direct']) && !empty(($aFrameConfig))) {
        if(empty($sName) && !empty($aFrameConfig['tplName'])) $sName = $aFrameConfig['tplName'];
        $aFrameConfig['closable'] = false;
    }

    Access::checkAccess($sName);
    System::$aGetParams = @json_decode($_REQUEST['getParams'],true);

    $target = parseNS($sName);
    $sTPLClassName = $target['ns'].'\\TPL'.$target['className'];
    $baseParams = json_decode($_REQUEST['baseParams'],true);

    if(!class_exists($sTPLClassName, true)) throw new Exception(L("Страница %s не съществува.",$sTPLClassName));
    $oTpl = new $sTPLClassName($sPrefix, $baseParams);

    if(!($oTpl instanceof TPLBase)) throw new Exception(L("Страница %s не съществува.",$sTPLClassName));

    if(!empty($_REQUEST['direct'])) {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                jQuery(document).ready(function () {
                    framework.attachMethods(document.body);
                });
                jQuery(window).resize(function () {
                    layout.autoGridHeight();
                });
            });
        </script>
        <?php
    }

    if(!empty($_REQUEST['frameConfig']) || !empty($oTpl->bAlwaysPrintWithFrame)) {
        $oTpl->printHTMLWithFrame($aFrameConfig);
    } else {
        $oTpl->printHTMLWithoutFrame();
    }

    if(!empty($_REQUEST['direct'])) {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function() {

                var name = '<?=$sName?>';
                var prefix = '<?=$sPrefix?>';
                var rootEl = document.getElementById(prefix) || {};

                var initParams = <?=empty($oTpl->aInitParams) ? '{}' : @json_encode($oTpl->aInitParams)?>;
                var paramsID = <?=empty($_POST['paramsID']) ? 0 : intval($_POST['paramsID'])?>;
                if(paramsID && window.opener && window.opener != window) {
                    var p = window.opener.framework.window.getPopupParams(paramsID);
                    if(p) initParams = p;
                }

                var fn = rootEl.fn;
                if(typeof (fn) == 'function') fn.apply({}, [rootEl, prefix, initParams]);
                else if(typeof(fn) == 'string') {
                    framework.addLoadingMask();
                    Server.loadFnScript(name, function(realFn){
                            realFn.apply({}, [rootEl, prefix, initParams]);
                            framework.removeLoadingMask();
                        }, {}, function(){
                            framework.removeLoadingMask();
                        }
                    );
                }

                if(rootEl.initResponse) rootEl.processResponse(rootEl.initResponse,null,{});
                if(rootEl.initError) rootEl.setError(rootEl.initError);

                <?php if(!empty($oTpl->bLoadDirectAutoResize)):?>
                var width = 0;
                var height = 0;
                if(window.outerWidth) width = window.outerWidth - window.innerWidth;
                if(window.outerHeight) height = window.outerHeight - window.innerHeight;
                window.resizeTo(jQuery(document).width() + width,jQuery(document).height() + height);
                <?php endif;?>
            });
        </script>
        <?php
    }

    echo(ob_get_clean());
} catch (Exception $e) {
    Logs::logException($e);
    ob_clean();
    if($e instanceof EmptySession) {
        echo "<script type='text/javascript'>Server.onSessionExpire();</script>";
        die();
    }
    $oErrTpl = new TPLError($sPrefix,$e);
    $aFrameConfig = !empty($aFrameConfig) ? $aFrameConfig : array();
    $oErrTpl->printHTMLWithFrame($aFrameConfig);
}
