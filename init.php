<?php

use main\db\DBC;
use main\db\DBConnection;

if(PHP_MAJOR_VERSION < 7) die("PHP 7.x.x required");

define('BASE_PATH', dirname(__FILE__));
date_default_timezone_set('Europe/Sofia');
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_WARNING);
ini_set('display_errors', false);
mb_internal_encoding("UTF-8");

if(!defined('REQUEST_TIME_LIMIT')) define('REQUEST_TIME_LIMIT', 300);
set_time_limit(REQUEST_TIME_LIMIT);

require_once 'config.php';

if(!defined('BASE_URL')) {
    $sURL = 'http';
    if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') $sURL .= 's';

    $sURL .= '://';
    if($_SERVER['SERVER_PORT'] != '80' && $_SERVER['SERVER_PORT'] != '443') $sURL .= $_SERVER['HTTP_HOST'] . ':' . $_SERVER['SERVER_PORT'];
    else $sURL .= $_SERVER['HTTP_HOST'];

    $sURL .= (dirname($_SERVER['SCRIPT_NAME']) == '.' ? '' : dirname($_SERVER['SCRIPT_NAME']));

    define('BASE_URL', rtrim($sURL, '\\/'));
}

require_once BASE_PATH . '/src/main/autoload.php';
require_once BASE_PATH . '/src/emc/autoload.php';
require_once BASE_PATH . '/src/pallex/autoload.php';

set_exception_handler('main\logs\Logs::logUncaughtException');

if(!empty($DSNs) && is_array($DSNs)) {

    if($GLOBALS['memcacheHost'] && class_exists("Memcached")) {
        $GLOBALS['memcacheHostA'] = explode(':', $GLOBALS['memcacheHost']);
        $GLOBALS['memcache'] = new Memcached();
        $GLOBALS['memcache']->addServer($GLOBALS['memcacheHostA'][0], $GLOBALS['memcacheHostA'][1] ? $GLOBALS['memcacheHostA'][1] : 11211);
    }
    $GLOBALS['connectionsByDSN'] = array();
    foreach ($DSNs as $name => $DSN) {
        if($GLOBALS['connectionsByDSN'][$DSN]) {
            $connection = $GLOBALS['connectionsByDSN'][$DSN];
        } else {
            $connection = new DBConnection($DSN, $GLOBALS['memcache']);
            $connection->setTimeZone(date('P'));
            $connection->execute("SET sql_mode='ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER'");
            $connection->execute('SET SESSION group_concat_max_len = 400000');
            $connection->execute('SET SESSION innodb_table_locks = FALSE');
            $GLOBALS['connectionsByDSN'][$DSN] = $connection;
            DBC::$connections[] = $connection;
        }
        DBC::$$name = $connection;
    }
    unset($GLOBALS['connectionsByDSN']);
    unset($GLOBALS['memcacheHostA']);
}


if(!defined('IS_ROBOT')) define('IS_ROBOT', false);
if(!$_SERVER['HTTP_AUTHORIZATION']) session_start();