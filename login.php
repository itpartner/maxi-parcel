<?php
use main\Login;
use main\logs\Logs;

try {
    require_once "init.php";

    if(SSL !== false && $_SERVER["SERVER_PORT"] != 443) {
        $sGet = $_GET['logout'] == "true" ? '?logout=true' : '';
        $sHTTPSURL = str_replace('http://', 'https://', BASE_URL);
        header("Location: $sHTTPSURL$sGet");
        exit();
    }

    $errLogin = false;

    $aRedirectToParams = array();
    $aURL = parse_url($_REQUEST['redirect_to']);
    parse_str($aURL['query'], $aRedirectToParams);


    if($_GET['logout'] == "true") {
        $_SESSION = null;
    }

    $eMsg = empty($_POST['eMsg']) ? '' : $_POST['eMsg'];
    Login::pass($_POST);

} catch (Exception $e) {
    Logs::logException($e);
    $errLogin = true;
    $eMsg = $e->getMessage();
}
?>
<!DOCTYPE html>
<html>
<head>
    <link type="text/css" href="css/site.css" rel="stylesheet"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Login</title>
    <script type="text/javascript" src="js/include/jquery.min.js"></script>
    <script type="text/javascript" src="js/include/util.js"></script>
    <script type="text/javascript" src="js/plugins/jquery.cookie.js"></script>
    <script type="text/javascript">
        BASE_URL = "<?=BASE_URL?>";
    </script>
    <style>
        #cm {
            background: none;
        }
        body {
            background: rgb(206, 220, 231);
            background: radial-gradient(ellipse at center, rgba(206, 220, 231, 1) 0%, #607D8B 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#cedce7', endColorstr='#607D8B', GradientType=1);
        }

        #login {
            position: relative;
            margin-top: -5em;
            display: inline-block;
            background-color: hsla(240, 20%, 99%, 1);
            box-shadow: 0px 0px 25px rgba(0, 0, 0, 0.5);
            border-radius: 3px;
        }

        #cm .fa {
            width: 35px;
            height: 30px;
            line-height: 30px;
            font-size: 26px;
            text-align: center;
        }

        #cm button {
            font-size: 26px;
        }

        #cm button .icon {
            width: 60px;
            height: 30px;
            line-height: 30px;
            font-size: 26px;
        }

        #cm input:not([type="radio"]):not([type="checkbox"]) {
            padding-left: 5px;
            font-size: 16px;
            line-height: 30px;
            height: 30px;
        }

        .errMsg {
            color: #607D8B;
            font-size: 14px;
            text-shadow: 0 0 5px #FFFFFF;
        }

        .logo {
            height: 280px;
            /*opacity: 0.6;*/
            background-image: url('images/maxi-parcel.svg');
            background-repeat: no-repeat;
            background-size: 450px;
            background-size: 100% 100%;
            background-position: center;
        }

    </style>
</head>
<body>
<div id="cm" style="text-align:center; overflow: hidden;">
    <table width="100%" height="100%">
        <tr style="height: 40%;">
            <td>
                <div class="logo">
                </div>
            </td>
        </tr>
        <tr>
            <td valign="top" align="center">
                <div id="login">
                    <form action="login.php?redirect_to=<?= $_REQUEST['redirect_to'] ?>" method="post" name="loginForm">
                        <table border="0" cellspacing="5">
                            <tr>
                                <td><label for="userLogin"><span class="icon fa fa-user"></span></label></td>
                                <td>
                                    <input type="text" name="userlogin" style="width:150px" id="userLogin" tabindex=1/>
                                    <script type="text/javascript">document.getElementById('userLogin').focus()</script>
                                </td>
                                <td rowspan="2">
                                    <button type="submit" name="enter" style="width:100%;height:100%;line-height: 76px;" tabindex=4>
                                        <span class="icon fa fa-sign-in"></span>
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="userpass"><span class="icon fa fa-key"></span></label></td>
                                <td>
                                    <input type="password" id="userpass" name="userpass" style="width:150px" tabindex=2 />
                                </td>
                            </tr>
                            <tr>
                                <td><label for="inputstring"><span class="icon fa fa-question"></span></label></td>
                                <td>
                                    <input type="text" id="inputstring" name="inputstring" style="width:150px" tabindex=3 autocomplete="off"/>
                                </td>
                                <?php
                                    $_SESSION['captcha'] = rand(1000, 9999);
                                ?>
                                <td><img src="<?=BASE_URL?>/genis.php" style="width: 100px; height: 33px;border-radius: 3px; border: 1px solid #cccccc;"/></td>
                            </tr>
                        </table>
                    </form>
                </div>
                <div class="errMsg" style="padding-top: 5px;"><?= L($eMsg) ?></div>
            </td>
        </tr>
    </table>
</div>
</body>
</html>