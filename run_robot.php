<?php
use main\db\DBC;
use main\db\DBSQLException;
use main\Exception;
use main\InvalidParameter;
use main\logs\Logs;
use main\Settings;
use main\Util;

define('IS_ROBOT', true);

require_once 'init.php';

$task = [];
try {
    if(!Util::isCli()) {
        if(!Util::checkIP($_SERVER['REMOTE_ADDR'], explode("\n", Settings::$aSettings['SYSTEM']['run_robot_allowed_ips'][0]))) throw new Exception(L('Access denied'));

        $shellParams = [];
        if(($taskId = intval($_GET['task_id']))) $shellParams[] = $taskId;
        else $shellParams[] = escapeshellarg($_GET['name']);

        unset($_GET['task_id']);
        unset($_GET['name']);

        $_GET['base_url'] = BASE_URL;
        foreach ($_GET as $paramName => $paramValue) $shellParams[] = escapeshellarg("{$paramName}={$paramValue}");

        $bg = (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' ? '' : ' > /dev/null 2>&1 &');
        exec('php ' . escapeshellarg(__FILE__) . ' ' . implode(' ', $shellParams) . $bg);

        echo 'Task is added';

        die();
    }

    set_error_handler(function ($severity, $message, $file, $line) {
        if($severity & (E_USER_ERROR | E_ERROR | E_RECOVERABLE_ERROR)) {
            throw new ErrorException($message, 0, $severity, $file, $line);
        } else {
            return false;
        }
    });

    $ob = '';
    ob_start(function ($t) use (&$ob) {
        $ob .= $t;
        return $t;
    }, 2);

    $retryDelay = 100000;
    retry:
    try {
        $trans = DBC::$main->startTrans();

        $sqlInfo = DBC::$main->selectRow("
			SELECT
				UNIX_TIMESTAMP() - gs.variable_value AS start_time,
				CONNECTION_ID() AS cid
			FROM performance_schema.global_status AS gs
			WHERE TRUE
				AND gs.variable_name = 'uptime'
		");
        //check params
        if(is_numeric($argv[1])) {
            if(!($task = DBC::$main->selectRow("
				SELECT
					rt.*
				FROM robot_tasks AS rt
				WHERE TRUE
					AND rt.id = {$argv[1]}
				FOR UPDATE
			"))
            ) throw new Exception(L('task not found'));
            if($task['status'] != 'wait') die("already started");

            $params = json_decode($task['params'], true);
        } else {
            $task = ['robot_name' => $argv[1]];
            $params = [];
            if($argc > 2) {
                for ($i = 2; $i < $argc; $i++) {
                    $a = explode('=', $argv[$i]);
                    if(empty($a[0])) continue;
                    $params[$a[0]] = empty($a[1]) ? '' : $a[1];
                }
            }
            if($params['base_url']) {
                $task['base_url'] = $params['base_url'];
                unset($params['base_url']);
            }
            $task['params'] = json_encode($params, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        }

        $task['status'] = 'running';
        $task['sql_start_time'] = $sqlInfo['start_time'];
        $task['sql_cid'] = $sqlInfo['cid'];
        $task['os_host'] = gethostname();
        $task['os_pid'] = getmypid();
        $task['start_time'] = date('Y-m-d H:i:s');

        DBC::$main->insert('robot_tasks', $task);

        $className = $task['robot_name'];
        if(empty($task['robot_name']) || !class_exists($className)) throw new InvalidParameter(L('Robot not found: ') . $className);
        if(!in_array('main\daemons\BaseRobot', class_parents($className))) throw new InvalidParameter(L('Robot instance error: ') . $className);
        $time = time();

        /** @var main\daemons\BaseRobot $bot */
        $bot = new $className();

        $bot->baseURL = $task['base_url'];
        $bot->robotName = $task['robot_name'];
        $bot->taskID = $task['id'];

        if(!empty($bot->mutexRobots)) {
            $escapedRobotNames = implode(',', array_map(array(DBC::$main, 'quote'), $bot->mutexRobots));
            if(!empty(DBC::$main->select("
				SELECT
					pl.ID AS id
				FROM information_schema.PROCESSLIST AS pl
				WHERE pl.ID IN (
					SELECT
						rt.sql_cid
					FROM robot_tasks AS rt
					WHERE TRUE
						AND rt.status = 'running'
						AND rt.robot_name IN ($escapedRobotNames)
						AND rt.sql_start_time = {$sqlInfo['start_time']}
						AND rt.id != {$task['id']}
				)
				FOR UPDATE
			"))
            ) die("Mutex robot already running ({$escapedRobotNames}) - " . date('Y-m-d H:i:s', $time) . "\r\n");
        }

        $trans->commit();
    } catch (Exception $e) {
        if(!empty($trans)) {
            if($e instanceof DBSQLException && strpos($e->getSQLError(), 'try restarting transaction') !== false && ($retryDelay *= 2) < 5000000) {
                $trans->rollback();
                goto retry;
            }
            $trans->commit();
        }
        throw $e;
    }

    echo sprintf("%s %s started \r\n", date('Y-m-d H:i:s', $time), $task['robot_name']);

    set_time_limit(0);

    $bot->run($time, $params);

    DBC::$main->resetTrans();

    echo sprintf("%s %s finished \r\n", date('Y-m-d H:i:s', time()), $task['robot_name']);

    //albanski update za da ne dava deadlock ot insert-update
    $escapedOb = DBC::$main->escape($ob);
    DBC::$main->execute("
        UPDATE robot_tasks SET
            status = 'finished',
            end_time = NOW(),
            output = '$escapedOb'
        WHERE id = {$task['id']}
    ");

} catch (Exception $e) {
    DBC::$main->resetTrans();
    if($task['id']) {
        $task['status'] = 'failed';
        $task['output'] = $ob;
        $task['error'] = serialize(Exception::convertToLogObject($e));
        if($task['error'] === false) $task['error'] = "Unserializable error";
        $task['end_time'] = date('Y-m-d H:i:s');
        DBC::$main->multiInsert('robot_tasks', [$task]);
        $id = $task['id'];
    } else {
        $id = Logs::logException($e);
    }

    try {
        $robotName = ($task['robot_name'] ? $task['robot_name'] : $argv[1]);
        $subject = "Възникна проблем с робот {$robotName}";
        main\notifications\Mailer::send(explode(',', main\Settings::$aSettings['SYSTEM']['monitoring_mail'][0]), $subject, "{$subject}:<br/>" . $e->getMessage() . ' (' . $id . ')');
    } catch (\Exception $ex) {
    }

    echo $e->getMessage();
}